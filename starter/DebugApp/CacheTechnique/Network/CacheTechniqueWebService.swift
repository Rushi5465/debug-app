//
//  CacheTechniqueWebService.swift
//  BloodPressureApp
//
//  Created by Rushikant on 25/12/21.
//

import Foundation

// swiftlint:disable all
class CacheTechniqueWebService {
	private var graphQL: ApolloClientService
	private var service: RestService
	public static var refreshTokenCount1 = 0
	public static var refreshTokenCount2 = 0
	public static var refreshTokenCount3 = 0
	public static var refreshTokenCount4 = 0
	public static var refreshTokenCount5 = 0
	public static var refreshTokenCount6 = 0
	public static var refreshTokenCount7 = 0
	public static var refreshTokenCount8 = 0
	public static var refreshTokenCount9 = 0
	public static var refreshTokenCount10 = 0
	public static var refreshTokenCount11 = 0
	public static var refreshTokenCount12 = 0
	public static var refreshTokenCount13 = 0
	
	init(graphQL: ApolloClientService = ApolloClientService(),service: RestService = RestService.shared) {
		self.graphQL = graphQL
		self.service = service
	}
	
	func getAllDataForLastThirtyDays() {
		
	}
	
	// Sleep Data
	func fetch(sleepDataBy range: DateRange) {
		if(Utility.makeAPICall(timeIntervalRange: range.intervalRange)){
			
			let query = QuerySleepDataByTimestampRangeQuery(from_ts: range.intervalRange.lowerBound, to_ts: range.intervalRange.upperBound)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let items = graphQLResult.data?.querySleepDataByTimestampRange?.items {
							for each in items where each != nil {
								CoreDataSleepStage.addSleepStage(timeInterval: each!.sleepTimestamp, value: each!.sleepStage)
							}
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount1 == 0 {
							CacheTechniqueWebService.refreshTokenCount1 = 1
							self.requestAccessToken { [weak self] response, error in
								if (response != nil) {
									self?.graphQL.setDefaultClient()
									self?.fetch(sleepDataBy: range)
								}
								if let error = error {
									Logger.shared.addLog(error.localizedDescription)
								}
							}
						}
					} else {
						
					}
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}else{
			
		}
	}
	
	// Daily Averages Data
	func fetchAverageData(by timestampRange: Range<Double>) {
		if(Utility.makeAPICall(timeIntervalRange: timestampRange)){
			
			let fromTs = Date(timeIntervalSince1970: timestampRange.lowerBound).dateToString(format: DateFormat.serverDateFormat
			)
			let toTs = Date(timeIntervalSince1970: timestampRange.upperBound).dateToString(format: DateFormat.serverDateFormat)
			let query = QueryDailyAveragesByTimestampRangeQuery(from_ts: fromTs, to_ts: toTs)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let item = graphQLResult.data?.queryDailyAveragesByTimestampRange?.items {
						for each in item where each != nil {
							if each!.sleepStartTime != nil {
								let avgPulseRate = convertStringToDictionary(text: each!.averagePulseRate ?? "")
								let dailyAvgPulseRateData = DailyAverageModel(average: (avgPulseRate?["average"] as? Double))
								
								let avgHRV = convertStringToDictionary(text: each!.averageHrv ?? "")
								let dailyAvgHRVData = DailyAverageModel(average: (avgHRV?["average"] as? Double))
								
								let avgOxygenRate = convertStringToDictionary(text: each!.averageSpO2 ?? "")
								let dailyAvgOxygenData = DailyAverageModel(average: (avgOxygenRate?["average"] as? Double))
								
								let avgBreathingRate = convertStringToDictionary(text: each!.averageBreathingRate ?? "")
								let dailyAvgBreathingRateData = DailyAverageModel(average: (avgBreathingRate?["average"] as? Double))
								
								let avgSystolicRate = convertStringToDictionary(text: each!.averageSystolic ?? "")
								let dailyAvgSystolicData = DailyAverageModel(average: (avgSystolicRate?["average"] as? Double))
								
								let avgDiastolicRate = convertStringToDictionary(text: each!.averageDiastolic ?? "")
								let dailyAvgDiastolicData = DailyAverageModel(average: (avgDiastolicRate?["average"] as? Double))
								
								let avgSkinTempRate = convertStringToDictionary(text: each!.averageTemperature ?? "")
								let dailyAvgTempData = DailyAverageModel(average: (avgSkinTempRate?["average"] as? Double))
								
								let totalStepData = convertStringToDictionary(text: each!.totalStepCount ?? "")
								let dailyTotalStepData = DailyAverageModel(average: (totalStepData?["total"] as? Double))
								
								let totalCaloriesData = convertStringToDictionary(text: each!.totalCaloriesBurnt ?? "")
								let dailyTotalCaloriesData = DailyAverageModel(average: (totalCaloriesData?["total"] as? Double))
								
								CoreDataSleepTime.addSleepTime(date: each!.dailyAveragesDate.toDate(format: DateFormat.serverDateFormat) ?? Date(), sleepStartTime: each!.sleepStartTime!, sleepEndTime: each!.sleepEndTime!, deepValue: each!.sleepData![0]!, lightValue: each!.sleepData![1]!, remValue: each!.sleepData![2]!, awakeValue: each!.sleepData![3]!, avgHR: dailyAvgPulseRateData.average ?? 0.0, avgHRV: dailyAvgHRVData.average ?? 0.0, avgOxygen: dailyAvgOxygenData.average ?? 0.0, avgBreathingRate: dailyAvgBreathingRateData.average ?? 0.0, avgSystolicBP: dailyAvgSystolicData.average ?? 0.0, avgDiastolicBP: dailyAvgDiastolicData.average ?? 0.0, avgSkinTempVar: dailyAvgTempData.average ?? 0.0, totalStepCount: Int(dailyTotalStepData.average ?? 0), totalCaloriesBurnt: dailyTotalCaloriesData.average ?? 0.0, healthScore: each?.healthScore ?? 0.0)
								
							} else {
								let avgPulseRate = convertStringToDictionary(text: each!.averagePulseRate ?? "")
								let dailyAvgPulseRateData = DailyAverageModel(average: (avgPulseRate?["average"] as? Double))
								
								let avgHRV = convertStringToDictionary(text: each!.averageHrv ?? "")
								let dailyAvgHRVData = DailyAverageModel(average: (avgHRV?["average"] as? Double))
								
								let avgOxygenRate = convertStringToDictionary(text: each!.averageSpO2 ?? "")
								let dailyAvgOxygenData = DailyAverageModel(average: (avgOxygenRate?["average"] as? Double))
								
								let avgBreathingRate = convertStringToDictionary(text: each!.averageBreathingRate ?? "")
								let dailyAvgBreathingRateData = DailyAverageModel(average: (avgBreathingRate?["average"] as? Double))
								
								let avgSystolicRate = convertStringToDictionary(text: each!.averageSystolic ?? "")
								let dailyAvgSystolicData = DailyAverageModel(average: (avgSystolicRate?["average"] as? Double))
								
								let avgDiastolicRate = convertStringToDictionary(text: each!.averageDiastolic ?? "")
								let dailyAvgDiastolicData = DailyAverageModel(average: (avgDiastolicRate?["average"] as? Double))
								
								let avgSkinTempRate = convertStringToDictionary(text: each!.averageTemperature ?? "")
								let dailyAvgTempData = DailyAverageModel(average: (avgSkinTempRate?["average"] as? Double))
								
								let totalStepData = convertStringToDictionary(text: each!.totalStepCount ?? "")
								let dailyTotalStepData = DailyAverageModel(average: (totalStepData?["total"] as? Double))
								
								let totalCaloriesData = convertStringToDictionary(text: each!.totalCaloriesBurnt ?? "")
								let dailyTotalCaloriesData = DailyAverageModel(average: (totalCaloriesData?["total"] as? Double))
								
								CoreDataSleepTime.addSleepTime(date: each!.dailyAveragesDate.toDate(format: DateFormat.serverDateFormat) ?? Date(), sleepStartTime: "0", sleepEndTime: "0", deepValue: 0, lightValue: 0, remValue: 0, awakeValue: 0, avgHR: dailyAvgPulseRateData.average ?? 0.0, avgHRV: dailyAvgHRVData.average ?? 0.0, avgOxygen: dailyAvgOxygenData.average ?? 0.0, avgBreathingRate: dailyAvgBreathingRateData.average ?? 0.0, avgSystolicBP: dailyAvgSystolicData.average ?? 0.0, avgDiastolicBP: dailyAvgDiastolicData.average ?? 0.0, avgSkinTempVar: dailyAvgTempData.average ?? 0.0, totalStepCount: Int(dailyTotalStepData.average ?? 0), totalCaloriesBurnt: dailyTotalCaloriesData.average ?? 0.0, healthScore: each?.healthScore ?? 0.0)
							}
						}
						
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount2 == 0 {
							CacheTechniqueWebService.refreshTokenCount2 = 1
						}
						
					} else {
						
					}
					
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}else{
			
		}
	}
	
	func fetch(dailyAveragesBy timestampRange: Range<Double>) {
		if(Utility.makeAPICall(timeIntervalRange: timestampRange)){
			let userIdInput = TableStringFilterInput(eq: KeyChain.shared.userId)
			let dateRangeInput = TableFloatFilterInput(between: [timestampRange.lowerBound, timestampRange.upperBound])
			
			let filter = TableDailyAveragesFilterInput(userId: userIdInput)
			let query = ListDailyAveragesQuery(filter: filter, limit: nil)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let item = graphQLResult.data?.listDailyAverages?.items {
						for each in item where each != nil {
							CoreDataSleepTime.addSleepTime(date: each!.dailyAveragesDate.toDate(format: "yyyy-MM-dd'T'HHmmss") ?? Date(), sleepStartTime: each!.sleepStartTime!, sleepEndTime: each!.sleepEndTime!, deepValue: each!.sleepData![0]!, lightValue: each!.sleepData![1]!, remValue: each!.sleepData![2]!, awakeValue: each!.sleepData![3]!, avgHR: each!.averagePulseRate?.toDouble ?? 0, avgHRV: each!.averageHrv?.toDouble ?? 0, avgOxygen: each!.averageSpO2?.toDouble ?? 0, avgBreathingRate: each!.averageBreathingRate?.toDouble ?? 0, avgSystolicBP: each!.averageSystolic?.toDouble ?? 0, avgDiastolicBP: each!.averageSystolic?.toDouble ?? 0, avgSkinTempVar: each!.averageTemperature?.toDouble ?? 0, totalStepCount: Int(each!.totalStepCount?.toDouble ?? 0), totalCaloriesBurnt: each!.totalCaloriesBurnt?.toDouble ?? 0.0, healthScore: each?.healthScore ?? 0.0)
						}
						
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount3 == 0 {
							CacheTechniqueWebService.refreshTokenCount3 = 1
							self.requestAccessToken { [weak self] response, error in
								if (response != nil) {
									self?.graphQL.setDefaultClient()
									self?.fetchAverageData(by: timestampRange)
								}
								if let error = error {
									Logger.shared.addLog(error.localizedDescription)
								}
							}
						}
					} else {
						
					}
					
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}
	}
	
	// HRV Data
	func fetch(hrvBy timestampRange: Range<Double>) {
		if(Utility.makeAPICall(timeIntervalRange:timestampRange)){
			let query = QueryHeartRateVariabilitiesByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let items = graphQLResult.data?.queryHeartRateVariabilitiesByTimestampRange?.items {
						for each in items where each != nil {
							CoreDataHRV.add(timeInterval: each!.hrvTimestamp, value: each!.hrvValue)
						}
						
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount4 == 0 {
						CacheTechniqueWebService.refreshTokenCount4 = 1
						self.requestAccessToken { [weak self] response, error in
							if (response != nil) {
								self?.graphQL.setDefaultClient()
								self?.fetch(hrvBy: timestampRange)
							}
							if let error = error {
								Logger.shared.addLog(error.localizedDescription)
							}
						}
					}
					} else {
						
					}
					
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}else{
			
		}
	}
	
	// Oxygen Data
	func fetch(oxygenBy timestampRange: Range<Double>) {
		if(Utility.makeAPICall(timeIntervalRange: timestampRange)){
			
			let query = QuerySpO2ByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let item = graphQLResult.data?.querySpO2ByTimestampRange?.items {
						for each in item where each != nil {
							CoreDataOxygen.add(timeInterval: each!.spO2Timestamp, value: each!.spO2Value)
						}
						
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount5 == 0 {
							CacheTechniqueWebService.refreshTokenCount5 = 1
							self.requestAccessToken { [weak self] response, error in
								if (response != nil) {
									self?.graphQL.setDefaultClient()
									self?.fetch(oxygenBy: timestampRange)
								}
								if let error = error {
									//									Logger.shared.addLog(error.localizedDescription)
								}
							}
						}
					} else {
						
					}
					
					//									Logger.shared.addLog(error.localizedDescription)
				}
			}
		}
	}
	
	// Take A Pause Data
	func fetch(takeAPauseDataBy timestampRange: Range<Double>) {
		if(Utility.makeAPICall(timeIntervalRange: timestampRange)){
			
			let userIdInput = TableStringFilterInput(eq: KeyChain.shared.userId)
			let dateRangeInput = TableFloatFilterInput(between: [timestampRange.lowerBound, timestampRange.upperBound])
			
			let filter = TablePauseDataFilterInput(userId: userIdInput, endTime: dateRangeInput)
			let query = ListPauseDataQuery(filter: filter, limit: nil)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let item = graphQLResult.data?.listPauseData?.items {
						for each in item where each != nil {
							CoreDataPauseReading.add(startTime: each!.startTime, endTime: each!.endTime, systolic: each!.averageSystolic, diastolic: each!.averageDiastolic, skinTemperature: each!.averageSkinTemprature, pulseRate: each!.averagePulseRate, breathingRate: each!.averageBreathingRate)
						}
						
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount6 == 0 {
							CacheTechniqueWebService.refreshTokenCount6 = 1
							self.requestAccessToken { [weak self] response, error in
								if (response != nil) {
									self?.graphQL.setDefaultClient()
									self?.fetch(takeAPauseDataBy: timestampRange)
								}
								if let error = error {
									Logger.shared.addLog(error.localizedDescription)
								}
							}
						}
					} else {
						
					}
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}
	}
	
	// Exercise Data
	func fetch(exerciseDataBy timestampRange: Range<Double>) {
		if(Utility.makeAPICall(timeIntervalRange: timestampRange)){
			let userIdInput = TableStringFilterInput(eq: KeyChain.shared.userId)
			let dateRangeInput = TableFloatFilterInput(between: [timestampRange.lowerBound, timestampRange.upperBound])
			
			let filter = TableExerciseDataFilterInput(userId: userIdInput, endTime: dateRangeInput)
			let query = ListExerciseDataQuery(filter: filter, limit: nil)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let item = graphQLResult.data?.listExerciseData?.items {
						for each in item where each != nil {
							CoreDataExercise.add(startTime: each!.startTime, endTime: each!.endTime, systolic: each!.averageSystolic, diastolic: each!.averageDiastolic, breathingRate: each!.averageBreathingRate, skinTemperature: each!.averageSkinTemprature, pulseRate: each!.averagePulseRate, caloriesBurnt: each!.totalCaloriesBurnt, stepCount: each!.totalStepCount, distanceCovered: each!.totalDistanceCovered, oxygen: each!.averageSpo2)
						}
						
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount7 == 0 {
							CacheTechniqueWebService.refreshTokenCount7 = 1
							self.requestAccessToken { [weak self] response, error in
								if (response != nil) {
									self?.graphQL.setDefaultClient()
									self?.fetch(exerciseDataBy: timestampRange)
								}
								if let error = error {
									Logger.shared.addLog(error.localizedDescription)
								}
							}
						}
					} else {
						
					}
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}
	}
	
	// Breathing Rate Data
	func fetch(breathingRateDataBy timestampRange: Range<Double>) {
		if(Utility.makeAPICall(timeIntervalRange: timestampRange)){
			let query = QueryBreathingRateByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let items = graphQLResult.data?.queryBreathingRateByTimestampRange?.items {
						for each in items where each != nil {
							CoreDataBreathingRate.add(timeInterval: each!.breathingRateTimestamp, value: each!.breathingRateValue)
						}
						
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount8 == 0 {
							CacheTechniqueWebService.refreshTokenCount8 = 1
							self.requestAccessToken { [weak self] response, error in
								if (response != nil) {
									self?.graphQL.setDefaultClient()
									self?.fetch(breathingRateDataBy: timestampRange)
								}
								if let error = error {
									Logger.shared.addLog(error.localizedDescription)
								}
							}
						}
					} else {
						
					}
					
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}else{
			
		}
	}
	
	// Pulse Rate Data
	func fetch(pulseRateDataBy timestampRange: Range<Double>) {
		if(Utility.makeAPICall(timeIntervalRange: timestampRange)){
			
			let query = QueryPulseRateByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let items = graphQLResult.data?.queryPulseRateByTimestampRange?.items {
						for each in items where each != nil {
							CoreDataPulseRate.add(timeInterval: each!.pulseRateTimestamp, value: each!.pulseRateValue)
						}
						
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount9 == 0 {
							CacheTechniqueWebService.refreshTokenCount9 = 1
							self.requestAccessToken { [weak self] response, error in
								if (response != nil) {
									self?.graphQL.setDefaultClient()
									self?.fetch(pulseRateDataBy: timestampRange)
								}
								if let error = error {
									Logger.shared.addLog(error.localizedDescription)
								}
							}
						}
					} else {
						
					}
					
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}else{
			
		}
	}
	
	// Step Count Data
	func fetch(stepCountBy timestampRange: Range<Double>, completionHandler: @escaping (Bool?) -> Void) {
		if(Utility.makeAPICall(timeIntervalRange: timestampRange)){
			let query = QueryStepCountByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let items = graphQLResult.data?.queryStepCountByTimestampRange?.items {
						
						for each in items where each != nil {
							CoreDataStepCount.add(timeInterval: each!.stepCountTimestamp, value: each!.stepCountValue, calories: each!.caloriesBurnt, distance: each!.distanceCovered)
						}
						completionHandler(true)
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount10 == 0 {
							CacheTechniqueWebService.refreshTokenCount10 = 1
							self.requestAccessToken { [weak self] response, error in
								if (response != nil) {
									self?.graphQL.setDefaultClient()
									self?.fetch(stepCountBy: timestampRange, completionHandler: completionHandler)
								}
								if let error = error {
									Logger.shared.addLog(error.localizedDescription)
								}
							}
						}
					} else {
						
					}
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}else{
			
		}
	}
	
	// Skin Temp Data
	func fetch(skinTempDataBy timestampRange: Range<Double>) {
		if(Utility.makeAPICall(timeIntervalRange: timestampRange)){
			
			let query = QuerySkinTemperatureByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let items = graphQLResult.data?.querySkinTemperatureByTimestampRange?.items {
						for each in items where each != nil {
							CoreDataSkinTemperature.add(timeInterval: each!.temperatureTimestamp, value: each!.temperatureValue)
						}
						
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount11 == 0 {
							CacheTechniqueWebService.refreshTokenCount11 = 1
							self.requestAccessToken { [weak self] response, error in
								if (response != nil) {
									self?.graphQL.setDefaultClient()
									self?.fetch(skinTempDataBy: timestampRange)
								}
								if let error = error {
									Logger.shared.addLog(error.localizedDescription)
								}
							}
						}
					} else {
						
					}
					
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}else{
			
		}
	}
	
	// Blood Pressure Data
	func listBloodPressures(range: Range<TimeInterval>) {
		if(Utility.makeAPICall(timeIntervalRange: range)){
			
			let query = QueryBloodPressureByTimestampRangeQuery(from_ts: range.lowerBound, to_ts: range.upperBound)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let item = graphQLResult.data?.queryBloodPressureByTimestampRange?.items {
						for each in item where each != nil {
							CoreDataBPReading.addBPReading(timeInterval: each!.dataTimestamp, systolic: each!.systolic!, diastolic: each!.diastolic!, pulseRate: each!.pulseRate!)
						}
						
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount12 == 0 {
							CacheTechniqueWebService.refreshTokenCount12 = 1
							self.requestAccessToken { [weak self] response, error in
								if (response != nil) {
									self?.graphQL.setDefaultClient()
									self?.listBloodPressures(range: range)
								}
								if let error = error {
									Logger.shared.addLog(error.localizedDescription)
								}
							}
						}
					} else {
						
					}
					
					Logger.shared.addLog(error.localizedDescription)
				}
			}
			
		}else{
			
		}
	}
	
	// Activity Intensity Data
	func fetchActivtyIntensity(by timestampRange: Range<Double>){
		if(Utility.makeAPICall(timeIntervalRange:timestampRange)){
			let query = QueryActivityIntensityByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let items = graphQLResult.data?.queryActivityIntensityByTimestampRange?.items {
						for each in items where each != nil {
							CoreDataActivity.add(timeInterval: each!.dataTimestamp, value: Double(each!.value))
						}
						
					} else {
						
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						if CacheTechniqueWebService.refreshTokenCount13 == 0 {
							CacheTechniqueWebService.refreshTokenCount13 = 1
							self.requestAccessToken { [weak self] response, error in
								if (response != nil) {
									self?.graphQL.setDefaultClient()
									self?.fetchActivtyIntensity(by: timestampRange)
								}
								if let error = error {
									Logger.shared.addLog(error.localizedDescription)
								}
							}
						}
					} else {
						
					}
					
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}else{
			
		}
	}
	
	func fetchIntensityPulseData(weekStartdate: String, weekEndDate: String) {
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.fetchIntensity)!
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "Authorization", value: KeyChain.shared.idToken)
		header.addHeader(key: "AccessToken", value: KeyChain.shared.accessToken!)
		
		let parameters: [String: Any] = ["week_start": weekStartdate,
										 "week_end": weekEndDate]
		let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters)
		
		service.request(apiType: APIType.FETCH_INTENSITY_DATA, request: request) { (result) in
			switch result {
			case .success(let response):
				do {
					let responseModel = try JSONDecoder().decode([ActivityIntensityModel].self, from: response.rawData())
					for x in 0..<responseModel.count {
						let timeInterval = responseModel[x].date.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970
						CoreDataActivityIntensity.add(timeInterval: timeInterval ?? 0, avgActivityIntensity: responseModel[x].avg_activity_intensity ?? 0, avgPulseRate: responseModel[x].avg_pulse_rate ?? 0)
					}
					let newResponselModel = CoreDataActivityIntensity.fetch(for: Date(timeIntervalSince1970: weekStartdate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0) ..< Date(timeIntervalSince1970: weekEndDate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0))
				} catch let error {
					print("\(error)")
					let newResponselModel = CoreDataActivityIntensity.fetch(for: Date(timeIntervalSince1970: weekStartdate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0) ..< Date(timeIntervalSince1970: weekEndDate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0))
				}
			case .failure(let error):
				let newResponselModel = CoreDataActivityIntensity.fetch(for: Date(timeIntervalSince1970: weekStartdate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0) ..< Date(timeIntervalSince1970: weekEndDate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0))
			}
		}
	}
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
			case .success(let response):
				
				let userSession = UserSession(json: response["data"])
				KeyChain.shared.saveAccessToken(response: userSession)
				
				if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
					completionHandler(responseModel, nil)
				} else {
					completionHandler(nil, MovanoError.invalidResponseModel)
				}
			case .failure(let error):
				completionHandler(nil, error)
			}
		}
	}
}
