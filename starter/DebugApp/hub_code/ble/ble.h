//
//  ble.h
//
//  Created by Phil Kelly on 2/5/20.
//  Copyright © 2020 Movano. All rights reserved.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes string function may be unsafe warning on Windows

#ifndef ble_h
#define ble_h

#include <stdbool.h>

#if ! defined (WIN32) && ! defined (WIN64)
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#endif

typedef void *HANDLE;

#ifdef __cplusplus
extern "C" {
#endif

typedef enum SaveData
{
    BLE_SAVE_DATA_START_SESSION,
    BLE_SAVE_DATA_CONTINUE_SESSION,
} SaveData;

typedef struct SaveSensorData
{
    int rv;                                     // return value (0 = success)
    SaveData eState;                            // SaveData enum
    unsigned long ulChunkCount;                 // Current chunk number
    unsigned long ulFrameCount;                 // Current frame number
    unsigned long ulPackageCount;               // Current package number
    char *pcPackageBuffer;                      // Memory allocated for 1 package (one or more frames)
    unsigned long ulFrameBufferSize;            // Number of bytes allocated to 1 frame
    unsigned long ulPackageBufferSize;          // Number of bytes allocated for 1 package
    unsigned long ulPackageBufferLength;        // Number of bytes used in 1 frame
} SaveSensorData;

extern struct SaveSensorData gSaveSensorData;

#pragma mark BLE Parameters

    //
    // BLE Parameters
    //
    typedef struct BleParams
    {
        HANDLE bleHandle;
    } BleParams;


#pragma mark BLE Prototypes

    void blePError(int iDisplay, int rv, const char* pcMessage, const char* pcFileName, int iLineNumber);
    int bleSetup(HANDLE* pHandle, BleParams* psBlePrams);

    void bleSetupSensorData(void);
    void bleCloseSensorData(void);
    int bleSaveSensorData(unsigned char *pcPtr, unsigned long ulNumberOfBytes);

#ifdef __cplusplus
}
#endif

#endif /* ble_h */
