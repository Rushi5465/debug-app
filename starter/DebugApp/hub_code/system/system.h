//
//  system.h
//
//  Created by Phil Kelly on 3/24/20.
//  Copyright © 2020 Movano. All rights reserved.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes string function may be unsafe warning on Windows

#ifndef system_h
#define system_h

#include <stdio.h>
//#include <climits>      // INT_MAX

#pragma mark System Parameters

#define SYSTEM_HOME_DIR_STRLEN 1024                 // Session directory path length
#define SYSTEM_SESSIONS_LIST_STRLEN 1024            // Length of system Test strings
#define SYSTEM_KUSE_STRLEN 64                       // Length of Kuse string (a bracketed list)
#define SYSTEM_X_USER_ID_STRLEN 1024                // Length of user ID string
#define SYSTEM_X_SENS_LOC_STRLEN 1024               // Length of sensor location string

//
// System Parameters
//

typedef struct SystemParams
{
    // Hub software.
    char acHubSoftwareVersion[32];              // Hub software version.
    int iDebugLevel;                            // Debug level.
 
    // Host application parameters.
    char acHomeDir[SYSTEM_HOME_DIR_STRLEN];     // Session directory.
    int iNumberOfFramesPerPackage;              // P, Number of frames per package (frames per acquisition file).
    char acSessionsList[SYSTEM_SESSIONS_LIST_STRLEN];     // Vector of collection tests.
//    long lFramesPerBlock = INT_MAX;           // Number of frames to process at a time.

    // Sensor related parameters.
    double dSensorStartUpTime;                  // Time allowed for control plane communications to startup.
    char acKuse[SYSTEM_KUSE_STRLEN];            // Antennas specified by user that are in use. ("[1 2 3 4]")

    // User parameters.
    int iUserStartEachSession;                  // Wait for user keyboard input before starting each session.
    int iPauseBeforeSession;                    // Number of times to pause before actually starting each session.
    int iPauseAfterSession;                     // Number of times to pause after completing a session.
    int iShowUserPackageProgress;               // Print statements every N packages.
    int iRunDataContinuously;                   // Ask for data even if the user has paused collection.
    
    // Health/medical parameters.
    char acUserID[SYSTEM_X_USER_ID_STRLEN];             // User ID
    char acSensorLocation[SYSTEM_X_SENS_LOC_STRLEN];    // Sensor location
} SystemParams;

typedef enum
{
    SYSTEM_HOME_DIRECTORY               = 1,
    SYSTEM_FRAMES_PER_BLOCK             = 1 << 1,
    SYSTEM_KUSE                         = 1 << 2,
    SYSTEM_FRAMES_PER_PACKAGE           = 1 << 3,
    SYSTEM_SWID                         = 1 << 4,
    SYSTEM_DEBUG_LEVEL                  = 1 << 5,
    SYSTEM_SESSIONS_LIST                = 1 << 6,
    SYSTEM_USER_KEYBOARD_INPUT          = 1 << 7,
    SYSTEM_PAUSE_BEFORE_COLLECT         = 1 << 8,
    SYSTEM_PAUSE_AFTER_COLLECT          = 1 << 9,
    SYSTEM_SHOW_USER_PACKAGE_PROGRESS   = 1 << 10,
    SYSTEM_RUN_DATA_CONTINUOUSLY        = 1 << 11,
    SYSTEM_SENSOR_STARTUP_TIME          = 1 << 12,
    SYSTEM_X_USER_ID                    = 1 << 13,
    SYSTEM_X_SENS_LOC                   = 1 << 14,
    SYSTEM_ALL                          = 0xFFFFFFFF,
    SYSTEM_SAVE_TO_FILE                 = SYSTEM_ALL ^ SYSTEM_HOME_DIRECTORY
                                                     ^ SYSTEM_DEBUG_LEVEL
                                                     ^ SYSTEM_SESSIONS_LIST
                                                     ^ SYSTEM_USER_KEYBOARD_INPUT
                                                     ^ SYSTEM_PAUSE_BEFORE_COLLECT
                                                     ^ SYSTEM_PAUSE_AFTER_COLLECT
                                                     ^ SYSTEM_SHOW_USER_PACKAGE_PROGRESS
                                                     ^ SYSTEM_SENSOR_STARTUP_TIME

} SystemID;


#ifdef __cplusplus
extern "C" {
#endif

#pragma mark System Prototypes

    void systemInit(SystemParams* psSystemParams);
    void systemReportParams(SystemParams* psSystemParams,
        const char* pcTitleString,          // = NULL,
        unsigned int uiSystemPrintOptions,  // = SYSTEM_ALL,
        int iNumberOfIdentSpaces,           // = 2,
        FILE* stream,                       // = NULL,
        const char* pcEquals,               // = " = ",
        const char* pcLeft,                 // = "(",
        const char* pcRight                 // = ")"
    );
    void systemReportParams2(SystemParams* psSystemParams,
        const char* pcTitleString
    );
    void systemReportParams3(SystemParams* psSystemParams,
        const char* pcTitleString,
        unsigned int uiSystemPrintOptions
    );
    int systemSetParameters(SystemParams* psSystemParams, const char* name, const char* value, unsigned int* puiSystemPrintOptions);

#ifdef __cplusplus
}
#endif

#endif /* system_h */
