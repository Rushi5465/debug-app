//
//  hash.h
//
//  Created by Phil Kelly on 11/3/19.
//  Copyright © 2019 Movano. All rights reserved.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes string function may be unsafe warning on Windows

#ifndef hash_h
#define hash_h


/**
 * List is a node in the hash table
 *
 * @key: The key string of a pair
 * The key is unique in the HashTable
 *
 * @value: The value corresponding to a key
 * A value is not unique. It can correspond to several keys
 *
 * @next: A pointer to the next node of the List
 */
typedef struct List
{
        char *key;
        char *value;
        struct List *next;
} List;

/**
 * HashTable is the hash table
 *
 * @size: The size of the array
 *
 * @array: An array of size @size
 * Each cell of this array is a pointer to the first node of a linked list,
 * because we want our HashTable to use a Chaining collision handling
 */
typedef struct HashTable
{
    unsigned int size;
    List **array;
} HashTable;

#ifdef __cplusplus
extern "C" {
#endif

/*
 * @key: The key to hash
 *
 * @size: The size of the hashtable
 *
 * @return: An integer N like 0 <= N < @size
 * This integer represents the index of @key in an array of size @size
 */
unsigned int hashGetIndex(const char *key, unsigned int size);

HashTable *hashCreateDict(unsigned int);
int hashPutNode(HashTable *, const char *, const char *);
void hashNodeHandler(HashTable *, List *);
char *hashGetValue(HashTable *, const char *);
void hashFreeDict(HashTable *);


#ifdef __cplusplus
}
#endif

#endif /* hash_h */
