//
//  path.h
//
//  Created by Phil Kelly on 11/3/19.
//  Copyright © 2019 Movano. All rights reserved.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes string function may be unsafe warning on Windows

#ifndef path_h
#define path_h

#ifdef __cplusplus
extern "C" {
#endif

	int pathStripPath(char* pcString);
	int pathCombinePaths(char* pcDestination, char* pcPath1, const char* pcPath2);

#ifdef __cplusplus
}
#endif

#endif /* path */
