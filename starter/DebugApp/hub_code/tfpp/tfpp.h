//
//  tfpp.h
//
//  Created by Phil Kelly on 11/3/19.
//  Copyright © 2019 Movano. All rights reserved.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes string function may be unsafe warning on Windows

#ifndef tfpp_h
#define tfpp_h

#ifdef __cplusplus
extern "C" {
#endif

	int     tfppParse(const char* pcFName, char** ppcString, int* piArgc, char*** ppArgv);
	int     tfppGetBoolean(const char* pcValue);
	long    tfppGetLong(const char* pcValue);
	int     tfppGetInt(const char* pcValue);
	double  tfppGetDouble(const char* pcValue);
	float   tfppGetFloat(const char* pcValue);

#ifdef __cplusplus
}
#endif

#endif /* tfpp_h */
