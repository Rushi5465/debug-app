//
//  timer.hpp
//
//  Created by Phil Kelly on 12/9/20.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes string function may be unsafe warning on Windows

#ifndef timer_hpp
#define timer_hpp

#ifdef __cplusplus
extern "C" {
#endif

	double timerNowPlus(void);
	double timerDeltaPlus(double dStartTime);

#ifdef __cplusplus
}
#endif

#endif /* timer_hpp */
