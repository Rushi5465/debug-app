//
//  timer.h
//
//  Created by Phil Kelly on 12/9/20.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes string function may be unsafe warning on Windows

#ifndef timer_h
#define timer_h

#include <time.h>

#if defined(WIN32) || defined(WIN64)
typedef int clockid_t;
enum ClockID
{
	CLOCK_REALTIME,
	CLOCK_MONOTONIC,
};
#endif

#ifdef __cplusplus
extern "C" {
#endif

	void timerClock(clockid_t tClockToUse);
	double timerScale(void);

	double timerNow(void);
	double timerDelta(double dStartTime);

	double timerMonotonicNow(void);
	double timerMonotonicDelta(double dStartTime);

	double timerRealTimeNow(void);
	double timerRealTimeDelta(double dStartTime);

	void timerCalendar(char* pcStr, double dTime);
	void timerCalendarExtended(char* pcStr, double dTime);

#ifdef __cplusplus
}
#endif

#endif /* timer_h */
