//
//  ServiceGenericAccess.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
// UUIDs for the service and associated characteristics.
let BLE_GA_ServiceGenericAccess                     = CBUUID.init(string: "1800")    // Generic Access       org.bluetooth.BLE_Service.generic_access            0x1800    GSS
let BLE_GA_CharacteristicDeviceName                 = CBUUID.init(string: "2A00")    // Device Name          org.bluetooth.characteristic.gap.device_name        0x2A00    Core 4.0
let BLE_GA_CharacteristicDeviceAppearance           = CBUUID.init(string: "2A01")    // Device Appearance    org.bluetooth.characteristic.gap.appearance         0x2A01    Core 4.0


/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
class GA_Service : Service {
    let myServiceDirectoryName = "ga_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.

    override func didUpdate(characteristic: CBCharacteristic) {
        var ch = "Unknown"
        var str = "Unknown"
        if characteristic.uuid == BLE_GA_CharacteristicDeviceName {
            str = characteristic.gaDeviceName()
            self.manager!.bleString[BLE_GA_CharacteristicDeviceName] = str
            ch = "Device name"
        }
        os_log("didUpdateValueFor   %{public}s: %{public}s", log: OSLog.ble1, type:.info, ch, str)
//        diagnostics(str)
        
        self.manager!.bleString[BLE_GA_ServiceGenericAccess] = str
    }
}


extension BluetoothManager {
 
    public func initGenericAccess(_ manager: BluetoothManager) {
        let service = GA_Service()
        service.manager = manager
        soiDict[BLE_GA_ServiceGenericAccess] = service
        
        
        // Add to BLE strings.
        bleString[BLE_GA_CharacteristicDeviceName]              = "unknown"
        bleString[BLE_GA_ServiceGenericAccess]                  = "unknown"

        // Add to UUID strings
        uuidDictionary[BLE_GA_ServiceGenericAccess]             = "Generic Access (1800)"
        uuidDictionary[BLE_GA_CharacteristicDeviceName]         = "Device name (2A00)"
        uuidDictionary[BLE_GA_CharacteristicDeviceAppearance]   = "Device appearance (2A01)"

        uuidDictionary[BLE_FU_ServiceOTA]                       = "Over the air"
        uuidDictionary[BLE_FU_CharacteristicFWUpgrade]          = "Over the air firmware upgrades"
    }

}


extension CBCharacteristic {

    /**
     Device name
     
     Pertains to characteristic.uuid = "2A00".
     
     - Returns: "Name"
     */
    func gaDeviceName() -> String {
        let str = self.stringValue()
        return str
    }

    /**
     Device appearance
     
     Pertains to characteristic.uuid = "2A01".
     
     - Returns: "Appearance"
     */
    func gaDeviceAppearance() -> String {
        let str = self.stringValue()
        return str
    }
    
}
