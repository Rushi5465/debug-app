//
//  ServiceHallEffect.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
// UUIDs for the service and associated characteristics.
let BLE_HE_ServiceHallEffect                        = CBUUID.init(string: "F598DBC5-2F00-4EC5-9936-B3D1AA4F957F")   // Hall Effect (custom)
let BLE_HE_CharacteristicHallState                  = CBUUID.init(string: "F598DBC5-2F01-4EC5-9936-B3D1AA4F957F")   // Hall Effecct: State (custom)
let BLE_HE_CharacteristicHallFieldStrength          = CBUUID.init(string: "F598DBC5-2F02-4EC5-9936-B3D1AA4F957F")   // Hall Effecct: Field Strength (custom)
let BLE_HE_CharacteristicHallControlPoint           = CBUUID.init(string: "F598DBC5-2F03-4EC5-9936-B3D1AA4F957F")   // Hall Effecct: Control Point (custom)


/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
class HE_Service : Service {
    let myServiceDirectoryName = "he_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.

    override func didUpdate(characteristic: CBCharacteristic) {
        var ch = "Unknown"
        var str = "Unknown"
        if characteristic.uuid == BLE_HE_CharacteristicHallState {
            str = characteristic.heStateString()
            self.manager!.bleString[BLE_HE_CharacteristicHallState] = str
            ch = "Hall Effect State"
        } else if characteristic.uuid == BLE_HE_CharacteristicHallFieldStrength {
            str = characteristic.heFieldStrengthString()
            self.manager!.bleString[BLE_HE_CharacteristicHallFieldStrength] = str
            self.manager!.bleString[BLE_HE_ServiceHallEffect] = str
            self.manager!.bleData[BLE_HE_ServiceHallEffect] = characteristic.value
            ch = "Hall Effect Field Strength"
        }
        
        // Display the data
        chText = ch
        displayData(characteristic: characteristic)

        // Save the data.
        saveData(characteristic: characteristic, serviceDirName: myServiceDirectoryName, sessionDirName: mySessionDirectoryName, fCount: &myFileCount)
    }

    override func notifyStart(characteristic: CBCharacteristic) {
        notifyStart1(characteristic: characteristic, serviceDirName: myServiceDirectoryName, sessionDirName: &mySessionDirectoryName, fCount: &myFileCount, nCount: &myCharacteristicsNotifying)
    }

    override func notifyStop(characteristic: CBCharacteristic) {
        notifyStop1(characteristic: characteristic, sessionDirName: &mySessionDirectoryName, fCount: &myFileCount, nCount: &myCharacteristicsNotifying)
    }
    
}

extension BluetoothManager {

    public func initHallEffect(_ manager: BluetoothManager) {
        let service = HE_Service()
        service.manager = manager
        soiDict[BLE_HE_ServiceHallEffect] = service
        
        // Add to BLE strings.
        bleString[BLE_HE_CharacteristicHallState]               = "unknown"
        bleString[BLE_HE_CharacteristicHallFieldStrength]       = "0"
        bleString[BLE_HE_ServiceHallEffect]                     = bleString[BLE_HE_CharacteristicHallFieldStrength]
        
        // Add to UUID strings
        uuidDictionary[BLE_HE_ServiceHallEffect]                = "Hall Effect"
        uuidDictionary[BLE_HE_CharacteristicHallState]          = "Hall Effect state"
        uuidDictionary[BLE_HE_CharacteristicHallFieldStrength]  = "Hall Effect field strength"
        uuidDictionary[BLE_HE_CharacteristicHallControlPoint]   = "Hall Effect control point"
    }
    
}

extension CBCharacteristic {

    /**
     Hall effect field state.
     
     Pertains to  characteristic.uuid = "F598DBC5-2F01-4EC5-9936-B3D1AA4F957F".

     The state is
     - 0 Closed
     - 1 Open
     - 2 Tamper

     - Returns: The state number. (nil is returned if there was a problem reading the state.)
     */
    func heState() -> UInt8? {
        guard self.value != nil else { return nil }
        guard self.value!.count >= 1 else { return nil }
        return UInt8(self.value![0])
    }
    
    /**
     Hall effect field state as a string.
     
     Pertains to  characteristic.uuid = "F598DBC5-2F01-4EC5-9936-B3D1AA4F957F".

     The return string is
     - "Closed"
     - "Open"
     - "Tamper"

     - Parameter characteristic: A characteristic representing acceleration.
     - Returns: String with the Hall field state. (The string is empty if there was a problem reading the state.)
     */
    func heStateString() -> String {
        guard self.value != nil else { return "" }
        guard self.value!.count >= 1 else { return "" }
        switch self.value![0] {
        case 0:
            return "Closed"
        case 1:
            return "Open"
        case 2:
            return "Tamper"
        default:
            return ""
        }
    }
    
    /**
     Hall effect field strength.
     
     Pertains to characteristic.uuid = "F598DBC5-2F02-4EC5-9936-B3D1AA4F957F")

     - Returns: Field strength.
     */
    func heFieldStrength() -> Int32? {
        guard self.value != nil else { return nil }
        guard self.value!.count >= 1 else { return nil }
        return self.int32Value()
    }
    
    /**
     Hall effect field strength as a string.
     
     Pertains to characteristic.uuid = "F598DBC5-2F02-4EC5-9936-B3D1AA4F957F")

     - Returns: Field strength.
     */
    func heFieldStrengthString() -> String {
        guard self.value != nil else { return "" }
        guard self.value!.count >= 1 else { return "" }
        return "\(self.int32Value()!)"
    }
    
}
