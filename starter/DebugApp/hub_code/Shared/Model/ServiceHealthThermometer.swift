//
//  ServiceHealthThermometer.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
// UUIDs for the service and associated characteristics.
let BLE_HT_ServiceHealthThermometer                 = CBUUID.init(string: "1809")          // Health Thermometer (HT)   org.bluetooth.BLE_Service.health_thermometer        0x1809    GSS
let BLE_HT_CharacteristicTemperature                = CBUUID.init(string: "2A1C")
let BLE_HT_CharacteristicType                       = CBUUID.init(string: "2A1D")
let BLE_HT_CharacteristicIntermediate               = CBUUID.init(string: "2A1E")
let BLE_HT_CharacteristicInterval                   = CBUUID.init(string: "2A21")


/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
class HT_Service : Service {
    let myServiceDirectoryName = "ht_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.

    override func didUpdate(characteristic: CBCharacteristic) {
        var str = ""
        switch characteristic.uuid {
        case BLE_HT_CharacteristicTemperature, BLE_HT_CharacteristicIntermediate:
            var temperature: Float
            var celcius: Bool
//            var date: DateComponents
            var type: String
//            var timeStampPresent: Bool
            var typePresent: Bool
            
//            (temperature, celcius, date, type, timeStampPresent, typePresent) = characteristic.htTemperature()
            (temperature, celcius, _, type, _, typePresent) = characteristic.htTemperature()
            if celcius {
                temperature = 9/5 * temperature + 32.0
            }
            self.manager!.bleString[BLE_HT_CharacteristicTemperature] = String(format: "%.1f", temperature)
            if typePresent {
                self.manager!.bleString[BLE_HT_CharacteristicType] = type
            }
            str = htTemperature()
            
            self.manager!.bleData[BLE_HT_CharacteristicTemperature] = characteristic.value
            // Save the data.
            let data = self.manager!.bleData[BLE_HT_CharacteristicTemperature]!
            self.manager!.bleData[BLE_HT_ServiceHealthThermometer] = data
            saveData(characteristic: characteristic, serviceDirName: myServiceDirectoryName, sessionDirName: mySessionDirectoryName, fCount: &myFileCount)
            
        case BLE_HT_CharacteristicType:
            self.manager!.bleString[BLE_HT_CharacteristicType] = characteristic.htTypeString()
            str = htTemperature()
        case BLE_HT_CharacteristicInterval:
            str = "Unknown"
        default:
            return
        }
 
        self.manager!.bleString[BLE_HT_ServiceHealthThermometer] = str
    }
    
    override func notifyStart(characteristic: CBCharacteristic) {
        // Override this function to insert myServiceDirectoryName. 
        notifyStart1(characteristic: characteristic, serviceDirName: myServiceDirectoryName, sessionDirName: &mySessionDirectoryName, fCount: &myFileCount, nCount: &myCharacteristicsNotifying)
    }

    func htTemperature() -> String {
        return "\(self.manager!.bleString[BLE_HT_CharacteristicTemperature]!)"
    }

}


extension BluetoothManager {

    public func initHealthThermometer(_ manager: BluetoothManager) {
        let service = HT_Service()
        service.manager = manager
        soiDict[BLE_HT_ServiceHealthThermometer] = service
        
        // Add to BLE strings.
        bleString[BLE_HT_CharacteristicTemperature]         = "0 °F"
        bleString[BLE_HT_CharacteristicType]                = "unknown"
        bleString[BLE_HT_CharacteristicIntermediate]        = "unknown"
        bleString[BLE_HT_CharacteristicInterval]            = "unknown"
        bleString[BLE_HT_ServiceHealthThermometer]          = "0 °F\nunknown" //htTemperature()
        
        // Add to UUID strings
        uuidDictionary[BLE_HT_ServiceHealthThermometer]     = "Health Thermometer (1809)"
        uuidDictionary[BLE_HT_CharacteristicTemperature]    = "Temperature (2A1C)"
        uuidDictionary[BLE_HT_CharacteristicType]           = "Type (2A1D)"
        uuidDictionary[BLE_HT_CharacteristicIntermediate]   = "Intermediate (2A1E)"
        uuidDictionary[BLE_HT_CharacteristicInterval]       = "Interval (2A21)"
    }

}

/**
 Report the body location of a temperature measurement.
 
 Body locations:
    0 -> Other
    1 -> Armpit
    2 -> Body                (Body - general)
    3 -> Ear
    4 -> Finger
    5 -> GI Tract            (Gastro-intenstinal tract)
    6 -> Mouth
    7 -> Rectum
    8 -> Toe
    9 -> Ear                    (Tympanum - ear drum)
    ? -> Unknown          (Reserved for future use)

 - Parameter location: An integer indicating the body location.
 - Returns: The body location as a string.
 */
public func bodyLocation(location: Int) -> String
{

    switch location {
        case 0: return "Other"
        case 1: return "Armpit"
        case 2: return "Body"           // "Body - general"
        case 3: return "Ear"
        case 4: return "Finger"
        case 5: return "GI Tract"       // "Gastro-intenstinal tract"
        case 6: return "Mouth"
        case 7: return "Rectum"
        case 8: return "Toe"
        case 9: return "Ear"            // "Tympanum - ear drum"
        default:
          return "Unknown"              // "Reserved for future use"
    }
}

extension CBCharacteristic {

    /**
     Temperature
     
     Percent battery level.
     Pertains to characteristic.uuid = "21AC".
     
     The temperature characteristic is a variable length structure. It contains temperature, and may contain a time stamp and/or temperature type (indicating body location).
     
     Return values:
     - temperature:Float The temperature.
     - celcius: Bool True if the temperature is in units of Celcius, otherwise the temperature is in units of Fahrenheit
     - timeStamp: DateComponents The date components if present, otherwise nil
     - tempType: String The body location for the temperature.
     - timeStampPresent: Bool True if the time stamp is provided.
     - tempTypePresent: Bool True if the temperature type is provided.

     - Parameter characteristic: A characteristic representing a temperature measurement (UUID "2A1E").
     - Returns (temperature,celcius,date,tempType,timeStampPresent,temperatureTypePresent)
     */
	// swiftlint:disable:next large_tuple
    func htTemperature() -> (temperature: Float, celcius: Bool, timeStamp: DateComponents, tempType: String, timeStampPresent: Bool, tempTypePresent: Bool) {
        guard self.value != nil else { return (Float(0x007FFFFF),true,DateComponents(),"Unknown",false,false) }
        guard self.value!.count >= 1 else { return (Float(0x007FFFFF),true,DateComponents(),"Unknown",false,false) }

        let byteArray = [UInt8](self.value!)
        var dataOffset = 0
        
        /*
         The temperature measurement characteristic is a variable length structure. It contains temperature, and may contain a time stamp and/or temperature type (indicating body location);
         - flags                1 byte
         - temperature          4 bytes
         - [time stamp]         12 bytes
         - [temperture type]    2 bytes
         
         flags bit 0        x = 0: temperature is in degrees Celcius, x = 1: temperature is in degrees Fahrenheit
         flags bit 1        Time stamp is present (year, month, day, hour, minute, sec)
         flags bit 2        Temperature type is present (e.g., body location)
         flags bits 3-8     Reserved

         flags = 0x000x: the structure has flags + temperature fields
         flags = 0x001x: the structure has flags + temperature + time stamp
         flags = 0x010x: the structure has flags + temperature + temperature type
         flags = 0x011x: the structure has flags + temperature +time stamp + temperature type
         */
        
        // Temperature measurement is 4 bytes.
        // Time stamp is 12 bytes
        // Temperature type is (nominally?) 1 byte
        
        let flag = byteArray[0]
        let temperatureUnitIsCelsius = (flag & 0x01) == 0
        let timeStampPresent         = (flag & 0x02) > 0
        let temperatureTypePresent   = (flag & 0x04) > 0

        // Temperature measurement.
        dataOffset = 1
        var tempData: UInt32 = UInt32((byteArray[dataOffset]))
        tempData            |= UInt32((byteArray[dataOffset+1])) << 8
        tempData            |= UInt32((byteArray[dataOffset+2])) << 16
        tempData            |= UInt32((byteArray[dataOffset+3])) << 24

        var exponent = (Float) (tempData >> 24)
        if exponent > 127 { exponent -= 256 }         // exponent is -128..127
        let mantissa = tempData & 0x00FFFFFF
        if mantissa == 0x007FFFFF {
            return (Float(0x007FFFFF),true,DateComponents(),"Unknown",false,false)
//            let str = "Invalid temperature received"
//            print("  \(str)")
//            self.diagnostics += str + "\n"
        }
        let mantissaFloat = Float(mantissa)
        let temperature = mantissaFloat * pow(10.0,exponent)

         // Temperature unit.
        let celcius = temperatureUnitIsCelsius
        
        // Time stamp.
        dataOffset = 5
        var date = DateComponents() // initializes to nil
        if timeStampPresent
        {
            var tempData: UInt16 = UInt16((byteArray[dataOffset]))
            tempData            |= UInt16((byteArray[dataOffset+1])) << 8
            date.year = Int(tempData)
            date.month = Int(byteArray[dataOffset+2])
            date.day = Int(byteArray[dataOffset+3])
            date.hour = Int(byteArray[dataOffset+4])
            date.minute = Int(byteArray[dataOffset+5])
            date.second = Int(byteArray[dataOffset+6])
            dataOffset += 7
        }
        
        // Temperature type.
        var tempType = ""
        if temperatureTypePresent
        {
            let location = Int(byteArray[dataOffset+1])
            tempType = bodyLocation(location: location)
        }

        return (temperature,celcius,date,tempType,timeStampPresent,temperatureTypePresent)
    }

    /**
     Temperature type (location)
     
     Body locations:
     - 0 -> Other
     - 1 -> Armpit
     - 2 -> Body                (Body - general)
     - 3 -> Ear
     - 4 -> Finger
     - 5 -> GI Tract            (Gastro-intenstinal tract)
     - 6 -> Mouth
     - 7 -> Rectum
     - 8 -> Toe
     - 9 -> Ear                    (Tympanum - ear drum)
     - ? -> Unknown          (Reserved for future use)

     Pertains to characteristic.uuid = "2A1D".
     
     - Returns 0
     */
    func htType() -> UInt8? {
        guard self.value != nil else { return nil }
        guard self.value!.count >= 1 else { return nil }
        
        let type = self.uint8Value()!
        return type
    }

    /**
     Temperature type (location) as a string
     
     Body locations:
     - "Other"
     - "Armpit"
     - "Body"                (Body - general)
     - "Ear"
     - "Finger"
     - "GI Tract"            (Gastro-intenstinal tract)
     - "Mouth"
     - "Rectum"
     - "Toe"
     - "Ear"                    (Tympanum - ear drum)
     - "Unknown"          (Reserved for future use)

     Pertains to characteristic.uuid = "2A1D".
     
     - Returns "Body"
     */
    func htTypeString() -> String {
        guard self.value != nil else { return "" }
        guard self.value!.count >= 1 else { return "" }
        
        switch self.value![0] {
            case 0: return "Other"
            case 1: return "Armpit"
            case 2: return "Body"           // "Body - general"
            case 3: return "Ear"
            case 4: return "Finger"
            case 5: return "GI Tract"       // "Gastro-intenstinal tract"
            case 6: return "Mouth"
            case 7: return "Rectum"
            case 8: return "Toe"
            case 9: return "Ear"            // "Tympanum - ear drum"
            default:
              return "Unknown"              // "Reserved for future use"
        }
    }

}

