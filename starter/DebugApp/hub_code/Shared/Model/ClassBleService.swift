//
//  ClassBleService.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// MARK: - BLE Service Class

// swiftlint:disable all
/**
 Handles functions for BLE characteristics associated with a given service.
 
 The BLE Client class has methods that are called in response to BLE events. The BLE Client class implemented here forwards actions to this Service class or a user defined subclass. This class defines default actions for CBCharacteristics associated with a given CBService. A subclass can be created to override default actions as desired.
 
 For example, when BluetoothManager.peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) is called, it calls Service.didUpdate(characteristic: CBCharacteristic) to take an appropriate action. .
 */
class Service {
    var manager: BluetoothManager? = nil     // BLE client class.
    var chText = ""                     // Characteristic name used when displaying characteristic values in log messages.
    var serviceDirectoryName = ""       // The directory name used when storing data for a given service. Data is stored in topLevel/serviceDirectoryName/sessionDirectoryName. Subclasses should set this appropriately.
    var sessionDirectoryName = ""       // The directory name used for a given session. Data is stored in topLevel/serviceDirectoryName/sessionDirectoryName. Subclasses should set this appropriately.
    var fileCount: UInt = 1             // Data is stored in  topLevel/serviceDirectoryName/sessionDirectoryName/n.acq where n = fileCount. Subclasses should set this appropriately.
    var characteristicsNotifying = 0    // The number of characteristics reporting during a session. 0 means no session. 1 or more means keep a session going.

    /**
     Set theBLE Client class instantiation.
     
     This is required but not checked. **User beware**
     */
    func setBluetoothManager(manager: BluetoothManager) {
        self.manager = manager
    }
    
    /**
     Respond to an update for a characteristic.
     
     The delegate peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) calls didUpdate so that a desired update action can be taken for the given characteristic.
     
     The default action defined here is to dispay the characteristic value. If the value is text, it is displayed as a string. Otherwise it is displayed as hex characters. A subclass can be created to override this function and take more specific actions.
     
     - parameter characteristic: The characteristic for which an update has been received.
     */
    func didUpdate(characteristic: CBCharacteristic) {
        // Check whether return value is text or data.
        let text = characteristic.isText()

        // Look up our mapping from UUID to descriptive string. If not found, use the UUID as a string.
        let characteristicName = manager!.uuidDictionary[characteristic.uuid, default:"\(characteristic.uuid)"]

        var str = ""
        if text {
            // Appears to be text; display as text.
            let data = String(decoding: characteristic.value!, as: UTF8.self)
            str = "\(characteristicName): (default) \(data)"
            os_log("didUpdateValueFor   %{public}s", log: OSLog.ble1, type:.info, str)
//            diagnostics(str)
        } else {
            // Appears to be binary data; show as hex.
            let data = characteristic.value!.map { String(format: "%02x", $0) }.joined()
            let length = characteristic.value!.count
            str = "\(characteristicName) (default) (\(length) bytes): 0x\(data)"
            os_log("didUpdateValueFor   %{public}s", log: OSLog.ble1, type:.info, str)
//            diagnostics(str)
        }
        
        self.chText = characteristicName
        displayData(characteristic: characteristic)
    }
    
    func displayData(characteristic: CBCharacteristic) {
		let uuid = characteristic.service!.uuid
        var str = self.manager!.bleString[uuid]
        if str == nil {
            str = "unknown"
        }
        str = str!.replacingOccurrences(of: "\n", with: ", ")
        os_log("didUpdateValueFor   %{public}s: %{public}s", log: OSLog.ble1, type:.info, chText, str!)
    }

    func saveData(characteristic: CBCharacteristic, serviceDirName: String, sessionDirName: String, fCount: inout UInt) {
        guard characteristic.isNotifying else { return }
        
        // Check that there is data to save.
		let uuid = characteristic.service!.uuid
        guard self.manager!.bleData[uuid] != nil else { return }

        // Add a time stamp to the data.
        let timestamp = timeStamp()
        var data = timestamp.data(using: .utf8)
        data?.append(self.manager!.bleData[uuid]!)
        
        // Save the data.
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("\(serviceDirName)/\(sessionDirName)/\(fCount).acq")
        try? data!.write(to: path)
        fCount += 1
    }

    /**
     Create a session directory and set the file count to 1.
     
     The directory for service data is topLevel/serviceDirName/sessionDirName. The serviceDirName directory is created if necessary. The sessionDirName is directory with a current date and time formatted as "YYYY-mm-ddTHHMMSS". If successful, the error parameter is nil. If unsuccessful, the error parameter returns the cause of the failure.

     - parameter serviceDirName: A directory name for an associated service.
     - parameter sessionDirName: A directory name of a session. The name returned is of the form "yyyy-MM-dd'T'HHmmss".
     - parameter fCount: The file count used for the first file in the session directory; the return value is 1.
     - parameter error: nil if directories are created. Otherwise the cause of the failure.
     */
    func createSessionDirectory(serviceDirName: String, sessionDirName: inout String, fCount: inout UInt, error: Error?) {
        // Create subdirectory for service data (if it doesn't already exist).
        let pathString = self.manager!.dataDirectoryPath[0].path + "/\(serviceDirName)"
        do {
            try FileManager.default.createDirectory(atPath: pathString, withIntermediateDirectories: true, attributes: nil)
        } catch {
            print(error.localizedDescription)
        }
        
        // Create session directory for service data.
        sessionDirName = timeStamp()
        let sessionDirectory = "\(pathString)" + "/\(sessionDirName)"
        do {
            try FileManager.default.createDirectory(atPath: sessionDirectory, withIntermediateDirectories: true, attributes: nil)
        } catch {
            print(error.localizedDescription)
        }
        
        fCount = 1
    }


    /**
     Perform actions prior to setting a characteristic notification state to true.
     
     The BluetoothManager.updateNotificationStateToTrue method calls this before sending a message to set a characteristic notification state to true.
     
     @Note: Override this function to insert the appropriate serviceDirectoryName. 

     - parameter characteristic: The characteristic for which an update notification will be sent.
     */
    func notifyStart(characteristic: CBCharacteristic) {
        notifyStart1(characteristic: characteristic, serviceDirName: serviceDirectoryName, sessionDirName: &sessionDirectoryName, fCount: &fileCount, nCount: &characteristicsNotifying)
    }

    /**
     Perform actions prior to setting a characteristic notification state to true.
     
     The BluetoothManager.updateNotificationStateToTrue method calls this before sending a message to set a characteristic notification state to true.
     
     Data associated with a service is stored in topLevel/serviceDirName/sessionDirName/n.acq.

     - parameter characteristic: The characteristic for which an update notification will be sent.
     - parameter serviceDirName: The name of the service directory.
     - parameter sessionDirName:  The name of the session directory.
     - parameter fCount: The number assigned to the saved file, n = 1, 2, 3, ....
     - parameter nCount: The nnumber of characteristics contributing to the data.
     */
    func notifyStart1(characteristic: CBCharacteristic, serviceDirName: String, sessionDirName: inout String, fCount: inout UInt, nCount: inout Int) {
        // Create subdirectory for service data (if it doesn't already exist).
        let pathString = self.manager!.dataDirectoryPath[0].path + "/\(serviceDirName)"
        do {
            try FileManager.default.createDirectory(atPath: pathString, withIntermediateDirectories: true, attributes: nil)
        } catch {
            print(error.localizedDescription)
        }
        
        // Create session directory for service data.
        if nCount == 0 {
            sessionDirName = timeStamp()
            let sessionDirectory = "\(pathString)" + "/\(sessionDirName)"
            do {
                try FileManager.default.createDirectory(atPath: sessionDirectory, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
            fCount = 1
        }
        
        nCount += 1
    }

    /**
     Perform actions after to setting a characteristic notification state to false.
     
     The BluetoothManager.updateNotificationStateToFalse method calls this before sending a message to set a characteristic notification state to false.

     - parameter characteristic: The characteristic for which an update notification has been sent.
     */
    func notifyStop(characteristic: CBCharacteristic) {
        notifyStop1(characteristic: characteristic, sessionDirName: &sessionDirectoryName, fCount: &fileCount, nCount: &characteristicsNotifying)
    }

    /**
     Perform actions after to setting a characteristic notification state to false.
     
     The BluetoothManager.updateNotificationStateToFalse method calls this before sending a message to set a characteristic notification state to false.

     - parameter characteristic: The characteristic for which an update notification has been sent.
     */
    func notifyStop1(characteristic: CBCharacteristic, sessionDirName: inout String, fCount: inout UInt, nCount: inout Int) {
        nCount -= 1
        if nCount == 0 {
            sessionDirName = ""
            fCount = 1
        }
    }

    /**
     Perform actions when a characteristic is discovered.

     - parameter characteristic: The characteristic which has been discovered.
     */
    func didDiscoverCharacteristicAction(characteristic: CBCharacteristic) {
        let str = self.manager!.characteristicString(characteristic: characteristic)
        os_log("didDiscoverCharacteristicAction     %{public}s", log: OSLog.ble1, type:.info, str)
        return
    }
    
    /**
     Create a date and time string of the form "yyyy-MM-dd'T'HHmmss".
     */
    func timeStamp() -> String {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HHmmss"
        let timestamp = format.string(from: date)
        return "\(timestamp)"
    }
    
}
