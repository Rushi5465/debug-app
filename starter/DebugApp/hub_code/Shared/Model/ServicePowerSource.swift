//
//  ServicePowerSource.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
// UUIDs for the service and associated characteristics.
let BLE_PS_ServicePowerSource                       = CBUUID.init(string: "EC61A454-ED00-A5E8-B8F9-DE9EC026EC51")    // Thunderboard custom
let BLE_PS_CharacteristicPowerSource                = CBUUID.init(string: "EC61A454-ED01-A5E8-B8F9-DE9EC026EC51")    // Thunderboard custom


/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
class PS_Service : Service {
    let myServiceDirectoryName = "ps_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.

    override func didUpdate(characteristic: CBCharacteristic) {
        var ch = ""
        var str = ""
        switch characteristic.uuid {
        case BLE_PS_CharacteristicPowerSource:
            ch = "Power source"
            str = characteristic.psPowerSourceString()
            self.manager!.bleString[BLE_PS_CharacteristicPowerSource] = str
        default:
            return
        }

        self.manager!.bleString[BLE_PS_ServicePowerSource] = str
        str = str.replacingOccurrences(of: "\n", with: ", ")
        os_log("didUpdateValueFor   %{public}s: %{public}s", log: OSLog.ble1, type:.info, ch, str)
    }
}


extension BluetoothManager {
    
    public func initPowerSource(_ manager: BluetoothManager) {
        let service = PS_Service()
        service.manager = manager
        soiDict[BLE_PS_ServicePowerSource] = service
        
        // Add to BLE strings.
        bleString[BLE_PS_CharacteristicPowerSource]             = "unknown"
        bleString[BLE_PS_ServicePowerSource]                    = bleString[BLE_PS_CharacteristicPowerSource]

        // Add to UUID strings
        uuidDictionary[BLE_PS_ServicePowerSource]               = "Power source custom"
        uuidDictionary[BLE_PS_CharacteristicPowerSource]        = "Power source"
    }

}


extension CBCharacteristic {

    /**
     Power source
     
     Sources are
     - 0 = unknown
     - 1 = USB
     - 2 = AA
     - 3 = AAA
     - 4 = Coin cell
     
     Pertains to characteristic.uuid = "EC61A454-ED01-A5E8-B8F9-DE9EC026EC51".
     
     - Returns: 0
     */
    func psPowerSource() -> UInt8 {
        // 0x00 : POWER_SOURCE_TYPE_UNKNOWN   -->
        // 0x01 : POWER_SOURCE_TYPE_USB       -->
        // 0x02 : POWER_SOURCE_TYPE_AA        -->
        // 0x03 : POWER_SOURCE_TYPE_AAA       -->
        // 0x04 : POWER_SOURCE_TYPE_COIN_CELL -->

        let power = self.uint8Value()!
        return power
    }

    /**
     Power source as a string
     
     Sources are
     - "Unknown"
     - "USB"
     - "AA"
     - "AAA"
     - "Coin cell"
     
     Pertains to characteristic.uuid = "EC61A454-ED01-A5E8-B8F9-DE9EC026EC51".
     
     - Returns: "USB"
     */
    func psPowerSourceString() -> String {
        // 0x00 : POWER_SOURCE_TYPE_UNKNOWN   -->
        // 0x01 : POWER_SOURCE_TYPE_USB       -->
        // 0x02 : POWER_SOURCE_TYPE_AA        -->
        // 0x03 : POWER_SOURCE_TYPE_AAA       -->
        // 0x04 : POWER_SOURCE_TYPE_COIN_CELL -->

        let power = self.uint8Value()!
        switch power {
        case 1:
            return "USB"
        case 2, 3:
            return "AA"
        case 4:
            return "Coin Cell"
        default:
            return "Unknown"
        }
    }

}
