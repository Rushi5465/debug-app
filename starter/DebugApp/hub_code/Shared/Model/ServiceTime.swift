//
//  ServiceCurrentTime.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
// UUIDs for the service and associated characteristics.
let BLE_CT_ServiceCurrentTime                       = CBUUID.init(string: "1805")
let BLE_CT_CharacteristicCurrentTime                = CBUUID.init(string: "2A2B")
let BLE_CT_CharacteristicLocalTimeInfo              = CBUUID.init(string: "2A0F")


/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
class CT_Service : Service {
    let myServiceDirectoryName = "ct_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.

    override func didUpdate(characteristic: CBCharacteristic) {
        var ch = "Unknown"
        var str = "Unknown"
        switch characteristic {
        case BLE_CT_CharacteristicCurrentTime:
            str = characteristic.ctCurrentTime() ?? ""
            self.manager!.bleString[BLE_CT_CharacteristicCurrentTime] = str
            ch = "Current time"
        case BLE_CT_CharacteristicLocalTimeInfo:
            let (hours, dstOffset, _) = characteristic.ctLocalTimeInformation() ?? (0, 0, "")
            str = "\(hours) hours from UTC + \(dstOffset) DST offset"
            self.manager!.bleString[BLE_CT_CharacteristicLocalTimeInfo] = str
            ch = "Local time info"
        default:
            return
        }
        os_log("didUpdateValueFor   %{public}s: %{public}s", log: OSLog.ble1, type:.info, ch, str)
//        diagnostics(str)

        self.manager!.bleString[BLE_CT_ServiceCurrentTime] = str
        str = str.replacingOccurrences(of: "\n", with: ", ")
        os_log("didUpdateValueFor   %{public}s: %{public}s", log: OSLog.ble1, type:.info, ch, str)
    }
}


extension BluetoothManager {

    public func initCurrentTime(_ manager: BluetoothManager) {
        let service = CT_Service()
        service.manager = manager
        soiDict[BLE_CT_ServiceCurrentTime] = service
        
        // Add to BLE strings.
        bleString[BLE_CT_CharacteristicCurrentTime]             = "2021-01-01T001122"
        bleString[BLE_CT_CharacteristicLocalTimeInfo]           = bleString[BLE_CT_CharacteristicLocalTimeInfo]
        bleString[BLE_CT_ServiceCurrentTime]                    = bleString[BLE_CT_CharacteristicCurrentTime]

        // Add to UUID strings
        uuidDictionary[BLE_CT_ServiceCurrentTime]               = "Current time (1805)"
        uuidDictionary[BLE_CT_CharacteristicCurrentTime]        = "Current time (2A2B)"
        uuidDictionary[BLE_CT_CharacteristicLocalTimeInfo]      = "Local time info (2A0F)"
    }
    
}


extension CBCharacteristic {

    /**
     Current time.
     
     Pertains to characteristic.uuid = "2A2B".

     - returns: "YYYY-MM-DDTHH:MMSS"
     */
    func ctCurrentTime() -> String? {
        guard self.value != nil else { return nil }
        guard self.value!.count >= 1 else { return nil }
        let bytes = self.value!

        let year = UInt16(bytes[0]) + (UInt16(bytes[1]) << 8)   // Unknown(0), 1582 to 9999
        let month = bytes[2]            // January(1) to December(12), unknown(0)
        let day = bytes[3]              // day of month, 1 to 31, unknown(0)
        var hours = bytes[4]            // hours past midnight, 0 to 23
        let minutes = bytes[5]          // minutes since start of hour, 0 to 59
        let seconds = bytes[6]          // seconds since start of minute, 0 to 59
        //let dayOfWeek = bytes[7]        // Monday(1) to Sunday(7), unknown(0)
        //let fractions = bytes[8]        // Seconds = fractions/256
        let adjustReason = bytes[9]     // Standard time(0) = 0, Daylight time(4) = +1, unknown(255)
        
        if adjustReason == 4 {
            hours += 1
        }
        let str = "\(year)" + "-" + String(format: "%02d", month) + "-" + String(format: "%02d", day) + "T" + String(format: "%02d", hours) + ":" + String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds)
        return str
    }

    /**
     Locatl time information
     
     Extract hours and Daylight Savings Time (DST) offsets.
     Pertains to characteristic.uuid = "2A0F".

     - Note
     A Local Time Information characteristic is used to define the relation (offset) between local time and UTC. It contains time zone and Daylight Savings Time (DST) offset information.
     
     The range for hours is -12.0 hours to +14.0 hours. (Positive means hours ahead, negative means hours behind.) Hours are expressed in quarter hour increments; an offset of 1 hour and 15 minutes woud be returned as 1.25 hours.
     
     The DST offset is a number with the following meaning:
     - Value -> Description
     - 0 -> Standard Time
     - 2 -> Half an hour Daylight Time (+ 0.5h)
     - 4 -> Daylight Time (+ 1h)
     - 8 -> Double Daylight Time (+ 2h)
     - 255 -> DST offset unknown
     - 1, 3, 5–7 and 9–254 -> Reserved for Future Use
     
     - Returns: A tuple (time zone offset from UTC in hours, DST offset, DST string)
     */
    public func ctLocalTimeInformation() -> (Double,Int,String)? {
        guard self.value != nil else { return nil }
        guard self.value!.count >= 1 else { return nil }
        let bytes = self.value!

        // The time zone is provided as the number of 15-minute offsets between local time and UTC. The valid range is -48 to +56.
        var offset = bytes[0] > 127 ? Int(bytes[0]) - 128 : Int(bytes[0])       // signed 8 bits
        offset = offset - 64                                                    // signed 6 bits
        let hours = (Double(offset) * 15.0) / 60.0
        
        let dstOffset = Int(bytes[1])
 
        var dst = ""
        switch dstOffset {
            case 0:
                dst = "Standard Time"
            case 2:
                dst = "Half an hour Daylight Time (+0.5h)"
            case 4:
                dst = "Daylight Time (+1h)"
            case 8:
                dst = "Double Daylight Time (+2h)"
            default:
                dst = "Unknown"
        }

        return (hours, dstOffset, dst)
    }

}
