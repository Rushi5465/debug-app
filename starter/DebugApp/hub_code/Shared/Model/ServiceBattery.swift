//
//  ServiceBattery.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
// UUIDs for the service and associated characteristics.
let BLE_BA_ServiceBattery                           = CBUUID.init(string: "180F")   // Battery Info Service (org.bluetooth.service.battery_service)
let BLE_BA_CharacteristicBatteryLevel               = CBUUID.init(string: "2A19")   // Battery Level (org.bluetooth.characteristic.battery_level)


class BA_Service : Service {
    let myServiceDirectoryName = "ba_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.

    override func didUpdate(characteristic: CBCharacteristic) {
        let ch = "Battery level"
        let str = characteristic.batteryLevelString()
        self.manager!.bleString[BLE_BA_ServiceBattery] = str
        os_log("didUpdateValueFor   %{public}s: %{public}s", log: OSLog.ble1, type:.info, ch, str)
    }
}


/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
extension BluetoothManager {
    
    public func initBattery(_ manager: BluetoothManager) {
        let service = BA_Service()
        service.manager = manager
        soiDict[BLE_BA_ServiceBattery] = service
        
        // Add to BLE strings.
        bleString[BLE_BA_CharacteristicBatteryLevel]        = "0%"
        bleString[BLE_BA_ServiceBattery]                    = bleString[BLE_BA_CharacteristicBatteryLevel]

        // Add to UUID strings
        uuidDictionary[BLE_BA_ServiceBattery]               = "Battery (180F)"
        uuidDictionary[BLE_BA_CharacteristicBatteryLevel]   = "Battery level (2A19)"
    }
    
}


extension CBCharacteristic {

    /**
     Battery level.
     
     Percent battery level.
     Pertains to characteristic.uuid = "2A19".
     
     - Returns: Level 0 to 100.
     */
    func batteryLevel() -> UInt8? {
        guard self.value != nil else { return nil }
        guard self.value!.count >= 1 else { return nil }
        let level = self.value![0]
        return level
    }
    
    /**
     Battery level as a string.
     
     Percent battery level.
     Pertains to characteristic.uuid = "2A19".
     
     - Returns: "50%"
     */
    func batteryLevelString() -> String {
        guard self.value != nil else { return "" }
        guard self.value!.count >= 1 else { return "" }
        let level = self.batteryLevel()
        return "\(level!)"
    }

}
