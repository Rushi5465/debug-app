//
//  ServiceAccelerationOrientationb.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
// UUIDs for the service and associated characteristics.
let BLE_AO_ServiceAccelerationOrientation           = CBUUID.init(string: "A4E649F4-4BE5-11E5-885D-FEFF819CDC9F")   // Thunderboard Inertial Measurement (custom) (aka Acceleration and Orientation)
let BLE_AO_CharacteristicAcceleration               = CBUUID.init(string: "C4C1F6E2-4BE5-11E5-885D-FEFF819CDC9F")   // Thunderboard Acceleration Measurement (custom)
let BLE_AO_CharacteristicOrientation                = CBUUID.init(string: "B7C4B694-BEE3-45DD-BA9F-F3B5E994F49A")   // Thunderboard Orientation Measurement (custom)
let BLE_AO_CharacteristicCalibrate                  = CBUUID.init(string: "71E30B8C-4131-4703-B0A0-B0BBBA75856B")   // Thunderboard Calibrate Command (custom)


/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
class AO_Service : Service {
    let myServiceDirectoryName = "ao_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.
    
    override func didUpdate(characteristic: CBCharacteristic) {
        var ch = "Unknown"
        if characteristic.uuid == BLE_AO_CharacteristicAcceleration {
            self.manager!.bleData[BLE_AO_CharacteristicAcceleration] = characteristic.value
            self.manager!.bleString[BLE_AO_CharacteristicAcceleration] = characteristic.aoAccelerationString()
            ch = "Acceleration"
        } else if characteristic.uuid == BLE_AO_CharacteristicOrientation {
            self.manager!.bleData[BLE_AO_CharacteristicOrientation] = characteristic.value
            self.manager!.bleString[BLE_AO_CharacteristicOrientation] = characteristic.aoOrientationString()
            ch = "Orientation"
//        } else if characteristic.uuid == BLE_AO_CharacteristicCalibrate {
//            str = calibrateValue(characteristic: characteristic)
//            ch = "Calibrate"
        }
        
        // Diplay the data.
        self.chText = ch
        self.manager!.bleString[BLE_AO_ServiceAccelerationOrientation] = aoAccelerationString()
        displayData(characteristic: characteristic)

        // Save the data.
        var data = self.manager!.bleData[BLE_AO_CharacteristicAcceleration]!
        data.append(self.manager!.bleData[BLE_AO_CharacteristicOrientation]!)
        self.manager!.bleData[BLE_AO_ServiceAccelerationOrientation] = data
        saveData(characteristic: characteristic, serviceDirName: myServiceDirectoryName, sessionDirName: mySessionDirectoryName, fCount: &myFileCount)
    }
    
    override func notifyStart(characteristic: CBCharacteristic) {
        notifyStart1(characteristic: characteristic, serviceDirName: myServiceDirectoryName, sessionDirName: &mySessionDirectoryName, fCount: &myFileCount, nCount: &myCharacteristicsNotifying)
    }

    override func notifyStop(characteristic: CBCharacteristic) {
        notifyStop1(characteristic: characteristic, sessionDirName: &mySessionDirectoryName, fCount: &myFileCount, nCount: &myCharacteristicsNotifying)
    }
    
    /**
     Concatenate acceleration and orientation results.
     
     - returns "x = 0.013 g\ny = -0.087 g\nz = 0.979 g\n\nx = 0°\ny = 0°\nz = 0°"
     */
    func aoAccelerationString() -> String {
        return "\(self.manager!.bleString[BLE_AO_CharacteristicAcceleration] ?? "x=0 g\ny=0 g\nz=0 g")\n\n\(self.manager!.bleString[BLE_AO_CharacteristicOrientation] ?? "x=0°\ny=0°\nz=0°")"
    }
    
}


extension BluetoothManager {

    public func initAccelerationOrientation(_ manager: BluetoothManager) {
        let service = AO_Service()
        service.manager = manager
        soiDict[BLE_AO_ServiceAccelerationOrientation] = service
                
        // Add to BLE strings.
        bleString[BLE_AO_CharacteristicAcceleration]            = "x = 0 g\ny = 0 g\nz = 0 g"
        bleString[BLE_AO_CharacteristicOrientation]             = "x = 0°\ny = 0°\nz = 0°"
        bleString[BLE_AO_ServiceAccelerationOrientation]        = "x = 0 g\ny = 0 g\nz = 0 g" + "\n\n" + "x = 0°\ny = 0°\nz = 0°"
        
        // Add to UUID strings
        uuidDictionary[BLE_AO_ServiceAccelerationOrientation]   = "Inertial (Acceleration/Orientation)"
        uuidDictionary[BLE_AO_CharacteristicAcceleration]       = "Acceleration"
        uuidDictionary[BLE_AO_CharacteristicOrientation]        = "Orientation"
        uuidDictionary[BLE_AO_CharacteristicCalibrate]          = "Calibrate"
        
        // Add to BLE data.
        bleData[BLE_AO_CharacteristicAcceleration]              = Data.init(count: 6)
        bleData[BLE_AO_CharacteristicOrientation]               = Data.init(count: 6)
        bleData[BLE_AO_ServiceAccelerationOrientation]          = Data.init(count: 12)
    }
    
}


extension CBCharacteristic {

    /**
     X, Y, Z acceleration.
     
     Pertains to characteristic.uuid = "A4E649F4-4BE5-11E5-885D-FEFF819CDC9F".
     
     - Returns: (X,Y,Z) acceleration values as integers times 1000.
     */
    func aoAcceleration() -> (Int16, Int16, Int16) {
        guard self.value != nil else { return (0, 0, 0) }
        guard self.value!.count >= 1 else { return (0, 0, 0) }
        let x = self.int16Value(0)!
        let y = self.int16Value(2)!
        let z = self.int16Value(4)!
        return (x, y, z)
    }
    
    /**
     X, Y, Z acceleration.
     
     Pertains to characteristic.uuid = "A4E649F4-4BE5-11E5-885D-FEFF819CDC9F".
     
     - Returns: (X,Y,Z) acceleration values as doubles.
     */
    func aoAccelerationDouble() -> (Double, Double, Double) {
        guard self.value != nil else { return (0.0, 0.0, 0.0) }
        guard self.value!.count >= 1 else { return (0.0, 0.0, 0.0) }
        let x = Double(self.int16Value(0)!) / 1000.0
        let y = Double(self.int16Value(2)!) / 1000.0
        let z = Double(self.int16Value(4)!) / 1000.0
        return (x, y, z)
    }
    
    /**
     X, Y, Z acceleration as a string.
     
     Pertains to characteristic.uuid = "A4E649F4-4BE5-11E5-885D-FEFF819CDC9F".
     
     - Returns: "x = 1.0 g\ny = 1.0 g\nz = 1.0 g".
     */
    func aoAccelerationString() -> String {
        guard self.value != nil else { return "" }
        guard self.value!.count >= 1 else { return "" }
        let (x,y,z) = self.aoAccelerationDouble()
        return "x = \(x) g\ny = \(y) g\nz = \(z) g"
    }
   
    /**
     X, Y, Z inclination angles.
     
     Pertains to characteristic.uuid = "B7C4B694-BEE3-45DD-BA9F-F3B5E994F49A".
     
     - Returns: (X,Y,Z) in degrees as integers times 100.
     */
    func aoOrientation() -> (Int16, Int16, Int16) {
        guard self.value != nil else { return (0, 0, 0) }
        guard self.value!.count >= 1 else { return (0, 0, 0) }
        let x = self.int16Value(0)!
        let y = self.int16Value(2)!
        let z = self.int16Value(4)!
        return (x, y, z)
    }

    /**
     X, Y, Z inclination angles.
     
     Pertains to characteristic.uuid = "B7C4B694-BEE3-45DD-BA9F-F3B5E994F49A".
     
     - Returns: (X,Y,Z) in degrees as doubles.
     */
    func aoOrientationDouble() -> (Double, Double, Double) {
        guard self.value != nil else { return (0.0, 0.0, 0.0) }
        guard self.value!.count >= 1 else { return (0.0, 0.0, 0.0) }
        let x = Double(self.int16Value(0)!) / 100.0
        let y = Double(self.int16Value(2)!) / 100.0
        let z = Double(self.int16Value(4)!) / 100.0
        return (x, y, z)
    }
    
    /**
     X, Y, Z inclination angles as a string.
     
     Pertains to characteristic.uuid = "B7C4B694-BEE3-45DD-BA9F-F3B5E994F49A".
 
     - Returns: "x = 1.0°\ny = 1.0°\nz = 1.0°".
     */
    func aoOrientationString() -> String {
        guard self.value != nil else { return "" }
        guard self.value!.count >= 1 else { return "" }
        let (x,y,z) = self.aoOrientationDouble()
        return "x = \(x)°\ny = \(y)°\nz = \(z)°"
    }
    
}

