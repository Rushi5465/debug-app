//
//  ServiceX.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//


// Services and characteristics not supported, but identified.

import Foundation
import CoreBluetooth
import os.log

// Firmware upgrade
// UUIDs for the service and associated characteristics.
//let BLE_ServiceOTA                                  = CBUUID.init(string: "1D14D6EE-FD63-4FA1-BFA4-8F47B42119F0")    // Over the air service
//let BLE_CharacteristicFWUpgrade                     = CBUUID.init(string: "F7BF3564-FB6D-4E53-88A4-5E37E0326063")    // Over the air characteristic of firmware upgrades


// UUIDs for the service and associated characteristics.
//let BLE_BP_ServiceBloodPressure                     = CBUUID.init(string: "1810")    // Blood Pressure
//let BLE_BP_CharacteristicCuffPressure               = CBUUID.init(string: "2A36")    // Intermediate Cuff Pressure
//let BLE_BP_CharacteristicBPMeasurement              = CBUUID.init(string: "2A35")    // Blood Pressure Measurement
//let BLE_BP_CharacteristicBPFeature                  = CBUUID.init(string: "2A49")    // Blood Pressure Feature


//extension BleClient {
//
//    public func initX() {
//
//        // Add to BLE strings.
//        bleString[BLE_ServiceOTA]                           = "unknown"
//        bleString[BLE_CharacteristicFWUpgrade]              = "unknown"
//
//        // Add to UUID strings
//        uuidDictionary[BLE_ServiceOTA]                      = "Over the air"
//        uuidDictionary[BLE_CharacteristicFWUpgrade]         = "Over the air firmware upgrades"
//
//        // Add to BLE strings.
//        bleString[BLE_BP_CharacteristicCuffPressure]        = "0"
//        bleString[BLE_BP_CharacteristicBPMeasurement]       = "0"
//        bleString[BLE_BP_CharacteristicBPFeature]           = "0"
//        bleString[BLE_BP_ServiceBloodPressure]              = bleString[BLE_BP_CharacteristicCuffPressure]
//
//        // Add to UUID strings
//        uuidDictionary[BLE_BP_ServiceBloodPressure]         = "Blood pressure (1810)"
//        uuidDictionary[BLE_BP_CharacteristicCuffPressure]   = "Cuff pressure (2A36)"
//        uuidDictionary[BLE_BP_CharacteristicBPMeasurement]  = "BP measurement (2A35)"
//        uuidDictionary[BLE_BP_CharacteristicBPFeature]      = "BP feature (2A49)"
//    }
//
//    public func didUpdateX(characteristic: CBCharacteristic) -> Bool {
//        let ch = "Unknown"
//        let str = "Unknown"
//        bleString[characteristic.uuid] = str
//
//        os_log("didUpdateValueFor   %{public}s: %{public}s", log: OSLog.ble1, type:.info, ch, str)
//        diagnostics(str)
//
////        if currentView == .firmwareupgrade {
////            realTimeDisplay = str
////        }
//
//        return true
//    }
//
////    func didDiscoverActionX(characteristic: CBCharacteristic) {
////        print ("discovered characteristic \(characteristic.uuid)")
////    }
////
////    public func notifyStartX(characteristic: CBCharacteristic) {
////        return
////    }
////
////    public func notifyStopX(characteristic: CBCharacteristic) {
////        return
////    }
//
//}

extension CBCharacteristic {
    
}
