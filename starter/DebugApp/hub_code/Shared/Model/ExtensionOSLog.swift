//
//  ExtensionOSLog.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//


//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// MARK: - OSLog Extension

// Provide a means for filtering output in the Console app.
// Search for "ble-4321" to see only level 1 messages.
// Search for "ble-432"  to see level 1 and 2 messages.
// Search for "ble-43"   to see level 1, 2, and 3 messages.
// Search for "ble-4"    to see level 1, 2, 3, and 4 messages.
extension OSLog {
    // Name our subsystem.
    private static var subsystem = Bundle.main.bundleIdentifier!

    // Add our categories.
    static let ble1 = OSLog(subsystem: subsystem, category: "ble-4321")
    static let ble2 = OSLog(subsystem: subsystem, category: "ble-432")
    static let ble3 = OSLog(subsystem: subsystem, category: "ble-43")
    static let ble4 = OSLog(subsystem: subsystem, category: "ble-4")
}

// Swift briding function to allow C code to call Swift logging.
// Swift bridging function. Implementation of using Swift logging for C debug printing (as implemented in hub dprintf()).
// https://forums.swift.org/t/best-way-to-call-a-swift-function-from-c/9829/2
//private func swiftLogImpl(modifier: UnsafePointer<CChar>) {
//    let str = String(cString: modifier)
//    os_log("%@", log: OSLog.ble1, type:.info, str)
//}
//
//var callbacks = cCallbacks(
//    swiftLogImpl: { (modifier) in
//        swiftLogImpl(modifier: modifier)
//    }
//)
private func swiftLogImpl(modifier: UnsafePointer<CChar>, level: Int32) {
    let str = String(cString: modifier)
    var ble = OSLog.ble1
    switch level {
    case 1:
        ble = OSLog.ble1
    case 2:
        ble = OSLog.ble2
    case 3:
        ble = OSLog.ble3
    case 4:
        ble = OSLog.ble4
    default:
        ble = OSLog.ble1
    }
    os_log("%@", log: ble, type:.info, str)
}

//var callbacks = cCallbacks(
//    swiftLogImpl: { (modifier, level) in
//        swiftLogImpl(modifier: modifier, level: level)
//    }
//)

//// https://ofstack.com/C++/20858/c-language-call-swift-function-example.html
//private func swiftLogImpl(_ cchar: UnsafeMutablePointer<Int8>) -> Void {
//    print("This is a swift function")
//    print("This is a swift function: %s", cchar)
//}



//var gLogDebugLevel = 1
//
//func dblog(_ threshold: Int, _ items: Any..., separator: String = " ", terminator: String = "\n") {
//    if threshold <= gLogDebugLevel {
//        let output = items.map { "\($0)" }.joined(separator: separator)
//        Swift.print(output, terminator: terminator)
//    }
//}
