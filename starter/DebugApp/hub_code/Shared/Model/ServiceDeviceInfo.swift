//
//  ServiceDeviceInfo.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
// UUIDs for the service and associated characteristics.
let BLE_DI_ServiceDeviceInfo                        = CBUUID.init(string: "180A")    // Device Information   org.bluetooth.BLE_Service.device_information        0x180A    GSS
let BLE_DI_CharacteristicSystemID                   = CBUUID.init(string: "2A23")    // System ID
let BLE_DI_CharacteristicModelNumber                = CBUUID.init(string: "2A24")    // Model Number         org.bluetooth.characteristic.model_number_string    0x2A24    GSS
let BLE_DI_CharacteristicSerialNumber               = CBUUID.init(string: "2A25")    // Serial Number        org.bluetooth.characteristic.serial_number_string   0x2A25    GSS
let BLE_DI_CharacteristicFirmwareRevision           = CBUUID.init(string: "2A26")    // Firmware Revision
let BLE_DI_CharacteristicHardwareRevision           = CBUUID.init(string: "2A27")    // Hardware Revision
let BLE_DI_CharacteristicSoftwareRevision           = CBUUID.init(string: "2A28")    // Software Revision
let BLE_DI_CharacteristicManufacturerName           = CBUUID.init(string: "2A29")    // Manufacturer Name    org.bluetooth.characteristic.manufacturer_name_string    0x2A29    GSS


/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
class DI_Service : Service {
    let myServiceDirectoryName = "di_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.

    override func didUpdate(characteristic: CBCharacteristic) {
        var ch = "Unknown"
        var str = "Unknown"
        if characteristic.uuid == BLE_DI_CharacteristicManufacturerName {
            str = characteristic.diManufacturerName()
            self.manager!.bleString[BLE_DI_CharacteristicManufacturerName] = str
            ch = "Manufacturer name"
        } else if characteristic.uuid == BLE_DI_CharacteristicSystemID {
            let (str1,str2) = characteristic.diSystemID()
            str = "\(str1)\n\(str2)"
            self.manager!.bleString[BLE_DI_CharacteristicSystemID] = str
            ch = "System ID"
        } else if characteristic.uuid == BLE_DI_CharacteristicModelNumber {
            str = characteristic.diModelNumber()
            self.manager!.bleString[BLE_DI_CharacteristicModelNumber] = str
           ch = "Model number"
        } else if characteristic.uuid == BLE_DI_CharacteristicSerialNumber {
            str = characteristic.diSerialNumber()
            self.manager!.bleString[BLE_DI_CharacteristicSerialNumber] = str
            ch = "Model number"
        } else if characteristic.uuid == BLE_DI_CharacteristicFirmwareRevision {
            str = characteristic.diFirmware()
            self.manager!.bleString[BLE_DI_CharacteristicFirmwareRevision] = str
            ch = "Firmware revision"
        } else if characteristic.uuid == BLE_DI_CharacteristicHardwareRevision {
            str = characteristic.diHardware()
            self.manager!.bleString[BLE_DI_CharacteristicHardwareRevision] = str
            ch = "Hardware revision"
        } else if characteristic.uuid == BLE_DI_CharacteristicSoftwareRevision {
            str = characteristic.diSoftware()
            self.manager!.bleString[BLE_DI_CharacteristicSoftwareRevision] = str
            ch = "Software revision"
        }
        os_log("didUpdateValueFor   %{public}s: %{public}s", log: OSLog.ble1, type:.info, ch, str)
//        diagnostics(str)

        self.manager!.bleString[BLE_DI_ServiceDeviceInfo] = str
    }
}


extension BluetoothManager {

    public func initDeviceInfo(_ manager: BluetoothManager) {
        let service = DI_Service()
        service.manager = manager
        soiDict[BLE_DI_ServiceDeviceInfo] = service

        // Add to BLE strings.
        bleString[BLE_DI_CharacteristicSystemID]                = "unknown"
        bleString[BLE_DI_CharacteristicModelNumber]             = "unknown"
        bleString[BLE_DI_CharacteristicSerialNumber]            = "unknown"
        bleString[BLE_DI_CharacteristicFirmwareRevision]        = "unknown"
        bleString[BLE_DI_CharacteristicHardwareRevision]        = "unknown"
        bleString[BLE_DI_CharacteristicSoftwareRevision]        = "unknown"
        bleString[BLE_DI_CharacteristicManufacturerName]        = "unknown"
        bleString[BLE_DI_ServiceDeviceInfo]                     = "unknown"

        // Add to UUID strings
        uuidDictionary[BLE_DI_ServiceDeviceInfo]                = "Device info (180A)"
        uuidDictionary[BLE_DI_CharacteristicSystemID]           = "System ID (2A23)"
        uuidDictionary[BLE_DI_CharacteristicModelNumber]        = "Model number (2A24)"
        uuidDictionary[BLE_DI_CharacteristicSerialNumber]       = "Serial number (2A25)"
        uuidDictionary[BLE_DI_CharacteristicFirmwareRevision]   = "Firmware revision (2A26)"
        uuidDictionary[BLE_DI_CharacteristicHardwareRevision]   = "Hardware revision (2A27)"
        uuidDictionary[BLE_DI_CharacteristicSoftwareRevision]   = "Software revision (2A28)"
        uuidDictionary[BLE_DI_CharacteristicManufacturerName]   = "Manufacturer name (2A29)"
    }
    
}


extension CBCharacteristic {

    /**
     Model number
     
     Pertains to characteristic.uuid = "2A24".
     
     - Returns: "Number"
     */
    func diModelNumber() -> String {
        let str = self.stringValue()
        return str
    }

    /**
     Serial number
     
     Pertains to characteristic.uuid = "2A25".
     
     - Returns: "Number"
     */
    func diSerialNumber() -> String {
        let str = self.stringValue()
        return str
    }
    /**
     Firmware revision
     
     Pertains to characteristic.uuid = "2A26".
     
     - Returns: "A12"
     */
    func diFirmware() -> String {
        let str = self.stringValue()
        return str
    }

    /**
     Hardware revision
     
     Pertains to characteristic.uuid = "2A27".
     
     - Returns: "A12"
     */
    func diHardware() -> String {
        let str = self.stringValue()
        return str
    }

    /**
     Software revision
     
     Pertains to characteristic.uuid = "2A28".
     
     - Returns: "A12"
     */
    func diSoftware() -> String {
        let str = self.stringValue()
        return str
    }

    /**
     Manufacturer name
     
     Pertains to characteristic.uuid = "2A29".
     
     - Returns: "A12"
     */
    func diManufacturerName() -> String {
        let str = self.stringValue()
        return str
    }

    /**
     System ID
     
     A system ID consists of 2 fields. The first is a 40-bit manufacturer-defined identifier. The second is a 24-bit unique Organizationally Unique Identifier (OUI)
     
     Pertains to characteristic.uuid = "2A23".
     
     - Returns: A tuple ("00:01:02:03:04", "05:06:07") indicating the 40-bit MDI and 24-bit OUI.
     */
    func diSystemID() -> (String,String) {
        // The SYSTEM ID characteristic consists of a structure with two fields. The first field are the LSOs and the second field contains the MSOs. This is a 64-bit structure which consists of a 40-bit manufacturer-defined identifier concatenated with a 24 bit unique Organizationally Unique Identifier (OUI). The OUI is issued by the IEEE Registration Authority (http://standards.ieee.org/regauth/index.html) and is required to be used in accordance with IEEE Standard 802-2001.6 while the least significant 40 bits are manufacturer defined. If System ID generated based on a Bluetooth Device Address, it is required to be done as follows. System ID and the Bluetooth Device Address have a very similar structure: a Bluetooth Device Address is 48 bits in length and consists of a 24 bit Company Assigned Identifier (manufacturer defined identifier) concatenated with a 24 bit Company Identifier (OUI). In order to encapsulate a Bluetooth Device Address as System ID, the Company Identifier is concatenated with 0xFFFE followed by the Company Assigned Identifier of the Bluetooth Address. For more guidelines related to EUI-64, refer to http://standards.ieee.org/develop/regauth/tut/eui64.pdf. If the system ID is based of a Bluetooth Device Address with a Company Identifier (OUI) is 0x123456 and the Company Assigned Identifier is 0x9ABCDE, then the System Identifier is required to be 0x123456FFFE9ABCDE. Mandatory uint40 0 1099511627775 Mandatory uint24 0 16777215 The fields in the above table are in the order of LSO to MSO. Where LSO = Least Significant Octet and MSO = Most Significant Octet.
        
        guard self.value != nil else { return ("","") }
        guard self.value!.count == 8 else { return ("","") }
        
        // 8-bytes = 64 bits.
        // First 5 bytes (40 bits) are the manufacture-defined ID (often the MAC address)
        // Second 3 bytes (24 bits) are the Organizationally Unique Identifier (OUI)
        var mdid = ""
        for i in 0...3 {
            mdid = mdid + String(format: "%2x:", self.value![i])
        }
        mdid = mdid + String(format: "%2x", self.value![4])

        var oui = ""
        for i in 5...6 {
            oui = oui + String(format: "%2x:", self.value![i])
        }
        oui = oui + String(format: "%2x", self.value![7])

        return (mdid, oui)
    }

}
