//
//  ServiceAutomationIO.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
// UUIDs for the service and associated characteristics.
let BLE_AI_ServiceAutomationIo                      = CBUUID.init(string: "1815")   // Automation IO (org.bluetooth.service.automation_io)
let BLE_AI_CharacteristicDigital                    = CBUUID.init(string: "2A56")   // Digital (org.bluetooth.characteristic.digital)
let BLE_AI_CharacteristicPresentationFormat         = CBUUID(string: "0x2904")      // Characteristic Presentation Format (org.bluetooth.descriptor.gatt.characteristic_presentation_format)
let BLE_AI_CharacteristicNumberOfDigitals           = CBUUID(string: "0x2909")      // Number of Digitals (org.bluetooth.descriptor.number_of_digitals)
let BLE_AI_CharacteristicSenseRGBOutput             = CBUUID(string: "FCB89C40-C603-59F3-7DC3-5ECE444A401B")


/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
class AI_Service : Service {
    let myServiceDirectoryName = "ai_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.

    override func didUpdate(characteristic: CBCharacteristic) {
        var ch = "Unknown"
        var str = "Unknown"
        if characteristic.uuid == BLE_AI_CharacteristicDigital {
            str = "\(characteristic.int8Value() ?? 0)"
            self.manager!.bleString[BLE_AI_CharacteristicDigital] = str
            ch = "Digital"
        } else if characteristic.uuid == BLE_AI_CharacteristicSenseRGBOutput {
            str = "\(characteristic.int8Value() ?? 0)"
            self.manager!.bleString[BLE_AI_CharacteristicSenseRGBOutput] = str
            ch = "RGB output"
        }
        
        os_log("didUpdateValueFor   %{public}s: %{public}s", log: OSLog.ble1, type:.info, ch, str)
//        diagnostics(str)
        
        self.manager!.bleString[BLE_AI_ServiceAutomationIo] = str
    }
}


extension BluetoothManager {

    public func initAutomationIo(_ manager: BluetoothManager) {
        let service = AI_Service()
        service.manager = manager
        soiDict[BLE_AI_ServiceAutomationIo] = service
        
        // Add to BLE strings.
        bleString[BLE_AI_CharacteristicDigital]                 = "0"
        bleString[BLE_AI_CharacteristicPresentationFormat]      = "0"
        bleString[BLE_AI_CharacteristicNumberOfDigitals]        = "0"
        bleString[BLE_AI_CharacteristicSenseRGBOutput]          = "0"
        bleString[BLE_AI_ServiceAutomationIo]                   = "0"

        // Add to UUID strings
        uuidDictionary[BLE_AI_ServiceAutomationIo]              = "Automation IO (1815)"
        uuidDictionary[BLE_AI_CharacteristicDigital]            = "Digital (2A56)"
        uuidDictionary[BLE_AI_CharacteristicPresentationFormat] = "Digital (0x2904)"
        uuidDictionary[BLE_AI_CharacteristicNumberOfDigitals]   = "Digital (0x2909)"
        uuidDictionary[BLE_AI_CharacteristicSenseRGBOutput]     = "RGB output"
   }

}
