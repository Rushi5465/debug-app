//
//  Hub.swift
//  ble
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Phil Kelly. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
let BLE_BP_ServiceBloodPressure                     = CBUUID.init(string: "1810")    // Blood Pressure
let BLE_BP_CharacteristicCuffPressure               = CBUUID.init(string: "2A36")    // Intermediate Cuff Pressure
let BLE_BP_CharacteristicBPMeasurement              = CBUUID.init(string: "2A35")    // Blood Pressure Measurement
let BLE_BP_CharacteristicBPFeature                  = CBUUID.init(string: "2A49")    // Blood Pressure Feature


/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
class BP_Service : Service {
    let myServiceDirectoryName = "bp_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.

    override func didUpdate(characteristic: CBCharacteristic) {
        var ch = "Unknown"
        var str = "Unknown"
        if characteristic.uuid == BLE_BP_CharacteristicCuffPressure {
//            str = characteristic.bpCuffPressure()
            str = ""
            self.manager!.bleString[BLE_BP_CharacteristicCuffPressure] = str
            ch = "Cuff pressure"
        } else if characteristic.uuid == BLE_BP_CharacteristicBPMeasurement {
//            str = characteristic.bpMeasurement()
            str = ""
            self.manager!.bleString[BLE_BP_CharacteristicBPMeasurement] = str
            ch = "BP measurement"
        } else if characteristic.uuid == BLE_BP_CharacteristicBPFeature {
//            str = characteristic.bpFeature()
            str = ""
            self.manager!.bleString[BLE_BP_CharacteristicBPFeature] = str
           ch = "BP Feature"
        }
        os_log("didUpdateValueFor   %{public}s: %{public}s", log: OSLog.ble1, type:.info, ch, str)
//        diagnostics(str)

        self.manager!.bleString[BLE_BP_ServiceBloodPressure] = str
    }
}

extension BluetoothManager {
    
    public func ServiceBloodPressureCharacteristics(_ manager: BluetoothManager) {
        let service = DI_Service()
        service.manager = manager
        soiDict[BLE_BP_ServiceBloodPressure] = service
        
        // Add to BLE strings.
        bleString[BLE_BP_CharacteristicCuffPressure]        = "0"
        bleString[BLE_BP_CharacteristicBPMeasurement]       = "0"
        bleString[BLE_BP_CharacteristicBPFeature]           = "0"
        bleString[BLE_BP_ServiceBloodPressure]              = bleString[BLE_BP_CharacteristicCuffPressure]

        // Add to UUID strings
        uuidDictionary[BLE_BP_ServiceBloodPressure]         = "Blood pressure (1810)"
        uuidDictionary[BLE_BP_CharacteristicCuffPressure]   = "Cuff pressure (2A36)"
        uuidDictionary[BLE_BP_CharacteristicBPMeasurement]  = "BP measurement (2A35)"
        uuidDictionary[BLE_BP_CharacteristicBPFeature]      = "BP feature (2A49)"
    }
    
}


//https://github.com/sputnikdev/bluetooth-gatt-parser/blob/master/src/main/resources/gatt/characteristic/org.bluetooth.characteristic.blood_pressure_measurement.xml
//<InformativeText>
//    <Abstract>
//        The Blood Pressure Measurement characteristic is a variable length structure containing a Flags field, a
//        Blood Pressure Measurement Compound Value field, and contains additional fields such as Time Stamp, Pulse
//        Rate and User ID as determined by the contents of the Flags field.
//    </Abstract>
//</InformativeText>
//<Value>
//    <Field name="Flags">
//        <InformativeText>These flags define which data fields are present in the Characteristic value
//        </InformativeText>
//        <Requirement>Mandatory</Requirement>
//        <Format>8bit</Format>
//        <BitField>
//            <Bit index="0" size="1" name="Blood Pressure Units Flag">
//                <Enumerations>
//                    <Enumeration key="0" value="Blood pressure for Systolic, Diastolic and MAP in units of mmHg"
//                                 requires="C1"/>
//                    <Enumeration key="1" value="Blood pressure for Systolic, Diastolic and MAP in units of kPa"
//                                 requires="C2"/>
//                </Enumerations>
//            </Bit>
//            <Bit index="1" size="1" name="Time Stamp Flag">
//                <Enumerations>
//                    <Enumeration key="0" value="Time Stamp not present"/>
//                    <Enumeration key="1" value="Time Stamp present" requires="C3"/>
//                </Enumerations>
//            </Bit>
//            <Bit index="2" size="1" name="Pulse Rate Flag">
//                <Enumerations>
//                    <Enumeration key="0" value="Pulse Rate not present"/>
//                    <Enumeration key="1" value="Pulse Rate present" requires="C4"/>
//                </Enumerations>
//            </Bit>
//            <Bit index="3" size="1" name="User ID Flag">
//                <Enumerations>
//                    <Enumeration key="0" value="User ID not present"/>
//                    <Enumeration key="1" value="User ID present" requires="C5"/>
//                </Enumerations>
//            </Bit>
//            <Bit index="4" size="1" name="Measurement Status Flag">
//                <Enumerations>
//                    <Enumeration key="0" value="Measurement Status not present"/>
//                    <Enumeration key="1" value="Measurement Status present" requires="C6"/>
//                </Enumerations>
//            </Bit>
//            <ReservedForFutureUse index="5" size="1"/>
//            <ReservedForFutureUse index="6" size="1"/>
//            <ReservedForFutureUse index="7" size="1"/>
//        </BitField>
//    </Field>
//    <Field name="Blood Pressure Measurement Compound Value - Systolic (mmHg)">
//        <InformativeText>C1: Field exists if the key of bit 0 of the Flags field is set to 0</InformativeText>
//        <Requirement>C1</Requirement>
//        <Format>SFLOAT</Format>
//        <Unit>org.bluetooth.unit.pressure.millimetre_of_mercury</Unit>
//    </Field>
//    <Field name="Blood Pressure Measurement Compound Value - Diastolic (mmHg)">
//        <InformativeText>C1: Field exists if the key of bit 0 of the Flags field is set to 0</InformativeText>
//        <Requirement>C1</Requirement>
//        <Format>SFLOAT</Format>
//        <Unit>org.bluetooth.unit.pressure.millimetre_of_mercury</Unit>
//    </Field>
//    <Field name="Blood Pressure Measurement Compound Value - Mean Arterial Pressure (mmHg)">
//        <InformativeText>C1: Field exists if the key of bit 0 of the Flags field is set to 0</InformativeText>
//        <Requirement>C1</Requirement>
//        <Format>SFLOAT</Format>
//        <Unit>org.bluetooth.unit.pressure.millimetre_of_mercury</Unit>
//    </Field>
//    <Field name="Blood Pressure Measurement Compound Value - Systolic (kPa)">
//        <InformativeText>C2: Field exists if the key of bit 0 of the Flags field is set to 1</InformativeText>
//        <Requirement>C2</Requirement>
//        <Format>SFLOAT</Format>
//        <Unit>org.bluetooth.unit.pressure.pascal</Unit>
//        <DecimalExponent>3</DecimalExponent>
//    </Field>
//    <Field name="Blood Pressure Measurement Compound Value - Diastolic (kPa)">
//        <InformativeText>C2: Field exists if the key of bit 0 of the Flags field is set to 1</InformativeText>
//        <Requirement>C2</Requirement>
//        <Format>SFLOAT</Format>
//        <Unit>org.bluetooth.unit.pressure.pascal</Unit>
//        <DecimalExponent>3</DecimalExponent>
//    </Field>
//    <Field name="Blood Pressure Measurement Compound Value - Mean Arterial Pressure (kPa)">
//        <InformativeText>C2: Field exists if the key of bit 0 of the Flags field is set to 1</InformativeText>
//        <Requirement>C2</Requirement>
//        <Format>SFLOAT</Format>
//        <Unit>org.bluetooth.unit.pressure.pascal</Unit>
//        <DecimalExponent>3</DecimalExponent>
//    </Field>
//    <Field name="Time Stamp">
//        <InformativeText>C3: Field exists if the key of bit 1 of the Flags field is set to 1</InformativeText>
//        <Requirement>C3</Requirement>
//        <Reference>org.bluetooth.characteristic.date_time</Reference>
//    </Field>
//    <Field name="Pulse Rate">
//        <InformativeText>C4: Field exists if the key of bit 2 of the Flags field is set to 1</InformativeText>
//        <Requirement>C4</Requirement>
//        <Format>SFLOAT</Format>
//        <Unit>org.bluetooth.unit.period.beats_per_minute</Unit>
//    </Field>
//    <Field name="User ID">
//        <InformativeText>C5: Field exists if the key of bit 3 of the Flags field is set to 1</InformativeText>
//        <Requirement>C5</Requirement>
//        <Format>uint8</Format>
//        <Enumerations>
//            <Enumeration key="255" value="Unknown User"/>
//            <DefinedByServiceSpecification start="0" end="254"/>
//        </Enumerations>
//    </Field>
//    <Field name="Measurement Status">
//        <InformativeText>C6: Field exists if the key of bit 4 of the Flags field is set to 1</InformativeText>
//        <Requirement>C6</Requirement>
//        <Format>16bit</Format>
//        <BitField>
//            <Bit index="0" size="1" name="Body Movement Detection Flag">
//                <Enumerations>
//                    <Enumeration key="0" value="No body movement"/>
//                    <Enumeration key="1" value="Body movement during measurement"/>
//                </Enumerations>
//            </Bit>
//            <Bit index="1" size="1" name="Cuff Fit Detection Flag">
//                <Enumerations>
//                    <Enumeration key="0" value="Cuff fits properly"/>
//                    <Enumeration key="1" value="Cuff too loose"/>
//                </Enumerations>
//            </Bit>
//            <Bit index="2" size="1" name="Irregular Pulse Detection Flag">
//                <Enumerations>
//                    <Enumeration key="0" value="No irregular pulse detected"/>
//                    <Enumeration key="1" value="Irregular pulse detected"/>
//                </Enumerations>
//            </Bit>
//            <Bit index="3" size="2" name="Pulse Rate Range Detection Flags">
//                <Enumerations>
//                    <Enumeration key="0" value="Pulse rate is within the range"/>
//                    <Enumeration key="1" value="Pulse rate exceeds upper limit"/>
//                    <Enumeration key="2" value="Pulse rate is less than lower limit"/>
//                    <ReservedForFutureUse1 start1="3" end1="3"/>
//                </Enumerations>
//            </Bit>
//            <Bit index="5" size="1" name="Measurement Position Detection Flag">
//                <Enumerations>
//                    <Enumeration key="0" value="Proper measurement position"/>
//                    <Enumeration key="1" value="Improper measurement position"/>
//                </Enumerations>
//            </Bit>
//            <ReservedForFutureUse index="6" size="1"/>
//            <ReservedForFutureUse index="7" size="1"/>
//            <ReservedForFutureUse index="8" size="1"/>
//            <ReservedForFutureUse index="9" size="1"/>
//            <ReservedForFutureUse index="10" size="1"/>
//            <ReservedForFutureUse index="11" size="1"/>
//            <ReservedForFutureUse index="12" size="1"/>
//            <ReservedForFutureUse index="13" size="1"/>
//            <ReservedForFutureUse index="14" size="1"/>
//            <ReservedForFutureUse index="15" size="1"/>
//        </BitField>
//    </Field>
//</Value>
//<Note>
//    The fields in the above table are in the order of LSO to MSO. Where LSO = Least Significant Octet and MSO =
//    Most Significant Octet and MAP = Mean Arterial Pressure.
//</Note>
