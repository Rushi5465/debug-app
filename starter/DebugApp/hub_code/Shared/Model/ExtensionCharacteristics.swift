//
//  ExtensionCharacteristic.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log


// MARK: - CBCharacteristic General Extensions

extension CBCharacteristic {

    func stringValue() -> String {
        guard self.value != nil else { return "" }
        guard self.value!.count >= 1 else { return "" }
        let str = String(decoding: self.value!, as: UTF8.self)
        return str
    }

    func int8Value(_ offset: Int = 0) -> Int8? {
        guard self.value != nil else { return nil }
        guard self.value!.count >= 1 else { return nil }
        return Int8(self.value![0+offset])
    }
    
    func uint8Value(_ offset: Int = 0) -> UInt8? {
        guard self.value != nil else { return nil }
        guard self.value!.count >= 1 else { return nil }
        return self.value![0+offset]
    }
    
    func int16Value(_ offset: Int = 0) -> Int16? {
        guard self.value != nil else { return nil }
        guard self.value!.count >= 2 else { return nil }
        let value = Int16(self.value![0+offset]) + (Int16((self.value![1+offset])) << 8)
        return value
    }
    
    func uint16Value(_ offset: Int = 0) -> UInt16? {
        guard self.value != nil else { return nil }
        guard self.value!.count >= 2 else { return nil }
        let value = UInt16(self.value![0+offset]) + (UInt16((self.value![1+offset])) << 8)
        return value
    }
    
    func int32Value(_ offset: Int = 0) -> Int32? {
        guard self.value != nil else { return nil }
        guard self.value!.count >= 4 else { return nil }
        let value = Int32(self.value![0+offset]) + (Int32((self.value![1+offset])) << 8) + (Int32((self.value![2+offset])) << 16) + (Int32((self.value![3+offset])) << 24)
        return value
    }
    
    func uint32Value(_ offset: Int = 0) -> UInt32? {
        guard self.value != nil else { return nil }
        guard self.value!.count >= 4 else { return nil }
        let value = UInt32(self.value![0+offset]) + (UInt32((self.value![1+offset])) << 8) + (UInt32((self.value![2+offset])) << 16) + (UInt32((self.value![3+offset])) << 24)
        return value
    }
    
    /**
     Indicate whether a characteristic value is printable.
     
     Printable characters are alphanumeric, punctuation, and spaces.
     - Returns: False if the characteristic value contains any non-printable characters, otherwise true.
    */
    func isText() -> Bool {
        guard let data = self.value else { return false }

        // Check characters to see if they are not-printable.
        for byte in data {
            if isalnum(Int32(byte)) == 0 &&
                ispunct(Int32(byte)) == 0 &&
                isspace(Int32(byte)) == 0 {
                return false
            }
        }
        // All of the characters are printable.
        return true
    }

}
