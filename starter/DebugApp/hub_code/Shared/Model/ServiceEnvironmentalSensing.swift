//
//  ServiceEnvironmentalSensing.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
// UUIDs for the service and associated characteristics.
let BLE_ES_ServiceEnvironmentalSensing              = CBUUID.init(string: "181A")       // Environmental Sensing (ES)
let BLE_ES_CharacteristicTemperature                = CBUUID.init(string: "2A6E")
let BLE_ES_CharacteristicPressure                   = CBUUID.init(string: "2A6D")
let BLE_ES_CharacteristicHumidity                   = CBUUID.init(string: "2A6F")
let BLE_ES_CharacteristicUvIndex                    = CBUUID.init(string: "2A76")
let BLE_ES_CharacteristicAmbientLightReact          = CBUUID.init(string: "C8546913-BFD9-45EB-8DDE-9F8754F4A32E")   // Thunderboard
let BLE_ES_CharacteristicControlPoint               = CBUUID.init(string: "C8546913-BF03-45EB-8DDE-9F8754F4A32E")   // Thunderboard


/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
class ES_Service : Service {
    let myServiceDirectoryName = "es_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.

    override func didUpdate(characteristic: CBCharacteristic) {
        var ch = ""
        var str = ""
        switch characteristic.uuid {
        case BLE_ES_CharacteristicTemperature:
            ch = "Temperature"
            let value = characteristic.esTemperature()
            str = String(format: "%0.2f °F", value)
            self.manager!.bleString[BLE_ES_CharacteristicTemperature] = str
        case BLE_ES_CharacteristicHumidity:
            ch = "Humidity"
            let value = characteristic.esHumidity()
            str = String(format: "%0.2f%%", value)
            self.manager!.bleString[BLE_ES_CharacteristicHumidity] = str
        case BLE_ES_CharacteristicUvIndex:
            ch = "UV Index"
            let value = characteristic.esUvIndex()
            str = "\(value)"
            self.manager!.bleString[BLE_ES_CharacteristicUvIndex] = str
        case BLE_ES_CharacteristicAmbientLightReact:
            ch = "Ambient Light"
            let value = characteristic.esAmbientLightReact()
            str = String(format: "%0.2f Lux", value)
            self.manager!.bleString[BLE_ES_CharacteristicAmbientLightReact] = str
        case BLE_ES_CharacteristicPressure:
            ch = "Pressure"
            let value = characteristic.esPressure()
            str = String(format: "%0.3f", value)
            self.manager!.bleString[BLE_ES_CharacteristicPressure] = str
        default:
            return
        }

        self.manager!.bleString[BLE_ES_ServiceEnvironmentalSensing] = str
        str = str.replacingOccurrences(of: "\n", with: ", ")
        os_log("didUpdateValueFor   %{public}s: %{public}s", log: OSLog.ble1, type:.info, ch, str)
    }
}


extension BluetoothManager {
    
    public func initEnvironmentalSensing(_ manager: BluetoothManager) {
        let service = ES_Service()
        service.manager = manager
        soiDict[BLE_ES_ServiceEnvironmentalSensing] = service
        
        //Add result strings with default settings.
        bleString[BLE_ES_CharacteristicTemperature]             = "0 °F"
        bleString[BLE_ES_CharacteristicPressure]                = "0"
        bleString[BLE_ES_CharacteristicHumidity]                = "0%"
        bleString[BLE_ES_CharacteristicUvIndex]                 = "0%"
        bleString[BLE_ES_CharacteristicAmbientLightReact]       = "0 Lux"
        bleString[BLE_ES_ServiceEnvironmentalSensing]           = bleString[BLE_ES_CharacteristicTemperature]

        // Add descriptive strings for UUIDs
        uuidDictionary[BLE_ES_ServiceEnvironmentalSensing]      = "Environmental Sensing (181A)"
        uuidDictionary[BLE_ES_CharacteristicTemperature]        = "Temperature (2A6E)"
        uuidDictionary[BLE_ES_CharacteristicPressure]           = "Pressure (2A6D)"
        uuidDictionary[BLE_ES_CharacteristicHumidity]           = "Humidity (2A6F)"
        uuidDictionary[BLE_ES_CharacteristicUvIndex]            = "UV Index (2A76)"
        uuidDictionary[BLE_ES_CharacteristicAmbientLightReact]  = "Ambient light"
        uuidDictionary[BLE_ES_CharacteristicControlPoint]       = "Control point"
    }

}


extension CBCharacteristic {

    /**
     Temperature
     
     Pertains to characteristic.uuid = "2A6E".
     
     - Returns: 74.82
     */
    func esTemperature() -> Double {
        guard self.value != nil else { return 0.0 }
        guard self.value!.count >= 1 else { return 0.0 }
        // Temperature is a 16 bit little endian value. Units are 0.01 °C.
        var temperature = Double(self.int16Value()!)
        temperature /= 100

        // Convert to Fahrenheit.
        temperature = temperature * 9.0/5.0 + 32.0
        return temperature
    }

    /**
     Pressure
     
     Pertains to characteristic.uuid = "2A6D".
     
     - Returns: 10.257
     */
    func esPressure() -> Double {
        guard self.value != nil else { return 0.0 }
        guard self.value!.count >= 1 else { return 0.0 }
        // Pressure is a 16 bit little endian value. Units are 0.001.
        var pressure = Double(self.int16Value()!)
        pressure /= 1000
        return pressure
    }

    /**
     Humidity
     
     Pertains to characteristic.uuid = "2A6F".
     
     - Returns: 10.25 (percent)
     */
    func esHumidity() -> Double {
        guard self.value != nil else { return 0.0 }
        guard self.value!.count >= 1 else { return 0.0 }
        // Humidity is a 16 bit little endian value. Units are 0.01%.
        var humidity = Double(self.int16Value()!)
        humidity /= 100
        return humidity
    }

    /**
     UV Index
     
     Pertains to characteristic.uuid = "2A76".
     
     - Returns: 10 (unitless)
     */
    func esUvIndex() -> UInt8 {
        guard self.value != nil else { return 0 }
        guard self.value!.count >= 1 else { return 0 }
        // UV index is an 8 bit little endian value. Units are 1.
        let index = self.uint8Value()!
        return index
    }

    /**
     Ambient light react
     
     Pertains to characteristic.uuid = "C8546913-BFD9-45EB-8DDE-9F8754F4A32E".
     
     - Returns: 10.25  (Lux: lumens falling on a surface)
     */
    func esAmbientLightReact() -> Double {
        // Ambient light is a 16 bit little endian value. Units are 0.01 Lux.
        var light = Double(self.int16Value()!)
        light /= 100
        return light
    }

}
