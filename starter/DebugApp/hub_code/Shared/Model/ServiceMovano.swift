//
//  ServiceMovano.swift
//  Hub
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
// UUIDs for the service and associated characteristics.
let BLE_MV_ServiceMovanoSensor                         = CBUUID.init(string: "B56C04EA-6CBA-4DD4-83B9-7F3056A1BAC5")    // Service specific to a Movano sensor
////let BLE_MV_CharacteristicMovanoControl                 = CBUUID.init(string: "FFC32EC7-CD20-467F-961E-B8B810F28309")    // Control Plane
////let BLE_MV_CharacteristicMovanoData                    = CBUUID.init(string: "09A4B32C-AF3B-4938-ADFC-69C922A86401")    // Data Plane
let BLE_MV_CharacteristicMovanoControl                 = CBUUID.init(string: "09A4B32C-AF3B-4938-ADFC-69C922A86401")    // Control Plane
let BLE_MV_CharacteristicMovanoData                    = CBUUID.init(string: "FFC32EC7-CD20-467F-961E-B8B810F28309")    // Data Plane

//let BLE_MV_ServiceMovanoSensor                         = CBUUID.init(string: "129bda25-3a33-418d-92f0-4138fefb54d1")    // Service specific to a Movano sensor
//let BLE_MV_CharacteristicMovanoControl                 = CBUUID.init(string: "24625142-cc99-494f-98ac-dd63665e23c8")    // Control Plane
//let BLE_MV_CharacteristicMovanoData                    = CBUUID.init(string: "c2601536-7467-48d1-beca-d319a31b571d")    // Data Plane


// Hub code related members.
public var hubVersionKnown = false                             // Indicates whether the sensor's firmware version has been determined. This is required before any control plane communications occurs.
//public let hubSensorCommandNumberStart = -1                    // Value to set hubSensorCommandNumber to when starting to read sensor parameters.
//public let hubSensorCommandNumberDone = -2                     // Value to set hubSensorCommandNumber to when all commands to read sensor parameters have been handled.
public var hubSensorCommandNumber = -1                         // Number indicating which sensor parameters command in a list of commands was last sent. Initialize with hubSensorCommandNumberStart.
enum hubSensorCommandNumberEnum: Int {
    case Start = -1
    case Done = -2
}

/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
class MV_Service : Service {
    let myServiceDirectoryName = "mv_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.

    override func didUpdate(characteristic: CBCharacteristic) {
        switch characteristic.uuid {
        case BLE_MV_CharacteristicMovanoControl:
            didUpdateMovanoControl(characteristic: characteristic)
        case BLE_MV_CharacteristicMovanoData:
            didUpdateMovanoData(characteristic: characteristic)
        default:
            return
        }
    }

    func didUpdateMovanoControl(characteristic: CBCharacteristic) {
        var str: String?
        if characteristic.isText() {
            str = "value (\(String(describing: characteristic.value?.count))): " + String(decoding: characteristic.value!, as: UTF8.self)
            self.manager!.bleString[BLE_MV_CharacteristicMovanoControl] = String(decoding: characteristic.value!, as: UTF8.self)
        } else {
            str = "value (\(String(describing: characteristic.value?.count))): \(characteristic.value!)"
        }
        os_log("didUpdateValueFor   %{public}s", log: OSLog.ble1, type:.info, str!)
//        self.diagnostics = str! + self.diagnostics + "\n"
        
//        let rv = processControlResponse(value: characteristic.value!)
//        if rv != 0 && characteristic.value!.count != 0 {
//            let str = String(decoding: characteristic.value!, as: UTF8.self)
//            os_log("didUpdateValueFor   not handled: %{public}s", log: OSLog.ble1, type:.info, str)
//        }
        
        if hubSensorCommandNumber != hubSensorCommandNumberEnum.Done.rawValue {
            // There are additional sensor commands in our list of commands to query sensor parameters.
            print("hubSensorCommandNumber = \(hubSensorCommandNumber)");
//            hubRequestSensorParameters()
        }
        
        // Diplay the control response.
        self.chText = "Control"
        self.manager!.bleString[BLE_MV_ServiceMovanoSensor] = self.manager!.bleString[BLE_MV_CharacteristicMovanoControl]
        displayData(characteristic: characteristic)
    }
    
    public func didUpdateMovanoData(characteristic: CBCharacteristic) {
        guard characteristic.value != nil else { return }
        guard characteristic.value!.count > 0 else { return }

        // Display bytes received.
        let str = characteristic.mvDataString()
        os_log("didUpdateMovanoData   %{public}s", log: OSLog.ble1, type:.info, str)
//        diagnostics(str)
//            print(str)
        // Save the sensor data.
        var data = characteristic.value!.map {UInt8($0)}
        print(self.manager!.bleString[BLE_MV_CharacteristicMovanoData] as Any)

        // Diplay the data.
        self.chText = "Data"
        self.manager!.bleString[BLE_MV_ServiceMovanoSensor] = self.manager!.bleString[BLE_MV_CharacteristicMovanoData]
        displayData(characteristic: characteristic)

        return
    }
    
//    override func didDiscoverCharacteristicAction(characteristic: CBCharacteristic) {
//        if characteristic.uuid == BLE_MV_CharacteristicMovanoControl {
//            // Always listen for control messages.
//			self.manager!.connectedPeripheral?.peripheral?.setNotifyValue(true, for: characteristic)
//
//            // Initialize hub components.
//            hubInit()
//            // Indicate that we are using the BLE connection for control plane and data plane communications.
//            hubSetSensorIO(BleControlDevice, BleDataDevice)
//
//            // Read system parameters.
//            hubReadSystemParameters()
//
//            // Set the system parameter's sessions directory.
//            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)  // find all possible documents directories for this user.
//            let str = paths[0].path + "/" + myServiceDirectoryName
//            hubChangeSystemParameter("home", str)
//
//            // Make request to get sensor parameters.
//            // Initialize the counter in our list of sensor commands to send.
//            hubSensorCommandNumber = hubSensorCommandNumberEnum.Start.rawValue
//            // Send commands to query sensor parameters.
//            hubRequestSensorParameters()
//        }
//    }

    override func notifyStart(characteristic: CBCharacteristic) {
        notifyStart1(characteristic: characteristic, serviceDirName: myServiceDirectoryName, sessionDirName: &mySessionDirectoryName, fCount: &myFileCount, nCount: &myCharacteristicsNotifying)
        hubStart()
    }

    override func notifyStop(characteristic: CBCharacteristic) {
        hubStop()
        notifyStop1(characteristic: characteristic, sessionDirName: &mySessionDirectoryName, fCount: &myFileCount, nCount: &myCharacteristicsNotifying)
    }


    /**
     Send a sensor control plane command.
     
     Sent is a wrapped string: for a command *cmd*, sent is a string $*cmd*#cs where cs represents a 2-byte check sum expressed using 2 hex characters.
     For example, the command "V,g" is sent as "$V,g#e9".
     
     - Parameter command: The command to send (e.g., "V,g")
     */
    public func hubSendControl(command: String) {
        guard command.count > 0 else { return }
        guard self.manager!.chDict[BLE_MV_CharacteristicMovanoControl] != nil else { return }
                
        var checksum: Int = 0

        // Compute the checksum and its 2-byte representation.
        for char in command {
            checksum += Int(char.asciiValue!)
        }
        let cs1: UInt8 = UInt8(checksum & 0b0000000011110000)  >> 4
        let cs2: UInt8 = UInt8(checksum & 0b0000000000001111)
        
        // Form $*command*#cs.
        let buffer = "$" + command + "#" + String(format:"%x",cs1) + String(format:"%x",cs2)

        // Write the command.
        self.manager!.writeValue(uuid: BLE_MV_CharacteristicMovanoControl, data:buffer.data(using: .utf8)!)
        // Provide time for the write to happen.
        usleep(10000)
    }
    
    /**
     Get both system and sensor parameters.
     
     System parameters are read from a user defined file.
     A series of commands are sent to the sensor to querry its parameters.
     */
    public func hubRequestParameters() {
        // Get user system  parameters.
//        self.hubReadSystemParameters()

        // Send commands requesting sensor parameters.
        // See peripheral(_:didUpdateValueFor:error:) for responses.
        hubSensorCommandNumber = hubSensorCommandNumberEnum.Start.rawValue;
//        hubRequestSensorParameters()
    }
    
    /**
     Read system (user specified) parameters.
     */
//    public func hubReadSystemParameters() {
//        // Retrieve and set any user system and sensor parameters.
////        let argsfile = "./args/args/args.txt"
//        let argsfile = "/Users/phil/Library/Containers/movano.hub-macOS/Data/Documents/"
//        let rv = hubGetUserParameters(argsfile);
//        if rv != 0 {
//            let str = "-->  Unable to read USER parameters in file \(argsfile)"
//            os_log("%{public}s", log: OSLog.ble1, type:.info, str)
////                diagnostics(str)
//        }
//    }

    //     MARK: - Hub Internal Commands
    /**
     Request sensor parameters.
     
     A series of commands are defined to query sensor parameters. Each time this routine is called a next command is sent.
     The actual commands are firmware dependent. Refer to sensorConfigurationCommand().
     
     - Note: The Hub variable *hubSensorCommandNumber* is an index that tracks which command to send. A *version* command is always first; it is assigned number -1. Remaining commands are firmware build specific and are numbered 0, 1, 2, 3, ... *hubSensorCommandNumber* is set to -2 when there are no longer any commands to send.
     */
//    public func hubRequestSensorParameters() {
//        // The hubSensorCommandNumber variable specifies the current state:
//        // -1 => Start querying sensor parameters
//        //  n => Issue the nth command, n = 0, 1, 2, ...
//        // -2 => All of the commands to querry sensor parameters have been issued; no more to do.
//
//        if hubSensorCommandNumber == hubSensorCommandNumberEnum.Done.rawValue {
//            return
//        }
//
//        // Start by issueing a version command ("V,g").
//        if hubSensorCommandNumber == hubSensorCommandNumberEnum.Start.rawValue {
//            // First issue a version command.
//            self.hubSendControl(command: "V,g")
//            // Next time start with the first command in the list of commands for the firmware in use.
//            hubSensorCommandNumber = 0
//            return
//        }
//
//        // Issue the next command.
//        let psSensorParams: UnsafeMutablePointer<SensorParams>? = hubSensorParams();
//        var cptr = [Int8](repeating: 0, count: Int(SCP_CMD_BUFFER_MAX))
//        if sensorConfigurationCommand(psSensorParams, Int32(hubSensorCommandNumber), &cptr) == 0 {
//            let str = String(cString: cptr)
//            self.hubSendControl(command: str)
//            hubSensorCommandNumber += 1
//        } else {
//            // No more commands to send.
//            hubSensorCommandNumber = hubSensorCommandNumberEnum.Done.rawValue
//        }
//    }

    /**
     Handle a hub control plane response.
     
     Handling a response includes removing message wrappers, verifying the checksum, and updating sensor parameters based on the response message.
     
     The response is a string:
     - ,$*response-message*#cs\n.
     
     The format is
     - ',' indicated here as a comma, but more generally it represents one or more miscellaneous characters that may precede the response;
     - '$' denotes the beginning of a response;
     - *response-message* a variable length message; the response message likely has a trailing comma, which is removed if present;
     - '#' marks the end of the response;
     - 'cs' represents a 2-byte checksum;
     - '\n' represents any miscellaneous characters that may follow the response.
     
     The response is parsed and appropriate actions are taken based on the contents of the response.
     
     - Note: **Parsing the response message first requires that a response to a version command has been processed**. (This only needs to happen once after class initialization.) Responses provided before the version command response is processed are ignored.
     
     - Parameter value: The control characteristic value sent from the peripheral to the client.
     - Returns: 0 on success, otherwise an error code.
     */
//    public func processControlResponse(value: Data) -> Int {
//        guard value.count > 0 else { return -1 }
//        let psSensorParams: UnsafeMutablePointer<SensorParams>? = hubSensorParams();
//        var cptr: [Int8] = value.map{Int8($0)}
//
//        var rv: Int32 = 0
//
//        // Strip off outer characters to get *response-message* and check the checksum.
//        rv = scpGetPacket2(&cptr)
//        // The return value should be 0 if the message is a response to a command (i.e., the checksum is correct).
//        guard rv == 0 else { return Int(rv) }
//
//        let response = String(cString: cptr)
//
//        // We can't interpret responses until we know the firmware version.
//        if hubVersionKnown == false {
//            // Look for the response to a version command.
//            // Parsing with sensorParseResponse requires we know the firmware release; Extract the major and minor firmware release numbers.
//            var firmwareMajor: Int32 = 0
//            var firmwareMinor: Int32 = 0
//            rv = scpVersion(response, &firmwareMajor, &firmwareMinor);
//
//            // Stop if this wasn't a response to a version command, or the response couldn't be parsed.
//            guard rv == 0 else { return Int(rv) }
//
//            psSensorParams!.pointee.iFirmwareMajor = firmwareMajor
//            psSensorParams!.pointee.iFirmwareMinor = firmwareMinor
//            hubVersionKnown = true
//
//            // Specify the commands/responses to use based on the version information.
//            sensorFirmwareCommands(psSensorParams)
//        }
//
//        rv = sensorParseResponse(psSensorParams, response, "")
//        return Int(rv)
//    }
    
    /**
     Processes a hub data plane response.
     
     The response is a binary chunk of data.
     Validated data is accumulated and then saved to a file.
     - Parameter value: The data characteristic value sent from the peripheral to the client.
     - Returns: 0 on success, otherwise an error code.
     */
//    public func processDataResponse(value: Data?) -> Int {
//        guard value != nil else {
//            return -1
//        }
//        guard value!.count > 0 else {
//            return -2
//        }
//
//        var data = value!.map {UInt8($0)}   // The data is binary. Conversion to a string won't work.
//        let rv = bleSaveSensorData(&data, UInt(value!.count))
//        return Int(rv);
//    }

}

//     MARK: - Hub User Commands
    
    /**
     Start collecting data from a sensor.
     
     Create a new session and send a BLE notification so that the peripheral will begin forwarding data plane sensor data.
     */
    public func hubStart() {
//        guard connectedPeripheral != nil, coiDictionary[BLE_CharacteristicMovanoData]?.characteristic != nil else { return }
        
//        bleSetupSensorData();
//        connectedPeripheral!.setNotifyValue(true, for: (coiDictionary[BLE_CharacteristicMovanoData]?.characteristic!)!)
    }
    
    /**
     Stop collecting data from a sensor.
     
     Send a BLE notification so that the peripheral will stop forwarding data plane sensor data, and close the current session.
     */
    public func hubStop() {
//        guard connectedPeripheral != nil, coiDictionary[BLE_CharacteristicMovanoData]?.characteristic != nil else { return }
        
//        connectedPeripheral!.setNotifyValue(false, for: (coiDictionary[BLE_CharacteristicMovanoData]?.characteristic!)!)
//        bleCloseSensorData();
    }
    
    /**
     Prepare a sensor control plane command for sending
     
     Created is a wrapped string: for a command *cmd*, returned is a string $*cmd*#cs where cs represents a 2-byte check sum expressed using 2 hex characters.
     For example, the command "V,g" is wrapped as "$V,g#e9".
     
     - Parameter command: The command to send (e.g., "V,g")
     */
    public func hubWrapControlCommand(command: String) -> String {
        guard command.count > 0 else { return "" }
        
        var checksum: Int = 0

        // Compute the checksum and its 2-byte representation.
        for char in command {
            checksum += Int(char.asciiValue!)
        }
        let cs1: UInt8 = UInt8(checksum & 0b0000000011110000)  >> 4
        let cs2: UInt8 = UInt8(checksum & 0b0000000000001111)
        
        // Form $*command*#cs.
        let buffer = "$" + command + "#" + String(format:"%x",cs1) + String(format:"%x",cs2)

        return buffer
    }




extension BluetoothManager {

//    public func initMovano(_ manager: BluetoothManager) {
//        let service = MV_Service()
//        service.manager = manager
//        soiDict[BLE_MV_ServiceMovanoSensor] = service
//
//        // Add to BLE strings.
//        bleString[BLE_MV_CharacteristicMovanoData]             = "no frames"
//        bleString[BLE_MV_ServiceMovanoSensor]                  = bleString[BLE_MV_CharacteristicMovanoData]
//
//        // Add to UUID strings
//        uuidDictionary[BLE_MV_ServiceMovanoSensor]             = "Movano Sensor"
//        uuidDictionary[BLE_MV_CharacteristicMovanoControl]     = "Control Plane"
//        uuidDictionary[BLE_MV_CharacteristicMovanoData]        = "Data Plane"
//
//        // Setup the C structure so that C can call the Swift swiftLog function.
//        cSetup(&callbacks)
//    }
    
}


extension CBCharacteristic {

    
    /**
     Data as a string.
     
     Pertains to characteristic.uuid = "A4E649F4-4BE5-11E5-885D-FEFF819CDC9F".
     
     - Returns: "value: (N  bytes) = 0x... ", which is a byte dump of N bytes.
     */
    func mvDataString() -> String {
        guard self.value != nil else { return "" }
        guard self.value!.count >= 1 else { return "" }
        let length = self.value!.count
        let dataMap = self.value!.map { String(format: "%02x", $0) }.joined()
        let str = "value: (\(length) bytes) = 0x\(dataMap)"
        return str
    }
       
}
