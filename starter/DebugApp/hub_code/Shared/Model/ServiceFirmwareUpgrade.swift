//
//  Hub.swift
//  ble
//
//  Created by Phil Kelly on 8/27/20.
//  Copyright © 2020 Phil Kelly. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

// swiftlint:disable all
// UUIDs for the service and associated characteristics.
let BLE_FU_ServiceOTA                               = CBUUID.init(string: "1D14D6EE-FD63-4FA1-BFA4-8F47B42119F0")    // Over the air service
let BLE_FU_CharacteristicFWUpgrade                  = CBUUID.init(string: "F7BF3564-FB6D-4E53-88A4-5E37E0326063")    // Over the air characteristic of firmware upgrades


/**
 Subclass to handle functions for BLE characteristics associated with a service.
 */
class FU_Service : Service {
    let myServiceDirectoryName = "fu_data"          // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/
    var mySessionDirectoryName = ""                 // Path for saved data is topLevel/myServiceDirectoryName/mySessionDirectoryName/, mySessionDirectoryName is a timestamp derived at run time.
    var myFileCount: UInt = 1                       // Data files are topLevel/myServiceDirectoryName/mySessionDirectoryName/n.acq where n = myFileCount = 1, 2, 3, ...
    var myCharacteristicsNotifying = 0              // Flag indicating the number of characteristics simultaneously reporting data. A session is active as long as myCharacteristicsNotifying is greater than 0.

    override func didUpdate(characteristic: CBCharacteristic) {
        var ch = ""
        var str = ""
        switch characteristic.uuid {
        case BLE_FU_CharacteristicFWUpgrade:
            ch = "FW upgrade"
//            str = characteristic.psPowerSourceString()
            str = ""
            self.manager!.bleString[BLE_FU_CharacteristicFWUpgrade] = str
        default:
            return
        }

        self.manager!.bleString[BLE_FU_ServiceOTA] = str
        str = str.replacingOccurrences(of: "\n", with: ", ")
        os_log("didUpdateValueFor   %{public}s: %{public}s", log: OSLog.ble1, type:.info, ch, str)
    }
}



extension BluetoothManager {
 
    public func initFirmwareUpgrade(_ manager: BluetoothManager) {
        let service = FU_Service()
        service.manager = manager
        soiDict[BLE_FU_ServiceOTA] = service
        
        // Add to BLE strings.
        bleString[BLE_FU_CharacteristicFWUpgrade]                  = "unknown"
        bleString[BLE_FU_ServiceOTA]                               = "unknown"

        // Add to UUID strings
        uuidDictionary[BLE_FU_ServiceOTA]                          = "Over the air"
        uuidDictionary[BLE_FU_CharacteristicFWUpgrade]             = "Over the air firmware upgrades"
    }

}
