//
//  hub.h
//
//  Created by Phil Kelly on 5/8/20.
//  Copyright © 2020 Movano. All rights reserved.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows

#ifndef hub_h
#define hub_h

#include "scp.h"
#include "sdp.h"
#include "system.h"
#include "sensor.h"

#include "utils.h"
#ifndef BLE_ONLY
#include "pmap.h"
#endif
#include "sdp.h"
#include "user.h"
#include "session.h"
#include "timer.h"

#if defined(WIN32) || defined(WIN64)
#include <Windows.h>    // Beep(), Sleep(), usleep()
#else
#include <unistd.h>
#endif

typedef void *HANDLE;
struct RunData;

// Structure for all device, sensor, and system parameters.
typedef struct Prams
{
    struct SystemParams sSystemParams;
    struct SensorParams sSensorParams;

    SensorControlPlane  sControlPlane;
    SensorDataPlane     sDataPlane;

//    union DataDevice    sDataDevice;
//    union ControlDevice sControlDevice;

    int                 iUserSettingsValid;
    int                 iFtdiSettingsValid;
    int                 iUartSettingsValid;
    int                 iBleSettingsValid;
        
    HANDLE              hHandle;
    int                 iOffset;
} Prams;

#ifdef __cplusplus
extern "C" {
#endif

    extern int giDebugLevel;

#pragma mark Prototypes - General

    void hubInit(void);
    void hubSetDebugLevel(int iLevel);
    int hubGetDebugLevel(void);
    Prams *hubParams(void);
    SystemParams *hubSystemParams(void);
    SensorParams *hubSensorParams(void);
    SensorControlPlane *hubControlPlane(void);

#pragma mark Prototypes - Parameters

    int hubChangeSystemParameter(const char* pcName, const char* pcValue);
    void hubGetSystemParameterSessions(char* pcSessionsList, int iLength);
    void hubDisplaySystemParameters(const char* pcTitleString, unsigned int uiSystemPrintOptions); // = SYSTEM_ALL);
    void hubDisplaySensorParameters(const char* pcTitleString, SensorID uiSensorPrintOptions); // = SENSOR_STATIC);

#pragma mark Prototypes - Firmware

    int hubControlSendReceive(const char* pcCommand, float fWaitTimeInSeconds); // = 5);
    int hubUpdateSensorFlash(const char* pcFlashFileName, int iWaitTimeInMs);

#pragma mark Prototypes - Configuration

    void hubSetSensorIO(SensorControlDevice eSensorControlDevice, SensorDataDevice eSensorDataDevice);
    int hubGetAllParameters(const char* pcUserFileName);
    int hubGetUserParameters(const char* pcUserFileName);
    int hubGetFtdiParameters(void);
    int hubGetUartParameters(void);
    int hubModifyFtdiDevice(const char* pcNewDescription); // = NULL);

    void hubReportSystemParams(const char* pcTitleString,                       // = NULL,
        unsigned int uiSystemPrintOptions,                  // = SYSTEM_ALL,
        int iNumberOfIdentSpaces,                           // = 2,
        FILE* stream,                                       // = NULL,
        const char* pcEquals,                               // = " = ",
        const char* pcLeft,                                 // = "(",
        const char* pcRight                                 // = ")"
    );
    int hubSetSystemParameters(const char* name, const char* value, unsigned int* puiSystemPrintOptions); // = NULL);

    void hubReportSensorParams(const char* pcTitleString,
        SensorID uiSensorPrintOptions,                      // = SENSOR_STATIC,
        int iNumberOfIdentSpaces,                           // = 2,
        FILE* stream,                                       // = NULL,
        const char* pcEquals,                               // = " = ",
        const char* pcLeft,                                 // = "(",
        const char* pcRight                                 // = ")"
    );
    int hubSetSensorParameters(const char* group, const char* name, const char* value, SensorID* puiSensorPrintOptions); // = NULL);

    void hubDisplayUartParameters(char* pcTitleString); // = NULL);

    void hubDisplayFtdiParameters(const char* pcTitleString); // = NULL);

#pragma mark Prototypes - Sensor Data

    int hubCheckSync(void);
    int hubStartThreads(int iSeconds, struct RunData* psRunData);
    int hubStart(int iSeconds, const char* pcTag);      //int hubStart(int iSeconds = 0, const char *pcTag = NULL);
    void hubStop(void);
    void hubDone(void);
    void hubDataClose(void);
    int hubStartGenerationThreads(int iSeconds, RunData *psRunData);
    int hubStartGeneration(int iSeconds);
    void hubStopGeneration(void);

    void hubDoze(int iNumberOfTimesToSleep, double dTimeBetweenSleepsInSeconds);
#ifdef __APPLE__
    void Beep(int iFrequency, int iDurationInMs);
#endif

#pragma mark Prototypes - Internal

    // Functions used internally. These should not be called directly; refer to hubStart().
    void hubReadDataRepeatedly(void *psRunData);
    void hubReadAndSaveData(void* psRunData);
    void hubStartStopTimer(void* pvSeconds);

#ifdef __cplusplus
}
#endif

#endif /* hub_h */
