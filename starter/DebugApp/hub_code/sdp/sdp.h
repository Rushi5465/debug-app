//
//  sdp.h
//
//  Created by Phil Kelly on 3/19/20.
//  Copyright © 2020 Movano. All rights reserved.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes string function may be unsafe warning on Windows

#ifndef sdp_h
#define sdp_h

struct SensorParams;
typedef void* SDP_Handle;

#ifndef BLE_ONLY
#include "ftdi60x.h"
#endif // BLE_ONLY
#include "ble.h"

#pragma mark Enums

typedef enum SensorDataDevice
{
    UnknownDataDevice = 0,
    FtdiDataDevice = 1,
    BleDataDevice = 2,
    SpiDataDevice = 3,
} SensorDataDevice;

typedef struct SensorDataPlane
{
    SensorDataDevice        eDataDevice;
    union
    {
#ifndef BLE_ONLY
        struct FtdiPrams    sFtdiParams;
#endif // BLE_ONLY
        struct BleParams    sBleParams;
    };
} SensorDataPlane;


#pragma mark Structures

typedef struct CilantroHeader
{
    int     frequency;
    int     frame;
    int     versionMajor;
    int     versionMinor;
    int     versionPatch;
    int     flavor;
} CilantroHeader;

#ifdef __cplusplus
extern "C" {
#endif

#pragma mark Data Plane Prototypes

void sdpClose(SensorDataDevice eDataDevice, SDP_Handle handle);
int sdpRead(SensorDataDevice eDataDevice, SDP_Handle handle, char *pcBuffer, unsigned long ulBufferSize, unsigned long *pulBytesTransferred, double dTimeOutInSeconds);
int sdpSynchronize(int *piOffset, SensorDataDevice eDataDevice, SDP_Handle handle, struct SensorParams *psSensorParams, int iHeaderOptions); // = 1);

#ifdef BLE_ONLY

#pragma mark Enums (BLE_ONLY)

typedef enum SynchronizationSate
{
    SDP_SYNC_NOT_SYNCHRONIZED,
    SDP_SYNC_CHUNK_BOUNDARY,
    SDP_SYNC_FRAME_BOUNDARY,
    SDP_SYNC_TX_BOUNDARY,
    SDP_SYNC_COMPLETE,
} SynchronizationSate;
extern SynchronizationSate geSyncState;
extern SynchronizationSate geSyncStateForCurrentData;
extern unsigned long gulChunkCount;

#pragma mark Data Plane Prototypes (BLE_ONLY)

int sdpSynchronize2(struct SensorParams *psSensorParams, const char *pcBuf, int iHeaderOptions);

#endif // BLE_ONLY

#ifdef __cplusplus
}
#endif

#endif  /* sdp_h */
