//
//  session.h
//  hub
//
//  Created by Phil Kelly on 3/25/20.
//  Copyright © 2020 Movano. All rights reserved.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes string function may be unsafe warning on Windows

#ifndef session_h
#define session_h

#include <stdio.h>

#pragma mark Session Parameters

#define SESSION_DIRECTORY_STRLEN 1024
#define SESSION_TAG_STRLEN 256

#define SESSION_HOME_DIR_STRLEN 1024

typedef struct Session
{
    int     iSession;
    double  dStart;
    int     iNumberOfFramesPerPackage;
    int     iNumberOfBytesPerChunk;
    int     iNumberOfBytesPerFrame;
    int     iNumberOfBytesPerPackage;
    int     iNumberOfChunksPerFrame;
    int     iCollectionDurationSeconds;
    char    acParentDir[SESSION_DIRECTORY_STRLEN];
    char    acSessionTag[SESSION_TAG_STRLEN];
    char    acSessionDir[SESSION_DIRECTORY_STRLEN];
    char    acStatusFile[SESSION_HOME_DIR_STRLEN];
    char    acInfoFile[SESSION_HOME_DIR_STRLEN];
    int     iShowUserDataProgress;
} Session;

typedef struct RunData
{
    void        *pvHandle;
    char        *pcData;
    int         iBufferSize;
    int         iOffset;
    Session     sSession;
} RunData;


typedef struct SessionsListItem
{
    int     iCollectionDurationSeconds;
    char    acSessionTag[SESSION_TAG_STRLEN];           
} SessionsListItem;

struct SensorParams;
struct SystemParams;

#ifdef __cplusplus
extern "C" {
#endif

#pragma mark Session Prototypes

    void sessionPError(int iDisplay, int rv, const char* pcMessage, const char* pcFileName, int iLineNumber);

    int sessionPrepare(struct SystemParams* psSystemParams, struct SensorParams* psSensorParams, SessionsListItem* psSessionListItem, Session* psSession);
    int sessionEnd(Session* psSession);
    int sessionRefactorSessionsString(char* pcSessionsString, int* piNumberOfSessions);
    int sessionGetNumberOfSessions(const char* pcTest, int* piNumberOfSessions);
    int sessionGetSessions(const char* pcSessionsString, SessionsListItem* psSessionCollect[], int* iNumberOfSessions);
    int sessionVerifyHeader(char* pcData, int iNumberOfBytes, int uiChunkSize, int iNumberOfChunksPerFrame, int iNumberOfFramesPerPacket);
    int sessionWriteData(Session* psSession, char* pcData, size_t tNumberOfBytes, int iPackage);

#pragma mark Sessioin Prototypes (Windows)

#if defined(WIN32) || defined(WIN64)
    int strptime(const char* pcString, const char* pcFormat, struct tm* psTm);
    int mkdir(const char* pcString, int x);
#endif

#ifdef __cplusplus
}
#endif

#endif /* session_h */
