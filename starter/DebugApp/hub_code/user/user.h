//
//  user.h
//
//  Created by Phil Kelly on 3/1/20.
//  Copyright © 2020 Movano. All rights reserved.
//

#ifndef user_h
#define user_h

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes string function may be unsafe warning on Windows

struct SystemParams;
struct SensorParams;

#ifdef __cplusplus
extern "C" {
#endif

# pragma mark User Prototypes

	int userGetParams(const char* pcTextFile, struct SystemParams* psSystemParams, struct SensorParams* psSensorParams);

#ifdef __cplusplus
}
#endif

#endif /* user_h */
