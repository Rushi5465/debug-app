//
//  utils.h
//
//  Created by Phil Kelly on 3/23/20.
//  Copyright © 2020 Movano. All rights reserved.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       /// removes unsafe string warnings on Windows

#ifndef utils_h
#define utils_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>     // va_args on Windows

struct Error;

#ifdef __cplusplus
extern "C" {
#endif

#ifndef BLE_ONLY
#define _Nullable
#endif

    extern int giDebugLevel;
	extern FILE * _Nullable gpLogFileStream;       // stream for log messages (stdout, file, etc.)

#pragma mark Utils Prototypes

	//
	// Memory functions
	//
	char* _Nullable reallocstr(char* _Nullable pcBuffer, size_t* _Nullable tMaxSize, size_t tAddSize, size_t tIncrementSize);

	//
	// Print functions
	//
	int lprintf(const char* _Nullable pcFormat, ...);
	int lfprintf(FILE* _Nullable pFile, const char* _Nullable pcFormat, ...);
	int lvprintf(const char* _Nullable pcFormat, ...);
	void lflush(void);
	int dbprintf(int iThreshold, const char* _Nullable pcFormat, ...);
	int pprintf(FILE* _Nullable stream, int iNumberOfIndentSpaces, const char* _Nullable pcComment, const char* _Nullable pcLeft, const char* _Nullable pcRight, const char* _Nullable pcFormat, ...);
	void eprintf(int iDisplay, struct Error err, const char* _Nullable pcMessage, const char* _Nullable pcFileName, int iLineNumber);

	//
	// String functions
	//
	int utilsPathCharacters(char* _Nullable pcString);
	int utilsCombinePaths(char* _Nullable pcDestination, size_t tDestinationLength, char* _Nullable pcPath1, const char* _Nullable pcPath2);

	int utilsReplaceCharacters(char* _Nullable pcStr, char cFind, char cReplace);
	int utilsRemoveCharacters(char* _Nullable pcString, const char* _Nullable pcStrip);
	int utilsReplaceWhiteSpace(char* _Nullable pcStr, char cReplace);
	int utilsRemoveWhiteSpace(char* _Nullable pcStr);
	int utilsRemoveRepeatWhiteSpace(char* _Nullable pcStr);
	int utilsRemoveSurroundingCharacters(char* _Nullable pcStr, char cCharacter);
	int utilsRemoveComments(char* _Nullable pcText, char cDelimiter);
int utilsParseString(char* _Nullable pcString, int* _Nullable iArgc, char*_Nullable * _Nullable pArgv[_Nullable], const char* _Nullable pcBrackets, char cDelimiter, char cQuote);


#ifdef BLE_ONLY
typedef struct cCallbacks {
    void (* _Nonnull swiftLogImpl)(const char * _Nonnull modifier, int level);
} cCallbacks;
extern void cSetup(const cCallbacks * _Nonnull callbacks);

extern void swiftLog(char * _Nonnull pcStr, int level);
#endif // BLE_ONLY

#ifdef __cplusplus
}
#endif

#endif /* utils_h */
