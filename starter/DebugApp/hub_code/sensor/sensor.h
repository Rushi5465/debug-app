//
//  sensor.h
//
//  Created by Phil Kelly on 3/24/20.
//  Copyright © 2020 Movano. All rights reserved.
//

/**
A system consists of a sensor and a procesor to store and process sensor data.

Relevant parameters:
 - SensorParams: parameters associated with the sensor hardware and firmware.
 - SystemParams: parameters associated with processing and output of collected data.
 - UartParams: parameters associated with the CP210x SiliconLabs USB to UART bridge.
 - FtdiParams: parameters associated with the 601Q FTDI USB to FIFO bridge.

*/

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes string function may be unsafe warning on Windows

#ifndef sensor_h
#define sensor_h

#include "sensor_enum.h"            // enums associated with sensor parameters
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdint.h>

struct SensorControlPlane;
typedef uint64_t SensorID;

#pragma mark Sensor Parameters

#define SENSOR_COMMAND_STRLEN       32
#define SENSOR_VERSION_STRLEN       32
#define ANTENNA_ID_STRLEN           32
#define SENSOR_ID_STRLEN            32
#define FW_CILANTRO_REV_STRLEN      32
#define FW_FIRMWARE_REV_STRLEN      32
#define SENSOR_OUTPUT_STRLEN         8
#define SENSOR_MODE_STRLEN          32
#define SENSOR_TITLE_STRLEN         80
#define SENSOR_TIMER_CLOCK_STRLEN    8
#define SENSOR_SELECTED_VCO_STRLEN   8
#define SENSOR_LOW_POWER_STRLEN     32

//
// Sensor Parameters
//

/**
 Sensor configuration parameters are primarily static. They are either defined by the hardware build, or they are defined in flash.
 (Flash may be reconfigured, but the expectation is that this doesn't happen during normal operation.)
 
 Some parameters that the sensor provides are dynamic. For example, parameters reported in a chunk header may be board related, like temperature.
  
 The SensorParams structure is for configuration parameters that are considered to be static.
 The SensorDataHeader structure is for dynamic parameters provided via the chunk header that precedes chunk data.
 */

typedef struct SensorParams
{
    int iNumberOfHeaderBytesPerChunk;
    int iNumberOfBytesPerComplexSample;
    int iNumberOfOutputSamplesPerChannel;
    int iMaxNumberOfAntennasInHardware;
    int iNumberOfChunksPerFrame;
    
    int iNumberOfBitsPerSample;
    int iOutputDataFormatID;

    int iNumberOfDecimatedSamples;
    int iNumberOfDecimatedSamplesSkipped;
    int iAveragingFactor;
    int iDecimationFactor;

    int iFrequencyMap;
    float fRfCarrierOffsetFrequencyHz;
    float fRxBasebandOffsetFrequencyHz;
    float fRxSamplingRateHz;
    float fPllStartingFrequencyHz;
    float fPllStepSizeHz;
    float fPllBandwidthHz;
    
    int iFramesPerSecond;
    
    int iTx1Cycles;
    int iTx2Cycles;
    
    int iPA1Gain;
    int iPA2Gain;
    
    char acOutputPort[SENSOR_OUTPUT_STRLEN];
    char acOutputMode[SENSOR_MODE_STRLEN];

    char acAntennaIdentifier[ANTENNA_ID_STRLEN];
    char acSensorIdentifier[SENSOR_ID_STRLEN];
    char acCilantroRev[FW_CILANTRO_REV_STRLEN] ;
    char acFirmwareRev[FW_FIRMWARE_REV_STRLEN];
    char acICDMessageRev[FW_FIRMWARE_REV_STRLEN];
    char acHeaderRev[FW_FIRMWARE_REV_STRLEN];
    
    
    char acSensorModel[SENSOR_ID_STRLEN] ;
    char acSensorRevision[SENSOR_ID_STRLEN];
    char acSensorSerialNumber[SENSOR_ID_STRLEN];

    char acAntennaModel[SENSOR_ID_STRLEN];
    char acAntennaName[SENSOR_ID_STRLEN];
    char acAntennaSerialNumber[SENSOR_ID_STRLEN];

    
    int iDutyCycleEnable;
    int iPANumber;
    int iPA1State;
    int iPA2State;
    int iPllTnEval;
    int iPllEnd;
    int iPllStart;
    int iPllTnSVal;
    
    int iDrdyTnEval;
    int iDrdyEnd;
    int iDrdyStart;
    int iDrdyTnSVal;
    
    int iPaTnEval;
    int iPaEnd;
    int iPaStart;
    int iPaTnSVal;
    
    int iChunkHeaderVersion;
    
    char acClockSource[SENSOR_TIMER_CLOCK_STRLEN];  // Clock source
    
    int iRfRegPa1;
    int iRfRegAgc1;
    int iRfRegPa2;
    int iRfRegAgc2;
    int iPllDevice;
    int iAdcDevice;
    
    int iRfReg0;
    int iRfReg1;
    int iRfReg2;
    int iRfReg3;
    int iRfReg4;
    int iRfReg5;
    int iRfReg6;
    int iRfReg7;
    int iRfReg8;
    int iRfReg9;
    int iRfReg10;
    int iRfReg11;


    char acSelectedVCO[SENSOR_COMMAND_STRLEN];
    int iVcoIIntegerDivide;
    int iVcoFractionalDivide;
    int iVcoTransmitStatus;
    int iVcoLockStatus;
    
    int iHardwareUnderflow;
    int iHardwareOverflow;
    int iHardware26Timeout;
    int iHardware15Timeout;
    int iHardwarePllPortSwitch;
    int iHardwarePaSwitchFault;
    int iHardwarePowerReductionFault;
    int iHardwareUartChecksum;
    int iHardwareGoClck;
    int iHardwareAMCInit;
    int iHardware26LoadRegister;
    int iHardware15LoadRegister;
    int iHardware26Calibration;
    int iHardware15Calibration;
    int iHardwareRficInit;
    int iHardwareFlashInit;
    int iHardwareFlashRead;
    int iHardwareFlashErase;
    int iHardwareFlashProgram;
    int iHardwareSerialToParallelADC;
    int iHardwareSerialtoParallelPLL;
    
    int iFlashRfReg0;
    int iFlashRfReg1;
    int iFlashRfReg2;
    int iFlashRfReg3;
    int iFlashRfReg4;
    int iFlashRfReg5;
    int iFlashRfReg6;
    int iFlashRfReg7;
    int iFlashRfReg8;
    int iFlashRfReg9;
    int iFlashRfReg10;
    int iFlashRfReg11;

    int iDeviceRfReg0;
    int iDeviceRfReg1;
    int iDeviceRfReg2;
    int iDeviceRfReg3;
    int iDeviceRfReg4;
    int iDeviceRfReg5;
    int iDeviceRfReg6;
    int iDeviceRfReg7;
    int iDeviceRfReg8;
    int iDeviceRfReg9;
    int iDeviceRfReg10;
    int iDeviceRfReg11;
    
    int iRfRegister;
    
    int iDllStatus;
    
    int iAdcDillV1Bypass;
    int iAdcDillV1CouplingEnabled;
    int iAdcDillV1Gain;
    int iAdcDillV1Trim;
    
    int iTemperatureDevice;
    float fTemperature1Value;
    float fTemperature2Value;
    float fTemperature3Value;

    int iFirmwareMajor;
    int iFirmwareMinor;                         
    
    char acLowPowerMode[SENSOR_LOW_POWER_STRLEN];
    int iLowPowerAModeEnabled;
    int iLowPowerMModeEnabled;

} SensorParams;


typedef struct FirmwareVersion
{
    int     versionMajor;
    int     versionMinor;
    int     versionPatch;
    int     versionFormatter;
} FirmwareVersion;

typedef struct SensorHeaderParams
{
    FirmwareVersion     sFirmwareVersion;
    float               fTemperature1;
    float               fTemperature2;
    int                 iOnPA1;
    int                 iOnPA2;
    int                 iDutyCyclesPA1;
    int                 iDutyCyclesPA2;
} SensorHeaderParams;

typedef struct SensorDataHeader
{
    int     frequency;
    int     frame;
    union
    {
        int     temp1;
        int     temp2;
        FirmwareVersion version;
    };
} SensorDataHeader;

typedef enum SensorHeaderKind
{
    SENSOR_HEADER_VERSION           = 1,
    SENSOR_HEADER_INFO,
    SENSOR_HEADER_TEMP,
    SENSOR_HEADER_TEMP1,
    SENSOR_HEADER_TEMP2,
    SENSOR_HEADER_TX_SOURCE,
} SensorHeaderKind;

#ifdef __cplusplus
extern "C" {
#endif

#pragma mark Sensor Prototypes

    void sensorInit(SensorParams* psSensorParams);
    void sensorHeaderInit(SensorHeaderParams* psSensorHeaderParams);
    void sensorHeaderFrequency(char* pcBuf, int* piFrequency);
    void sensorHeaderParams(char* pcBuf, SensorHeaderParams* psSensorHeader, int iChunkSize);
    //void sensorHeaderParams(uint8_t *pcBuf, SensorDataHeader *psCH, int iChunkNumber); // = 0);
    void sensorHeaderParams2(char *pcBuf, int iChunkCounter, SensorHeaderParams *psSensorHeader);
    void sensorReportParams(SensorParams* psSensorParams,
        const char* pcTitleString,
        SensorID uiSensorPrintOptions,      // = SENSOR_STATIC,
        int iNumberOfIdentSpaces,           // = 2,
        FILE* stream,                       // = NULL,
        const char* pcEquals,               // = " = ",
        const char* pcLeft,                 // = "(",
        const char* pcRight                 // = ")"
    );
    void sensorReportParams2(SensorParams* psSensorParams,
        const char* pcTitleString
    );
    void sensorReportParams3(SensorParams* psSensorParams,
        const char* pcTitleString,
        SensorID uiSensorPrintOptions
    );
    void sensorReportParams4(SensorParams* psSensorParams,
        const char* pcTitleString,
        SensorID uiSensorPrintOptions,
        int iNumberOfIdentSpaces
    );
    int sensorSetParameters(SensorParams* psSensorParams, const char* group, const char* name, const char* value, SensorID* puiSensorPrintOptions); // = NULL);

    int sensorConfigurationCommand(SensorParams *psSensorParams, int iCmd, char *pcCommand);
    int sensorControlConfiguration(SensorParams* psSensorParams, struct SensorControlPlane* psControlDevice);
    int sensorFirmwareCommands(SensorParams* psSensorParams);
    int sensorSendReceive(SensorParams* psSensorParams, struct SensorControlPlane* psControlDevice, const char* pcCommand, float fWaitTimeInSeconds);
    int sensorParseResponse(SensorParams* psSensorParams, const char* pcResponse, const char* pcCommand);
    int sensorReceive(SensorParams *psSensorParams, struct SensorControlPlane *psControlDevice, const char *pcCommand, double dWaitTimeInSeconds);

#ifdef __cplusplus
}
#endif

#endif /* sensor_h */
