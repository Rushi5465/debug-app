//
//  scp.h
//
//  Created by Phil Kelly on 1/29/20.
//  Copyright © 2020 Movano. All rights reserved.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes string function may be unsafe warning on Windows

#ifndef scp_h
#define scp_h

#ifndef BLE_ONLY
#include "uart.h"
#endif // BLE_ONLY
#include "ble.h"

// Max number of characters in inbound/outbound buffers.
#define SCP_CMD_BUFFER_MAX          1024
#define SCP_RSP_BUFFER_MAX          4096        

// Global variables associated with writing serial data
extern char gacScpCommandBuffer[SCP_CMD_BUFFER_MAX];
extern unsigned long gulScpCommandBufferLength;
extern char gacScpResponseBuffer[SCP_RSP_BUFFER_MAX];
extern unsigned long gulScpResponseBufferLength;

#define FW_CILANTRO_REV_STRLEN      32
#define FW_FIRMWARE_REV_STRLEN      32
#define FW_COMMAND_STRLEN           32

typedef enum SensorControlDevice
{
    UnknownControlDevice = 0,
    UartControlDevice = 1,
    BleControlDevice = 2,
    SpiControlDevice = 3,
} SensorControlDevice;

typedef struct SensorControlPlane
{
    SensorControlDevice     eControlDevice;
    union
    {
#ifndef BLE_ONLY
        struct UartParams   sUartParams;
#endif // BLE_ONLY
        struct BleParams    sBleParams;
    };
} SensorControlPlane;

typedef struct ProcessCommand
{
    SensorControlPlane *psControlDevice;
//    char                acCommand[FW_COMMAND_STRLEN];
    const char          *pcCommand;
    char                *pcResponse;
    double              dWaitTimeInSeconds;
    int                 returnValue;
} ProcessCommand;


#ifdef __cplusplus
extern "C" {
#endif

#pragma mark Control Plane Device IO and Device Configuration Prototypes
    int scpOpen(SensorControlPlane* psControlDevice);
    int scpClose(SensorControlPlane* psControlDevice);
    int scpGetPacket2(char *pcResponse);
    int scpGetMessage(void);
    void scpWrite(SensorControlPlane* psControlDevice, double dWaitTimeAfterWriteInSeconds); //unsigned int uiTimerInMs); // = 100)
    int scpRead(SensorControlPlane* psControlDevice, double dWaitTimeForReadInSeconds); //unsigned int uiTimerInMs); // = 60)
    int scpDeviceConfiguration(SensorControlPlane* psControlDevice);

    int scpVersion(const char* pcVersionResponse, int* piMajor, int* piMinor);
    int scpFlush(SensorControlPlane* psControlDevice, const char* pcCommand, double dWaitTimeInSeconds);
//    void scpProcessCommand(void* ProcessCommand);
    int scpProcessCommand(SensorControlPlane* psControlDevice, const char* pcCommand, char** pcResponse, double dWaitTimeInSeconds); // = 0.);
//    int scpProcessCommandResponse(SensorControlPlane *psControlDevice, const char *pcCommand, char **pcResponse, double dWaitTimeInSeconds);
    int scpGetResponse(SensorControlPlane *psControlDevice, char **pcResponse, double dWaitTimeInSeconds);
    int scpFlash(int iFirmwareMajor, int iFirmwareMinor, SensorControlPlane* psControlDevice, const char* pcFlashFileName, int iWaitTimeInMs);

#ifdef __cplusplus
}
#endif

#endif /* scp_h */
