//
//  eserrors.h      // Error Structure reporting of Errors.
//
//  Created by Phil Kelly on 3/23/20.
//  Copyright © 2020 Movano. All rights reserved.
//

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows
#pragma warning(disable:4996)       // removes unsafe string warnings on Windows

#ifndef eserrors_h
#define eserrors_h

#pragma mark Error Structure

#define ERROR_CATEGORY_STRLEN 128
#define ERROR_MESSAGE_STRLEN 256

typedef struct Error
{
    char acCategory[ERROR_CATEGORY_STRLEN];     // Category or module
    int iValue;                                 // Error value
    char acMessage[ERROR_MESSAGE_STRLEN];       // Error message
} Error;

#endif /* eserrors_h */
