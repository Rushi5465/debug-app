//
//  pmap.h
//
//  Created by Phil Kelly on 3/5/20.
//  Copyright © 2020 Movano. All rights reserved.
//

#ifndef pmap_h
#define pmap_h

#pragma warning(disable:4068)       // removes unknown pragma warning on Windows

struct SensorParams;

#pragma mark Prototypes

#ifdef __cplusplus
extern "C" {
#endif

	void pmapFtdiSerialNumber(char* pcSerialNumber, struct SensorParams* psSensorParams);
	void pmapDisplaySerialNumber(struct SensorParams* psSensorParams);

	int pmapParseFtdiDescription(char* pcDesc, struct SensorParams* psSensorParams, int iPrintFlag); // = 1);
	int pmapFormFtdiDescription(struct SensorParams* psSensorParams, char* pcDesc);

#ifdef __cplusplus
}
#endif

#endif /* pmap_h */
