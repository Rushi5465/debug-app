//
//  BloodPressureTableViewCell.swift
//  DataUploader
//
//  Created by Sankalp Gupta on 24/03/21.
//

import UIKit

// swiftlint:disable all
class BloodPressureTableViewCell: UITableViewCell {
	
	@IBOutlet weak var timeLabel: MovanoLabel!
	@IBOutlet weak var bloodPressureLabel: MovanoLabel!
	@IBOutlet weak var pulseLabel: MovanoLabel!
	
	var data: BloodPressureReading! {
		didSet {
			addData()
		}
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
	}
	
	func addData() {
		self.bloodPressureLabel.attributedText = self.bloodPressureData()
		self.pulseLabel.attributedText = self.pulseData()
	}
	
	func bloodPressureData() -> NSAttributedString {
		let str = NSMutableAttributedString()
		
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.sfProDisplay.bold(withSize: 24), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.outsideTargetColor]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.sfProDisplay.bold(withSize: 24), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.primaryColor]
		let attrs3 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.sfProDisplay.regular(withSize: 17), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.primaryColor]
		
		str.append(NSMutableAttributedString(string: "\(self.data.systolic)", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
		str.append(NSMutableAttributedString(string: "/", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		str.append(NSMutableAttributedString(string: "\(self.data.diastolic)", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
		str.append(NSMutableAttributedString(string: "\n mm/Hg", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs3)))
		
		return str
		
	}
	
	func pulseData() -> NSAttributedString {
		let str = NSMutableAttributedString()
		
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.sfProDisplay.semiBold(withSize: 17), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textTitleColor]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.sfProDisplay.regular(withSize: 17), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.valueTitleColor]
		
		str.append(NSMutableAttributedString(string: "\(self.data.pulse)", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
		str.append(NSMutableAttributedString(string: "\n bpm", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		
		return str
		
	}
}
