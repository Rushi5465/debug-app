//
//  BloodPressureWebServiceProtocol.swift
//  DataUploader
//
//  Created by Sankalp Gupta on 01/04/21.
//

import Foundation

// swiftlint:disable all
protocol BloodPressureWebServiceProtocol {
	
	func getBloodPressureDataByTimeStamp(for patient: Patient, timestamp: Double, completionHandler: @escaping ([QueryClinicalTrialUsersDataByTimestampRangeQuery.Data.QueryClinicalTrialUsersDataByTimestampRange.Item?]?, MovanoError?) -> Void)
	
	func listBloodPressures(range: Range<TimeInterval>, completionHandler: @escaping ([CoreDataBPReading]?, MovanoError?) -> Void)
	
//	func fetchAverageData(forDate:String,completionHandler: @escaping (SleepAverageDataModel?, MovanoError?) -> Void)
	
}
