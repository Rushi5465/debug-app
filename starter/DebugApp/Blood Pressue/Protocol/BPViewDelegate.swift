//
//  BPViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/06/21.
//

import Foundation

protocol BPViewDelegate: AnyObject {
	
	func postFileInfoSuccess(with responseModel: FetchPresignResponseModel)
	func postFileInfoErrorHandler(error: MovanoError)
	func uploadFileToPresignedCompleted()
	func uploadFileToPresignedErrorHandler(error: MovanoError)
	func accessTokenSuccess(with responseModel: AccessTokenResponseModel)
	func accessTokenErrorHandler(error: MovanoError)
	func userInfoSuccess(with responseModel: UserResponseModel)
	func userInfoErrorHandler(error: MovanoError)
	func putMessageToSnsSuccess(with responseModel: MessageToSnsResponseModel)
	func putMessageToSnsErrorHandler(error: MovanoError)
	func getBpByTimeStampSuccess(with responseModel: EmptyResponseModel)
	func getBpByTimeStampErrorHandler(error: MovanoError)
}
