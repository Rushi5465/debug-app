//
//  UserDetailViewDelegate.swift
//  DataUploader
//
//  Created by Sankalp Gupta on 01/04/21.
//

import Foundation

protocol UserDetailViewDelegate: AnyObject {
	func getAllUserSuccess(with listModel: EmptyResponseModel)
	func getAllUserErrorHandler(error: MovanoError)
	func getBloodPressureDataSuccess(with responseModel: EmptyResponseModel)
	func getBloodPressureDataErrorHandler(error: MovanoError)
	
}
