//
//  Patient.swift
//  DataUploader
//
//  Created by Sankalp Gupta on 24/03/21.
//

import Foundation

struct Patient: Decodable {
	
	let id: String
	let name: String	
}
