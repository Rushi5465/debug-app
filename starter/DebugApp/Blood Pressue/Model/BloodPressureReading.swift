//
//  BloodPressureReading.swift
//  DataUploader
//
//  Created by Sankalp Gupta on 24/03/21.
//

import Foundation

struct BloodPressureReading: Decodable {
	
	let systolic: Int
	let diastolic: Int
	let pulse: Int
	let type: String
	let timeStamp: TimeInterval
}
