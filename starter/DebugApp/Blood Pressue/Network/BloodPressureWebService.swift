//
//  BloodPressureWebService.swift
//  DataUploader
//
//  Created by Sankalp Gupta on 01/04/21.
//

import Foundation

// swiftlint:disable all
class BloodPressureWebService: BloodPressureWebServiceProtocol {
	
	private var graphQL: ApolloClientService
	private var service: RestService
	
	init(graphQL: ApolloClientService = ApolloClientService(),service: RestService = RestService.shared) {
		self.graphQL = graphQL
		self.service = service
	}
	
	func getBloodPressureDataByTimeStamp(for patient: Patient, timestamp: Double, completionHandler: @escaping ([QueryClinicalTrialUsersDataByTimestampRangeQuery.Data.QueryClinicalTrialUsersDataByTimestampRange.Item?]?, MovanoError?) -> Void) {
		
		let query = QueryClinicalTrialUsersDataByTimestampRangeQuery(user_name: patient.name, from_ts: Date(timeIntervalSince1970: timestamp).dateBefore(component: .minute, value: 1).timeIntervalSince1970, to_ts: timestamp)
		
		_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
			switch result {
				case .success(let graphQLResult):
					if let items = graphQLResult.data?.queryClinicalTrialUsersDataByTimestampRange?.items {
						completionHandler(items, nil)
					} else {
						completionHandler(nil, .invalidResponseModel)
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						
					} else {
						
					}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
					Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func listBloodPressures(range: Range<TimeInterval>, completionHandler: @escaping ([CoreDataBPReading]?, MovanoError?) -> Void) {
        if(Utility.makeAPICall(timeIntervalRange: range) || !CoreDataBPReading.isBPReadingAvailable(range: range)){
            
            let query = QueryBloodPressureByTimestampRangeQuery(from_ts: range.lowerBound, to_ts: range.upperBound)
                        
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
                switch result {
                case .success(let graphQLResult):
                    if let item = graphQLResult.data?.queryBloodPressureByTimestampRange?.items {
                        for each in item where each != nil {
                            CoreDataBPReading.addBPReading(timeInterval: each!.dataTimestamp, systolic: each!.systolic!, diastolic: each!.diastolic!, pulseRate: each!.pulseRate!)
                        }
                        let bpReadingData = CoreDataBPReading.fetchBloodPressure(for: Date(timeIntervalSince1970: range.lowerBound) ..< Date(timeIntervalSince1970: range.upperBound))
                        completionHandler(bpReadingData, nil)
                    } else {
                        completionHandler(nil, .invalidResponseModel)
                    }
                case .failure(let error):
                    if error.localizedDescription == "Received error response: Unauthorized" ||
                        error.localizedDescription == "Received error response: Token has expired." {
                        
                    } else {
                        
                    }
					let bpReadingData = CoreDataBPReading.fetchBloodPressure(for: Date(timeIntervalSince1970: range.lowerBound) ..< Date(timeIntervalSince1970: range.upperBound))
					completionHandler(bpReadingData, nil)
//					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                    Logger.shared.addLog(error.localizedDescription)
                }
            }
        
        }else{
            let bpReadingData = CoreDataBPReading.fetchBloodPressure(for: Date(timeIntervalSince1970: range.lowerBound) ..< Date(timeIntervalSince1970: range.upperBound))
            completionHandler(bpReadingData, nil)
        }
    }
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
