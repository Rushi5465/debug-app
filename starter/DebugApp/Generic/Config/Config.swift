//
//  Config.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 13/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

class Config {
	
	var dict = [String: AnyObject]()
	static var singleInstance: Config?
	
	static var shared: Config {
		if singleInstance == nil {
			singleInstance = Config()
		}
		return singleInstance!
	}
	
	private init() {
		if let path = Bundle.main.path(forResource: "Config", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
			self.dict = dict
		}
	}
	
	var currentServer: Server {
		return ServerManager.shared.currentServer
	}
	
	var serverList: [Server] {
		if let videoList = dict["serverBaseList"] as? [[String: Any]] {
			var servers: [Server] = []
			for each in videoList {
				if let serverUrl = each["serverUrl"] as? String,
					let graphQLUrl = each["graphQLUrl"] as? String,
					let serverType = each["serverType"] as? String {
					
					servers.append(Server(serverUrl: serverUrl, graphQLUrl: graphQLUrl, type: ServerType(rawValue: serverType)!))
				}
			}
			return servers
		}
		return [Server]()
	}
	
	var authToken: String {
		if let authToken = dict["authToken"] as? String {
			return authToken
		}
		return ""
	}
	
	var autoLoginMessage: String {
		if let autoLoginMessage = dict["autoLoginMessage"] as? String {
			return autoLoginMessage
		}
		return ""
	}
	
	var calibrationDeadline: NSNumber {
		return dict["calibrationDeadline"] as! NSNumber
	}
}
