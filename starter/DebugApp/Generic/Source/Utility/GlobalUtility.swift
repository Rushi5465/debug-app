//
//  GlobalUtility.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 13/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

// Helper function inserted by Swift 4.2 migrator.
func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
