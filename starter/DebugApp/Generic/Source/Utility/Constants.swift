//
//  Constants.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 07/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import UIKit

struct ErrorString {
	static let invalidCode = "Please enter code."
	static let invalidNumber = "Invalid Phone Number."
	static let invalidEmail = "Invalid Email Address."
	static let invalidDailingCode = "Please enter country dialing code."
	static let invalidDailingCodeLength = "Country dialing code should be 3 digits only."
	static let invalidPasswordLength = "Password should have a minimum of 8 characters."
	//static let invalidPasswordFormat = "Please ensure that you have at least one lower case letter, one upper case letter, one digit and one special character."
	static let invalidPasswordFormat = """
        Password should contain minimum – 8 characters,
        1 lowercase and 1 uppercase alphabet,
        1 digit and 1 special character
        """
	static let invalidFirstName = "Invalid First Name"
//		"First name should contain only alphabets not numbers or other special characters"
	static let invalidLastName = "Invalid Last Name"
//		"Last name should contain only alphabets not numbers or other special characters"
	
	static let blankFirstName = "First Name can't blank"
	static let blankLastName = "Last Name can't blank"
	
	static let passwordCriteria = "Password should contain minimum - "
	static let mincharacters = "8 characters"
	static let lowercase = "1 lowercase"
	static let uppercase = "1 uppercase alphabet"
	static let digit = "1 digit"
	static let special = "1 special character"
	static let passwordsDontMatch = "Passwords do not match"
	static let invalidGlucoseValue = "Enter valid value."
	static let enterOTP = "Please validate phone number before proceeding."
	static let invalidOTP = "Invalid OTP"
}

struct StringConstants {
	static let emailInfoString = "A verification email will be sent."
	static let phoneInfoString = "A one-time code will be sent via SMS."
	static let verificationString = "We have sent verification code to your email id "
	static let validateString = ". Please validate the code next."
}

struct ColorConstants {
	//  COLOR CONSTANT
	static let kColorTextFieldBorder = UIColor.valueBoxColor
	static let kColorTextFieldErrorBorder = UIColor.outsideTargetColor
	static let kNavigationTitleColor = UIColor.primaryColor
	static let kNavigationBackgroundColor = UIColor.backgroundColor
	static let kNavigationTitlePostLoginColor = UIColor.valueTitleColor
	static let kPersonalDetailsTitleText = UIColor.valueTitleColor
	static let kPlaceholderText = UIColor.mediumBlueColor
}

struct ColorBandLevels {
	static let glucoseBandLevelNormal = UIColor.bandLevelNormalColor.withAlphaComponent(0.3)
	static let glucoseBandLevel1 = UIColor.bandLevel1Color.withAlphaComponent(0.3)
	static let glucoseBandLevel2 = UIColor.bandLevel2Color.withAlphaComponent(0.3)
	static let glucoseBandLevel3 = UIColor.bandLevel3Color.withAlphaComponent(0.3)
	
}

//URLConstants.swift
struct APPURL {
	
	static let authToken = Config.shared.authToken
	
	struct Routes {
		static let inviteCode  = "invite/validatecode"
		static let register = "users/register"
		static let login = "users/login"
		static let verify = "users/validatemfa"
		static let sendUserData = "users/updateuserattributes"
		static let resendLink = "users/verifyemail"
		static let getUserData = "users/getuserattributes"
		static let getnewToken = "users/getaccesstoken"
		static let changePassword = "users/changepassword"
		static let forgotPassword = "users/forgotpassword"
		static let confirmForgotPassword = "users/resetpassword"
		static let delete = "users/deleteuser"
		static let phoneVerification = "users/sendcodetoverifynumber"
		static let confirmRegistration = "users/validateregistrationcode"
		static let chart = "users/getchartsdata"
		static let uploadFiles  = "users/fetchpresignedurl"
		static let putMessageToSns = "users/putmsgtosns"
		static let uploadAccelerometerFiles  = "users/fetchpresignedurl-accelerometer"
		static let putMessageToSnsAccelerometer = "users/putmsgtosns-accelerometer"
		static let firmwareUpdate = "firmware-update-check"
		static let fetchExerciseData = "users/getexercisedata"
		static let getUserSummary = "users/getsummary"
		static let fetchIntensity = "users/fetchactivityintensitysummary"
	}
	
	static let graphQLEndPoint = Config.shared.currentServer.graphQLUrl
	static let BaseURL = Config.shared.currentServer.serverUrl
	static let inviteCodeURL = BaseURL + Routes.inviteCode
	static let registerURL = BaseURL + Routes.register
	static let loginURL = BaseURL + Routes.login
	static let verifyURL = BaseURL + Routes.verify
	static let getUserDataURL = BaseURL + Routes.getUserData
	static let sendUserDataURL = BaseURL + Routes.sendUserData
	static let resendLinkURL = BaseURL + Routes.resendLink
	static let getnewTokenURL = BaseURL + Routes.getnewToken
	static let changePasswordURL = BaseURL + Routes.changePassword
	static let forgotPasswordURL = BaseURL + Routes.forgotPassword
	static let confirmForgotPasswordURL = BaseURL + Routes.confirmForgotPassword
	static let deleteURL = BaseURL + Routes.delete
	static let phoneVerificationURL = BaseURL + Routes.phoneVerification
	static let confirmRegistrationURL = BaseURL + Routes.confirmRegistration
	static let chartURL = BaseURL + Routes.chart
	static let uploadFilesURL  = BaseURL + Routes.uploadFiles
	static let putMessageToSnsURL = BaseURL + Routes.putMessageToSns
	static let uploadAccelerometerFilesURL  = BaseURL + Routes.uploadAccelerometerFiles
	static let putMessageToSnsAccelerometerURL = BaseURL + Routes.putMessageToSnsAccelerometer
	static let firmwareUpdateURL = BaseURL + Routes.firmwareUpdate
	static let fetchExerciseDataURL = BaseURL + Routes.fetchExerciseData
	static let sleepStageURL = BaseURL + Routes.getUserSummary
	static let intensityDataURL = BaseURL + Routes.fetchIntensity
    static let DASHBOARD_DATA_URL = "https://run.mocky.io/v3/d994957d-acce-4be5-a2eb-05145d4d482c"

}

struct DateFormat {
	static let birthdateFormat = "MM/dd/yyyy"
	static let serverDateFormat = "yyyy-MM-dd"
}

struct UtilityConstant {
	static let STRIDE_LENGTH_CONSTANT = 0.413
	static let MINIMUM_BIRTH_AGE = 13
}
