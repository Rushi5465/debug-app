//
//  Utility.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 07/01/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication

enum FontWeight {
    case Regular
    case Semibold
    case Bold
    case Light
}

class Utility {
	
	class func getReadableDate(timeStamp: TimeInterval, isMonth: Bool? = false) -> (timeString: String?, dateString: String?) {
		var timeString: String, dateString: String
		let date = Date(timeIntervalSince1970: timeStamp)
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "hh:mm a"
		timeString = dateFormatter.string(from: date)
		if Calendar.current.isDateInYesterday(date) {
			if isMonth == true {
				dateFormatter.dateFormat = "MM/dd/yyyy"
				dateString = dateFormatter.string(from: date)
			} else {
				dateString =  "Yesterday "
			}
			return(timeString, dateString)
		} else if Calendar.current.isDateInToday(date) {
			if isMonth == true {
				dateFormatter.dateFormat = "MM/dd/yyyy"
				dateString = dateFormatter.string(from: date)
			} else {
				dateString =  "Today "
			}
			return(timeString, dateString)
		} else {
			dateFormatter.dateFormat = "MM/dd/yyyy"
			dateString = dateFormatter.string(from: date)
			dateFormatter.dateFormat = "hh:mm a"
			timeString = dateFormatter.string(from: date)
			return(timeString, dateString)
		}
	}
	
	class func getDateTimeString(timeStamp: TimeInterval) -> String {
		let date = Date(timeIntervalSince1970: timeStamp)
		let dateFormatter = DateFormatter()
		
		if Calendar.current.isDateInYesterday(date) {
			dateFormatter.dateFormat = "hh:mma"
			return "Yesterday " + dateFormatter.string(from: date)
		} else if Calendar.current.isDateInToday(date) {
			dateFormatter.dateFormat = "hh:mma"
			return "Today " + dateFormatter.string(from: date)
		} else {
			dateFormatter.dateFormat = "MM/dd/yy hh:mma"
			return dateFormatter.string(from: date)
		}
	}
	
	class func getHeightInInches(height: String) -> Float {
		var heightString = height.replacingOccurrences(of: "feet ", with: "")
		heightString = heightString.replacingOccurrences(of: " inches", with: "")
		heightString = heightString.replacingOccurrences(of: " inch", with: "")
		
		let heightArray = heightString.split(separator: " ")
		if heightArray.count > 0 {
			let feet = Int("\(heightArray[0])")!
			let inch = Int("\(heightArray[2])")!
			return Float((feet * 12) + inch)
		} else {
			return Float(0)
		}
		
	}
	
	class func getHeightFeets(height: Float) -> (feet: Int, inch: Int) {
		let valueInFeets = (height / 12.0)
		let stringFeet = String(format: "%.1f", valueInFeets)
		let stringValues = stringFeet.split(separator: ".")
		if let feet = Int(stringValues[0]), let inch = Int(stringValues[1]) {
			return (feet, inch)
		}
		return(0, 0)
	}
	
	class func getGlucoseRangeArray() -> [Int] {
		var glucoseRange = [Int]()
		let lowValue = 100
		let highValue = 180
		let steppingValue = Int((highValue - lowValue)/5)
		for item in stride(from: lowValue, through: highValue, by: steppingValue) {
			glucoseRange.append(item)
		}
		
		return glucoseRange
	}
	
	class func getBandDictionary() -> [Int: UIColor] {
		var bandDictionary = [Int: UIColor]()
		let glucoseRange = getGlucoseRangeArray()
		for index in glucoseRange.indices {
			switch index {
				case 0, 6:
					bandDictionary[index] = ColorBandLevels.glucoseBandLevel3
				case 1, 5:
					bandDictionary[index] = ColorBandLevels.glucoseBandLevel2
				case 2, 4:
					bandDictionary[index] = ColorBandLevels.glucoseBandLevel1
				case 3:
					bandDictionary[index] = ColorBandLevels.glucoseBandLevelNormal
				default:
					// break
					bandDictionary[index] = ColorBandLevels.glucoseBandLevel3
			}
		}
		return bandDictionary
	}
	
	class func getIndexForGlucoseValue(glucoseValue: Int) -> Int {
		let glucoseRange: [Int] = getGlucoseRangeArray().reversed()
		
		for index in glucoseRange.indices.reversed() {
			if glucoseValue <= glucoseRange[5] {
				return 6
			} else if glucoseValue >= glucoseRange[0] {
				return 0
			}
			if index != 0 {
				if (glucoseRange[index] ... glucoseRange[index-1]).contains(glucoseValue) {
					return index
				}
			}
			
		}
		return 0
	}
	
	class func getColorValueFor(glucoseValue: Double?) -> UIColor {
		if let glucoseValue = glucoseValue {
			if let lowValue = Double(KeyChain.shared.lowGlucoseValue), let highValue = Double(KeyChain.shared.highGlucoseValue) {
				if glucoseValue >= lowValue && glucoseValue <= highValue {
					return UIColor.withinTargetColor
				} else {
					return UIColor.outsideTargetColor
				}
			}
		}
		return ColorBandLevels.glucoseBandLevelNormal
	}
	
	class func getBandForValue(glucoseValue: Int) -> UIColor {
		var glucoseRange = [Int]()
		if let lowValue = Int(KeyChain.shared.lowGlucoseValue), let highValue = Int(KeyChain.shared.highGlucoseValue) {
			let steppingValue = Int((highValue - lowValue)/5)
			for item in stride(from: lowValue, through: highValue, by: steppingValue) {
				glucoseRange.append(item)
			}
			//let glucoseRange:[Int] = getGlucoseRangeArray().reversed()
			for (index, element) in glucoseRange.reversed().enumerated() {
				print(element)
				if index != 5 {
					if (glucoseRange[index] ... glucoseRange[index+1]).contains(glucoseValue) {
						switch index+1 {
							case 3:
								return ColorBandLevels.glucoseBandLevelNormal
							case 2, 4:
								return ColorBandLevels.glucoseBandLevel1
							case 1, 5:
								return ColorBandLevels.glucoseBandLevel2
							case 0, 6:
								return ColorBandLevels.glucoseBandLevel3
							default:
								return ColorBandLevels.glucoseBandLevelNormal
						}
					}
				}
				if glucoseValue >= glucoseRange[5] {
					return ColorBandLevels.glucoseBandLevel3
				} else if glucoseValue <= glucoseRange[0] {
					return ColorBandLevels.glucoseBandLevel3
				}
			}
		}
		return ColorBandLevels.glucoseBandLevelNormal
	}
	
	class func convertDateFormat(dateString: String) -> String {
		let inputFormatter = DateFormatter()
		inputFormatter.dateFormat = DateFormat.birthdateFormat
		inputFormatter.dateStyle = .medium
		let showDate = inputFormatter.date(from: dateString)
		inputFormatter.dateFormat = DateFormat.birthdateFormat
		let resultString = inputFormatter.string(from: showDate!)
		return resultString
	}
	
	class func getFormattedDateFromString(dateString: String) -> Date {
		let dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .medium
		dateFormatter.dateFormat = DateFormat.birthdateFormat
		let dateofbirth = dateFormatter.date(from: dateString)
		return dateofbirth!
	}
	
	class func getValidColourCodedString(error: String) -> NSAttributedString {
		
		let range = (ErrorString.invalidPasswordFormat as NSString).range(of: error)
		let attributedText = NSMutableAttributedString.init(string: ErrorString.invalidPasswordFormat)
		attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: ColorBandLevels.glucoseBandLevelNormal.withAlphaComponent(1.0), range: range)
		return attributedText
	}
	
	class func getErrorColourCodedString(error: String) -> NSAttributedString {
		let range = (ErrorString.invalidPasswordFormat as NSString).range(of: error)
		let attributedText = NSMutableAttributedString.init(string: ErrorString.invalidPasswordFormat)
		attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: ColorBandLevels.glucoseBandLevel3.withAlphaComponent(1.0), range: range)
		return attributedText
	}
	
    class func getCustomFontName(fontWeight:FontWeight)->String{
        switch fontWeight {
        case .Regular:
            return "Gilroy-Regular"
        case .Semibold:
            return "Gilroy-SemiBold"
        case .Bold:
            return "Gilroy-Bold"
        case .Light:
            return "Gilroy-Light"
        }
    }
    
    class func getCustomMovanoFontName(fontWeight:MovanoFontWeight)->String{
        switch fontWeight {
        case .regular:
            return "Gilroy-Regular"
        case .semibold:
            return "Gilroy-Semibold"
            //return "OpenSans-Semibold"
        case .bold:
            return "Gilroy-Bold"
        case .light:
            return "Gilroy-Light"
        case .ultralight:
            return "Gilroy-Ultralight"
        case .thin:
            return "Gilroy-Thin"
        case .medium:
            return "Gilroy-Medium"
        case .heavy:
            return "Gilroy-Heavy"
        case .black:
            return "Gilroy-Black"
        case .italic:
            return "Gilroy-Italic"
        }
    }
    
    class func convertminutesToHMFormat (_ minutes : Int) -> String {
        return "\(minutes / 60)h \(minutes % 60)m"
    }
    
    class func convertMinutesSleepDialTimeFormat(_ minutes : Int)->Double{
        return Double(minutes/60) + Double(minutes % 60)/60.0
    }
    
    class func makeAPICall(timeInterval:TimeInterval)->Bool {
        return Calendar.current.isDateInToday(Date(timeIntervalSince1970: timeInterval))
    }
    
    class func makeAPICall(timeIntervalRange:Range<TimeInterval>)->Bool{
        return Calendar.current.isDateInToday(Date(timeIntervalSince1970: timeIntervalRange.lowerBound)) || Calendar.current.isDateInToday(Date(timeIntervalSince1970: timeIntervalRange.upperBound))
    }
    
    class func makeAPICall(date:Date)->Bool {
        return Calendar.current.isDateInToday(date)
    }
    
    class func temperatureInFahrenheit(temperature: Double) -> Double {
          let fahrenheitTemperature = temperature * 9 / 5 + 32
          return fahrenheitTemperature
    }
	
	class func getTempVariation(temperature: Double) -> Double {
		return (temperature - (KeyChain.shared.baselineBodyTemprature?.toDouble ?? 98.6))
	}
	
	class func getFontFamilyForCharts() -> String {
		return "OpenSans-Regular"
	}
}
