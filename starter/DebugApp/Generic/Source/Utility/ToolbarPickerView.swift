//
//  ToolbarPickerView.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 18/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import UIKit

protocol ToolbarPickerViewDelegate: AnyObject {
	func didTapDone()
	func didTapCancel()
}

class ToolbarPickerView: UIPickerView {
	
	public private(set) var toolbar: UIToolbar?
	public weak var toolbarDelegate: ToolbarPickerViewDelegate?
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.commonInit()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.commonInit()
	}
	
	private func commonInit() {
		self.backgroundColor = UIColor.white
		let screenWidth = UIScreen.main.bounds.width
		let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0))
		let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
		
		let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.cancelTapped))
		cancelButton.addMovanoProperties()
		
		let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneTapped))
		doneButton.addMovanoProperties()
		
		toolBar.backgroundColor = ColorConstants.kNavigationBackgroundColor
		toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
		toolBar.isUserInteractionEnabled = true
		
		self.toolbar = toolBar
	}
	
	@objc func doneTapped() {
		self.toolbarDelegate?.didTapDone()
	}
	
	@objc func cancelTapped() {
		self.toolbarDelegate?.didTapCancel()
	}
}
