//
//  AppUtility.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 13/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit

struct AppUtility {
	
	static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
		
		if let delegate = UIApplication.shared.delegate as? AppDelegate {
			delegate.orientationLock = orientation
		}
	}
	
}
