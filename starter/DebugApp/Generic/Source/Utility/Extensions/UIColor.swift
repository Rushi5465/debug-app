//
//  UIColor.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 26/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
	
	class func hexColor(hex: String) -> UIColor? {
		var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
		
		if cString.hasPrefix("#") {
			cString.remove(at: cString.startIndex)
		}
		
		if (cString.count) != 6 {
			return nil
		}
		
		var rgbValue: UInt64 = 0
		Scanner(string: cString).scanHexInt64(&rgbValue)
		
		return UIColor(
			red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
			green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
			blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
			alpha: CGFloat(1.0)
		)
	}
	
	var hexString: String {
		let components = self.cgColor.components
		let r: CGFloat = components?[0] ?? 0.0
		let g: CGFloat = components?[1] ?? 0.0
		let b: CGFloat = components?[2] ?? 0.0
		
		let hexString = String.init(format: "#%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
		return hexString
	}
	
	static var avgColor: UIColor {
		return UIColor(named: "Average")!
	}
	
	static var backgroundColor: UIColor {
		return UIColor(named: "Background")!
	}
	
	static var dividerDarkColor: UIColor {
		return UIColor(named: "Divider Dark")!
	}
	
	static var dividerLightColor: UIColor {
		return UIColor(named: "Divider Light")!
	}
	
	static var maxColor: UIColor {
		return UIColor(named: "Maximum")!
	}
	
	static var minColor: UIColor {
		return UIColor(named: "Minimum")!
	}
	
	static var mediumBlueColor: UIColor {
		return UIColor(named: "Movano Medium Blue")!
	}
	
	static var mutedButtonColor: UIColor {
		return UIColor(named: "Muted Button")!
	}
	
	static var outsideTargetColor: UIColor {
		return UIColor(named: "Outside Target")!
	}
	
	static var primaryColor: UIColor {
		return UIColor(named: "PrimaryColour")!
	}
	
	static var shadow: UIColor {
		return UIColor(named: "Shadow")!
	}
	
	static var subTextTitleColor: UIColor {
		return UIColor(named: "Sub Text")!
	}
    
    static var secondaryTextColor: UIColor {
        return UIColor(named: "SecondaryTextColor")!
    }
	static var textTitleColor: UIColor {
		return UIColor(named: "Text Title")!
	}
	
	static var valueBoxBackgroundColor: UIColor {
		return UIColor(named: "Value Box Background")!
	}
	
	static var valueBoxColor: UIColor {
		return UIColor(named: "Value BoxLight Movano Blue")!
	}
	
	static var valueTitleColor: UIColor {
		return UIColor(named: "Value Title Color")!
	}
	
	static var withinTargetColor: UIColor {
		return UIColor(named: "Within Target")!
	}
	
	static var bandLevelNormalColor: UIColor {
		return UIColor(named: "BandLevel-Normal")!
	}
	
	static var bandLevel1Color: UIColor {
		return UIColor(named: "BandLevel-1")!
	}
	
	static var bandLevel2Color: UIColor {
		return UIColor(named: "BandLevel-2")!
	}
	
	static var bandLevel3Color: UIColor {
		return UIColor(named: "BandLevel-3")!
	}
	
	static var textColor: UIColor {
		return UIColor(named: "Text Color")!
	}
	
	static var dashboardBackground: UIColor {
		return UIColor(named: "DashboardBackground")!
	}
	
	static var settingBackground: UIColor {
		return UIColor(named: "SettingBackground")!
	}
	
	static var barPurple: UIColor {
		return UIColor(named: "BarPurple")!
	}
	
	static var barPink: UIColor {
		return UIColor(named: "BarPink")!
	}
	
	static var batterySelect: UIColor {
		return UIColor(named: "Battery-Select")!
	}
	
	static var batteryDeselect: UIColor {
		return UIColor(named: "Battery-Deselect")!
	}
}
