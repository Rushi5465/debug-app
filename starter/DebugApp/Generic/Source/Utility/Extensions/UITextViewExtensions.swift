//
//  UITextViewExtensions.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 12/03/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
	func showToolBar() {
		// Create a toolbar and assign it to inputAccessoryView
		let screenWidth = UIScreen.main.bounds.width
		let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
		let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
		
		let barButton = UIBarButtonItem(title: "ButtonDone".localized, style: .done, target: target, action: #selector(tapDone)) //7
		barButton.addMovanoProperties()
		
		toolBar.setItems([flexible, barButton], animated: false) //8
		toolBar.backgroundColor = ColorConstants.kNavigationBackgroundColor
		self.inputAccessoryView = toolBar //9
	}
	
	@objc func tapCancel() {
		self.resignFirstResponder()
	}
	
	@objc func tapDone() {
		self.resignFirstResponder()
	}
	
	@IBInspectable
	var cornerRadius: CGFloat {
		get {
			return layer.cornerRadius
		}
		set {
			layer.cornerRadius = newValue
			layer.masksToBounds = newValue > 0
		}
	}
}
