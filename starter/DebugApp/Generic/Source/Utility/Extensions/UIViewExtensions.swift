//
//  UIViewExtensions.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 19/12/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
extension UIView {
	
	@IBInspectable
	var borderWidth: CGFloat {
		get {
			return layer.borderWidth
		}
		set {
			layer.borderWidth = newValue
		}
	}
	
	@IBInspectable
	var borderColor: UIColor? {
		get {
			let color = UIColor.init(cgColor: layer.borderColor!)
			return color
		}
		set {
			layer.borderColor = newValue?.cgColor
		}
	}
	
	func roundCorners(_ corners: CACornerMask, radius: CGFloat) {
		if #available(iOS 11, *) {
			self.layer.cornerRadius = radius
			self.layer.maskedCorners = corners
		} else {
			var cornerMask = UIRectCorner()
			if corners.contains(.layerMinXMinYCorner) {
				cornerMask.insert(.topLeft)
			}
			if corners.contains(.layerMaxXMinYCorner) {
				cornerMask.insert(.topRight)
			}
			if corners.contains(.layerMinXMaxYCorner) {
				cornerMask.insert(.bottomLeft)
			}
			if corners.contains(.layerMaxXMaxYCorner) {
				cornerMask.insert(.bottomRight)
			}
			let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: cornerMask, cornerRadii: CGSize(width: radius, height: radius))
			let mask = CAShapeLayer()
			mask.path = path.cgPath
			self.layer.mask = mask
		}
	}
	
	func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
		let animation = CABasicAnimation(keyPath: "transform.rotation")
		animation.toValue = toValue
		animation.duration = duration
		animation.isRemovedOnCompletion = false
		animation.fillMode = CAMediaTimingFillMode.forwards
		self.layer.add(animation, forKey: nil)
	}
	
	func navigationBarShadow(color: UIColor, shadowOffset: CGSize, shadowOpacity: Float) {
		let shadowPath = UIBezierPath(rect: CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: self.bounds.height))
		self.layer.masksToBounds = false
		self.layer.shadowColor = color.cgColor
		self.layer.shadowOffset = CGSize.zero
		self.layer.shadowOpacity = shadowOpacity
		self.layer.shadowPath = shadowPath.cgPath
	}
	
	func addShadow(offset: CGSize, color: UIColor, opacity: Float) {
		layer.masksToBounds = false
		layer.shadowOffset = offset
		layer.shadowColor = color.cgColor
		layer.shadowRadius = self.layer.cornerRadius - offset.height
		layer.shadowOpacity = opacity
		
		let backgroundCGColor = backgroundColor?.cgColor
		backgroundColor = nil
		layer.backgroundColor =  backgroundCGColor
	}
	
	func removeView(specificView: UIView) {
		for subview in self.subviews where specificView == subview {
			subview.removeFromSuperview()
		}
	}
	
	func removeView(otherThan otherViews: [UIView]) {
		for subview in self.subviews {
			if !otherViews.contains(subview) {
				subview.removeFromSuperview()
			}
		}
	}
}
