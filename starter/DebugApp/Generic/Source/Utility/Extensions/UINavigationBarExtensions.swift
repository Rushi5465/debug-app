//
//  UINavigationBarExtensions.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 21/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
	func setUp() {
		self.isTranslucent = true
		self.barTintColor = ColorConstants.kNavigationBackgroundColor
		self.tintColor = ColorConstants.kNavigationTitleColor
		self.shadowImage = UIImage()
		self.titleTextAttributes = [NSAttributedString.Key.foregroundColor: ColorConstants.kNavigationTitleColor,
									NSAttributedString.Key.font: UIFont.gotham.bold(withSize: 26)]
		
	}
}
