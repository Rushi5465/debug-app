//
//  OTPTextField.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 28/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import UIKit

class OTPTextField: MovanoTextField {
	// MARK: Life cycle
	
	var numberTextAttributes: [NSAttributedString.Key: Any]?
	var otpForController: String = ""
	
	override func awakeFromNib() {
		super.awakeFromNib()
		delegate = self
		let paragraph = NSMutableParagraphStyle()
		paragraph.alignment = .center
		// Set text attributes
		numberTextAttributes = [
			NSAttributedString.Key.strokeColor: UIColor.black,
			NSAttributedString.Key.font: UIFont(name: "OpenSans-Semibold", size: 26)!,
			NSAttributedString.Key.foregroundColor: UIColor.white,
			NSAttributedString.Key.paragraphStyle: paragraph
			] as [NSAttributedString.Key: Any]
		
	}
	
	// MARK: Methods
	
	override func deleteBackward() {
		super.deleteBackward()
		
		let previousTag = self.tag - 1
		if let previousResponder = self.superview?.viewWithTag(previousTag) {
			previousResponder.becomeFirstResponder()
		}
	}
}

// MARK: - UITextFieldDelegate
extension OTPTextField: UITextFieldDelegate {
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if let attributes = numberTextAttributes {
			textField.defaultTextAttributes = attributes
		}
		guard let textFieldCount = textField.text?.count else { return false }
		
		// Сlosure
		let setValueAndMoveForward = {
			textField.text = string
			let nextTag = textField.tag + 1
			if let nextResponder = textField.superview?.viewWithTag(nextTag) {
				nextResponder.becomeFirstResponder()
			}
		}
		
		// Сlosure
		let clearValueAndMoveBack = {
			textField.text = ""
			let previousTag = textField.tag - 1
			if let previousResponder = textField.superview?.viewWithTag(previousTag) {
				previousResponder.becomeFirstResponder()
			}
		}
		
		if textFieldCount < 1 && string.count > 0 {
			setValueAndMoveForward()
			if textField.tag == 6 {
				textField.resignFirstResponder()
				NotificationCenter.default.post(name: NSNotification.Name(rawValue: "otpEntered"), object: nil)
			}
			if textField.tag == 4 {
				textField.resignFirstResponder()
				if otpForController == "NewPasscode" {
					NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newPasscodeEntered"), object: nil)
				} else if otpForController == "CurrentPasscode" {
					NotificationCenter.default.post(name: NSNotification.Name(rawValue: "currentPasscodeEntered"), object: nil)
				}
			}
			return false
		} else if textFieldCount >= 1 && string.count == 0 {
			clearValueAndMoveBack()
			return false
		} else if textFieldCount >= 1 {
			setValueAndMoveForward()
			return false
		}
		
		return true
		
	}
	
}
