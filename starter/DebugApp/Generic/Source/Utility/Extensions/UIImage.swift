//
//  UIImage.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 17/08/21.
//

import Foundation
import UIKit

extension UIImage {
	
	class func createSelectionIndicator(color: UIColor, size: CGSize, lineHeight: CGFloat) -> UIImage {
		UIGraphicsBeginImageContextWithOptions(size, false, 0)
		color.setFill()
		UIRectFill(CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: size.width, height: lineHeight)))
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		return image!
	}
}
