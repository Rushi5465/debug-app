//
//  String.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 02/07/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit

extension String {
	
	var trim: String {
		return self.trimmingCharacters(in: .whitespacesAndNewlines)
	}
	
	func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
		let disallowedCharacterSet = NSCharacterSet(charactersIn: matchCharacters).inverted
		return self.rangeOfCharacter(from: disallowedCharacterSet) == nil
	}
	
	var isValidEmail: Bool {
		let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
		
		let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
		return emailTest.evaluate(with: self)
	}
	
	var isValidPassword: Bool {
		let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}")
		return passwordTest.evaluate(with: self)
	}
	
	var hasUpperCase: Bool {
		let uppercaseRegEx  = ".*[A-Z]+.*"
		let passwordTest = NSPredicate(format: "SELF MATCHES %@", uppercaseRegEx)
		return passwordTest.evaluate(with: self)
	}
	
	var hasLowerCase: Bool {
		let lowerRegEx  = ".*[a-z]+.*"
		let passwordTest = NSPredicate(format: "SELF MATCHES %@", lowerRegEx)
		return passwordTest.evaluate(with: self)
	}
	
	var hasSpecialCharacter: Bool {
		let specialRegEx  = ".*[!@#$&*]+.*"
		// , -'\"
		// ".*[!@#$&*%'()*+./:;<>=?[]^_{}|~]+.*"
		let passwordTest = NSPredicate(format: "SELF MATCHES %@", specialRegEx)
		return passwordTest.evaluate(with: self)
	}
	
	var hasDigit: Bool {
		let digitRegEx  = ".*[0-9]+.*"
		let passwordTest = NSPredicate(format: "SELF MATCHES %@", digitRegEx)
		return passwordTest.evaluate(with: self)
	}
	
	var doubleToNumber: NSNumber? {
		if let myInteger = Double(self) {
			return NSNumber(value: myInteger)
		}
		return nil
	}
	
	var isValidPhoneNumber: Bool {
		if self.containsOnlyCharactersIn(matchCharacters: "(012) 345-67896") && self.count <= 14 {
			return true
		}
		return false
	}
	
	var unformattedPhone: String {
		var phone = self
		phone = phone.replacingOccurrences(of: "(", with: "")
		phone = phone.replacingOccurrences(of: ")", with: "")
		phone = phone.replacingOccurrences(of: "-", with: "")
		phone = phone.replacingOccurrences(of: " ", with: "")
		return phone
	}
	
	var isCountryCodeValid: Bool {
		if self.containsOnlyCharactersIn(matchCharacters: "0123456789") && self.count > 0 && self.count <= 3 {
			return true
		}
		return false
	}
	
	var localized: String {
		return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
	}
	
	func toDate(format: String) -> Date? {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = format
		dateFormatter.locale = Locale(identifier: "en")
		
		return dateFormatter.date(from: self)
	}
	
	func jsonString(dictionary: Any) -> String? {
		if let arrayDict = dictionary as? [[String: Any]] {
			if arrayDict.count > 0 {
				if let theJSONData = try? JSONSerialization.data(withJSONObject: arrayDict, options: []) {
					let theJSONText = String(data: theJSONData, encoding: .ascii)
					return theJSONText!
				}
			}
			return nil
		}
		
		if let strDict = dictionary as? [String: String] {
			if strDict.count > 0 {
				if let theJSONData = try? JSONSerialization.data(withJSONObject: strDict, options: []) {
					let theJSONText = String(data: theJSONData, encoding: .ascii)
					return theJSONText!
				}
			}
			return nil
		}
		
		if let normalDict = dictionary as? [String: Any] {
			if normalDict.count > 0 {
				if let theJSONData = try? JSONSerialization.data(withJSONObject: normalDict, options: []) {
					let theJSONText = String(data: theJSONData, encoding: .ascii)
					return theJSONText!
				}
			}
			return nil
		}
		return nil
	}
	
	var toDouble: Double? {
		return NumberFormatter().number(from: self)?.doubleValue
	}
	
	var floatValue: Float? {
		return Float(self)
	}
	
	func safelyLimitedTo(length n: Int) -> String {
		if self.count <= n {
			return self
		}
		return String( Array(self).prefix(upTo: n) )
	}
	
	var heightInInch: Float? {
		let data = self.components(separatedBy: " ")
		if data.count == 4 {
			let foot = Int(data[0])!
			let inch = Int(data[2])!
			
			return Float((foot * 12) + inch)
		}
		return nil
	}
	
	var weightInLbs: String {
		var data = self.replacingOccurrences(of: "lbs", with: "")
		data = data.replacingOccurrences(of: " ", with: "")
		return data
	}
	
	var formattedHeight: String {
		if let value  = Double(self) {
			let intValue = Int(value)
			let inch = intValue % 12
			let foot = intValue / 12
			return "\(foot) ft \(inch) inch"
		}
		return ""
	}
	
	var formattedWeight: String {
		return self.appending(" lbs")
	}
	
	var isAlphabetic: Bool {
		let hasLetters = rangeOfCharacter(from: .letters, options: .numeric, range: nil) != nil
		let hasNumbers = rangeOfCharacter(from: .decimalDigits, options: .literal, range: nil) != nil
		
		let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
		let hasOnlyLetters = rangeOfCharacter(from: characterset) != nil
		
		return hasLetters && !hasNumbers && hasOnlyLetters
	}
}

extension Substring {
	var toDouble: Double? {
		return NumberFormatter().number(from: String(self))?.doubleValue
	}
}

func convertStringToDictionary(text: String) -> [String:AnyObject]? {
   if let data = text.data(using: .utf8) {
	   do {
		   let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
		   return json
	   } catch {
		   print("Something went wrong")
	   }
   }
   return nil
}
