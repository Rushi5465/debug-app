//
//  Int.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 01/07/21.
//

import Foundation

extension Int {
	
	var toCounterFormat : String {
		let minute = self / 60
		let seconds = self % 60
		return String(format: "%02d", minute) + " : " + String(format: "%02d", seconds)
	}
}
