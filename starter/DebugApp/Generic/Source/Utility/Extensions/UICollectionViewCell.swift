//
//  UICollectionViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
	
	static var IDENTIFIER: String {
		return String(describing: self)
	}
	
	static func dequeueReusableCell(collectionView: UICollectionView, indexPath: IndexPath) -> Self {
		return Self.instantiate(collectionView: collectionView, indexPath: indexPath)
	}
	
	static func instantiate<T: UICollectionViewCell>(collectionView: UICollectionView, indexPath: IndexPath, function: String = #function, line: Int = #line, file: String = #file) -> T {
		guard let scene = collectionView.dequeueReusableCell(withReuseIdentifier: Self.IDENTIFIER, for: indexPath) as? T else {
			fatalError("Error")
		}
		return scene
	}
}
