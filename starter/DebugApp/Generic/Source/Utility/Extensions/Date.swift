//
//  Date.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 07/07/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

extension Date {
	
	func dateToString(format: String) -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = format
		dateFormatter.locale = Locale(identifier: "en")
		return dateFormatter.string(from: self)
	}
    
    func dateToReadableTime()->String{
        let formatter = DateFormatter()
        formatter.timeZone = .current
        formatter.dateFormat = "h:mm a"
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"
        return formatter.string(from:self)
    }
    
	var dayBefore: Date {
		var calendar = Calendar.current
		calendar.timeZone = TimeZone.current//TimeZone(abbreviation: "UTC")!
		return calendar.date(byAdding: .day, value: -1, to: noon)!
	}
	
	var dayAfter: Date {
		var calendar = Calendar.current
		//calendar.timeZone = TimeZone(abbreviation: "UTC")!
		calendar.timeZone = TimeZone.current
		return calendar.date(byAdding: .day, value: 1, to: noon)!
	}
	
	var noon: Date {
		var calendar = Calendar.current
		//calendar.timeZone = TimeZone(abbreviation: "UTC")!
		calendar.timeZone = TimeZone.current
		return calendar.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
	}
	
	var startOfDay: Date {
		var calendar = Calendar.current
		//calendar.timeZone = TimeZone(abbreviation: "UTC")!
		calendar.timeZone = TimeZone.current
		return calendar.startOfDay(for: self)
	}
	
	var endOfDay: Date {
		return Calendar.current.date(byAdding: DateComponents(day: 1, second: -1), to: self.startOfDay)!
	}
	
	var startOfWeek: Date {
		let gregorian = Calendar.current
		let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: gregorian.startOfDay(for: self)))!
		return gregorian.date(byAdding: .day, value: 0, to: sunday)!
	}
	
	var endOfWeek: Date {
		let gregorian = Calendar.current
		return gregorian.date(byAdding: DateComponents(day: 7, second: -1), to: self.startOfWeek)!
	}
	
	var startOfMonth: Date {
		var calendar = Calendar.current
		// calendar.timeZone = TimeZone(abbreviation: "UTC")!
		calendar.timeZone = TimeZone.current
		return calendar.date(from: calendar.dateComponents([.year, .month], from: calendar.startOfDay(for: self)))!
	}
	
	var endOfMonth: Date {
		var calendar = Calendar.current
		//calendar.timeZone = TimeZone(abbreviation: "UTC")!
		calendar.timeZone = TimeZone.current
		return calendar.date(byAdding: DateComponents(month: 1, second: -1), to: self.startOfMonth)!
	}
	
	var startOfYear: Date {
        let calendar = Calendar.current
        var components = calendar.dateComponents([.year], from: self)
        components.day = 1
        components.month = 1
        return calendar.date(from: components)!
    }
    
    var endOfYear: Date {
        
        let calendar = Calendar.current
        var components = DateComponents()
        components.year = 1
        components.second = -1
        return calendar.date(byAdding: components, to: startOfYear)!
    }
	
	var month: String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MMMM"
		return dateFormatter.string(from: self)
	}
	
	var dateFormatWithSuffix: String {
		return "d'\(self.daySuffix())' MMM"
	}
	
	var formattedDateWithSuffix: String {
		return self.dateToString(format: dateFormatWithSuffix)
	}
	
	func daySuffix() -> String {
		let calendar = Calendar.current
		let components = (calendar as NSCalendar).components(.day, from: self)
		let dayOfMonth = components.day
		switch dayOfMonth {
			case 1, 21, 31:
				return "st"
			case 2, 22:
				return "nd"
			case 3, 23:
				return "rd"
			default:
				return "th"
		}
	}
	
	func dateBefore(component: Calendar.Component, value: Int) -> Date {
		let thisDate = self
		let calendar = Calendar.current
		
		return calendar.date(byAdding: component, value: (-1 * value), to: thisDate)!
		
	}
	
	func dateAfter(component: Calendar.Component, value: Int) -> Date {
		let thisDate = self
		let calendar = Calendar.current
		
		return calendar.date(byAdding: component, value: value, to: thisDate)!
		
	}
	
	func dateAfter(days: Int? = nil, month: Int? = nil) -> Date {
		let thisDate = self.startOfDay
		let calendar = Calendar.current
		
		var returnDate = self
		if days != nil {
			returnDate = calendar.date(byAdding: .day, value: days!, to: thisDate)!
		} else if month != nil {
			returnDate = calendar.date(byAdding: .month, value: month!, to: thisDate)!
		}
		return returnDate
		
	}
	
	func dateBefore(days: Int? = nil, month: Int? = nil) -> Date {
		let thisDate = self.startOfDay
		let calendar = Calendar.current
		
		var returnDate = self
		if days != nil {
			returnDate = calendar.date(byAdding: .day, value: (-1 * days!), to: thisDate)!
		}
		
		if month != nil {
			returnDate = calendar.date(byAdding: .month, value: (-1 * month!), to: thisDate)!
		}
		return returnDate
		
	}
	
	func dayOfWeek() -> String? {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "EEE"
		dateFormatter.locale = Locale(identifier: "en")
		return dateFormatter.string(from: self).capitalized
	}
	
	func dayNumberOfWeek() -> Int? {
			return Calendar.current.dateComponents([.weekday], from: self).weekday
		}
	
	func isEqual(to date: Date, toGranularity component: Calendar.Component, in calendar: Calendar = .current) -> Bool {
		calendar.isDate(self, equalTo: date, toGranularity: component)
	}
	
	func isInSameMonth(date: Date) -> Bool { isEqual(to: date, toGranularity: .month) }
	
	func isInSameYear (date: Date) -> Bool { isEqual(to: date, toGranularity: .year) }
	
	static func dates(from fromDate: Date, to toDate: Date, interval: Int, component: Calendar.Component) -> [Date] {
		var dates: [Date] = []
		var date = fromDate
		
		while date <= toDate {
			dates.append(date)
			guard let newDate = Calendar.current.date(byAdding: component, value: interval, to: date) else { break }
			date = newDate
		}
		return dates
	}
	
	static func dateBetween(initialDate: Date, finalDate: Date) -> Date {
		let dateDifference = finalDate.timeIntervalSince1970 - initialDate.timeIntervalSince1970
		return Date(timeIntervalSince1970: initialDate.timeIntervalSince1970 + (dateDifference / 2))
	}
	
	var readableDateString : String {
		let dateFormatter = DateFormatter()
		
		if Calendar.current.isDateInYesterday(self) {
			dateFormatter.dateFormat = "hh:mma"
			return "Yesterday"
		} else if Calendar.current.isDateInToday(self) {
			dateFormatter.dateFormat = "hh:mma"
			return "Today"
		} else {
			dateFormatter.dateFormat = "d MMMM yy"
			return dateFormatter.string(from: self)
		}
	}
	
	static func timeIntervalBetween(initialDate: Date, finalDate: Date) -> Int {
		return Int(finalDate.timeIntervalSinceReferenceDate - initialDate.timeIntervalSinceReferenceDate)
	}
    
    static func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
      }
    public func setTime(hour: Int, min: Int, sec: Int, timeZoneAbbrev: String = "UTC") -> Date? {
          let x: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]
          let cal = Calendar.current
          var components = cal.dateComponents(x, from: self)

          components.hour = hour
          components.minute = min
          components.second = sec

          return cal.date(from: components)
      }

}

extension DateComponentsFormatter {
	func difference(from timeInterval: TimeInterval) -> String? {
        self.allowedUnits = [.hour, .minute, .second]
		self.maximumUnitCount = 5
		self.collapsesLargestUnit = true
		self.zeroFormattingBehavior = .default
		self.unitsStyle = .abbreviated
		return self.string(from: timeInterval)
	}
}
