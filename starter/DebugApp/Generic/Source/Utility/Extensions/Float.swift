//
//  Float.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/21.
//

import Foundation

extension Float {
	
	var formattedHeight: String {
		let value  = Int(self)
		let inch = value % 12
		let foot = value / 12
		return "\(foot) ft \(inch) inch"
	}
	
	var formattedWeight: String {
		let value  = Int(self)
		return "\(value) lbs"
	}
}
