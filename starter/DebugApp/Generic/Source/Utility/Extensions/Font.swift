//
//  Font.swift
//  BloodPressureApp
//
//  Created by Rushikant on 15/12/21.
//

import Foundation
import UIKit
import SwiftUI

@available(iOS 13.0, *)
public extension Font {
  init(uiFont: UIFont) {
	self = Font(uiFont as CTFont)
  }
}
