//
//  Double.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/12/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

extension Double {
	
	var numberValue: NSNumber {
		return NSNumber(value: self)
	}
	
	var stringValue: String {
		return String(self)
	}
	
	func inchToMile() -> Double {
		let value = self * 0.000015783
		return (value * 100).rounded()/100.0
	}
	
	func roundToDecimal(_ fractionDigits: Int) -> Double {
		let multiplier = pow(10, Double(fractionDigits))
		return Darwin.round(self * multiplier) / multiplier
	}
	
	func inchToKm() -> Double {
		let value = self * (1/39370)
		return (((value * 100).rounded())/100)
	}
}
