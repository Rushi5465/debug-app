//
//  PasswordTextField.swift
//  BloodPressureApp
//
//  Created by Rushikant on 29/11/21.
//

import UIKit

class PasswordTextField: MovanoTextField {

	override var isSecureTextEntry: Bool {
		didSet {
			if isFirstResponder {
				_ = becomeFirstResponder()
			}
		}
	}

	override func becomeFirstResponder() -> Bool {

		let success = super.becomeFirstResponder()
		if isSecureTextEntry, let text = self.text {
			self.text?.removeAll()
			insertText(text)
		}
		return success
	}

}
