//
//  UIViewControllerExtensions.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 13/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import UIKit

private var scrollViewKey: UInt8 = 0

extension UIViewController {
	
	//StoryBoardID
	class var storyboardID: String {
		return "\(self)"
	}
	
	static func instantiate(fromAppStoryboard appStoryboard: MovanoStoryboard) -> Self {
		return appStoryboard.viewController(viewControllerClass: self)
	}
	
	// MARK: Keyboard Events
	
	fileprivate var internalScrollView: UIScrollView! {
		get {
			return objc_getAssociatedObject(self, &scrollViewKey) as? UIScrollView
		}
		set(newValue) {
			objc_setAssociatedObject(self, &scrollViewKey, newValue, .OBJC_ASSOCIATION_ASSIGN)
		}
	}
	
	//Register the keyboard for notifications in  viewDidLoad
	func registerForKeyboardNotifications(_ scrollView: UIScrollView) {
		// setup keyboard event
		NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow),
											   name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide),
											   name: UIResponder.keyboardWillHideNotification, object: nil)
		internalScrollView = scrollView
	}
	
	//Deregister the keyboard for notification in viewWillDisapper
	func deregisterFromKeyboardNotifications() {
		//Stop listening for keyboard hide/show events
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
		
	}
	
	@objc func keyboardWillShow(notification: NSNotification) {
		if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
			self.internalScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
		}
		
	}
	
	@objc func keyboardWillHide(notification: NSNotification) {
		UIView.animate(withDuration: 0.2, animations: {
			self.internalScrollView.contentOffset = .zero
			self.internalScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		})
	}
	
	@objc func dismissKeyboard() {
		view.endEditing(true)
	}
	
	func dismissKeyboardGesture() {
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
		view.addGestureRecognizer(tap)
	}
	
	func showAuthAlert(title: String, message: String, actions: [UIAlertAction]? = nil) -> UIAlertController {
		let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
		let titleAttributes = [NSAttributedString.Key.font: UIFont.sfProDisplay.semiBold(withSize: 20), NSAttributedString.Key.foregroundColor: ColorConstants.kPersonalDetailsTitleText]
		let titleString = NSAttributedString(string: title, attributes: titleAttributes)
		let messageAttributes = [NSAttributedString.Key.font: UIFont.sfProDisplay.regular(withSize: 16), NSAttributedString.Key.foregroundColor: UIColor.black]
		let messageString = NSAttributedString(string: message, attributes: messageAttributes)
		alert.setValue(titleString, forKey: "attributedTitle")
		alert.setValue(messageString, forKey: "attributedMessage")

		if let actions = actions {
			for each in actions {
				alert.addAction(each)
			}
		} else {
			alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
		}
		self.present(alert, animated: true, completion: nil)
		return alert
	}
	
	func showAlert(title: String, message: String, actions: [UIAlertAction]? = nil) {
		let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
		let titleAttributes = [NSAttributedString.Key.font: UIFont.sfProDisplay.semiBold(withSize: 20), NSAttributedString.Key.foregroundColor: ColorConstants.kPersonalDetailsTitleText]
		let titleString = NSAttributedString(string: title, attributes: titleAttributes)
		let messageAttributes = [NSAttributedString.Key.font: UIFont.sfProDisplay.regular(withSize: 16), NSAttributedString.Key.foregroundColor: UIColor.black]
		let messageString = NSAttributedString(string: message, attributes: messageAttributes)
		alert.setValue(titleString, forKey: "attributedTitle")
		alert.setValue(messageString, forKey: "attributedMessage")

		if let actions = actions {
			for each in actions {
				if each.style == .destructive {
					each.setValue(UIColor.outsideTargetColor, forKey: "titleTextColor")
				} else {
					each.setValue(UIColor.primaryColor, forKey: "titleTextColor")
				}
				alert.addAction(each)
			}
		} else {
			let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
			okAction.setValue(UIColor.primaryColor, forKey: "titleTextColor")
			alert.addAction(okAction)
		}
		self.present(alert, animated: true, completion: nil)
	}
	
	func showErrorAlert(error: MovanoError) {
		var actions = [UIAlertAction]()
		actions.append(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
			//self.navigationController?.popToRootViewController(animated: true)
		}))
		self.showAlert(title: "Error", message: error.errorMessage, actions: actions)
	}
	
	func showSecurityAlert() {
		
		var message = "Do you want to use\n"
		if BiometricAuthentication.shared.isFaceIDSupported {
			message.append("Face ID for login?")
		} else if BiometricAuthentication.shared.isTouchIDSupported {
			message.append("Touch ID for login?")
		}
		
		let alert = AlertController(title: "", message: message, preferredStyle: .alert)
		alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = UIColor.white
		if BiometricAuthentication.shared.isFaceIDSupported {
			alert.setTitleImage(#imageLiteral(resourceName: "FaceID"))
		} else if BiometricAuthentication.shared.isTouchIDSupported {
			alert.setTitleImage(#imageLiteral(resourceName: "TouchID"))
		}
		
		let titleAttributes = [NSAttributedString.Key.font: UIFont.sfProDisplay.semiBold(withSize: 20), NSAttributedString.Key.foregroundColor: ColorConstants.kPersonalDetailsTitleText]
		let titleString = NSAttributedString(string: "", attributes: titleAttributes)
		let messageAttributes = [NSAttributedString.Key.font: UIFont.sfProDisplay.bold(withSize: 18), NSAttributedString.Key.foregroundColor: UIColor.textTitleColor]
		let messageString = NSAttributedString(string: message, attributes: messageAttributes)
		alert.setValue(titleString, forKey: "attributedTitle")
		alert.setValue(messageString, forKey: "attributedMessage")
		
		let yesAction = UIAlertAction(title: "Yes", style: .default) { (_) in
			KeyChain.shared.currentUser?.isSecurityEnabled = true
			KeyChain.shared.currentUser?.remindLater = false
			self.firstTimeAuthentication()
		}
		yesAction.setValue(UIColor.primaryColor, forKey: "titleTextColor")
		alert.addAction(yesAction)
		
		let noAction = UIAlertAction(title: "No", style: .default) { (_) in
			KeyChain.shared.currentUser?.isSecurityEnabled = false
			KeyChain.shared.currentUser?.remindLater = false
			NavigationUtility.setInitialController()
		}
		noAction.setValue(UIColor.primaryColor, forKey: "titleTextColor")
		alert.addAction(noAction)
		
		let laterAction = UIAlertAction(title: "Remind Later", style: .default) { (_) in
			KeyChain.shared.currentUser?.isSecurityEnabled = false
			KeyChain.shared.currentUser?.remindLater = true
			NavigationUtility.setInitialController()
		}
		laterAction.setValue(UIColor.primaryColor, forKey: "titleTextColor")
		alert.addAction(laterAction)
		
		self.present(alert, animated: true, completion: nil)
	}
	
	func showUploadPopup(data: Data, completion: @escaping (() -> Void)) {
		
		var actions = [UIAlertAction]()
		
		actions.append(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
			completion()
		}))
		
		actions.append(UIAlertAction(title: "Discard", style: .cancel, handler: { (_) in
			self.navigationController?.popToRootViewController(animated: true)
		}))
		
		showAlert(title: "Movano", message: "Do you want to save and upload this measurement for \(KeyChain.shared.firstName)?", actions: actions)
	}
	
	func firstTimeAuthentication() {
		BiometricAuthentication().authenticateUser { (status, error) in
			if let movanoError = error {
				var actions = [UIAlertAction]()
				actions.append(UIAlertAction(title: "Go to Settings", style: .cancel, handler: { (_) in
					
					guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
						return
					}
					
					if UIApplication.shared.canOpenURL(settingsUrl) {
						UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
							print("Settings opened: \(success)") // Prints true
						})
					}
				}))
				self.showAlert(title: "Movano", message: movanoError.message, actions: actions)
			}
			if status {
				NavigationUtility.setInitialController()
			}
		}
	}
	
	func checkForAuthenticationSupport() {
		if KeyChain.shared.accessToken != nil {
			authenticateUser()
		} else {
			performBiometricLogin()
		}
	}
	
	func performBiometricLogin() {
		let faceIdAccounts = KeyChain.shared.userAccounts.filter { (account) -> Bool in
			return account.isSecurityEnabled
		}
		
		let faceIdAccount = faceIdAccounts.first
		if (faceIdAccount != nil) && KeyChain.shared.accessToken == nil {
			BiometricAuthentication.shared.loginUserWithBiometric(user: faceIdAccount!, currentView: self.view) { (_, error) in
				if let movanoError = error as? MovanoError {
					self.showAlert(title: "Movano", message: movanoError.errorMessage)
				}
			}
		} else {
		 let alert = AlertController(title: "", message: "No Face ID Found!\n", preferredStyle: .alert)
		 alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = UIColor.white
		 if BiometricAuthentication.shared.isFaceIDSupported {
			 alert.setTitleImage(#imageLiteral(resourceName: "FaceID"))
		 } else if BiometricAuthentication.shared.isTouchIDSupported {
			 alert.setTitleImage(#imageLiteral(resourceName: "TouchID"))
		 }
		 
		 let okAction = UIAlertAction(title: "OK", style: .default)
		 okAction.setValue(UIColor.primaryColor, forKey: "titleTextColor")
		 alert.addAction(okAction)
		 
		 self.present(alert, animated: true, completion: nil)
	 }
	}
	
	func authenticateUser() {
		if (KeyChain.shared.currentUser?.isSecurityEnabled ?? false) && KeyChain.shared.accessToken != nil {
			let blurEffect = UIBlurEffect(style: .light)
			let blurEffectView = UIVisualEffectView(effect: blurEffect)
			blurEffectView.frame = self.view.bounds
			self.view.addSubview(blurEffectView)
			self.view.bringSubviewToFront(blurEffectView)
			BiometricAuthentication.shared.authenticateUser { (status, error) in
				if let movanoError = error {
					self.showAlert(title: "Movano", message: movanoError.message)
				}
				
				if status {
					self.view.removeView(specificView: blurEffectView)
				}
			}
		}
	}
	
	func hideTabWhenPushed(controller: UIViewController) {
		controller.hidesBottomBarWhenPushed = true
		self.navigationController?.pushViewController(controller, animated: true)
	}
	
    func showMyDeviceScreenPopup(){
        var actions = [UIAlertAction]()
        actions.append(UIAlertAction(title: "Connect", style: .default, handler: { (_) in
            let controller = MyDeviceViewController.instantiate(fromAppStoryboard: .bluetooth)
            self.navigationController?.pushViewController(controller, animated: true)
        }))
        actions.append(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.showAlert(title: "Movano", message: "There’s no device connected.", actions: actions)
    }
	func showDevicePopup() {
		var actions = [UIAlertAction]()
		actions.append(UIAlertAction(title: "Pair Movano Ring", style: .default, handler: { (_) in
			let controller = DeviceListViewController.instantiate(fromAppStoryboard: .bluetooth)
			self.navigationController?.pushViewController(controller, animated: true)
		}))
		actions.append(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
		self.showAlert(title: "Movano", message: "There’s no device connected.", actions: actions)
	}
	
	func pushViewControllerFromTop(viewController vc: UIViewController) {
		vc.view.alpha = 0
		self.present(vc, animated: false) { () -> Void in
			vc.view.frame = CGRect(x: 0, y: -vc.view.frame.height, width: vc.view.frame.width, height: vc.view.frame.height)
			vc.view.alpha = 1
			UIView.animate(withDuration: 0.3,
						   animations: { () -> Void in
							vc.view.frame = CGRect(x: 0, y: 0, width: vc.view.frame.width, height: vc.view.frame.height)
						   },
						   completion: nil)
		}
	}
	
	func dismissViewControllerToTop() {
		if let vc = self.presentedViewController {
			UIView.animate(withDuration: 0.3,
						   animations: { () -> Void in
							vc.view.frame = CGRect(x: 0, y: -vc.view.frame.height, width: vc.view.frame.width, height: vc.view.frame.height)
						   },
						   completion: { complete -> Void in
								self.dismiss(animated: false, completion: nil)
						   })
		}
	}
}

// MARK: - UIViewControllerTransitioningDelegate
extension UIViewController: UIViewControllerTransitioningDelegate {
	public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
		if presented is CalendarView {
			return CalendarPresentationController(presentedViewController: presented, presenting: presenting)
		} else if presented is ActionSheetOverlayView {
			return PresentationController(presentedViewController: presented, presenting: presenting)
		}
		return nil
	}
}
