//
//  UILabel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 17/07/21.
//

import Foundation
import UIKit

extension UILabel {
	
	func addImage(image: UIImage) {
		let attachment:NSTextAttachment = NSTextAttachment()
		attachment.image = image

		let attachmentString:NSAttributedString = NSAttributedString(attachment: attachment)
		let myString:NSMutableAttributedString = NSMutableAttributedString(string: self.text! + "   ")
		myString.append(attachmentString)

		self.attributedText = myString
	}
	
	func errorText(text: String) {
		self.text = text
		self.isHidden = false
	}
}
