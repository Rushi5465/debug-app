//
//  UIFont.swift
//  KYM
//
//  Created by indexnine on 12/09/17.
//  Copyright © 2017 indexnine. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
	
	struct CustomFont {
		
		private var fontFamily: String!
		
		static let defaultFontSize: CGFloat = 12
		
		func regular(withSize size: CGFloat = CustomFont.defaultFontSize) -> UIFont {
			return UIFont(name: "\(fontFamily!)-Regular", size: size)!
		}
		
		func semiBold(withSize size: CGFloat = CustomFont.defaultFontSize) -> UIFont {
			return UIFont(name: "\(fontFamily!)-SemiBold", size: size)!
		}
		
		func bold(withSize size: CGFloat = CustomFont.defaultFontSize) -> UIFont {
			return UIFont(name: "\(fontFamily!)-Bold", size: size)!
		}
		
		func heavy(withSize size: CGFloat = CustomFont.defaultFontSize) -> UIFont {
			return UIFont(name: "\(fontFamily!)-Heavy", size: size)!
		}
		
		init(fontFamily: String) {
			self.fontFamily = fontFamily
		}
	}
	
	class var sfProDisplay: CustomFont {
		return CustomFont(fontFamily: "SFProDisplay")
	}
	
	class var gotham: CustomFont {
		return CustomFont(fontFamily: "Gotham")
	}
	
	class var openSans: CustomFont {
		return CustomFont(fontFamily: "OpenSans")
	}
	
	class var gilroy: CustomFont {
		return CustomFont(fontFamily: "Gilroy")
	}
}
