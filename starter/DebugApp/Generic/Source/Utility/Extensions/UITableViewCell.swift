//
//  UITableViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

extension UITableViewCell {
	
	static var IDENTIFIER: String {
		return String(describing: self)
	}
	
	static func dequeueReusableCell(tableView: UITableView, indexPath: IndexPath) -> Self {
		return Self.instantiate(tableView: tableView, indexPath: indexPath)
	}
	
	static func instantiate<T: UITableViewCell>(tableView: UITableView, indexPath: IndexPath, function: String = #function, line: Int = #line, file: String = #file) -> T {
		guard let scene = tableView.dequeueReusableCell(withIdentifier: Self.IDENTIFIER, for: indexPath) as? T else {
			fatalError("Error")
		}
		return scene
	}
	
	func roundCorner(for indexPath: IndexPath, radius: CGFloat, count: Int) {
		if indexPath.row == 0 {
			self.roundCorners([.layerMaxXMinYCorner, .layerMinXMinYCorner], radius: radius)
		} else if indexPath.row == count - 1 {
			self.roundCorners([.layerMaxXMaxYCorner, .layerMinXMaxYCorner], radius: radius)
		} else {
			self.layer.cornerRadius = 0
		}
	}
}
