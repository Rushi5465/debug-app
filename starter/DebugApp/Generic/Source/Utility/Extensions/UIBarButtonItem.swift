//
//  UIBarButtonItem.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 22/12/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit

extension UIBarButtonItem {
	
	func addMovanoProperties() {
		self.tintColor = UIColor.primaryColor
		if self.style == .done {
			self.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.sfProDisplay.bold(withSize: 18)], for: .normal)
		} else {
			self.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.sfProDisplay.regular(withSize: 18)], for: .normal)
		}
	}
}
