//
//  UIView.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 09/11/21.
//

import Foundation
import UIKit

extension UIView {

    @IBInspectable var viewCornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue

            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            self.layer.masksToBounds = true
            
        }
    }

}
