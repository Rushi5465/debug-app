//
//  Array.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 23/09/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

extension Array where Element == UserAccount {
	
	func add(_ element: Element) -> [Element] {
		var list = self
		let item = list.filter({ (account) -> Bool in
			return account.username == element.username
		})
		
		if !item.isEmpty {
			for each in item {
				list.remove(at: list.firstIndex(of: each)!)
			}
		}
		list.append(element)
		
		return list
	}
	
	func indexOf(_ username: String) -> Int? {
		let list = self
		let item = list.filter({ (account) -> Bool in
			return account.username == username
		})
		
		if !item.isEmpty {
			if let firstItem = item.first {
				return list.firstIndex(of: firstItem)
			}
		}
		return nil
	}
}

extension Array where Element == Reading {
	
	func getStepCountArray(for interval: Int, component: Calendar.Component) -> [Element] {
		let data = self.sorted { $0.timeStamp < $1.timeStamp }
		
		guard let initialData = data.first, let lastData = data.last else {
			return data
		}
        let startTime = Int(initialData.timeStamp)
        let endTime = Int(lastData.timeStamp)
        
        let iterationCount = (endTime - startTime)/300
        var timeIntervalArray:[Int] = []
        timeIntervalArray.append(startTime)
        
        if(iterationCount > 0){
            for index in 1 ..< iterationCount{
                timeIntervalArray.append(startTime + (300 * index))
            }
            
            if let lastEle = timeIntervalArray.last{
                if(lastEle != endTime && lastEle < endTime){
                    timeIntervalArray.append(endTime)
                }
            }
        }else{
            timeIntervalArray.append(endTime)
        }
        
        
        //let roundStartToNeaestPrev5 = floor(initialData.timeStamp/300.0)*300
        //let rountToNearest5Minutes = ceil(lastData.timeStamp/300.0)*300.0

       // let dateArray = Date.dates(from: Date(timeIntervalSince1970: initialData.timeStamp), to: Date(timeIntervalSince1970: lastData.timeStamp), interval: interval, component: component)
        
//        if let lastEle = dateArray.last{
//            let timeIntLast = lastEle.timeIntervalSince1970
//            if(lastEle.timeIntervalSince1970 < lastData.timeStamp){
//                dateArray.append(Date(timeIntervalSince1970: timeIntLast + 300.0))
//            }
//
//        }
        		
		var output = [Element]()
//		for index in 0 ..< dateArray.count {
//			var to_ts: TimeInterval!
//			if index == dateArray.count - 1 {
//				to_ts = lastData.timeStamp
//			} else {
//                let modDate = Calendar.current.date(byAdding: .second, value: -1, to: dateArray[index + 1]) ?? dateArray[index + 1]
//				to_ts = modDate.timeIntervalSince1970
//			}
//
//			let manipulatedData = data.filter({
//				($0.timeStamp >= dateArray[index].timeIntervalSince1970 && $0.timeStamp <= to_ts )
//			})
//
//			let sumArray = manipulatedData.reduce(0) { (result: Double, nextItem: Reading) -> Double in
//				return result + nextItem.reading
//			}
//
//			output.append(Reading(timeStamp: dateArray[index].timeIntervalSince1970, reading: sumArray))
//		}
        for index in 0 ..< timeIntervalArray.count {
            var to_ts: Int
            if index == timeIntervalArray.count - 1 {
                to_ts = endTime
            } else {
                to_ts = timeIntervalArray[index + 1] - 1
            }
            
            let manipulatedData = data.filter({
                (Int($0.timeStamp) >= timeIntervalArray[index] && Int($0.timeStamp) <= to_ts )
            })
            
            let sumArray = manipulatedData.reduce(0) { (result: Double, nextItem: Reading) -> Double in
                return result + nextItem.reading
            }
            
            output.append(Reading(timeStamp: TimeInterval(timeIntervalArray[index]), reading: sumArray))
        }
		return output
	}
	
	func getAverageDataArray(for interval: Int, component: Calendar.Component) -> [Element] {
		let data = self.sorted { $0.timeStamp < $1.timeStamp }
		
		guard let initialData = data.first, let lastData = data.last else {
			return data
		}
        let startTime = Int(initialData.timeStamp)
        let endTime = Int(lastData.timeStamp)
        
        let iterationCount = (endTime - startTime)/300
        var timeIntervalArray:[Int] = []
        timeIntervalArray.append(startTime)
        
        if(iterationCount > 0){
            for index in 1 ..< iterationCount{
                timeIntervalArray.append(startTime + (300 * index))
            }
            
            if let lastEle = timeIntervalArray.last{
                if(lastEle != endTime && lastEle < endTime){
                    timeIntervalArray.append(endTime)
                }
            }
        }else{
            timeIntervalArray.append(endTime)
        }
        
        //let rountToNearest5Minutes = ceil(lastData.timeStamp/300.0)*300.0
       // var dateArray = Date.dates(from: Date(timeIntervalSince1970: initialData.timeStamp), to: Date(timeIntervalSince1970: lastData.timeStamp), interval: interval, component: component)
//        if let lastEle = dateArray.last{
//            if(lastEle.timeIntervalSince1970 < lastData.timeStamp){
//                dateArray.append(Date(timeIntervalSince1970: lastEle.timeIntervalSince1970 + 300.0))
//            }
//
//        }
		var output = [Element]()
//		for index in 0 ..< dateArray.count {
//			var to_ts: TimeInterval!
//			if index == dateArray.count - 1{
//				to_ts = lastData.timeStamp
//			} else {
//                let modDate = Calendar.current.date(byAdding: .second, value: -1, to: dateArray[index + 1]) ?? dateArray[index + 1]
//                to_ts = modDate.timeIntervalSince1970
//			}
//
//			let manipulatedData = data.filter({
//				($0.timeStamp >= dateArray[index].timeIntervalSince1970 && $0.timeStamp <= to_ts )
//			})
//
//			let sumArray = manipulatedData.reduce(0) { (result: Double, nextItem: Reading) -> Double in
//				return result + nextItem.reading
//			}
//			var averageTemp = sumArray / Double(manipulatedData.count)
//			averageTemp = (averageTemp * 10).rounded() / 10.0
//
//			if averageTemp.isNaN {
//				averageTemp = 0
//			}
//
//			output.append(Reading(timeStamp: dateArray[index].timeIntervalSince1970, reading: averageTemp))
//		}
        
        for index in 0 ..< timeIntervalArray.count {
            var to_ts: Int
            if index == timeIntervalArray.count - 1{
                to_ts = endTime
            } else {
                to_ts = timeIntervalArray[index + 1] - 1
            }
            
            let manipulatedData = data.filter({
				(Int($0.timeStamp) >= timeIntervalArray[index] && Int($0.timeStamp) <= to_ts && ($0.reading != 0.0) && ($0.reading != nil))
            })
            
            let sumArray = manipulatedData.reduce(0) { (result: Double, nextItem: Reading) -> Double in
                return result + nextItem.reading
            }
			
            var averageTemp = sumArray / Double(manipulatedData.count)
            averageTemp = (averageTemp * 10).rounded() / 10.0
            
            if averageTemp.isNaN {
                averageTemp = 0
            }
            
            output.append(Reading(timeStamp: TimeInterval(timeIntervalArray[index]), reading: averageTemp))
        }
		return output
	}
}
