//
//  UITextFieldExtensions.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 14/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import UIKit

private var __maxLengths = [UITextField: Int]()
extension UITextField {
	
	@IBInspectable var maxLength: Int {
		get {
			guard let l = __maxLengths[self] else {
				return 150 // (global default-limit. or just, Int.max)
			}
			return l
		}
		set {
			__maxLengths[self] = newValue
			addTarget(self, action: #selector(fix), for: .editingChanged)
		}
	}
	@objc func fix(textField: UITextField) {
		let t = textField.text
		textField.text = t?.safelyLimitedTo(length: maxLength)
	}
	
	@IBInspectable
	var cornerRadius: CGFloat {
		get {
			return layer.cornerRadius
		}
		set {
			layer.cornerRadius = newValue
			layer.masksToBounds = newValue > 0
		}
	}
	
	//Padding
	enum PaddingSide {
		case left(CGFloat)
		case right(CGFloat)
		case both(CGFloat)
	}
	
	func addPadding(_ padding: PaddingSide) {
		
		self.leftViewMode = .always
		self.layer.masksToBounds = true
		
		switch padding {
			
			case .left(let spacing):
				let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
				self.leftView = paddingView
				self.rightViewMode = .always
			
			case .right(let spacing):
				let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
				self.rightView = paddingView
				self.rightViewMode = .always
			
			case .both(let spacing):
				let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
				// left
				self.leftView = paddingView
				self.leftViewMode = .always
				// right
				self.rightView = paddingView
				self.rightViewMode = .always
		}
	}
	
	enum ViewType {
		case left, right
	}
	
	// (1)
	func setView(_ type: ViewType, with view: UIView) {
		if type == ViewType.left {
			leftView = view
			leftViewMode = .always
		} else if type == .right {
			rightView = view
			rightViewMode = .always
		}
	}
	
	func setView(_ view: ViewType, image: UIImage?) -> UIButton {
		let size = 20
		let button = UIButton(frame: CGRect(x: 0, y: 0, width: size*2, height: size*2))
		button.setImage(image, for: .normal)
		button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
		button.imageView!.contentMode = .scaleAspectFit
		button.isUserInteractionEnabled = true
		button.isMultipleTouchEnabled = true
		setView(view, with: button)
		return button
	}
	
	func setImage(_ view: ViewType, image: UIImage?) {
		let size = 20
		let outerView = UIView(frame: CGRect(x: 0, y: 0, width: size*2, height: size*2) )
		let iconView  = UIImageView(frame: CGRect(x: 10, y: 10, width: size, height: size))
		iconView.image = image
		iconView.contentMode = .scaleAspectFit
		outerView.addSubview(iconView)
		setView(view, with: outerView)
	}
	
	func setInputView(isDatePicker: Bool, target: Any, selector: Selector) {
		// Create a UIDatePicker object and assign to inputView
		let screenWidth = UIScreen.main.bounds.width
		if isDatePicker {
			let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
			if #available(iOS 13.4, *) {
				datePicker.preferredDatePickerStyle = .wheels
			}
			datePicker.datePickerMode = .date //2
			datePicker.maximumDate = Date()
			datePicker.backgroundColor = UIColor.white
			datePicker.setValue(UIColor.textTitleColor, forKeyPath: "textColor")
			self.inputView = datePicker //3
		} else {
			let picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
			self.inputView = picker
		}
		
		// Create a toolbar and assign it to inputAccessoryView
		let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
		let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
		let cancel = UIBarButtonItem(title: "ButtonCancel".localized, style: .plain, target: nil, action: #selector(tapCancel)) // 6
		cancel.addMovanoProperties()
		
		let barButton = UIBarButtonItem(title: "ButtonDone".localized, style: .done, target: target, action: selector) //7
		barButton.addMovanoProperties()
		
		toolBar.backgroundColor = ColorConstants.kNavigationBackgroundColor
		toolBar.setItems([cancel, flexible, barButton], animated: false) //8
		self.inputAccessoryView = toolBar //9
	}
	
	@objc func tapCancel() {
		self.resignFirstResponder()
	}
	
	func showErrorView(image: UIImage? = nil) {
		if let errorIcon = image {
			self.setImage(.left, image: errorIcon)
		}
		self.borderColor = ColorConstants.kColorTextFieldErrorBorder
	}
	
	func showNormalView(image: UIImage? = nil) {
		if let icon = image {
			self.setImage(.left, image: icon)
		}
		self.borderColor = ColorConstants.kColorTextFieldBorder
	}
	
	func showToolBar() {
		// Create a toolbar and assign it to inputAccessoryView
		let screenWidth = UIScreen.main.bounds.width
		let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
		let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
		let barButton = UIBarButtonItem(title: "ButtonDone".localized, style: .done, target: target, action: #selector(tapDone)) //7
		barButton.addMovanoProperties()
		
		toolBar.setItems([flexible, barButton], animated: false) //8
		toolBar.backgroundColor = ColorConstants.kNavigationBackgroundColor
		self.inputAccessoryView = toolBar //9
	}
	
	@objc func tapDone() {
		self.resignFirstResponder()
	}
	
	func addBottomBorder() {
		let bottomLine = CALayer()
		bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
		bottomLine.backgroundColor = UIColor.white.cgColor
		borderStyle = .none
		layer.addSublayer(bottomLine)
	}
}
