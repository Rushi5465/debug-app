//
//  BundleExtensions.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 09/04/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

extension Bundle {
	
	var releaseVersionNumber: String {
		return infoDictionary?["CFBundleShortVersionString"] as! String
	}
	
	var buildVersionNumber: String {
		return infoDictionary?["CFBundleVersion"] as! String
	}
	
	var releaseVersionNumberPretty: String {
		return "App version : \(releaseVersionNumber)" + "(" + "\(buildVersionNumber)" + ")"
	}
}
