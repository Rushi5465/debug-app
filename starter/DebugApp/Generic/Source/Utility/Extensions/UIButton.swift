//
//  UIButton.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 18/08/21.
//

import Foundation
import UIKit

extension UIButton {
	
	func interaction(enabled: Bool) {
		self.isUserInteractionEnabled = enabled
		self.alpha = enabled ? 1.0 : 0.5
	}
}
