//
//  NavigationUtility.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 05/12/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import UIKit

class NavigationUtility {
	
	enum UserState: String {
		case signedUp
		case signUpVerified
		case loggedIn
		case verified
		case onBoarded
		case logout
	}
	
	//Set Root View Controller
	class func setRootViewController(viewController: UIViewController) {
		if let appDelegate = UIApplication.shared.delegate {
			if let window = appDelegate.window {
				let navigationController = UINavigationController(rootViewController: viewController)
				navigationController.setNavigationBarHidden(true, animated: true)
				window?.rootViewController = navigationController
			}
		}
	}
	
	class func setInitialController() {
		if KeyChain.shared.firstName != "" {
			let homeViewController = UserViewController.instantiate(fromAppStoryboard: .bluetooth)
			self.setRootViewController(viewController: homeViewController)
		} else {
			let homeViewController = StartExerciseViewController.instantiate(fromAppStoryboard: .dashboard)
			self.setRootViewController(viewController: homeViewController)
		}
	}
	
}
