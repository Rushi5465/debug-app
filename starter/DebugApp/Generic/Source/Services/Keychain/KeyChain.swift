//
//  KeyChain.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import KeychainSwift

// swiftlint:disable line_length
// swiftlint:disable all
class KeyChain {
	
	private var keychain: KeychainSwift
	
	private static var singleInstance: KeyChain?
	
	static var shared: KeyChain {
		if singleInstance == nil {
			singleInstance = KeyChain()
		}
		return singleInstance!
	}
	
	private init() {
		keychain = KeychainSwift()
	}
	
	var firstOpen: Bool {
		get {
			if let firstOpen = keychain.getBool("firstOpen") {
				return firstOpen
			}
			return true
		}
		set {
			keychain.set(newValue, forKey: "firstOpen")
		}
	}
	
	func saveLoginResponse(response: LoginResponse) {
		Logger.shared.addLog("Saving logging response to keychain")
		if let sessionId = response.session {
			keychain.set(sessionId, forKey: "session")
		}
		if let challengeName = response.challengeName {
			keychain.set(challengeName, forKey: "challengeName")
		}
		if let username = response.usernameForSrp {
			Logger.shared.addLog("usernameForSrp in keychain is \(username)")
			keychain.set(username, forKey: "usernameForSrp")
		}
		if let mobile = response.mobilenumber {
			keychain.set(mobile, forKey: "registeredPhone")
		}
	}
	
	func saveUserNameSrp(usernameSrp: String) {
		keychain.set(usernameSrp, forKey: "usernameForSrp")
	}
	
	func saveUserDetails(details: UserDetails) {
		if let dob = details.dateOfBirth?.toDate(format: "MM/dd/yyyy") {
			keychain.set(dob.dateToString(format: DateFormat.birthdateFormat), forKey: dateOfBirth)
		}
		
		if let gender = details.gender {
			keychain.set(gender, forKey: "gender")
		}
		
		if let weight = details.weight {
			keychain.set(String(weight), forKey: "weight")
		}
		if let weightUnits = details.weightUnits {
			keychain.set(weightUnits, forKey: "weightUnits")
		}
		
		if let heightInInches = details.heightInInches {
			keychain.set(String(heightInInches), forKey: "height")
		}
		
		if let userId = details.userId {
			keychain.set(userId, forKey: "userId")
		}
		
		if let baselineBodyTemprature = details.baselineBodyTemprature {
			keychain.set(String(baselineBodyTemprature), forKey: "baselineBodyTemprature")
		}
	}
	
	var mfa: Bool {
		get {
			if let mfa = keychain.getBool("mfa") {
				return mfa
			}
			return false
		}
		set {
			keychain.set(newValue, forKey: "mfa")
		}
	}
	
	var isFaceID: Bool {
		get {
			if let faceId = keychain.getBool("faceId") {
				return faceId
			}
			return false
		}
		set {
			keychain.set(newValue, forKey: "faceId")
		}
	}
	
	var isTouchID: Bool {
		get {
			if let touchId = keychain.getBool("touchId") {
				return touchId
			}
			return false
		}
		set {
			keychain.set(newValue, forKey: "touchId")
		}
	}
	
	var userId: String {
		if let userId = keychain.get("userId") {
			return userId
		}
		return ""
	}
	
	var lowGlucoseValue: String {
		get {
			if let lowGlucoseValue = keychain.get("lowGlucoseValue") {
				return lowGlucoseValue
			}
			return ""
		}
		set {
			keychain.set(newValue, forKey: "lowGlucoseValue")
		}
	}
	
	var highGlucoseValue: String {
		get {
			if let highGlucoseValue = keychain.get("highGlucoseValue") {
				return highGlucoseValue
			}
			return ""
		}
		set {
			keychain.set(newValue, forKey: "highGlucoseValue")
		}
	}
	
	var email: String? {
		get {
			if let username = keychain.get("username") {
				return username
			}
			return nil
		}
		set {
			keychain.set(newValue!, forKey: "username")
		}
	}
	
	var firstName: String {
		get {
			if let firstname = keychain.get("firstname") {
				return firstname.trim
			}
			return ""
		}
		set {
			keychain.set(newValue, forKey: "firstname")
		}
	}
	
	var lastName: String {
		get {
			if let lastname = keychain.get("lastname") {
				return lastname.trim
			}
			return ""
		}
		set {
			keychain.set(newValue, forKey: "lastname")
		}
	}
	
	var fullName: String {
		get {
			if let fullname = keychain.get("fullname") {
				return fullname
			}
			return ""
		}
		set {
			keychain.set(newValue, forKey: "fullname")
		}
	}
	
	var password: String {
		get {
			if let password = keychain.get("password") {
				return password
			}
			return ""
		}
		set {
			keychain.set(newValue, forKey: "password")
		}
	}
	
	var currentUser: UserAccount? {
		get {
			if let data = keychain.getData("currentUser") {
				let user = try? PropertyListDecoder().decode(UserAccount.self, from: data)
				return user!
			}
			return nil
		}
		set {
			if let value = newValue {
				var addedAccount = KeyChain.shared.userAccounts
				addedAccount = addedAccount.add(value)
				KeyChain.shared.userAccounts = addedAccount
				
				keychain.set(try! PropertyListEncoder().encode(value), forKey: "currentUser")
			} else {
				keychain.delete("currentUser")
			}
		}
	}
	
	var userAccounts: [UserAccount] {
		get {
			if let data = keychain.getData("userAccounts") {
				let userList = try? PropertyListDecoder().decode([UserAccount].self, from: data)
				return userList!
			}
			return []
		}
		set {
			keychain.set(try! PropertyListEncoder().encode(newValue), forKey: "userAccounts")
		}
	}
	
	func saveUserSession(response: UserSession) {
		let defaults = UserDefaults.standard
		defaults.set(true, forKey: "DataSavedToKeychain")
		
		if let idToken = response.idToken {
			keychain.set(idToken, forKey: "idToken")
		}
		if let accessToken = response.accessToken {
			keychain.set(accessToken, forKey: "accessToken")
		}
		if let refreshToken = response.refreshToken {
			keychain.set(refreshToken, forKey: "refreshToken")
		}
		if let usernameForSrp = response.usernameForSrp {
			keychain.set(usernameForSrp, forKey: "usernameForSrp")
		}
	}
	
	func saveUserSession(model: UserSessionModel) {
		let defaults = UserDefaults.standard
		defaults.set(true, forKey: "DataSavedToKeychain")
		
		keychain.set(model.id_token, forKey: "idToken")
		keychain.set(model.access_token, forKey: "accessToken")
		keychain.set(model.refresh_token, forKey: "refreshToken")
		keychain.set(model.username_for_srp, forKey: "usernameForSrp")
	}
	
	func saveAccessToken(response: UserSession) {
		if let accessToken = response.accessToken {
			keychain.set(accessToken, forKey: "accessToken")
		}
		if let idToken = response.idToken {
			keychain.set(idToken, forKey: "idToken")
		}
	}
	
	var userState: String? {
		get {
			if let userState = keychain.get("userState") {
				return userState
			}
			return nil
		}
		set {
			keychain.set(newValue!, forKey: "userState")
		}
	}
	
	var currentOnboardingPage: String {
		get {
			if let page = keychain.get("page") {
				return page
			}
			return ""
		}
		set {
			keychain.set(newValue, forKey: "page")
		}
	}
	
	func setOnBoardingComplete(user: String) {
		keychain.set(true, forKey: user)
	}
	
	func setfalseOnBoardingComplete(user: String) {
		keychain.set(false, forKey: user)
	}
	
	var passcode: String {
		get {
			if let passcode = keychain.get("passcode" + (email ?? "")) {
				return passcode
			}
			return ""
		}
		set {
			keychain.set(newValue, forKey: "passcode" + (email ?? ""))
		}
	}
	
	var accessToken: String? {
		if let accessToken = keychain.get("accessToken") {
			return accessToken
		}
		return nil
	}
	
	var idToken: String {
		if let idToken = keychain.get("idToken") {
			return idToken
		}
		return ""
	}
	
	var refreshToken: String {
		if let refreshToken = keychain.get("refreshToken") {
			return refreshToken
		}
		return ""
	}
	
	var sessionId: String? {
		if let session = keychain.get("session") {
			return session
		}
		return nil
	}
	
	var challengeName: String {
		if let challengeName = keychain.get("challengeName") {
			return challengeName
		}
		return ""
	}
	
	var usernameSrp: String {
		if let usernameForSrp = keychain.get("usernameForSrp") {
			return usernameForSrp
		}
		return ""
	}
	
	var registeredPhone: String {
		if let registeredPhone = keychain.get("registeredPhone") {
			return registeredPhone
		}
		return ""
	}
	
	var dateOfBirth: String {
		if let dateOfBirth = keychain.get("dateOfBirth") {
			return dateOfBirth
		}
		return ""
	}
	
	var gender: String {
		if let gender = keychain.get("gender") {
			return gender
		}
		return ""
	}
	
	var height: String? {
		if let height = keychain.get("height") {
			return height
		}
		return nil
	}
	
	var weight: String? {
		if let weight = keychain.get("weight") {
			return weight
		}
		return nil
	}
	
	var weightUnit: String {
		if let weightUnits = keychain.get("weightUnits") {
			return weightUnits
		}
		return "lbs"
	}
	
	var baselineBodyTemprature: String? {
		if let baselineBodyTemprature = keychain.get("baselineBodyTemprature") {
			return baselineBodyTemprature
		}
		return nil
	}
	
	func isOnBoardingComplete() -> Bool? {
		if let emailVal = email {
			return keychain.getBool(emailVal)
		}
		return false
	}
	
	func clear() {
		keychain.clear()
	}
	
	func clearTokens() {
		//return keychain.clear()
        keychain.delete("userId")
		keychain.delete("session")
		keychain.delete("challengeName")
		keychain.delete("usernameForSrp")
		keychain.delete("registeredPhone")
		keychain.delete("userState")
		//keychain.delete("passcode")
		keychain.delete("page")
		keychain.delete("accessToken")
		keychain.delete("idToken")
		keychain.delete("fullName")
		keychain.delete("refreshToken")
		//        keychain.delete("password")
		keychain.delete("dateOfBirth")
		keychain.delete("gender")
		keychain.delete("height")
		keychain.delete("weight")
		keychain.delete("weightUnits")
		keychain.delete("lowGlucoseValue")
		keychain.delete("highGlucoseValue")
	}
}
