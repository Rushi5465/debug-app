//
//  RestService.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 20/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum HTTPResponseCode: Int {
	case ok = 200
	case badRequest = 400
	case unauthorized = 401
	case forbidden = 403
	case notFound = 404
	case versionError = 600
}

enum MovanoError: LocalizedError, Equatable {
	
	case invalidResponseModel
	case invalidRequestURL
	case noInternetConnection
	case experiencingIssue
	case failedRequest(description: String)
	case notAbleToZipFile
	case notFetchingData
	case requestTimeOut
	case canNotSendMail
	
	var errorMessage: String {
		switch self {
			case .failedRequest(let description):
				return description
			case .invalidResponseModel:
				return "Invalid Response"
			case .invalidRequestURL:
				return "Invalid URL"
			case .noInternetConnection:
				return "The internet connection appears to be offline. Please check internet connectivity."
			case .experiencingIssue:
				return "We are experincing some issues, we'll fix this soon."
			case .notAbleToZipFile:
				return "There's some issue with the data you have received."
			case .notFetchingData:
				return "Not able to fetch data from the device. Please reconnect."
			case .requestTimeOut:
				return "We are experincing some issue."
			case .canNotSendMail:
				return "Your device could not send e-mail.  Please check e-mail configuration and try again."
		}
	}
	
}

class Connectivity {
	class func isConnectedToInternet() -> Bool {
		return NetworkReachabilityManager()!.isReachable
	}
}

class RestService {
	
	var method: HTTPMethod?
	var url: String?
	var response: HTTPURLResponse?
	var apiType: String?
	var result: String?
	var error: Error?
	var header: String?
	var param: String?
	var statusCode: NSNumber?
	var output: Any?
	var session: Alamofire.Session?
	
	static func getAuthHeaderToken() -> String {
		return KeyChain.shared.idToken
	}
	
	static var shared: RestService {
		return RestService(AF)
	}
	
	init(_ session: Alamofire.Session = AF) {
		self.session = session
	}
	
	func requestToken(apiType: String = "", request: NetworkRequest? = nil, tokenCompletion: @escaping (Result<JSON, Error>) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		self.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if var apiRequest = request {
						var thisHeader = apiRequest.header
						
						thisHeader?["AccessToken"] = userSession.accessToken
						thisHeader?["Authorization"] = userSession.idToken
						apiRequest.header = thisHeader
						
						self.request(apiType: apiType, request: apiRequest) { (result) in
							switch result {
								case .success(let response):
									tokenCompletion(.success(response))
								case .failure(let error):
									tokenCompletion(.failure(error))
							}
						}
					} else {
						tokenCompletion(.success(response))
					}
				
				case .failure(let error):
					tokenCompletion(.failure(error))
			}
		}
	}
	
	func request(apiType: String, request: NetworkRequest, completion: @escaping (Result<JSON, MovanoError>) -> Void) {
		
		self.apiType = apiType
		self.method = request.method
		self.url = request.url.absoluteString
		self.header = "".jsonString(dictionary: request.header?.dictionary as Any)
		self.param = "".jsonString(dictionary: request.param as Any)
		self.statusCode = NSNumber(value: 0)
		
		if Connectivity.isConnectedToInternet() {
			session?.request(request.url.absoluteString, method: request.method, parameters: request.param, encoding: JSONEncoding.default, headers: request.header).responseJSON { (response) -> Void in
				
				self.error = response.error
				
				let statusCode = response.response?.statusCode
				if statusCode != nil {
					self.statusCode = NSNumber(value: statusCode!)
				}
				
				if let data = response.data {
					let json = JSON(data)
					self.output = json
					
					if statusCode == HTTPResponseCode.ok.rawValue {
						completion(.success(json))
					} else if statusCode == HTTPResponseCode.badRequest.rawValue {
						let message = json["error_message"].stringValue
						let exceptionMessage = json["errorMessage"].stringValue
						let data = exceptionMessage.data(using: .utf8)!
						do {
							if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String: Any] {
								let error = MovanoError.failedRequest(description: jsonArray["error_message"]! as! String)
								completion(.failure(error))
							} else {
								let error = MovanoError.invalidResponseModel
								completion(.failure(error))
							}
						} catch let error as NSError {
							if message != "" {
								let error = MovanoError.failedRequest(description: message)
								completion(.failure(error))
							} else {
								let error = MovanoError.failedRequest(description: error.localizedDescription)
								completion(.failure(error))
							}
						}
					} else if statusCode == HTTPResponseCode.unauthorized.rawValue {
						self.requestToken(apiType: apiType, request: request) { (tokenResult) in
							switch tokenResult {
							case .success(let response):
								var header = Header()
								header.addHeader(key: "Authorization", value: KeyChain.shared.idToken)
								header.addHeader(key: "AccessToken", value: KeyChain.shared.accessToken!)
								var newRequest = request
								newRequest.header = header.allHeaders
								self.request(apiType: apiType, request: newRequest, completion: completion)
								completion(.success(response))
							case .failure(let error):
								completion(.failure(MovanoError.failedRequest(description: error.localizedDescription)))
							}
						}
					} else {
						
						if response.error != nil {
							let movanoError = MovanoError.noInternetConnection
							completion(.failure(movanoError))
						} else {
							completion(.failure(MovanoError.failedRequest(description: "Server Internal Issue. We will get it fix soon.")))
						}
					}
				}
				Logger.shared.addLog(self.toString())
			}
		} else {
			completion(.failure(MovanoError.noInternetConnection))
		}
	}
	
	func uploadFile(apiType: String, request: NetworkRequest, fileUrl: URL, completion: @escaping (Result<JSON, MovanoError>) -> Void) {
		
		reset()
		self.apiType = apiType
		self.method = request.method
		self.url = request.url.absoluteString
		self.header = "".jsonString(dictionary: request.header?.dictionary as Any)
		self.param = "".jsonString(dictionary: request.param as Any)
		self.statusCode = NSNumber(value: 0)
		
		if Connectivity.isConnectedToInternet() {
			session?.upload(fileUrl, to: request.url, method: request.method).responseJSON(completionHandler: { (response) in
				
				self.error = response.error
				
				let statusCode = response.response?.statusCode
				if statusCode != nil {
					self.statusCode = NSNumber(value: statusCode!)
				}
				
				var json = JSON()
				if let data = response.data {
					json = JSON(data)
					self.output = json
				}
				
				if statusCode == HTTPResponseCode.ok.rawValue {
					completion(.success(json))
				} else if statusCode == HTTPResponseCode.badRequest.rawValue {
					let message = json["error_message"].stringValue
					let error = MovanoError.failedRequest(description: message)
					completion(.failure(error))
				} else {
					
					if response.error != nil {
						let movanoError = MovanoError.noInternetConnection
						completion(.failure(movanoError))
					}
				}
				
				Logger.shared.addLog(self.toString())
			})
		} else {
			let error = MovanoError.noInternetConnection
			completion(.failure(error))
		}
	}
	
	func downloadFile(apiType: String, request: NetworkRequest, destination: @escaping DownloadRequest.Destination, completion: @escaping (Result<URL, MovanoError>) -> Void) {
		
		reset()
		self.apiType = apiType
		self.method = request.method
		self.url = request.url.absoluteString
		self.header = "".jsonString(dictionary: request.header?.dictionary as Any)
		self.param = "".jsonString(dictionary: request.param as Any)
		self.statusCode = NSNumber(value: 0)
		
		if Connectivity.isConnectedToInternet() {
			session?.download(request.url, to: destination).response { response in
				let statusCode = response.response?.statusCode
				if statusCode != nil {
					self.statusCode = NSNumber(value: statusCode!)
				}
				
				if let response = response.fileURL {
					completion(.success(response))
				}
				
				if let error = response.error {
					completion(.failure(MovanoError.failedRequest(description: error.errorDescription!)))
				}
				
				Logger.shared.addLog(self.toString())
			}
		} else {
			completion(.failure(.noInternetConnection))
		}
	}
	
	func toString() -> String {
		var stringValue = "------------------------------------------------------------\n"
		if let apiType = self.apiType {
			stringValue.append("API : \(apiType)\n")
		}
		if let url = self.url {
			stringValue.append("URL : \(url)\n")
		}
		if let method = self.method {
			stringValue.append("METHOD : \(method)\n")
		}
		if let param = self.param {
			stringValue.append("PARAM: \(param)\n")
		}
		if let header = self.header {
			stringValue.append("HEADER: \(header)\n")
		}
		if let statusCode = self.statusCode {
			stringValue.append("STATUS CODE: \(statusCode)\n")
		}
		if let result = self.result {
			stringValue.append("RESULT : \(result)\n")
		}
		if let output = self.output {
			stringValue.append("OUTPUT: \(output)\n")
		}
		if let error = self.error {
			stringValue.append("ERROR : \(error)\n")
		}
		stringValue.append("------------------------------------------------------------\n")
		
		return stringValue
	}
	
	func reset() {
		self.method = nil
		self.url = nil
		self.response = nil
		self.apiType = nil
		self.result = nil
		self.error = nil
		self.header = nil
		self.param = nil
		self.statusCode = nil
		self.output = nil
	}
}
