//
//  ApolloClientService.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 30/12/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import Apollo

class ApolloClientService {
    //static let shared = ApolloClientService()
    
	let graphEndpoint = Config.shared.currentServer.graphQLUrl
	var client: ApolloClient?
	
    init(_ networkTransport: NetworkTransport? = nil) {
		resetClient(networkTransport)
	}
	
	func resetClient(_ networkTransport: NetworkTransport? = nil) {
		if networkTransport == nil {
			self.setDefaultClient()
		} else {
			self.client = ApolloClient(networkTransport: networkTransport!)
		}
	}

	func setDefaultClient() {
		self.client = {
			let authPayloads = ["Authorization": "\(KeyChain.shared.accessToken!)"]
			let configuration = URLSessionConfiguration.default
			configuration.httpAdditionalHeaders = authPayloads
			let endpointURL = URL(string: graphEndpoint)!
			return ApolloClient(networkTransport: HTTPNetworkTransport(url: endpointURL, session: URLSession(configuration: configuration)))
		}()
	}
}
