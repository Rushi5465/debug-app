//
//  TabViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 19/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import Highcharts
import SwiftQueue
import Reachability


class TabSegue: UIStoryboardSegue {
	override func perform() {
		// Leave empty (we override prepareForSegue)
	}
}

// swiftlint:disable line_length
class TabViewController: UIViewController {
	
	@IBOutlet var movanoTabBar: UITabBar!
	@IBOutlet weak var headerView: HeaderView!
	@IBOutlet weak var containerView: UIView!
	
	@IBOutlet weak var actionButton: MovanoButton!
    
    var timer = Timer()
	var selectedDate = DateManager.current.selectedDate
	var bpDataTimer: Timer!
	var presenter: TabPresenter?
    var containerVC:UIViewController = UIViewController()
    var navController:UINavigationController = UINavigationController()
    var dashboardVC:DashboardViewController?
    var sleepVC:SleepViewController?
    var activityVC:ActivityViewController?
    var trendsVC:TrendsViewController?
    var reachability: Reachability!
   
	override func viewDidLoad() {
		super.viewDidLoad()
		bluetoothManager.explicitDisconnection = false
		//movanoTabBar.barTintColor = UIColor.dashboardBackground
		//movanoTabBar.roundCorners([.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 20)
		movanoTabBar.layer.masksToBounds = true
		
		movanoTabBar.unselectedItemTintColor = UIColor.secondaryTextColor
		movanoTabBar.tintColor = UIColor.textColor
		movanoTabBar.delegate = self
		movanoTabBar.selectedItem = movanoTabBar.items?[0]
		
		navController = UINavigationController()
		navController.navigationBar.isHidden = true
		self.addChild(navController)
		
		navController.view.frame = containerView.bounds
		containerView?.addSubview(navController.view)
		navController.didMove(toParent: self)
		
		//performSegue(withIdentifier: "DashboardSegue", sender: nil)
		let dashboardVC = DashboardViewController.instantiate(fromAppStoryboard: .dashboard)
		navController.pushViewController(dashboardVC, animated: true)
		
		self.registerPresenters()
		NotificationCenter.default.addObserver(self, selector: #selector(self.scanForBLEDevice), name: NSNotification.Name("ScanForBLEDevice"), object: nil)
		scanForBLEDevice()
		if UserDefault.shared.connectedDevice != nil {
			bluetoothManager.startScanning()
		}
		
		self.presenter?.requestAccessToken()
		
		// add OpenSans-Regular to higcharts
		let fontPath = Bundle.main.path(forResource: "Gilroy-Regular", ofType: "ttf")
		HIChartView.addFont(fontPath)
		
		/*
		 Logic to save last 30 month data in database
		 */
		/*
		 let todaysDate = Date()
		 let modDate = Calendar.current.date(byAdding: .day, value: -29, to: todaysDate)!
		 let range = DateRange(start: modDate, end: todaysDate, type: .monthly)
		 
		 DispatchQueue.global(qos: .background).async {
		 self.presenter?.cacheServiceService.fetchActivtyIntensity(by: range.intervalRange)
		 DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
		 print("Activity Intensity CoreData Updated")
		 })
		 }
		 
		 DispatchQueue.global(qos: .background).async {
		 self.presenter?.cacheServiceService.fetch(sleepDataBy: range)
		 DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
		 print("Sleep data CoreData Updated")
		 })
		 }
		 
		 DispatchQueue.global(qos: .background).async {
		 self.presenter?.cacheServiceService.fetchAverageData(by: range.intervalRange)
		 DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
		 print("Average CoreData Updated")
		 })
		 }
		 
		 DispatchQueue.global(qos: .background).async {
		 self.presenter?.cacheServiceService.fetch(dailyAveragesBy: range.intervalRange)
		 DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
		 print("Daily avergae CoreData Updated")
		 })
		 }
		 
		 DispatchQueue.global(qos: .background).async {
		 self.presenter?.cacheServiceService.fetch(hrvBy: range.intervalRange)
		 DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
		 print("HRV CoreData Updated")
		 })
		 }
		 
		 DispatchQueue.global(qos: .background).async {
		 self.presenter?.cacheServiceService.fetch(oxygenBy: range.intervalRange)
		 DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
		 print("Oxygen CoreData Updated")
		 })
		 }
		 
		 DispatchQueue.global(qos: .background).async {
		 self.presenter?.cacheServiceService.fetch(pulseRateDataBy: range.intervalRange)
		 DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
		 print("Pulse Rate CoreData Updated")
		 })
		 }
		 
		 DispatchQueue.global(qos: .background).async {
		 self.presenter?.cacheServiceService.fetch(skinTempDataBy: range.intervalRange)
		 DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
		 print("Skin Temp CoreData Updated")
		 })
		 }
		 
		 DispatchQueue.global(qos: .background).async {
		 self.presenter?.cacheServiceService.fetch(breathingRateDataBy: range.intervalRange)
		 DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
		 print("Breathing CoreData Updated")
		 })
		 }
		 
		 DispatchQueue.global(qos: .background).async {
		 self.presenter?.cacheServiceService.listBloodPressures(range: range.intervalRange)
		 DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
		 print("BP CoreData Updated")
		 })
		 }
		 
		 DispatchQueue.global(qos: .background).async {
		 self.presenter?.cacheServiceService.fetch(exerciseDataBy: range.intervalRange)
		 DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
		 print("Exercise CoreData Updated")
		 })
		 }
		 
		 DispatchQueue.global(qos: .background).async {
		 self.presenter?.cacheServiceService.fetch(stepCountBy: range.intervalRange)
		 DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
		 print("Step Count CoreData Updated")
		 })
		 }
		 */
		//		self.presenter?.cacheServiceService.fetch(takeAPauseDataBy: range.intervalRange)
	}
   
    @objc func reachabilityChanged(_ note: NSNotification) {

        let reachability = note.object as! Reachability

        if reachability.connection != .unavailable {
           // uploadSessionsIfPresent()
        } else {
            print("Not reachable")
        }
    }
    func uploadSessionsIfPresent(){
        let sessionData = CoreDataExerciseSession.fetch()
        if(sessionData.count > 0){
            dataUploadJobManager.cancelAllOperations()
            let uploadWebServices : BackgroundDataUploadWebServices = BackgroundDataUploadWebServices()
           uploadWebServices.breathingRateService = BreathingRateWebService()
           uploadWebServices.exerciseService = ExerciseDataWebService()
           uploadWebServices.stepCountService = StepCountWebService()
           uploadWebServices.pulseRateService = PulseRateWebService()
           uploadWebServices.temperatureService = SkinTemperatureWebService()
           uploadWebServices.oxygenRateService = OxygenDataWebService()
            
            for eachSession in sessionData{
                let model = ExerciseReading(startTime: eachSession.startTime.timeIntervalSince1970, endTime: eachSession.endTime.timeIntervalSince1970)
                let exerciseModel = ExerciseSessionData(timingModel: model, stepsData: eachSession.stepsData.stepCount, pulseRateData: eachSession.pulseRateData.pulseRate, breathingRateData: eachSession.breathingRateData.breathingRate, temperatureData: eachSession.temperatureData.skinTemperature, oxygenData: eachSession.oxygenData.saturatedOxygen)
                scheduleSessionTimeUploadJob(sessionDataModel: exerciseModel, uploadWebServices: uploadWebServices)
            }
        }
        
    }
    
    func scheduleSessionTimeUploadJob(sessionDataModel:ExerciseSessionData,uploadWebServices : BackgroundDataUploadWebServices){
                   // dataUploadJobManager.cancelAllOperations()
                    JobBuilder(type: UploadStepsDataJob.type)
                    .priority(priority: .veryHigh)
                    .service(quality: .background)
                    .addTag(tag: "\(sessionDataModel.timingModel.startTime)")
                    .internet(atLeast: .cellular)
                        .with(params: [UploadSessionTimeJob.SessionDataModel:sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:uploadWebServices as Any])
                    .schedule(manager: dataUploadJobManager)
    }
	
	func scheduleRequestAccessToken(sessionExpiryTime:Double,authWebServices : AuthorizationWebService) {
		JobBuilder(type: RequestAccessToken.type)
			.priority(priority: .veryHigh)
			.service(quality: .background)
			.internet(atLeast: .cellular)
			.delay(time: sessionExpiryTime)
//			.periodic(limit: .unlimited, interval: sessionExpiryTime)
			.with(params: [RequestAccessToken.SessionExpiryTime:sessionExpiryTime as Any, RequestAccessToken.AuthorizationWebService:authWebServices as Any])
			.schedule(manager: dataUploadJobManager)
	}
	
    @objc func scanForBLEDevice(){
        if UserDefault.shared.connectedDevice != nil {
            //bluetoothManager.autoConnect = true
            Logger.shared.addLog("Found BLE device info in user defaults : \(String(describing: UserDefault.shared.connectedDevice))")
            self.timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: {[weak self] _ in
                self?.checkAndConnect()
               })
        }
    }
    
    func checkAndConnect(){
        if(bluetoothManager.explicitDisconnection == false){
            if(bluetoothManager.isBLEConnected == false){
                bluetoothManager.startScanning()
            }
        }
    }
    
    func stopTimer() {
        timer.invalidate()
    }
    
    deinit {
        stopTimer()
    }
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged(_:)), name: Notification.Name.reachabilityChanged, object: reachability)

        do {
            try reachability = Reachability()
            try reachability?.startNotifier()
        } catch let error{
            print("Could not notify Reachability. error \(error)")
            return
        }
        
		self.headerView.currentController = self
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.deviceConnected), name: NSNotification.Name("DeviceConnected"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.deviceDidDisconnect), name: NSNotification.Name("deviceDidDisconnect"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.deviceDidConnect), name: NSNotification.Name("deviceDidConnect"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.batteryLevelChanged), name: Notification.Name(BLE_BA_CharacteristicBatteryLevel.uuidString), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.batteryLevelChanged), name: Notification.Name(TransferService.dateServiceUUID.uuidString), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.batteryChargingStatusChanged), name: Notification.Name(TransferService.chargingUUID), object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.calendarDateSelected), name: NSNotification.Name("calendarDateSelected"), object: nil)
		if (bluetoothManager.connectedPeripheral != nil) {
			let isCharging = UserDefaults.standard.object(forKey: "isBatteryCharging") as? Int
			if let batteryRemaining = UserDefaults.standard.object(forKey: "batteryPercentage") as? Int, batteryRemaining != 0 {
				self.headerView.circularSlider.endPointValue = CGFloat(batteryRemaining)
				self.headerView.movanoLogo.isHidden = false
				self.headerView.movanoLogo.image = #imageLiteral(resourceName: "batteryIcon-select")
				self.headerView.movanoChargingLogo.isHidden = true
				self.headerView.chargePercentLabel.isHidden = true
				/*
				if isCharging != 0 {
					self.headerView.movanoLogo.isHidden = true
					self.headerView.movanoChargingLogo.isHidden = true
					self.headerView.chargePercentLabel.isHidden = true
					self.headerView.movanoLogo.image = #imageLiteral(resourceName: "batteryIcon-select")
				} else {
					self.headerView.movanoLogo.isHidden = false
					self.headerView.movanoLogo.image = #imageLiteral(resourceName: "batteryIcon-select")
					self.headerView.movanoChargingLogo.isHidden = true
					self.headerView.chargePercentLabel.isHidden = true
				}*/
			} else {
				self.headerView.movanoLogo.isHidden = false
				self.headerView.circularSlider.endPointValue = 0.0
				self.headerView.movanoLogo.image = #imageLiteral(resourceName: "batteryIcon-deselect")
				self.headerView.movanoChargingLogo.isHidden = true
				self.headerView.chargePercentLabel.isHidden = true
			}
		} else {
			self.headerView.circularSlider.endPointValue = 0.0
			self.headerView.movanoLogo.image = #imageLiteral(resourceName: "batteryIcon-deselect")
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		print("deinit")
//		bluetoothManager.updateNotificationStateToFalse(uuid: BLE_BA_CharacteristicBatteryLevel)
//		bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
//		self.bpDataTimer.invalidate()
        NotificationCenter.default.removeObserver(self, name: Notification.Name.reachabilityChanged, object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name("DeviceConnected"), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name("deviceDidDisconnect"), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name("deviceDidConnect"), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name(BLE_BA_CharacteristicBatteryLevel.uuidString), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name(TransferService.dateServiceUUID.uuidString), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name(TransferService.chargingUUID), object: nil)
		
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name("calendarDateSelected"), object: nil)
		
//		UserDefaults.standard.set(0, forKey: "batteryPercentage")
	}
	
	override func viewDidLayoutSubviews() {
		movanoTabBar.selectionIndicatorImage = UIImage.createSelectionIndicator(color: UIColor.hexColor(hex: "#228FFE") ?? UIColor.textColor, size: CGSize(width: movanoTabBar.frame.width/CGFloat(movanoTabBar.items!.count), height: movanoTabBar.frame.height), lineHeight: 2.0)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if segue is TabSegue {
			if children.count == 0 {
				
				self.addChild(segue.destination)
				segue.destination.view.frame = containerView.bounds
				self.containerView.addSubview(segue.destination.view)
				segue.destination.didMove(toParent: self)
			} else {
				let oldViewController = self.children[0]
				segue.destination.view.frame = oldViewController.view.frame
				oldViewController.willMove(toParent: nil)
				self.addChild(segue.destination)
				self.transition(from: oldViewController, to: segue.destination, duration: 0, options: .transitionCrossDissolve, animations: nil) { completed in
					oldViewController.removeFromParent()
					segue.destination.didMove(toParent: self)
				}
			}
		}
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let stepCountService = StepCountWebService()
			let breathingRateService = BreathingRateWebService()
			let pauseReadingService = PauseDataWebService()
			let exerciseService = ExerciseDataWebService()
			let pulseRateService = PulseRateWebService()
			let temperatureService = SkinTemperatureWebService()
			let oxygenService = OxygenDataWebService()
			let authService = AuthorizationWebService()
			let userService = ProfileService()
			let goalService = UserGoalsWebService()
			let uploaderService = DataUploaderWebService()
			let cacheService = CacheTechniqueWebService()
			self.presenter = TabPresenter(stepCountService: stepCountService, breathingRateService: breathingRateService, pauseReadingService: pauseReadingService, exerciseService: exerciseService, pulseRateService: pulseRateService, temperatureService: temperatureService, oxygenService: oxygenService, authService: authService, userService: userService, goalService: goalService, uploaderService: uploaderService, cacheService: cacheService, delegate: self)
			
			BluetoothManager.shared.setBluetoothService(serivce: presenter)
		}
	}
	
	@IBAction func floatingButtonClicked(_ sender: Any) {
		self.presenter?.showFloatingButtonActionSheet()
	}
}

extension TabViewController: TabDelegate {
	
	func pairDeviceError(_ controller: ActionSheetOverlayView) {
		controller.dismiss(animated: true, completion: nil)
        if UserDefault.shared.connectedDevice != nil{
            self.showMyDeviceScreenPopup()
        }else{
            self.showDevicePopup()

        }
	}
	
	func showFloatingPopup(alert: ActionSheetOverlayView) {
        alert.view.backgroundColor = UIColor.dashboardBackground
		alert.transitioningDelegate = self
		present(alert, animated: true, completion: nil)
	}
	
	func actionSelected(_ action: ActionSheetType, for controller: ActionSheetOverlayView) {
		controller.dismiss(animated: true, completion: nil)
		if action == .takeAPause {
			let bpViewController = StartBloodPressureViewController.instantiate(fromAppStoryboard: .dashboard)
			self.navigationController?.pushViewController(bpViewController, animated: true)
		} else if action == .startExtercise {
            let bpViewController = StartExerciseViewController.instantiate(fromAppStoryboard: .dashboard)
            bpViewController.action = .startExtercise
			self.navigationController?.pushViewController(bpViewController, animated: true)
		} else if action == .accelerometer {
//			let bpViewController = AccelerometerViewController.instantiate(fromAppStoryboard: .accelerometer)
//			self.navigationController?.pushViewController(bpViewController, animated: true)
        }else if action == .debug{
            let bpViewController = StartExerciseViewController.instantiate(fromAppStoryboard: .dashboard)
            bpViewController.action = .debug
            self.navigationController?.pushViewController(bpViewController, animated: true)
        }
	}
	
	func userDetailSuccess(with responseModel: UserResponseModel) {
		if let firstName = responseModel.data.firstName {
			KeyChain.shared.firstName = firstName
		}
		if let lastName = responseModel.data.lastName {
			KeyChain.shared.lastName = lastName
		}
		
		KeyChain.shared.saveUserDetails(details: responseModel.data)
	}
	
	func userDetailErrorHandler(error: MovanoError) {
		if error == .noInternetConnection {
			self.showErrorAlert(error: error)
		}
	}
	
	func accessTokenSuccess(with responseModel: AccessTokenResponseModel) {
        self.uploadSessionsIfPresent()
		self.presenter?.fetchUserInformation()
		self.presenter?.fetchData(for: self.selectedDate.start)
		
		let expiryTime = Double(responseModel.data.expiryTime ?? 1800) - 600
		scheduleRequestAccessToken(sessionExpiryTime: expiryTime, authWebServices: AuthorizationWebService())
		
	}
	
	func accessTokenErrorHandler(error: MovanoError) {
		print(error)
	}
	
	func putMessageToSns(with result: Result<MessageToSnsResponseModel, MovanoError>) {
		switch result {
		case .success: break
//			self.fetchBluetoothData()
		case .failure:
			break
		}
	}
}

// MARK: - Calendar Callbacks
extension TabViewController {
	@objc private func calendarDateSelected(notification: NSNotification) {
		guard let object = notification.object as? DateRange else {
			return
		}
		
		self.selectedDate = object
		self.presenter?.fetchData(for: self.selectedDate.start)
	}
}

// MARK: - UITabBarDelegate
extension TabViewController: UITabBarDelegate {
    
	func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
		self.actionButton.isHidden = false
		headerView.selectedDate = DateManager.current.selectedDate
		headerView.updateDate = true
		guard let index = tabBar.items?.firstIndex(of: item) else { return }
       
		switch index {
		case 0:
			//performSegue(withIdentifier: "DashboardSegue", sender: nil)
			headerView.calendarView.backgroundColor = UIColor.hexColor(hex: "#1e1e45")
			headerView.leftArrow.isHidden = false
			headerView.rightArrow.isHidden = false
			headerView.calendarImageView.isHidden = false
			headerView.dateLabelLeadingConstraint.constant = 8
			headerView.calendarImgViewWidthConstraint.constant = 25
			headerView.currentDateLabel.font = headerView.currentDateLabel.font.withSize(14)
			loadDashboardVC()
		case 1:
			//performSegue(withIdentifier: "SleepSegue", sender: nil)
			headerView.calendarView.backgroundColor = UIColor.hexColor(hex: "#1e1e45")
			headerView.leftArrow.isHidden = false
			headerView.rightArrow.isHidden = false
			headerView.calendarImageView.isHidden = false
			headerView.dateLabelLeadingConstraint.constant = 8
			headerView.calendarImgViewWidthConstraint.constant = 25
			headerView.currentDateLabel.font = headerView.currentDateLabel.font.withSize(14)
			loadSleepVC()
		case 2:
			//performSegue(withIdentifier: "ActivitySegue", sender: nil)
			headerView.calendarView.backgroundColor = UIColor.hexColor(hex: "#1e1e45")
			headerView.leftArrow.isHidden = false
			headerView.rightArrow.isHidden = false
			headerView.calendarImageView.isHidden = false
			headerView.dateLabelLeadingConstraint.constant = 8
			headerView.calendarImgViewWidthConstraint.constant = 25
			headerView.currentDateLabel.font = headerView.currentDateLabel.font.withSize(14)
			loadActivityVC()
		case 3:
			headerView.selectedDate = DateManager.current.selectedMonth
			headerView.updateDate = false
			headerView.calendarView.backgroundColor = .clear
			headerView.leftArrow.isHidden = true
			headerView.rightArrow.isHidden = true
			headerView.currentDateLabel.text = "Trends"
			headerView.currentDateLabel.font = headerView.currentDateLabel.font.withSize(18)
			headerView.calendarImageView.isHidden = true
			headerView.dateLabelLeadingConstraint.constant = 0
			headerView.calendarImgViewWidthConstraint.constant = 0
			loadTrendsVC()
			//performSegue(withIdentifier: "TrendSegue", sender: nil)
			
		default:
			break
		}
	}
    
    func loadDashboardVC(){
        let existVC = self.navController.viewControllers.contains(where: { $0.isKind(of: DashboardViewController.self) })
        
        if(existVC){
            for controller in self.navController.viewControllers as Array {
                if controller.isKind(of: DashboardViewController.self) {
                    self.navController.popToViewController(controller, animated: true)
                    break
                }
            }
        }else{
            if(dashboardVC == nil){
                dashboardVC = DashboardViewController.instantiate(fromAppStoryboard: .dashboard)
            }
            self.navController.pushViewController(dashboardVC!, animated: true)
        }

    }
    
    func loadSleepVC(){
        let existVC = self.navController.viewControllers.contains(where: { $0.isKind(of: SleepViewController.self) })
        
        if(existVC){
            for controller in self.navController.viewControllers as Array {
                if controller.isKind(of: SleepViewController.self) {
                    self.navController.popToViewController(controller, animated: true)
                    break
                }
            }
        }else{
            if(sleepVC == nil){
                sleepVC = SleepViewController.instantiate(fromAppStoryboard: .sleep)
            }
            self.navController.pushViewController(sleepVC!, animated: true)
        }
    }
    
    func loadActivityVC(){
        let existVC = self.navController.viewControllers.contains(where: { $0.isKind(of: ActivityViewController.self) })
        
        if(existVC){
            for controller in self.navController.viewControllers as Array {
                if controller.isKind(of: ActivityViewController.self) {
                    self.navController.popToViewController(controller, animated: true)
                    break
                }
            }
        }else{
            if(activityVC == nil){
                activityVC = ActivityViewController.instantiate(fromAppStoryboard: .activity)
            }
            self.navController.pushViewController(activityVC!, animated: true)
        }
    }
    
    func loadTrendsVC(){
        let existVC = self.navController.viewControllers.contains(where: { $0.isKind(of: TrendsViewController.self) })
        
        if(existVC){
            for controller in self.navController.viewControllers as Array {
                if controller.isKind(of: TrendsViewController.self) {
                    self.navController.popToViewController(controller, animated: true)
                    break
                }
            }
        }else{
            if(trendsVC == nil){
                trendsVC = TrendsViewController.instantiate(fromAppStoryboard: .trends)
            }
            self.navController.pushViewController(trendsVC!, animated: true)
        }
    }
}

// MARK: - Bluetooth related operation
extension TabViewController {
	
	@objc private func batteryLevelChanged(notification: NSNotification) {
		if let object = notification.object as? String, notification.name.rawValue == BLE_BA_CharacteristicBatteryLevel.uuidString {
			let value = Int(object)
			if value != 0 {
				UserDefaults.standard.set(value, forKey: "batteryPercentage")
			}
			self.headerView.movanoLogo.image = #imageLiteral(resourceName: "batteryIcon-select")
			if let batteryRemaining = UserDefaults.standard.object(forKey: "batteryPercentage") as? Int, batteryRemaining != 0 {
				self.headerView.circularSlider.endPointValue = CGFloat(batteryRemaining)
			} else {
				self.headerView.circularSlider.endPointValue = CGFloat(value!)
			}
		}
		
		if let object = notification.object as? String, notification.name.rawValue == TransferService.dateServiceUUID.uuidString {
			let value = Int(object)
			self.headerView.movanoLogo.image = #imageLiteral(resourceName: "batteryIcon-select")
			
			if value != 0 {
				UserDefaults.standard.set(value, forKey: "batteryPercentage")
				self.headerView.circularSlider.endPointValue = CGFloat(value!)
			} else {
				if let batteryRemaining = UserDefaults.standard.object(forKey: "batteryPercentage") as? Int, batteryRemaining != 0 {
					self.headerView.circularSlider.endPointValue = CGFloat(batteryRemaining)
				} else {
					self.headerView.circularSlider.endPointValue = CGFloat(value!)
				}
			}
		}
	}
	
	@objc private func batteryChargingStatusChanged(notification: NSNotification) {
		if let object = notification.object as? String, notification.name.rawValue == TransferService.chargingUUID {
			let value = Int(object)
			
			if value != 0 {
//				self.headerView.circularSlider.endPointValue = CGFloat(value!)
				self.headerView.movanoLogo.image = #imageLiteral(resourceName: "chargingIcon")
//				UserDefaults.standard.set(value, forKey: "batteryPercentage")
				
//				self.headerView.movanoLogo.isHidden = true
				self.headerView.movanoChargingLogo.isHidden = true
				self.headerView.chargePercentLabel.isHidden = true
//				self.headerView.chargePercentLabel.text = "\(value!) mA"
			} else {
				let percentage = UserDefaults.standard.object(forKey: "batteryPercentage") as? Int
				self.headerView.movanoLogo.image = #imageLiteral(resourceName: "batteryIcon-select")
				self.headerView.circularSlider.endPointValue = CGFloat(percentage ?? 0)
			}
			UserDefaults.standard.set(value, forKey: "isBatteryCharging")
//			bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
		}
	}
	
	@objc private func deviceConnected(notification: NSNotification) {
//		bluetoothManager.updateNotificationStateToTrue(uuid: BLE_BA_CharacteristicBatteryLevel)
		//bluetoothManager.updateNotificationStateToTrue(uuid: TransferService.dateServiceUUID)
       // Logger.shared.addLog("updateNotificationStateToTrue : \(String(describing: self))")

		NotificationCenter.default.post(name: NSNotification.Name(BLE_BA_CharacteristicBatteryLevel.uuidString), object: nil)
		NotificationCenter.default.post(name: NSNotification.Name(TransferService.dateServiceUUID.uuidString), object: nil)
		NotificationCenter.default.post(name: NSNotification.Name(TransferService.chargingUUID), object: nil)
//		self.fetchBluetoothData()
	}
	
	@objc private func deviceDidDisconnect(notification: NSNotification) {
		self.headerView.circularSlider.endPointValue = 0.0
		UserDefaults.standard.set(0, forKey: "batteryPercentage")
		self.headerView.movanoLogo.image = #imageLiteral(resourceName: "batteryIcon-deselect")
		self.headerView.chargePercentLabel.isHidden = true
		self.headerView.movanoChargingLogo.isHidden = true
	}
	
	@objc private func deviceDidConnect(notification: NSNotification) {
		guard let object = notification.object as? PeripheralDevice else {
			return
		}
		
		self.headerView.circularSlider.endPointValue = CGFloat(object.detail.batteryPercentage)
		
		self.headerView.movanoChargingLogo.isHidden = true
		self.headerView.chargePercentLabel.isHidden = true
		self.headerView.movanoLogo.image = #imageLiteral(resourceName: "batteryIcon-select")
		
		UserDefaults.standard.set(object.detail.batteryPercentage, forKey: "batteryPercentage")
		
		let banner = FloatingNotificationBanner(title: "Device Connected!", subtitle: "Connected to " + object.detail.name + ".", titleFont: UIFont.gilroy.bold(withSize: 15), titleColor: .backgroundColor, titleTextAlign: .left, subtitleFont: UIFont.gilroy.regular(withSize: 14), subtitleColor: .backgroundColor, subtitleTextAlign: .left, leftView: nil, rightView: nil, style: .success, colors: nil, iconPosition: .top)
		banner.backgroundColor = UIColor.textColor
		banner.show()
	}
	
	@objc private func hideCalendarView(notification: NSNotification) {
		guard let object = notification.object as? String else {
			return
		}
		self.headerView.calendarView.backgroundColor = .clear
		self.headerView.currentDateLabel.text = object
	}
}

// MARK: - Scheduler Callbacks
extension TabViewController {
	
	func fetchBluetoothData() {
		//bluetoothManager.updateNotificationStateToTrue(uuid: TransferService.dateServiceUUID)
       // Logger.shared.addLog("updateNotificationStateToTrue : \(String(describing: self))")

		self.bpDataTimer = Timer.scheduledTimer(withTimeInterval: 15, repeats: false) { _ in
//			bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
			self.presenter?.zipFile()
		}
	}
}
