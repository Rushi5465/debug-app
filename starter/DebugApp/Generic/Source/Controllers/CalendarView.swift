//
//  CalendarView.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 10/08/21.
//

import UIKit
import FSCalendar

protocol CalendarDelegate {
	func calendar(_ selectedDate: Date)
}

class CalendarView: UIViewController {
	
	var hasSetPointOrigin = false
	var pointOrigin: CGPoint?
	
	@IBOutlet weak var calendarView: FSCalendar!
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var leftButton: MovanoButton!
	@IBOutlet weak var rightButton: MovanoButton!
	@IBOutlet weak var currentCalendarTitle: MovanoButton!
	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var calendarTitleView: UIView!
	
	var dateList = [Date]()
	var rangeType: DateRangeType!
	var selectedDate: Date!
	
	var delegate: CalendarDelegate?
	private var presenter: CalendarPresenter?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.registerPresenters()
		self.registerCellToCollectionView()
		self.assignDelegate()
		
		let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
		view.addGestureRecognizer(panGesture)
		
		self.calendarTitleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(calendarTapped(tap:))))
		self.calendarTitleView.isUserInteractionEnabled = true
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if rangeType == .daily {
			self.calendarView.isHidden = false
			self.collectionView.isHidden = true
			
			self.presenter?.assignCalendarControl()
			self.presenter?.showCalendarUI(with: self.selectedDate)
		} else {
			self.calendarView.isHidden = true
			self.collectionView.isHidden = false
			
			if rangeType == .monthly {
				self.dateList = Date.dates(from: self.selectedDate.startOfYear, to: self.selectedDate.endOfYear, interval: 1, component: .month)
			} else {
				self.dateList = Date.dates(from: Date().startOfYear.dateBefore(component: .year, value: 10), to: Date().endOfYear, interval: 1, component: .year)
			}
			self.collectionView.reloadData()
		}
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			self.presenter = CalendarPresenter(calendarView: calendarView, delegate: self)
		}
	}
	
	override func viewDidLayoutSubviews() {
		if !hasSetPointOrigin {
			hasSetPointOrigin = true
			pointOrigin = self.view.frame.origin
		}
	}
	
	func registerCellToCollectionView() {
		let profileNib = UINib.init(nibName: "CalendarCollectionViewCell", bundle: nil)
		self.collectionView.register(profileNib, forCellWithReuseIdentifier: CalendarCollectionViewCell.IDENTIFIER)
	}
	
	func assignDelegate() {
		self.collectionView.delegate = self
		self.collectionView.dataSource = self
	}
	
	@objc func calendarTapped(tap: UITapGestureRecognizer) {
		self.dismiss(animated: false, completion: nil)
	}
	
	@objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
		let translation = sender.translation(in: view)
		
		// Not allowing the user to drag the view upward
		guard translation.y <= 0 else { return }
		
		// setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
		view.frame.origin = CGPoint(x: 0, y: self.pointOrigin!.y + translation.y)
		
		if sender.state == .ended {
			self.dismiss(animated: false, completion: nil)
		}
	}
	
	@IBAction func prevBtnClicked(_ sender: Any) {
		self.presenter?.showPreviousCalendar()
	}
	
	@IBAction func nextBtnClicked(_ sender: Any) {
		self.presenter?.showNextCalendar()
	}
}

// MARK: - UICollectionViewDelegate
extension CalendarView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let width = (collectionView.frame.width / 4) - 30
		return CGSize(width: width, height: 50)
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		self.selectedDate = dateList[indexPath.item]
		self.dismiss(animated: false, completion: nil)
		self.delegate?.calendar(self.selectedDate)
	}
}

// MARK: - UICollectionViewDataSource
extension CalendarView: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return dateList.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let item = dateList[indexPath.item]
		let cell = CalendarCollectionViewCell.dequeueReusableCell(collectionView: collectionView, indexPath: indexPath)
		if rangeType == .monthly {
			cell.titleLabel.text = item.dateToString(format: "MMM")
			if self.selectedDate.dateToString(format: "MMM yyyy") == item.dateToString(format: "MMM yyyy") {
				cell.background.backgroundColor = UIColor.textColor
				cell.titleLabel.textColor = UIColor.dashboardBackground
			} else {
				cell.background.backgroundColor = UIColor.dashboardBackground
				cell.titleLabel.textColor = UIColor.textColor
			}
		} else {
			cell.titleLabel.text = item.dateToString(format: "yyyy")
			if self.selectedDate.dateToString(format: "yyyy") == item.dateToString(format: "yyyy") {
				cell.background.backgroundColor = UIColor.textColor
				cell.titleLabel.textColor = UIColor.dashboardBackground
			} else {
				cell.background.backgroundColor = UIColor.dashboardBackground
				cell.titleLabel.textColor = UIColor.textColor
			}
		}
		cell.background.layer.cornerRadius = 25
		return cell
	}
}

// MARK: - CalendarViewDelegate
extension CalendarView: CalendarViewDelegate {
	
	func calendarViewDateSelected(_ selectedDate: Date) {
		self.currentCalendarTitle.setTitle(selectedDate.dateToString(format: "MMM yyyy"), for: .normal)
		self.titleLabel.text = selectedDate.formattedDateWithSuffix
		let enabled = (selectedDate.startOfMonth == Date().startOfMonth) ? false : true
		self.rightButton.interaction(enabled: enabled)
	}
	
	func calendarViewDidClose() {
		self.dismiss(animated: false, completion: nil)
		self.delegate?.calendar(calendarView.selectedDate!)
	}
}
