//
//  MovanoStoryBoard.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 03/12/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import UIKit

enum MovanoStoryboard: String {
    case main = "Main"
    case home = "Home"
    case dashboard = "Dashboard"
    case profile = "Profile"
    case bluetooth = "Bluetooth"
	case accelerometer = "Accelerometer"
	case commandLineInterface = "CLI"
	case activity = "Activity"
	case sleep = "Sleep"
	case trends = "Trends"
    case debug = "Debug"
	
    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }

    func viewController<T: UIViewController>(viewControllerClass: T.Type, function: String = #function, line: Int = #line, file: String = #file) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            fatalError("ViewController with identifier \(storyboardID), " +
            "not found in \(self.rawValue) " +
            "Storyboard.\n" +
            "File : \(file) \n" +
            "Line Number : \(line) \n" +
            "Function : \(function)")
        }
        return scene
    }

    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}
