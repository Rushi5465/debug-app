//
//  TabDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/08/21.
//

import Foundation

protocol TabDelegate: AnyObject {
	
	func userDetailSuccess(with responseModel: UserResponseModel)
	func userDetailErrorHandler(error: MovanoError)
	func accessTokenSuccess(with responseModel: AccessTokenResponseModel)
	func accessTokenErrorHandler(error: MovanoError)
	
	func pairDeviceError(_ controller: ActionSheetOverlayView)
	func actionSelected(_ action: ActionSheetType, for controller: ActionSheetOverlayView)
	func showFloatingPopup(alert: ActionSheetOverlayView)
	
	func putMessageToSns(with result: Result<MessageToSnsResponseModel, MovanoError>)
}
