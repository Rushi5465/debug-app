//
//  CalendarPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 11/08/21.
//

import Foundation
import FSCalendar

protocol CalendarPresenterProtocol {
	
	init(calendarView: FSCalendar, delegate: CalendarViewDelegate)
	func showPreviousCalendar()
	func showNextCalendar()
	
}
