//
//  CalendarViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 11/08/21.
//

import Foundation

protocol CalendarViewDelegate: AnyObject {
	
	func calendarViewDateSelected(_ selectedDate: Date)
	func calendarViewDidClose()
}
