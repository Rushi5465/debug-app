//
//  TabPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/08/21.
//

import Foundation

// swiftlint:disable line_length
protocol TabPresenterProtocol {
	
	init(stepCountService: StepCountWebServiceProtocol, breathingRateService: BreathingRateWebServiceProtocol, pauseReadingService: PauseDataWebServiceProtocol, exerciseService: ExerciseDataWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, temperatureService: SkinTemperatureWebServiceProtocol, oxygenService: OxygenDataWebServiceProtocol, authService: AuthorizationWebServiceProtocol, userService: UserServiceProtocol, goalService: UserGoalsWebServiceProtocol, uploaderService: DataUploaderWebServiceProtocol, cacheService: CacheTechniqueWebService, delegate: TabDelegate)
	
	func requestAccessToken()
	func fetchUserInformation()
	func fetchData(for date: Date)
	
	func showFloatingButtonActionSheet()
}
