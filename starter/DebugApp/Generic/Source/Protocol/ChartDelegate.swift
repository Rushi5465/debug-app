//
//  ChartDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 01/09/21.
//

import Highcharts
import Foundation

protocol ChartDelegate: AnyObject {
	func chart(with options: HIOptions, peakValue: String)
	func chart(with options: HIOptions, peakValue: NSAttributedString)
}

extension ChartDelegate {
	func chart(with options: HIOptions, peakValue: NSAttributedString) {
		
	}
}
