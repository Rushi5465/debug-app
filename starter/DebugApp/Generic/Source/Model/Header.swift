//
//  Header.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 08/07/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import Alamofire

struct Header {
	
	private var header: HTTPHeaders
	
	init() {
		header = HTTPHeaders()
		header["Accept"] = "application/json"
	}
	
	var allHeaders: HTTPHeaders {
		return header
	}
	
	mutating func addHeader(key: String, value: String) {
		header[key] = value
	}
	
}
