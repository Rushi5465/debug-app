//
//  BuildType.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/06/21.
//

import Foundation

enum BuildType {
	case dev
	case staging
	case prod
}
