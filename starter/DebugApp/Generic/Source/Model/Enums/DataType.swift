//
//  DataType.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 28/07/21.
//

import Foundation

enum DataType : String {
	case movano = "mv_data"
	case accelerometer = "ao_data"
}
