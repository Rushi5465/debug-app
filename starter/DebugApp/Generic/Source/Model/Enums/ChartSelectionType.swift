//
//  ChartSelectionType.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 12/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

enum ChartSelectionType: String {
	case all
	case insight = "insightschart"
	case activity = "logschart"
}
