//
//  WeightUnits.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

enum WeightUnits: String {
	case lbs
	case kgs
}
