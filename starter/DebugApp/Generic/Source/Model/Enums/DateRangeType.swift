//
//  DateRangeType.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

enum DateRangeType: String {
	
	case daily
	case weekly
	case monthly
	case yearly
	
}
