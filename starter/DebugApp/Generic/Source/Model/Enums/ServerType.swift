//
//  ServerType.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 13/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

enum ServerType: String {
	case dev = "Development"
	case staging = "Staging"
	case prod = "Production"
}
