//
//  Gender.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

enum Gender: Int {
	case male
	case female
	case others
	
	var stringValue: String {
		switch self {
			case .male: return "Male"
			case .female: return "Female"
			case .others : return "Others"
		}
	}
}
