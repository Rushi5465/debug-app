//
//  CalendarType.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 10/08/21.
//

import Foundation

enum CalendarType {
	case day
	case month
	case year
}
