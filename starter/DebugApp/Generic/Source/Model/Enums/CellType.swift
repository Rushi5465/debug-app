//
//  CellType.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

enum CellType {
	case glucose
	case target
}
