//
//  OnBoardingStatus.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/07/21.
//

import Foundation

enum OnBoardingStatus: String {
	case personalDetails = "PersonalDetails"
	case success = "Success"
}
