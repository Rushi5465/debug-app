//
//  LoginResponse.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 03/12/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import SwiftyJSON

class LoginResponse {
	var mfaEnabled: Bool?
	var mobilenumber: String?
	var challengeName: String?
	var usernameForSrp: String?
	var session: String?
	init() {
		
	}
}

extension LoginResponse {
	convenience init(json: JSON) {
		self.init()
		mobilenumber = json["mobile_number"].stringValue
		challengeName = json["challange_name"].stringValue
		usernameForSrp = json["username_for_srp"].stringValue
		session = json["session"].stringValue
		mfaEnabled = json["MFA_enabled"].boolValue
	}
}
