//
//  Server.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 13/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

struct Server {
	
	let serverUrl: String
	let graphQLUrl: String
	let type: ServerType
	
}
