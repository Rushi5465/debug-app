//
//  Logger.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/06/21.
//

import Foundation

class Logger: NSObject, Codable {
	
	var messages: [Logs] {
		didSet {
			if messages.count > 200000000 {
				self.messages.remove(at: 0)
			}
			UserDefault.shared.logs = self
		}
	}

	private static var singleInstance: Logger?
	
	static var shared: Logger {
		if singleInstance == nil {
			singleInstance = UserDefault.shared.logs
		}
		return singleInstance!
	}
	
	override init() {
		messages = [Logs]()
	}
	
	func addLog(_ log: String) {
		if ServerManager.shared.currentServer.type != .prod {
			messages.append(Logs(message: log, timeStamp: Date().timeIntervalSince1970))
		}
		print(log)
	}
	
	func printLogs() -> String {
		var logs = String()
		for each in messages {
			logs += "\n" + Date(timeIntervalSince1970: each.timeStamp).dateToString(format: "dd MMM yy hh:mm:ss a") + " - " + each.message
		}
		return logs
	}
}

struct Logs: Codable {
	
	var message: String
	var timeStamp: TimeInterval
}
