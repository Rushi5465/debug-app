//
//  DateManager.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/08/21.
//

import Foundation

class DateManager {
	
	private static var singleInstance: DateManager?
	
	var selectedDate: DateRange
	
	var selectedMonth: DateRange {
		return DateRange(selectedDate.start, type: .monthly)
	}
	
	private init() {
		selectedDate = DateRange(Date(), type: .daily)
	}
	
	static var current: DateManager {
		if singleInstance == nil {
			singleInstance = DateManager()
		}
		return singleInstance!
	}
	
	func resetManager() {
		selectedDate = DateRange(Date(), type: .daily)
	}
}
