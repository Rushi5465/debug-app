//
//  NetworkRequest.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 08/07/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import Alamofire

struct NetworkRequest {
	
	var url: URL
	var method: HTTPMethod
	var header: HTTPHeaders?
	var param: [String: Any]?
}
