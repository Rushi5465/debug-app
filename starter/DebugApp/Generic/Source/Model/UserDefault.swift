//
//  UserDefault.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/09/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit

class UserDefault {
	
	private var userDefault: UserDefaults
	
	/// singleton Instance
	fileprivate static var singleInstance: UserDefault?
	
	private init() {
		userDefault = UserDefaults.standard
	}
	
	static var shared: UserDefault {
		if singleInstance == nil {
			singleInstance = UserDefault()
		}
		return singleInstance!
	}
	
	var isTestBuild: Bool {
		return userDefault.bool(forKey: "isTest")
	}
	
	var connectedDevice: DeviceDetail? {
		get {
			if let data = userDefault.object(forKey: "connectedDevice") as? Data {
				let movies = try? PropertyListDecoder().decode(DeviceDetail.self, from: data)
				return movies ?? nil
			}
			return nil
		} set {
			if let value = newValue {
				userDefault.set(try? PropertyListEncoder().encode(value), forKey: "connectedDevice")
			} else {
				userDefault.removeObject(forKey: "connectedDevice")
			}
		}
	}
	
	var logs: Logger {
		get {
			if let data = userDefault.object(forKey: "logs") as? Data {
				let movies = try? PropertyListDecoder().decode(Logger.self, from: data)
				return movies!
			}
			return Logger()
		} set {
			userDefault.set(try? PropertyListEncoder().encode(newValue), forKey: "logs")
		}
	}
	
	var goals: Goal {
		get {
			if let data = userDefault.object(forKey: "goals") as? Data {
				let movies = try? PropertyListDecoder().decode(Goal.self, from: data)
				return movies!
			}
			return Goal()
		} set {
			userDefault.set(try? PropertyListEncoder().encode(newValue), forKey: "goals")
		}
	}
	
	func deleteAllUserDefault() {
		
		let userDataKeys = ["connectedDevice", "goals"]
		for eachKey in userDataKeys {
			UserDefaults.standard.removeObject(forKey: eachKey)
		}
		let dictionary = UserDefaults.standard.dictionaryRepresentation()
		dictionary.keys.forEach { key in
			UserDefaults.standard.removeObject(forKey: key)
		}
		UserDefaults.standard.synchronize()
	}
}
