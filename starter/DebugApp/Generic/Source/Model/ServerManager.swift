//
//  ServerManager.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 13/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

class ServerManager {
	
	var serverList = [Server]()
	
	private static var singleInstance: ServerManager?
	var currentServer: Server!
	
	private init() {
		serverList = Config.shared.serverList
		currentServer = automaticallyManagedServer
	}
	
	static var shared: ServerManager {
		if singleInstance == nil {
			singleInstance = ServerManager()
		}
		return singleInstance!
	}
	
	func getServer(_ type: ServerType) -> Server? {
		for each in serverList where each.type == type {
			return each
		}
		return nil
	}
	
	var automaticallyManagedServer: Server {
		#if ENV_DEV
		return getServer(.dev)!
		#elseif ENV_STAGE
		return getServer(.staging)!
		#elseif ENV_PROD
		return getServer(.prod)!
		#endif
		return getServer(.staging)!
	}
}
