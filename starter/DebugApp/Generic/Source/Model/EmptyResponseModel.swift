//
//  EmptyResponseModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

struct EmptyResponseModel: Decodable {
	var message: String
	var status_code: Int
}
