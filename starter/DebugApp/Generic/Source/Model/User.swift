//
//  User.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 29/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import SwiftyJSON

struct User: Codable {
	var firstName: String
	var lastName: String
	var userName: String
	var password: String
	//var phone:String
	
	enum CodingKeys: String, CodingKey {
		case firstName = "first_name"
		case lastName = "last_name"
		case userName = "username"
		case password = "password"
	}
	
	static func isLoggedIn() -> Bool {
		if KeyChain.shared.sessionId != nil {
			return true
		} else {
			return false
		}
	}
	
	static func isVerified() -> Bool {
		if KeyChain.shared.accessToken != nil {
			return true
		} else {
			return false
		}
	}
}

class UserSession: Codable {
	var accessToken: String?
	var idToken: String?
	var refreshToken: String?
	var usernameForSrp: String?
	var expiryTime: Int?
	
	enum CodingKeys: String, CodingKey {
		case accessToken = "access_token"
		case idToken = "id_token"
		case refreshToken = "refresh_token"
		case usernameForSrp = "username_for_srp"
		case expiryTime = "expires_in"
	}
	init() {}
}
extension UserSession {
	convenience init(json: JSON) {
		self.init()
		accessToken = json["access_token"].stringValue
		idToken = json["id_token"].stringValue
		refreshToken = json["refresh_token"].stringValue
		usernameForSrp = json["username_for_srp"].stringValue
		expiryTime = json["expires_in"].int
	}
}

/*struct Login : Codable{
var mobilenumber:Int
var challengeName: String
var usernameForSrp: String
var session:String

enum CodingKeys: String, CodingKey {
case mobilenumber = "mobile_number"
case challengeName = "challange_name"
case usernameForSrp = "username_for_srp"
case session
}
}*/
