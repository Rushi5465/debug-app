//
//  Insight.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 25/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

struct InsightOld {
	
	var title: String
	var statistics: [ActivityStats]
}
