//
//  APIType.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

class APIType {
	static let NEW_TOKEN_API = "API to get new access"
	static let LOAD_READINGS_API = "API to load all readings with filter applied"
	static let LOAD_READINGS_FOR_ACTIVITY_LOG_API = "API to load all readings with filter applied for Activity Log Screen"
	static let INVITE_USER = "API to invite new user"
	static let REGISTER_USER = "API to register a new user"
	static let DELETE_USER = "API to delete a user"
	static let LOGIN_USER = "API for user to get logged in"
	static let UPDATE_USER_DATA = "API to update user information"
	static let GET_USER_DATA = "API to fetch user information"
	static let CONFIRM_REGISTRATION_CODE_API = "API to check registration code"
	static let VERIFY_USER_API = "API to verify user"
	static let RESEND_VERIFICATION_LINK_USER_API = "API to resend verification link"
	static let FORGOT_PASSWORD_API = "API for forgot password"
	static let CONFIRM_FORGOT_PASSWORD_API = "API for confirm forgot password"
	static let CHANGE_PASSWORD_LINK_API = "API to change password link"
	static let VERIFY_PHONE_API = "API to verify phone number"
	static let USER_ACTIVITY_LOG_LIST_API = "API to get list of user activity log in given date range"
	static let INSIGHT_API = "API to get insight for given date range"
	static let FETCH_PRESIGNED_API = "API to post filename to the server"
	static let UPDATE_FIRMWARE_API = "API to update the firmware"
	static let DOWNLOAD_FIRMWARE_API = "API to download new firmware"
	static let FETCH_ACTIVITY_SUMMARY_API = "API to fetch activity summary for defined range"
	static let FETCH_SLEEP_STAGE_SUMMARY_API = "API to fetch sleep stage data"
    static let FETCH_DASHBOARD_DATA_API = "API to fetch dashborad data"
	static let FETCH_INTENSITY_DATA = "API to fetch intensity data"
}
