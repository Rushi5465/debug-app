//
//  DateRange.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/08/21.
//

import Foundation

struct DateRange {
	
	var start: Date
	var end: Date
	var type: DateRangeType
	
	var dateRangeString: String {
		if type == .daily {
			return start.formattedDateWithSuffix
		} else if type == .weekly {
			let thisStart = start
			let thisEnd = end
			
			var commonYear = false
			var commonMonth = false
			if thisStart.dateToString(format: "yyyy") == thisEnd.dateToString(format: "yyyy") {
				commonYear = true
			}
			if thisStart.dateToString(format: "MMM") == thisEnd.dateToString(format: "MMM") {
				commonMonth = true
			}
			
			if commonYear {
				if commonMonth {
					return start.dateToString(format: "d") + " - " + end.dateToString(format: "d") + " " + start.dateToString(format: "MMM yy")
				}
				return start.dateToString(format: "d MMM") + " - " + end.dateToString(format: "d MMM yy")
			}
			return start.dateToString(format: "d MMM yy") + " - " + end.dateToString(format: "d MMM yy")
		} else if type == .monthly {
			return start.dateToString(format: "MMMM yy")
		} else {
			return start.dateToString(format: "yyyy")
		}
	}
	
	init(_ rangeForDate: Date, type: DateRangeType) {
		if type == .daily {
			start = rangeForDate.startOfDay
			end = rangeForDate.endOfDay
		} else if type == .weekly {
			start = rangeForDate.startOfWeek
			end = rangeForDate.endOfWeek
		} else if type == .monthly {
			start = rangeForDate.startOfMonth
			end = rangeForDate.endOfMonth
		} else {
			start = rangeForDate.startOfYear
			end = rangeForDate.endOfYear
		}
		
		self.type = type
	}
	
	init(range: Range<Date>, type: DateRangeType) {
		
		self.start = range.lowerBound
		self.end = range.upperBound
		self.type = type
	}
	
	init(start: Date, end: Date, type: DateRangeType) {
		self.start = start
		self.end = end
		self.type = type
	}
	
	var isNextAllowed: Bool {
		var comparableDate = Date()
		if type == .daily {
			comparableDate = Date().startOfDay
		} else if type == .weekly {
			comparableDate = Date().startOfWeek
		} else if type == .monthly {
			comparableDate = Date().startOfMonth
		} else if type == .yearly {
			comparableDate = Date().startOfYear
		}
		
		return (comparableDate == start) ? false : true
	}
	
	mutating func selectPrevious() {
		if type == .daily {
			self.start = self.start.dateBefore(component: .day, value: 1)
			self.end = self.end.dateBefore(component: .day, value: 1)
		} else if type == .weekly {
			self.start = self.start.dateBefore(component: .day, value: 7)
			self.end = self.end.dateBefore(component: .day, value: 7)
		} else if type == .monthly {
			self.start = self.start.dateBefore(component: .month, value: 1)
			self.end = self.end.dateBefore(component: .month, value: 1)
		} else if type == .yearly {
			self.start = self.start.dateBefore(component: .year, value: 1)
			self.end = self.end.dateBefore(component: .year, value: 1)
		}
	}
	
	mutating func selectNext() {
		if type == .daily {
			self.start = self.start.dateAfter(component: .day, value: 1)
			self.end = self.end.dateAfter(component: .day, value: 1)
		} else if type == .weekly {
			self.start = self.start.dateAfter(component: .day, value: 7)
			self.end = self.end.dateAfter(component: .day, value: 7)
		} else if type == .monthly {
			self.start = self.start.dateAfter(component: .month, value: 1)
			self.end = self.end.dateAfter(component: .month, value: 1)
		} else if type == .yearly {
			self.start = self.start.dateAfter(component: .year, value: 1)
			self.end = self.end.dateAfter(component: .year, value: 1)
		}
	}
	
	var intervalRange: Range<TimeInterval> {
		return start.timeIntervalSince1970 ..< end.timeIntervalSince1970
	}
	
	var dateRange: Range<Date> {
		return start ..< end
	}
}
