//
//  UserAccount.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 06/11/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

struct UserAccount: Codable, Hashable {
	var username: String
	var password: String
	var isSecurityEnabled: Bool
	var remindLater: Bool?
}
