//
//  ActivityStats.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 25/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit

struct ActivityStats {
	
	var title: String
	var unit: String
	var value: NSNumber?
	var imageRepresentation: UIImage
	var colorRepresentation: UIColor
	var readingTime: String?
	var cellType: CellType
}
