//
//  BiometricAuthentication.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication

enum BiometricType {
	case none
	case touchID
	case faceID
}

enum BiometricError: Error {
	case appCancel, failed, userCancel, userFallback, systemCancel, passcodeNotSet, biometryNotEnrolled, biometryLockedout, invalidContext, biometryNotAvailable, other
	
	public static func errorType(_ error: LAError) -> BiometricError {
		switch Int32(error.errorCode) {
			
			case kLAErrorAuthenticationFailed:
				return failed
			case kLAErrorUserCancel:
				return userCancel
			case kLAErrorUserFallback:
				return userFallback
			case kLAErrorSystemCancel:
				return systemCancel
			case kLAErrorPasscodeNotSet:
				return passcodeNotSet
			case kLAErrorBiometryNotEnrolled:
				return biometryNotEnrolled
			case kLAErrorBiometryLockout:
				return biometryLockedout
			case kLAErrorAppCancel:
				return appCancel
			case kLAErrorInvalidContext:
				return invalidContext
			case kLAErrorBiometryNotAvailable:
				return biometryNotAvailable
			default:
				return other
		}
	}
	
	var message: String {
		switch self {
			case .appCancel:
				return "Authentication was cancelled by application."
			case .failed:
				return "The user failed to provide valid credentials."
			case .invalidContext:
				return "The context is invalid."
			case .userFallback:
				return "The user chose to use the fallback."
			case .userCancel:
				return "Please authenticate to unlock. The app is secured from unauthorized access. You can disable this feature from profile"
			case .passcodeNotSet:
				return "Passcode is not set on the device."
			case .systemCancel:
				return "Authentication was cancelled by the system."
			case .biometryNotEnrolled:
				return "Biometric is not enrolled on the device."
			case .biometryLockedout:
				return "Too many failed attempts."
			case .biometryNotAvailable:
				return "Biometric is not available on the device."
			case .other:
				return "Did not find error code on LAError object."
		}
	}
}
class BiometricAuthentication {
	
	static var singleInstance: BiometricAuthentication?
	
	static var shared: BiometricAuthentication {
		if singleInstance == nil {
			singleInstance = BiometricAuthentication()
		}
		return singleInstance!
	}
	
	var available: Bool {
		return (self.biometricType != .none)
	}
	
	var isFaceIDSupported: Bool {
		if #available(iOS 11.0, *) {
			if self.biometricType == .faceID {
				return true
			}
		}
		return false
	}
	
	var isTouchIDSupported: Bool {
		if #available(iOS 11.0, *) {
			if self.biometricType == .touchID {
				return true
			}
		}
		return false
	}
	
	var biometricType: LABiometryType {
		let authContext: LAContext = LAContext()
		authContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil)
		return authContext.biometryType
	}
	
	func authenticateUser(completion: @escaping(Bool, BiometricError?) -> Void) {
		
		let authContext: LAContext = LAContext()
		authContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: Config.shared.autoLoginMessage) { (success, error) in
			DispatchQueue.main.async {
				if let laError = error as? LAError {
					completion(success, BiometricError.errorType(laError))
				} else {
					completion(success, nil)
				}
			}
		}
	}
	
	func loginUserWithBiometric(user: UserAccount, currentView: UIView, completion: @escaping(Bool, Error?) -> Void) {
		
		let authContext: LAContext = LAContext()
		authContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: Config.shared.autoLoginMessage) { (success, error) in
			DispatchQueue.main.async {
				if let laError = error {
					completion(success, laError)
				} else {
					self.autoLogin(user: user, currentView: currentView) { (success, error) in
						completion(success, error)
					}
					
				}
			}
		}
	}
	
	func autoLogin(user: UserAccount, currentView: UIView, loginCompletion: @escaping(Bool, Error?) -> Void) {
		hud.show(in: currentView)
		UserService.loginUser(username: user.username, password: user.password) { (result) in
			switch result {
				case .success:
					KeyChain.shared.email = user.username
					KeyChain.shared.password = user.password
					
					let allUsers = KeyChain.shared.userAccounts
					let index = allUsers.indexOf(KeyChain.shared.email!)
					if let index = index {
						KeyChain.shared.currentUser = allUsers[index]
					} else {
						KeyChain.shared.currentUser = UserAccount(username: user.username, password: user.password, isSecurityEnabled: false)
					}
					
					let mfaFlag = KeyChain.shared.mfa
					if mfaFlag {
						KeyChain.shared.userState =  NavigationUtility.UserState.loggedIn.rawValue
						DispatchQueue.main.async {
							hud.dismiss()
							let otpViewController = OTPViewController.instantiate(fromAppStoryboard: .main)
							otpViewController.navigationItem.hidesBackButton = false
							//                            self.navigationController?.pushViewController(otpViewController, animated: true)
						}
					} else {
						KeyChain.shared.userState =  NavigationUtility.UserState.onBoarded.rawValue
						NavigationUtility.setInitialController()
					}
				case .failure(let error as MovanoError):
					DispatchQueue.main.async {
						loginCompletion(false, error)
					}
				default:
					break
			}
			hud.dismiss()
		}
	}
}
