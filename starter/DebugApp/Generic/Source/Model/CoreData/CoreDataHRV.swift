//
//  CoreDataHRV.swift
//  BloodPressureApp
//
//  Created by Rushikant on 14/11/21.
//

import CoreData
import UIKit

class CoreDataHRV: NSManagedObject {
	
	@NSManaged var date: Date
	@NSManaged var value: NSNumber
	
	static let IDENTIFIER = "CoreDataHRV"
	
	static var instance: CoreDataHRV {
		
		let delegate = UIApplication.shared.delegate as! AppDelegate
		
		let bill_tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataHRV.IDENTIFIER, in:delegate.managedObjectContext!)
		let bill_TableRecord = NSManagedObject(entity: bill_tableEntity!,
											   insertInto: delegate.managedObjectContext) as! CoreDataHRV
		
		return(bill_TableRecord)
	}
	
	func insert() {
		
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		do {
			setValue(date, forKey: "date")
			setValue(value, forKey: "value")
			
			try managedContext!.save()
		} catch let error as NSError {
			print("Could not save \(error), \(error.userInfo)")
		}
	}
	
	func setCoreDataHRV(date: Date, value: NSNumber) {
		
		self.date = date
		self.value = value
	}
	
	private static func insert(for date: Date, value: NSNumber) {
		let hrvInstance = CoreDataHRV.instance
		
		hrvInstance.setCoreDataHRV(date: date, value: value)
		hrvInstance.insert()
	}
	
	static func isReadingAvailable(for date: Date) -> Bool {
		
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataHRV.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataHRV] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func isReadingAvailable(range: Range<Double>) -> Bool {
		if((range.upperBound - range.lowerBound) > 86400){
			return false
		}
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataHRV.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", Date(timeIntervalSince1970: range.lowerBound) as CVarArg, Date(timeIntervalSince1970: range.upperBound) as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataHRV] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func update(for date: Date, value: NSNumber) {
		
		let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
		let context: NSManagedObjectContext = appDel.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataHRV.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
		do {
			let fetchResults = try context.fetch(fetchRequest) as? [CoreDataHRV]
			if !fetchResults!.isEmpty {
				
				let thisHRV = fetchResults?[0]
				thisHRV?.value = value
				thisHRV?.insert()
				
				try context.save()
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
	}
	
	static func add(timeInterval: TimeInterval, value: Int) {
		
		let date = Date(timeIntervalSince1970: timeInterval)
		if CoreDataHRV.isReadingAvailable(for: date) {
			update(for: date, value: NSNumber(value: value))
		} else {
			insert(for: date, value: NSNumber(value: value))
		}
	}
	
	static func fetchHRVChartData(dateRange: DateRange) -> [ChartElement] {
		var dateList = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 5, component: .minute)
		} else if dateRange.type == .monthly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .yearly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
		}
		
		var elementList = [ChartElement]()
		for index in 0 ..< dateList.count {
			var range: Range<Date>!
			if index == dateList.count - 1 {
				range = dateList[index]..<dateRange.end
			} else {
				range = dateList[index]..<dateList[index + 1]
			}
			
			let chunkData = CoreDataHRV.fetch(for: range)
			var chunkHRV: Double? = chunkData.reduce(0) { (result: Double, nextItem: CoreDataHRV) -> Double in
				return result + nextItem.value.doubleValue
			}
			
			if chunkData.count > 0 {
				chunkHRV = chunkHRV! / Double(chunkData.count)
			}
			
			if chunkHRV == 0 {
				chunkHRV = nil
			}
			
			if dateRange.type == .daily && chunkData.count != 0 {
			if dateList.count > 1 {
				elementList.append(ChartElement(date: range.lowerBound, value: Double(chunkData[0].value)))
			} else {
					elementList.append(ChartElement(date: range.lowerBound, value: Double(chunkData[0].value)))
			}} else {
				elementList.append(ChartElement(date: range.lowerBound, value: chunkHRV))
			}
			
//			elementList.append(ChartElement(date: range.lowerBound, value: chunkHRV))
		}
		
		return elementList
	}
	
	static func fetch(for dateRange: Range<Date>) -> [CoreDataHRV] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataHRV.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataHRV] {
					return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataHRV]()
	}
	
	static func fetch(timeStamp: Double) -> CoreDataHRV? {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataHRV.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", Date(timeIntervalSince1970: timeStamp) as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataHRV] {
				return readings.first
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return nil
	}
	
	static func fetch() -> [CoreDataHRV] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataHRV.IDENTIFIER)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataHRV] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataHRV]()
	}
	
	static func delete(for date: Date) {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataHRV.IDENTIFIER)
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedContext!.fetch(fetchRequest)
			for managedObject in results {
				let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
				managedContext!.delete(managedObjectData)
			}
			try managedContext?.save()
		} catch let error as NSError {
			print("delete all data in \(String(describing: entity)) error : \(error) \(error.userInfo)")
		}
	}
}

