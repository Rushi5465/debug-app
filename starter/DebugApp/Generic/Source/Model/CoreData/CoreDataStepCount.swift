//
//  CoreDataStepCount.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/08/21.
//

import CoreData
import UIKit

class CoreDataStepCount: NSManagedObject {
	
	@NSManaged var date: Date
	@NSManaged var value: NSNumber
	@NSManaged var calories: NSNumber?
	@NSManaged var distance: NSNumber?
	
	static let IDENTIFIER = "CoreDataStepCount"
	
	static var instance: CoreDataStepCount {
		
		let delegate = UIApplication.shared.delegate as! AppDelegate
		
		let bill_tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataStepCount.IDENTIFIER, in:delegate.managedObjectContext!)
		let bill_TableRecord = NSManagedObject(entity: bill_tableEntity!,
											   insertInto: delegate.managedObjectContext) as! CoreDataStepCount
		
		return(bill_TableRecord)
	}
	
	func insert() {
		
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		do {
			setValue(date, forKey: "date")
			setValue(value, forKey: "value")
			setValue(calories, forKey: "calories")
			setValue(distance, forKey: "distance")
			
			try managedContext!.save()
		} catch let error as NSError {
			print("Could not save \(error), \(error.userInfo)")
		}
	}
	
	func setCoreDataStepCount(date: Date, value: NSNumber, calories: NSNumber?, distance: NSNumber?) {
		
		self.date = date
		self.value = value
		self.calories = calories
		self.distance = distance
	}
	
	private static func insert(for date: Date, value: NSNumber, calories: NSNumber?, distance: NSNumber?) {
		let stepCountInstance = CoreDataStepCount.instance
		
		stepCountInstance.setCoreDataStepCount(date: date, value: value, calories: calories, distance: distance)
		stepCountInstance.insert()
	}
	
	static func isReadingAvailable(for date: Date) -> Bool {
		
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataStepCount.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataStepCount] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func isReadingAvailable(range: Range<Double>) -> Bool {
		if((range.upperBound - range.lowerBound) > 86400){
			return false
		}
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataStepCount.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", Date(timeIntervalSince1970: range.lowerBound) as CVarArg, Date(timeIntervalSince1970: range.upperBound) as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataStepCount] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func update(for date: Date, value: NSNumber, calories: NSNumber?, distance: NSNumber?) {
		
		let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
		let context: NSManagedObjectContext = appDel.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataStepCount.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
		do {
			let fetchResults = try context.fetch(fetchRequest) as? [CoreDataStepCount]
			if !fetchResults!.isEmpty {
				
				let thisStepCount = fetchResults?[0]
				thisStepCount?.value = value
				thisStepCount?.calories = calories
				thisStepCount?.distance = distance
				thisStepCount?.insert()
				
				try context.save()
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
	}
	
	static func add(timeInterval: TimeInterval, value: Int, calories: Double?, distance: Double?) {
		
		let date = Date(timeIntervalSince1970: timeInterval)
		if CoreDataStepCount.isReadingAvailable(for: date) {
			update(for: date, value: NSNumber(value: value), calories: calories as NSNumber?, distance: distance as NSNumber?)
		} else {
			insert(for: date, value: NSNumber(value: value), calories: calories as NSNumber?, distance: distance as NSNumber?)
		}
	}
	
	static func fetch(for dateRange: Range<Date>) -> [CoreDataStepCount] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataStepCount.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataStepCount] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataStepCount]()
	}
	
	static func fetch(timeStamp: Double) -> CoreDataStepCount? {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataStepCount.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", Date(timeIntervalSince1970: timeStamp) as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataStepCount] {
				return readings.first
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return nil
	}
	
	static func fetch() -> [CoreDataStepCount] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataStepCount.IDENTIFIER)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataStepCount] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataStepCount]()
	}
	
	static func delete(for date: Date) {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataStepCount.IDENTIFIER)
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedContext!.fetch(fetchRequest)
			for managedObject in results {
				let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
				managedContext!.delete(managedObjectData)
			}
			try managedContext?.save()
		} catch let error as NSError {
			print("delete all data in \(String(describing: entity)) error : \(error) \(error.userInfo)")
		}
	}
	
	static func fetchStepCountChartData(dateRange: DateRange) -> [ChartElement] {
		var dateList = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .hour)
		} else if dateRange.type == .monthly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .yearly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
		}
		
		var elementList = [ChartElement]()
		for index in 0 ..< dateList.count {
			var range: Range<Date>!
			if index == dateList.count - 1 {
				range = dateList[index]..<dateRange.end
			} else {
				range = dateList[index]..<dateList[index + 1]
			}
			
			let chunkData = CoreDataStepCount.fetch(for: range)
			var chunkStepCount: Double? = chunkData.reduce(0) { (result: Double, nextItem: CoreDataStepCount) -> Double in
				return result + nextItem.value.doubleValue
			}
			if chunkStepCount == 0 {
				chunkStepCount = nil
			}
			
			elementList.append(ChartElement(date: range.lowerBound, value: chunkStepCount))
		}
		
		return elementList
	}
	
	static func fetchCalorieChartData(dateRange: DateRange) -> [ChartElement] {
		var dateList = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .hour)
		} else if dateRange.type == .monthly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .yearly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
		}
		
		var elementList = [ChartElement]()
		for index in 0 ..< dateList.count {
			var range: Range<Date>!
			if index == dateList.count - 1 {
				range = dateList[index]..<dateRange.end
			} else {
				range = dateList[index]..<dateList[index + 1]
			}
			
			let chunkData = CoreDataStepCount.fetch(for: range)
			var chunkStepCount: Double? = chunkData.reduce(0) { (result: Double, nextItem: CoreDataStepCount) -> Double in
				return result + (nextItem.calories?.doubleValue ?? 0)
			}
			
			if chunkStepCount == 0 {
				chunkStepCount = nil
			}
			elementList.append(ChartElement(date: range.lowerBound, value: chunkStepCount))
		}
		
		return elementList
	}
}
