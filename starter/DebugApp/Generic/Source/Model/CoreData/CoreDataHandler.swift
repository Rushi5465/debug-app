//
//  CoreDataHandler.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/09/21.
//

import Foundation
import UIKit
import CoreData

// swiftlint:disable line_length
class CoreDataHandler {
	
	static func deleteAllCoreData() {
		let entityArray = [CoreDataBPReading.IDENTIFIER, CoreDataStepCount.IDENTIFIER, CoreDataBreathingRate.IDENTIFIER, CoreDataPauseReading.IDENTIFIER, CoreDataExercise.IDENTIFIER, CoreDataPulseRate.IDENTIFIER, CoreDataSkinTemperature.IDENTIFIER, CoreDataOxygen.IDENTIFIER, CoreDataSleepStage.IDENTIFIER, CoreDataHRV.IDENTIFIER,CoreDataSleepTime.IDENTIFIER,CoreDataActivity.IDENTIFIER,CoreDataExerciseSession.IDENTIFIER,CoreDataActivityIntensity.IDENTIFIER]
		for entity in entityArray {
			CoreDataHandler.deleteAllData(entity)
		}
	}
	
	static func deleteAllData(_ entity: String) {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		// Create Fetch Request
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)

		// Create Batch Delete Request
		let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

		do {
			try managedContext!.execute(batchDeleteRequest)

		} catch {
			Logger.shared.addLog("CoreData Deletion Error: " + error.localizedDescription)
		}
	}
}
