//
//  CoreDataPulseRate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 26/08/21.
//

import CoreData
import UIKit

class CoreDataPulseRate: NSManagedObject {
	
	@NSManaged var date: Date
	@NSManaged var value: NSNumber
	
	static let IDENTIFIER = "CoreDataPulseRate"
	
	static var instance: CoreDataPulseRate {
		
		let delegate = UIApplication.shared.delegate as! AppDelegate
		
		let bill_tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataPulseRate.IDENTIFIER, in:delegate.managedObjectContext!)
		let bill_TableRecord = NSManagedObject(entity: bill_tableEntity!,
											   insertInto: delegate.managedObjectContext) as! CoreDataPulseRate
		
		return(bill_TableRecord)
	}
	
	func insert() {
		
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		do {
			setValue(date, forKey: "date")
			setValue(value, forKey: "value")
			
			try managedContext!.save()
		} catch let error as NSError {
			print("Could not save \(error), \(error.userInfo)")
		}
	}
	
	func setCoreDataPulseRate(date: Date, value: NSNumber) {
		
		self.date = date
		self.value = value
	}
	
	private static func insert(for date: Date, value: NSNumber) {
		let PulseRateInstance = CoreDataPulseRate.instance
		
		PulseRateInstance.setCoreDataPulseRate(date: date, value: value)
		PulseRateInstance.insert()
	}
	
	static func isReadingAvailable(for date: Date) -> Bool {
		
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPulseRate.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataPulseRate] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func isReadingAvailable(range: Range<Double>) -> Bool {
        if((range.upperBound - range.lowerBound) > 86400){
            return false
        }
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPulseRate.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", Date(timeIntervalSince1970: range.lowerBound) as CVarArg, Date(timeIntervalSince1970: range.upperBound) as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataPulseRate] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func update(for date: Date, value: NSNumber) {
		
		let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
		let context: NSManagedObjectContext = appDel.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPulseRate.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
		do {
			let fetchResults = try context.fetch(fetchRequest) as? [CoreDataPulseRate]
			if !fetchResults!.isEmpty {
				
				let thisPulseRate = fetchResults?[0]
				thisPulseRate?.value = value
				thisPulseRate?.insert()
				
				try context.save()
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
	}
	
	static func add(timeInterval: TimeInterval, value: Int) {
		
		let date = Date(timeIntervalSince1970: timeInterval)
		if CoreDataPulseRate.isReadingAvailable(for: date) {
			update(for: date, value: NSNumber(value: value))
		} else {
			insert(for: date, value: NSNumber(value: value))
		}
	}
	
	static func fetchPulseRateChartData(dateRange: DateRange) -> [ChartElement] {
		var dateList = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 5, component: .minute)
		} else if dateRange.type == .monthly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .yearly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
		}
		
		var elementList = [ChartElement]()
		for index in 0 ..< dateList.count {
			var range: Range<Date>!
			if index == dateList.count - 1 {
				range = dateList[index]..<dateRange.end
			} else {
				range = dateList[index]..<dateList[index + 1]
			}
			
			let chunkData = CoreDataPulseRate.fetch(for: range)
			var chunkPulseRate: Double? = chunkData.reduce(0) { (result: Double, nextItem: CoreDataPulseRate) -> Double in
				return result + nextItem.value.doubleValue
			}
			
			if chunkData.count > 0 {
				chunkPulseRate = chunkPulseRate! / Double(chunkData.count)
			}
			
			if chunkPulseRate == 0 {
				chunkPulseRate = nil
			}
			
			if dateRange.type == .daily && chunkData.count != 0 {
				if dateList.count > 1 {
					elementList.append(ChartElement(date: range.lowerBound, value: Double(chunkData[0].value)))
				} else {
					elementList.append(ChartElement(date: range.lowerBound, value: Double(chunkData[0].value)))
				} } else {
				elementList.append(ChartElement(date: range.lowerBound, value: chunkPulseRate))
			}
			
//			elementList.append(ChartElement(date: range.lowerBound, value: chunkPulseRate))
		}
		
		return elementList
	}
	
	static func fetch(for dateRange: Range<Date>) -> [CoreDataPulseRate] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPulseRate.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataPulseRate] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataPulseRate]()
	}
	
	static func fetch(timeStamp: Double) -> CoreDataPulseRate? {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPulseRate.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", Date(timeIntervalSince1970: timeStamp) as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataPulseRate] {
				return readings.first
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return nil
	}
	
	static func fetch() -> [CoreDataPulseRate] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPulseRate.IDENTIFIER)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataPulseRate] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataPulseRate]()
	}
	
	static func delete(for date: Date) {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPulseRate.IDENTIFIER)
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedContext!.fetch(fetchRequest)
			for managedObject in results {
				let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
				managedContext!.delete(managedObjectData)
			}
			try managedContext?.save()
		} catch let error as NSError {
			print("delete all data in \(String(describing: entity)) error : \(error) \(error.userInfo)")
		}
	}
}
