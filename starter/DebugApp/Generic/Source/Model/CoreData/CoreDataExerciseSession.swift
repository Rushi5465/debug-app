//
//  CoreDataExerciseSessionSession.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 25/12/21.
//

import UIKit
import CoreData

class BackgroundDataUploadWebServices{
     var exerciseService:ExerciseDataWebServiceProtocol?
     var temperatureService: SkinTemperatureWebServiceProtocol?
     var stepCountService: StepCountWebServiceProtocol?
     var pulseRateService: PulseRateWebServiceProtocol?
     var breathingRateService : BreathingRateWebServiceProtocol?
     var oxygenRateService: OxygenDataWebServiceProtocol?
    
    
}

// swiftlint:disable line_length
class ExerciseSessionData{
    
    var timingModel:ExerciseReading
    var stepsData: [StepCount]
    var pulseRateData: [PulseRate]
    var breathingRateData: [BreathingRate]
    var temperatureData: [SkinTemperature]
    var oxygenData: [SaturatedOxygen]
    
    init(
        timingModel:ExerciseReading,
        stepsData: [StepCount],
        pulseRateData: [PulseRate],
        breathingRateData: [BreathingRate],
        temperatureData: [SkinTemperature],
        oxygenData: [SaturatedOxygen]
    ){
        self.timingModel = timingModel
        self.stepsData = stepsData
        self.pulseRateData = pulseRateData
        self.breathingRateData = breathingRateData
        self.temperatureData = temperatureData
        self.oxygenData = oxygenData
        
    }
    
    
    
    
}

// swiftlint:disable line_length
class CoreDataExerciseSession: NSManagedObject {
    @NSManaged var startTime: Date
    @NSManaged var endTime: Date
    @NSManaged var stepsData: StepCountCoreData
    @NSManaged var pulseRateData: PulseRateCoreData
    @NSManaged var breathingRateData: BreathingRateCoreData
    @NSManaged var temperatureData: SkinTemperatureCoreData
    @NSManaged var oxygenData: SaturatedOxygenCoreData

    
    static let IDENTIFIER = "CoreDataExerciseSession"
    static var instance: CoreDataExerciseSession{
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        let tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataExerciseSession.IDENTIFIER, in:delegate.managedObjectContext!)
        let tableRecord = NSManagedObject(entity: tableEntity!,
                                               insertInto: delegate.managedObjectContext) as! CoreDataExerciseSession
        
        return(tableRecord)
    }
    
    func insert() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        do {
            setValue(startTime, forKey: "startTime")
            setValue(endTime, forKey: "endTime")
            setValue(stepsData, forKey: "stepsData")
            setValue(pulseRateData, forKey: "pulseRateData")
            setValue(breathingRateData, forKey: "breathingRateData")
            setValue(temperatureData, forKey: "temperatureData")
            setValue(oxygenData, forKey: "oxygenData")
            try managedContext!.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
        
    func setExerciseSessionData(startTime: Date, endTime: Date, stepCount:StepCountCoreData,pulseRateData:PulseRateCoreData, breathingRateData:BreathingRateCoreData, temperatureData:SkinTemperatureCoreData, oxygenData:SaturatedOxygenCoreData) {
        
        self.startTime = startTime
        self.endTime = endTime
        self.stepsData = stepCount
        self.pulseRateData = pulseRateData
        self.breathingRateData = breathingRateData
        self.temperatureData = temperatureData
        self.oxygenData = oxygenData
        
    }
    
    private static func insert(startTime: Date, endTime: Date, stepCount:StepCountCoreData,pulseRateData:PulseRateCoreData, breathingRateData:BreathingRateCoreData, temperatureData:SkinTemperatureCoreData, oxygenData:SaturatedOxygenCoreData)  {
        
        let exerciseInstance = CoreDataExerciseSession.instance
        
        exerciseInstance.setExerciseSessionData(startTime: startTime, endTime: endTime, stepCount: stepCount,pulseRateData: pulseRateData,breathingRateData: breathingRateData,temperatureData: temperatureData,oxygenData: oxygenData)
        exerciseInstance.insert()
    }
    
    static func isReadingAvailable(for date: Date) -> Bool {
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let managedContext = appDelegate.managedObjectContext!
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExerciseSession.IDENTIFIER)
            fetchRequest.predicate = NSPredicate(format: "endTime = %@", date as CVarArg)
            
            do {
                let results =
                    try managedContext.fetch(fetchRequest)
                if let allDrugs = results as? [CoreDataExerciseSession] {
                    if !allDrugs.isEmpty {
                        return true
                    }
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }
        return false
    }
    
    static func isReadingAvailable(range: Range<Double>) -> Bool {
		if((range.upperBound - range.lowerBound) > 86400){
			return false
		}
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let managedContext = appDelegate.managedObjectContext!
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExerciseSession.IDENTIFIER)
            fetchRequest.predicate = NSPredicate(format: "endTime >= %@ AND endTime <= %@", Date(timeIntervalSince1970: range.lowerBound) as CVarArg, Date(timeIntervalSince1970: range.upperBound) as CVarArg)
            
            do {
                let results =
                    try managedContext.fetch(fetchRequest)
                if let allDrugs = results as? [CoreDataExerciseSession] {
                    if !allDrugs.isEmpty {
                        return true
                    }
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }
        return false
    }
    
    static func update(startTime: Date, endTime: Date, stepCount:StepCountCoreData,pulseRateData:PulseRateCoreData, breathingRateData:BreathingRateCoreData, temperatureData:SkinTemperatureCoreData, oxygenData:SaturatedOxygenCoreData) {
        
        let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context: NSManagedObjectContext = appDel.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExerciseSession.IDENTIFIER)
        fetchRequest.predicate = NSPredicate(format: "endTime = %@", endTime as CVarArg)
        do {
            let fetchResults = try context.fetch(fetchRequest) as? [CoreDataExerciseSession]
            if !fetchResults!.isEmpty {
                
                let thisExerciseData = fetchResults?[0]
                thisExerciseData?.startTime = startTime
                thisExerciseData?.endTime = endTime
                thisExerciseData?.stepsData = stepCount
                
                
                thisExerciseData?.insert()
                
                try context.save()
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    static func add(startTime: Double, endTime: Double, stepCount:StepCountCoreData,pulseRateData:PulseRateCoreData, breathingRateData:BreathingRateCoreData, temperatureData:SkinTemperatureCoreData, oxygenData:SaturatedOxygenCoreData)  {
        
        let startDate = Date(timeIntervalSince1970: startTime)
        let endDate = Date(timeIntervalSince1970: endTime)
        
        if CoreDataExerciseSession.isReadingAvailable(for: endDate) {
            update(startTime: startDate, endTime: endDate, stepCount: stepCount, pulseRateData: pulseRateData, breathingRateData: breathingRateData,temperatureData: temperatureData,oxygenData: oxygenData)
        } else {
            insert(startTime: startDate, endTime: endDate, stepCount: stepCount, pulseRateData: pulseRateData, breathingRateData: breathingRateData,temperatureData: temperatureData,oxygenData: oxygenData)
        }
    }
    
    static func fetch(for dateRange: Range<Date>) -> [CoreDataExerciseSession] {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate?.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExerciseSession.IDENTIFIER)
        fetchRequest.predicate = NSPredicate(format: "startTime >= %@ AND endTime =< %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
        
        do {
            let results = try managedContext?.fetch(fetchRequest)
            if let readings = results as? [CoreDataExerciseSession] {
                return readings
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return [CoreDataExerciseSession]()
    }
    
    static func fetch() -> [CoreDataExerciseSession] {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate?.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExerciseSession.IDENTIFIER)
        
        do {
            let results = try managedContext?.fetch(fetchRequest)
            if let readings = results as? [CoreDataExerciseSession] {
                return readings
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return [CoreDataExerciseSession]()
    }
    
    static func fetch(timeStamp: Double) -> CoreDataExerciseSession? {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate?.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExerciseSession.IDENTIFIER)
        fetchRequest.predicate = NSPredicate(format: "endTime = %@", Date(timeIntervalSince1970: timeStamp) as CVarArg)
        
        do {
            let results = try managedContext?.fetch(fetchRequest)
            if let readings = results as? [CoreDataExerciseSession] {
                return readings.first
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return nil
    }
    
    static func delete(for date: Date) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExerciseSession.IDENTIFIER)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedContext!.fetch(fetchRequest)
            for managedObject in results {
                let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
                managedContext!.delete(managedObjectData)
            }
            try managedContext?.save()
        } catch let error as NSError {
            print("delete all data in CoreDataExerciseSession entity. error : \(error) \(error.userInfo)")
        }
    }
    
    static func delete(for dateRange: Range<Date>){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate?.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExerciseSession.IDENTIFIER)
        fetchRequest.predicate = NSPredicate(format: "startTime >= %@ AND endTime =< %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
        
        do {
            let results = try managedContext!.fetch(fetchRequest)
            for managedObject in results {
                let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
                managedContext!.delete(managedObjectData)
            }
        } catch let error as NSError {
            print("Could not delete \(error), \(error.userInfo)")
        }
    }
    
}
