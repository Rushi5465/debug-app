//
//  CoreDataSleepStage.swift
//  BloodPressureApp
//
//  Created by Rushikant on 08/09/21.
//

import CoreData
import UIKit

class CoreDataSleepStage: NSManagedObject {
	
	@NSManaged var date: Date
	@NSManaged var value: NSNumber
	
	static let IDENTIFIER = "CoreDataSleepStage"
	
	static var instance: CoreDataSleepStage {
		
		let delegate = UIApplication.shared.delegate as! AppDelegate
		
		let bill_tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataSleepStage.IDENTIFIER, in:delegate.managedObjectContext!)
		let bill_TableRecord = NSManagedObject(entity: bill_tableEntity!,
											   insertInto: delegate.managedObjectContext) as! CoreDataSleepStage
		
		return(bill_TableRecord)
	}
	
	func insert() {
		
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		do {
			setValue(date, forKey: "date")
			setValue(value, forKey: "value")
			
			try managedContext!.save()
		} catch let error as NSError {
			print("Could not save \(error), \(error.userInfo)")
		}
	}
	
	func setCoreDataSleepStage(date: Date, value: NSNumber) {
		
		self.date = date
		self.value = value
	}
	
	private static func insertSleepStage(for date: Date, value: NSNumber) {
		let SleepStageInstance = CoreDataSleepStage.instance
		
		SleepStageInstance.setCoreDataSleepStage(date: date, value: value)
		SleepStageInstance.insert()
	}
	
	static func isSleepStageAvailable(for date: Date) -> Bool {
		
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataSleepStage.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataSleepStage] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func isSleepStageAvailable(range: Range<Double>) -> Bool {
//        if((range.upperBound - range.lowerBound) > 1440){
//            return false
//        }
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataSleepStage.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", Date(timeIntervalSince1970: range.lowerBound) as CVarArg, Date(timeIntervalSince1970: range.upperBound) as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataSleepStage] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func updateSleepStage(for date: Date, value: NSNumber) {
		
		let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
		let context: NSManagedObjectContext = appDel.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataSleepStage.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
		do {
			let fetchResults = try context.fetch(fetchRequest) as? [CoreDataSleepStage]
			if !fetchResults!.isEmpty {
				
				let thisSleepStage = fetchResults?[0]
				thisSleepStage?.value = value
				thisSleepStage?.insert()
				
				try context.save()
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
	}
	
	static func addSleepStage(timeInterval: TimeInterval, value: Int) {
		
		let date = Date(timeIntervalSince1970: timeInterval)
		if CoreDataSleepStage.isSleepStageAvailable(for: date) {
			updateSleepStage(for: date, value: NSNumber(value: value))
		} else {
			insertSleepStage(for: date, value: NSNumber(value: value))
		}
	}
	
	static func fetchSleepStageChartData(dateRange: DateRange) -> [ChartElement] {
		var dateList = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .hour)
		} else if dateRange.type == .monthly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .yearly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
		}
		
		var elementList = [ChartElement]()
		for index in 0 ..< dateList.count {
			var range: Range<Date>!
			if index == dateList.count - 1 {
				range = dateList[index]..<dateRange.end
			} else {
				let modDate = Calendar.current.date(byAdding: .second, value: -1, to: dateList[index + 1]) ?? dateList[index + 1]
				range = dateList[index]..<modDate
			}
			
			let chunkData = CoreDataSleepStage.fetchSleepStage(for: range)
			
			for z in 0 ..< chunkData.count {
				elementList.append(ChartElement(date: range.lowerBound, value: chunkData[z].value.doubleValue))
			}
		}
		
		return elementList
	}
	
	static func fetchSleepStageChartDataForSingleDay(dateRange: DateRange) -> [ChartElement] {
		var dateList = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 5, component: .minute)
		} else if dateRange.type == .monthly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .yearly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
		}
		
		var elementList = [ChartElement]()
		for index in 0 ..< dateList.count {
			var range: Range<Date>!
			if index == dateList.count - 1 {
				range = dateList[index]..<dateRange.end
			} else {
				let modDate = Calendar.current.date(byAdding: .second, value: -1, to: dateList[index + 1]) ?? dateList[index + 1]
				range = dateList[index]..<modDate
			}
			
			let chunkData = CoreDataSleepStage.fetchSleepStage(for: range)
			var averageSleep = 0.0
			for z in 0 ..< chunkData.count {
				averageSleep += chunkData[z].value.doubleValue
			}
			if chunkData.count > 0 {
				averageSleep = chunkData[0].value.doubleValue
			}
//			(averageSleep/Double((chunkData.count))).rounded()
			elementList.append(ChartElement(date: range.lowerBound, value: averageSleep))
		}
		
		return elementList
	}
	
	static func fetchSleepStage(for dateRange: Range<Date>) -> [CoreDataSleepStage] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataSleepStage.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataSleepStage] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataSleepStage]()
	}
	
	static func deleteSleepStage(for date: Date) {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataSleepStage.IDENTIFIER)
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedContext!.fetch(fetchRequest)
			for managedObject in results {
				let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
				managedContext!.delete(managedObjectData)
			}
			try managedContext?.save()
		} catch let error as NSError {
			print("delete all data in \(String(describing: entity)) error : \(error) \(error.userInfo)")
		}
	}
}
