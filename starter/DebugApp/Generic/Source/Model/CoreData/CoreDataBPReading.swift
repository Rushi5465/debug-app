//
//  CoreDataBPReading.swift
//  CoreDataBPReadingApp
//
//  Created by Sankalp Gupta on 24/08/21.
//

import CoreData
import UIKit

// swiftlint:disable line_length
class CoreDataBPReading: NSManagedObject {
	
	@NSManaged var date: Date
	@NSManaged var systolic: NSNumber
	@NSManaged var diastolic: NSNumber
	@NSManaged var pulseRate: NSNumber
	
	static let IDENTIFIER = "CoreDataBPReading"
	
	static var instance: CoreDataBPReading {
		
		let delegate = UIApplication.shared.delegate as! AppDelegate
		
		let bill_tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataBPReading.IDENTIFIER, in:delegate.managedObjectContext!)
		let bill_TableRecord = NSManagedObject(entity: bill_tableEntity!,
											   insertInto: delegate.managedObjectContext) as! CoreDataBPReading
		
		return(bill_TableRecord)
	}
	
	func insert() {
		
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		do {
			setValue(date, forKey: "date")
			setValue(systolic, forKey: "systolic")
			setValue(diastolic, forKey: "diastolic")
			setValue(pulseRate, forKey: "pulseRate")
			try managedContext!.save()
		} catch let error as NSError {
			print("Could not save \(error), \(error.userInfo)")
		}
	}
	
	func setCoreDataBPReading(date: Date, systolic: NSNumber, diastolic: NSNumber, pulseRate: NSNumber) {
		
		self.date = date
		self.systolic = systolic
		self.diastolic = diastolic
		self.pulseRate = pulseRate
	}
	
	static func insertBPReading(date: Date, systolic: NSNumber, diastolic: NSNumber, pulseRate: NSNumber) {
		let bpReadingInstance = CoreDataBPReading.instance
		
		bpReadingInstance.setCoreDataBPReading(date: date, systolic: systolic, diastolic: diastolic, pulseRate: pulseRate)
		bpReadingInstance.insert()
	}
	
	static func isBPReadingAvailable(for date: Date) -> Bool {
		
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBPReading.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataBPReading] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
    static func isBPReadingAvailable(range: Range<Double>) -> Bool {
//        if((range.upperBound - range.lowerBound) > 1440){
//            return false
//        }
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let managedContext = appDelegate.managedObjectContext!
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBPReading.IDENTIFIER)
            fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", Date(timeIntervalSince1970: range.lowerBound) as CVarArg, Date(timeIntervalSince1970: range.upperBound) as CVarArg)
            
            do {
                let results =
                    try managedContext.fetch(fetchRequest)
                if let allDrugs = results as? [CoreDataBPReading] {
                    if !allDrugs.isEmpty {
                        return true
                    }
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }
        return false
    }
    
    
	static func updateBPReading(for date: Date, systolic: NSNumber, diastolic: NSNumber, pulseRate: NSNumber) {
		
		let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
		let context: NSManagedObjectContext = appDel.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBPReading.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
		do {
			let fetchResults = try context.fetch(fetchRequest) as? [CoreDataBPReading]
			if !fetchResults!.isEmpty {
				
				let thisStepCount = fetchResults?[0]
				thisStepCount?.systolic = systolic
				thisStepCount?.diastolic = diastolic
				thisStepCount?.pulseRate = pulseRate
				thisStepCount?.insert()
				
				try context.save()
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
	}
	
	static func addBPReading(timeInterval: TimeInterval, systolic: Int, diastolic: Int, pulseRate: Int) {
		
		let date = Date(timeIntervalSince1970: timeInterval)
		if CoreDataBPReading.isBPReadingAvailable(for: date) {
			updateBPReading(for: date, systolic: NSNumber(value: systolic), diastolic: NSNumber(value: diastolic), pulseRate: NSNumber(value: pulseRate))
		} else {
			insertBPReading(date: date, systolic: NSNumber(value: systolic), diastolic: NSNumber(value: diastolic), pulseRate: NSNumber(value: pulseRate))
		}
	}
	
	static func fetchBloodPressureChartData(dateRange: DateRange) -> ([ChartElement], [ChartElement]) {
		var dateList = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 5, component: .minute)
		} else if dateRange.type == .monthly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .yearly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
		}
		
		var elementList1 = [ChartElement]()
		var elementList2 = [ChartElement]()
		for index in 0 ..< dateList.count {
			var range: Range<Date>!
			if index == dateList.count - 1 {
				range = dateList[index]..<dateRange.end
			} else {
				range = dateList[index]..<dateList[index + 1]
			}
			
			let chunkData = CoreDataBPReading.fetchBloodPressure(for: range)
			var chunkBPReadingDiastolic: Double? = chunkData.reduce(0) { (result: Double, nextItem: CoreDataBPReading) -> Double in
				return result + Double(truncating: nextItem.diastolic)
			}
			
			var chunkBPReadingSystolic: Double? = chunkData.reduce(0) { (result: Double, nextItem: CoreDataBPReading) -> Double in
				return result + Double(truncating: nextItem.systolic)
			}
			
			if chunkData.count > 0 {
				chunkBPReadingDiastolic = chunkBPReadingDiastolic! / Double(chunkData.count)
				chunkBPReadingSystolic = chunkBPReadingSystolic! / Double(chunkData.count)
			}
			
			if chunkBPReadingDiastolic == 0 {
				chunkBPReadingDiastolic = nil
			}
			
			if chunkBPReadingSystolic == 0 {
				chunkBPReadingSystolic = nil
			}
			
			elementList1.append(ChartElement(date: range.lowerBound, value: chunkBPReadingDiastolic))
			elementList2.append(ChartElement(date: range.lowerBound, value: chunkBPReadingSystolic))
		}
		
		return (elementList1, elementList2)
	}
	
	static func fetchBloodPressure(for dateRange: Range<Date>) -> [CoreDataBPReading] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBPReading.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataBPReading] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataBPReading]()
	}
	
	static func fetch(date: Date) -> CoreDataBPReading? {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBPReading.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date == %@", date as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataBPReading] {
				return readings.first
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return nil
	}
	
	static func deleteBloodPressureReading(for date: Date) {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBPReading.IDENTIFIER)
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedContext!.fetch(fetchRequest)
			for managedObject in results {
				let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
				managedContext!.delete(managedObjectData)
			}
			try managedContext?.save()
		} catch let error as NSError {
			print("delete all data in \(String(describing: entity)) error : \(error) \(error.userInfo)")
		}
	}
}
