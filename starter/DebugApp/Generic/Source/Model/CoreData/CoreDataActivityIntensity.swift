//
//  CoreDataActivityIntensity.swift
//  BloodPressureApp
//
//  Created by Rushikant on 29/12/21.
//

import CoreData
import UIKit

class CoreDataActivityIntensity: NSManagedObject {
	
	@NSManaged var date: Date
	@NSManaged var avgActivityIntensity: Double
	@NSManaged var avgPulseRate: Double
	
	static let IDENTIFIER = "CoreDataActivityIntensity"
	
	static var instance: CoreDataActivityIntensity {
		
		let delegate = UIApplication.shared.delegate as! AppDelegate
		
		let bill_tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataActivityIntensity.IDENTIFIER, in:delegate.managedObjectContext!)
		let bill_TableRecord = NSManagedObject(entity: bill_tableEntity!,
											   insertInto: delegate.managedObjectContext) as! CoreDataActivityIntensity
		
		return(bill_TableRecord)
	}
	
	func insert() {
		
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		do {
			setValue(date, forKey: "date")
			setValue(avgActivityIntensity, forKey: "avgActivityIntensity")
			setValue(avgPulseRate, forKey: "avgPulseRate")
			
			try managedContext!.save()
		} catch let error as NSError {
			print("Could not save \(error), \(error.userInfo)")
		}
	}
	
	func setCoreDataActivityIntensity(date: Date, avgActivityIntensity: Double, avgPulseRate: Double) {
		
		self.date = date
		self.avgActivityIntensity = avgActivityIntensity
		self.avgPulseRate = avgPulseRate
	}
	
	private static func insert(for date: Date, avgActivityIntensity: Double, avgPulseRate: Double) {
		let activityIntensityInstance = CoreDataActivityIntensity.instance
		
		activityIntensityInstance.setCoreDataActivityIntensity(date: date, avgActivityIntensity: avgActivityIntensity, avgPulseRate: avgPulseRate)
		activityIntensityInstance.insert()
	}
	
	static func isReadingAvailable(for date: Date) -> Bool {
		
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivityIntensity.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataActivityIntensity] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func isReadingAvailable(range: Range<Double>) -> Bool {
		if((range.upperBound - range.lowerBound) > 86400){
			return false
		}
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivityIntensity.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", Date(timeIntervalSince1970: range.lowerBound) as CVarArg, Date(timeIntervalSince1970: range.upperBound) as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataActivityIntensity] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func update(for date: Date, avgActivityIntensity: Double, avgPulseRate: Double) {
		
		let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
		let context: NSManagedObjectContext = appDel.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivityIntensity.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
		do {
			let fetchResults = try context.fetch(fetchRequest) as? [CoreDataActivityIntensity]
			if !fetchResults!.isEmpty {
				
				let thisIntensity = fetchResults?[0]
				thisIntensity?.avgActivityIntensity = avgActivityIntensity
				thisIntensity?.avgPulseRate = avgPulseRate
				thisIntensity?.insert()
				
				try context.save()
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
	}
	
	static func add(timeInterval: TimeInterval, avgActivityIntensity: Double, avgPulseRate: Double) {
		
		let date = Date(timeIntervalSince1970: timeInterval)
		if CoreDataActivityIntensity.isReadingAvailable(for: date) {
			update(for: date, avgActivityIntensity: avgActivityIntensity, avgPulseRate: avgPulseRate)
		} else {
			insert(for: date, avgActivityIntensity: avgActivityIntensity, avgPulseRate: avgPulseRate)
		}
	}
	
//	static func fetchActivityIntensityChartData(dateRange: DateRange) -> [ChartElement] {
//		var dateList = [Date]()
//		if dateRange.type == .weekly {
//			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
//		} else if dateRange.type == .daily {
//			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 5, component: .minute)
//		} else if dateRange.type == .monthly {
//			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
//		} else if dateRange.type == .yearly {
//			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
//		}
//		
//		var elementList = [ChartElement]()
//		for index in 0 ..< dateList.count {
//			var range: Range<Date>!
//			if index == dateList.count - 1 {
//				range = dateList[index]..<dateRange.end
//			} else {
//				range = dateList[index]..<dateList[index + 1]
//			}
//			
//			let chunkData = CoreDataActivityIntensity.fetch(for: range)
//			var chunkActivityIntensity: Double? = chunkData.reduce(0) { (result: Double, nextItem: CoreDataActivityIntensity) -> Double in
//				return result + nextItem.value.doubleValue
//			}
//			
//			if chunkData.count > 0 {
//				chunkActivityIntensity = chunkActivityIntensity! / Double(chunkData.count)
//			}
//			
//			if chunkActivityIntensity == 0 {
//				chunkActivityIntensity = nil
//			}
//			
//			if dateRange.type == .daily && chunkData.count != 0 {
//			if dateList.count > 1 {
//				elementList.append(ChartElement(date: range.lowerBound, value: Double(chunkData[0].value)))
//			} else {
//					elementList.append(ChartElement(date: range.lowerBound, value: Double(chunkData[0].value)))
//			}} else {
//				elementList.append(ChartElement(date: range.lowerBound, value: chunkActivityIntensity))
//			}
//			
////			elementList.append(ChartElement(date: range.lowerBound, value: chunkActivityIntensity))
//		}
//		
//		return elementList
//	}
	
	static func fetch(for dateRange: Range<Date>) -> [CoreDataActivityIntensity] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivityIntensity.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataActivityIntensity] {
					return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataActivityIntensity]()
	}
	
	static func fetch(timeStamp: Double) -> CoreDataActivityIntensity? {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivityIntensity.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", Date(timeIntervalSince1970: timeStamp) as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataActivityIntensity] {
				return readings.first
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return nil
	}
	
	static func fetch() -> [CoreDataActivityIntensity] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivityIntensity.IDENTIFIER)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataActivityIntensity] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataActivityIntensity]()
	}
	
	static func delete(for date: Date) {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivityIntensity.IDENTIFIER)
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedContext!.fetch(fetchRequest)
			for managedObject in results {
				let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
				managedContext!.delete(managedObjectData)
			}
			try managedContext?.save()
		} catch let error as NSError {
			print("delete all data in \(String(describing: entity)) error : \(error) \(error.userInfo)")
		}
	}
}


