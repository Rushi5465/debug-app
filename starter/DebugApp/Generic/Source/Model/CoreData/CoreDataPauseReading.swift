//
//  CoreDataPauseReading.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/08/21.
//

import CoreData
import UIKit

// swiftlint:disable line_length
class CoreDataPauseReading: NSManagedObject {
	
	@NSManaged var startTime: Date
	@NSManaged var endTime: Date
	@NSManaged var systolic: NSNumber?
	@NSManaged var diastolic: NSNumber?
	@NSManaged var pulseRate: NSNumber?
	@NSManaged var skinTemperature: NSNumber?
	@NSManaged var breathingRate: NSNumber?
	
	static let IDENTIFIER = "CoreDataPauseReading"
    
	static var instance: CoreDataPauseReading {
		
		let delegate = UIApplication.shared.delegate as! AppDelegate
		
		let bill_tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataPauseReading.IDENTIFIER, in:delegate.managedObjectContext!)
		let bill_TableRecord = NSManagedObject(entity: bill_tableEntity!,
											   insertInto: delegate.managedObjectContext) as! CoreDataPauseReading
		
		return(bill_TableRecord)
	}
	
	func insert() {
		
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		do {
			setValue(startTime, forKey: "startTime")
			setValue(endTime, forKey: "endTime")
			setValue(systolic, forKey: "systolic")
			setValue(diastolic, forKey: "diastolic")
			setValue(pulseRate, forKey: "pulseRate")
			setValue(skinTemperature, forKey: "skinTemperature")
			setValue(breathingRate, forKey: "breathingRate")
			
			try managedContext!.save()
		} catch let error as NSError {
			print("Could not save \(error), \(error.userInfo)")
		}
	}
	
	func setPauseReading(startTime: Date, endTime: Date, systolic: NSNumber?, diastolic: NSNumber?, pulseRate: NSNumber?, skinTemperature: NSNumber?, breathingRate: NSNumber?) {
		
		self.startTime = startTime
		self.endTime = endTime
		self.systolic = systolic
		self.diastolic = diastolic
		self.pulseRate = pulseRate
		self.skinTemperature = skinTemperature
		self.breathingRate = breathingRate
	}
	
	private static func insert(startTime: Date, endTime: Date, systolic: NSNumber?, diastolic: NSNumber?, pulseRate: NSNumber?, skinTemperature: NSNumber?, breathingRate: NSNumber?) {
		let pauseReadingInstance = CoreDataPauseReading.instance
		
		pauseReadingInstance.setPauseReading(startTime: startTime, endTime: endTime, systolic: systolic, diastolic: diastolic, pulseRate: pulseRate, skinTemperature: skinTemperature, breathingRate: breathingRate)
		pauseReadingInstance.insert()
	}
	
	static func isReadingAvailable(for date: Date) -> Bool {
		
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPauseReading.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "endTime = %@", date as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataPauseReading] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func isReadingAvailable(range: Range<Double>) -> Bool {
		if((range.upperBound - range.lowerBound) > 86400){
			return false
		}
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPauseReading.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "startTime >= %@ AND endTime <= %@", Date(timeIntervalSince1970: range.lowerBound) as CVarArg, Date(timeIntervalSince1970: range.upperBound) as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataPauseReading] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func update(startTime: Date, endTime: Date, systolic: NSNumber?, diastolic: NSNumber?, pulseRate: NSNumber?, skinTemperature: NSNumber?, breathingRate: NSNumber?) {
		
		let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
		let context: NSManagedObjectContext = appDel.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPauseReading.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "endTime = %@", endTime as CVarArg)
		do {
			let fetchResults = try context.fetch(fetchRequest) as? [CoreDataPauseReading]
			if !fetchResults!.isEmpty {
				
				let thisPauseReading = fetchResults?[0]
				thisPauseReading?.systolic = systolic
				thisPauseReading?.diastolic = diastolic
				thisPauseReading?.pulseRate = pulseRate
				thisPauseReading?.skinTemperature = skinTemperature
				thisPauseReading?.breathingRate = breathingRate
				thisPauseReading?.insert()
				
				try context.save()
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
	}
	
	static func add(startTime: TimeInterval, endTime: TimeInterval, systolic: Double?, diastolic: Double?, skinTemperature: Double?, pulseRate: Double?, breathingRate: Double?) {
		
		let startDate = Date(timeIntervalSince1970: startTime)
		let endDate = Date(timeIntervalSince1970: endTime)
		
		if CoreDataPauseReading.isReadingAvailable(for: endDate) {
			update(startTime: startDate, endTime: endDate, systolic: systolic as NSNumber?, diastolic: diastolic as NSNumber?, pulseRate: pulseRate as NSNumber?, skinTemperature: skinTemperature as NSNumber?, breathingRate: breathingRate as NSNumber?)
		} else {
			insert(startTime: startDate, endTime: endDate, systolic: systolic as NSNumber?, diastolic: diastolic as NSNumber?, pulseRate: pulseRate as NSNumber?, skinTemperature: skinTemperature as NSNumber?, breathingRate: breathingRate as NSNumber?)
		}
	}
	
	static func fetch(for dateRange: Range<Date>) -> [CoreDataPauseReading] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPauseReading.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "startTime >= %@ AND endTime <= %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataPauseReading] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataPauseReading]()
	}
	
	static func fetch(timeStamp: Double) -> CoreDataPauseReading? {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPauseReading.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "endTime = %@", Date(timeIntervalSince1970: timeStamp) as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataPauseReading] {
				return readings.first
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return nil
	}
	
	static func fetch() -> [CoreDataPauseReading] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPauseReading.IDENTIFIER)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataPauseReading] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataPauseReading]()
	}
	
	static func delete(for date: Date) {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPauseReading.IDENTIFIER)
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedContext!.fetch(fetchRequest)
			for managedObject in results {
				let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
				managedContext!.delete(managedObjectData)
			}
			try managedContext?.save()
		} catch let error as NSError {
			print("delete all data in \(String(describing: entity)) error : \(error) \(error.userInfo)")
		}
	}
}
