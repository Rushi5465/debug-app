//
//  CoreDataSleepTime.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 12/11/21.
//

import UIKit
import CoreData

// swiftlint:disable line_length
// swiftlint:disable all
class CoreDataSleepTime: NSManagedObject {
    @NSManaged var date: Date
    @NSManaged var sleepStartTime: String
    @NSManaged var sleepEndTime:String
	@NSManaged var deepValue: Int
	@NSManaged var remValue: Int
	@NSManaged var lightValue: Int
	@NSManaged var awakeValue: Int
	@NSManaged var avgHR: Double
	@NSManaged var avgHRV: Double
	@NSManaged var avgOxygen: Double
	@NSManaged var avgBreathingRate: Double
	@NSManaged var avgSystolicBP: Double
	@NSManaged var avgDiastolicBP: Double
	@NSManaged var avgSkinTempVar: Double
	@NSManaged var totalStepCount: Int
	@NSManaged var totalCaloriesBurnt: Double
    @NSManaged var healthScore: Double
    
    static let IDENTIFIER = "CoreDataSleepTime"
    
    static var instance: CoreDataSleepTime {
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        let sleepTime_tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataSleepTime.IDENTIFIER, in:delegate.managedObjectContext!)
        let sleepTime_TableRecord = NSManagedObject(entity: sleepTime_tableEntity!,
                                               insertInto: delegate.managedObjectContext) as! CoreDataSleepTime
        
        return(sleepTime_TableRecord)
    }
    
    func insert() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        do {
            setValue(date, forKey: "date")
            setValue(sleepStartTime, forKey: "sleepStartTime")
            setValue(sleepEndTime, forKey: "sleepEndTime")
			setValue(deepValue, forKey: "deepValue")
			setValue(lightValue, forKey: "lightValue")
			setValue(remValue, forKey: "remValue")
			setValue(awakeValue, forKey: "awakeValue")
			
			setValue(avgHR, forKey: "avgHR")
			setValue(avgHRV, forKey: "avgHRV")
			setValue(avgOxygen, forKey: "avgOxygen")
			setValue(avgBreathingRate, forKey: "avgBreathingRate")
			setValue(avgSystolicBP, forKey: "avgSystolicBP")
			setValue(avgDiastolicBP, forKey: "avgDiastolicBP")
			setValue(avgSkinTempVar, forKey: "avgSkinTempVar")
			
			setValue(totalStepCount, forKey: "totalStepCount")
			setValue(totalCaloriesBurnt, forKey: "totalCaloriesBurnt")
            setValue(healthScore, forKey: "healthScore")

            try managedContext!.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func setCoreDataSleeptime(date: Date, sleepStartTime: String,sleepEndTime:String,deepValue: Int,lightValue: Int,remValue: Int,awakeValue: Int,avgHR: Double,avgHRV: Double,avgOxygen: Double,avgBreathingRate: Double,avgSystolicBP: Double,avgDiastolicBP: Double,avgSkinTempVar: Double,totalStepCount: Int,totalCaloriesBurnt: Double, healthScore:Double) {
        
        self.date = date
        self.sleepStartTime = sleepStartTime
        self.sleepEndTime = sleepEndTime
		self.deepValue = deepValue
		self.lightValue = lightValue
		self.remValue = remValue
		self.awakeValue = awakeValue
		self.avgHR = avgHR
		self.avgHRV = avgHRV
		self.avgOxygen = avgOxygen
		self.avgBreathingRate = avgBreathingRate
		self.avgSystolicBP = avgSystolicBP
		self.avgDiastolicBP = avgDiastolicBP
		self.avgSkinTempVar = avgSkinTempVar
		self.totalStepCount = totalStepCount
		self.totalCaloriesBurnt = totalCaloriesBurnt
        self.healthScore = healthScore
		
    }
    
    private static func insertSleepTime(for date: Date, sleepStartTime: String,sleepEndTime:String,deepValue: Int,lightValue: Int,remValue: Int,awakeValue: Int, avgHR: Double,avgHRV: Double,avgOxygen: Double,avgBreathingRate: Double,avgSystolicBP: Double,avgDiastolicBP: Double,avgSkinTempVar: Double,totalStepCount: Int,totalCaloriesBurnt: Double, healthScore:Double) {
        let sleepTimeInstance = CoreDataSleepTime.instance
        sleepTimeInstance.setCoreDataSleeptime(date: date, sleepStartTime: sleepStartTime, sleepEndTime: sleepEndTime,deepValue: deepValue,lightValue:lightValue,remValue:remValue,awakeValue: awakeValue, avgHR: avgHR,avgHRV: avgHRV,avgOxygen: avgOxygen,avgBreathingRate: avgBreathingRate,avgSystolicBP: avgSystolicBP,avgDiastolicBP: avgDiastolicBP,avgSkinTempVar: avgSkinTempVar,totalStepCount: totalStepCount,totalCaloriesBurnt: totalCaloriesBurnt, healthScore: healthScore )
        sleepTimeInstance.insert()
    }
    
    static func fetch() -> [CoreDataSleepTime] {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate?.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataSleepTime.IDENTIFIER)
        
        do {
            let results = try managedContext?.fetch(fetchRequest)
            if let readings = results as? [CoreDataSleepTime] {
                return readings
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return [CoreDataSleepTime]()
    }
	
	static func fetch(for dateRange: Range<Date>) -> [CoreDataSleepTime] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataSleepTime.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataSleepTime] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataSleepTime]()
	}
    
    static func fetch(forDate: Date) -> CoreDataSleepTime? {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate?.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataSleepTime.IDENTIFIER)
        fetchRequest.predicate = NSPredicate(format: "date = %@", forDate as CVarArg)
        
        do {
            let results = try managedContext?.fetch(fetchRequest)
            if let readings = results as? [CoreDataSleepTime] {
                return readings.first
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return nil
    }
    
    
    static func isSleepTimeAvailable(for date: Date) -> Bool {
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let managedContext = appDelegate.managedObjectContext!
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataSleepTime.IDENTIFIER)
            fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
            
            do {
                let results =
                    try managedContext.fetch(fetchRequest)
                if let allDrugs = results as? [CoreDataSleepTime] {
                    if !allDrugs.isEmpty {
                        return true
                    }
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }
        return false
    }
    
    static func updateSleepTime(for date: Date, sleepStartTime: String,sleepEndTime:String,deepValue: Int,lightValue: Int,remValue: Int,awakeValue: Int,avgHR: Double,avgHRV: Double,avgOxygen: Double,avgBreathingRate: Double,avgSystolicBP: Double,avgDiastolicBP: Double,avgSkinTempVar: Double,totalStepCount: Int,totalCaloriesBurnt: Double,healthScore:Double) {
        
        let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context: NSManagedObjectContext = appDel.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataSleepTime.IDENTIFIER)
        fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
        do {
            let fetchResults = try context.fetch(fetchRequest) as? [CoreDataSleepTime]
            if !fetchResults!.isEmpty {
                
                let thisSleepStage = fetchResults?[0]
                thisSleepStage?.sleepStartTime = sleepStartTime
                thisSleepStage?.sleepEndTime = sleepEndTime
				thisSleepStage?.deepValue = deepValue
				thisSleepStage?.lightValue = lightValue
				thisSleepStage?.remValue = remValue
				thisSleepStage?.awakeValue = awakeValue
				thisSleepStage?.avgHR = avgHR
				thisSleepStage?.avgHRV = avgHRV
				thisSleepStage?.avgOxygen = avgOxygen
				thisSleepStage?.avgBreathingRate = avgBreathingRate
				thisSleepStage?.avgSystolicBP = avgSystolicBP
				thisSleepStage?.avgDiastolicBP = avgDiastolicBP
				thisSleepStage?.avgSkinTempVar = avgSkinTempVar
				thisSleepStage?.totalStepCount = totalStepCount
				thisSleepStage?.totalCaloriesBurnt = totalCaloriesBurnt
                thisSleepStage?.healthScore = healthScore
                thisSleepStage?.insert()
                
                try context.save()
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    static func addSleepTime(date: Date, sleepStartTime: String,sleepEndTime:String,deepValue: Int,lightValue: Int,remValue: Int,awakeValue: Int,avgHR: Double,avgHRV: Double,avgOxygen: Double,avgBreathingRate: Double,avgSystolicBP: Double,avgDiastolicBP: Double,avgSkinTempVar: Double,totalStepCount: Int,totalCaloriesBurnt: Double,healthScore:Double) {
        if CoreDataSleepTime.isSleepTimeAvailable(for: date) {
            updateSleepTime(for: date, sleepStartTime: sleepStartTime, sleepEndTime: sleepEndTime,deepValue: deepValue,lightValue:lightValue,remValue:remValue,awakeValue: awakeValue, avgHR: avgHR,avgHRV: avgHRV,avgOxygen: avgOxygen,avgBreathingRate: avgBreathingRate,avgSystolicBP: avgSystolicBP,avgDiastolicBP: avgDiastolicBP,avgSkinTempVar: avgSkinTempVar,totalStepCount: totalStepCount,totalCaloriesBurnt: totalCaloriesBurnt, healthScore: healthScore )
        } else {
            insertSleepTime(for: date, sleepStartTime: sleepStartTime, sleepEndTime: sleepEndTime, deepValue:deepValue, lightValue:lightValue, remValue: remValue, awakeValue: awakeValue, avgHR: avgHR,avgHRV: avgHRV,avgOxygen: avgOxygen,avgBreathingRate: avgBreathingRate,avgSystolicBP: avgSystolicBP,avgDiastolicBP: avgDiastolicBP,avgSkinTempVar: avgSkinTempVar,totalStepCount: totalStepCount,totalCaloriesBurnt: totalCaloriesBurnt, healthScore: healthScore)
        }
    }
    
    static func deleteSleepTime(for date: Date) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataSleepTime.IDENTIFIER)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedContext!.fetch(fetchRequest)
            for managedObject in results {
                let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
                managedContext!.delete(managedObjectData)
            }
            try managedContext?.save()
        } catch let error as NSError {
            print("delete all data in \(String(describing: entity)) error : \(error) \(error.userInfo)")
        }
    }
	
	static func fetchSleepAvgChartData(dateRange: DateRange) -> [[ChartElement]] {
		var dateList = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 5, component: .minute)
		} else if dateRange.type == .monthly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .yearly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
		}
		
		var deepElementList = [ChartElement]()
		var lightElementList = [ChartElement]()
		var remElementList = [ChartElement]()
		var awakeElementList = [ChartElement]()
		for index in 0 ..< dateList.count {
			let chunkData = CoreDataSleepTime.fetch(forDate: dateList[index])
			
			if chunkData != nil {
				deepElementList.append(ChartElement(date: dateList[index], value: Double(chunkData?.deepValue ?? 0)))
				lightElementList.append(ChartElement(date: dateList[index], value: Double(chunkData?.lightValue ?? 0)))
				remElementList.append(ChartElement(date: dateList[index], value: Double(chunkData?.remValue ?? 0)))
				awakeElementList.append(ChartElement(date: dateList[index], value: Double(chunkData?.awakeValue ?? 0)))
			}
		}
		
		return [deepElementList, lightElementList, remElementList, awakeElementList]
	}
	
	static func fetchWeekAvgChartData(dateRange: DateRange, dataType: DetailDataType!, isSys: Bool? = true) -> [ChartElement] {
		var dateList = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 5, component: .minute)
		} else if dateRange.type == .monthly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .yearly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
		}
		
		var elementList = [ChartElement]()
		
		let chunkData = CoreDataSleepTime.fetch(for: (dateRange.start ..< dateRange.end))
		if chunkData.count == dateList.count {
			for index in 0 ..< dateList.count {
				if chunkData.count > index {
					if dataType == .bpReading {
						if isSys! {
							elementList.append(ChartElement(date: dateList[index], value: Double(chunkData[index].avgSystolicBP )))
						} else {
							elementList.append(ChartElement(date: dateList[index], value: Double(chunkData[index].avgDiastolicBP )))
						}
					} else if dataType == .breathingRateReading {
						elementList.append(ChartElement(date: dateList[index], value: Double(chunkData[index].avgBreathingRate )))
					} else if dataType == .heartRateReading {
						elementList.append(ChartElement(date: dateList[index], value: Double(chunkData[index].avgHR )))
					} else if dataType == .hrvReading {
						elementList.append(ChartElement(date: dateList[index], value: Double(chunkData[index].avgHRV )))
					} else if dataType == .spO2Reading {
						elementList.append(ChartElement(date: dateList[index], value: Double(chunkData[index].avgOxygen )))
					} else if dataType == .tempratureReading {
						elementList.append(ChartElement(date: dateList[index], value: Double(chunkData[index].avgSkinTempVar )))
					}
				} else {
					if dataType == .bpReading {
						if isSys! {
							elementList.append(ChartElement(date: dateList[index], value: 0))
						} else {
							elementList.append(ChartElement(date: dateList[index], value: 0))
						}
					} else if dataType == .breathingRateReading {
						elementList.append(ChartElement(date: dateList[index], value: 0))
					} else if dataType == .heartRateReading {
						elementList.append(ChartElement(date: dateList[index], value: 0))
					} else if dataType == .hrvReading {
						elementList.append(ChartElement(date: dateList[index], value: 0))
					} else if dataType == .spO2Reading {
						elementList.append(ChartElement(date: dateList[index], value: 0))
					} else if dataType == .tempratureReading {
						elementList.append(ChartElement(date: dateList[index], value: 0))
					}
				}
			}
		} else {
			var newElementList = [ChartElement](repeating: ChartElement(date: Date()), count:7)
			for x in 0..<dateList.count {
				newElementList[x] = ChartElement(date: dateList[x], value: nil)
				for z in 0..<chunkData.count {
					if chunkData[z].date == dateList[x] {
						if dataType == .bpReading {
							if isSys! {
								newElementList[x] = ChartElement(date: dateList[x], value: Double(chunkData[z].avgSystolicBP ))
							} else {
								newElementList[x] = ChartElement(date: dateList[x], value: Double(chunkData[z].avgDiastolicBP ))
							}
						} else if dataType == .breathingRateReading {
							newElementList[x] = ChartElement(date: dateList[x], value: Double(chunkData[z].avgBreathingRate ))
						} else if dataType == .heartRateReading {
							newElementList[x] = ChartElement(date: dateList[x], value: Double(chunkData[z].avgHR ))
						} else if dataType == .hrvReading {
							newElementList[x] = ChartElement(date: dateList[x], value: Double(chunkData[z].avgHRV ))
						} else if dataType == .spO2Reading {
							newElementList[x] = ChartElement(date: dateList[x], value: Double(chunkData[z].avgOxygen ))
						} else if dataType == .tempratureReading {
							newElementList[x] = ChartElement(date: dateList[x], value: Double(chunkData[z].avgSkinTempVar ))
						}
					}
				}
			}
			elementList = newElementList
		}
		return elementList
	}
	
	static func fetchSleepWeekAvgChartData(dateRange: DateRange) -> [[ChartElement]] {
		var dateList = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 5, component: .minute)
		} else if dateRange.type == .monthly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .yearly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
		}
		
		let chunkData = CoreDataSleepTime.fetch(for: (dateRange.start ..< dateRange.end))
		var deepElementList = [ChartElement]()
		var lightElementList = [ChartElement]()
		var remElementList = [ChartElement]()
		var awakeElementList = [ChartElement]()
		if chunkData.count == dateList.count {
		for index in 0 ..< dateList.count {
			if chunkData.count > index {
				deepElementList.append(ChartElement(date: dateList[index], value: Double(chunkData[index].deepValue )))
				lightElementList.append(ChartElement(date: dateList[index], value: Double(chunkData[index].lightValue )))
				remElementList.append(ChartElement(date: dateList[index], value: Double(chunkData[index].remValue )))
				awakeElementList.append(ChartElement(date: dateList[index], value: Double(chunkData[index].awakeValue )))
			} else {
				deepElementList.append(ChartElement(date: dateList[index], value: Double(0)))
				lightElementList.append(ChartElement(date: dateList[index], value: Double(0)))
				remElementList.append(ChartElement(date: dateList[index], value: Double(0)))
				awakeElementList.append(ChartElement(date: dateList[index], value: Double(0)))
			}
		} } else {
			var newDeepElementList = [ChartElement](repeating: ChartElement(date: Date()), count:7)
			var newLightElementList = [ChartElement](repeating: ChartElement(date: Date()), count:7)
			var newRemElementList = [ChartElement](repeating: ChartElement(date: Date()), count:7)
			var newAwakeElementList = [ChartElement](repeating: ChartElement(date: Date()), count:7)
			for x in 0..<dateList.count {
				newDeepElementList[x] = ChartElement(date: dateList[x], value: nil)
				newLightElementList[x] = ChartElement(date: dateList[x], value: nil)
				newRemElementList[x] = ChartElement(date: dateList[x], value: nil)
				newAwakeElementList[x] = ChartElement(date: dateList[x], value: nil)
				for z in 0..<chunkData.count {
					if chunkData[z].date == dateList[x] {
						newDeepElementList[x] = ChartElement(date: dateList[x], value: Double(chunkData[z].deepValue ))
						newLightElementList[x] = ChartElement(date: dateList[x], value: Double(chunkData[z].lightValue ))
						newRemElementList[x] = ChartElement(date: dateList[x], value: Double(chunkData[z].remValue ))
						newAwakeElementList[x] = ChartElement(date: dateList[x], value: Double(chunkData[z].awakeValue ))
					}
				}
			}
			return [newDeepElementList, newLightElementList, newRemElementList, newAwakeElementList]
		}
		
		return [deepElementList, lightElementList, remElementList, awakeElementList]
	}
}
