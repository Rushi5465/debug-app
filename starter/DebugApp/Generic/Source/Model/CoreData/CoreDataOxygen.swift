//
//  CoreDataOxygen.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 26/08/21.
//

import CoreData
import UIKit

class CoreDataOxygen: NSManagedObject {
	
	@NSManaged var date: Date
	@NSManaged var value: NSNumber
	
	static let IDENTIFIER = "CoreDataOxygen"
	
	static var instance: CoreDataOxygen {
		
		let delegate = UIApplication.shared.delegate as! AppDelegate
		
		let bill_tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataOxygen.IDENTIFIER, in:delegate.managedObjectContext!)
		let bill_TableRecord = NSManagedObject(entity: bill_tableEntity!,
											   insertInto: delegate.managedObjectContext) as! CoreDataOxygen
		
		return(bill_TableRecord)
	}
	
	func insert() {
		
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		do {
			setValue(date, forKey: "date")
			setValue(value, forKey: "value")
			
			try managedContext!.save()
		} catch let error as NSError {
			print("Could not save \(error), \(error.userInfo)")
		}
	}
	
	func setCoreDataOxygen(date: Date, value: NSNumber) {
		
		self.date = date
		self.value = value
	}
	
	private static func insert(for date: Date, value: NSNumber) {
		let OxygenReadingInstance = CoreDataOxygen.instance
		
		OxygenReadingInstance.setCoreDataOxygen(date: date, value: value)
		OxygenReadingInstance.insert()
	}
	
	static func isReadingAvailable(for date: Date) -> Bool {
		
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataOxygen.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
			
			do {
				let results =
				try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataOxygen] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func isReadingAvailable(range: Range<Double>) -> Bool {
		if((range.upperBound - range.lowerBound) > 86400){
			return false
		}
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataOxygen.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", Date(timeIntervalSince1970: range.lowerBound) as CVarArg, Date(timeIntervalSince1970: range.upperBound) as CVarArg)
			
			do {
				let results =
				try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataOxygen] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func update(for date: Date, value: NSNumber) {
		
		let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
		let context: NSManagedObjectContext = appDel.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataOxygen.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
		do {
			let fetchResults = try context.fetch(fetchRequest) as? [CoreDataOxygen]
			if !fetchResults!.isEmpty {
				
				let thisOxygenReading = fetchResults?[0]
				thisOxygenReading?.value = value
				thisOxygenReading?.insert()
				
				try context.save()
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
	}
	
	static func add(timeInterval: TimeInterval, value: Int) {
		
		let date = Date(timeIntervalSince1970: timeInterval)
		if CoreDataOxygen.isReadingAvailable(for: date) {
			update(for: date, value: NSNumber(value: value))
		} else {
			insert(for: date, value: NSNumber(value: value))
		}
	}
	
	static func fetchOxygenReadingChartData(dateRange: DateRange) -> [ChartElement] {
		var dateList = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 5, component: .minute)
		}
		
		var elementList = [ChartElement]()
		for index in 0 ..< dateList.count {
			var range: Range<Date>!
			if index == dateList.count - 1 {
				range = dateList[index]..<dateRange.end
			} else {
				range = dateList[index]..<dateList[index + 1]
			}
			
			let chunkData = CoreDataOxygen.fetch(for: range)
			var chunkOxygenReading: Double? = chunkData.reduce(0) { (result: Double, nextItem: CoreDataOxygen) -> Double in
				return result + nextItem.value.doubleValue
			}
			
			if chunkData.count > 0 {
				chunkOxygenReading = chunkOxygenReading! / Double(chunkData.count)
			}
			
			if chunkOxygenReading == 0 {
				chunkOxygenReading = nil
			}
			
			if dateRange.type == .daily && chunkData.count != 0 {
				if dateList.count > 1 {
					elementList.append(ChartElement(date: range.lowerBound, value: Double(chunkData[0].value)))
				} else {
					elementList.append(ChartElement(date: range.lowerBound, value: Double(chunkData[0].value)))
				}} else {
				elementList.append(ChartElement(date: range.lowerBound, value: chunkOxygenReading))
			}
			
//			elementList.append(ChartElement(date: range.lowerBound, value: chunkOxygenReading))
		}
		
		return elementList
	}
	
	static func fetch(for dateRange: Range<Date>) -> [CoreDataOxygen] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataOxygen.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataOxygen] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataOxygen]()
	}
	
	static func fetch(timeStamp: Double) -> CoreDataOxygen? {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataOxygen.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", Date(timeIntervalSince1970: timeStamp) as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataOxygen] {
				return readings.first
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return nil
	}
	
	static func fetch() -> [CoreDataOxygen] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataOxygen.IDENTIFIER)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataOxygen] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataOxygen]()
	}
	
	static func delete(for date: Date) {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataOxygen.IDENTIFIER)
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedContext!.fetch(fetchRequest)
			for managedObject in results {
				let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
				managedContext!.delete(managedObjectData)
			}
			try managedContext?.save()
		} catch let error as NSError {
			print("delete all data in \(String(describing: entity)) error : \(error) \(error.userInfo)")
		}
	}
}
