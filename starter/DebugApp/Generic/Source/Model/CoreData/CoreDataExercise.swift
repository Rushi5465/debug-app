//
//  CoreDataExercise.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 26/08/21.
//

import CoreData
import UIKit

// swiftlint:disable line_length
// swiftlint:disable all
class CoreDataExercise: NSManagedObject {
	
	@NSManaged var startTime: Date
	@NSManaged var endTime: Date
	@NSManaged var breathingRate: NSNumber?
	@NSManaged var diastolic: NSNumber?
	@NSManaged var systolic: NSNumber?
	@NSManaged var skinTemperature: NSNumber?
	@NSManaged var pulseRate: NSNumber?
	@NSManaged var caloriesBurnt: NSNumber?
	@NSManaged var stepCount: NSNumber?
	@NSManaged var distanceCovered: NSNumber?
	@NSManaged var oxygen: NSNumber?
	
	static let IDENTIFIER = "CoreDataExercise"
	static var instance: CoreDataExercise {
		
		let delegate = UIApplication.shared.delegate as! AppDelegate
		
		let bill_tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataExercise.IDENTIFIER, in:delegate.managedObjectContext!)
		let bill_TableRecord = NSManagedObject(entity: bill_tableEntity!,
											   insertInto: delegate.managedObjectContext) as! CoreDataExercise
		
		return(bill_TableRecord)
	}
	
	func insert() {
		
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		do {
			setValue(startTime, forKey: "startTime")
			setValue(endTime, forKey: "endTime")
			setValue(breathingRate, forKey: "breathingRate")
			setValue(diastolic, forKey: "diastolic")
			setValue(systolic, forKey: "systolic")
			setValue(skinTemperature, forKey: "skinTemperature")
			setValue(pulseRate, forKey: "pulseRate")
			setValue(caloriesBurnt, forKey: "caloriesBurnt")
			setValue(stepCount, forKey: "stepCount")
			setValue(distanceCovered, forKey: "distanceCovered")
			setValue(oxygen, forKey: "oxygen")
			
			try managedContext!.save()
		} catch let error as NSError {
			print("Could not save \(error), \(error.userInfo)")
		}
	}
	
	func setExerciseData(startTime: Date, endTime: Date, breathingRate: NSNumber?, diastolic: NSNumber?, systolic: NSNumber?, skinTemperature: NSNumber?, pulseRate: NSNumber?, caloriesBurnt: NSNumber?, stepCount: NSNumber?, distanceCovered: NSNumber?, oxygen: NSNumber?) {
		
		self.startTime = startTime
		self.endTime = endTime
		self.breathingRate = breathingRate
		self.diastolic = diastolic
		self.systolic = systolic
		self.skinTemperature = skinTemperature
		self.pulseRate = pulseRate
		self.caloriesBurnt = caloriesBurnt
		self.stepCount = stepCount
		self.distanceCovered = distanceCovered
		self.oxygen = oxygen
	}
	
	private static func insert(startTime: Date, endTime: Date, breathingRate: NSNumber?, diastolic: NSNumber?, systolic: NSNumber?, skinTemperature: NSNumber?, pulseRate: NSNumber?, caloriesBurnt: NSNumber?, stepCount: NSNumber?, distanceCovered: NSNumber?, oxygen: NSNumber?) {
		
		let exerciseInstance = CoreDataExercise.instance
		
		exerciseInstance.setExerciseData(startTime: startTime, endTime: endTime, breathingRate: breathingRate, diastolic: diastolic, systolic: systolic, skinTemperature: skinTemperature, pulseRate: pulseRate, caloriesBurnt: caloriesBurnt, stepCount: stepCount, distanceCovered: distanceCovered, oxygen: oxygen)
		exerciseInstance.insert()
	}
	
	static func isReadingAvailable(for date: Date) -> Bool {
		
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExercise.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "endTime = %@", date as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataExercise] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func isReadingAvailable(range: Range<Double>) -> Bool {
		if((range.upperBound - range.lowerBound) > 86400){
			return false
		}
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExercise.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "startTime >= %@ AND endTime <= %@", Date(timeIntervalSince1970: range.lowerBound) as CVarArg, Date(timeIntervalSince1970: range.upperBound) as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataExercise] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func update(startTime: Date, endTime: Date, breathingRate: NSNumber?, diastolic: NSNumber?, systolic: NSNumber?, skinTemperature: NSNumber?, pulseRate: NSNumber?, caloriesBurnt: NSNumber?, stepCount: NSNumber?, distanceCovered: NSNumber?, oxygen: NSNumber?) {
		
		let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
		let context: NSManagedObjectContext = appDel.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExercise.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "endTime = %@", endTime as CVarArg)
		do {
			let fetchResults = try context.fetch(fetchRequest) as? [CoreDataExercise]
			if !fetchResults!.isEmpty {
				
				let thisExerciseData = fetchResults?[0]
				thisExerciseData?.breathingRate = breathingRate
				thisExerciseData?.diastolic = diastolic
				thisExerciseData?.systolic = systolic
				thisExerciseData?.skinTemperature = skinTemperature
				thisExerciseData?.pulseRate = pulseRate
				thisExerciseData?.caloriesBurnt = caloriesBurnt
				thisExerciseData?.stepCount = stepCount
				thisExerciseData?.distanceCovered = distanceCovered
				thisExerciseData?.oxygen = oxygen
				
				thisExerciseData?.insert()
				
				try context.save()
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
	}
	
	static func add(startTime: TimeInterval, endTime: TimeInterval, systolic: Double?, diastolic: Double?, breathingRate: Double?, skinTemperature: Double?, pulseRate: Double?, caloriesBurnt: Double?, stepCount: Double?, distanceCovered: Double?, oxygen: Double?) {
		
		let startDate = Date(timeIntervalSince1970: startTime)
		let endDate = Date(timeIntervalSince1970: endTime)
		
		if CoreDataExercise.isReadingAvailable(for: endDate) {
			update(startTime: startDate, endTime: endDate, breathingRate: breathingRate as NSNumber?, diastolic: diastolic as NSNumber?, systolic: systolic as NSNumber?, skinTemperature: skinTemperature as NSNumber?, pulseRate: pulseRate as NSNumber?, caloriesBurnt: caloriesBurnt as NSNumber?, stepCount: stepCount as NSNumber?, distanceCovered: distanceCovered as NSNumber?, oxygen: oxygen as NSNumber?)
		} else {
			insert(startTime: startDate, endTime: endDate, breathingRate: breathingRate as NSNumber?, diastolic: diastolic as NSNumber?, systolic: systolic as NSNumber?, skinTemperature: skinTemperature as NSNumber?, pulseRate: pulseRate as NSNumber?, caloriesBurnt: caloriesBurnt as NSNumber?, stepCount: stepCount as NSNumber?, distanceCovered: distanceCovered as NSNumber?, oxygen: oxygen as NSNumber?)
		}
	}
	
	static func fetch(for dateRange: Range<Date>) -> [CoreDataExercise] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExercise.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "startTime >= %@ AND endTime <= %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataExercise] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataExercise]()
	}
	
	static func fetch(timeStamp: Double) -> CoreDataExercise? {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExercise.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "endTime = %@", Date(timeIntervalSince1970: timeStamp) as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataExercise] {
				return readings.first
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return nil
	}
	
	static func delete(for date: Date) {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataExercise.IDENTIFIER)
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedContext!.fetch(fetchRequest)
			for managedObject in results {
				let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
				managedContext!.delete(managedObjectData)
			}
			try managedContext?.save()
		} catch let error as NSError {
			print("delete all data in \(String(describing: entity)) error : \(error) \(error.userInfo)")
		}
	}
}
