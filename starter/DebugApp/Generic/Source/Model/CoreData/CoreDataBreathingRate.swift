//
//  CoreDataBreathingRate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/08/21.
//

import CoreData
import UIKit

class CoreDataBreathingRate: NSManagedObject {
	
	@NSManaged var date: Date
	@NSManaged var value: NSNumber
	
	static let IDENTIFIER = "CoreDataBreathingRate"
	
	static var instance: CoreDataBreathingRate {
		
		let delegate = UIApplication.shared.delegate as! AppDelegate
		
		let bill_tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataBreathingRate.IDENTIFIER, in:delegate.managedObjectContext!)
		let bill_TableRecord = NSManagedObject(entity: bill_tableEntity!,
											   insertInto: delegate.managedObjectContext) as! CoreDataBreathingRate
		
		return(bill_TableRecord)
	}
	
	func insert() {
		
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		do {
			setValue(date, forKey: "date")
			setValue(value, forKey: "value")
			
			try managedContext!.save()
		} catch let error as NSError {
			print("Could not save \(error), \(error.userInfo)")
		}
	}
	
	func setCoreDataBreathingRate(date: Date, value: NSNumber) {
		
		self.date = date
		self.value = value
	}
	
	private static func insert(for date: Date, value: NSNumber) {
		let BreathingRateInstance = CoreDataBreathingRate.instance
		
		BreathingRateInstance.setCoreDataBreathingRate(date: date, value: value)
		BreathingRateInstance.insert()
	}
	
	static func isReadingAvailable(for date: Date) -> Bool {
		
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBreathingRate.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataBreathingRate] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func isReadingAvailable(range: Range<Double>) -> Bool {
		if((range.upperBound - range.lowerBound) > 86400){
			return false
		}
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			let managedContext = appDelegate.managedObjectContext!
			
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBreathingRate.IDENTIFIER)
			fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", Date(timeIntervalSince1970: range.lowerBound) as CVarArg, Date(timeIntervalSince1970: range.upperBound) as CVarArg)
			
			do {
				let results =
					try managedContext.fetch(fetchRequest)
				if let allDrugs = results as? [CoreDataBreathingRate] {
					if !allDrugs.isEmpty {
						return true
					}
				}
			} catch let error as NSError {
				print("Could not fetch \(error), \(error.userInfo)")
			}
		}
		return false
	}
	
	static func update(for date: Date, value: NSNumber) {
		
		let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
		let context: NSManagedObjectContext = appDel.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBreathingRate.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
		do {
			let fetchResults = try context.fetch(fetchRequest) as? [CoreDataBreathingRate]
			if !fetchResults!.isEmpty {
				
				let thisBreathingRate = fetchResults?[0]
				thisBreathingRate?.value = value
				thisBreathingRate?.insert()
				
				try context.save()
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
	}
	
	static func add(timeInterval: TimeInterval, value: Double) {
		
		let date = Date(timeIntervalSince1970: timeInterval)
		if CoreDataBreathingRate.isReadingAvailable(for: date) {
			update(for: date, value: NSNumber(value: value))
		} else {
			insert(for: date, value: NSNumber(value: value))
		}
	}
	
	static func fetchBreathingRateChartData(dateRange: DateRange) -> [ChartElement] {
		var dateList = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 5, component: .minute)
		} else if dateRange.type == .monthly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .yearly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
		}
		
		var elementList = [ChartElement]()
		for index in 0 ..< dateList.count {
			var range: Range<Date>!
			if index == dateList.count - 1 {
				range = dateList[index]..<dateRange.end
			} else {
				range = dateList[index]..<dateList[index + 1]
			}
			
			let chunkData = CoreDataBreathingRate.fetch(for: range)
			var chunkBreathingRate: Double? = chunkData.reduce(0) { (result: Double, nextItem: CoreDataBreathingRate) -> Double in
				return result + nextItem.value.doubleValue
			}
			if chunkData.count > 0 {
				chunkBreathingRate = chunkBreathingRate! / Double(chunkData.count)
			}
			
			if chunkBreathingRate == 0 {
				chunkBreathingRate = nil
			}
			
			if dateRange.type == .daily && chunkData.count != 0 {
				if dateList.count > 1 {
					elementList.append(ChartElement(date: range.lowerBound, value: Double(chunkData[0].value)))
				} else {
					elementList.append(ChartElement(date: range.lowerBound, value: Double(chunkData[0].value)))
				} } else {
				elementList.append(ChartElement(date: range.lowerBound, value: chunkBreathingRate))
			}
			
//			elementList.append(ChartElement(date: range.lowerBound, value: chunkBreathingRate))
		}
		
		return elementList
	}
	
	static func fetch(for dateRange: Range<Date>) -> [CoreDataBreathingRate] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBreathingRate.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataBreathingRate] {
					return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataBreathingRate]()
	}
	
	static func fetch(timeStamp: Double) -> CoreDataBreathingRate? {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBreathingRate.IDENTIFIER)
		fetchRequest.predicate = NSPredicate(format: "date = %@", Date(timeIntervalSince1970: timeStamp) as CVarArg)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataBreathingRate] {
				return readings.first
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return nil
	}
	
	static func fetch() -> [CoreDataBreathingRate] {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate?.managedObjectContext!
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBreathingRate.IDENTIFIER)
		
		do {
			let results = try managedContext?.fetch(fetchRequest)
			if let readings = results as? [CoreDataBreathingRate] {
				return readings
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
		return [CoreDataBreathingRate]()
	}
	
	static func delete(for date: Date) {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataBreathingRate.IDENTIFIER)
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedContext!.fetch(fetchRequest)
			for managedObject in results {
				let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
				managedContext!.delete(managedObjectData)
			}
			try managedContext?.save()
		} catch let error as NSError {
			print("delete all data in \(String(describing: entity)) error : \(error) \(error.userInfo)")
		}
	}
}
