//
//  CoreDataActivity.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 24/11/21.
//

import UIKit
import CoreData

// swiftlint:disable line_length
// swiftlint:disable all
class CoreDataActivity: NSManagedObject {
    
    @NSManaged var date: Date
    @NSManaged var value: Double
    
    static let IDENTIFIER = "CoreDataActivity"
    
    static var instance: CoreDataActivity {
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        let bill_tableEntity =  NSEntityDescription.entity(forEntityName: CoreDataActivity.IDENTIFIER, in:delegate.managedObjectContext!)
        let bill_TableRecord = NSManagedObject(entity: bill_tableEntity!,
                                               insertInto: delegate.managedObjectContext) as! CoreDataActivity
        
        return(bill_TableRecord)
    }
    
    func insert() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        do {
            setValue(date, forKey: "date")
            setValue(value, forKey: "value")
            
            try managedContext!.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func setCoreDataActivity(date: Date, value: Double) {
        
        self.date = date
        self.value = value
    }
    
    private static func insert(for date: Date, value: Double) {
        let BreathingRateInstance = CoreDataActivity.instance
        
        BreathingRateInstance.setCoreDataActivity(date: date, value: value)
        BreathingRateInstance.insert()
    }
    
    static func isReadingAvailable(for date: Date) -> Bool {
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let managedContext = appDelegate.managedObjectContext!
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivity.IDENTIFIER)
            fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
            
            do {
                let results =
                    try managedContext.fetch(fetchRequest)
                if let allDrugs = results as? [CoreDataActivity] {
                    if !allDrugs.isEmpty {
                        return true
                    }
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }
        return false
    }
    
    static func isReadingAvailable(range: Range<Double>) -> Bool {
		if((range.upperBound - range.lowerBound) > 86400){
			return false
		}
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let managedContext = appDelegate.managedObjectContext!
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivity.IDENTIFIER)
            fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", Date(timeIntervalSince1970: range.lowerBound) as CVarArg, Date(timeIntervalSince1970: range.upperBound) as CVarArg)
            
            do {
                let results =
                    try managedContext.fetch(fetchRequest)
                if let allDrugs = results as? [CoreDataActivity] {
                    if !allDrugs.isEmpty {
                        return true
                    }
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }
        return false
    }
    
    static func update(for date: Date, value: Double) {
        
        let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context: NSManagedObjectContext = appDel.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivity.IDENTIFIER)
        fetchRequest.predicate = NSPredicate(format: "date = %@", date as CVarArg)
        do {
            let fetchResults = try context.fetch(fetchRequest) as? [CoreDataActivity]
            if !fetchResults!.isEmpty {
                
                let activity = fetchResults?[0]
                activity?.value = value
                activity?.insert()
                
                try context.save()
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    static func add(timeInterval: TimeInterval, value: Double) {
        
        let date = Date(timeIntervalSince1970: timeInterval)
        if CoreDataActivity.isReadingAvailable(for: date) {
            update(for: date, value: value)
        } else {
            insert(for: date, value: value)
        }
    }
    
	static func fetchActivityChartData(dateRange: DateRange, isAverage: Bool? = false) -> ([ChartElement], [ChartElement]) {
		var dateList = [Date]()
		var dateList2 = [Date]()
		if dateRange.type == .weekly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .daily {
			if isAverage! {
				dateList2 = Date.dates(from: dateRange.start, to: dateRange.end, interval: 5, component: .minute)
				dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .hour)
			} else {
				dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .hour)
			}
		} else if dateRange.type == .monthly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .day)
		} else if dateRange.type == .yearly {
			dateList = Date.dates(from: dateRange.start, to: dateRange.end, interval: 1, component: .month)
		}
		
		var elementList = [ChartElement]()
		for index in 0 ..< dateList.count {
			var range: Range<Date>!
			if index == dateList.count - 1 {
				range = dateList[index]..<dateRange.end
			} else {
				range = dateList[index]..<dateList[index + 1]
			}
			
			let chunkData = CoreDataActivity.fetch(for: range)
			var chunkBreathingRate: Double? = chunkData.reduce(0) { (result: Double, nextItem: CoreDataActivity) -> Double in
				return result + nextItem.value
			}
			
			var nonNilChunkDataCount = 0
			for z in 0..<chunkData.count {
				if chunkData[z].value != 0 {
					nonNilChunkDataCount = nonNilChunkDataCount + 1
				}
			}
			if chunkData.count > 0 {
				chunkBreathingRate = (nonNilChunkDataCount != 0) ? (chunkBreathingRate! / Double(nonNilChunkDataCount)) : (chunkBreathingRate! / Double(chunkData.count))
			}
			
			if chunkBreathingRate == 0 {
				//                chunkBreathingRate = nil
			}
			
			if dateRange.type == .daily && chunkData.count != 0 {
				if dateList.count > 1 {
					elementList.append(ChartElement(date: range.lowerBound, value: chunkBreathingRate))
				} else {
					elementList.append(ChartElement(date: range.lowerBound, value: chunkBreathingRate))
				} } else {
					elementList.append(ChartElement(date: range.lowerBound, value: chunkBreathingRate))
				}
		}
		
		var elementList2 = [ChartElement]()
		for index in 0..<dateList2.count {
			var range: Range<Date>!
			if index == dateList2.count - 1 {
				range = dateList2[index]..<dateRange.end
			} else {
				range = dateList2[index]..<dateList2[index + 1]
			}
			
			let chunkData = CoreDataActivity.fetch(for: range)
			var chunkBreathingRate: Double? = chunkData.reduce(0) { (result: Double, nextItem: CoreDataActivity) -> Double in
				return result + nextItem.value
			}
			
			var nonNilChunkDataCount2 = 0
			for z in 0..<chunkData.count {
				if chunkData[z].value != 0 {
					nonNilChunkDataCount2 = nonNilChunkDataCount2 + 1
				}
			}
			
			if chunkData.count > 0 {
				chunkBreathingRate = (nonNilChunkDataCount2 != 0) ? (chunkBreathingRate! / Double(nonNilChunkDataCount2)) : (chunkBreathingRate! / Double(chunkData.count))

			}
			
			if dateRange.type == .daily && chunkData.count != 0 {
				if dateList2.count > 1 {
					elementList2.append(ChartElement(date: range.lowerBound, value: chunkBreathingRate))
				} else {
					elementList2.append(ChartElement(date: range.lowerBound, value: chunkBreathingRate))
				} } else {
					elementList2.append(ChartElement(date: range.lowerBound, value: chunkBreathingRate))
				}
		}
		return (elementList, elementList2)
	}
    
    static func fetch(for dateRange: Range<Date>) -> [CoreDataActivity] {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate?.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivity.IDENTIFIER)
        fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", dateRange.lowerBound as CVarArg, dateRange.upperBound as CVarArg)
        
        do {
            let results = try managedContext?.fetch(fetchRequest)
            if let readings = results as? [CoreDataActivity] {
                    return readings
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return [CoreDataActivity]()
    }
    
    static func fetch(timeStamp: Double) -> CoreDataActivity? {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate?.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivity.IDENTIFIER)
        fetchRequest.predicate = NSPredicate(format: "date = %@", Date(timeIntervalSince1970: timeStamp) as CVarArg)
        
        do {
            let results = try managedContext?.fetch(fetchRequest)
            if let readings = results as? [CoreDataActivity] {
                return readings.first
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return nil
    }
    
    static func fetch() -> [CoreDataActivity] {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate?.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivity.IDENTIFIER)
        
        do {
            let results = try managedContext?.fetch(fetchRequest)
            if let readings = results as? [CoreDataActivity] {
                return readings
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return [CoreDataActivity]()
    }
    
    static func delete(for date: Date) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataActivity.IDENTIFIER)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedContext!.fetch(fetchRequest)
            for managedObject in results {
                let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
                managedContext!.delete(managedObjectData)
            }
            try managedContext?.save()
        } catch let error as NSError {
            print("delete all data in \(String(describing: entity)) error : \(error) \(error.userInfo)")
        }
    }
}
