//
//  DocumentManager.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/06/21.
//

import Foundation
import UIKit
import Zip

// swiftlint:disable all
class DocumentManager {
	
	var fileManager : FileManager
	
	init(manager: FileManager) {
		self.fileManager = manager
	}
	
	func files(in documentsUrl: URL, skipsHiddenFiles: Bool = true) -> [URL]? {
		let documentsURLArray = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
		let documentsURL = documentsURLArray[0]
		do {
			let fileURLs = try fileManager.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: skipsHiddenFiles ? .skipsHiddenFiles : [])
			return fileURLs
			// process files
		} catch {
			print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
		}
		return nil
	}
	
	func deleteFile(_ file: URL) {
		do {
			try fileManager.removeItem(at: file)
			
		} catch let error as NSError {
			print("Error: \(error)")
		}
	}
	
	func deleteFiles(_ files: [URL]) {
		for each in files {
			deleteFile(each)
		}
	}
	
	// swiftlint:disable line_length
	func addFile(data: Data, fileName: String) -> URL? {
		let fileURL = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
			.appendingPathComponent(fileName + ".zip")

		do {
			try data.write(to: fileURL)
			return fileURL
		} catch {
			print(error)
		}
		return nil
	}
	
	func uploadDataURL(dataType: DataType, fileName: String) -> URL? {
		let url = bluetoothManager.dataDirectoryPath[0].appendingPathComponent(dataType.rawValue)
		if let files = self.files(in: url) {
			if !files.isEmpty {
				let zippedFileUrl = try? Zip.quickZipFiles(files, fileName: fileName) { (progress) in
					if progress == 1.0 {
						self.deleteFiles(files)
					}
				}
				return zippedFileUrl
			}
		}
		return nil
	}
}
