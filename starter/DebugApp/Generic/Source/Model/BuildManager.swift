//
//  BuildManager.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/06/21.
//

import Foundation

class BuildManager {
	
	private static var singleInstance: BuildManager?
	
	private init() {
	}
	
	static var current: BuildManager {
		if singleInstance == nil {
			singleInstance = BuildManager()
		}
		return singleInstance!
	}
	
//	var buildType: BuildType {
//		#if ENV_DEV
//		return .dev
//		#elseif ENV_STAGE
//		return .staging
//		#elseif ENV_PROD
//		return .prod
//		#endif
//	}
}
