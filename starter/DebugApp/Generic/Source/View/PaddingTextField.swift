//
//  PaddingTextField.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 03/08/21.
//

import UIKit
import SkyFloatingLabelTextField

class PaddingTextField: SkyFloatingLabelTextField {
	
	override func textRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.inset(by: UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0))
	}
	
	override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.inset(by: UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0))
	}
	
	override func editingRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.inset(by: UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0))
	}
}
