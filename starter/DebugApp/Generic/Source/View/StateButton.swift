//
//  StateButton.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/11/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import  UIKit

class StateButton: MovanoButton {
	
	private var interactionEnabled = false
	
	var isInteractionEnabled : Bool {
		get {
			return interactionEnabled
		}
		set {
			self.interactionEnabled = newValue
			manageUI()
		}
	}
	
	private func manageUI() {
		self.layer.cornerRadius = 4.0
		self.setTitleColor(UIColor.white, for: .normal)
		self.isUserInteractionEnabled = self.interactionEnabled
		if interactionEnabled {
			self.alpha = 1.0
		} else {
			self.alpha = 0.5
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		manageUI()
	}
}
