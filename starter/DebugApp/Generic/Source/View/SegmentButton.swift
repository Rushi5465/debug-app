//
//  SegmentButton.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 04/12/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import  UIKit

@IBDesignable
class SegmentButton: MovanoButton {
	
	private var tabSelect = false
	private var _deselectedSegmentBackgroundColor = UIColor()
	
	@IBInspectable
	public var deselectedSegmentBackgroundColor: UIColor = .clear {
		didSet {
			self._deselectedSegmentBackgroundColor = self.deselectedSegmentBackgroundColor
		}
	}
	
	var isTabSelected : Bool {
		get {
			return tabSelect
		}
		set {
			self.tabSelect = newValue
			manageUI()
		}
	}
	
	private func manageUI() {
		if tabSelect {
			self.setTitleColor(UIColor.white, for: .normal)
			self.backgroundColor = UIColor.primaryColor
			self.layer.borderColor = UIColor.primaryColor.cgColor
			self.layer.borderWidth = 0.5
			self.tintColor = UIColor.white
		} else {
			setTitleColor(UIColor.valueTitleColor, for: .normal)
			self.backgroundColor = self._deselectedSegmentBackgroundColor
			self.layer.borderColor = UIColor.primaryColor.cgColor
			self.layer.borderWidth = 0.5
			self.tintColor = UIColor.valueTitleColor
		}
	}
}
