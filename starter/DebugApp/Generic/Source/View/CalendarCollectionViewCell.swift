//
//  CalendarCollectionViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 10/08/21.
//

import UIKit

class CalendarCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var background: UIView!
	@IBOutlet weak var titleLabel: MovanoLabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
