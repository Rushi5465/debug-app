//
//  HeaderView.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/08/21.
//

import UIKit
import HGCircularSlider

class HeaderView: UIView {

	@IBOutlet weak var movanoLogo: UIImageView!
	@IBOutlet var contentView: UIView!
	@IBOutlet weak var circularSlider: CircularSlider!
	@IBOutlet weak var settingButton: MovanoButton!
	@IBOutlet weak var calendarView: UIView!
	@IBOutlet weak var currentDateLabel: MovanoLabel!
	@IBOutlet weak var backButton: MovanoButton!
	@IBOutlet weak var leftArrow: MovanoButton!
	@IBOutlet weak var rightArrow: MovanoButton!
	@IBOutlet weak var calendarImageView: UIImageView!
	
	//Charging IBOutlets
	@IBOutlet weak var movanoChargingLogo: UIImageView!
	@IBOutlet weak var chargePercentLabel: MovanoLabel!
	
	//IBOutlet constraints
	
	@IBOutlet weak var calendarImgViewWidthConstraint: NSLayoutConstraint!
	@IBOutlet weak var dateLabelLeadingConstraint: NSLayoutConstraint!
	
	var currentController: UIViewController?
	var selectedDate: DateRange! {
		didSet {
			self.currentDateLabel.text = self.selectedDate.dateRangeString
			self.rightArrow.interaction(enabled: self.selectedDate.isNextAllowed)
			self.calendarView.isUserInteractionEnabled = (selectedDate.type == .weekly) ? false: true
		}
	}
	
	var isInNavigation: Bool = false {
		didSet {
			self.circularSlider.isHidden = isInNavigation
			self.settingButton.isHidden = isInNavigation
			self.backButton.isHidden = !isInNavigation
		}
	}
	
	var updateDate: Bool = true
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		commonInit()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		
		commonInit()
	}
	
	func commonInit() {
		let viewFromXib = Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)![0] as! UIView
		viewFromXib.frame = self.bounds
		addSubview(viewFromXib)
		
		self.calendarView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(calendarTapped(tap:))))
		self.calendarView.isUserInteractionEnabled = true
		
		self.selectedDate = DateManager.current.selectedDate
		self.circularSlider.endPointValue = 0.0
		self.movanoLogo.image = #imageLiteral(resourceName: "batteryIcon-deselect")
	}
	
	@objc func calendarTapped(tap: UITapGestureRecognizer) {
		if selectedDate.type == .daily {
			let slideVC = CalendarView()
			slideVC.modalPresentationStyle = .custom
			slideVC.rangeType = self.selectedDate.type
			
			slideVC.selectedDate = self.selectedDate.start
			
			slideVC.delegate = self
			slideVC.transitioningDelegate = currentController
			self.currentController?.pushViewControllerFromTop(viewController: slideVC)
		}
	}
	
	override func prepareForInterfaceBuilder() {
		commonInit()
	}
	
	@IBAction func settingButtonClicked(_ sender: Any) {
//		let controller = SettingViewController.instantiate(fromAppStoryboard: .profile)
//		self.currentController?.navigationController?.pushViewController(controller, animated: true)
	}
	
	@IBAction func sliderClicked(_ sender: Any) {
		if UserDefault.shared.connectedDevice == nil {
			let controller = DeviceListViewController.instantiate(fromAppStoryboard: .bluetooth)
			controller.fromOnboarding = false
			self.currentController?.navigationController?.pushViewController(controller, animated: true)
		} else {
			let controller = MyDeviceViewController.instantiate(fromAppStoryboard: .bluetooth)
			self.currentController?.navigationController?.pushViewController(controller, animated: true)
		}
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		
//		if ((self.currentController as? SleepHistoryDataViewController) != nil) {
//			NotificationCenter.default.post(name: NSNotification.Name("backBtntapped"), object: self.backButton)
//		} else {
//			self.currentController?.navigationController?.popViewController(animated: true)
//		}
		self.currentController?.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func leftArrowClicked(_ sender: Any) {
		self.selectedDate.selectPrevious()
		if updateDate {
			DateManager.current.selectedDate = selectedDate
		}
		NotificationCenter.default.post(name: NSNotification.Name("calendarDateSelected"), object: self.selectedDate)
	}
	
	@IBAction func rightArrowClicked(_ sender: Any) {
		self.selectedDate.selectNext()
		if updateDate {
			DateManager.current.selectedDate = selectedDate
		}
		NotificationCenter.default.post(name: NSNotification.Name("calendarDateSelected"), object: self.selectedDate)
	}
}

// MARK: - CalendarDelegate
extension HeaderView: CalendarDelegate {
	
	func calendar(_ selectedDate: Date) {
		self.selectedDate = DateRange(selectedDate, type: self.selectedDate.type)
		if updateDate {
			DateManager.current.selectedDate = self.selectedDate
		}
		NotificationCenter.default.post(name: NSNotification.Name("calendarDateSelected"), object: self.selectedDate)
	}
}
