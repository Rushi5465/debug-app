//
//  CollectionTableViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 27/08/21.
//

import UIKit

class CollectionTableViewCell: UITableViewCell {

	@IBOutlet weak var collectionView: UICollectionView!
	
	var dataList = [Any]()
	
}
