// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

// swiftlint:disable line_length
// swiftlint:disable all
public struct CreateSkinTemperatureInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - temperatureTimestamp
  ///   - temperatureValue
  public init(temperatureTimestamp: Double, temperatureValue: Double) {
    graphQLMap = ["temperature_timestamp": temperatureTimestamp, "temperature_value": temperatureValue]
  }

  public var temperatureTimestamp: Double {
    get {
      return graphQLMap["temperature_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "temperature_timestamp")
    }
  }

  public var temperatureValue: Double {
    get {
      return graphQLMap["temperature_value"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "temperature_value")
    }
  }
}

public struct UpdateSkinTemperatureInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - temperatureTimestamp
  ///   - temperatureValue
  public init(temperatureTimestamp: Double, temperatureValue: Swift.Optional<Double?> = nil) {
    graphQLMap = ["temperature_timestamp": temperatureTimestamp, "temperature_value": temperatureValue]
  }

  public var temperatureTimestamp: Double {
    get {
      return graphQLMap["temperature_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "temperature_timestamp")
    }
  }

  public var temperatureValue: Swift.Optional<Double?> {
    get {
      return graphQLMap["temperature_value"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "temperature_value")
    }
  }
}

public struct DeleteSkinTemperatureInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - temperatureTimestamp
  public init(temperatureTimestamp: Double) {
    graphQLMap = ["temperature_timestamp": temperatureTimestamp]
  }

  public var temperatureTimestamp: Double {
    get {
      return graphQLMap["temperature_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "temperature_timestamp")
    }
  }
}

public struct CreateStepCountInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - stepCountTimestamp
  ///   - stepCountValue
  public init(stepCountTimestamp: Double, stepCountValue: Int) {
    graphQLMap = ["step_count_timestamp": stepCountTimestamp, "step_count_value": stepCountValue]
  }

  public var stepCountTimestamp: Double {
    get {
      return graphQLMap["step_count_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "step_count_timestamp")
    }
  }

  public var stepCountValue: Int {
    get {
      return graphQLMap["step_count_value"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "step_count_value")
    }
  }
}

public struct UpdateStepCountInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - stepCountTimestamp
  ///   - stepCountValue
  ///   - caloriesBurnt
  ///   - distanceCovered
  public init(stepCountTimestamp: Double, stepCountValue: Swift.Optional<Int?> = nil, caloriesBurnt: Swift.Optional<Double?> = nil, distanceCovered: Swift.Optional<Double?> = nil) {
    graphQLMap = ["step_count_timestamp": stepCountTimestamp, "step_count_value": stepCountValue, "calories_burnt": caloriesBurnt, "distance_covered": distanceCovered]
  }

  public var stepCountTimestamp: Double {
    get {
      return graphQLMap["step_count_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "step_count_timestamp")
    }
  }

  public var stepCountValue: Swift.Optional<Int?> {
    get {
      return graphQLMap["step_count_value"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "step_count_value")
    }
  }

  public var caloriesBurnt: Swift.Optional<Double?> {
    get {
      return graphQLMap["calories_burnt"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "calories_burnt")
    }
  }

  public var distanceCovered: Swift.Optional<Double?> {
    get {
      return graphQLMap["distance_covered"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "distance_covered")
    }
  }
}

public struct DeleteStepCountInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - stepCountTimestamp
  public init(stepCountTimestamp: Double) {
    graphQLMap = ["step_count_timestamp": stepCountTimestamp]
  }

  public var stepCountTimestamp: Double {
    get {
      return graphQLMap["step_count_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "step_count_timestamp")
    }
  }
}

public struct CreatePulseRateInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - pulseRateTimestamp
  ///   - pulseRateValue
  public init(pulseRateTimestamp: Double, pulseRateValue: Int) {
    graphQLMap = ["pulse_rate_timestamp": pulseRateTimestamp, "pulse_rate_value": pulseRateValue]
  }

  public var pulseRateTimestamp: Double {
    get {
      return graphQLMap["pulse_rate_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pulse_rate_timestamp")
    }
  }

  public var pulseRateValue: Int {
    get {
      return graphQLMap["pulse_rate_value"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pulse_rate_value")
    }
  }
}

public struct UpdatePulseRateInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - pulseRateTimestamp
  ///   - pulseRateValue
  public init(pulseRateTimestamp: Double, pulseRateValue: Swift.Optional<Int?> = nil) {
    graphQLMap = ["pulse_rate_timestamp": pulseRateTimestamp, "pulse_rate_value": pulseRateValue]
  }

  public var pulseRateTimestamp: Double {
    get {
      return graphQLMap["pulse_rate_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pulse_rate_timestamp")
    }
  }

  public var pulseRateValue: Swift.Optional<Int?> {
    get {
      return graphQLMap["pulse_rate_value"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pulse_rate_value")
    }
  }
}

public struct DeletePulseRateInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - pulseRateTimestamp
  public init(pulseRateTimestamp: Double) {
    graphQLMap = ["pulse_rate_timestamp": pulseRateTimestamp]
  }

  public var pulseRateTimestamp: Double {
    get {
      return graphQLMap["pulse_rate_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pulse_rate_timestamp")
    }
  }
}

public struct CreateExerciseDataInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - startTime
  ///   - endTime
  ///   - averagePulseRate
  ///   - totalStepCount
  ///   - totalCaloriesBurnt
  ///   - totalDistanceCovered
  ///   - averageSkinTemprature
  ///   - averageSystolic
  ///   - averageDiastolic
  ///   - averageBreathingRate
  ///   - averageSpo2
  public init(startTime: Double, endTime: Double, averagePulseRate: Swift.Optional<Double?> = nil, totalStepCount: Swift.Optional<Double?> = nil, totalCaloriesBurnt: Swift.Optional<Double?> = nil, totalDistanceCovered: Swift.Optional<Double?> = nil, averageSkinTemprature: Swift.Optional<Double?> = nil, averageSystolic: Swift.Optional<Double?> = nil, averageDiastolic: Swift.Optional<Double?> = nil, averageBreathingRate: Swift.Optional<Double?> = nil, averageSpo2: Swift.Optional<Double?> = nil) {
    graphQLMap = ["start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "total_step_count": totalStepCount, "total_calories_burnt": totalCaloriesBurnt, "total_distance_covered": totalDistanceCovered, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate, "average_spo2": averageSpo2]
  }

  public var startTime: Double {
    get {
      return graphQLMap["start_time"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "start_time")
    }
  }

  public var endTime: Double {
    get {
      return graphQLMap["end_time"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "end_time")
    }
  }

  public var averagePulseRate: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_pulse_rate"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_pulse_rate")
    }
  }

  public var totalStepCount: Swift.Optional<Double?> {
    get {
      return graphQLMap["total_step_count"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "total_step_count")
    }
  }

  public var totalCaloriesBurnt: Swift.Optional<Double?> {
    get {
      return graphQLMap["total_calories_burnt"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "total_calories_burnt")
    }
  }

  public var totalDistanceCovered: Swift.Optional<Double?> {
    get {
      return graphQLMap["total_distance_covered"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "total_distance_covered")
    }
  }

  public var averageSkinTemprature: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_skin_temprature"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_skin_temprature")
    }
  }

  public var averageSystolic: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_systolic"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_systolic")
    }
  }

  public var averageDiastolic: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_diastolic"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_diastolic")
    }
  }

  public var averageBreathingRate: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_breathing_rate"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_breathing_rate")
    }
  }

  public var averageSpo2: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_spo2"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_spo2")
    }
  }
}

public struct UpdateExerciseDataInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - id
  ///   - startTime
  ///   - endTime
  ///   - averagePulseRate
  ///   - totalStepCount
  ///   - totalCaloriesBurnt
  ///   - totalDistanceCovered
  ///   - averageSkinTemprature
  ///   - averageSystolic
  ///   - averageDiastolic
  ///   - averageBreathingRate
  ///   - averageSpo2
  public init(id: GraphQLID, startTime: Swift.Optional<Double?> = nil, endTime: Swift.Optional<Double?> = nil, averagePulseRate: Swift.Optional<Double?> = nil, totalStepCount: Swift.Optional<Double?> = nil, totalCaloriesBurnt: Swift.Optional<Double?> = nil, totalDistanceCovered: Swift.Optional<Double?> = nil, averageSkinTemprature: Swift.Optional<Double?> = nil, averageSystolic: Swift.Optional<Double?> = nil, averageDiastolic: Swift.Optional<Double?> = nil, averageBreathingRate: Swift.Optional<Double?> = nil, averageSpo2: Swift.Optional<Double?> = nil) {
    graphQLMap = ["id": id, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "total_step_count": totalStepCount, "total_calories_burnt": totalCaloriesBurnt, "total_distance_covered": totalDistanceCovered, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate, "average_spo2": averageSpo2]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var startTime: Swift.Optional<Double?> {
    get {
      return graphQLMap["start_time"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "start_time")
    }
  }

  public var endTime: Swift.Optional<Double?> {
    get {
      return graphQLMap["end_time"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "end_time")
    }
  }

  public var averagePulseRate: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_pulse_rate"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_pulse_rate")
    }
  }

  public var totalStepCount: Swift.Optional<Double?> {
    get {
      return graphQLMap["total_step_count"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "total_step_count")
    }
  }

  public var totalCaloriesBurnt: Swift.Optional<Double?> {
    get {
      return graphQLMap["total_calories_burnt"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "total_calories_burnt")
    }
  }

  public var totalDistanceCovered: Swift.Optional<Double?> {
    get {
      return graphQLMap["total_distance_covered"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "total_distance_covered")
    }
  }

  public var averageSkinTemprature: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_skin_temprature"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_skin_temprature")
    }
  }

  public var averageSystolic: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_systolic"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_systolic")
    }
  }

  public var averageDiastolic: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_diastolic"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_diastolic")
    }
  }

  public var averageBreathingRate: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_breathing_rate"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_breathing_rate")
    }
  }

  public var averageSpo2: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_spo2"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_spo2")
    }
  }
}

public struct DeleteExerciseDataInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - id
  public init(id: GraphQLID) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct TableExerciseDataFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - id
  ///   - userId
  ///   - startTime
  ///   - endTime
  ///   - averagePulseRate
  ///   - totalStepCount
  ///   - averageSkinTemprature
  ///   - averageSystolic
  ///   - averageDiastolic
  ///   - averageBreathingRate
  ///   - averageSpo2
  public init(id: Swift.Optional<TableIDFilterInput?> = nil, userId: Swift.Optional<TableStringFilterInput?> = nil, startTime: Swift.Optional<TableFloatFilterInput?> = nil, endTime: Swift.Optional<TableFloatFilterInput?> = nil, averagePulseRate: Swift.Optional<TableFloatFilterInput?> = nil, totalStepCount: Swift.Optional<TableFloatFilterInput?> = nil, averageSkinTemprature: Swift.Optional<TableFloatFilterInput?> = nil, averageSystolic: Swift.Optional<TableFloatFilterInput?> = nil, averageDiastolic: Swift.Optional<TableFloatFilterInput?> = nil, averageBreathingRate: Swift.Optional<TableFloatFilterInput?> = nil, averageSpo2: Swift.Optional<TableFloatFilterInput?> = nil) {
    graphQLMap = ["id": id, "user_id": userId, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "total_step_count": totalStepCount, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate, "average_spo2": averageSpo2]
  }

  public var id: Swift.Optional<TableIDFilterInput?> {
    get {
      return graphQLMap["id"] as? Swift.Optional<TableIDFilterInput?> ?? Swift.Optional<TableIDFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var userId: Swift.Optional<TableStringFilterInput?> {
    get {
      return graphQLMap["user_id"] as? Swift.Optional<TableStringFilterInput?> ?? Swift.Optional<TableStringFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "user_id")
    }
  }

  public var startTime: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["start_time"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "start_time")
    }
  }

  public var endTime: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["end_time"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "end_time")
    }
  }

  public var averagePulseRate: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_pulse_rate"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_pulse_rate")
    }
  }

  public var totalStepCount: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["total_step_count"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "total_step_count")
    }
  }

  public var averageSkinTemprature: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_skin_temprature"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_skin_temprature")
    }
  }

  public var averageSystolic: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_systolic"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_systolic")
    }
  }

  public var averageDiastolic: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_diastolic"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_diastolic")
    }
  }

  public var averageBreathingRate: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_breathing_rate"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_breathing_rate")
    }
  }

  public var averageSpo2: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_spo2"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_spo2")
    }
  }
}

public struct TableIDFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - ne
  ///   - eq
  ///   - le
  ///   - lt
  ///   - ge
  ///   - gt
  ///   - contains
  ///   - notContains
  ///   - between
  ///   - beginsWith
  public init(ne: Swift.Optional<GraphQLID?> = nil, eq: Swift.Optional<GraphQLID?> = nil, le: Swift.Optional<GraphQLID?> = nil, lt: Swift.Optional<GraphQLID?> = nil, ge: Swift.Optional<GraphQLID?> = nil, gt: Swift.Optional<GraphQLID?> = nil, contains: Swift.Optional<GraphQLID?> = nil, notContains: Swift.Optional<GraphQLID?> = nil, between: Swift.Optional<[GraphQLID?]?> = nil, beginsWith: Swift.Optional<GraphQLID?> = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between, "beginsWith": beginsWith]
  }

  public var ne: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["ne"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["eq"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["le"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["lt"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["ge"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["gt"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["contains"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["notContains"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: Swift.Optional<[GraphQLID?]?> {
    get {
      return graphQLMap["between"] as? Swift.Optional<[GraphQLID?]?> ?? Swift.Optional<[GraphQLID?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }

  public var beginsWith: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["beginsWith"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beginsWith")
    }
  }
}

public struct TableStringFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - ne
  ///   - eq
  ///   - le
  ///   - lt
  ///   - ge
  ///   - gt
  ///   - contains
  ///   - notContains
  ///   - between
  ///   - beginsWith
  public init(ne: Swift.Optional<String?> = nil, eq: Swift.Optional<String?> = nil, le: Swift.Optional<String?> = nil, lt: Swift.Optional<String?> = nil, ge: Swift.Optional<String?> = nil, gt: Swift.Optional<String?> = nil, contains: Swift.Optional<String?> = nil, notContains: Swift.Optional<String?> = nil, between: Swift.Optional<[String?]?> = nil, beginsWith: Swift.Optional<String?> = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between, "beginsWith": beginsWith]
  }

  public var ne: Swift.Optional<String?> {
    get {
      return graphQLMap["ne"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: Swift.Optional<String?> {
    get {
      return graphQLMap["eq"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: Swift.Optional<String?> {
    get {
      return graphQLMap["le"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: Swift.Optional<String?> {
    get {
      return graphQLMap["lt"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: Swift.Optional<String?> {
    get {
      return graphQLMap["ge"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: Swift.Optional<String?> {
    get {
      return graphQLMap["gt"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: Swift.Optional<String?> {
    get {
      return graphQLMap["contains"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: Swift.Optional<String?> {
    get {
      return graphQLMap["notContains"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: Swift.Optional<[String?]?> {
    get {
      return graphQLMap["between"] as? Swift.Optional<[String?]?> ?? Swift.Optional<[String?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }

  public var beginsWith: Swift.Optional<String?> {
    get {
      return graphQLMap["beginsWith"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beginsWith")
    }
  }
}

public struct TableFloatFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - ne
  ///   - eq
  ///   - le
  ///   - lt
  ///   - ge
  ///   - gt
  ///   - contains
  ///   - notContains
  ///   - between
  public init(ne: Swift.Optional<Double?> = nil, eq: Swift.Optional<Double?> = nil, le: Swift.Optional<Double?> = nil, lt: Swift.Optional<Double?> = nil, ge: Swift.Optional<Double?> = nil, gt: Swift.Optional<Double?> = nil, contains: Swift.Optional<Double?> = nil, notContains: Swift.Optional<Double?> = nil, between: Swift.Optional<[Double?]?> = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between]
  }

  public var ne: Swift.Optional<Double?> {
    get {
      return graphQLMap["ne"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: Swift.Optional<Double?> {
    get {
      return graphQLMap["eq"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: Swift.Optional<Double?> {
    get {
      return graphQLMap["le"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: Swift.Optional<Double?> {
    get {
      return graphQLMap["lt"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: Swift.Optional<Double?> {
    get {
      return graphQLMap["ge"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: Swift.Optional<Double?> {
    get {
      return graphQLMap["gt"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: Swift.Optional<Double?> {
    get {
      return graphQLMap["contains"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: Swift.Optional<Double?> {
    get {
      return graphQLMap["notContains"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: Swift.Optional<[Double?]?> {
    get {
      return graphQLMap["between"] as? Swift.Optional<[Double?]?> ?? Swift.Optional<[Double?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }
}

public struct CreateBreathingRateInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - breathingRateTimestamp
  ///   - breathingRateValue
  public init(breathingRateTimestamp: Double, breathingRateValue: Double) {
    graphQLMap = ["breathing_rate_timestamp": breathingRateTimestamp, "breathing_rate_value": breathingRateValue]
  }

  public var breathingRateTimestamp: Double {
    get {
      return graphQLMap["breathing_rate_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "breathing_rate_timestamp")
    }
  }

  public var breathingRateValue: Double {
    get {
      return graphQLMap["breathing_rate_value"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "breathing_rate_value")
    }
  }
}

public struct UpdateBreathingRateInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - breathingRateTimestamp
  ///   - breathingRateValue
  public init(breathingRateTimestamp: Double, breathingRateValue: Swift.Optional<Double?> = nil) {
    graphQLMap = ["breathing_rate_timestamp": breathingRateTimestamp, "breathing_rate_value": breathingRateValue]
  }

  public var breathingRateTimestamp: Double {
    get {
      return graphQLMap["breathing_rate_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "breathing_rate_timestamp")
    }
  }

  public var breathingRateValue: Swift.Optional<Double?> {
    get {
      return graphQLMap["breathing_rate_value"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "breathing_rate_value")
    }
  }
}

public struct DeleteBreathingRateInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - breathingRateTimestamp
  public init(breathingRateTimestamp: Double) {
    graphQLMap = ["breathing_rate_timestamp": breathingRateTimestamp]
  }

  public var breathingRateTimestamp: Double {
    get {
      return graphQLMap["breathing_rate_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "breathing_rate_timestamp")
    }
  }
}

public struct CreatePauseDataInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - startTime
  ///   - endTime
  ///   - averagePulseRate
  ///   - averageSkinTemprature
  ///   - averageSystolic
  ///   - averageDiastolic
  ///   - averageBreathingRate
  public init(startTime: Double, endTime: Double, averagePulseRate: Swift.Optional<Double?> = nil, averageSkinTemprature: Swift.Optional<Double?> = nil, averageSystolic: Swift.Optional<Double?> = nil, averageDiastolic: Swift.Optional<Double?> = nil, averageBreathingRate: Swift.Optional<Double?> = nil) {
    graphQLMap = ["start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate]
  }

  public var startTime: Double {
    get {
      return graphQLMap["start_time"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "start_time")
    }
  }

  public var endTime: Double {
    get {
      return graphQLMap["end_time"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "end_time")
    }
  }

  public var averagePulseRate: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_pulse_rate"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_pulse_rate")
    }
  }

  public var averageSkinTemprature: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_skin_temprature"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_skin_temprature")
    }
  }

  public var averageSystolic: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_systolic"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_systolic")
    }
  }

  public var averageDiastolic: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_diastolic"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_diastolic")
    }
  }

  public var averageBreathingRate: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_breathing_rate"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_breathing_rate")
    }
  }
}

public struct UpdatePauseDataInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - id
  ///   - startTime
  ///   - endTime
  ///   - averagePulseRate
  ///   - averageSkinTemprature
  ///   - averageSystolic
  ///   - averageDiastolic
  ///   - averageBreathingRate
  public init(id: GraphQLID, startTime: Swift.Optional<Double?> = nil, endTime: Swift.Optional<Double?> = nil, averagePulseRate: Swift.Optional<Double?> = nil, averageSkinTemprature: Swift.Optional<Double?> = nil, averageSystolic: Swift.Optional<Double?> = nil, averageDiastolic: Swift.Optional<Double?> = nil, averageBreathingRate: Swift.Optional<Double?> = nil) {
    graphQLMap = ["id": id, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var startTime: Swift.Optional<Double?> {
    get {
      return graphQLMap["start_time"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "start_time")
    }
  }

  public var endTime: Swift.Optional<Double?> {
    get {
      return graphQLMap["end_time"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "end_time")
    }
  }

  public var averagePulseRate: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_pulse_rate"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_pulse_rate")
    }
  }

  public var averageSkinTemprature: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_skin_temprature"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_skin_temprature")
    }
  }

  public var averageSystolic: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_systolic"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_systolic")
    }
  }

  public var averageDiastolic: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_diastolic"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_diastolic")
    }
  }

  public var averageBreathingRate: Swift.Optional<Double?> {
    get {
      return graphQLMap["average_breathing_rate"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_breathing_rate")
    }
  }
}

public struct DeletePauseDataInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - id
  public init(id: GraphQLID) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct TablePauseDataFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - id
  ///   - userId
  ///   - startTime
  ///   - endTime
  ///   - averagePulseRate
  ///   - averageSkinTemprature
  ///   - averageSystolic
  ///   - averageDiastolic
  ///   - averageBreathingRate
  public init(id: Swift.Optional<TableIDFilterInput?> = nil, userId: Swift.Optional<TableStringFilterInput?> = nil, startTime: Swift.Optional<TableFloatFilterInput?> = nil, endTime: Swift.Optional<TableFloatFilterInput?> = nil, averagePulseRate: Swift.Optional<TableFloatFilterInput?> = nil, averageSkinTemprature: Swift.Optional<TableFloatFilterInput?> = nil, averageSystolic: Swift.Optional<TableFloatFilterInput?> = nil, averageDiastolic: Swift.Optional<TableFloatFilterInput?> = nil, averageBreathingRate: Swift.Optional<TableFloatFilterInput?> = nil) {
    graphQLMap = ["id": id, "user_id": userId, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate]
  }

  public var id: Swift.Optional<TableIDFilterInput?> {
    get {
      return graphQLMap["id"] as? Swift.Optional<TableIDFilterInput?> ?? Swift.Optional<TableIDFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var userId: Swift.Optional<TableStringFilterInput?> {
    get {
      return graphQLMap["user_id"] as? Swift.Optional<TableStringFilterInput?> ?? Swift.Optional<TableStringFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "user_id")
    }
  }

  public var startTime: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["start_time"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "start_time")
    }
  }

  public var endTime: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["end_time"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "end_time")
    }
  }

  public var averagePulseRate: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_pulse_rate"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_pulse_rate")
    }
  }

  public var averageSkinTemprature: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_skin_temprature"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_skin_temprature")
    }
  }

  public var averageSystolic: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_systolic"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_systolic")
    }
  }

  public var averageDiastolic: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_diastolic"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_diastolic")
    }
  }

  public var averageBreathingRate: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_breathing_rate"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_breathing_rate")
    }
  }
}

public struct CreateUserGoalsInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - stepCount
  ///   - caloriesBurned
  ///   - systolicLow
  ///   - systolicHigh
  ///   - diastolicLow
  ///   - diastolicHigh
  ///   - temperatureLow
  ///   - temperatureHigh
  ///   - breathingRateLow
  ///   - breathingRateHigh
  ///   - pulseRateLow
  ///   - pulseRateHigh
  ///   - spO2Low
  ///   - spO2High
  ///   - sleepHrsLow
  ///   - sleepHrsHigh
  ///   - hrvLow
  ///   - hrvHigh
  public init(stepCount: Swift.Optional<Int?> = nil, caloriesBurned: Swift.Optional<Int?> = nil, systolicLow: Swift.Optional<Int?> = nil, systolicHigh: Swift.Optional<Int?> = nil, diastolicLow: Swift.Optional<Int?> = nil, diastolicHigh: Swift.Optional<Int?> = nil, temperatureLow: Swift.Optional<Int?> = nil, temperatureHigh: Swift.Optional<Int?> = nil, breathingRateLow: Swift.Optional<Int?> = nil, breathingRateHigh: Swift.Optional<Int?> = nil, pulseRateLow: Swift.Optional<Int?> = nil, pulseRateHigh: Swift.Optional<Int?> = nil, spO2Low: Swift.Optional<Int?> = nil, spO2High: Swift.Optional<Int?> = nil, sleepHrsLow: Swift.Optional<Int?> = nil, sleepHrsHigh: Swift.Optional<Int?> = nil, hrvLow: Swift.Optional<Int?> = nil, hrvHigh: Swift.Optional<Int?> = nil) {
    graphQLMap = ["step_count": stepCount, "calories_burned": caloriesBurned, "systolic_low": systolicLow, "systolic_high": systolicHigh, "diastolic_low": diastolicLow, "diastolic_high": diastolicHigh, "temperature_low": temperatureLow, "temperature_high": temperatureHigh, "breathing_rate_low": breathingRateLow, "breathing_rate_high": breathingRateHigh, "pulse_rate_low": pulseRateLow, "pulse_rate_high": pulseRateHigh, "sp_o2_low": spO2Low, "sp_o2_high": spO2High, "sleep_hrs_low": sleepHrsLow, "sleep_hrs_high": sleepHrsHigh, "hrv_low": hrvLow, "hrv_high": hrvHigh]
  }

  public var stepCount: Swift.Optional<Int?> {
    get {
      return graphQLMap["step_count"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "step_count")
    }
  }

  public var caloriesBurned: Swift.Optional<Int?> {
    get {
      return graphQLMap["calories_burned"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "calories_burned")
    }
  }

  public var systolicLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["systolic_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "systolic_low")
    }
  }

  public var systolicHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["systolic_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "systolic_high")
    }
  }

  public var diastolicLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["diastolic_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "diastolic_low")
    }
  }

  public var diastolicHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["diastolic_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "diastolic_high")
    }
  }

  public var temperatureLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["temperature_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "temperature_low")
    }
  }

  public var temperatureHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["temperature_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "temperature_high")
    }
  }

  public var breathingRateLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["breathing_rate_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "breathing_rate_low")
    }
  }

  public var breathingRateHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["breathing_rate_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "breathing_rate_high")
    }
  }

  public var pulseRateLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["pulse_rate_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pulse_rate_low")
    }
  }

  public var pulseRateHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["pulse_rate_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pulse_rate_high")
    }
  }

  public var spO2Low: Swift.Optional<Int?> {
    get {
      return graphQLMap["sp_o2_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sp_o2_low")
    }
  }

  public var spO2High: Swift.Optional<Int?> {
    get {
      return graphQLMap["sp_o2_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sp_o2_high")
    }
  }

  public var sleepHrsLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["sleep_hrs_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sleep_hrs_low")
    }
  }

  public var sleepHrsHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["sleep_hrs_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sleep_hrs_high")
    }
  }

  public var hrvLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["hrv_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "hrv_low")
    }
  }

  public var hrvHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["hrv_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "hrv_high")
    }
  }
}

public struct UpdateUserGoalsInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - stepCount
  ///   - caloriesBurned
  ///   - systolicLow
  ///   - systolicHigh
  ///   - diastolicLow
  ///   - diastolicHigh
  ///   - temperatureLow
  ///   - temperatureHigh
  ///   - breathingRateLow
  ///   - breathingRateHigh
  ///   - pulseRateLow
  ///   - pulseRateHigh
  ///   - spO2Low
  ///   - spO2High
  ///   - sleepHrsLow
  ///   - sleepHrsHigh
  ///   - hrvLow
  ///   - hrvHigh
  public init(stepCount: Swift.Optional<Int?> = nil, caloriesBurned: Swift.Optional<Int?> = nil, systolicLow: Swift.Optional<Int?> = nil, systolicHigh: Swift.Optional<Int?> = nil, diastolicLow: Swift.Optional<Int?> = nil, diastolicHigh: Swift.Optional<Int?> = nil, temperatureLow: Swift.Optional<Int?> = nil, temperatureHigh: Swift.Optional<Int?> = nil, breathingRateLow: Swift.Optional<Int?> = nil, breathingRateHigh: Swift.Optional<Int?> = nil, pulseRateLow: Swift.Optional<Int?> = nil, pulseRateHigh: Swift.Optional<Int?> = nil, spO2Low: Swift.Optional<Int?> = nil, spO2High: Swift.Optional<Int?> = nil, sleepHrsLow: Swift.Optional<Int?> = nil, sleepHrsHigh: Swift.Optional<Int?> = nil, hrvLow: Swift.Optional<Int?> = nil, hrvHigh: Swift.Optional<Int?> = nil) {
    graphQLMap = ["step_count": stepCount, "calories_burned": caloriesBurned, "systolic_low": systolicLow, "systolic_high": systolicHigh, "diastolic_low": diastolicLow, "diastolic_high": diastolicHigh, "temperature_low": temperatureLow, "temperature_high": temperatureHigh, "breathing_rate_low": breathingRateLow, "breathing_rate_high": breathingRateHigh, "pulse_rate_low": pulseRateLow, "pulse_rate_high": pulseRateHigh, "sp_o2_low": spO2Low, "sp_o2_high": spO2High, "sleep_hrs_low": sleepHrsLow, "sleep_hrs_high": sleepHrsHigh, "hrv_low": hrvLow, "hrv_high": hrvHigh]
  }

  public var stepCount: Swift.Optional<Int?> {
    get {
      return graphQLMap["step_count"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "step_count")
    }
  }

  public var caloriesBurned: Swift.Optional<Int?> {
    get {
      return graphQLMap["calories_burned"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "calories_burned")
    }
  }

  public var systolicLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["systolic_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "systolic_low")
    }
  }

  public var systolicHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["systolic_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "systolic_high")
    }
  }

  public var diastolicLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["diastolic_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "diastolic_low")
    }
  }

  public var diastolicHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["diastolic_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "diastolic_high")
    }
  }

  public var temperatureLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["temperature_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "temperature_low")
    }
  }

  public var temperatureHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["temperature_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "temperature_high")
    }
  }

  public var breathingRateLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["breathing_rate_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "breathing_rate_low")
    }
  }

  public var breathingRateHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["breathing_rate_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "breathing_rate_high")
    }
  }

  public var pulseRateLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["pulse_rate_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pulse_rate_low")
    }
  }

  public var pulseRateHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["pulse_rate_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pulse_rate_high")
    }
  }

  public var spO2Low: Swift.Optional<Int?> {
    get {
      return graphQLMap["sp_o2_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sp_o2_low")
    }
  }

  public var spO2High: Swift.Optional<Int?> {
    get {
      return graphQLMap["sp_o2_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sp_o2_high")
    }
  }

  public var sleepHrsLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["sleep_hrs_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sleep_hrs_low")
    }
  }

  public var sleepHrsHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["sleep_hrs_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sleep_hrs_high")
    }
  }

  public var hrvLow: Swift.Optional<Int?> {
    get {
      return graphQLMap["hrv_low"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "hrv_low")
    }
  }

  public var hrvHigh: Swift.Optional<Int?> {
    get {
      return graphQLMap["hrv_high"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "hrv_high")
    }
  }
}

public struct CreateSpO2Input: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - spO2Timestamp
  ///   - spO2Value
  public init(spO2Timestamp: Double, spO2Value: Int) {
    graphQLMap = ["sp_o2_timestamp": spO2Timestamp, "sp_o2_value": spO2Value]
  }

  public var spO2Timestamp: Double {
    get {
      return graphQLMap["sp_o2_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sp_o2_timestamp")
    }
  }

  public var spO2Value: Int {
    get {
      return graphQLMap["sp_o2_value"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sp_o2_value")
    }
  }
}

public struct UpdateSpO2Input: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - spO2Timestamp
  ///   - spO2Value
  public init(spO2Timestamp: Double, spO2Value: Swift.Optional<Int?> = nil) {
    graphQLMap = ["sp_o2_timestamp": spO2Timestamp, "sp_o2_value": spO2Value]
  }

  public var spO2Timestamp: Double {
    get {
      return graphQLMap["sp_o2_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sp_o2_timestamp")
    }
  }

  public var spO2Value: Swift.Optional<Int?> {
    get {
      return graphQLMap["sp_o2_value"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sp_o2_value")
    }
  }
}

public struct DeleteSpO2Input: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - spO2Timestamp
  public init(spO2Timestamp: Double) {
    graphQLMap = ["sp_o2_timestamp": spO2Timestamp]
  }

  public var spO2Timestamp: Double {
    get {
      return graphQLMap["sp_o2_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sp_o2_timestamp")
    }
  }
}

public struct CreateBloodPressureInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - dataTimestamp
  ///   - diastolic
  ///   - systolic
  ///   - pulseRate
  public init(dataTimestamp: Double, diastolic: Swift.Optional<Int?> = nil, systolic: Swift.Optional<Int?> = nil, pulseRate: Swift.Optional<Int?> = nil) {
    graphQLMap = ["data_timestamp": dataTimestamp, "diastolic": diastolic, "systolic": systolic, "pulse_rate": pulseRate]
  }

  public var dataTimestamp: Double {
    get {
      return graphQLMap["data_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "data_timestamp")
    }
  }

  public var diastolic: Swift.Optional<Int?> {
    get {
      return graphQLMap["diastolic"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "diastolic")
    }
  }

  public var systolic: Swift.Optional<Int?> {
    get {
      return graphQLMap["systolic"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "systolic")
    }
  }

  public var pulseRate: Swift.Optional<Int?> {
    get {
      return graphQLMap["pulse_rate"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pulse_rate")
    }
  }
}

public struct UpdateBloodPressureInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - dataTimestamp
  ///   - diastolic
  ///   - systolic
  ///   - pulseRate
  public init(dataTimestamp: Double, diastolic: Swift.Optional<Int?> = nil, systolic: Swift.Optional<Int?> = nil, pulseRate: Swift.Optional<Int?> = nil) {
    graphQLMap = ["data_timestamp": dataTimestamp, "diastolic": diastolic, "systolic": systolic, "pulse_rate": pulseRate]
  }

  public var dataTimestamp: Double {
    get {
      return graphQLMap["data_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "data_timestamp")
    }
  }

  public var diastolic: Swift.Optional<Int?> {
    get {
      return graphQLMap["diastolic"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "diastolic")
    }
  }

  public var systolic: Swift.Optional<Int?> {
    get {
      return graphQLMap["systolic"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "systolic")
    }
  }

  public var pulseRate: Swift.Optional<Int?> {
    get {
      return graphQLMap["pulse_rate"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pulse_rate")
    }
  }
}

public struct DeleteBloodPressureInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - dataTimestamp
  public init(dataTimestamp: Double) {
    graphQLMap = ["data_timestamp": dataTimestamp]
  }

  public var dataTimestamp: Double {
    get {
      return graphQLMap["data_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "data_timestamp")
    }
  }
}

public struct TableBloodPressureFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - id
  ///   - dataTimestamp
  ///   - diastolic
  ///   - systolic
  ///   - pulseRate
  public init(id: Swift.Optional<TableStringFilterInput?> = nil, dataTimestamp: Swift.Optional<TableFloatFilterInput?> = nil, diastolic: Swift.Optional<TableIntFilterInput?> = nil, systolic: Swift.Optional<TableIntFilterInput?> = nil, pulseRate: Swift.Optional<TableIntFilterInput?> = nil) {
    graphQLMap = ["id": id, "data_timestamp": dataTimestamp, "diastolic": diastolic, "systolic": systolic, "pulse_rate": pulseRate]
  }

  public var id: Swift.Optional<TableStringFilterInput?> {
    get {
      return graphQLMap["id"] as? Swift.Optional<TableStringFilterInput?> ?? Swift.Optional<TableStringFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var dataTimestamp: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["data_timestamp"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "data_timestamp")
    }
  }

  public var diastolic: Swift.Optional<TableIntFilterInput?> {
    get {
      return graphQLMap["diastolic"] as? Swift.Optional<TableIntFilterInput?> ?? Swift.Optional<TableIntFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "diastolic")
    }
  }

  public var systolic: Swift.Optional<TableIntFilterInput?> {
    get {
      return graphQLMap["systolic"] as? Swift.Optional<TableIntFilterInput?> ?? Swift.Optional<TableIntFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "systolic")
    }
  }

  public var pulseRate: Swift.Optional<TableIntFilterInput?> {
    get {
      return graphQLMap["pulse_rate"] as? Swift.Optional<TableIntFilterInput?> ?? Swift.Optional<TableIntFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pulse_rate")
    }
  }
}

public struct TableIntFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - ne
  ///   - eq
  ///   - le
  ///   - lt
  ///   - ge
  ///   - gt
  ///   - contains
  ///   - notContains
  ///   - between
  public init(ne: Swift.Optional<Int?> = nil, eq: Swift.Optional<Int?> = nil, le: Swift.Optional<Int?> = nil, lt: Swift.Optional<Int?> = nil, ge: Swift.Optional<Int?> = nil, gt: Swift.Optional<Int?> = nil, contains: Swift.Optional<Int?> = nil, notContains: Swift.Optional<Int?> = nil, between: Swift.Optional<[Int?]?> = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between]
  }

  public var ne: Swift.Optional<Int?> {
    get {
      return graphQLMap["ne"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: Swift.Optional<Int?> {
    get {
      return graphQLMap["eq"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: Swift.Optional<Int?> {
    get {
      return graphQLMap["le"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: Swift.Optional<Int?> {
    get {
      return graphQLMap["lt"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: Swift.Optional<Int?> {
    get {
      return graphQLMap["ge"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: Swift.Optional<Int?> {
    get {
      return graphQLMap["gt"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: Swift.Optional<Int?> {
    get {
      return graphQLMap["contains"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: Swift.Optional<Int?> {
    get {
      return graphQLMap["notContains"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: Swift.Optional<[Int?]?> {
    get {
      return graphQLMap["between"] as? Swift.Optional<[Int?]?> ?? Swift.Optional<[Int?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }
}

public struct CreateSleepDataInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - sleepTimestamp
  ///   - sleepStage
  public init(sleepTimestamp: Double, sleepStage: Int) {
    graphQLMap = ["sleep_timestamp": sleepTimestamp, "sleep_stage": sleepStage]
  }

  public var sleepTimestamp: Double {
    get {
      return graphQLMap["sleep_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sleep_timestamp")
    }
  }

  public var sleepStage: Int {
    get {
      return graphQLMap["sleep_stage"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sleep_stage")
    }
  }
}

public struct UpdateSleepDataInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - sleepTimestamp
  ///   - sleepStage
  public init(sleepTimestamp: Double, sleepStage: Swift.Optional<Int?> = nil) {
    graphQLMap = ["sleep_timestamp": sleepTimestamp, "sleep_stage": sleepStage]
  }

  public var sleepTimestamp: Double {
    get {
      return graphQLMap["sleep_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sleep_timestamp")
    }
  }

  public var sleepStage: Swift.Optional<Int?> {
    get {
      return graphQLMap["sleep_stage"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sleep_stage")
    }
  }
}

public struct DeleteSleepDataInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - sleepTimestamp
  public init(sleepTimestamp: Double) {
    graphQLMap = ["sleep_timestamp": sleepTimestamp]
  }

  public var sleepTimestamp: Double {
    get {
      return graphQLMap["sleep_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sleep_timestamp")
    }
  }
}

public struct TableDailyAveragesFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - userId
  ///   - dailyAveragesDate
  ///   - averagePulseRate
  ///   - totalStepCount
  ///   - totalCaloriesBurnt
  ///   - totalDistanceCovered
  ///   - averageTemperature
  ///   - averageSystolic
  ///   - averageDiastolic
  ///   - averageBreathingRate
  ///   - averageSpO2
  ///   - averageHrv
  ///   - sleepData
  ///   - sleepStartTime
  ///   - sleepEndTime
  ///   - healthScore
  ///   - dataPointScore
  public init(userId: Swift.Optional<TableStringFilterInput?> = nil, dailyAveragesDate: Swift.Optional<TableStringFilterInput?> = nil, averagePulseRate: Swift.Optional<TableFloatFilterInput?> = nil, totalStepCount: Swift.Optional<TableFloatFilterInput?> = nil, totalCaloriesBurnt: Swift.Optional<TableFloatFilterInput?> = nil, totalDistanceCovered: Swift.Optional<TableFloatFilterInput?> = nil, averageTemperature: Swift.Optional<TableFloatFilterInput?> = nil, averageSystolic: Swift.Optional<TableFloatFilterInput?> = nil, averageDiastolic: Swift.Optional<TableFloatFilterInput?> = nil, averageBreathingRate: Swift.Optional<TableFloatFilterInput?> = nil, averageSpO2: Swift.Optional<TableFloatFilterInput?> = nil, averageHrv: Swift.Optional<TableFloatFilterInput?> = nil, sleepData: Swift.Optional<TableIntFilterInput?> = nil, sleepStartTime: Swift.Optional<TableStringFilterInput?> = nil, sleepEndTime: Swift.Optional<TableStringFilterInput?> = nil, healthScore: Swift.Optional<TableFloatFilterInput?> = nil, dataPointScore: Swift.Optional<TableFloatFilterInput?> = nil) {
    graphQLMap = ["user_id": userId, "daily_averages_date": dailyAveragesDate, "average_pulse_rate": averagePulseRate, "total_step_count": totalStepCount, "total_calories_burnt": totalCaloriesBurnt, "total_distance_covered": totalDistanceCovered, "average_temperature": averageTemperature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate, "average_sp_o2": averageSpO2, "average_hrv": averageHrv, "sleep_data": sleepData, "sleep_start_time": sleepStartTime, "sleep_end_time": sleepEndTime, "health_score": healthScore, "data_point_score": dataPointScore]
  }

  public var userId: Swift.Optional<TableStringFilterInput?> {
    get {
      return graphQLMap["user_id"] as? Swift.Optional<TableStringFilterInput?> ?? Swift.Optional<TableStringFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "user_id")
    }
  }

  public var dailyAveragesDate: Swift.Optional<TableStringFilterInput?> {
    get {
      return graphQLMap["daily_averages_date"] as? Swift.Optional<TableStringFilterInput?> ?? Swift.Optional<TableStringFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "daily_averages_date")
    }
  }

  public var averagePulseRate: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_pulse_rate"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_pulse_rate")
    }
  }

  public var totalStepCount: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["total_step_count"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "total_step_count")
    }
  }

  public var totalCaloriesBurnt: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["total_calories_burnt"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "total_calories_burnt")
    }
  }

  public var totalDistanceCovered: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["total_distance_covered"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "total_distance_covered")
    }
  }

  public var averageTemperature: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_temperature"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_temperature")
    }
  }

  public var averageSystolic: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_systolic"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_systolic")
    }
  }

  public var averageDiastolic: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_diastolic"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_diastolic")
    }
  }

  public var averageBreathingRate: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_breathing_rate"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_breathing_rate")
    }
  }

  public var averageSpO2: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_sp_o2"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_sp_o2")
    }
  }

  public var averageHrv: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["average_hrv"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "average_hrv")
    }
  }

  public var sleepData: Swift.Optional<TableIntFilterInput?> {
    get {
      return graphQLMap["sleep_data"] as? Swift.Optional<TableIntFilterInput?> ?? Swift.Optional<TableIntFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sleep_data")
    }
  }

  public var sleepStartTime: Swift.Optional<TableStringFilterInput?> {
    get {
      return graphQLMap["sleep_start_time"] as? Swift.Optional<TableStringFilterInput?> ?? Swift.Optional<TableStringFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sleep_start_time")
    }
  }

  public var sleepEndTime: Swift.Optional<TableStringFilterInput?> {
    get {
      return graphQLMap["sleep_end_time"] as? Swift.Optional<TableStringFilterInput?> ?? Swift.Optional<TableStringFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sleep_end_time")
    }
  }

  public var healthScore: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["health_score"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "health_score")
    }
  }

  public var dataPointScore: Swift.Optional<TableFloatFilterInput?> {
    get {
      return graphQLMap["data_point_score"] as? Swift.Optional<TableFloatFilterInput?> ?? Swift.Optional<TableFloatFilterInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "data_point_score")
    }
  }
}

public struct CreateActivityIntensityInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - dataTimestamp
  ///   - value
  public init(dataTimestamp: Double, value: Int) {
    graphQLMap = ["data_timestamp": dataTimestamp, "value": value]
  }

  public var dataTimestamp: Double {
    get {
      return graphQLMap["data_timestamp"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "data_timestamp")
    }
  }

  public var value: Int {
    get {
      return graphQLMap["value"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "value")
    }
  }
}

public final class QueryClinicalTrialUsersDataByTimestampRangeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query queryClinicalTrialUsersDataByTimestampRange($user_name: String!, $from_ts: Float, $to_ts: Float) {
      queryClinicalTrialUsersDataByTimestampRange(id: $user_name, from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          id
          data_timestamp
          systolic
          diastolic
          pulse_rate
          username
        }
      }
    }
    """

  public let operationName: String = "queryClinicalTrialUsersDataByTimestampRange"

  public var user_name: String
  public var from_ts: Double?
  public var to_ts: Double?

  public init(user_name: String, from_ts: Double? = nil, to_ts: Double? = nil) {
    self.user_name = user_name
    self.from_ts = from_ts
    self.to_ts = to_ts
  }

  public var variables: GraphQLMap? {
    return ["user_name": user_name, "from_ts": from_ts, "to_ts": to_ts]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("queryClinicalTrialUsersDataByTimestampRange", arguments: ["id": GraphQLVariable("user_name"), "from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QueryClinicalTrialUsersDataByTimestampRange.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(queryClinicalTrialUsersDataByTimestampRange: QueryClinicalTrialUsersDataByTimestampRange? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "queryClinicalTrialUsersDataByTimestampRange": queryClinicalTrialUsersDataByTimestampRange.flatMap { (value: QueryClinicalTrialUsersDataByTimestampRange) -> ResultMap in value.resultMap }])
    }

    public var queryClinicalTrialUsersDataByTimestampRange: QueryClinicalTrialUsersDataByTimestampRange? {
      get {
        return (resultMap["queryClinicalTrialUsersDataByTimestampRange"] as? ResultMap).flatMap { QueryClinicalTrialUsersDataByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryClinicalTrialUsersDataByTimestampRange")
      }
    }

    public struct QueryClinicalTrialUsersDataByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ClinicalTrialUsersDataConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "ClinicalTrialUsersDataConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["ClinicalTrialUsersData"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(String.self))),
          GraphQLField("data_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("systolic", type: .scalar(Int.self)),
          GraphQLField("diastolic", type: .scalar(Int.self)),
          GraphQLField("pulse_rate", type: .scalar(Int.self)),
          GraphQLField("username", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String, dataTimestamp: Double, systolic: Int? = nil, diastolic: Int? = nil, pulseRate: Int? = nil, username: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "ClinicalTrialUsersData", "id": id, "data_timestamp": dataTimestamp, "systolic": systolic, "diastolic": diastolic, "pulse_rate": pulseRate, "username": username])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String {
          get {
            return resultMap["id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var dataTimestamp: Double {
          get {
            return resultMap["data_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "data_timestamp")
          }
        }

        public var systolic: Int? {
          get {
            return resultMap["systolic"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "systolic")
          }
        }

        public var diastolic: Int? {
          get {
            return resultMap["diastolic"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "diastolic")
          }
        }

        public var pulseRate: Int? {
          get {
            return resultMap["pulse_rate"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "pulse_rate")
          }
        }

        public var username: String? {
          get {
            return resultMap["username"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "username")
          }
        }
      }
    }
  }
}

public final class CreateSkinTemperatureMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createSkinTemperature($CreateSkinTemperatureInput: CreateSkinTemperatureInput!) {
      createSkinTemperature(input: $CreateSkinTemperatureInput) {
        __typename
        user_id
        temperature_timestamp
        temperature_value
      }
    }
    """

  public let operationName: String = "createSkinTemperature"

  public var CreateSkinTemperatureInput: CreateSkinTemperatureInput

  public init(CreateSkinTemperatureInput: CreateSkinTemperatureInput) {
    self.CreateSkinTemperatureInput = CreateSkinTemperatureInput
  }

  public var variables: GraphQLMap? {
    return ["CreateSkinTemperatureInput": CreateSkinTemperatureInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createSkinTemperature", arguments: ["input": GraphQLVariable("CreateSkinTemperatureInput")], type: .object(CreateSkinTemperature.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createSkinTemperature: CreateSkinTemperature? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createSkinTemperature": createSkinTemperature.flatMap { (value: CreateSkinTemperature) -> ResultMap in value.resultMap }])
    }

    public var createSkinTemperature: CreateSkinTemperature? {
      get {
        return (resultMap["createSkinTemperature"] as? ResultMap).flatMap { CreateSkinTemperature(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createSkinTemperature")
      }
    }

    public struct CreateSkinTemperature: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SkinTemperature"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("temperature_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("temperature_value", type: .nonNull(.scalar(Double.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, temperatureTimestamp: Double, temperatureValue: Double) {
        self.init(unsafeResultMap: ["__typename": "SkinTemperature", "user_id": userId, "temperature_timestamp": temperatureTimestamp, "temperature_value": temperatureValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var temperatureTimestamp: Double {
        get {
          return resultMap["temperature_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_timestamp")
        }
      }

      public var temperatureValue: Double {
        get {
          return resultMap["temperature_value"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_value")
        }
      }
    }
  }
}

public final class CreateMultipleSkinTemperatureMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createMultipleSkinTemperature($CreateSkinTemperatureInput: [CreateSkinTemperatureInput!]!) {
      createMultipleSkinTemperature(input: $CreateSkinTemperatureInput) {
        __typename
        user_id
        temperature_timestamp
        temperature_value
      }
    }
    """

  public let operationName: String = "createMultipleSkinTemperature"

  public var CreateSkinTemperatureInput: [CreateSkinTemperatureInput]

  public init(CreateSkinTemperatureInput: [CreateSkinTemperatureInput]) {
    self.CreateSkinTemperatureInput = CreateSkinTemperatureInput
  }

  public var variables: GraphQLMap? {
    return ["CreateSkinTemperatureInput": CreateSkinTemperatureInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createMultipleSkinTemperature", arguments: ["input": GraphQLVariable("CreateSkinTemperatureInput")], type: .list(.object(CreateMultipleSkinTemperature.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createMultipleSkinTemperature: [CreateMultipleSkinTemperature?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createMultipleSkinTemperature": createMultipleSkinTemperature.flatMap { (value: [CreateMultipleSkinTemperature?]) -> [ResultMap?] in value.map { (value: CreateMultipleSkinTemperature?) -> ResultMap? in value.flatMap { (value: CreateMultipleSkinTemperature) -> ResultMap in value.resultMap } } }])
    }

    public var createMultipleSkinTemperature: [CreateMultipleSkinTemperature?]? {
      get {
        return (resultMap["createMultipleSkinTemperature"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [CreateMultipleSkinTemperature?] in value.map { (value: ResultMap?) -> CreateMultipleSkinTemperature? in value.flatMap { (value: ResultMap) -> CreateMultipleSkinTemperature in CreateMultipleSkinTemperature(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [CreateMultipleSkinTemperature?]) -> [ResultMap?] in value.map { (value: CreateMultipleSkinTemperature?) -> ResultMap? in value.flatMap { (value: CreateMultipleSkinTemperature) -> ResultMap in value.resultMap } } }, forKey: "createMultipleSkinTemperature")
      }
    }

    public struct CreateMultipleSkinTemperature: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SkinTemperature"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("temperature_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("temperature_value", type: .nonNull(.scalar(Double.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, temperatureTimestamp: Double, temperatureValue: Double) {
        self.init(unsafeResultMap: ["__typename": "SkinTemperature", "user_id": userId, "temperature_timestamp": temperatureTimestamp, "temperature_value": temperatureValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var temperatureTimestamp: Double {
        get {
          return resultMap["temperature_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_timestamp")
        }
      }

      public var temperatureValue: Double {
        get {
          return resultMap["temperature_value"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_value")
        }
      }
    }
  }
}

public final class UpdateSkinTemperatureMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateSkinTemperature($UpdateSkinTemperatureInput: UpdateSkinTemperatureInput!) {
      updateSkinTemperature(input: $UpdateSkinTemperatureInput) {
        __typename
        user_id
        temperature_timestamp
        temperature_value
      }
    }
    """

  public let operationName: String = "updateSkinTemperature"

  public var UpdateSkinTemperatureInput: UpdateSkinTemperatureInput

  public init(UpdateSkinTemperatureInput: UpdateSkinTemperatureInput) {
    self.UpdateSkinTemperatureInput = UpdateSkinTemperatureInput
  }

  public var variables: GraphQLMap? {
    return ["UpdateSkinTemperatureInput": UpdateSkinTemperatureInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateSkinTemperature", arguments: ["input": GraphQLVariable("UpdateSkinTemperatureInput")], type: .object(UpdateSkinTemperature.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateSkinTemperature: UpdateSkinTemperature? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateSkinTemperature": updateSkinTemperature.flatMap { (value: UpdateSkinTemperature) -> ResultMap in value.resultMap }])
    }

    public var updateSkinTemperature: UpdateSkinTemperature? {
      get {
        return (resultMap["updateSkinTemperature"] as? ResultMap).flatMap { UpdateSkinTemperature(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateSkinTemperature")
      }
    }

    public struct UpdateSkinTemperature: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SkinTemperature"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("temperature_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("temperature_value", type: .nonNull(.scalar(Double.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, temperatureTimestamp: Double, temperatureValue: Double) {
        self.init(unsafeResultMap: ["__typename": "SkinTemperature", "user_id": userId, "temperature_timestamp": temperatureTimestamp, "temperature_value": temperatureValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var temperatureTimestamp: Double {
        get {
          return resultMap["temperature_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_timestamp")
        }
      }

      public var temperatureValue: Double {
        get {
          return resultMap["temperature_value"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_value")
        }
      }
    }
  }
}

public final class DeleteSkinTemperatureMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deleteSkinTemperature($DeleteSkinTemperatureInput: DeleteSkinTemperatureInput!) {
      deleteSkinTemperature(input: $DeleteSkinTemperatureInput) {
        __typename
        user_id
        temperature_timestamp
        temperature_value
      }
    }
    """

  public let operationName: String = "deleteSkinTemperature"

  public var DeleteSkinTemperatureInput: DeleteSkinTemperatureInput

  public init(DeleteSkinTemperatureInput: DeleteSkinTemperatureInput) {
    self.DeleteSkinTemperatureInput = DeleteSkinTemperatureInput
  }

  public var variables: GraphQLMap? {
    return ["DeleteSkinTemperatureInput": DeleteSkinTemperatureInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteSkinTemperature", arguments: ["input": GraphQLVariable("DeleteSkinTemperatureInput")], type: .object(DeleteSkinTemperature.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteSkinTemperature: DeleteSkinTemperature? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteSkinTemperature": deleteSkinTemperature.flatMap { (value: DeleteSkinTemperature) -> ResultMap in value.resultMap }])
    }

    public var deleteSkinTemperature: DeleteSkinTemperature? {
      get {
        return (resultMap["deleteSkinTemperature"] as? ResultMap).flatMap { DeleteSkinTemperature(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteSkinTemperature")
      }
    }

    public struct DeleteSkinTemperature: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SkinTemperature"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("temperature_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("temperature_value", type: .nonNull(.scalar(Double.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, temperatureTimestamp: Double, temperatureValue: Double) {
        self.init(unsafeResultMap: ["__typename": "SkinTemperature", "user_id": userId, "temperature_timestamp": temperatureTimestamp, "temperature_value": temperatureValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var temperatureTimestamp: Double {
        get {
          return resultMap["temperature_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_timestamp")
        }
      }

      public var temperatureValue: Double {
        get {
          return resultMap["temperature_value"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_value")
        }
      }
    }
  }
}

public final class GetSkinTemperatureQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getSkinTemperature($temperature_timestamp: Float!) {
      getSkinTemperature(temperature_timestamp: $temperature_timestamp) {
        __typename
        user_id
        temperature_timestamp
        temperature_value
      }
    }
    """

  public let operationName: String = "getSkinTemperature"

  public var temperature_timestamp: Double

  public init(temperature_timestamp: Double) {
    self.temperature_timestamp = temperature_timestamp
  }

  public var variables: GraphQLMap? {
    return ["temperature_timestamp": temperature_timestamp]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getSkinTemperature", arguments: ["temperature_timestamp": GraphQLVariable("temperature_timestamp")], type: .object(GetSkinTemperature.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getSkinTemperature: GetSkinTemperature? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getSkinTemperature": getSkinTemperature.flatMap { (value: GetSkinTemperature) -> ResultMap in value.resultMap }])
    }

    public var getSkinTemperature: GetSkinTemperature? {
      get {
        return (resultMap["getSkinTemperature"] as? ResultMap).flatMap { GetSkinTemperature(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getSkinTemperature")
      }
    }

    public struct GetSkinTemperature: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SkinTemperature"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("temperature_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("temperature_value", type: .nonNull(.scalar(Double.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, temperatureTimestamp: Double, temperatureValue: Double) {
        self.init(unsafeResultMap: ["__typename": "SkinTemperature", "user_id": userId, "temperature_timestamp": temperatureTimestamp, "temperature_value": temperatureValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var temperatureTimestamp: Double {
        get {
          return resultMap["temperature_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_timestamp")
        }
      }

      public var temperatureValue: Double {
        get {
          return resultMap["temperature_value"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_value")
        }
      }
    }
  }
}

public final class ListSkinTemperaturesQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query listSkinTemperatures {
      listSkinTemperatures {
        __typename
        items {
          __typename
          user_id
          temperature_timestamp
          temperature_value
        }
      }
    }
    """

  public let operationName: String = "listSkinTemperatures"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listSkinTemperatures", type: .object(ListSkinTemperature.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(listSkinTemperatures: ListSkinTemperature? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "listSkinTemperatures": listSkinTemperatures.flatMap { (value: ListSkinTemperature) -> ResultMap in value.resultMap }])
    }

    public var listSkinTemperatures: ListSkinTemperature? {
      get {
        return (resultMap["listSkinTemperatures"] as? ResultMap).flatMap { ListSkinTemperature(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "listSkinTemperatures")
      }
    }

    public struct ListSkinTemperature: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SkinTemperatureConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "SkinTemperatureConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SkinTemperature"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("temperature_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("temperature_value", type: .nonNull(.scalar(Double.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, temperatureTimestamp: Double, temperatureValue: Double) {
          self.init(unsafeResultMap: ["__typename": "SkinTemperature", "user_id": userId, "temperature_timestamp": temperatureTimestamp, "temperature_value": temperatureValue])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var temperatureTimestamp: Double {
          get {
            return resultMap["temperature_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "temperature_timestamp")
          }
        }

        public var temperatureValue: Double {
          get {
            return resultMap["temperature_value"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "temperature_value")
          }
        }
      }
    }
  }
}

public final class QuerySkinTemperatureByTimestampRangeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query querySkinTemperatureByTimestampRange($from_ts: Float, $to_ts: Float) {
      querySkinTemperatureByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          temperature_timestamp
          temperature_value
        }
      }
    }
    """

  public let operationName: String = "querySkinTemperatureByTimestampRange"

  public var from_ts: Double?
  public var to_ts: Double?

  public init(from_ts: Double? = nil, to_ts: Double? = nil) {
    self.from_ts = from_ts
    self.to_ts = to_ts
  }

  public var variables: GraphQLMap? {
    return ["from_ts": from_ts, "to_ts": to_ts]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("querySkinTemperatureByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QuerySkinTemperatureByTimestampRange.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(querySkinTemperatureByTimestampRange: QuerySkinTemperatureByTimestampRange? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "querySkinTemperatureByTimestampRange": querySkinTemperatureByTimestampRange.flatMap { (value: QuerySkinTemperatureByTimestampRange) -> ResultMap in value.resultMap }])
    }

    public var querySkinTemperatureByTimestampRange: QuerySkinTemperatureByTimestampRange? {
      get {
        return (resultMap["querySkinTemperatureByTimestampRange"] as? ResultMap).flatMap { QuerySkinTemperatureByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "querySkinTemperatureByTimestampRange")
      }
    }

    public struct QuerySkinTemperatureByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SkinTemperatureConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "SkinTemperatureConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SkinTemperature"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("temperature_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("temperature_value", type: .nonNull(.scalar(Double.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, temperatureTimestamp: Double, temperatureValue: Double) {
          self.init(unsafeResultMap: ["__typename": "SkinTemperature", "user_id": userId, "temperature_timestamp": temperatureTimestamp, "temperature_value": temperatureValue])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var temperatureTimestamp: Double {
          get {
            return resultMap["temperature_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "temperature_timestamp")
          }
        }

        public var temperatureValue: Double {
          get {
            return resultMap["temperature_value"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "temperature_value")
          }
        }
      }
    }
  }
}

public final class CreateStepCountMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createStepCount($CreateStepCountInput: CreateStepCountInput!) {
      createStepCount(input: $CreateStepCountInput) {
        __typename
        user_id
        step_count_timestamp
        step_count_value
        calories_burnt
        distance_covered
      }
    }
    """

  public let operationName: String = "createStepCount"

  public var CreateStepCountInput: CreateStepCountInput

  public init(CreateStepCountInput: CreateStepCountInput) {
    self.CreateStepCountInput = CreateStepCountInput
  }

  public var variables: GraphQLMap? {
    return ["CreateStepCountInput": CreateStepCountInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createStepCount", arguments: ["input": GraphQLVariable("CreateStepCountInput")], type: .object(CreateStepCount.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createStepCount: CreateStepCount? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createStepCount": createStepCount.flatMap { (value: CreateStepCount) -> ResultMap in value.resultMap }])
    }

    public var createStepCount: CreateStepCount? {
      get {
        return (resultMap["createStepCount"] as? ResultMap).flatMap { CreateStepCount(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createStepCount")
      }
    }

    public struct CreateStepCount: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["StepCount"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("step_count_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("step_count_value", type: .nonNull(.scalar(Int.self))),
        GraphQLField("calories_burnt", type: .scalar(Double.self)),
        GraphQLField("distance_covered", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, stepCountTimestamp: Double, stepCountValue: Int, caloriesBurnt: Double? = nil, distanceCovered: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "StepCount", "user_id": userId, "step_count_timestamp": stepCountTimestamp, "step_count_value": stepCountValue, "calories_burnt": caloriesBurnt, "distance_covered": distanceCovered])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var stepCountTimestamp: Double {
        get {
          return resultMap["step_count_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count_timestamp")
        }
      }

      public var stepCountValue: Int {
        get {
          return resultMap["step_count_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count_value")
        }
      }

      public var caloriesBurnt: Double? {
        get {
          return resultMap["calories_burnt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "calories_burnt")
        }
      }

      public var distanceCovered: Double? {
        get {
          return resultMap["distance_covered"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "distance_covered")
        }
      }
    }
  }
}

public final class CreateMultipleStepCountMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createMultipleStepCount($CreateStepCountInput: [CreateStepCountInput!]!) {
      createMultipleStepCount(input: $CreateStepCountInput) {
        __typename
        user_id
        step_count_timestamp
        step_count_value
        calories_burnt
        distance_covered
      }
    }
    """

  public let operationName: String = "createMultipleStepCount"

  public var CreateStepCountInput: [CreateStepCountInput]

  public init(CreateStepCountInput: [CreateStepCountInput]) {
    self.CreateStepCountInput = CreateStepCountInput
  }

  public var variables: GraphQLMap? {
    return ["CreateStepCountInput": CreateStepCountInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createMultipleStepCount", arguments: ["input": GraphQLVariable("CreateStepCountInput")], type: .list(.object(CreateMultipleStepCount.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createMultipleStepCount: [CreateMultipleStepCount?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createMultipleStepCount": createMultipleStepCount.flatMap { (value: [CreateMultipleStepCount?]) -> [ResultMap?] in value.map { (value: CreateMultipleStepCount?) -> ResultMap? in value.flatMap { (value: CreateMultipleStepCount) -> ResultMap in value.resultMap } } }])
    }

    public var createMultipleStepCount: [CreateMultipleStepCount?]? {
      get {
        return (resultMap["createMultipleStepCount"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [CreateMultipleStepCount?] in value.map { (value: ResultMap?) -> CreateMultipleStepCount? in value.flatMap { (value: ResultMap) -> CreateMultipleStepCount in CreateMultipleStepCount(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [CreateMultipleStepCount?]) -> [ResultMap?] in value.map { (value: CreateMultipleStepCount?) -> ResultMap? in value.flatMap { (value: CreateMultipleStepCount) -> ResultMap in value.resultMap } } }, forKey: "createMultipleStepCount")
      }
    }

    public struct CreateMultipleStepCount: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["StepCount"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("step_count_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("step_count_value", type: .nonNull(.scalar(Int.self))),
        GraphQLField("calories_burnt", type: .scalar(Double.self)),
        GraphQLField("distance_covered", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, stepCountTimestamp: Double, stepCountValue: Int, caloriesBurnt: Double? = nil, distanceCovered: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "StepCount", "user_id": userId, "step_count_timestamp": stepCountTimestamp, "step_count_value": stepCountValue, "calories_burnt": caloriesBurnt, "distance_covered": distanceCovered])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var stepCountTimestamp: Double {
        get {
          return resultMap["step_count_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count_timestamp")
        }
      }

      public var stepCountValue: Int {
        get {
          return resultMap["step_count_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count_value")
        }
      }

      public var caloriesBurnt: Double? {
        get {
          return resultMap["calories_burnt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "calories_burnt")
        }
      }

      public var distanceCovered: Double? {
        get {
          return resultMap["distance_covered"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "distance_covered")
        }
      }
    }
  }
}

public final class UpdateStepCountMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateStepCount($UpdateStepCountInput: UpdateStepCountInput!) {
      updateStepCount(input: $UpdateStepCountInput) {
        __typename
        user_id
        step_count_timestamp
        step_count_value
        calories_burnt
        distance_covered
      }
    }
    """

  public let operationName: String = "updateStepCount"

  public var UpdateStepCountInput: UpdateStepCountInput

  public init(UpdateStepCountInput: UpdateStepCountInput) {
    self.UpdateStepCountInput = UpdateStepCountInput
  }

  public var variables: GraphQLMap? {
    return ["UpdateStepCountInput": UpdateStepCountInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateStepCount", arguments: ["input": GraphQLVariable("UpdateStepCountInput")], type: .object(UpdateStepCount.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateStepCount: UpdateStepCount? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateStepCount": updateStepCount.flatMap { (value: UpdateStepCount) -> ResultMap in value.resultMap }])
    }

    public var updateStepCount: UpdateStepCount? {
      get {
        return (resultMap["updateStepCount"] as? ResultMap).flatMap { UpdateStepCount(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateStepCount")
      }
    }

    public struct UpdateStepCount: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["StepCount"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("step_count_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("step_count_value", type: .nonNull(.scalar(Int.self))),
        GraphQLField("calories_burnt", type: .scalar(Double.self)),
        GraphQLField("distance_covered", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, stepCountTimestamp: Double, stepCountValue: Int, caloriesBurnt: Double? = nil, distanceCovered: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "StepCount", "user_id": userId, "step_count_timestamp": stepCountTimestamp, "step_count_value": stepCountValue, "calories_burnt": caloriesBurnt, "distance_covered": distanceCovered])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var stepCountTimestamp: Double {
        get {
          return resultMap["step_count_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count_timestamp")
        }
      }

      public var stepCountValue: Int {
        get {
          return resultMap["step_count_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count_value")
        }
      }

      public var caloriesBurnt: Double? {
        get {
          return resultMap["calories_burnt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "calories_burnt")
        }
      }

      public var distanceCovered: Double? {
        get {
          return resultMap["distance_covered"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "distance_covered")
        }
      }
    }
  }
}

public final class DeleteStepCountMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deleteStepCount($DeleteStepCountInput: DeleteStepCountInput!) {
      deleteStepCount(input: $DeleteStepCountInput) {
        __typename
        user_id
        step_count_timestamp
        step_count_value
        calories_burnt
        distance_covered
      }
    }
    """

  public let operationName: String = "deleteStepCount"

  public var DeleteStepCountInput: DeleteStepCountInput

  public init(DeleteStepCountInput: DeleteStepCountInput) {
    self.DeleteStepCountInput = DeleteStepCountInput
  }

  public var variables: GraphQLMap? {
    return ["DeleteStepCountInput": DeleteStepCountInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteStepCount", arguments: ["input": GraphQLVariable("DeleteStepCountInput")], type: .object(DeleteStepCount.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteStepCount: DeleteStepCount? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteStepCount": deleteStepCount.flatMap { (value: DeleteStepCount) -> ResultMap in value.resultMap }])
    }

    public var deleteStepCount: DeleteStepCount? {
      get {
        return (resultMap["deleteStepCount"] as? ResultMap).flatMap { DeleteStepCount(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteStepCount")
      }
    }

    public struct DeleteStepCount: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["StepCount"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("step_count_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("step_count_value", type: .nonNull(.scalar(Int.self))),
        GraphQLField("calories_burnt", type: .scalar(Double.self)),
        GraphQLField("distance_covered", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, stepCountTimestamp: Double, stepCountValue: Int, caloriesBurnt: Double? = nil, distanceCovered: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "StepCount", "user_id": userId, "step_count_timestamp": stepCountTimestamp, "step_count_value": stepCountValue, "calories_burnt": caloriesBurnt, "distance_covered": distanceCovered])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var stepCountTimestamp: Double {
        get {
          return resultMap["step_count_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count_timestamp")
        }
      }

      public var stepCountValue: Int {
        get {
          return resultMap["step_count_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count_value")
        }
      }

      public var caloriesBurnt: Double? {
        get {
          return resultMap["calories_burnt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "calories_burnt")
        }
      }

      public var distanceCovered: Double? {
        get {
          return resultMap["distance_covered"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "distance_covered")
        }
      }
    }
  }
}

public final class GetStepCountQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getStepCount($step_count_timestamp: Float!) {
      getStepCount(step_count_timestamp: $step_count_timestamp) {
        __typename
        user_id
        step_count_timestamp
        step_count_value
        calories_burnt
        distance_covered
      }
    }
    """

  public let operationName: String = "getStepCount"

  public var step_count_timestamp: Double

  public init(step_count_timestamp: Double) {
    self.step_count_timestamp = step_count_timestamp
  }

  public var variables: GraphQLMap? {
    return ["step_count_timestamp": step_count_timestamp]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getStepCount", arguments: ["step_count_timestamp": GraphQLVariable("step_count_timestamp")], type: .object(GetStepCount.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getStepCount: GetStepCount? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getStepCount": getStepCount.flatMap { (value: GetStepCount) -> ResultMap in value.resultMap }])
    }

    public var getStepCount: GetStepCount? {
      get {
        return (resultMap["getStepCount"] as? ResultMap).flatMap { GetStepCount(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getStepCount")
      }
    }

    public struct GetStepCount: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["StepCount"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("step_count_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("step_count_value", type: .nonNull(.scalar(Int.self))),
        GraphQLField("calories_burnt", type: .scalar(Double.self)),
        GraphQLField("distance_covered", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, stepCountTimestamp: Double, stepCountValue: Int, caloriesBurnt: Double? = nil, distanceCovered: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "StepCount", "user_id": userId, "step_count_timestamp": stepCountTimestamp, "step_count_value": stepCountValue, "calories_burnt": caloriesBurnt, "distance_covered": distanceCovered])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var stepCountTimestamp: Double {
        get {
          return resultMap["step_count_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count_timestamp")
        }
      }

      public var stepCountValue: Int {
        get {
          return resultMap["step_count_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count_value")
        }
      }

      public var caloriesBurnt: Double? {
        get {
          return resultMap["calories_burnt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "calories_burnt")
        }
      }

      public var distanceCovered: Double? {
        get {
          return resultMap["distance_covered"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "distance_covered")
        }
      }
    }
  }
}

public final class ListStepCountsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query listStepCounts {
      listStepCounts {
        __typename
        items {
          __typename
          user_id
          step_count_timestamp
          step_count_value
          calories_burnt
          distance_covered
        }
      }
    }
    """

  public let operationName: String = "listStepCounts"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listStepCounts", type: .object(ListStepCount.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(listStepCounts: ListStepCount? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "listStepCounts": listStepCounts.flatMap { (value: ListStepCount) -> ResultMap in value.resultMap }])
    }

    public var listStepCounts: ListStepCount? {
      get {
        return (resultMap["listStepCounts"] as? ResultMap).flatMap { ListStepCount(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "listStepCounts")
      }
    }

    public struct ListStepCount: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["StepCountConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "StepCountConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["StepCount"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("step_count_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("step_count_value", type: .nonNull(.scalar(Int.self))),
          GraphQLField("calories_burnt", type: .scalar(Double.self)),
          GraphQLField("distance_covered", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, stepCountTimestamp: Double, stepCountValue: Int, caloriesBurnt: Double? = nil, distanceCovered: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "StepCount", "user_id": userId, "step_count_timestamp": stepCountTimestamp, "step_count_value": stepCountValue, "calories_burnt": caloriesBurnt, "distance_covered": distanceCovered])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var stepCountTimestamp: Double {
          get {
            return resultMap["step_count_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "step_count_timestamp")
          }
        }

        public var stepCountValue: Int {
          get {
            return resultMap["step_count_value"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "step_count_value")
          }
        }

        public var caloriesBurnt: Double? {
          get {
            return resultMap["calories_burnt"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "calories_burnt")
          }
        }

        public var distanceCovered: Double? {
          get {
            return resultMap["distance_covered"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "distance_covered")
          }
        }
      }
    }
  }
}

public final class QueryStepCountByTimestampRangeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query queryStepCountByTimestampRange($from_ts: Float, $to_ts: Float) {
      queryStepCountByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          step_count_timestamp
          step_count_value
          calories_burnt
          distance_covered
        }
      }
    }
    """

  public let operationName: String = "queryStepCountByTimestampRange"

  public var from_ts: Double?
  public var to_ts: Double?

  public init(from_ts: Double? = nil, to_ts: Double? = nil) {
    self.from_ts = from_ts
    self.to_ts = to_ts
  }

  public var variables: GraphQLMap? {
    return ["from_ts": from_ts, "to_ts": to_ts]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("queryStepCountByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QueryStepCountByTimestampRange.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(queryStepCountByTimestampRange: QueryStepCountByTimestampRange? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "queryStepCountByTimestampRange": queryStepCountByTimestampRange.flatMap { (value: QueryStepCountByTimestampRange) -> ResultMap in value.resultMap }])
    }

    public var queryStepCountByTimestampRange: QueryStepCountByTimestampRange? {
      get {
        return (resultMap["queryStepCountByTimestampRange"] as? ResultMap).flatMap { QueryStepCountByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryStepCountByTimestampRange")
      }
    }

    public struct QueryStepCountByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["StepCountConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "StepCountConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["StepCount"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("step_count_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("step_count_value", type: .nonNull(.scalar(Int.self))),
          GraphQLField("calories_burnt", type: .scalar(Double.self)),
          GraphQLField("distance_covered", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, stepCountTimestamp: Double, stepCountValue: Int, caloriesBurnt: Double? = nil, distanceCovered: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "StepCount", "user_id": userId, "step_count_timestamp": stepCountTimestamp, "step_count_value": stepCountValue, "calories_burnt": caloriesBurnt, "distance_covered": distanceCovered])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var stepCountTimestamp: Double {
          get {
            return resultMap["step_count_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "step_count_timestamp")
          }
        }

        public var stepCountValue: Int {
          get {
            return resultMap["step_count_value"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "step_count_value")
          }
        }

        public var caloriesBurnt: Double? {
          get {
            return resultMap["calories_burnt"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "calories_burnt")
          }
        }

        public var distanceCovered: Double? {
          get {
            return resultMap["distance_covered"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "distance_covered")
          }
        }
      }
    }
  }
}

public final class CreatePulseRateMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createPulseRate($CreatePulseRateInput: CreatePulseRateInput!) {
      createPulseRate(input: $CreatePulseRateInput) {
        __typename
        user_id
        pulse_rate_timestamp
        pulse_rate_value
      }
    }
    """

  public let operationName: String = "createPulseRate"

  public var CreatePulseRateInput: CreatePulseRateInput

  public init(CreatePulseRateInput: CreatePulseRateInput) {
    self.CreatePulseRateInput = CreatePulseRateInput
  }

  public var variables: GraphQLMap? {
    return ["CreatePulseRateInput": CreatePulseRateInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createPulseRate", arguments: ["input": GraphQLVariable("CreatePulseRateInput")], type: .object(CreatePulseRate.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createPulseRate: CreatePulseRate? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createPulseRate": createPulseRate.flatMap { (value: CreatePulseRate) -> ResultMap in value.resultMap }])
    }

    public var createPulseRate: CreatePulseRate? {
      get {
        return (resultMap["createPulseRate"] as? ResultMap).flatMap { CreatePulseRate(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createPulseRate")
      }
    }

    public struct CreatePulseRate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PulseRate"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("pulse_rate_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("pulse_rate_value", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, pulseRateTimestamp: Double, pulseRateValue: Int) {
        self.init(unsafeResultMap: ["__typename": "PulseRate", "user_id": userId, "pulse_rate_timestamp": pulseRateTimestamp, "pulse_rate_value": pulseRateValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var pulseRateTimestamp: Double {
        get {
          return resultMap["pulse_rate_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_timestamp")
        }
      }

      public var pulseRateValue: Int {
        get {
          return resultMap["pulse_rate_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_value")
        }
      }
    }
  }
}

public final class CreateMultiplePulseRateMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createMultiplePulseRate($CreatePulseRateInput: [CreatePulseRateInput!]!) {
      createMultiplePulseRate(input: $CreatePulseRateInput) {
        __typename
        user_id
        pulse_rate_timestamp
        pulse_rate_value
      }
    }
    """

  public let operationName: String = "createMultiplePulseRate"

  public var CreatePulseRateInput: [CreatePulseRateInput]

  public init(CreatePulseRateInput: [CreatePulseRateInput]) {
    self.CreatePulseRateInput = CreatePulseRateInput
  }

  public var variables: GraphQLMap? {
    return ["CreatePulseRateInput": CreatePulseRateInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createMultiplePulseRate", arguments: ["input": GraphQLVariable("CreatePulseRateInput")], type: .list(.object(CreateMultiplePulseRate.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createMultiplePulseRate: [CreateMultiplePulseRate?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createMultiplePulseRate": createMultiplePulseRate.flatMap { (value: [CreateMultiplePulseRate?]) -> [ResultMap?] in value.map { (value: CreateMultiplePulseRate?) -> ResultMap? in value.flatMap { (value: CreateMultiplePulseRate) -> ResultMap in value.resultMap } } }])
    }

    public var createMultiplePulseRate: [CreateMultiplePulseRate?]? {
      get {
        return (resultMap["createMultiplePulseRate"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [CreateMultiplePulseRate?] in value.map { (value: ResultMap?) -> CreateMultiplePulseRate? in value.flatMap { (value: ResultMap) -> CreateMultiplePulseRate in CreateMultiplePulseRate(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [CreateMultiplePulseRate?]) -> [ResultMap?] in value.map { (value: CreateMultiplePulseRate?) -> ResultMap? in value.flatMap { (value: CreateMultiplePulseRate) -> ResultMap in value.resultMap } } }, forKey: "createMultiplePulseRate")
      }
    }

    public struct CreateMultiplePulseRate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PulseRate"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("pulse_rate_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("pulse_rate_value", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, pulseRateTimestamp: Double, pulseRateValue: Int) {
        self.init(unsafeResultMap: ["__typename": "PulseRate", "user_id": userId, "pulse_rate_timestamp": pulseRateTimestamp, "pulse_rate_value": pulseRateValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var pulseRateTimestamp: Double {
        get {
          return resultMap["pulse_rate_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_timestamp")
        }
      }

      public var pulseRateValue: Int {
        get {
          return resultMap["pulse_rate_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_value")
        }
      }
    }
  }
}

public final class UpdatePulseRateMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updatePulseRate($UpdatePulseRateInput: UpdatePulseRateInput!) {
      updatePulseRate(input: $UpdatePulseRateInput) {
        __typename
        user_id
        pulse_rate_timestamp
        pulse_rate_value
      }
    }
    """

  public let operationName: String = "updatePulseRate"

  public var UpdatePulseRateInput: UpdatePulseRateInput

  public init(UpdatePulseRateInput: UpdatePulseRateInput) {
    self.UpdatePulseRateInput = UpdatePulseRateInput
  }

  public var variables: GraphQLMap? {
    return ["UpdatePulseRateInput": UpdatePulseRateInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updatePulseRate", arguments: ["input": GraphQLVariable("UpdatePulseRateInput")], type: .object(UpdatePulseRate.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updatePulseRate: UpdatePulseRate? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updatePulseRate": updatePulseRate.flatMap { (value: UpdatePulseRate) -> ResultMap in value.resultMap }])
    }

    public var updatePulseRate: UpdatePulseRate? {
      get {
        return (resultMap["updatePulseRate"] as? ResultMap).flatMap { UpdatePulseRate(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updatePulseRate")
      }
    }

    public struct UpdatePulseRate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PulseRate"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("pulse_rate_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("pulse_rate_value", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, pulseRateTimestamp: Double, pulseRateValue: Int) {
        self.init(unsafeResultMap: ["__typename": "PulseRate", "user_id": userId, "pulse_rate_timestamp": pulseRateTimestamp, "pulse_rate_value": pulseRateValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var pulseRateTimestamp: Double {
        get {
          return resultMap["pulse_rate_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_timestamp")
        }
      }

      public var pulseRateValue: Int {
        get {
          return resultMap["pulse_rate_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_value")
        }
      }
    }
  }
}

public final class DeletePulseRateMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deletePulseRate($DeletePulseRateInput: DeletePulseRateInput!) {
      deletePulseRate(input: $DeletePulseRateInput) {
        __typename
        user_id
        pulse_rate_timestamp
        pulse_rate_value
      }
    }
    """

  public let operationName: String = "deletePulseRate"

  public var DeletePulseRateInput: DeletePulseRateInput

  public init(DeletePulseRateInput: DeletePulseRateInput) {
    self.DeletePulseRateInput = DeletePulseRateInput
  }

  public var variables: GraphQLMap? {
    return ["DeletePulseRateInput": DeletePulseRateInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deletePulseRate", arguments: ["input": GraphQLVariable("DeletePulseRateInput")], type: .object(DeletePulseRate.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deletePulseRate: DeletePulseRate? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deletePulseRate": deletePulseRate.flatMap { (value: DeletePulseRate) -> ResultMap in value.resultMap }])
    }

    public var deletePulseRate: DeletePulseRate? {
      get {
        return (resultMap["deletePulseRate"] as? ResultMap).flatMap { DeletePulseRate(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deletePulseRate")
      }
    }

    public struct DeletePulseRate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PulseRate"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("pulse_rate_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("pulse_rate_value", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, pulseRateTimestamp: Double, pulseRateValue: Int) {
        self.init(unsafeResultMap: ["__typename": "PulseRate", "user_id": userId, "pulse_rate_timestamp": pulseRateTimestamp, "pulse_rate_value": pulseRateValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var pulseRateTimestamp: Double {
        get {
          return resultMap["pulse_rate_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_timestamp")
        }
      }

      public var pulseRateValue: Int {
        get {
          return resultMap["pulse_rate_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_value")
        }
      }
    }
  }
}

public final class GetPulseRateQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getPulseRate($pulse_rate_timestamp: Float!) {
      getPulseRate(pulse_rate_timestamp: $pulse_rate_timestamp) {
        __typename
        user_id
        pulse_rate_timestamp
        pulse_rate_value
      }
    }
    """

  public let operationName: String = "getPulseRate"

  public var pulse_rate_timestamp: Double

  public init(pulse_rate_timestamp: Double) {
    self.pulse_rate_timestamp = pulse_rate_timestamp
  }

  public var variables: GraphQLMap? {
    return ["pulse_rate_timestamp": pulse_rate_timestamp]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getPulseRate", arguments: ["pulse_rate_timestamp": GraphQLVariable("pulse_rate_timestamp")], type: .object(GetPulseRate.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getPulseRate: GetPulseRate? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getPulseRate": getPulseRate.flatMap { (value: GetPulseRate) -> ResultMap in value.resultMap }])
    }

    public var getPulseRate: GetPulseRate? {
      get {
        return (resultMap["getPulseRate"] as? ResultMap).flatMap { GetPulseRate(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getPulseRate")
      }
    }

    public struct GetPulseRate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PulseRate"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("pulse_rate_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("pulse_rate_value", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, pulseRateTimestamp: Double, pulseRateValue: Int) {
        self.init(unsafeResultMap: ["__typename": "PulseRate", "user_id": userId, "pulse_rate_timestamp": pulseRateTimestamp, "pulse_rate_value": pulseRateValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var pulseRateTimestamp: Double {
        get {
          return resultMap["pulse_rate_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_timestamp")
        }
      }

      public var pulseRateValue: Int {
        get {
          return resultMap["pulse_rate_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_value")
        }
      }
    }
  }
}

public final class ListPulseRatesQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query listPulseRates {
      listPulseRates {
        __typename
        items {
          __typename
          user_id
          pulse_rate_timestamp
          pulse_rate_value
        }
      }
    }
    """

  public let operationName: String = "listPulseRates"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listPulseRates", type: .object(ListPulseRate.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(listPulseRates: ListPulseRate? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "listPulseRates": listPulseRates.flatMap { (value: ListPulseRate) -> ResultMap in value.resultMap }])
    }

    public var listPulseRates: ListPulseRate? {
      get {
        return (resultMap["listPulseRates"] as? ResultMap).flatMap { ListPulseRate(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "listPulseRates")
      }
    }

    public struct ListPulseRate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PulseRateConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "PulseRateConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["PulseRate"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("pulse_rate_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("pulse_rate_value", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, pulseRateTimestamp: Double, pulseRateValue: Int) {
          self.init(unsafeResultMap: ["__typename": "PulseRate", "user_id": userId, "pulse_rate_timestamp": pulseRateTimestamp, "pulse_rate_value": pulseRateValue])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var pulseRateTimestamp: Double {
          get {
            return resultMap["pulse_rate_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "pulse_rate_timestamp")
          }
        }

        public var pulseRateValue: Int {
          get {
            return resultMap["pulse_rate_value"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "pulse_rate_value")
          }
        }
      }
    }
  }
}

public final class QueryPulseRateByTimestampRangeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query queryPulseRateByTimestampRange($from_ts: Float, $to_ts: Float) {
      queryPulseRateByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          pulse_rate_timestamp
          pulse_rate_value
        }
      }
    }
    """

  public let operationName: String = "queryPulseRateByTimestampRange"

  public var from_ts: Double?
  public var to_ts: Double?

  public init(from_ts: Double? = nil, to_ts: Double? = nil) {
    self.from_ts = from_ts
    self.to_ts = to_ts
  }

  public var variables: GraphQLMap? {
    return ["from_ts": from_ts, "to_ts": to_ts]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("queryPulseRateByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QueryPulseRateByTimestampRange.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(queryPulseRateByTimestampRange: QueryPulseRateByTimestampRange? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "queryPulseRateByTimestampRange": queryPulseRateByTimestampRange.flatMap { (value: QueryPulseRateByTimestampRange) -> ResultMap in value.resultMap }])
    }

    public var queryPulseRateByTimestampRange: QueryPulseRateByTimestampRange? {
      get {
        return (resultMap["queryPulseRateByTimestampRange"] as? ResultMap).flatMap { QueryPulseRateByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryPulseRateByTimestampRange")
      }
    }

    public struct QueryPulseRateByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PulseRateConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "PulseRateConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["PulseRate"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("pulse_rate_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("pulse_rate_value", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, pulseRateTimestamp: Double, pulseRateValue: Int) {
          self.init(unsafeResultMap: ["__typename": "PulseRate", "user_id": userId, "pulse_rate_timestamp": pulseRateTimestamp, "pulse_rate_value": pulseRateValue])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var pulseRateTimestamp: Double {
          get {
            return resultMap["pulse_rate_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "pulse_rate_timestamp")
          }
        }

        public var pulseRateValue: Int {
          get {
            return resultMap["pulse_rate_value"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "pulse_rate_value")
          }
        }
      }
    }
  }
}

public final class CreateExerciseDataMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createExerciseData($CreateExerciseDataInput: CreateExerciseDataInput!) {
      createExerciseData(input: $CreateExerciseDataInput) {
        __typename
        id
        user_id
        start_time
        end_time
        average_pulse_rate
        total_step_count
        total_calories_burnt
        total_distance_covered
        average_skin_temprature
        average_systolic
        average_diastolic
        average_breathing_rate
        average_spo2
      }
    }
    """

  public let operationName: String = "createExerciseData"

  public var CreateExerciseDataInput: CreateExerciseDataInput

  public init(CreateExerciseDataInput: CreateExerciseDataInput) {
    self.CreateExerciseDataInput = CreateExerciseDataInput
  }

  public var variables: GraphQLMap? {
    return ["CreateExerciseDataInput": CreateExerciseDataInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createExerciseData", arguments: ["input": GraphQLVariable("CreateExerciseDataInput")], type: .object(CreateExerciseDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createExerciseData: CreateExerciseDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createExerciseData": createExerciseData.flatMap { (value: CreateExerciseDatum) -> ResultMap in value.resultMap }])
    }

    public var createExerciseData: CreateExerciseDatum? {
      get {
        return (resultMap["createExerciseData"] as? ResultMap).flatMap { CreateExerciseDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createExerciseData")
      }
    }

    public struct CreateExerciseDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ExerciseData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("start_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("end_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("average_pulse_rate", type: .scalar(Double.self)),
        GraphQLField("total_step_count", type: .scalar(Double.self)),
        GraphQLField("total_calories_burnt", type: .scalar(Double.self)),
        GraphQLField("total_distance_covered", type: .scalar(Double.self)),
        GraphQLField("average_skin_temprature", type: .scalar(Double.self)),
        GraphQLField("average_systolic", type: .scalar(Double.self)),
        GraphQLField("average_diastolic", type: .scalar(Double.self)),
        GraphQLField("average_breathing_rate", type: .scalar(Double.self)),
        GraphQLField("average_spo2", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, userId: String, startTime: Double, endTime: Double, averagePulseRate: Double? = nil, totalStepCount: Double? = nil, totalCaloriesBurnt: Double? = nil, totalDistanceCovered: Double? = nil, averageSkinTemprature: Double? = nil, averageSystolic: Double? = nil, averageDiastolic: Double? = nil, averageBreathingRate: Double? = nil, averageSpo2: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "ExerciseData", "id": id, "user_id": userId, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "total_step_count": totalStepCount, "total_calories_burnt": totalCaloriesBurnt, "total_distance_covered": totalDistanceCovered, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate, "average_spo2": averageSpo2])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var startTime: Double {
        get {
          return resultMap["start_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "start_time")
        }
      }

      public var endTime: Double {
        get {
          return resultMap["end_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "end_time")
        }
      }

      public var averagePulseRate: Double? {
        get {
          return resultMap["average_pulse_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_pulse_rate")
        }
      }

      public var totalStepCount: Double? {
        get {
          return resultMap["total_step_count"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_step_count")
        }
      }

      public var totalCaloriesBurnt: Double? {
        get {
          return resultMap["total_calories_burnt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_calories_burnt")
        }
      }

      public var totalDistanceCovered: Double? {
        get {
          return resultMap["total_distance_covered"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_distance_covered")
        }
      }

      public var averageSkinTemprature: Double? {
        get {
          return resultMap["average_skin_temprature"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_skin_temprature")
        }
      }

      public var averageSystolic: Double? {
        get {
          return resultMap["average_systolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_systolic")
        }
      }

      public var averageDiastolic: Double? {
        get {
          return resultMap["average_diastolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_diastolic")
        }
      }

      public var averageBreathingRate: Double? {
        get {
          return resultMap["average_breathing_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_breathing_rate")
        }
      }

      public var averageSpo2: Double? {
        get {
          return resultMap["average_spo2"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_spo2")
        }
      }
    }
  }
}

public final class UpdateExerciseDataMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateExerciseData($UpdateExerciseDataInput: UpdateExerciseDataInput!) {
      updateExerciseData(input: $UpdateExerciseDataInput) {
        __typename
        id
        user_id
        start_time
        end_time
        average_pulse_rate
        total_step_count
        total_calories_burnt
        total_distance_covered
        average_skin_temprature
        average_systolic
        average_diastolic
        average_breathing_rate
        average_spo2
      }
    }
    """

  public let operationName: String = "updateExerciseData"

  public var UpdateExerciseDataInput: UpdateExerciseDataInput

  public init(UpdateExerciseDataInput: UpdateExerciseDataInput) {
    self.UpdateExerciseDataInput = UpdateExerciseDataInput
  }

  public var variables: GraphQLMap? {
    return ["UpdateExerciseDataInput": UpdateExerciseDataInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateExerciseData", arguments: ["input": GraphQLVariable("UpdateExerciseDataInput")], type: .object(UpdateExerciseDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateExerciseData: UpdateExerciseDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateExerciseData": updateExerciseData.flatMap { (value: UpdateExerciseDatum) -> ResultMap in value.resultMap }])
    }

    public var updateExerciseData: UpdateExerciseDatum? {
      get {
        return (resultMap["updateExerciseData"] as? ResultMap).flatMap { UpdateExerciseDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateExerciseData")
      }
    }

    public struct UpdateExerciseDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ExerciseData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("start_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("end_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("average_pulse_rate", type: .scalar(Double.self)),
        GraphQLField("total_step_count", type: .scalar(Double.self)),
        GraphQLField("total_calories_burnt", type: .scalar(Double.self)),
        GraphQLField("total_distance_covered", type: .scalar(Double.self)),
        GraphQLField("average_skin_temprature", type: .scalar(Double.self)),
        GraphQLField("average_systolic", type: .scalar(Double.self)),
        GraphQLField("average_diastolic", type: .scalar(Double.self)),
        GraphQLField("average_breathing_rate", type: .scalar(Double.self)),
        GraphQLField("average_spo2", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, userId: String, startTime: Double, endTime: Double, averagePulseRate: Double? = nil, totalStepCount: Double? = nil, totalCaloriesBurnt: Double? = nil, totalDistanceCovered: Double? = nil, averageSkinTemprature: Double? = nil, averageSystolic: Double? = nil, averageDiastolic: Double? = nil, averageBreathingRate: Double? = nil, averageSpo2: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "ExerciseData", "id": id, "user_id": userId, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "total_step_count": totalStepCount, "total_calories_burnt": totalCaloriesBurnt, "total_distance_covered": totalDistanceCovered, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate, "average_spo2": averageSpo2])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var startTime: Double {
        get {
          return resultMap["start_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "start_time")
        }
      }

      public var endTime: Double {
        get {
          return resultMap["end_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "end_time")
        }
      }

      public var averagePulseRate: Double? {
        get {
          return resultMap["average_pulse_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_pulse_rate")
        }
      }

      public var totalStepCount: Double? {
        get {
          return resultMap["total_step_count"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_step_count")
        }
      }

      public var totalCaloriesBurnt: Double? {
        get {
          return resultMap["total_calories_burnt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_calories_burnt")
        }
      }

      public var totalDistanceCovered: Double? {
        get {
          return resultMap["total_distance_covered"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_distance_covered")
        }
      }

      public var averageSkinTemprature: Double? {
        get {
          return resultMap["average_skin_temprature"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_skin_temprature")
        }
      }

      public var averageSystolic: Double? {
        get {
          return resultMap["average_systolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_systolic")
        }
      }

      public var averageDiastolic: Double? {
        get {
          return resultMap["average_diastolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_diastolic")
        }
      }

      public var averageBreathingRate: Double? {
        get {
          return resultMap["average_breathing_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_breathing_rate")
        }
      }

      public var averageSpo2: Double? {
        get {
          return resultMap["average_spo2"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_spo2")
        }
      }
    }
  }
}

public final class DeleteExerciseDataMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deleteExerciseData($DeleteExerciseDataInput: DeleteExerciseDataInput!) {
      deleteExerciseData(input: $DeleteExerciseDataInput) {
        __typename
        id
        user_id
        start_time
        end_time
        average_pulse_rate
        total_step_count
        total_calories_burnt
        total_distance_covered
        average_skin_temprature
        average_systolic
        average_diastolic
        average_breathing_rate
        average_spo2
      }
    }
    """

  public let operationName: String = "deleteExerciseData"

  public var DeleteExerciseDataInput: DeleteExerciseDataInput

  public init(DeleteExerciseDataInput: DeleteExerciseDataInput) {
    self.DeleteExerciseDataInput = DeleteExerciseDataInput
  }

  public var variables: GraphQLMap? {
    return ["DeleteExerciseDataInput": DeleteExerciseDataInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteExerciseData", arguments: ["input": GraphQLVariable("DeleteExerciseDataInput")], type: .object(DeleteExerciseDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteExerciseData: DeleteExerciseDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteExerciseData": deleteExerciseData.flatMap { (value: DeleteExerciseDatum) -> ResultMap in value.resultMap }])
    }

    public var deleteExerciseData: DeleteExerciseDatum? {
      get {
        return (resultMap["deleteExerciseData"] as? ResultMap).flatMap { DeleteExerciseDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteExerciseData")
      }
    }

    public struct DeleteExerciseDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ExerciseData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("start_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("end_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("average_pulse_rate", type: .scalar(Double.self)),
        GraphQLField("total_step_count", type: .scalar(Double.self)),
        GraphQLField("total_calories_burnt", type: .scalar(Double.self)),
        GraphQLField("total_distance_covered", type: .scalar(Double.self)),
        GraphQLField("average_skin_temprature", type: .scalar(Double.self)),
        GraphQLField("average_systolic", type: .scalar(Double.self)),
        GraphQLField("average_diastolic", type: .scalar(Double.self)),
        GraphQLField("average_breathing_rate", type: .scalar(Double.self)),
        GraphQLField("average_spo2", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, userId: String, startTime: Double, endTime: Double, averagePulseRate: Double? = nil, totalStepCount: Double? = nil, totalCaloriesBurnt: Double? = nil, totalDistanceCovered: Double? = nil, averageSkinTemprature: Double? = nil, averageSystolic: Double? = nil, averageDiastolic: Double? = nil, averageBreathingRate: Double? = nil, averageSpo2: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "ExerciseData", "id": id, "user_id": userId, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "total_step_count": totalStepCount, "total_calories_burnt": totalCaloriesBurnt, "total_distance_covered": totalDistanceCovered, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate, "average_spo2": averageSpo2])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var startTime: Double {
        get {
          return resultMap["start_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "start_time")
        }
      }

      public var endTime: Double {
        get {
          return resultMap["end_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "end_time")
        }
      }

      public var averagePulseRate: Double? {
        get {
          return resultMap["average_pulse_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_pulse_rate")
        }
      }

      public var totalStepCount: Double? {
        get {
          return resultMap["total_step_count"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_step_count")
        }
      }

      public var totalCaloriesBurnt: Double? {
        get {
          return resultMap["total_calories_burnt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_calories_burnt")
        }
      }

      public var totalDistanceCovered: Double? {
        get {
          return resultMap["total_distance_covered"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_distance_covered")
        }
      }

      public var averageSkinTemprature: Double? {
        get {
          return resultMap["average_skin_temprature"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_skin_temprature")
        }
      }

      public var averageSystolic: Double? {
        get {
          return resultMap["average_systolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_systolic")
        }
      }

      public var averageDiastolic: Double? {
        get {
          return resultMap["average_diastolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_diastolic")
        }
      }

      public var averageBreathingRate: Double? {
        get {
          return resultMap["average_breathing_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_breathing_rate")
        }
      }

      public var averageSpo2: Double? {
        get {
          return resultMap["average_spo2"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_spo2")
        }
      }
    }
  }
}

public final class GetExerciseDataQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getExerciseData($id: ID!) {
      getExerciseData(id: $id) {
        __typename
        id
        user_id
        start_time
        end_time
        average_pulse_rate
        total_step_count
        total_calories_burnt
        total_distance_covered
        average_skin_temprature
        average_systolic
        average_diastolic
        average_breathing_rate
        average_spo2
      }
    }
    """

  public let operationName: String = "getExerciseData"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getExerciseData", arguments: ["id": GraphQLVariable("id")], type: .object(GetExerciseDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getExerciseData: GetExerciseDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getExerciseData": getExerciseData.flatMap { (value: GetExerciseDatum) -> ResultMap in value.resultMap }])
    }

    public var getExerciseData: GetExerciseDatum? {
      get {
        return (resultMap["getExerciseData"] as? ResultMap).flatMap { GetExerciseDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getExerciseData")
      }
    }

    public struct GetExerciseDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ExerciseData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("start_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("end_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("average_pulse_rate", type: .scalar(Double.self)),
        GraphQLField("total_step_count", type: .scalar(Double.self)),
        GraphQLField("total_calories_burnt", type: .scalar(Double.self)),
        GraphQLField("total_distance_covered", type: .scalar(Double.self)),
        GraphQLField("average_skin_temprature", type: .scalar(Double.self)),
        GraphQLField("average_systolic", type: .scalar(Double.self)),
        GraphQLField("average_diastolic", type: .scalar(Double.self)),
        GraphQLField("average_breathing_rate", type: .scalar(Double.self)),
        GraphQLField("average_spo2", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, userId: String, startTime: Double, endTime: Double, averagePulseRate: Double? = nil, totalStepCount: Double? = nil, totalCaloriesBurnt: Double? = nil, totalDistanceCovered: Double? = nil, averageSkinTemprature: Double? = nil, averageSystolic: Double? = nil, averageDiastolic: Double? = nil, averageBreathingRate: Double? = nil, averageSpo2: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "ExerciseData", "id": id, "user_id": userId, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "total_step_count": totalStepCount, "total_calories_burnt": totalCaloriesBurnt, "total_distance_covered": totalDistanceCovered, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate, "average_spo2": averageSpo2])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var startTime: Double {
        get {
          return resultMap["start_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "start_time")
        }
      }

      public var endTime: Double {
        get {
          return resultMap["end_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "end_time")
        }
      }

      public var averagePulseRate: Double? {
        get {
          return resultMap["average_pulse_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_pulse_rate")
        }
      }

      public var totalStepCount: Double? {
        get {
          return resultMap["total_step_count"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_step_count")
        }
      }

      public var totalCaloriesBurnt: Double? {
        get {
          return resultMap["total_calories_burnt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_calories_burnt")
        }
      }

      public var totalDistanceCovered: Double? {
        get {
          return resultMap["total_distance_covered"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_distance_covered")
        }
      }

      public var averageSkinTemprature: Double? {
        get {
          return resultMap["average_skin_temprature"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_skin_temprature")
        }
      }

      public var averageSystolic: Double? {
        get {
          return resultMap["average_systolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_systolic")
        }
      }

      public var averageDiastolic: Double? {
        get {
          return resultMap["average_diastolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_diastolic")
        }
      }

      public var averageBreathingRate: Double? {
        get {
          return resultMap["average_breathing_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_breathing_rate")
        }
      }

      public var averageSpo2: Double? {
        get {
          return resultMap["average_spo2"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_spo2")
        }
      }
    }
  }
}

public final class ListExerciseDataQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query listExerciseData($filter: TableExerciseDataFilterInput, $limit: Int) {
      listExerciseData(filter: $filter, limit: $limit) {
        __typename
        items {
          __typename
          id
          user_id
          start_time
          end_time
          average_pulse_rate
          total_step_count
          total_calories_burnt
          total_distance_covered
          average_skin_temprature
          average_systolic
          average_diastolic
          average_breathing_rate
          average_spo2
        }
      }
    }
    """

  public let operationName: String = "listExerciseData"

  public var filter: TableExerciseDataFilterInput?
  public var limit: Int?

  public init(filter: TableExerciseDataFilterInput? = nil, limit: Int? = nil) {
    self.filter = filter
    self.limit = limit
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listExerciseData", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit")], type: .object(ListExerciseDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(listExerciseData: ListExerciseDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "listExerciseData": listExerciseData.flatMap { (value: ListExerciseDatum) -> ResultMap in value.resultMap }])
    }

    public var listExerciseData: ListExerciseDatum? {
      get {
        return (resultMap["listExerciseData"] as? ResultMap).flatMap { ListExerciseDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "listExerciseData")
      }
    }

    public struct ListExerciseDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ExerciseDataConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "ExerciseDataConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["ExerciseData"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("start_time", type: .nonNull(.scalar(Double.self))),
          GraphQLField("end_time", type: .nonNull(.scalar(Double.self))),
          GraphQLField("average_pulse_rate", type: .scalar(Double.self)),
          GraphQLField("total_step_count", type: .scalar(Double.self)),
          GraphQLField("total_calories_burnt", type: .scalar(Double.self)),
          GraphQLField("total_distance_covered", type: .scalar(Double.self)),
          GraphQLField("average_skin_temprature", type: .scalar(Double.self)),
          GraphQLField("average_systolic", type: .scalar(Double.self)),
          GraphQLField("average_diastolic", type: .scalar(Double.self)),
          GraphQLField("average_breathing_rate", type: .scalar(Double.self)),
          GraphQLField("average_spo2", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, userId: String, startTime: Double, endTime: Double, averagePulseRate: Double? = nil, totalStepCount: Double? = nil, totalCaloriesBurnt: Double? = nil, totalDistanceCovered: Double? = nil, averageSkinTemprature: Double? = nil, averageSystolic: Double? = nil, averageDiastolic: Double? = nil, averageBreathingRate: Double? = nil, averageSpo2: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "ExerciseData", "id": id, "user_id": userId, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "total_step_count": totalStepCount, "total_calories_burnt": totalCaloriesBurnt, "total_distance_covered": totalDistanceCovered, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate, "average_spo2": averageSpo2])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return resultMap["id"]! as! GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var startTime: Double {
          get {
            return resultMap["start_time"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "start_time")
          }
        }

        public var endTime: Double {
          get {
            return resultMap["end_time"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "end_time")
          }
        }

        public var averagePulseRate: Double? {
          get {
            return resultMap["average_pulse_rate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_pulse_rate")
          }
        }

        public var totalStepCount: Double? {
          get {
            return resultMap["total_step_count"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "total_step_count")
          }
        }

        public var totalCaloriesBurnt: Double? {
          get {
            return resultMap["total_calories_burnt"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "total_calories_burnt")
          }
        }

        public var totalDistanceCovered: Double? {
          get {
            return resultMap["total_distance_covered"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "total_distance_covered")
          }
        }

        public var averageSkinTemprature: Double? {
          get {
            return resultMap["average_skin_temprature"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_skin_temprature")
          }
        }

        public var averageSystolic: Double? {
          get {
            return resultMap["average_systolic"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_systolic")
          }
        }

        public var averageDiastolic: Double? {
          get {
            return resultMap["average_diastolic"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_diastolic")
          }
        }

        public var averageBreathingRate: Double? {
          get {
            return resultMap["average_breathing_rate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_breathing_rate")
          }
        }

        public var averageSpo2: Double? {
          get {
            return resultMap["average_spo2"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_spo2")
          }
        }
      }
    }
  }
}

public final class CreateBreathingRateMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createBreathingRate($CreateBreathingRateInput: CreateBreathingRateInput!) {
      createBreathingRate(input: $CreateBreathingRateInput) {
        __typename
        user_id
        breathing_rate_timestamp
        breathing_rate_value
      }
    }
    """

  public let operationName: String = "createBreathingRate"

  public var CreateBreathingRateInput: CreateBreathingRateInput

  public init(CreateBreathingRateInput: CreateBreathingRateInput) {
    self.CreateBreathingRateInput = CreateBreathingRateInput
  }

  public var variables: GraphQLMap? {
    return ["CreateBreathingRateInput": CreateBreathingRateInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createBreathingRate", arguments: ["input": GraphQLVariable("CreateBreathingRateInput")], type: .object(CreateBreathingRate.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createBreathingRate: CreateBreathingRate? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createBreathingRate": createBreathingRate.flatMap { (value: CreateBreathingRate) -> ResultMap in value.resultMap }])
    }

    public var createBreathingRate: CreateBreathingRate? {
      get {
        return (resultMap["createBreathingRate"] as? ResultMap).flatMap { CreateBreathingRate(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createBreathingRate")
      }
    }

    public struct CreateBreathingRate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BreathingRate"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("breathing_rate_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("breathing_rate_value", type: .nonNull(.scalar(Double.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, breathingRateTimestamp: Double, breathingRateValue: Double) {
        self.init(unsafeResultMap: ["__typename": "BreathingRate", "user_id": userId, "breathing_rate_timestamp": breathingRateTimestamp, "breathing_rate_value": breathingRateValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var breathingRateTimestamp: Double {
        get {
          return resultMap["breathing_rate_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_timestamp")
        }
      }

      public var breathingRateValue: Double {
        get {
          return resultMap["breathing_rate_value"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_value")
        }
      }
    }
  }
}

public final class CreateMultipleBreathingRateMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createMultipleBreathingRate($CreateBreathingRateInput: [CreateBreathingRateInput!]!) {
      createMultipleBreathingRate(input: $CreateBreathingRateInput) {
        __typename
        user_id
        breathing_rate_timestamp
        breathing_rate_value
      }
    }
    """

  public let operationName: String = "createMultipleBreathingRate"

  public var CreateBreathingRateInput: [CreateBreathingRateInput]

  public init(CreateBreathingRateInput: [CreateBreathingRateInput]) {
    self.CreateBreathingRateInput = CreateBreathingRateInput
  }

  public var variables: GraphQLMap? {
    return ["CreateBreathingRateInput": CreateBreathingRateInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createMultipleBreathingRate", arguments: ["input": GraphQLVariable("CreateBreathingRateInput")], type: .list(.object(CreateMultipleBreathingRate.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createMultipleBreathingRate: [CreateMultipleBreathingRate?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createMultipleBreathingRate": createMultipleBreathingRate.flatMap { (value: [CreateMultipleBreathingRate?]) -> [ResultMap?] in value.map { (value: CreateMultipleBreathingRate?) -> ResultMap? in value.flatMap { (value: CreateMultipleBreathingRate) -> ResultMap in value.resultMap } } }])
    }

    public var createMultipleBreathingRate: [CreateMultipleBreathingRate?]? {
      get {
        return (resultMap["createMultipleBreathingRate"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [CreateMultipleBreathingRate?] in value.map { (value: ResultMap?) -> CreateMultipleBreathingRate? in value.flatMap { (value: ResultMap) -> CreateMultipleBreathingRate in CreateMultipleBreathingRate(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [CreateMultipleBreathingRate?]) -> [ResultMap?] in value.map { (value: CreateMultipleBreathingRate?) -> ResultMap? in value.flatMap { (value: CreateMultipleBreathingRate) -> ResultMap in value.resultMap } } }, forKey: "createMultipleBreathingRate")
      }
    }

    public struct CreateMultipleBreathingRate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BreathingRate"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("breathing_rate_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("breathing_rate_value", type: .nonNull(.scalar(Double.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, breathingRateTimestamp: Double, breathingRateValue: Double) {
        self.init(unsafeResultMap: ["__typename": "BreathingRate", "user_id": userId, "breathing_rate_timestamp": breathingRateTimestamp, "breathing_rate_value": breathingRateValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var breathingRateTimestamp: Double {
        get {
          return resultMap["breathing_rate_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_timestamp")
        }
      }

      public var breathingRateValue: Double {
        get {
          return resultMap["breathing_rate_value"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_value")
        }
      }
    }
  }
}

public final class UpdateBreathingRateMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateBreathingRate($UpdateBreathingRateInput: UpdateBreathingRateInput!) {
      updateBreathingRate(input: $UpdateBreathingRateInput) {
        __typename
        user_id
        breathing_rate_timestamp
        breathing_rate_value
      }
    }
    """

  public let operationName: String = "updateBreathingRate"

  public var UpdateBreathingRateInput: UpdateBreathingRateInput

  public init(UpdateBreathingRateInput: UpdateBreathingRateInput) {
    self.UpdateBreathingRateInput = UpdateBreathingRateInput
  }

  public var variables: GraphQLMap? {
    return ["UpdateBreathingRateInput": UpdateBreathingRateInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateBreathingRate", arguments: ["input": GraphQLVariable("UpdateBreathingRateInput")], type: .object(UpdateBreathingRate.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateBreathingRate: UpdateBreathingRate? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateBreathingRate": updateBreathingRate.flatMap { (value: UpdateBreathingRate) -> ResultMap in value.resultMap }])
    }

    public var updateBreathingRate: UpdateBreathingRate? {
      get {
        return (resultMap["updateBreathingRate"] as? ResultMap).flatMap { UpdateBreathingRate(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateBreathingRate")
      }
    }

    public struct UpdateBreathingRate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BreathingRate"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("breathing_rate_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("breathing_rate_value", type: .nonNull(.scalar(Double.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, breathingRateTimestamp: Double, breathingRateValue: Double) {
        self.init(unsafeResultMap: ["__typename": "BreathingRate", "user_id": userId, "breathing_rate_timestamp": breathingRateTimestamp, "breathing_rate_value": breathingRateValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var breathingRateTimestamp: Double {
        get {
          return resultMap["breathing_rate_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_timestamp")
        }
      }

      public var breathingRateValue: Double {
        get {
          return resultMap["breathing_rate_value"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_value")
        }
      }
    }
  }
}

public final class DeleteBreathingRateMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deleteBreathingRate($DeleteBreathingRateInput: DeleteBreathingRateInput!) {
      deleteBreathingRate(input: $DeleteBreathingRateInput) {
        __typename
        user_id
        breathing_rate_timestamp
        breathing_rate_value
      }
    }
    """

  public let operationName: String = "deleteBreathingRate"

  public var DeleteBreathingRateInput: DeleteBreathingRateInput

  public init(DeleteBreathingRateInput: DeleteBreathingRateInput) {
    self.DeleteBreathingRateInput = DeleteBreathingRateInput
  }

  public var variables: GraphQLMap? {
    return ["DeleteBreathingRateInput": DeleteBreathingRateInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteBreathingRate", arguments: ["input": GraphQLVariable("DeleteBreathingRateInput")], type: .object(DeleteBreathingRate.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteBreathingRate: DeleteBreathingRate? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteBreathingRate": deleteBreathingRate.flatMap { (value: DeleteBreathingRate) -> ResultMap in value.resultMap }])
    }

    public var deleteBreathingRate: DeleteBreathingRate? {
      get {
        return (resultMap["deleteBreathingRate"] as? ResultMap).flatMap { DeleteBreathingRate(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteBreathingRate")
      }
    }

    public struct DeleteBreathingRate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BreathingRate"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("breathing_rate_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("breathing_rate_value", type: .nonNull(.scalar(Double.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, breathingRateTimestamp: Double, breathingRateValue: Double) {
        self.init(unsafeResultMap: ["__typename": "BreathingRate", "user_id": userId, "breathing_rate_timestamp": breathingRateTimestamp, "breathing_rate_value": breathingRateValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var breathingRateTimestamp: Double {
        get {
          return resultMap["breathing_rate_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_timestamp")
        }
      }

      public var breathingRateValue: Double {
        get {
          return resultMap["breathing_rate_value"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_value")
        }
      }
    }
  }
}

public final class GetBreathingRateQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getBreathingRate($breathing_rate_timestamp: Float!) {
      getBreathingRate(breathing_rate_timestamp: $breathing_rate_timestamp) {
        __typename
        user_id
        breathing_rate_timestamp
        breathing_rate_value
      }
    }
    """

  public let operationName: String = "getBreathingRate"

  public var breathing_rate_timestamp: Double

  public init(breathing_rate_timestamp: Double) {
    self.breathing_rate_timestamp = breathing_rate_timestamp
  }

  public var variables: GraphQLMap? {
    return ["breathing_rate_timestamp": breathing_rate_timestamp]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getBreathingRate", arguments: ["breathing_rate_timestamp": GraphQLVariable("breathing_rate_timestamp")], type: .object(GetBreathingRate.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getBreathingRate: GetBreathingRate? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getBreathingRate": getBreathingRate.flatMap { (value: GetBreathingRate) -> ResultMap in value.resultMap }])
    }

    public var getBreathingRate: GetBreathingRate? {
      get {
        return (resultMap["getBreathingRate"] as? ResultMap).flatMap { GetBreathingRate(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getBreathingRate")
      }
    }

    public struct GetBreathingRate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BreathingRate"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("breathing_rate_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("breathing_rate_value", type: .nonNull(.scalar(Double.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, breathingRateTimestamp: Double, breathingRateValue: Double) {
        self.init(unsafeResultMap: ["__typename": "BreathingRate", "user_id": userId, "breathing_rate_timestamp": breathingRateTimestamp, "breathing_rate_value": breathingRateValue])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var breathingRateTimestamp: Double {
        get {
          return resultMap["breathing_rate_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_timestamp")
        }
      }

      public var breathingRateValue: Double {
        get {
          return resultMap["breathing_rate_value"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_value")
        }
      }
    }
  }
}

public final class ListBreathingRatesQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query listBreathingRates {
      listBreathingRates {
        __typename
        items {
          __typename
          user_id
          breathing_rate_timestamp
          breathing_rate_value
        }
      }
    }
    """

  public let operationName: String = "listBreathingRates"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listBreathingRates", type: .object(ListBreathingRate.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(listBreathingRates: ListBreathingRate? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "listBreathingRates": listBreathingRates.flatMap { (value: ListBreathingRate) -> ResultMap in value.resultMap }])
    }

    public var listBreathingRates: ListBreathingRate? {
      get {
        return (resultMap["listBreathingRates"] as? ResultMap).flatMap { ListBreathingRate(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "listBreathingRates")
      }
    }

    public struct ListBreathingRate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BreathingRateConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "BreathingRateConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["BreathingRate"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("breathing_rate_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("breathing_rate_value", type: .nonNull(.scalar(Double.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, breathingRateTimestamp: Double, breathingRateValue: Double) {
          self.init(unsafeResultMap: ["__typename": "BreathingRate", "user_id": userId, "breathing_rate_timestamp": breathingRateTimestamp, "breathing_rate_value": breathingRateValue])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var breathingRateTimestamp: Double {
          get {
            return resultMap["breathing_rate_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "breathing_rate_timestamp")
          }
        }

        public var breathingRateValue: Double {
          get {
            return resultMap["breathing_rate_value"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "breathing_rate_value")
          }
        }
      }
    }
  }
}

public final class QueryBreathingRateByTimestampRangeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query queryBreathingRateByTimestampRange($from_ts: Float, $to_ts: Float) {
      queryBreathingRateByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          breathing_rate_timestamp
          breathing_rate_value
        }
      }
    }
    """

  public let operationName: String = "queryBreathingRateByTimestampRange"

  public var from_ts: Double?
  public var to_ts: Double?

  public init(from_ts: Double? = nil, to_ts: Double? = nil) {
    self.from_ts = from_ts
    self.to_ts = to_ts
  }

  public var variables: GraphQLMap? {
    return ["from_ts": from_ts, "to_ts": to_ts]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("queryBreathingRateByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QueryBreathingRateByTimestampRange.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(queryBreathingRateByTimestampRange: QueryBreathingRateByTimestampRange? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "queryBreathingRateByTimestampRange": queryBreathingRateByTimestampRange.flatMap { (value: QueryBreathingRateByTimestampRange) -> ResultMap in value.resultMap }])
    }

    public var queryBreathingRateByTimestampRange: QueryBreathingRateByTimestampRange? {
      get {
        return (resultMap["queryBreathingRateByTimestampRange"] as? ResultMap).flatMap { QueryBreathingRateByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryBreathingRateByTimestampRange")
      }
    }

    public struct QueryBreathingRateByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BreathingRateConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "BreathingRateConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["BreathingRate"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("breathing_rate_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("breathing_rate_value", type: .nonNull(.scalar(Double.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, breathingRateTimestamp: Double, breathingRateValue: Double) {
          self.init(unsafeResultMap: ["__typename": "BreathingRate", "user_id": userId, "breathing_rate_timestamp": breathingRateTimestamp, "breathing_rate_value": breathingRateValue])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var breathingRateTimestamp: Double {
          get {
            return resultMap["breathing_rate_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "breathing_rate_timestamp")
          }
        }

        public var breathingRateValue: Double {
          get {
            return resultMap["breathing_rate_value"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "breathing_rate_value")
          }
        }
      }
    }
  }
}

public final class CreatePauseDataMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createPauseData($CreatePauseDataInput: CreatePauseDataInput!) {
      createPauseData(input: $CreatePauseDataInput) {
        __typename
        id
        user_id
        start_time
        end_time
        average_pulse_rate
        average_skin_temprature
        average_systolic
        average_diastolic
        average_breathing_rate
      }
    }
    """

  public let operationName: String = "createPauseData"

  public var CreatePauseDataInput: CreatePauseDataInput

  public init(CreatePauseDataInput: CreatePauseDataInput) {
    self.CreatePauseDataInput = CreatePauseDataInput
  }

  public var variables: GraphQLMap? {
    return ["CreatePauseDataInput": CreatePauseDataInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createPauseData", arguments: ["input": GraphQLVariable("CreatePauseDataInput")], type: .object(CreatePauseDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createPauseData: CreatePauseDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createPauseData": createPauseData.flatMap { (value: CreatePauseDatum) -> ResultMap in value.resultMap }])
    }

    public var createPauseData: CreatePauseDatum? {
      get {
        return (resultMap["createPauseData"] as? ResultMap).flatMap { CreatePauseDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createPauseData")
      }
    }

    public struct CreatePauseDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PauseData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("start_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("end_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("average_pulse_rate", type: .scalar(Double.self)),
        GraphQLField("average_skin_temprature", type: .scalar(Double.self)),
        GraphQLField("average_systolic", type: .scalar(Double.self)),
        GraphQLField("average_diastolic", type: .scalar(Double.self)),
        GraphQLField("average_breathing_rate", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, userId: String, startTime: Double, endTime: Double, averagePulseRate: Double? = nil, averageSkinTemprature: Double? = nil, averageSystolic: Double? = nil, averageDiastolic: Double? = nil, averageBreathingRate: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "PauseData", "id": id, "user_id": userId, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var startTime: Double {
        get {
          return resultMap["start_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "start_time")
        }
      }

      public var endTime: Double {
        get {
          return resultMap["end_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "end_time")
        }
      }

      public var averagePulseRate: Double? {
        get {
          return resultMap["average_pulse_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_pulse_rate")
        }
      }

      public var averageSkinTemprature: Double? {
        get {
          return resultMap["average_skin_temprature"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_skin_temprature")
        }
      }

      public var averageSystolic: Double? {
        get {
          return resultMap["average_systolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_systolic")
        }
      }

      public var averageDiastolic: Double? {
        get {
          return resultMap["average_diastolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_diastolic")
        }
      }

      public var averageBreathingRate: Double? {
        get {
          return resultMap["average_breathing_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_breathing_rate")
        }
      }
    }
  }
}

public final class UpdatePauseDataMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updatePauseData($UpdatePauseDataInput: UpdatePauseDataInput!) {
      updatePauseData(input: $UpdatePauseDataInput) {
        __typename
        id
        user_id
        start_time
        end_time
        average_pulse_rate
        average_skin_temprature
        average_systolic
        average_diastolic
        average_breathing_rate
      }
    }
    """

  public let operationName: String = "updatePauseData"

  public var UpdatePauseDataInput: UpdatePauseDataInput

  public init(UpdatePauseDataInput: UpdatePauseDataInput) {
    self.UpdatePauseDataInput = UpdatePauseDataInput
  }

  public var variables: GraphQLMap? {
    return ["UpdatePauseDataInput": UpdatePauseDataInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updatePauseData", arguments: ["input": GraphQLVariable("UpdatePauseDataInput")], type: .object(UpdatePauseDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updatePauseData: UpdatePauseDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updatePauseData": updatePauseData.flatMap { (value: UpdatePauseDatum) -> ResultMap in value.resultMap }])
    }

    public var updatePauseData: UpdatePauseDatum? {
      get {
        return (resultMap["updatePauseData"] as? ResultMap).flatMap { UpdatePauseDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updatePauseData")
      }
    }

    public struct UpdatePauseDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PauseData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("start_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("end_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("average_pulse_rate", type: .scalar(Double.self)),
        GraphQLField("average_skin_temprature", type: .scalar(Double.self)),
        GraphQLField("average_systolic", type: .scalar(Double.self)),
        GraphQLField("average_diastolic", type: .scalar(Double.self)),
        GraphQLField("average_breathing_rate", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, userId: String, startTime: Double, endTime: Double, averagePulseRate: Double? = nil, averageSkinTemprature: Double? = nil, averageSystolic: Double? = nil, averageDiastolic: Double? = nil, averageBreathingRate: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "PauseData", "id": id, "user_id": userId, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var startTime: Double {
        get {
          return resultMap["start_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "start_time")
        }
      }

      public var endTime: Double {
        get {
          return resultMap["end_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "end_time")
        }
      }

      public var averagePulseRate: Double? {
        get {
          return resultMap["average_pulse_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_pulse_rate")
        }
      }

      public var averageSkinTemprature: Double? {
        get {
          return resultMap["average_skin_temprature"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_skin_temprature")
        }
      }

      public var averageSystolic: Double? {
        get {
          return resultMap["average_systolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_systolic")
        }
      }

      public var averageDiastolic: Double? {
        get {
          return resultMap["average_diastolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_diastolic")
        }
      }

      public var averageBreathingRate: Double? {
        get {
          return resultMap["average_breathing_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_breathing_rate")
        }
      }
    }
  }
}

public final class DeletePauseDataMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deletePauseData($DeletePauseDataInput: DeletePauseDataInput!) {
      deletePauseData(input: $DeletePauseDataInput) {
        __typename
        id
        user_id
        start_time
        end_time
        average_pulse_rate
        average_skin_temprature
        average_systolic
        average_diastolic
      }
    }
    """

  public let operationName: String = "deletePauseData"

  public var DeletePauseDataInput: DeletePauseDataInput

  public init(DeletePauseDataInput: DeletePauseDataInput) {
    self.DeletePauseDataInput = DeletePauseDataInput
  }

  public var variables: GraphQLMap? {
    return ["DeletePauseDataInput": DeletePauseDataInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deletePauseData", arguments: ["input": GraphQLVariable("DeletePauseDataInput")], type: .object(DeletePauseDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deletePauseData: DeletePauseDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deletePauseData": deletePauseData.flatMap { (value: DeletePauseDatum) -> ResultMap in value.resultMap }])
    }

    public var deletePauseData: DeletePauseDatum? {
      get {
        return (resultMap["deletePauseData"] as? ResultMap).flatMap { DeletePauseDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deletePauseData")
      }
    }

    public struct DeletePauseDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PauseData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("start_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("end_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("average_pulse_rate", type: .scalar(Double.self)),
        GraphQLField("average_skin_temprature", type: .scalar(Double.self)),
        GraphQLField("average_systolic", type: .scalar(Double.self)),
        GraphQLField("average_diastolic", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, userId: String, startTime: Double, endTime: Double, averagePulseRate: Double? = nil, averageSkinTemprature: Double? = nil, averageSystolic: Double? = nil, averageDiastolic: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "PauseData", "id": id, "user_id": userId, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var startTime: Double {
        get {
          return resultMap["start_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "start_time")
        }
      }

      public var endTime: Double {
        get {
          return resultMap["end_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "end_time")
        }
      }

      public var averagePulseRate: Double? {
        get {
          return resultMap["average_pulse_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_pulse_rate")
        }
      }

      public var averageSkinTemprature: Double? {
        get {
          return resultMap["average_skin_temprature"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_skin_temprature")
        }
      }

      public var averageSystolic: Double? {
        get {
          return resultMap["average_systolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_systolic")
        }
      }

      public var averageDiastolic: Double? {
        get {
          return resultMap["average_diastolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_diastolic")
        }
      }
    }
  }
}

public final class GetPauseDataQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getPauseData($id: ID!) {
      getPauseData(id: $id) {
        __typename
        id
        user_id
        start_time
        end_time
        average_pulse_rate
        average_skin_temprature
        average_systolic
        average_diastolic
        average_breathing_rate
      }
    }
    """

  public let operationName: String = "getPauseData"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getPauseData", arguments: ["id": GraphQLVariable("id")], type: .object(GetPauseDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getPauseData: GetPauseDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getPauseData": getPauseData.flatMap { (value: GetPauseDatum) -> ResultMap in value.resultMap }])
    }

    public var getPauseData: GetPauseDatum? {
      get {
        return (resultMap["getPauseData"] as? ResultMap).flatMap { GetPauseDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getPauseData")
      }
    }

    public struct GetPauseDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PauseData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("start_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("end_time", type: .nonNull(.scalar(Double.self))),
        GraphQLField("average_pulse_rate", type: .scalar(Double.self)),
        GraphQLField("average_skin_temprature", type: .scalar(Double.self)),
        GraphQLField("average_systolic", type: .scalar(Double.self)),
        GraphQLField("average_diastolic", type: .scalar(Double.self)),
        GraphQLField("average_breathing_rate", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, userId: String, startTime: Double, endTime: Double, averagePulseRate: Double? = nil, averageSkinTemprature: Double? = nil, averageSystolic: Double? = nil, averageDiastolic: Double? = nil, averageBreathingRate: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "PauseData", "id": id, "user_id": userId, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var startTime: Double {
        get {
          return resultMap["start_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "start_time")
        }
      }

      public var endTime: Double {
        get {
          return resultMap["end_time"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "end_time")
        }
      }

      public var averagePulseRate: Double? {
        get {
          return resultMap["average_pulse_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_pulse_rate")
        }
      }

      public var averageSkinTemprature: Double? {
        get {
          return resultMap["average_skin_temprature"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_skin_temprature")
        }
      }

      public var averageSystolic: Double? {
        get {
          return resultMap["average_systolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_systolic")
        }
      }

      public var averageDiastolic: Double? {
        get {
          return resultMap["average_diastolic"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_diastolic")
        }
      }

      public var averageBreathingRate: Double? {
        get {
          return resultMap["average_breathing_rate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_breathing_rate")
        }
      }
    }
  }
}

public final class ListPauseDataQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query listPauseData($filter: TablePauseDataFilterInput, $limit: Int) {
      listPauseData(filter: $filter, limit: $limit) {
        __typename
        items {
          __typename
          id
          user_id
          start_time
          end_time
          average_pulse_rate
          average_skin_temprature
          average_systolic
          average_diastolic
          average_breathing_rate
        }
      }
    }
    """

  public let operationName: String = "listPauseData"

  public var filter: TablePauseDataFilterInput?
  public var limit: Int?

  public init(filter: TablePauseDataFilterInput? = nil, limit: Int? = nil) {
    self.filter = filter
    self.limit = limit
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listPauseData", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit")], type: .object(ListPauseDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(listPauseData: ListPauseDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "listPauseData": listPauseData.flatMap { (value: ListPauseDatum) -> ResultMap in value.resultMap }])
    }

    public var listPauseData: ListPauseDatum? {
      get {
        return (resultMap["listPauseData"] as? ResultMap).flatMap { ListPauseDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "listPauseData")
      }
    }

    public struct ListPauseDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PauseDataConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "PauseDataConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["PauseData"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("start_time", type: .nonNull(.scalar(Double.self))),
          GraphQLField("end_time", type: .nonNull(.scalar(Double.self))),
          GraphQLField("average_pulse_rate", type: .scalar(Double.self)),
          GraphQLField("average_skin_temprature", type: .scalar(Double.self)),
          GraphQLField("average_systolic", type: .scalar(Double.self)),
          GraphQLField("average_diastolic", type: .scalar(Double.self)),
          GraphQLField("average_breathing_rate", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, userId: String, startTime: Double, endTime: Double, averagePulseRate: Double? = nil, averageSkinTemprature: Double? = nil, averageSystolic: Double? = nil, averageDiastolic: Double? = nil, averageBreathingRate: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "PauseData", "id": id, "user_id": userId, "start_time": startTime, "end_time": endTime, "average_pulse_rate": averagePulseRate, "average_skin_temprature": averageSkinTemprature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return resultMap["id"]! as! GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var startTime: Double {
          get {
            return resultMap["start_time"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "start_time")
          }
        }

        public var endTime: Double {
          get {
            return resultMap["end_time"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "end_time")
          }
        }

        public var averagePulseRate: Double? {
          get {
            return resultMap["average_pulse_rate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_pulse_rate")
          }
        }

        public var averageSkinTemprature: Double? {
          get {
            return resultMap["average_skin_temprature"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_skin_temprature")
          }
        }

        public var averageSystolic: Double? {
          get {
            return resultMap["average_systolic"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_systolic")
          }
        }

        public var averageDiastolic: Double? {
          get {
            return resultMap["average_diastolic"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_diastolic")
          }
        }

        public var averageBreathingRate: Double? {
          get {
            return resultMap["average_breathing_rate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_breathing_rate")
          }
        }
      }
    }
  }
}

public final class CreateUserGoalsMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createUserGoals($CreateUserGoalsInput: CreateUserGoalsInput!) {
      createUserGoals(input: $CreateUserGoalsInput) {
        __typename
        user_id
        step_count
        calories_burned
        systolic_low
        systolic_high
        diastolic_low
        diastolic_high
        temperature_low
        temperature_high
        breathing_rate_low
        breathing_rate_high
        pulse_rate_low
        pulse_rate_high
        sp_o2_low
        sp_o2_high
        sleep_hrs_low
        sleep_hrs_high
      }
    }
    """

  public let operationName: String = "createUserGoals"

  public var CreateUserGoalsInput: CreateUserGoalsInput

  public init(CreateUserGoalsInput: CreateUserGoalsInput) {
    self.CreateUserGoalsInput = CreateUserGoalsInput
  }

  public var variables: GraphQLMap? {
    return ["CreateUserGoalsInput": CreateUserGoalsInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createUserGoals", arguments: ["input": GraphQLVariable("CreateUserGoalsInput")], type: .object(CreateUserGoal.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createUserGoals: CreateUserGoal? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createUserGoals": createUserGoals.flatMap { (value: CreateUserGoal) -> ResultMap in value.resultMap }])
    }

    public var createUserGoals: CreateUserGoal? {
      get {
        return (resultMap["createUserGoals"] as? ResultMap).flatMap { CreateUserGoal(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createUserGoals")
      }
    }

    public struct CreateUserGoal: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserGoals"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("step_count", type: .scalar(Int.self)),
        GraphQLField("calories_burned", type: .scalar(Int.self)),
        GraphQLField("systolic_low", type: .scalar(Int.self)),
        GraphQLField("systolic_high", type: .scalar(Int.self)),
        GraphQLField("diastolic_low", type: .scalar(Int.self)),
        GraphQLField("diastolic_high", type: .scalar(Int.self)),
        GraphQLField("temperature_low", type: .scalar(Int.self)),
        GraphQLField("temperature_high", type: .scalar(Int.self)),
        GraphQLField("breathing_rate_low", type: .scalar(Int.self)),
        GraphQLField("breathing_rate_high", type: .scalar(Int.self)),
        GraphQLField("pulse_rate_low", type: .scalar(Int.self)),
        GraphQLField("pulse_rate_high", type: .scalar(Int.self)),
        GraphQLField("sp_o2_low", type: .scalar(Int.self)),
        GraphQLField("sp_o2_high", type: .scalar(Int.self)),
        GraphQLField("sleep_hrs_low", type: .scalar(Int.self)),
        GraphQLField("sleep_hrs_high", type: .scalar(Int.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, stepCount: Int? = nil, caloriesBurned: Int? = nil, systolicLow: Int? = nil, systolicHigh: Int? = nil, diastolicLow: Int? = nil, diastolicHigh: Int? = nil, temperatureLow: Int? = nil, temperatureHigh: Int? = nil, breathingRateLow: Int? = nil, breathingRateHigh: Int? = nil, pulseRateLow: Int? = nil, pulseRateHigh: Int? = nil, spO2Low: Int? = nil, spO2High: Int? = nil, sleepHrsLow: Int? = nil, sleepHrsHigh: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserGoals", "user_id": userId, "step_count": stepCount, "calories_burned": caloriesBurned, "systolic_low": systolicLow, "systolic_high": systolicHigh, "diastolic_low": diastolicLow, "diastolic_high": diastolicHigh, "temperature_low": temperatureLow, "temperature_high": temperatureHigh, "breathing_rate_low": breathingRateLow, "breathing_rate_high": breathingRateHigh, "pulse_rate_low": pulseRateLow, "pulse_rate_high": pulseRateHigh, "sp_o2_low": spO2Low, "sp_o2_high": spO2High, "sleep_hrs_low": sleepHrsLow, "sleep_hrs_high": sleepHrsHigh])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var stepCount: Int? {
        get {
          return resultMap["step_count"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count")
        }
      }

      public var caloriesBurned: Int? {
        get {
          return resultMap["calories_burned"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "calories_burned")
        }
      }

      public var systolicLow: Int? {
        get {
          return resultMap["systolic_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "systolic_low")
        }
      }

      public var systolicHigh: Int? {
        get {
          return resultMap["systolic_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "systolic_high")
        }
      }

      public var diastolicLow: Int? {
        get {
          return resultMap["diastolic_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "diastolic_low")
        }
      }

      public var diastolicHigh: Int? {
        get {
          return resultMap["diastolic_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "diastolic_high")
        }
      }

      public var temperatureLow: Int? {
        get {
          return resultMap["temperature_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_low")
        }
      }

      public var temperatureHigh: Int? {
        get {
          return resultMap["temperature_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_high")
        }
      }

      public var breathingRateLow: Int? {
        get {
          return resultMap["breathing_rate_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_low")
        }
      }

      public var breathingRateHigh: Int? {
        get {
          return resultMap["breathing_rate_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_high")
        }
      }

      public var pulseRateLow: Int? {
        get {
          return resultMap["pulse_rate_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_low")
        }
      }

      public var pulseRateHigh: Int? {
        get {
          return resultMap["pulse_rate_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_high")
        }
      }

      public var spO2Low: Int? {
        get {
          return resultMap["sp_o2_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_low")
        }
      }

      public var spO2High: Int? {
        get {
          return resultMap["sp_o2_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_high")
        }
      }

      public var sleepHrsLow: Int? {
        get {
          return resultMap["sleep_hrs_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_hrs_low")
        }
      }

      public var sleepHrsHigh: Int? {
        get {
          return resultMap["sleep_hrs_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_hrs_high")
        }
      }
    }
  }
}

public final class UpdateUserGoalsMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateUserGoals($UpdateUserGoalsInput: UpdateUserGoalsInput!) {
      updateUserGoals(input: $UpdateUserGoalsInput) {
        __typename
        user_id
        step_count
        calories_burned
        systolic_low
        systolic_high
        diastolic_low
        diastolic_high
        temperature_low
        temperature_high
        breathing_rate_low
        breathing_rate_high
        pulse_rate_low
        pulse_rate_high
        sp_o2_low
        sp_o2_high
        sleep_hrs_low
        sleep_hrs_high
      }
    }
    """

  public let operationName: String = "updateUserGoals"

  public var UpdateUserGoalsInput: UpdateUserGoalsInput

  public init(UpdateUserGoalsInput: UpdateUserGoalsInput) {
    self.UpdateUserGoalsInput = UpdateUserGoalsInput
  }

  public var variables: GraphQLMap? {
    return ["UpdateUserGoalsInput": UpdateUserGoalsInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateUserGoals", arguments: ["input": GraphQLVariable("UpdateUserGoalsInput")], type: .object(UpdateUserGoal.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateUserGoals: UpdateUserGoal? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateUserGoals": updateUserGoals.flatMap { (value: UpdateUserGoal) -> ResultMap in value.resultMap }])
    }

    public var updateUserGoals: UpdateUserGoal? {
      get {
        return (resultMap["updateUserGoals"] as? ResultMap).flatMap { UpdateUserGoal(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateUserGoals")
      }
    }

    public struct UpdateUserGoal: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserGoals"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("step_count", type: .scalar(Int.self)),
        GraphQLField("calories_burned", type: .scalar(Int.self)),
        GraphQLField("systolic_low", type: .scalar(Int.self)),
        GraphQLField("systolic_high", type: .scalar(Int.self)),
        GraphQLField("diastolic_low", type: .scalar(Int.self)),
        GraphQLField("diastolic_high", type: .scalar(Int.self)),
        GraphQLField("temperature_low", type: .scalar(Int.self)),
        GraphQLField("temperature_high", type: .scalar(Int.self)),
        GraphQLField("breathing_rate_low", type: .scalar(Int.self)),
        GraphQLField("breathing_rate_high", type: .scalar(Int.self)),
        GraphQLField("pulse_rate_low", type: .scalar(Int.self)),
        GraphQLField("pulse_rate_high", type: .scalar(Int.self)),
        GraphQLField("sp_o2_low", type: .scalar(Int.self)),
        GraphQLField("sp_o2_high", type: .scalar(Int.self)),
        GraphQLField("sleep_hrs_low", type: .scalar(Int.self)),
        GraphQLField("sleep_hrs_high", type: .scalar(Int.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, stepCount: Int? = nil, caloriesBurned: Int? = nil, systolicLow: Int? = nil, systolicHigh: Int? = nil, diastolicLow: Int? = nil, diastolicHigh: Int? = nil, temperatureLow: Int? = nil, temperatureHigh: Int? = nil, breathingRateLow: Int? = nil, breathingRateHigh: Int? = nil, pulseRateLow: Int? = nil, pulseRateHigh: Int? = nil, spO2Low: Int? = nil, spO2High: Int? = nil, sleepHrsLow: Int? = nil, sleepHrsHigh: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserGoals", "user_id": userId, "step_count": stepCount, "calories_burned": caloriesBurned, "systolic_low": systolicLow, "systolic_high": systolicHigh, "diastolic_low": diastolicLow, "diastolic_high": diastolicHigh, "temperature_low": temperatureLow, "temperature_high": temperatureHigh, "breathing_rate_low": breathingRateLow, "breathing_rate_high": breathingRateHigh, "pulse_rate_low": pulseRateLow, "pulse_rate_high": pulseRateHigh, "sp_o2_low": spO2Low, "sp_o2_high": spO2High, "sleep_hrs_low": sleepHrsLow, "sleep_hrs_high": sleepHrsHigh])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var stepCount: Int? {
        get {
          return resultMap["step_count"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count")
        }
      }

      public var caloriesBurned: Int? {
        get {
          return resultMap["calories_burned"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "calories_burned")
        }
      }

      public var systolicLow: Int? {
        get {
          return resultMap["systolic_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "systolic_low")
        }
      }

      public var systolicHigh: Int? {
        get {
          return resultMap["systolic_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "systolic_high")
        }
      }

      public var diastolicLow: Int? {
        get {
          return resultMap["diastolic_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "diastolic_low")
        }
      }

      public var diastolicHigh: Int? {
        get {
          return resultMap["diastolic_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "diastolic_high")
        }
      }

      public var temperatureLow: Int? {
        get {
          return resultMap["temperature_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_low")
        }
      }

      public var temperatureHigh: Int? {
        get {
          return resultMap["temperature_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_high")
        }
      }

      public var breathingRateLow: Int? {
        get {
          return resultMap["breathing_rate_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_low")
        }
      }

      public var breathingRateHigh: Int? {
        get {
          return resultMap["breathing_rate_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_high")
        }
      }

      public var pulseRateLow: Int? {
        get {
          return resultMap["pulse_rate_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_low")
        }
      }

      public var pulseRateHigh: Int? {
        get {
          return resultMap["pulse_rate_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_high")
        }
      }

      public var spO2Low: Int? {
        get {
          return resultMap["sp_o2_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_low")
        }
      }

      public var spO2High: Int? {
        get {
          return resultMap["sp_o2_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_high")
        }
      }

      public var sleepHrsLow: Int? {
        get {
          return resultMap["sleep_hrs_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_hrs_low")
        }
      }

      public var sleepHrsHigh: Int? {
        get {
          return resultMap["sleep_hrs_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_hrs_high")
        }
      }
    }
  }
}

public final class DeleteUserGoalsMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deleteUserGoals {
      deleteUserGoals {
        __typename
        user_id
        step_count
        calories_burned
        systolic_low
        systolic_high
        diastolic_low
        diastolic_high
        temperature_low
        temperature_high
        breathing_rate_low
        breathing_rate_high
        pulse_rate_low
        pulse_rate_high
        sp_o2_low
        sp_o2_high
        sleep_hrs_low
        sleep_hrs_high
      }
    }
    """

  public let operationName: String = "deleteUserGoals"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteUserGoals", type: .object(DeleteUserGoal.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteUserGoals: DeleteUserGoal? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteUserGoals": deleteUserGoals.flatMap { (value: DeleteUserGoal) -> ResultMap in value.resultMap }])
    }

    public var deleteUserGoals: DeleteUserGoal? {
      get {
        return (resultMap["deleteUserGoals"] as? ResultMap).flatMap { DeleteUserGoal(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteUserGoals")
      }
    }

    public struct DeleteUserGoal: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserGoals"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("step_count", type: .scalar(Int.self)),
        GraphQLField("calories_burned", type: .scalar(Int.self)),
        GraphQLField("systolic_low", type: .scalar(Int.self)),
        GraphQLField("systolic_high", type: .scalar(Int.self)),
        GraphQLField("diastolic_low", type: .scalar(Int.self)),
        GraphQLField("diastolic_high", type: .scalar(Int.self)),
        GraphQLField("temperature_low", type: .scalar(Int.self)),
        GraphQLField("temperature_high", type: .scalar(Int.self)),
        GraphQLField("breathing_rate_low", type: .scalar(Int.self)),
        GraphQLField("breathing_rate_high", type: .scalar(Int.self)),
        GraphQLField("pulse_rate_low", type: .scalar(Int.self)),
        GraphQLField("pulse_rate_high", type: .scalar(Int.self)),
        GraphQLField("sp_o2_low", type: .scalar(Int.self)),
        GraphQLField("sp_o2_high", type: .scalar(Int.self)),
        GraphQLField("sleep_hrs_low", type: .scalar(Int.self)),
        GraphQLField("sleep_hrs_high", type: .scalar(Int.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, stepCount: Int? = nil, caloriesBurned: Int? = nil, systolicLow: Int? = nil, systolicHigh: Int? = nil, diastolicLow: Int? = nil, diastolicHigh: Int? = nil, temperatureLow: Int? = nil, temperatureHigh: Int? = nil, breathingRateLow: Int? = nil, breathingRateHigh: Int? = nil, pulseRateLow: Int? = nil, pulseRateHigh: Int? = nil, spO2Low: Int? = nil, spO2High: Int? = nil, sleepHrsLow: Int? = nil, sleepHrsHigh: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserGoals", "user_id": userId, "step_count": stepCount, "calories_burned": caloriesBurned, "systolic_low": systolicLow, "systolic_high": systolicHigh, "diastolic_low": diastolicLow, "diastolic_high": diastolicHigh, "temperature_low": temperatureLow, "temperature_high": temperatureHigh, "breathing_rate_low": breathingRateLow, "breathing_rate_high": breathingRateHigh, "pulse_rate_low": pulseRateLow, "pulse_rate_high": pulseRateHigh, "sp_o2_low": spO2Low, "sp_o2_high": spO2High, "sleep_hrs_low": sleepHrsLow, "sleep_hrs_high": sleepHrsHigh])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var stepCount: Int? {
        get {
          return resultMap["step_count"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count")
        }
      }

      public var caloriesBurned: Int? {
        get {
          return resultMap["calories_burned"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "calories_burned")
        }
      }

      public var systolicLow: Int? {
        get {
          return resultMap["systolic_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "systolic_low")
        }
      }

      public var systolicHigh: Int? {
        get {
          return resultMap["systolic_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "systolic_high")
        }
      }

      public var diastolicLow: Int? {
        get {
          return resultMap["diastolic_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "diastolic_low")
        }
      }

      public var diastolicHigh: Int? {
        get {
          return resultMap["diastolic_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "diastolic_high")
        }
      }

      public var temperatureLow: Int? {
        get {
          return resultMap["temperature_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_low")
        }
      }

      public var temperatureHigh: Int? {
        get {
          return resultMap["temperature_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_high")
        }
      }

      public var breathingRateLow: Int? {
        get {
          return resultMap["breathing_rate_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_low")
        }
      }

      public var breathingRateHigh: Int? {
        get {
          return resultMap["breathing_rate_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_high")
        }
      }

      public var pulseRateLow: Int? {
        get {
          return resultMap["pulse_rate_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_low")
        }
      }

      public var pulseRateHigh: Int? {
        get {
          return resultMap["pulse_rate_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_high")
        }
      }

      public var spO2Low: Int? {
        get {
          return resultMap["sp_o2_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_low")
        }
      }

      public var spO2High: Int? {
        get {
          return resultMap["sp_o2_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_high")
        }
      }

      public var sleepHrsLow: Int? {
        get {
          return resultMap["sleep_hrs_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_hrs_low")
        }
      }

      public var sleepHrsHigh: Int? {
        get {
          return resultMap["sleep_hrs_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_hrs_high")
        }
      }
    }
  }
}

public final class GetUserGoalsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getUserGoals {
      getUserGoals {
        __typename
        user_id
        step_count
        calories_burned
        systolic_low
        systolic_high
        diastolic_low
        diastolic_high
        temperature_low
        temperature_high
        breathing_rate_low
        breathing_rate_high
        pulse_rate_low
        pulse_rate_high
        sp_o2_low
        sp_o2_high
        sleep_hrs_low
        sleep_hrs_high
      }
    }
    """

  public let operationName: String = "getUserGoals"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getUserGoals", type: .object(GetUserGoal.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getUserGoals: GetUserGoal? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getUserGoals": getUserGoals.flatMap { (value: GetUserGoal) -> ResultMap in value.resultMap }])
    }

    public var getUserGoals: GetUserGoal? {
      get {
        return (resultMap["getUserGoals"] as? ResultMap).flatMap { GetUserGoal(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getUserGoals")
      }
    }

    public struct GetUserGoal: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserGoals"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("step_count", type: .scalar(Int.self)),
        GraphQLField("calories_burned", type: .scalar(Int.self)),
        GraphQLField("systolic_low", type: .scalar(Int.self)),
        GraphQLField("systolic_high", type: .scalar(Int.self)),
        GraphQLField("diastolic_low", type: .scalar(Int.self)),
        GraphQLField("diastolic_high", type: .scalar(Int.self)),
        GraphQLField("temperature_low", type: .scalar(Int.self)),
        GraphQLField("temperature_high", type: .scalar(Int.self)),
        GraphQLField("breathing_rate_low", type: .scalar(Int.self)),
        GraphQLField("breathing_rate_high", type: .scalar(Int.self)),
        GraphQLField("pulse_rate_low", type: .scalar(Int.self)),
        GraphQLField("pulse_rate_high", type: .scalar(Int.self)),
        GraphQLField("sp_o2_low", type: .scalar(Int.self)),
        GraphQLField("sp_o2_high", type: .scalar(Int.self)),
        GraphQLField("sleep_hrs_low", type: .scalar(Int.self)),
        GraphQLField("sleep_hrs_high", type: .scalar(Int.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, stepCount: Int? = nil, caloriesBurned: Int? = nil, systolicLow: Int? = nil, systolicHigh: Int? = nil, diastolicLow: Int? = nil, diastolicHigh: Int? = nil, temperatureLow: Int? = nil, temperatureHigh: Int? = nil, breathingRateLow: Int? = nil, breathingRateHigh: Int? = nil, pulseRateLow: Int? = nil, pulseRateHigh: Int? = nil, spO2Low: Int? = nil, spO2High: Int? = nil, sleepHrsLow: Int? = nil, sleepHrsHigh: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserGoals", "user_id": userId, "step_count": stepCount, "calories_burned": caloriesBurned, "systolic_low": systolicLow, "systolic_high": systolicHigh, "diastolic_low": diastolicLow, "diastolic_high": diastolicHigh, "temperature_low": temperatureLow, "temperature_high": temperatureHigh, "breathing_rate_low": breathingRateLow, "breathing_rate_high": breathingRateHigh, "pulse_rate_low": pulseRateLow, "pulse_rate_high": pulseRateHigh, "sp_o2_low": spO2Low, "sp_o2_high": spO2High, "sleep_hrs_low": sleepHrsLow, "sleep_hrs_high": sleepHrsHigh])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var stepCount: Int? {
        get {
          return resultMap["step_count"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "step_count")
        }
      }

      public var caloriesBurned: Int? {
        get {
          return resultMap["calories_burned"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "calories_burned")
        }
      }

      public var systolicLow: Int? {
        get {
          return resultMap["systolic_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "systolic_low")
        }
      }

      public var systolicHigh: Int? {
        get {
          return resultMap["systolic_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "systolic_high")
        }
      }

      public var diastolicLow: Int? {
        get {
          return resultMap["diastolic_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "diastolic_low")
        }
      }

      public var diastolicHigh: Int? {
        get {
          return resultMap["diastolic_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "diastolic_high")
        }
      }

      public var temperatureLow: Int? {
        get {
          return resultMap["temperature_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_low")
        }
      }

      public var temperatureHigh: Int? {
        get {
          return resultMap["temperature_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "temperature_high")
        }
      }

      public var breathingRateLow: Int? {
        get {
          return resultMap["breathing_rate_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_low")
        }
      }

      public var breathingRateHigh: Int? {
        get {
          return resultMap["breathing_rate_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "breathing_rate_high")
        }
      }

      public var pulseRateLow: Int? {
        get {
          return resultMap["pulse_rate_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_low")
        }
      }

      public var pulseRateHigh: Int? {
        get {
          return resultMap["pulse_rate_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate_high")
        }
      }

      public var spO2Low: Int? {
        get {
          return resultMap["sp_o2_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_low")
        }
      }

      public var spO2High: Int? {
        get {
          return resultMap["sp_o2_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_high")
        }
      }

      public var sleepHrsLow: Int? {
        get {
          return resultMap["sleep_hrs_low"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_hrs_low")
        }
      }

      public var sleepHrsHigh: Int? {
        get {
          return resultMap["sleep_hrs_high"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_hrs_high")
        }
      }
    }
  }
}

public final class ListUserGoalsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query listUserGoals {
      listUserGoals {
        __typename
        items {
          __typename
          user_id
          step_count
          calories_burned
          systolic_low
          systolic_high
          diastolic_low
          diastolic_high
          temperature_low
          temperature_high
          breathing_rate_low
          breathing_rate_high
          pulse_rate_low
          pulse_rate_high
          sp_o2_low
          sp_o2_high
          sleep_hrs_low
          sleep_hrs_high
        }
      }
    }
    """

  public let operationName: String = "listUserGoals"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listUserGoals", type: .object(ListUserGoal.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(listUserGoals: ListUserGoal? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "listUserGoals": listUserGoals.flatMap { (value: ListUserGoal) -> ResultMap in value.resultMap }])
    }

    public var listUserGoals: ListUserGoal? {
      get {
        return (resultMap["listUserGoals"] as? ResultMap).flatMap { ListUserGoal(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "listUserGoals")
      }
    }

    public struct ListUserGoal: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserGoalsConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserGoalsConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserGoals"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("step_count", type: .scalar(Int.self)),
          GraphQLField("calories_burned", type: .scalar(Int.self)),
          GraphQLField("systolic_low", type: .scalar(Int.self)),
          GraphQLField("systolic_high", type: .scalar(Int.self)),
          GraphQLField("diastolic_low", type: .scalar(Int.self)),
          GraphQLField("diastolic_high", type: .scalar(Int.self)),
          GraphQLField("temperature_low", type: .scalar(Int.self)),
          GraphQLField("temperature_high", type: .scalar(Int.self)),
          GraphQLField("breathing_rate_low", type: .scalar(Int.self)),
          GraphQLField("breathing_rate_high", type: .scalar(Int.self)),
          GraphQLField("pulse_rate_low", type: .scalar(Int.self)),
          GraphQLField("pulse_rate_high", type: .scalar(Int.self)),
          GraphQLField("sp_o2_low", type: .scalar(Int.self)),
          GraphQLField("sp_o2_high", type: .scalar(Int.self)),
          GraphQLField("sleep_hrs_low", type: .scalar(Int.self)),
          GraphQLField("sleep_hrs_high", type: .scalar(Int.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, stepCount: Int? = nil, caloriesBurned: Int? = nil, systolicLow: Int? = nil, systolicHigh: Int? = nil, diastolicLow: Int? = nil, diastolicHigh: Int? = nil, temperatureLow: Int? = nil, temperatureHigh: Int? = nil, breathingRateLow: Int? = nil, breathingRateHigh: Int? = nil, pulseRateLow: Int? = nil, pulseRateHigh: Int? = nil, spO2Low: Int? = nil, spO2High: Int? = nil, sleepHrsLow: Int? = nil, sleepHrsHigh: Int? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserGoals", "user_id": userId, "step_count": stepCount, "calories_burned": caloriesBurned, "systolic_low": systolicLow, "systolic_high": systolicHigh, "diastolic_low": diastolicLow, "diastolic_high": diastolicHigh, "temperature_low": temperatureLow, "temperature_high": temperatureHigh, "breathing_rate_low": breathingRateLow, "breathing_rate_high": breathingRateHigh, "pulse_rate_low": pulseRateLow, "pulse_rate_high": pulseRateHigh, "sp_o2_low": spO2Low, "sp_o2_high": spO2High, "sleep_hrs_low": sleepHrsLow, "sleep_hrs_high": sleepHrsHigh])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var stepCount: Int? {
          get {
            return resultMap["step_count"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "step_count")
          }
        }

        public var caloriesBurned: Int? {
          get {
            return resultMap["calories_burned"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "calories_burned")
          }
        }

        public var systolicLow: Int? {
          get {
            return resultMap["systolic_low"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "systolic_low")
          }
        }

        public var systolicHigh: Int? {
          get {
            return resultMap["systolic_high"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "systolic_high")
          }
        }

        public var diastolicLow: Int? {
          get {
            return resultMap["diastolic_low"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "diastolic_low")
          }
        }

        public var diastolicHigh: Int? {
          get {
            return resultMap["diastolic_high"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "diastolic_high")
          }
        }

        public var temperatureLow: Int? {
          get {
            return resultMap["temperature_low"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "temperature_low")
          }
        }

        public var temperatureHigh: Int? {
          get {
            return resultMap["temperature_high"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "temperature_high")
          }
        }

        public var breathingRateLow: Int? {
          get {
            return resultMap["breathing_rate_low"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "breathing_rate_low")
          }
        }

        public var breathingRateHigh: Int? {
          get {
            return resultMap["breathing_rate_high"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "breathing_rate_high")
          }
        }

        public var pulseRateLow: Int? {
          get {
            return resultMap["pulse_rate_low"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "pulse_rate_low")
          }
        }

        public var pulseRateHigh: Int? {
          get {
            return resultMap["pulse_rate_high"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "pulse_rate_high")
          }
        }

        public var spO2Low: Int? {
          get {
            return resultMap["sp_o2_low"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "sp_o2_low")
          }
        }

        public var spO2High: Int? {
          get {
            return resultMap["sp_o2_high"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "sp_o2_high")
          }
        }

        public var sleepHrsLow: Int? {
          get {
            return resultMap["sleep_hrs_low"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_hrs_low")
          }
        }

        public var sleepHrsHigh: Int? {
          get {
            return resultMap["sleep_hrs_high"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_hrs_high")
          }
        }
      }
    }
  }
}

public final class CreateSpO2Mutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createSpO2($CreateSpO2Input: CreateSpO2Input!) {
      createSpO2(input: $CreateSpO2Input) {
        __typename
        user_id
        sp_o2_timestamp
        sp_o2_value
      }
    }
    """

  public let operationName: String = "createSpO2"

  public var CreateSpO2Input: CreateSpO2Input

  public init(CreateSpO2Input: CreateSpO2Input) {
    self.CreateSpO2Input = CreateSpO2Input
  }

  public var variables: GraphQLMap? {
    return ["CreateSpO2Input": CreateSpO2Input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createSpO2", arguments: ["input": GraphQLVariable("CreateSpO2Input")], type: .object(CreateSpO2.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createSpO2: CreateSpO2? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createSpO2": createSpO2.flatMap { (value: CreateSpO2) -> ResultMap in value.resultMap }])
    }

    public var createSpO2: CreateSpO2? {
      get {
        return (resultMap["createSpO2"] as? ResultMap).flatMap { CreateSpO2(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createSpO2")
      }
    }

    public struct CreateSpO2: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SpO2"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("sp_o2_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("sp_o2_value", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, spO2Timestamp: Double, spO2Value: Int) {
        self.init(unsafeResultMap: ["__typename": "SpO2", "user_id": userId, "sp_o2_timestamp": spO2Timestamp, "sp_o2_value": spO2Value])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var spO2Timestamp: Double {
        get {
          return resultMap["sp_o2_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_timestamp")
        }
      }

      public var spO2Value: Int {
        get {
          return resultMap["sp_o2_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_value")
        }
      }
    }
  }
}

public final class CreateMultipleSpO2Mutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createMultipleSpO2($CreateSpO2Input: [CreateSpO2Input!]!) {
      createMultipleSpO2(input: $CreateSpO2Input) {
        __typename
        user_id
        sp_o2_timestamp
        sp_o2_value
      }
    }
    """

  public let operationName: String = "createMultipleSpO2"

  public var CreateSpO2Input: [CreateSpO2Input]

  public init(CreateSpO2Input: [CreateSpO2Input]) {
    self.CreateSpO2Input = CreateSpO2Input
  }

  public var variables: GraphQLMap? {
    return ["CreateSpO2Input": CreateSpO2Input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createMultipleSpO2", arguments: ["input": GraphQLVariable("CreateSpO2Input")], type: .list(.object(CreateMultipleSpO2.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createMultipleSpO2: [CreateMultipleSpO2?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createMultipleSpO2": createMultipleSpO2.flatMap { (value: [CreateMultipleSpO2?]) -> [ResultMap?] in value.map { (value: CreateMultipleSpO2?) -> ResultMap? in value.flatMap { (value: CreateMultipleSpO2) -> ResultMap in value.resultMap } } }])
    }

    public var createMultipleSpO2: [CreateMultipleSpO2?]? {
      get {
        return (resultMap["createMultipleSpO2"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [CreateMultipleSpO2?] in value.map { (value: ResultMap?) -> CreateMultipleSpO2? in value.flatMap { (value: ResultMap) -> CreateMultipleSpO2 in CreateMultipleSpO2(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [CreateMultipleSpO2?]) -> [ResultMap?] in value.map { (value: CreateMultipleSpO2?) -> ResultMap? in value.flatMap { (value: CreateMultipleSpO2) -> ResultMap in value.resultMap } } }, forKey: "createMultipleSpO2")
      }
    }

    public struct CreateMultipleSpO2: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SpO2"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("sp_o2_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("sp_o2_value", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, spO2Timestamp: Double, spO2Value: Int) {
        self.init(unsafeResultMap: ["__typename": "SpO2", "user_id": userId, "sp_o2_timestamp": spO2Timestamp, "sp_o2_value": spO2Value])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var spO2Timestamp: Double {
        get {
          return resultMap["sp_o2_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_timestamp")
        }
      }

      public var spO2Value: Int {
        get {
          return resultMap["sp_o2_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_value")
        }
      }
    }
  }
}

public final class UpdateSpO2Mutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateSpO2($UpdateSpO2Input: UpdateSpO2Input!) {
      updateSpO2(input: $UpdateSpO2Input) {
        __typename
        user_id
        sp_o2_timestamp
        sp_o2_value
      }
    }
    """

  public let operationName: String = "updateSpO2"

  public var UpdateSpO2Input: UpdateSpO2Input

  public init(UpdateSpO2Input: UpdateSpO2Input) {
    self.UpdateSpO2Input = UpdateSpO2Input
  }

  public var variables: GraphQLMap? {
    return ["UpdateSpO2Input": UpdateSpO2Input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateSpO2", arguments: ["input": GraphQLVariable("UpdateSpO2Input")], type: .object(UpdateSpO2.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateSpO2: UpdateSpO2? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateSpO2": updateSpO2.flatMap { (value: UpdateSpO2) -> ResultMap in value.resultMap }])
    }

    public var updateSpO2: UpdateSpO2? {
      get {
        return (resultMap["updateSpO2"] as? ResultMap).flatMap { UpdateSpO2(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateSpO2")
      }
    }

    public struct UpdateSpO2: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SpO2"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("sp_o2_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("sp_o2_value", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, spO2Timestamp: Double, spO2Value: Int) {
        self.init(unsafeResultMap: ["__typename": "SpO2", "user_id": userId, "sp_o2_timestamp": spO2Timestamp, "sp_o2_value": spO2Value])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var spO2Timestamp: Double {
        get {
          return resultMap["sp_o2_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_timestamp")
        }
      }

      public var spO2Value: Int {
        get {
          return resultMap["sp_o2_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_value")
        }
      }
    }
  }
}

public final class DeleteSpO2Mutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deleteSpO2($DeleteSpO2Input: DeleteSpO2Input!) {
      deleteSpO2(input: $DeleteSpO2Input) {
        __typename
        user_id
        sp_o2_timestamp
        sp_o2_value
      }
    }
    """

  public let operationName: String = "deleteSpO2"

  public var DeleteSpO2Input: DeleteSpO2Input

  public init(DeleteSpO2Input: DeleteSpO2Input) {
    self.DeleteSpO2Input = DeleteSpO2Input
  }

  public var variables: GraphQLMap? {
    return ["DeleteSpO2Input": DeleteSpO2Input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteSpO2", arguments: ["input": GraphQLVariable("DeleteSpO2Input")], type: .object(DeleteSpO2.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteSpO2: DeleteSpO2? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteSpO2": deleteSpO2.flatMap { (value: DeleteSpO2) -> ResultMap in value.resultMap }])
    }

    public var deleteSpO2: DeleteSpO2? {
      get {
        return (resultMap["deleteSpO2"] as? ResultMap).flatMap { DeleteSpO2(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteSpO2")
      }
    }

    public struct DeleteSpO2: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SpO2"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("sp_o2_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("sp_o2_value", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, spO2Timestamp: Double, spO2Value: Int) {
        self.init(unsafeResultMap: ["__typename": "SpO2", "user_id": userId, "sp_o2_timestamp": spO2Timestamp, "sp_o2_value": spO2Value])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var spO2Timestamp: Double {
        get {
          return resultMap["sp_o2_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_timestamp")
        }
      }

      public var spO2Value: Int {
        get {
          return resultMap["sp_o2_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_value")
        }
      }
    }
  }
}

public final class GetSpO2Query: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getSpO2($sp_o2_timestamp: Float!) {
      getSpO2(sp_o2_timestamp: $sp_o2_timestamp) {
        __typename
        user_id
        sp_o2_timestamp
        sp_o2_value
      }
    }
    """

  public let operationName: String = "getSpO2"

  public var sp_o2_timestamp: Double

  public init(sp_o2_timestamp: Double) {
    self.sp_o2_timestamp = sp_o2_timestamp
  }

  public var variables: GraphQLMap? {
    return ["sp_o2_timestamp": sp_o2_timestamp]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getSpO2", arguments: ["sp_o2_timestamp": GraphQLVariable("sp_o2_timestamp")], type: .object(GetSpO2.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getSpO2: GetSpO2? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getSpO2": getSpO2.flatMap { (value: GetSpO2) -> ResultMap in value.resultMap }])
    }

    public var getSpO2: GetSpO2? {
      get {
        return (resultMap["getSpO2"] as? ResultMap).flatMap { GetSpO2(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getSpO2")
      }
    }

    public struct GetSpO2: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SpO2"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("sp_o2_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("sp_o2_value", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, spO2Timestamp: Double, spO2Value: Int) {
        self.init(unsafeResultMap: ["__typename": "SpO2", "user_id": userId, "sp_o2_timestamp": spO2Timestamp, "sp_o2_value": spO2Value])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var spO2Timestamp: Double {
        get {
          return resultMap["sp_o2_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_timestamp")
        }
      }

      public var spO2Value: Int {
        get {
          return resultMap["sp_o2_value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sp_o2_value")
        }
      }
    }
  }
}

public final class ListSpO2sQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query listSpO2s {
      listSpO2s {
        __typename
        items {
          __typename
          user_id
          sp_o2_timestamp
          sp_o2_value
        }
      }
    }
    """

  public let operationName: String = "listSpO2s"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listSpO2s", type: .object(ListSpO2.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(listSpO2s: ListSpO2? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "listSpO2s": listSpO2s.flatMap { (value: ListSpO2) -> ResultMap in value.resultMap }])
    }

    public var listSpO2s: ListSpO2? {
      get {
        return (resultMap["listSpO2s"] as? ResultMap).flatMap { ListSpO2(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "listSpO2s")
      }
    }

    public struct ListSpO2: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SpO2Connection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "SpO2Connection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SpO2"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("sp_o2_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("sp_o2_value", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, spO2Timestamp: Double, spO2Value: Int) {
          self.init(unsafeResultMap: ["__typename": "SpO2", "user_id": userId, "sp_o2_timestamp": spO2Timestamp, "sp_o2_value": spO2Value])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var spO2Timestamp: Double {
          get {
            return resultMap["sp_o2_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "sp_o2_timestamp")
          }
        }

        public var spO2Value: Int {
          get {
            return resultMap["sp_o2_value"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "sp_o2_value")
          }
        }
      }
    }
  }
}

public final class QuerySpO2ByTimestampRangeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query querySpO2ByTimestampRange($from_ts: Float, $to_ts: Float) {
      querySpO2ByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          sp_o2_timestamp
          sp_o2_value
        }
      }
    }
    """

  public let operationName: String = "querySpO2ByTimestampRange"

  public var from_ts: Double?
  public var to_ts: Double?

  public init(from_ts: Double? = nil, to_ts: Double? = nil) {
    self.from_ts = from_ts
    self.to_ts = to_ts
  }

  public var variables: GraphQLMap? {
    return ["from_ts": from_ts, "to_ts": to_ts]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("querySpO2ByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QuerySpO2ByTimestampRange.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(querySpO2ByTimestampRange: QuerySpO2ByTimestampRange? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "querySpO2ByTimestampRange": querySpO2ByTimestampRange.flatMap { (value: QuerySpO2ByTimestampRange) -> ResultMap in value.resultMap }])
    }

    public var querySpO2ByTimestampRange: QuerySpO2ByTimestampRange? {
      get {
        return (resultMap["querySpO2ByTimestampRange"] as? ResultMap).flatMap { QuerySpO2ByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "querySpO2ByTimestampRange")
      }
    }

    public struct QuerySpO2ByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SpO2Connection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "SpO2Connection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SpO2"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("sp_o2_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("sp_o2_value", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, spO2Timestamp: Double, spO2Value: Int) {
          self.init(unsafeResultMap: ["__typename": "SpO2", "user_id": userId, "sp_o2_timestamp": spO2Timestamp, "sp_o2_value": spO2Value])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var spO2Timestamp: Double {
          get {
            return resultMap["sp_o2_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "sp_o2_timestamp")
          }
        }

        public var spO2Value: Int {
          get {
            return resultMap["sp_o2_value"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "sp_o2_value")
          }
        }
      }
    }
  }
}

public final class CreateBloodPressureMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createBloodPressure($CreateBloodPressureInput: CreateBloodPressureInput!) {
      createBloodPressure(input: $CreateBloodPressureInput) {
        __typename
        id
        data_timestamp
        diastolic
        systolic
        pulse_rate
      }
    }
    """

  public let operationName: String = "createBloodPressure"

  public var CreateBloodPressureInput: CreateBloodPressureInput

  public init(CreateBloodPressureInput: CreateBloodPressureInput) {
    self.CreateBloodPressureInput = CreateBloodPressureInput
  }

  public var variables: GraphQLMap? {
    return ["CreateBloodPressureInput": CreateBloodPressureInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createBloodPressure", arguments: ["input": GraphQLVariable("CreateBloodPressureInput")], type: .object(CreateBloodPressure.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createBloodPressure: CreateBloodPressure? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createBloodPressure": createBloodPressure.flatMap { (value: CreateBloodPressure) -> ResultMap in value.resultMap }])
    }

    public var createBloodPressure: CreateBloodPressure? {
      get {
        return (resultMap["createBloodPressure"] as? ResultMap).flatMap { CreateBloodPressure(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createBloodPressure")
      }
    }

    public struct CreateBloodPressure: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BloodPressure"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("data_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("diastolic", type: .scalar(Int.self)),
        GraphQLField("systolic", type: .scalar(Int.self)),
        GraphQLField("pulse_rate", type: .scalar(Int.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String, dataTimestamp: Double, diastolic: Int? = nil, systolic: Int? = nil, pulseRate: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "BloodPressure", "id": id, "data_timestamp": dataTimestamp, "diastolic": diastolic, "systolic": systolic, "pulse_rate": pulseRate])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return resultMap["id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var dataTimestamp: Double {
        get {
          return resultMap["data_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "data_timestamp")
        }
      }

      public var diastolic: Int? {
        get {
          return resultMap["diastolic"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "diastolic")
        }
      }

      public var systolic: Int? {
        get {
          return resultMap["systolic"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "systolic")
        }
      }

      public var pulseRate: Int? {
        get {
          return resultMap["pulse_rate"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate")
        }
      }
    }
  }
}

public final class UpdateBloodPressureMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateBloodPressure($UpdateBloodPressureInput: UpdateBloodPressureInput!) {
      updateBloodPressure(input: $UpdateBloodPressureInput) {
        __typename
        id
        data_timestamp
        diastolic
        systolic
        pulse_rate
      }
    }
    """

  public let operationName: String = "updateBloodPressure"

  public var UpdateBloodPressureInput: UpdateBloodPressureInput

  public init(UpdateBloodPressureInput: UpdateBloodPressureInput) {
    self.UpdateBloodPressureInput = UpdateBloodPressureInput
  }

  public var variables: GraphQLMap? {
    return ["UpdateBloodPressureInput": UpdateBloodPressureInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateBloodPressure", arguments: ["input": GraphQLVariable("UpdateBloodPressureInput")], type: .object(UpdateBloodPressure.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateBloodPressure: UpdateBloodPressure? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateBloodPressure": updateBloodPressure.flatMap { (value: UpdateBloodPressure) -> ResultMap in value.resultMap }])
    }

    public var updateBloodPressure: UpdateBloodPressure? {
      get {
        return (resultMap["updateBloodPressure"] as? ResultMap).flatMap { UpdateBloodPressure(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateBloodPressure")
      }
    }

    public struct UpdateBloodPressure: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BloodPressure"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("data_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("diastolic", type: .scalar(Int.self)),
        GraphQLField("systolic", type: .scalar(Int.self)),
        GraphQLField("pulse_rate", type: .scalar(Int.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String, dataTimestamp: Double, diastolic: Int? = nil, systolic: Int? = nil, pulseRate: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "BloodPressure", "id": id, "data_timestamp": dataTimestamp, "diastolic": diastolic, "systolic": systolic, "pulse_rate": pulseRate])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return resultMap["id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var dataTimestamp: Double {
        get {
          return resultMap["data_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "data_timestamp")
        }
      }

      public var diastolic: Int? {
        get {
          return resultMap["diastolic"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "diastolic")
        }
      }

      public var systolic: Int? {
        get {
          return resultMap["systolic"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "systolic")
        }
      }

      public var pulseRate: Int? {
        get {
          return resultMap["pulse_rate"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate")
        }
      }
    }
  }
}

public final class DeleteBloodPressureMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deleteBloodPressure($DeleteBloodPressureInput: DeleteBloodPressureInput!) {
      deleteBloodPressure(input: $DeleteBloodPressureInput) {
        __typename
        id
        data_timestamp
        diastolic
        systolic
        pulse_rate
      }
    }
    """

  public let operationName: String = "deleteBloodPressure"

  public var DeleteBloodPressureInput: DeleteBloodPressureInput

  public init(DeleteBloodPressureInput: DeleteBloodPressureInput) {
    self.DeleteBloodPressureInput = DeleteBloodPressureInput
  }

  public var variables: GraphQLMap? {
    return ["DeleteBloodPressureInput": DeleteBloodPressureInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteBloodPressure", arguments: ["input": GraphQLVariable("DeleteBloodPressureInput")], type: .object(DeleteBloodPressure.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteBloodPressure: DeleteBloodPressure? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteBloodPressure": deleteBloodPressure.flatMap { (value: DeleteBloodPressure) -> ResultMap in value.resultMap }])
    }

    public var deleteBloodPressure: DeleteBloodPressure? {
      get {
        return (resultMap["deleteBloodPressure"] as? ResultMap).flatMap { DeleteBloodPressure(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteBloodPressure")
      }
    }

    public struct DeleteBloodPressure: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BloodPressure"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("data_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("diastolic", type: .scalar(Int.self)),
        GraphQLField("systolic", type: .scalar(Int.self)),
        GraphQLField("pulse_rate", type: .scalar(Int.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String, dataTimestamp: Double, diastolic: Int? = nil, systolic: Int? = nil, pulseRate: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "BloodPressure", "id": id, "data_timestamp": dataTimestamp, "diastolic": diastolic, "systolic": systolic, "pulse_rate": pulseRate])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return resultMap["id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var dataTimestamp: Double {
        get {
          return resultMap["data_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "data_timestamp")
        }
      }

      public var diastolic: Int? {
        get {
          return resultMap["diastolic"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "diastolic")
        }
      }

      public var systolic: Int? {
        get {
          return resultMap["systolic"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "systolic")
        }
      }

      public var pulseRate: Int? {
        get {
          return resultMap["pulse_rate"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate")
        }
      }
    }
  }
}

public final class GetBloodPressureQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getBloodPressure($data_timestamp: Float!) {
      getBloodPressure(data_timestamp: $data_timestamp) {
        __typename
        id
        data_timestamp
        diastolic
        systolic
        pulse_rate
      }
    }
    """

  public let operationName: String = "getBloodPressure"

  public var data_timestamp: Double

  public init(data_timestamp: Double) {
    self.data_timestamp = data_timestamp
  }

  public var variables: GraphQLMap? {
    return ["data_timestamp": data_timestamp]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getBloodPressure", arguments: ["data_timestamp": GraphQLVariable("data_timestamp")], type: .object(GetBloodPressure.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getBloodPressure: GetBloodPressure? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getBloodPressure": getBloodPressure.flatMap { (value: GetBloodPressure) -> ResultMap in value.resultMap }])
    }

    public var getBloodPressure: GetBloodPressure? {
      get {
        return (resultMap["getBloodPressure"] as? ResultMap).flatMap { GetBloodPressure(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getBloodPressure")
      }
    }

    public struct GetBloodPressure: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BloodPressure"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("data_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("diastolic", type: .scalar(Int.self)),
        GraphQLField("systolic", type: .scalar(Int.self)),
        GraphQLField("pulse_rate", type: .scalar(Int.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String, dataTimestamp: Double, diastolic: Int? = nil, systolic: Int? = nil, pulseRate: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "BloodPressure", "id": id, "data_timestamp": dataTimestamp, "diastolic": diastolic, "systolic": systolic, "pulse_rate": pulseRate])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return resultMap["id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var dataTimestamp: Double {
        get {
          return resultMap["data_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "data_timestamp")
        }
      }

      public var diastolic: Int? {
        get {
          return resultMap["diastolic"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "diastolic")
        }
      }

      public var systolic: Int? {
        get {
          return resultMap["systolic"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "systolic")
        }
      }

      public var pulseRate: Int? {
        get {
          return resultMap["pulse_rate"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "pulse_rate")
        }
      }
    }
  }
}

public final class ListBloodPressuresQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query listBloodPressures($filter: TableBloodPressureFilterInput, $limit: Int) {
      listBloodPressures(filter: $filter, limit: $limit) {
        __typename
        items {
          __typename
          id
          data_timestamp
          diastolic
          systolic
          pulse_rate
        }
      }
    }
    """

  public let operationName: String = "listBloodPressures"

  public var filter: TableBloodPressureFilterInput?
  public var limit: Int?

  public init(filter: TableBloodPressureFilterInput? = nil, limit: Int? = nil) {
    self.filter = filter
    self.limit = limit
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listBloodPressures", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit")], type: .object(ListBloodPressure.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(listBloodPressures: ListBloodPressure? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "listBloodPressures": listBloodPressures.flatMap { (value: ListBloodPressure) -> ResultMap in value.resultMap }])
    }

    public var listBloodPressures: ListBloodPressure? {
      get {
        return (resultMap["listBloodPressures"] as? ResultMap).flatMap { ListBloodPressure(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "listBloodPressures")
      }
    }

    public struct ListBloodPressure: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BloodPressureConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "BloodPressureConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["BloodPressure"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(String.self))),
          GraphQLField("data_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("diastolic", type: .scalar(Int.self)),
          GraphQLField("systolic", type: .scalar(Int.self)),
          GraphQLField("pulse_rate", type: .scalar(Int.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String, dataTimestamp: Double, diastolic: Int? = nil, systolic: Int? = nil, pulseRate: Int? = nil) {
          self.init(unsafeResultMap: ["__typename": "BloodPressure", "id": id, "data_timestamp": dataTimestamp, "diastolic": diastolic, "systolic": systolic, "pulse_rate": pulseRate])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String {
          get {
            return resultMap["id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var dataTimestamp: Double {
          get {
            return resultMap["data_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "data_timestamp")
          }
        }

        public var diastolic: Int? {
          get {
            return resultMap["diastolic"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "diastolic")
          }
        }

        public var systolic: Int? {
          get {
            return resultMap["systolic"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "systolic")
          }
        }

        public var pulseRate: Int? {
          get {
            return resultMap["pulse_rate"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "pulse_rate")
          }
        }
      }
    }
  }
}

public final class QueryBloodPressureByTimestampRangeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query queryBloodPressureByTimestampRange($from_ts: Float, $to_ts: Float) {
      queryBloodPressureByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          id
          data_timestamp
          diastolic
          systolic
          pulse_rate
        }
      }
    }
    """

  public let operationName: String = "queryBloodPressureByTimestampRange"

  public var from_ts: Double?
  public var to_ts: Double?

  public init(from_ts: Double? = nil, to_ts: Double? = nil) {
    self.from_ts = from_ts
    self.to_ts = to_ts
  }

  public var variables: GraphQLMap? {
    return ["from_ts": from_ts, "to_ts": to_ts]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("queryBloodPressureByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QueryBloodPressureByTimestampRange.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(queryBloodPressureByTimestampRange: QueryBloodPressureByTimestampRange? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "queryBloodPressureByTimestampRange": queryBloodPressureByTimestampRange.flatMap { (value: QueryBloodPressureByTimestampRange) -> ResultMap in value.resultMap }])
    }

    public var queryBloodPressureByTimestampRange: QueryBloodPressureByTimestampRange? {
      get {
        return (resultMap["queryBloodPressureByTimestampRange"] as? ResultMap).flatMap { QueryBloodPressureByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryBloodPressureByTimestampRange")
      }
    }

    public struct QueryBloodPressureByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BloodPressureConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "BloodPressureConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["BloodPressure"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(String.self))),
          GraphQLField("data_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("diastolic", type: .scalar(Int.self)),
          GraphQLField("systolic", type: .scalar(Int.self)),
          GraphQLField("pulse_rate", type: .scalar(Int.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String, dataTimestamp: Double, diastolic: Int? = nil, systolic: Int? = nil, pulseRate: Int? = nil) {
          self.init(unsafeResultMap: ["__typename": "BloodPressure", "id": id, "data_timestamp": dataTimestamp, "diastolic": diastolic, "systolic": systolic, "pulse_rate": pulseRate])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String {
          get {
            return resultMap["id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var dataTimestamp: Double {
          get {
            return resultMap["data_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "data_timestamp")
          }
        }

        public var diastolic: Int? {
          get {
            return resultMap["diastolic"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "diastolic")
          }
        }

        public var systolic: Int? {
          get {
            return resultMap["systolic"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "systolic")
          }
        }

        public var pulseRate: Int? {
          get {
            return resultMap["pulse_rate"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "pulse_rate")
          }
        }
      }
    }
  }
}

public final class CreateSleepDataMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createSleepData($CreateSleepDataInput: CreateSleepDataInput!) {
      createSleepData(input: $CreateSleepDataInput) {
        __typename
        user_id
        sleep_timestamp
        sleep_stage
      }
    }
    """

  public let operationName: String = "createSleepData"

  public var CreateSleepDataInput: CreateSleepDataInput

  public init(CreateSleepDataInput: CreateSleepDataInput) {
    self.CreateSleepDataInput = CreateSleepDataInput
  }

  public var variables: GraphQLMap? {
    return ["CreateSleepDataInput": CreateSleepDataInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createSleepData", arguments: ["input": GraphQLVariable("CreateSleepDataInput")], type: .object(CreateSleepDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createSleepData: CreateSleepDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createSleepData": createSleepData.flatMap { (value: CreateSleepDatum) -> ResultMap in value.resultMap }])
    }

    public var createSleepData: CreateSleepDatum? {
      get {
        return (resultMap["createSleepData"] as? ResultMap).flatMap { CreateSleepDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createSleepData")
      }
    }

    public struct CreateSleepDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SleepData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("sleep_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("sleep_stage", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, sleepTimestamp: Double, sleepStage: Int) {
        self.init(unsafeResultMap: ["__typename": "SleepData", "user_id": userId, "sleep_timestamp": sleepTimestamp, "sleep_stage": sleepStage])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var sleepTimestamp: Double {
        get {
          return resultMap["sleep_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_timestamp")
        }
      }

      public var sleepStage: Int {
        get {
          return resultMap["sleep_stage"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_stage")
        }
      }
    }
  }
}

public final class CreateMultipleSleepDataMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createMultipleSleepData($CreateSleepDataInput: [CreateSleepDataInput!]!) {
      createMultipleSleepData(input: $CreateSleepDataInput) {
        __typename
        user_id
        sleep_timestamp
        sleep_stage
      }
    }
    """

  public let operationName: String = "createMultipleSleepData"

  public var CreateSleepDataInput: [CreateSleepDataInput]

  public init(CreateSleepDataInput: [CreateSleepDataInput]) {
    self.CreateSleepDataInput = CreateSleepDataInput
  }

  public var variables: GraphQLMap? {
    return ["CreateSleepDataInput": CreateSleepDataInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createMultipleSleepData", arguments: ["input": GraphQLVariable("CreateSleepDataInput")], type: .list(.object(CreateMultipleSleepDatum.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createMultipleSleepData: [CreateMultipleSleepDatum?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createMultipleSleepData": createMultipleSleepData.flatMap { (value: [CreateMultipleSleepDatum?]) -> [ResultMap?] in value.map { (value: CreateMultipleSleepDatum?) -> ResultMap? in value.flatMap { (value: CreateMultipleSleepDatum) -> ResultMap in value.resultMap } } }])
    }

    public var createMultipleSleepData: [CreateMultipleSleepDatum?]? {
      get {
        return (resultMap["createMultipleSleepData"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [CreateMultipleSleepDatum?] in value.map { (value: ResultMap?) -> CreateMultipleSleepDatum? in value.flatMap { (value: ResultMap) -> CreateMultipleSleepDatum in CreateMultipleSleepDatum(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [CreateMultipleSleepDatum?]) -> [ResultMap?] in value.map { (value: CreateMultipleSleepDatum?) -> ResultMap? in value.flatMap { (value: CreateMultipleSleepDatum) -> ResultMap in value.resultMap } } }, forKey: "createMultipleSleepData")
      }
    }

    public struct CreateMultipleSleepDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SleepData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("sleep_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("sleep_stage", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, sleepTimestamp: Double, sleepStage: Int) {
        self.init(unsafeResultMap: ["__typename": "SleepData", "user_id": userId, "sleep_timestamp": sleepTimestamp, "sleep_stage": sleepStage])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var sleepTimestamp: Double {
        get {
          return resultMap["sleep_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_timestamp")
        }
      }

      public var sleepStage: Int {
        get {
          return resultMap["sleep_stage"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_stage")
        }
      }
    }
  }
}

public final class UpdateSleepDataMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateSleepData($UpdateSleepDataInput: UpdateSleepDataInput!) {
      updateSleepData(input: $UpdateSleepDataInput) {
        __typename
        user_id
        sleep_timestamp
        sleep_stage
      }
    }
    """

  public let operationName: String = "updateSleepData"

  public var UpdateSleepDataInput: UpdateSleepDataInput

  public init(UpdateSleepDataInput: UpdateSleepDataInput) {
    self.UpdateSleepDataInput = UpdateSleepDataInput
  }

  public var variables: GraphQLMap? {
    return ["UpdateSleepDataInput": UpdateSleepDataInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateSleepData", arguments: ["input": GraphQLVariable("UpdateSleepDataInput")], type: .object(UpdateSleepDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateSleepData: UpdateSleepDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateSleepData": updateSleepData.flatMap { (value: UpdateSleepDatum) -> ResultMap in value.resultMap }])
    }

    public var updateSleepData: UpdateSleepDatum? {
      get {
        return (resultMap["updateSleepData"] as? ResultMap).flatMap { UpdateSleepDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateSleepData")
      }
    }

    public struct UpdateSleepDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SleepData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("sleep_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("sleep_stage", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, sleepTimestamp: Double, sleepStage: Int) {
        self.init(unsafeResultMap: ["__typename": "SleepData", "user_id": userId, "sleep_timestamp": sleepTimestamp, "sleep_stage": sleepStage])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var sleepTimestamp: Double {
        get {
          return resultMap["sleep_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_timestamp")
        }
      }

      public var sleepStage: Int {
        get {
          return resultMap["sleep_stage"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_stage")
        }
      }
    }
  }
}

public final class DeleteSleepDataMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deleteSleepData($DeleteSleepDataInput: DeleteSleepDataInput!) {
      deleteSleepData(input: $DeleteSleepDataInput) {
        __typename
        user_id
        sleep_timestamp
        sleep_stage
      }
    }
    """

  public let operationName: String = "deleteSleepData"

  public var DeleteSleepDataInput: DeleteSleepDataInput

  public init(DeleteSleepDataInput: DeleteSleepDataInput) {
    self.DeleteSleepDataInput = DeleteSleepDataInput
  }

  public var variables: GraphQLMap? {
    return ["DeleteSleepDataInput": DeleteSleepDataInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteSleepData", arguments: ["input": GraphQLVariable("DeleteSleepDataInput")], type: .object(DeleteSleepDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteSleepData: DeleteSleepDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteSleepData": deleteSleepData.flatMap { (value: DeleteSleepDatum) -> ResultMap in value.resultMap }])
    }

    public var deleteSleepData: DeleteSleepDatum? {
      get {
        return (resultMap["deleteSleepData"] as? ResultMap).flatMap { DeleteSleepDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteSleepData")
      }
    }

    public struct DeleteSleepDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SleepData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("sleep_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("sleep_stage", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, sleepTimestamp: Double, sleepStage: Int) {
        self.init(unsafeResultMap: ["__typename": "SleepData", "user_id": userId, "sleep_timestamp": sleepTimestamp, "sleep_stage": sleepStage])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var sleepTimestamp: Double {
        get {
          return resultMap["sleep_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_timestamp")
        }
      }

      public var sleepStage: Int {
        get {
          return resultMap["sleep_stage"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_stage")
        }
      }
    }
  }
}

public final class GetSleepDataQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getSleepData($sleep_timestamp: Float!) {
      getSleepData(sleep_timestamp: $sleep_timestamp) {
        __typename
        user_id
        sleep_timestamp
        sleep_stage
      }
    }
    """

  public let operationName: String = "getSleepData"

  public var sleep_timestamp: Double

  public init(sleep_timestamp: Double) {
    self.sleep_timestamp = sleep_timestamp
  }

  public var variables: GraphQLMap? {
    return ["sleep_timestamp": sleep_timestamp]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getSleepData", arguments: ["sleep_timestamp": GraphQLVariable("sleep_timestamp")], type: .object(GetSleepDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getSleepData: GetSleepDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getSleepData": getSleepData.flatMap { (value: GetSleepDatum) -> ResultMap in value.resultMap }])
    }

    public var getSleepData: GetSleepDatum? {
      get {
        return (resultMap["getSleepData"] as? ResultMap).flatMap { GetSleepDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getSleepData")
      }
    }

    public struct GetSleepDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SleepData"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("sleep_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("sleep_stage", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, sleepTimestamp: Double, sleepStage: Int) {
        self.init(unsafeResultMap: ["__typename": "SleepData", "user_id": userId, "sleep_timestamp": sleepTimestamp, "sleep_stage": sleepStage])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var sleepTimestamp: Double {
        get {
          return resultMap["sleep_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_timestamp")
        }
      }

      public var sleepStage: Int {
        get {
          return resultMap["sleep_stage"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_stage")
        }
      }
    }
  }
}

public final class ListSleepDataQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query listSleepData {
      listSleepData {
        __typename
        items {
          __typename
          user_id
          sleep_timestamp
          sleep_stage
        }
      }
    }
    """

  public let operationName: String = "listSleepData"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listSleepData", type: .object(ListSleepDatum.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(listSleepData: ListSleepDatum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "listSleepData": listSleepData.flatMap { (value: ListSleepDatum) -> ResultMap in value.resultMap }])
    }

    public var listSleepData: ListSleepDatum? {
      get {
        return (resultMap["listSleepData"] as? ResultMap).flatMap { ListSleepDatum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "listSleepData")
      }
    }

    public struct ListSleepDatum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SleepDataConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "SleepDataConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SleepData"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("sleep_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("sleep_stage", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, sleepTimestamp: Double, sleepStage: Int) {
          self.init(unsafeResultMap: ["__typename": "SleepData", "user_id": userId, "sleep_timestamp": sleepTimestamp, "sleep_stage": sleepStage])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var sleepTimestamp: Double {
          get {
            return resultMap["sleep_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_timestamp")
          }
        }

        public var sleepStage: Int {
          get {
            return resultMap["sleep_stage"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_stage")
          }
        }
      }
    }
  }
}

public final class QuerySleepDataByTimestampRangeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query querySleepDataByTimestampRange($from_ts: Float, $to_ts: Float) {
      querySleepDataByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          sleep_timestamp
          sleep_stage
        }
      }
    }
    """

  public let operationName: String = "querySleepDataByTimestampRange"

  public var from_ts: Double?
  public var to_ts: Double?

  public init(from_ts: Double? = nil, to_ts: Double? = nil) {
    self.from_ts = from_ts
    self.to_ts = to_ts
  }

  public var variables: GraphQLMap? {
    return ["from_ts": from_ts, "to_ts": to_ts]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("querySleepDataByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QuerySleepDataByTimestampRange.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(querySleepDataByTimestampRange: QuerySleepDataByTimestampRange? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "querySleepDataByTimestampRange": querySleepDataByTimestampRange.flatMap { (value: QuerySleepDataByTimestampRange) -> ResultMap in value.resultMap }])
    }

    public var querySleepDataByTimestampRange: QuerySleepDataByTimestampRange? {
      get {
        return (resultMap["querySleepDataByTimestampRange"] as? ResultMap).flatMap { QuerySleepDataByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "querySleepDataByTimestampRange")
      }
    }

    public struct QuerySleepDataByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SleepDataConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "SleepDataConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SleepData"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("sleep_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("sleep_stage", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, sleepTimestamp: Double, sleepStage: Int) {
          self.init(unsafeResultMap: ["__typename": "SleepData", "user_id": userId, "sleep_timestamp": sleepTimestamp, "sleep_stage": sleepStage])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var sleepTimestamp: Double {
          get {
            return resultMap["sleep_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_timestamp")
          }
        }

        public var sleepStage: Int {
          get {
            return resultMap["sleep_stage"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_stage")
          }
        }
      }
    }
  }
}

public final class GetDailyAveragesQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getDailyAverages($daily_averages_date: String!) {
      getDailyAverages(daily_averages_date: $daily_averages_date) {
        __typename
        user_id
        daily_averages_date
        average_pulse_rate
        total_step_count
        total_calories_burnt
        total_distance_covered
        average_temperature
        average_systolic
        average_diastolic
        average_breathing_rate
        average_sp_o2
        average_hrv
        sleep_data
        sleep_start_time
        sleep_end_time
        health_score
        data_point_score
      }
    }
    """

  public let operationName: String = "getDailyAverages"

  public var daily_averages_date: String

  public init(daily_averages_date: String) {
    self.daily_averages_date = daily_averages_date
  }

  public var variables: GraphQLMap? {
    return ["daily_averages_date": daily_averages_date]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getDailyAverages", arguments: ["daily_averages_date": GraphQLVariable("daily_averages_date")], type: .object(GetDailyAverage.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getDailyAverages: GetDailyAverage? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getDailyAverages": getDailyAverages.flatMap { (value: GetDailyAverage) -> ResultMap in value.resultMap }])
    }

    public var getDailyAverages: GetDailyAverage? {
      get {
        return (resultMap["getDailyAverages"] as? ResultMap).flatMap { GetDailyAverage(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getDailyAverages")
      }
    }

    public struct GetDailyAverage: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["DailyAverages"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("daily_averages_date", type: .nonNull(.scalar(String.self))),
        GraphQLField("average_pulse_rate", type: .scalar(String.self)),
        GraphQLField("total_step_count", type: .scalar(String.self)),
        GraphQLField("total_calories_burnt", type: .scalar(String.self)),
        GraphQLField("total_distance_covered", type: .scalar(String.self)),
        GraphQLField("average_temperature", type: .scalar(String.self)),
        GraphQLField("average_systolic", type: .scalar(String.self)),
        GraphQLField("average_diastolic", type: .scalar(String.self)),
        GraphQLField("average_breathing_rate", type: .scalar(String.self)),
        GraphQLField("average_sp_o2", type: .scalar(String.self)),
        GraphQLField("average_hrv", type: .scalar(String.self)),
        GraphQLField("sleep_data", type: .list(.scalar(Int.self))),
        GraphQLField("sleep_start_time", type: .scalar(String.self)),
        GraphQLField("sleep_end_time", type: .scalar(String.self)),
        GraphQLField("health_score", type: .scalar(Double.self)),
        GraphQLField("data_point_score", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, dailyAveragesDate: String, averagePulseRate: String? = nil, totalStepCount: String? = nil, totalCaloriesBurnt: String? = nil, totalDistanceCovered: String? = nil, averageTemperature: String? = nil, averageSystolic: String? = nil, averageDiastolic: String? = nil, averageBreathingRate: String? = nil, averageSpO2: String? = nil, averageHrv: String? = nil, sleepData: [Int?]? = nil, sleepStartTime: String? = nil, sleepEndTime: String? = nil, healthScore: Double? = nil, dataPointScore: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "DailyAverages", "user_id": userId, "daily_averages_date": dailyAveragesDate, "average_pulse_rate": averagePulseRate, "total_step_count": totalStepCount, "total_calories_burnt": totalCaloriesBurnt, "total_distance_covered": totalDistanceCovered, "average_temperature": averageTemperature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate, "average_sp_o2": averageSpO2, "average_hrv": averageHrv, "sleep_data": sleepData, "sleep_start_time": sleepStartTime, "sleep_end_time": sleepEndTime, "health_score": healthScore, "data_point_score": dataPointScore])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var dailyAveragesDate: String {
        get {
          return resultMap["daily_averages_date"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "daily_averages_date")
        }
      }

      public var averagePulseRate: String? {
        get {
          return resultMap["average_pulse_rate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_pulse_rate")
        }
      }

      public var totalStepCount: String? {
        get {
          return resultMap["total_step_count"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_step_count")
        }
      }

      public var totalCaloriesBurnt: String? {
        get {
          return resultMap["total_calories_burnt"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_calories_burnt")
        }
      }

      public var totalDistanceCovered: String? {
        get {
          return resultMap["total_distance_covered"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "total_distance_covered")
        }
      }

      public var averageTemperature: String? {
        get {
          return resultMap["average_temperature"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_temperature")
        }
      }

      public var averageSystolic: String? {
        get {
          return resultMap["average_systolic"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_systolic")
        }
      }

      public var averageDiastolic: String? {
        get {
          return resultMap["average_diastolic"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_diastolic")
        }
      }

      public var averageBreathingRate: String? {
        get {
          return resultMap["average_breathing_rate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_breathing_rate")
        }
      }

      public var averageSpO2: String? {
        get {
          return resultMap["average_sp_o2"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_sp_o2")
        }
      }

      public var averageHrv: String? {
        get {
          return resultMap["average_hrv"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "average_hrv")
        }
      }

      public var sleepData: [Int?]? {
        get {
          return resultMap["sleep_data"] as? [Int?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_data")
        }
      }

      public var sleepStartTime: String? {
        get {
          return resultMap["sleep_start_time"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_start_time")
        }
      }

      public var sleepEndTime: String? {
        get {
          return resultMap["sleep_end_time"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "sleep_end_time")
        }
      }

      public var healthScore: Double? {
        get {
          return resultMap["health_score"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "health_score")
        }
      }

      public var dataPointScore: String? {
        get {
          return resultMap["data_point_score"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "data_point_score")
        }
      }
    }
  }
}

public final class ListDailyAveragesQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query listDailyAverages($filter: TableDailyAveragesFilterInput, $limit: Int) {
      listDailyAverages(filter: $filter, limit: $limit) {
        __typename
        items {
          __typename
          user_id
          daily_averages_date
          average_pulse_rate
          total_step_count
          total_calories_burnt
          total_distance_covered
          average_temperature
          average_systolic
          average_diastolic
          average_breathing_rate
          average_sp_o2
          average_hrv
          sleep_data
          sleep_start_time
          sleep_end_time
          health_score
        }
      }
    }
    """

  public let operationName: String = "listDailyAverages"

  public var filter: TableDailyAveragesFilterInput?
  public var limit: Int?

  public init(filter: TableDailyAveragesFilterInput? = nil, limit: Int? = nil) {
    self.filter = filter
    self.limit = limit
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listDailyAverages", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit")], type: .object(ListDailyAverage.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(listDailyAverages: ListDailyAverage? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "listDailyAverages": listDailyAverages.flatMap { (value: ListDailyAverage) -> ResultMap in value.resultMap }])
    }

    public var listDailyAverages: ListDailyAverage? {
      get {
        return (resultMap["listDailyAverages"] as? ResultMap).flatMap { ListDailyAverage(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "listDailyAverages")
      }
    }

    public struct ListDailyAverage: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["DailyAveragesConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "DailyAveragesConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["DailyAverages"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("daily_averages_date", type: .nonNull(.scalar(String.self))),
          GraphQLField("average_pulse_rate", type: .scalar(String.self)),
          GraphQLField("total_step_count", type: .scalar(String.self)),
          GraphQLField("total_calories_burnt", type: .scalar(String.self)),
          GraphQLField("total_distance_covered", type: .scalar(String.self)),
          GraphQLField("average_temperature", type: .scalar(String.self)),
          GraphQLField("average_systolic", type: .scalar(String.self)),
          GraphQLField("average_diastolic", type: .scalar(String.self)),
          GraphQLField("average_breathing_rate", type: .scalar(String.self)),
          GraphQLField("average_sp_o2", type: .scalar(String.self)),
          GraphQLField("average_hrv", type: .scalar(String.self)),
          GraphQLField("sleep_data", type: .list(.scalar(Int.self))),
          GraphQLField("sleep_start_time", type: .scalar(String.self)),
          GraphQLField("sleep_end_time", type: .scalar(String.self)),
          GraphQLField("health_score", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, dailyAveragesDate: String, averagePulseRate: String? = nil, totalStepCount: String? = nil, totalCaloriesBurnt: String? = nil, totalDistanceCovered: String? = nil, averageTemperature: String? = nil, averageSystolic: String? = nil, averageDiastolic: String? = nil, averageBreathingRate: String? = nil, averageSpO2: String? = nil, averageHrv: String? = nil, sleepData: [Int?]? = nil, sleepStartTime: String? = nil, sleepEndTime: String? = nil, healthScore: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "DailyAverages", "user_id": userId, "daily_averages_date": dailyAveragesDate, "average_pulse_rate": averagePulseRate, "total_step_count": totalStepCount, "total_calories_burnt": totalCaloriesBurnt, "total_distance_covered": totalDistanceCovered, "average_temperature": averageTemperature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate, "average_sp_o2": averageSpO2, "average_hrv": averageHrv, "sleep_data": sleepData, "sleep_start_time": sleepStartTime, "sleep_end_time": sleepEndTime, "health_score": healthScore])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var dailyAveragesDate: String {
          get {
            return resultMap["daily_averages_date"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "daily_averages_date")
          }
        }

        public var averagePulseRate: String? {
          get {
            return resultMap["average_pulse_rate"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_pulse_rate")
          }
        }

        public var totalStepCount: String? {
          get {
            return resultMap["total_step_count"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "total_step_count")
          }
        }

        public var totalCaloriesBurnt: String? {
          get {
            return resultMap["total_calories_burnt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "total_calories_burnt")
          }
        }

        public var totalDistanceCovered: String? {
          get {
            return resultMap["total_distance_covered"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "total_distance_covered")
          }
        }

        public var averageTemperature: String? {
          get {
            return resultMap["average_temperature"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_temperature")
          }
        }

        public var averageSystolic: String? {
          get {
            return resultMap["average_systolic"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_systolic")
          }
        }

        public var averageDiastolic: String? {
          get {
            return resultMap["average_diastolic"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_diastolic")
          }
        }

        public var averageBreathingRate: String? {
          get {
            return resultMap["average_breathing_rate"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_breathing_rate")
          }
        }

        public var averageSpO2: String? {
          get {
            return resultMap["average_sp_o2"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_sp_o2")
          }
        }

        public var averageHrv: String? {
          get {
            return resultMap["average_hrv"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_hrv")
          }
        }

        public var sleepData: [Int?]? {
          get {
            return resultMap["sleep_data"] as? [Int?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_data")
          }
        }

        public var sleepStartTime: String? {
          get {
            return resultMap["sleep_start_time"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_start_time")
          }
        }

        public var sleepEndTime: String? {
          get {
            return resultMap["sleep_end_time"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_end_time")
          }
        }

        public var healthScore: Double? {
          get {
            return resultMap["health_score"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "health_score")
          }
        }
      }
    }
  }
}

public final class QueryDailyAveragesByTimestampRangeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query queryDailyAveragesByTimestampRange($from_ts: String, $to_ts: String) {
      queryDailyAveragesByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          daily_averages_date
          average_pulse_rate
          total_step_count
          total_calories_burnt
          total_distance_covered
          average_temperature
          average_systolic
          average_diastolic
          average_breathing_rate
          average_sp_o2
          average_hrv
          sleep_data
          sleep_start_time
          sleep_end_time
          health_score
          data_point_score
        }
      }
    }
    """

  public let operationName: String = "queryDailyAveragesByTimestampRange"

  public var from_ts: String?
  public var to_ts: String?

  public init(from_ts: String? = nil, to_ts: String? = nil) {
    self.from_ts = from_ts
    self.to_ts = to_ts
  }

  public var variables: GraphQLMap? {
    return ["from_ts": from_ts, "to_ts": to_ts]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("queryDailyAveragesByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QueryDailyAveragesByTimestampRange.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(queryDailyAveragesByTimestampRange: QueryDailyAveragesByTimestampRange? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "queryDailyAveragesByTimestampRange": queryDailyAveragesByTimestampRange.flatMap { (value: QueryDailyAveragesByTimestampRange) -> ResultMap in value.resultMap }])
    }

    public var queryDailyAveragesByTimestampRange: QueryDailyAveragesByTimestampRange? {
      get {
        return (resultMap["queryDailyAveragesByTimestampRange"] as? ResultMap).flatMap { QueryDailyAveragesByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryDailyAveragesByTimestampRange")
      }
    }

    public struct QueryDailyAveragesByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["DailyAveragesConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "DailyAveragesConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["DailyAverages"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("daily_averages_date", type: .nonNull(.scalar(String.self))),
          GraphQLField("average_pulse_rate", type: .scalar(String.self)),
          GraphQLField("total_step_count", type: .scalar(String.self)),
          GraphQLField("total_calories_burnt", type: .scalar(String.self)),
          GraphQLField("total_distance_covered", type: .scalar(String.self)),
          GraphQLField("average_temperature", type: .scalar(String.self)),
          GraphQLField("average_systolic", type: .scalar(String.self)),
          GraphQLField("average_diastolic", type: .scalar(String.self)),
          GraphQLField("average_breathing_rate", type: .scalar(String.self)),
          GraphQLField("average_sp_o2", type: .scalar(String.self)),
          GraphQLField("average_hrv", type: .scalar(String.self)),
          GraphQLField("sleep_data", type: .list(.scalar(Int.self))),
          GraphQLField("sleep_start_time", type: .scalar(String.self)),
          GraphQLField("sleep_end_time", type: .scalar(String.self)),
          GraphQLField("health_score", type: .scalar(Double.self)),
          GraphQLField("data_point_score", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, dailyAveragesDate: String, averagePulseRate: String? = nil, totalStepCount: String? = nil, totalCaloriesBurnt: String? = nil, totalDistanceCovered: String? = nil, averageTemperature: String? = nil, averageSystolic: String? = nil, averageDiastolic: String? = nil, averageBreathingRate: String? = nil, averageSpO2: String? = nil, averageHrv: String? = nil, sleepData: [Int?]? = nil, sleepStartTime: String? = nil, sleepEndTime: String? = nil, healthScore: Double? = nil, dataPointScore: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "DailyAverages", "user_id": userId, "daily_averages_date": dailyAveragesDate, "average_pulse_rate": averagePulseRate, "total_step_count": totalStepCount, "total_calories_burnt": totalCaloriesBurnt, "total_distance_covered": totalDistanceCovered, "average_temperature": averageTemperature, "average_systolic": averageSystolic, "average_diastolic": averageDiastolic, "average_breathing_rate": averageBreathingRate, "average_sp_o2": averageSpO2, "average_hrv": averageHrv, "sleep_data": sleepData, "sleep_start_time": sleepStartTime, "sleep_end_time": sleepEndTime, "health_score": healthScore, "data_point_score": dataPointScore])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var dailyAveragesDate: String {
          get {
            return resultMap["daily_averages_date"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "daily_averages_date")
          }
        }

        public var averagePulseRate: String? {
          get {
            return resultMap["average_pulse_rate"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_pulse_rate")
          }
        }

        public var totalStepCount: String? {
          get {
            return resultMap["total_step_count"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "total_step_count")
          }
        }

        public var totalCaloriesBurnt: String? {
          get {
            return resultMap["total_calories_burnt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "total_calories_burnt")
          }
        }

        public var totalDistanceCovered: String? {
          get {
            return resultMap["total_distance_covered"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "total_distance_covered")
          }
        }

        public var averageTemperature: String? {
          get {
            return resultMap["average_temperature"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_temperature")
          }
        }

        public var averageSystolic: String? {
          get {
            return resultMap["average_systolic"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_systolic")
          }
        }

        public var averageDiastolic: String? {
          get {
            return resultMap["average_diastolic"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_diastolic")
          }
        }

        public var averageBreathingRate: String? {
          get {
            return resultMap["average_breathing_rate"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_breathing_rate")
          }
        }

        public var averageSpO2: String? {
          get {
            return resultMap["average_sp_o2"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_sp_o2")
          }
        }

        public var averageHrv: String? {
          get {
            return resultMap["average_hrv"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "average_hrv")
          }
        }

        public var sleepData: [Int?]? {
          get {
            return resultMap["sleep_data"] as? [Int?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_data")
          }
        }

        public var sleepStartTime: String? {
          get {
            return resultMap["sleep_start_time"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_start_time")
          }
        }

        public var sleepEndTime: String? {
          get {
            return resultMap["sleep_end_time"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_end_time")
          }
        }

        public var healthScore: Double? {
          get {
            return resultMap["health_score"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "health_score")
          }
        }

        public var dataPointScore: String? {
          get {
            return resultMap["data_point_score"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "data_point_score")
          }
        }
      }
    }
  }
}

public final class QueryAllDataPointsByTimestampRangeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query queryAllDataPointsByTimestampRange($from_ts: Float, $to_ts: Float, $from_ts_sc: Float, $to_ts_sc: Float) {
      queryBreathingRateByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          breathing_rate_timestamp
          breathing_rate_value
        }
      }
      querySkinTemperatureByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          temperature_timestamp
          temperature_value
        }
      }
      queryBloodPressureByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          id
          data_timestamp
          diastolic
          diastolic
          systolic
          pulse_rate
        }
      }
      querySleepDataByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          sleep_timestamp
          sleep_stage
        }
      }
      queryHeartRateVariabilitiesByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          hrv_timestamp
          hrv_value
        }
      }
      querySpO2ByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          sp_o2_timestamp
          sp_o2_value
        }
      }
      queryPulseRateByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          pulse_rate_timestamp
          pulse_rate_value
        }
      }
      queryStepCountByTimestampRange(from_ts: $from_ts_sc, to_ts: $to_ts_sc) {
        __typename
        items {
          __typename
          user_id
          step_count_timestamp
          step_count_value
          calories_burnt
          distance_covered
        }
      }
    }
    """

  public let operationName: String = "queryAllDataPointsByTimestampRange"

  public var from_ts: Double?
  public var to_ts: Double?
  public var from_ts_sc: Double?
  public var to_ts_sc: Double?

  public init(from_ts: Double? = nil, to_ts: Double? = nil, from_ts_sc: Double? = nil, to_ts_sc: Double? = nil) {
    self.from_ts = from_ts
    self.to_ts = to_ts
    self.from_ts_sc = from_ts_sc
    self.to_ts_sc = to_ts_sc
  }

  public var variables: GraphQLMap? {
    return ["from_ts": from_ts, "to_ts": to_ts, "from_ts_sc": from_ts_sc, "to_ts_sc": to_ts_sc]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("queryBreathingRateByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QueryBreathingRateByTimestampRange.selections)),
      GraphQLField("querySkinTemperatureByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QuerySkinTemperatureByTimestampRange.selections)),
      GraphQLField("queryBloodPressureByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QueryBloodPressureByTimestampRange.selections)),
      GraphQLField("querySleepDataByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QuerySleepDataByTimestampRange.selections)),
      GraphQLField("queryHeartRateVariabilitiesByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QueryHeartRateVariabilitiesByTimestampRange.selections)),
      GraphQLField("querySpO2ByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QuerySpO2ByTimestampRange.selections)),
      GraphQLField("queryPulseRateByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QueryPulseRateByTimestampRange.selections)),
      GraphQLField("queryStepCountByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts_sc"), "to_ts": GraphQLVariable("to_ts_sc")], type: .object(QueryStepCountByTimestampRange.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(queryBreathingRateByTimestampRange: QueryBreathingRateByTimestampRange? = nil, querySkinTemperatureByTimestampRange: QuerySkinTemperatureByTimestampRange? = nil, queryBloodPressureByTimestampRange: QueryBloodPressureByTimestampRange? = nil, querySleepDataByTimestampRange: QuerySleepDataByTimestampRange? = nil, queryHeartRateVariabilitiesByTimestampRange: QueryHeartRateVariabilitiesByTimestampRange? = nil, querySpO2ByTimestampRange: QuerySpO2ByTimestampRange? = nil, queryPulseRateByTimestampRange: QueryPulseRateByTimestampRange? = nil, queryStepCountByTimestampRange: QueryStepCountByTimestampRange? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "queryBreathingRateByTimestampRange": queryBreathingRateByTimestampRange.flatMap { (value: QueryBreathingRateByTimestampRange) -> ResultMap in value.resultMap }, "querySkinTemperatureByTimestampRange": querySkinTemperatureByTimestampRange.flatMap { (value: QuerySkinTemperatureByTimestampRange) -> ResultMap in value.resultMap }, "queryBloodPressureByTimestampRange": queryBloodPressureByTimestampRange.flatMap { (value: QueryBloodPressureByTimestampRange) -> ResultMap in value.resultMap }, "querySleepDataByTimestampRange": querySleepDataByTimestampRange.flatMap { (value: QuerySleepDataByTimestampRange) -> ResultMap in value.resultMap }, "queryHeartRateVariabilitiesByTimestampRange": queryHeartRateVariabilitiesByTimestampRange.flatMap { (value: QueryHeartRateVariabilitiesByTimestampRange) -> ResultMap in value.resultMap }, "querySpO2ByTimestampRange": querySpO2ByTimestampRange.flatMap { (value: QuerySpO2ByTimestampRange) -> ResultMap in value.resultMap }, "queryPulseRateByTimestampRange": queryPulseRateByTimestampRange.flatMap { (value: QueryPulseRateByTimestampRange) -> ResultMap in value.resultMap }, "queryStepCountByTimestampRange": queryStepCountByTimestampRange.flatMap { (value: QueryStepCountByTimestampRange) -> ResultMap in value.resultMap }])
    }

    public var queryBreathingRateByTimestampRange: QueryBreathingRateByTimestampRange? {
      get {
        return (resultMap["queryBreathingRateByTimestampRange"] as? ResultMap).flatMap { QueryBreathingRateByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryBreathingRateByTimestampRange")
      }
    }

    public var querySkinTemperatureByTimestampRange: QuerySkinTemperatureByTimestampRange? {
      get {
        return (resultMap["querySkinTemperatureByTimestampRange"] as? ResultMap).flatMap { QuerySkinTemperatureByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "querySkinTemperatureByTimestampRange")
      }
    }

    public var queryBloodPressureByTimestampRange: QueryBloodPressureByTimestampRange? {
      get {
        return (resultMap["queryBloodPressureByTimestampRange"] as? ResultMap).flatMap { QueryBloodPressureByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryBloodPressureByTimestampRange")
      }
    }

    public var querySleepDataByTimestampRange: QuerySleepDataByTimestampRange? {
      get {
        return (resultMap["querySleepDataByTimestampRange"] as? ResultMap).flatMap { QuerySleepDataByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "querySleepDataByTimestampRange")
      }
    }

    public var queryHeartRateVariabilitiesByTimestampRange: QueryHeartRateVariabilitiesByTimestampRange? {
      get {
        return (resultMap["queryHeartRateVariabilitiesByTimestampRange"] as? ResultMap).flatMap { QueryHeartRateVariabilitiesByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryHeartRateVariabilitiesByTimestampRange")
      }
    }

    public var querySpO2ByTimestampRange: QuerySpO2ByTimestampRange? {
      get {
        return (resultMap["querySpO2ByTimestampRange"] as? ResultMap).flatMap { QuerySpO2ByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "querySpO2ByTimestampRange")
      }
    }

    public var queryPulseRateByTimestampRange: QueryPulseRateByTimestampRange? {
      get {
        return (resultMap["queryPulseRateByTimestampRange"] as? ResultMap).flatMap { QueryPulseRateByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryPulseRateByTimestampRange")
      }
    }

    public var queryStepCountByTimestampRange: QueryStepCountByTimestampRange? {
      get {
        return (resultMap["queryStepCountByTimestampRange"] as? ResultMap).flatMap { QueryStepCountByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryStepCountByTimestampRange")
      }
    }

    public struct QueryBreathingRateByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BreathingRateConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "BreathingRateConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["BreathingRate"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("breathing_rate_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("breathing_rate_value", type: .nonNull(.scalar(Double.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, breathingRateTimestamp: Double, breathingRateValue: Double) {
          self.init(unsafeResultMap: ["__typename": "BreathingRate", "user_id": userId, "breathing_rate_timestamp": breathingRateTimestamp, "breathing_rate_value": breathingRateValue])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var breathingRateTimestamp: Double {
          get {
            return resultMap["breathing_rate_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "breathing_rate_timestamp")
          }
        }

        public var breathingRateValue: Double {
          get {
            return resultMap["breathing_rate_value"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "breathing_rate_value")
          }
        }
      }
    }

    public struct QuerySkinTemperatureByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SkinTemperatureConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "SkinTemperatureConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SkinTemperature"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("temperature_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("temperature_value", type: .nonNull(.scalar(Double.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, temperatureTimestamp: Double, temperatureValue: Double) {
          self.init(unsafeResultMap: ["__typename": "SkinTemperature", "user_id": userId, "temperature_timestamp": temperatureTimestamp, "temperature_value": temperatureValue])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var temperatureTimestamp: Double {
          get {
            return resultMap["temperature_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "temperature_timestamp")
          }
        }

        public var temperatureValue: Double {
          get {
            return resultMap["temperature_value"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "temperature_value")
          }
        }
      }
    }

    public struct QueryBloodPressureByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BloodPressureConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "BloodPressureConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["BloodPressure"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(String.self))),
          GraphQLField("data_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("diastolic", type: .scalar(Int.self)),
          GraphQLField("diastolic", type: .scalar(Int.self)),
          GraphQLField("systolic", type: .scalar(Int.self)),
          GraphQLField("pulse_rate", type: .scalar(Int.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String, dataTimestamp: Double, diastolic: Int? = nil, systolic: Int? = nil, pulseRate: Int? = nil) {
          self.init(unsafeResultMap: ["__typename": "BloodPressure", "id": id, "data_timestamp": dataTimestamp, "diastolic": diastolic, "systolic": systolic, "pulse_rate": pulseRate])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String {
          get {
            return resultMap["id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var dataTimestamp: Double {
          get {
            return resultMap["data_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "data_timestamp")
          }
        }

        public var diastolic: Int? {
          get {
            return resultMap["diastolic"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "diastolic")
          }
        }

        public var systolic: Int? {
          get {
            return resultMap["systolic"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "systolic")
          }
        }

        public var pulseRate: Int? {
          get {
            return resultMap["pulse_rate"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "pulse_rate")
          }
        }
      }
    }

    public struct QuerySleepDataByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SleepDataConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "SleepDataConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SleepData"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("sleep_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("sleep_stage", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, sleepTimestamp: Double, sleepStage: Int) {
          self.init(unsafeResultMap: ["__typename": "SleepData", "user_id": userId, "sleep_timestamp": sleepTimestamp, "sleep_stage": sleepStage])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var sleepTimestamp: Double {
          get {
            return resultMap["sleep_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_timestamp")
          }
        }

        public var sleepStage: Int {
          get {
            return resultMap["sleep_stage"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "sleep_stage")
          }
        }
      }
    }

    public struct QueryHeartRateVariabilitiesByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["HeartRateVariabilityConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "HeartRateVariabilityConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["HeartRateVariability"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("hrv_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("hrv_value", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, hrvTimestamp: Double, hrvValue: Int) {
          self.init(unsafeResultMap: ["__typename": "HeartRateVariability", "user_id": userId, "hrv_timestamp": hrvTimestamp, "hrv_value": hrvValue])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var hrvTimestamp: Double {
          get {
            return resultMap["hrv_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "hrv_timestamp")
          }
        }

        public var hrvValue: Int {
          get {
            return resultMap["hrv_value"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "hrv_value")
          }
        }
      }
    }

    public struct QuerySpO2ByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SpO2Connection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "SpO2Connection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SpO2"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("sp_o2_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("sp_o2_value", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, spO2Timestamp: Double, spO2Value: Int) {
          self.init(unsafeResultMap: ["__typename": "SpO2", "user_id": userId, "sp_o2_timestamp": spO2Timestamp, "sp_o2_value": spO2Value])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var spO2Timestamp: Double {
          get {
            return resultMap["sp_o2_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "sp_o2_timestamp")
          }
        }

        public var spO2Value: Int {
          get {
            return resultMap["sp_o2_value"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "sp_o2_value")
          }
        }
      }
    }

    public struct QueryPulseRateByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PulseRateConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "PulseRateConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["PulseRate"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("pulse_rate_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("pulse_rate_value", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, pulseRateTimestamp: Double, pulseRateValue: Int) {
          self.init(unsafeResultMap: ["__typename": "PulseRate", "user_id": userId, "pulse_rate_timestamp": pulseRateTimestamp, "pulse_rate_value": pulseRateValue])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var pulseRateTimestamp: Double {
          get {
            return resultMap["pulse_rate_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "pulse_rate_timestamp")
          }
        }

        public var pulseRateValue: Int {
          get {
            return resultMap["pulse_rate_value"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "pulse_rate_value")
          }
        }
      }
    }

    public struct QueryStepCountByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["StepCountConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "StepCountConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["StepCount"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("step_count_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("step_count_value", type: .nonNull(.scalar(Int.self))),
          GraphQLField("calories_burnt", type: .scalar(Double.self)),
          GraphQLField("distance_covered", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, stepCountTimestamp: Double, stepCountValue: Int, caloriesBurnt: Double? = nil, distanceCovered: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "StepCount", "user_id": userId, "step_count_timestamp": stepCountTimestamp, "step_count_value": stepCountValue, "calories_burnt": caloriesBurnt, "distance_covered": distanceCovered])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var stepCountTimestamp: Double {
          get {
            return resultMap["step_count_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "step_count_timestamp")
          }
        }

        public var stepCountValue: Int {
          get {
            return resultMap["step_count_value"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "step_count_value")
          }
        }

        public var caloriesBurnt: Double? {
          get {
            return resultMap["calories_burnt"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "calories_burnt")
          }
        }

        public var distanceCovered: Double? {
          get {
            return resultMap["distance_covered"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "distance_covered")
          }
        }
      }
    }
  }
}

public final class QueryHeartRateVariabilitiesByTimestampRangeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query queryHeartRateVariabilitiesByTimestampRange($from_ts: Float, $to_ts: Float) {
      queryHeartRateVariabilitiesByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          hrv_timestamp
          hrv_value
        }
      }
    }
    """

  public let operationName: String = "queryHeartRateVariabilitiesByTimestampRange"

  public var from_ts: Double?
  public var to_ts: Double?

  public init(from_ts: Double? = nil, to_ts: Double? = nil) {
    self.from_ts = from_ts
    self.to_ts = to_ts
  }

  public var variables: GraphQLMap? {
    return ["from_ts": from_ts, "to_ts": to_ts]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("queryHeartRateVariabilitiesByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QueryHeartRateVariabilitiesByTimestampRange.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(queryHeartRateVariabilitiesByTimestampRange: QueryHeartRateVariabilitiesByTimestampRange? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "queryHeartRateVariabilitiesByTimestampRange": queryHeartRateVariabilitiesByTimestampRange.flatMap { (value: QueryHeartRateVariabilitiesByTimestampRange) -> ResultMap in value.resultMap }])
    }

    public var queryHeartRateVariabilitiesByTimestampRange: QueryHeartRateVariabilitiesByTimestampRange? {
      get {
        return (resultMap["queryHeartRateVariabilitiesByTimestampRange"] as? ResultMap).flatMap { QueryHeartRateVariabilitiesByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryHeartRateVariabilitiesByTimestampRange")
      }
    }

    public struct QueryHeartRateVariabilitiesByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["HeartRateVariabilityConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "HeartRateVariabilityConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["HeartRateVariability"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("hrv_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("hrv_value", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, hrvTimestamp: Double, hrvValue: Int) {
          self.init(unsafeResultMap: ["__typename": "HeartRateVariability", "user_id": userId, "hrv_timestamp": hrvTimestamp, "hrv_value": hrvValue])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var hrvTimestamp: Double {
          get {
            return resultMap["hrv_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "hrv_timestamp")
          }
        }

        public var hrvValue: Int {
          get {
            return resultMap["hrv_value"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "hrv_value")
          }
        }
      }
    }
  }
}

public final class CreateMultipleActivityIntensityMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createMultipleActivityIntensity($CreateActivityIntensityInput: [CreateActivityIntensityInput!]!) {
      createMultipleActivityIntensity(input: $CreateActivityIntensityInput) {
        __typename
        user_id
        data_timestamp
        value
      }
    }
    """

  public let operationName: String = "createMultipleActivityIntensity"

  public var CreateActivityIntensityInput: [CreateActivityIntensityInput]

  public init(CreateActivityIntensityInput: [CreateActivityIntensityInput]) {
    self.CreateActivityIntensityInput = CreateActivityIntensityInput
  }

  public var variables: GraphQLMap? {
    return ["CreateActivityIntensityInput": CreateActivityIntensityInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createMultipleActivityIntensity", arguments: ["input": GraphQLVariable("CreateActivityIntensityInput")], type: .list(.object(CreateMultipleActivityIntensity.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createMultipleActivityIntensity: [CreateMultipleActivityIntensity?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createMultipleActivityIntensity": createMultipleActivityIntensity.flatMap { (value: [CreateMultipleActivityIntensity?]) -> [ResultMap?] in value.map { (value: CreateMultipleActivityIntensity?) -> ResultMap? in value.flatMap { (value: CreateMultipleActivityIntensity) -> ResultMap in value.resultMap } } }])
    }

    public var createMultipleActivityIntensity: [CreateMultipleActivityIntensity?]? {
      get {
        return (resultMap["createMultipleActivityIntensity"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [CreateMultipleActivityIntensity?] in value.map { (value: ResultMap?) -> CreateMultipleActivityIntensity? in value.flatMap { (value: ResultMap) -> CreateMultipleActivityIntensity in CreateMultipleActivityIntensity(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [CreateMultipleActivityIntensity?]) -> [ResultMap?] in value.map { (value: CreateMultipleActivityIntensity?) -> ResultMap? in value.flatMap { (value: CreateMultipleActivityIntensity) -> ResultMap in value.resultMap } } }, forKey: "createMultipleActivityIntensity")
      }
    }

    public struct CreateMultipleActivityIntensity: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ActivityIntensity"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
        GraphQLField("data_timestamp", type: .nonNull(.scalar(Double.self))),
        GraphQLField("value", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, dataTimestamp: Double, value: Int) {
        self.init(unsafeResultMap: ["__typename": "ActivityIntensity", "user_id": userId, "data_timestamp": dataTimestamp, "value": value])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["user_id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "user_id")
        }
      }

      public var dataTimestamp: Double {
        get {
          return resultMap["data_timestamp"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "data_timestamp")
        }
      }

      public var value: Int {
        get {
          return resultMap["value"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "value")
        }
      }
    }
  }
}

public final class QueryActivityIntensityByTimestampRangeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query queryActivityIntensityByTimestampRange($from_ts: Float, $to_ts: Float) {
      queryActivityIntensityByTimestampRange(from_ts: $from_ts, to_ts: $to_ts) {
        __typename
        items {
          __typename
          user_id
          data_timestamp
          value
        }
      }
    }
    """

  public let operationName: String = "queryActivityIntensityByTimestampRange"

  public var from_ts: Double?
  public var to_ts: Double?

  public init(from_ts: Double? = nil, to_ts: Double? = nil) {
    self.from_ts = from_ts
    self.to_ts = to_ts
  }

  public var variables: GraphQLMap? {
    return ["from_ts": from_ts, "to_ts": to_ts]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("queryActivityIntensityByTimestampRange", arguments: ["from_ts": GraphQLVariable("from_ts"), "to_ts": GraphQLVariable("to_ts")], type: .object(QueryActivityIntensityByTimestampRange.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(queryActivityIntensityByTimestampRange: QueryActivityIntensityByTimestampRange? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "queryActivityIntensityByTimestampRange": queryActivityIntensityByTimestampRange.flatMap { (value: QueryActivityIntensityByTimestampRange) -> ResultMap in value.resultMap }])
    }

    public var queryActivityIntensityByTimestampRange: QueryActivityIntensityByTimestampRange? {
      get {
        return (resultMap["queryActivityIntensityByTimestampRange"] as? ResultMap).flatMap { QueryActivityIntensityByTimestampRange(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "queryActivityIntensityByTimestampRange")
      }
    }

    public struct QueryActivityIntensityByTimestampRange: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ActivityIntensityConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(items: [Item?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "ActivityIntensityConnection", "items": items.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (resultMap["items"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Item?] in value.map { (value: ResultMap?) -> Item? in value.flatMap { (value: ResultMap) -> Item in Item(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Item?]) -> [ResultMap?] in value.map { (value: Item?) -> ResultMap? in value.flatMap { (value: Item) -> ResultMap in value.resultMap } } }, forKey: "items")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["ActivityIntensity"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("user_id", type: .nonNull(.scalar(String.self))),
          GraphQLField("data_timestamp", type: .nonNull(.scalar(Double.self))),
          GraphQLField("value", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, dataTimestamp: Double, value: Int) {
          self.init(unsafeResultMap: ["__typename": "ActivityIntensity", "user_id": userId, "data_timestamp": dataTimestamp, "value": value])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["user_id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "user_id")
          }
        }

        public var dataTimestamp: Double {
          get {
            return resultMap["data_timestamp"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "data_timestamp")
          }
        }

        public var value: Int {
          get {
            return resultMap["value"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "value")
          }
        }
      }
    }
  }
}
