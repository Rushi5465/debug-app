//
//  CalendarPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 11/08/21.
//

import Foundation
import FSCalendar

class CalendarPresenter: NSObject, CalendarPresenterProtocol {
	
	private weak var delegate: CalendarViewDelegate?
	private var calendarView: FSCalendar
	
	required init(calendarView: FSCalendar, delegate: CalendarViewDelegate) {
		self.calendarView = calendarView
		self.delegate = delegate
	}
	
	func assignCalendarControl() {
		self.calendarView.delegate = self
		self.calendarView.dataSource = self
	}
	
	func showPreviousCalendar() {
		var currentPage = calendarView.currentPage
		currentPage = currentPage.dateBefore(component: .month, value: 1)
		self.calendarView.setCurrentPage(currentPage, animated: true)
		self.delegate?.calendarViewDateSelected(calendarView.currentPage)
	}
	
	func showNextCalendar() {
		var currentPage = calendarView.currentPage
		currentPage = currentPage.dateAfter(component: .month, value: 1)
		self.calendarView.setCurrentPage(currentPage, animated: true)
		self.delegate?.calendarViewDateSelected(calendarView.currentPage)
	}
	
	func showCalendarUI(with selectedDate: Date) {
		calendarView.today = nil
		calendarView.locale = NSLocale.init(localeIdentifier: "en_IN") as Locale
		calendarView.adjustsBoundingRectWhenChangingMonths = false
		calendarView.placeholderType = .none
		calendarView.appearance.headerMinimumDissolvedAlpha = 0
		calendarView.scrollEnabled = false
		calendarView.appearance.borderSelectionColor = UIColor.textColor
		calendarView.select(selectedDate)
		
		self.delegate?.calendarViewDateSelected(calendarView.selectedDate!)
	}
}

// MARK: - FSCalendarDelegate
extension CalendarPresenter: FSCalendarDelegate {
	
	func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
		self.delegate?.calendarViewDateSelected(date)
		self.delegate?.calendarViewDidClose()
	}
}

// MARK: - FSCalendarDataSource
extension CalendarPresenter: FSCalendarDataSource {
	
	func maximumDate(for calendar: FSCalendar) -> Date {
		return Date()
	}

	func minimumDate(for calendar: FSCalendar) -> Date {
		return Date().dateBefore(component: .year, value: 2)
	}
}

// MARK: - FSCalendarDelegateAppearance
extension CalendarPresenter: FSCalendarDelegateAppearance {
	
	func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
		return .clear
	}

	func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderSelectionColorFor date: Date) -> UIColor? {
		return 	.textColor
	}

	func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
		return .textColor
	}

	func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
		if date >= calendar.minimumDate && date <= calendar.maximumDate {
			return .textColor
		}
		return UIColor.textColor.withAlphaComponent(0.5)
	}
}
