//
//  TabPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/08/21.
//

import Foundation
import CoreBluetooth

// swiftlint:disable line_length
class TabPresenter: BluetoothService, TabPresenterProtocol {
	
	var stepCountService: StepCountWebServiceProtocol
	var breathingRateService: BreathingRateWebServiceProtocol
	var pauseReadingService: PauseDataWebServiceProtocol
	var exerciseService: ExerciseDataWebServiceProtocol
	var pulseRateService: PulseRateWebServiceProtocol
	var temperatureService: SkinTemperatureWebServiceProtocol
	var oxygenService: OxygenDataWebServiceProtocol
	var authService: AuthorizationWebServiceProtocol
	var userService: UserServiceProtocol
	var goalService: UserGoalsWebServiceProtocol
	var uploaderService: DataUploaderWebServiceProtocol
	var cacheServiceService: CacheTechniqueWebService
	var delegate: TabDelegate?
	let manager = DocumentManager(manager: .default)
	var msgToSnsRequest: MessageToSnsRequestModel!
	
	required init(stepCountService: StepCountWebServiceProtocol, breathingRateService: BreathingRateWebServiceProtocol, pauseReadingService: PauseDataWebServiceProtocol, exerciseService: ExerciseDataWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, temperatureService: SkinTemperatureWebServiceProtocol, oxygenService: OxygenDataWebServiceProtocol, authService: AuthorizationWebServiceProtocol, userService: UserServiceProtocol, goalService: UserGoalsWebServiceProtocol, uploaderService: DataUploaderWebServiceProtocol, cacheService: CacheTechniqueWebService, delegate: TabDelegate) {
		self.stepCountService = stepCountService
		self.breathingRateService = breathingRateService
		self.pauseReadingService = pauseReadingService
		self.exerciseService = exerciseService
		self.pulseRateService = pulseRateService
		self.temperatureService = temperatureService
		self.oxygenService = oxygenService
		self.authService = authService
		self.userService = userService
		self.goalService = goalService
		self.uploaderService = uploaderService
		self.cacheServiceService = cacheService
		self.delegate = delegate
		super.init()
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerValueUpdate), name: Notification.Name(TransferService.dateServiceUUID.uuidString), object: nil)
	}
	
	func requestAccessToken() {
		self.authService.requestAccessToken { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.accessTokenErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.accessTokenSuccess(with: response)
			}
		}
	}
	
	func fetchUserInformation() {
		self.userService.fetchUserDetails { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.userDetailErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.userDetailSuccess(with: response)
			}
		}
	}
	
	func fetchData(for date: Date) {
		self.fetchGoal()
		self.fetchStepCount(for: date)
		self.fetchBreathingRate(for: date)
		self.fetchExerciseData(for: date)
		self.fetchPauseReading(for: date)
		self.fetchOxygenReading(for: date)
		self.fetchPulseRate(for: date)
		self.fetchSkinTemperature(for: date)
	}
	
	private func fetchGoal() {
		
		goalService.fetch { item, error in
		}
	}
	
	private func fetchStepCount(for date: Date) {
		
		let range = date.startOfDay.timeIntervalSince1970 ..<  date.endOfDay.timeIntervalSince1970
		stepCountService.fetch(by: range) { items, error in
		}
	}
	
	private func fetchBreathingRate(for date: Date) {
		
		let range = date.startOfDay.timeIntervalSince1970 ..<  date.endOfDay.timeIntervalSince1970
		breathingRateService.fetch(by: range) { items, error in
		}
	}
	
	private func fetchPauseReading(for date: Date) {
		
		let range = date.startOfDay.timeIntervalSince1970 ..<  date.endOfDay.timeIntervalSince1970
		pauseReadingService.fetch(by: range) { items, error in
		}
	}
	
	private func fetchExerciseData(for date: Date) {
		
		let range = date.startOfDay.timeIntervalSince1970 ..<  date.endOfDay.timeIntervalSince1970
		exerciseService.fetch(by: range) { items, error in
		}
	}
	
	private func fetchPulseRate(for date: Date) {
		
		let range = date.startOfDay.timeIntervalSince1970 ..<  date.endOfDay.timeIntervalSince1970
		pulseRateService.fetch(by: range) { item, error in
		}
	}
	
	private func fetchSkinTemperature(for date: Date) {
		
		let range = date.startOfDay.timeIntervalSince1970 ..<  date.endOfDay.timeIntervalSince1970
		temperatureService.fetch(by: range) { item, error in
		}
	}
	
	private func fetchOxygenReading(for date: Date) {
		
		let range = date.startOfDay.timeIntervalSince1970 ..<  date.endOfDay.timeIntervalSince1970
		oxygenService.fetch(by: range) { item, error in
		}
	}
	
	func showFloatingButtonActionSheet() {
		
		let slideVC = ActionSheetOverlayView()
		slideVC.modalPresentationStyle = .custom
		slideVC.delegate = self
		self.delegate?.showFloatingPopup(alert: slideVC)
	}
	
	func zipFile() {
		if let zippedFileUrl = self.manager.uploadDataURL(dataType: .movano, fileName: KeyChain.shared.firstName + "_" + KeyChain.shared.lastName) {
			do {
				let data = try Data(contentsOf: zippedFileUrl)
				
				self.postFileInfo(data: data, filename: KeyChain.shared.firstName + "_" + KeyChain.shared.lastName + ".zip", username: (KeyChain.shared.firstName + "_" + KeyChain.shared.lastName))
			} catch {
			}
		}
	}
	
	func postFileInfo(data: Data, filename: String, username: String?) {
		uploaderService.postFileInfo(filename: filename, username: username) { (response, error) in
			if let response = response {
				let fileTimeStamp = Int(Date().timeIntervalSince1970)
				self.msgToSnsRequest = MessageToSnsRequestModel(file_name: response.data.file_name, folder_name: response.data.folder_name, file_timestamp: fileTimeStamp)
				if let url = self.manager.addFile(data: data, fileName: (KeyChain.shared.firstName + "_" + KeyChain.shared.lastName)) {
					self.uploadFileToPresigned(url: response.data.url, fileUrl: url)
				}
			}
		}
	}
	
	func uploadFileToPresigned(url: String, fileUrl: URL) {
		uploaderService.uploadFileToPresigned(url: url, fileUrl: fileUrl) { (isProgressCompleted, error) in
			
			if isProgressCompleted {
				self.putMessageToSns(request: self.msgToSnsRequest)
			}
		}
	}
	
	func putMessageToSns(request: MessageToSnsRequestModel) {
		uploaderService.putMessageToSns(request: request) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.putMessageToSns(with: .failure(error))
				return
			}
			
			if let response = response {
				self?.delegate?.putMessageToSns(with: .success(response))
			}
		}
	}
	
	func postStepCount(model: StepCount) {
		self.stepCountService.add(model: model) { item, error in
			
		}
	}
	
	override func didUpdate(characteristic: CBCharacteristic) {
		guard let characteristicData = characteristic.value else { return }
		let byteArray = [UInt8](characteristicData)
		let battery = UInt(byteArray[2])
		print("Battery percentage is -\(battery)")
		NotificationCenter.default.post(name: NSNotification.Name(TransferService.dateServiceUUID.uuidString), object: "\(battery)")
		
		let batteryCharging = UInt(byteArray[14])
//		NotificationCenter.default.post(name: NSNotification.Name(TransferService.chargingUUID), object: "\(batteryCharging)")
		
	}
}

// MARK: - ActionSheetDelegate
extension TabPresenter: ActionSheetDelegate {
	
	func actionSheetItemSelected(_ item: ActionSheetOption, for viewController: ActionSheetOverlayView) {
		if bluetoothManager.connectedPeripheral != nil {
			self.delegate?.actionSelected(item.title, for: viewController)
		} else {
			self.delegate?.pairDeviceError(viewController)
		}
	}
}

// MARK: - Bluetooth related operation
extension TabPresenter {

	@objc private func bluetoothManagerValueUpdate(notification: NSNotification) {
		if let object = notification.object as? String, notification.name.rawValue == TransferService.dataCharactersticUUID.uuidString {
			let value = object.toDouble!
//			self.temperatureReading.append(Reading(timeStamp: Date().timeIntervalSince1970, reading: value))
		}
	}
	
}
