//
//  RequestStructure.swift
//  starter
//
//  Created by Sankalp Gupta on 16/06/20.
//  Copyright © 2020 Indexnine Technologies. All rights reserved.
//

import Foundation
import Alamofire
struct RequestStructure {
    
    var isSilentCall: Bool
    var apiType: String
    var method: HTTPMethod
    var url: String
    var param: Parameters?
    var header: HTTPHeaders?
    
}
