//
//  AppInfo.swift
//  starter
//
//  Created by Sankalp on 12/06/19.
//  Copyright © 2019 Indexnine Technologies. All rights reserved.
//

import Foundation

class AppInfo {
    
    private var buildMachineOSBuild: String?
    private var bundleIdentifier: String?
    private var bundleExecutable: String?
    private var bundlePackageType: String?
    private var bundleName: String?
    private var minimumOSVersion: String?
    private var appVersionString: String?
    private var buildVersionString: String?
    private var bundleDisplayName: String?
    
    /// singleton Instance
    static var singleInstance: AppInfo?
    
    static var shared: AppInfo {
        if singleInstance == nil {
            singleInstance = AppInfo()
        }
        
        return singleInstance!
    }
    
    func getAppVersionString() -> String {
        return appVersionString!
    }
    func getBuildMachineOSBuild() -> String {
        return buildMachineOSBuild!
    }
    
    func  getBundleIdentifier() -> String {
        return bundleIdentifier!
    }
    
    func getBundleExecutable() -> String {
        return bundleExecutable!
    }
    
    func getBundlePackageType() -> String {
        return bundlePackageType!
    }
    
    func getBundleName() -> String {
        return bundleName!
    }
    
    func getMinimumOSVersion() -> String {
        return minimumOSVersion!
    }
    
    func getBuildVersionString() -> String {
        return buildVersionString!
    }
    
    func getBundleDisplayName() -> String {
        return bundleDisplayName!
    }
    
    init() {
        let dict: [String: Any] = Bundle.main.infoDictionary!
        self.bundleName = dict["CFBundleName"] as? String
        self.buildMachineOSBuild = dict["BuildMachineOSBuild"] as? String
        self.bundleIdentifier = dict["CFBundleIdentifier"] as? String
        self.bundleExecutable = dict["CFBundleExecutable"] as? String
        self.bundlePackageType = dict["CFBundlePackageType"] as? String
        self.minimumOSVersion = dict["MinimumOSVersion"] as? String
        self.appVersionString = dict["CFBundleShortVersionString"] as? String
        self.buildVersionString = dict["CFBundleVersion"] as? String
        self.bundleDisplayName = dict["CFBundleDisplayName"] as? String
    }
    
    var toString: String {
        return "------------------------------------------------------------" +
        "\nApp Info:\nBUNDLE MACHINE OS BUILD : \(buildMachineOSBuild!)" +
        "\nBUNDLE IDENTIFIER : \(bundleIdentifier!)" +
        "\nBUNDLE EXECUTABLE : \(bundleExecutable!)" +
        "\nPACKAGE TYPE : \(bundlePackageType!)" +
        "\nAPP NAME : \(bundleName!)" +
        "\nAPP VERSION : \(appVersionString!)" +
        "\nMIN OS VERSION : \(minimumOSVersion!)" +
        "\n------------------------------------------------------------\n"
    }
}
