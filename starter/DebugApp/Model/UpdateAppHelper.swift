//
//  UpdateAppHelper.swift
//  starter
//
//  Created by Sankalp on 02/07/19.
//  Copyright © 2019 Indexnine Technologies. All rights reserved.
//

import Foundation
import StoreKit

class UpdateAppHelper {
    
    static var appStoreVersion = ""
    
    static func isUpdateAvailable() -> Bool {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                print("Wrong URL")
                return false
        }
        do {
            let data = try Data(contentsOf: url)
            guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any]
                else {
                print("Not Parsable")
                return false
            }
            if let result = (json["results"] as? [Any])?.first as? [String: Any],
                let version = result["version"] as? String {
                print("version in app store", version, currentVersion)
                appStoreVersion = version
                return version != currentVersion
            }
        } catch {
            print("Error")
        }
        print("Invalid Response")
        return false
    }
    
}
