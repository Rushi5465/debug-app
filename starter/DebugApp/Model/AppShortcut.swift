//
//  AppShortcut.swift
//  starter
//
//  Created by Sankalp on 25/06/19.
//  Copyright © 2019 Indexnine Technologies. All rights reserved.
//

import Foundation
import UIKit

class AppShortcut: UIMutableApplicationShortcutItem {
    init(type: String, title: String, icon: String) {
        let translatedTitle = LocalizationSystem.shared.localizedStringForKey(key: title, comment: "")
        let iconImage = UIApplicationShortcutIcon(templateImageName: icon)
        super.init(type: type, localizedTitle: translatedTitle, localizedSubtitle: nil, icon: iconImage, userInfo: nil)
    }
}

class AppShortcuts {
    
    static var shortcuts: [AppShortcut] = []
    
    class func sync() {
        UIApplication.shared.shortcutItems = shortcuts
    }
    
    static func addShortcut(type: String, title: String, icon: String) {
        shortcuts.append(AppShortcut(type: type, title: title, icon: icon))
        sync()
    }
    
    static func removeShortcut(type: String, title: String, icon: String) {
        shortcuts.remove(at: shortcuts.index(of: AppShortcut(type: type, title: title, icon: icon))!)
        sync()
    }
    
    static func clearAll() {
        shortcuts = [AppShortcut]()
        sync()
    }
}
