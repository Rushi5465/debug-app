//
//  DeviceInfo.swift
//  starter
//
//  Created by Sankalp on 14/06/19.
//  Copyright © 2019 Indexnine Technologies. All rights reserved.
//

import Foundation
import  UIKit

class DeviceInfo {
    
    var modelName: String?
    var model: String?
    var uuid: String?
    var deviceName: String?
    var osName: String?
    var osVersion: String?
    
    /// singleton Instance
    static var singleInstance: DeviceInfo?
    
    /**
     Create single instance of DeviceInfo class for access all device releated information
     
     - returns: shared object
     */
    static var shared: DeviceInfo {
        if singleInstance == nil {
            singleInstance = DeviceInfo()
        }
        return singleInstance!
    }
    
    init() {
        
        let thisdevice = UIDevice.current
        self.modelName = UIDevice.modelName
        self.model = thisdevice.model
        self.uuid = thisdevice.identifierForVendor?.uuidString
        self.deviceName = thisdevice.name
        self.osName = thisdevice.systemName
        self.osVersion = thisdevice.systemVersion
    }
    
    var toString: String {
        return "------------------------------------------------------------" +
            "\nDevice Info:" +
            "\nUUID : \(uuid!)" +
            "\nMODEL : \(model!)" +
            "\nMODEL NAME : \(modelName!)" +
            "\nDEVICE NAME : \(deviceName!)" +
            "\nOS : \(osName!)" +
            "\nOS VERSION : \(osVersion!)" +
        "\n------------------------------------------------------------\n"
    }
    
    var json: NSDictionary {
        let json = NSMutableDictionary()
        
        json.setValue(modelName, forKey: "model_name")
        json.setValue(model, forKey: "model")
        json.setValue(uuid, forKey: "uuid")
        json.setValue(osName, forKey: "os_name")
        json.setValue(osVersion, forKey: "os_version")
        json.setValue(deviceName, forKey: "device_name")
        json.setValue(uuid, forKey: "temp_id")
        
        return json
    }
    
    func jsonToString(json: AnyObject) -> String {
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            let convertedString = String(data: data1, encoding: String.Encoding.utf8)
            return convertedString!
        } catch let myJSONError {
            print(myJSONError)
        }
        return ""
    }
}
