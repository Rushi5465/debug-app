//
//  Badge.swift
//  starter
//
//  Created by Sankalp on 17/06/19.
//  Copyright © 2019 Indexnine Technologies. All rights reserved.
//

import Foundation

class Badge {
    
    private var count = Int()
    
    /// singleton Instance
    static var singleInstance: Badge?
    
    init() {
        count = 0
    }
    
    static var shared: Badge {
        if singleInstance == nil {
            singleInstance = Badge()
        }
        return singleInstance!
    }
    
    var badgeCount: Int {
        get {
            return count
        }
        set {
            self.count = newValue
        }
    }
    
    func upCount(_ by: Int?) {
        if by != nil {
            self.count += by!
        } else {
            self.count += 1
        }
    }
    
    func downCount(_ by: Int?) {
        if by != nil {
            self.count -= by!
        } else {
            self.count -= 1
        }
    }
    
    func removeBadge() {
        self.count = 0
    }
    
}
