//
//  Dictionary.swift
//  starter
//
//  Created by Sankalp on 06/06/19.
//  Copyright © 2019 Indexnine Technologies. All rights reserved.
//

import Foundation

extension Dictionary {
    
    func toString() -> String {
        
        let allKeys = self.compactMap { $0.0 }
        var dictToString = "{"
        for eachKey in allKeys {
            dictToString += "\n\(eachKey) : \(self[eachKey]!)"
            if allKeys.last != eachKey {
                dictToString.append(",")
            } else {
                dictToString.append("}")
            }
            
        }
        return dictToString
    }
    
}
