//
//  FileManager.swift
//  starter
//
//  Created by Rushikant on 21/01/22.
//  Copyright © 2022 Indexnine Technologies. All rights reserved.
//

import Foundation

extension FileManager {
	static func getDocumentsDirectory() -> URL {
		return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
	}
	
	static func pathToDocumentDirectory(with fileName: String) -> URL{
		return getDocumentsDirectory().appendingPathComponent(fileName)
	}
}
