//
//  UserGoalsWebService.swift
//  BloodPressureApp
//
//  Created by Rushikant on 24/08/21.
//

import Foundation

// swiftlint:disable all
class UserGoalsWebService: UserGoalsWebServiceProtocol {
	
	private var graphQL: ApolloClientService
	private var service: RestService
	
	init(graphQL: ApolloClientService = ApolloClientService(),service: RestService = RestService.shared) {
		self.graphQL = graphQL
		self.service = service
	}
	
	func add(model: Goal, completionHandler: @escaping (Goal?, MovanoError?) -> Void) {
		let query = CreateUserGoalsMutation(CreateUserGoalsInput: model.createGoalsInput)
		
		_ = self.graphQL.client!.perform(mutation: query) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.createUserGoals {
					let goal = Goal(userID: item.userId, stepCount: item.stepCount!, caloriesBurned: item.caloriesBurned!, systolicLow: item.systolicLow!, systolicHigh: item.systolicHigh!, diastolicLow: item.diastolicLow!, diastolicHigh: item.diastolicHigh!, temperatureLow: item.temperatureLow!, temperatureHigh: item.temperatureHigh!, breathingRateLow: item.breathingRateLow!, breathingRateHigh: item.breathingRateHigh!, pulseRateLow: item.pulseRateLow!, pulseRateHigh: item.pulseRateHigh!, oxygenLow: item.spO2Low!, oxygenHigh: item.spO2High!, sleepHrsLow: item.sleepHrsLow!, sleepHrsHigh: item.sleepHrsHigh!)
					completionHandler(goal, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func update(model: Goal, completionHandler: @escaping (Goal?, MovanoError?) -> Void) {
		let query = UpdateUserGoalsMutation(UpdateUserGoalsInput: model.updateGoalsInput)
		
		_ = self.graphQL.client!.perform(mutation: query) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.updateUserGoals {
					let goal = Goal(userID: item.userId, stepCount: item.stepCount!, caloriesBurned: item.caloriesBurned!, systolicLow: item.systolicLow!, systolicHigh: item.systolicHigh!, diastolicLow: item.diastolicLow!, diastolicHigh: item.diastolicHigh!, temperatureLow: item.temperatureLow!, temperatureHigh: item.temperatureHigh!, breathingRateLow: item.breathingRateLow!, breathingRateHigh: item.breathingRateHigh!, pulseRateLow: item.pulseRateLow!, pulseRateHigh: item.pulseRateHigh!, oxygenLow: item.spO2Low!, oxygenHigh: item.spO2High!, sleepHrsLow: item.sleepHrsLow!, sleepHrsHigh: item.sleepHrsHigh!)
					completionHandler(goal, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				} else {
					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				}
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func delete(completionHandler: @escaping (Goal?, MovanoError?) -> Void) {
		let query = DeleteUserGoalsMutation()
		
		_ = self.graphQL.client!.perform(mutation: query) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.deleteUserGoals {
					let goal = Goal(userID: item.userId, stepCount: item.stepCount!, caloriesBurned: item.caloriesBurned!, systolicLow: item.systolicLow!, systolicHigh: item.systolicHigh!, diastolicLow: item.diastolicLow!, diastolicHigh: item.diastolicHigh!, temperatureLow: item.temperatureLow!, temperatureHigh: item.temperatureHigh!, breathingRateLow: item.breathingRateLow!, breathingRateHigh: item.breathingRateHigh!, pulseRateLow: item.pulseRateLow!, pulseRateHigh: item.pulseRateHigh!, oxygenLow: item.spO2Low!, oxygenHigh: item.spO2High!, sleepHrsLow: item.sleepHrsLow!, sleepHrsHigh: item.sleepHrsHigh!)
					completionHandler(goal, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func fetch(completionHandler: @escaping (Goal?, MovanoError?) -> Void) {
		let query = GetUserGoalsQuery()
		
		_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.getUserGoals {
					let goal = Goal(userID: item.userId, stepCount: item.stepCount!, caloriesBurned: item.caloriesBurned!, systolicLow: item.systolicLow!, systolicHigh: item.systolicHigh!, diastolicLow: item.diastolicLow!, diastolicHigh: item.diastolicHigh!, temperatureLow: item.temperatureLow!, temperatureHigh: item.temperatureHigh!, breathingRateLow: item.breathingRateLow!, breathingRateHigh: item.breathingRateHigh!, pulseRateLow: item.pulseRateLow!, pulseRateHigh: item.pulseRateHigh!, oxygenLow: item.spO2Low!, oxygenHigh: item.spO2High!, sleepHrsLow: item.sleepHrsLow!, sleepHrsHigh: item.sleepHrsHigh!)
					UserDefault.shared.goals = goal
					completionHandler(goal, nil)
				} else {
					let goal = Goal()
					completionHandler(goal, nil)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
