//
//  StepCountWebService.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 20/07/21.
//

import Foundation

class StepCountWebService: StepCountWebServiceProtocol {
	
	private var graphQL: ApolloClientService
	private var service: RestService
	public static var refreshTokenCount = 0
	
	init(graphQL: ApolloClientService = ApolloClientService(),service: RestService = RestService.shared) {
		self.graphQL = graphQL
		self.service = service
	}
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataStepCount]?, MovanoError?) -> Void) {
        if(Utility.makeAPICall(timeIntervalRange: timestampRange) || !CoreDataStepCount.isReadingAvailable(range: timestampRange)){
            let query = QueryStepCountByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
            
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
                switch result {
                case .success(let graphQLResult):
                    if let items = graphQLResult.data?.queryStepCountByTimestampRange?.items {
                        
                        for each in items where each != nil {
                            CoreDataStepCount.add(timeInterval: each!.stepCountTimestamp, value: each!.stepCountValue, calories: each!.caloriesBurnt, distance: each!.distanceCovered)
                        }
                        let stepCountData = CoreDataStepCount.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                        completionHandler(stepCountData, nil)
                    } else {
                        completionHandler(nil, .invalidResponseModel)
                    }
                case .failure(let error):
                    if error.localizedDescription == "Received error response: Unauthorized" ||
                        error.localizedDescription == "Received error response: Token has expired." {
                        let stepCountData = CoreDataStepCount.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                        completionHandler(stepCountData, nil)
                    } else {
						let stepCountData = CoreDataStepCount.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
						completionHandler(stepCountData, nil)
//                        completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                    }
                    Logger.shared.addLog(error.localizedDescription)
                }
            }
        }else{
            let stepCountData = CoreDataStepCount.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            completionHandler(stepCountData, nil)
        }
	}
	
	func fetch(by timestamp: TimeInterval, completionHandler: @escaping (CoreDataStepCount?, MovanoError?) -> Void) {
        if(Utility.makeAPICall(timeInterval: timestamp) || !CoreDataStepCount.isReadingAvailable(for: Date(timeIntervalSince1970: timestamp))){
            let query = GetStepCountQuery(step_count_timestamp: timestamp)
            
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
                switch result {
                case .success(let graphQLResult):
                    if let items = graphQLResult.data?.getStepCount {
                        CoreDataStepCount.add(timeInterval: items.stepCountTimestamp, value: items.stepCountValue, calories: items.caloriesBurnt, distance: items.distanceCovered)
                        let reading = CoreDataStepCount.fetch(timeStamp: items.stepCountTimestamp)
                        completionHandler(reading, nil)
                    } else {
                        completionHandler(nil, .invalidResponseModel)
                    }
                case .failure(let error):
                    if error.localizedDescription == "Received error response: Unauthorized" ||
                        error.localizedDescription == "Received error response: Token has expired." {
                        let stepCountData = CoreDataStepCount.fetch(timeStamp: timestamp)
                        completionHandler(stepCountData, nil)
                    } else {
                        completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                    }
                    Logger.shared.addLog(error.localizedDescription)
                }
            }
        }else{
            let stepCountData = CoreDataStepCount.fetch(timeStamp: timestamp)
            completionHandler(stepCountData, nil)
        }
		
	}
	
	func fetchAll(completionHandler: @escaping ([CoreDataStepCount]?, MovanoError?) -> Void) {
		let query = ListStepCountsQuery()
		
		_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let items = graphQLResult.data?.listStepCounts?.items {
					for each in items where each != nil {
						CoreDataStepCount.add(timeInterval: each!.stepCountTimestamp, value: each!.stepCountValue, calories: each!.caloriesBurnt, distance: each!.distanceCovered)
					}
					let readings = CoreDataStepCount.fetch()
					completionHandler(readings, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func delete(model: StepCount, completionHandler: @escaping (Bool?, MovanoError?) -> Void) {
		let mutation = DeleteStepCountMutation(DeleteStepCountInput: DeleteStepCountInput(stepCountTimestamp: model.timestamp))
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.deleteStepCount {
					CoreDataStepCount.delete(for: Date(timeIntervalSince1970: item.stepCountTimestamp))
					completionHandler(true, nil)
				} else {
					completionHandler(false, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func add(model: StepCount, completionHandler: @escaping (CoreDataStepCount?, MovanoError?) -> Void) {
		
		let mutation = CreateStepCountMutation(CreateStepCountInput: CreateStepCountInput(stepCountTimestamp: model.timestamp, stepCountValue: model.stepCount))
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.createStepCount {
					CoreDataStepCount.add(timeInterval: item.stepCountTimestamp, value: item.stepCountValue, calories: item.caloriesBurnt, distance: item.distanceCovered)
					let reading = CoreDataStepCount.fetch(timeStamp: item.stepCountTimestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func update(model: StepCount, completionHandler: @escaping (CoreDataStepCount?, MovanoError?) -> Void) {
		let mutation = UpdateStepCountMutation(UpdateStepCountInput: UpdateStepCountInput(stepCountTimestamp: model.timestamp, stepCountValue: model.stepCount))
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.updateStepCount {
					CoreDataStepCount.add(timeInterval: item.stepCountTimestamp, value: item.stepCountValue, calories: item.caloriesBurnt, distance: item.distanceCovered)
					let reading = CoreDataStepCount.fetch(timeStamp: item.stepCountTimestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func add(models: [StepCount], completionHandler: @escaping ([CoreDataStepCount]?, MovanoError?) -> Void) {
		var data = [CreateStepCountInput]()
		for each in models {
			data.append(CreateStepCountInput(stepCountTimestamp: each.timestamp, stepCountValue: each.stepCount))
		}
		let mutation = CreateMultipleStepCountMutation(CreateStepCountInput: data)
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let items = graphQLResult.data?.createMultipleStepCount {
					for each in items where each != nil {
						CoreDataStepCount.add(timeInterval: each!.stepCountTimestamp, value: each!.stepCountValue, calories: each!.caloriesBurnt, distance: each!.distanceCovered)
					}
                    
                    if let firstItem = items.first, let timestamp = firstItem?.stepCountTimestamp{
                        let reading = CoreDataStepCount.fetch(for: DateRange(Date(timeIntervalSince1970: timestamp), type: .daily).dateRange)
                        completionHandler(reading, nil)
                    }else{
                        completionHandler(nil, nil)
                    }
					
				} else {
					Logger.shared.addLog("Current time is -\(Date())")
					Logger.shared.addLog( "Data is not in [CreateMultipleStepCount] format")
					Logger.shared.addLog("Graphql result for step count \(graphQLResult.data?.createMultipleStepCount)")
					completionHandler(nil, nil)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					if StepCountWebService.refreshTokenCount == 0 {
						StepCountWebService.refreshTokenCount = 1
						self.requestAccessToken { [weak self] response, error in
							if (response != nil) {
								self?.graphQL.setDefaultClient()
								self?.add(models: models, completionHandler: completionHandler)
							}
							if let error = error {
								completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
							}
						}
					}
				} else {
					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				}
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
    
    func fetchActivtyIntensity(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataActivity]?, MovanoError?) -> Void){
		if(Utility.makeAPICall(timeIntervalRange:timestampRange) || !CoreDataActivity.isReadingAvailable(range: timestampRange)){
	  let query = QueryActivityIntensityByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
	  
	  _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
		  switch result {
		  case .success(let graphQLResult):
			  if let items = graphQLResult.data?.queryActivityIntensityByTimestampRange?.items {
				  for each in items where each != nil {
					  CoreDataActivity.add(timeInterval: each!.dataTimestamp, value: Double(each!.value))
				  }
				  let activityData = CoreDataActivity.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
				  completionHandler(activityData, nil)
			  } else {
				  completionHandler(nil, .invalidResponseModel)
			  }
		  case .failure(let error):
			  if error.localizedDescription == "Received error response: Unauthorized" ||
				  error.localizedDescription == "Received error response: Token has expired." {
				  
			  } else {
				  
			  }
			  let activityData = CoreDataActivity.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
			  completionHandler(activityData, nil)
//			  completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
			  Logger.shared.addLog(error.localizedDescription)
		  }
	  }
  }else{
	  let activityData = CoreDataActivity.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
	  completionHandler(activityData, nil)
  }
}
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
