//
//  StepCountWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 20/07/21.
//

import Foundation

protocol StepCountWebServiceProtocol {
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataStepCount]?, MovanoError?) -> Void)
	
	func fetch(by timestamp: TimeInterval, completionHandler: @escaping (CoreDataStepCount?, MovanoError?) -> Void)
	
	func fetchAll(completionHandler: @escaping ([CoreDataStepCount]?, MovanoError?) -> Void)
	
	func add(model: StepCount, completionHandler: @escaping (CoreDataStepCount?, MovanoError?) -> Void)
	
	func add(models: [StepCount], completionHandler: @escaping ([CoreDataStepCount]?, MovanoError?) -> Void)
	
	func update(model: StepCount, completionHandler: @escaping (CoreDataStepCount?, MovanoError?) -> Void)
	
	func delete(model: StepCount, completionHandler: @escaping (Bool?, MovanoError?) -> Void)
    
    func fetchActivtyIntensity(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataActivity]?, MovanoError?) -> Void)

}
