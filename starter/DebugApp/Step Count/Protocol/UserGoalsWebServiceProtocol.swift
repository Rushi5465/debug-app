//
//  UserGoalsWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 24/08/21.
//

import Foundation

protocol UserGoalsWebServiceProtocol {
	
	func add(model: Goal, completionHandler: @escaping (Goal?, MovanoError?) -> Void)
	
	func update(model: Goal, completionHandler: @escaping (Goal?, MovanoError?) -> Void)
	
	func delete(completionHandler: @escaping (Goal?, MovanoError?) -> Void)
	
	func fetch(completionHandler: @escaping (Goal?, MovanoError?) -> Void)
}
