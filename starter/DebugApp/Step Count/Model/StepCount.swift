//
//  StepCount.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 18/08/21.
//

import Foundation

public class StepCount:NSObject, NSCoding{
	
	public var timestamp: TimeInterval = 0
    public var stepCount: Int = 0
    
    enum Key:String{
        case timestamp = "timestamp"
        case stepCount = "stepCount"
    }
    
    public override init() {
        super.init()
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(timestamp,forKey: Key.timestamp.rawValue)
        aCoder.encode(stepCount,forKey: Key.stepCount.rawValue)

    }
  
    required public convenience init(coder aDecoder: NSCoder)  {
        let mTimestamp = aDecoder.decodeDouble(forKey: Key.timestamp.rawValue)
        let mStepCount = aDecoder.decodeInteger(forKey: Key.stepCount.rawValue)
        self.init(timeStamp:mTimestamp, stepCount:mStepCount)
    }
    
    init(timeStamp:TimeInterval, stepCount:Int) {
        self.timestamp = timeStamp
        self.stepCount = stepCount
    }
}

public class StepCountCoreData:NSObject, NSCoding{
    public var stepCount : [StepCount] = []
    
    enum Key:String{
        case stepCount = "stepCount"
    }
    
    init(stepCount:[StepCount]){
        self.stepCount = stepCount
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(stepCount,forKey: Key.stepCount.rawValue)
    }
    
    required public convenience init(coder aDecoder: NSCoder)  {
        let mStepCount = aDecoder.decodeObject(forKey: Key.stepCount.rawValue) as! [StepCount]
        
        self.init(stepCount:mStepCount)
    }
}

