//
//  PulseRateWebService.swift
//  BloodPressureApp
//
//  Created by Rushikant on 20/07/21.
//

import Foundation
// swiftlint:disable all
class PulseRateWebService: PulseRateWebServiceProtocol {
	
	private var graphQL: ApolloClientService
	private var service: RestService
	public static var refreshTokenCount = 0
	
	init(service: RestService = RestService.shared, graphQL: ApolloClientService = ApolloClientService()) {
		self.graphQL = graphQL
		self.service = service
	}
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataPulseRate]?, MovanoError?) -> Void) {
        if(Utility.makeAPICall(timeIntervalRange: timestampRange) || !CoreDataPulseRate.isReadingAvailable(range: timestampRange)){
            
            let query = QueryPulseRateByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
            
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
                switch result {
                case .success(let graphQLResult):
                    if let items = graphQLResult.data?.queryPulseRateByTimestampRange?.items {
                        for each in items where each != nil {
                            CoreDataPulseRate.add(timeInterval: each!.pulseRateTimestamp, value: each!.pulseRateValue)
                        }
                        let pulseRateData = CoreDataPulseRate.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                        completionHandler(pulseRateData, nil)
                    } else {
                        completionHandler(nil, .invalidResponseModel)
                    }
                case .failure(let error):
                    if error.localizedDescription == "Received error response: Unauthorized" ||
                        error.localizedDescription == "Received error response: Token has expired." {
                        
                    } else {
                        
                    }
					let pulseRateData = CoreDataPulseRate.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
					completionHandler(pulseRateData, nil)
//					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                    Logger.shared.addLog(error.localizedDescription)
                }
            }
        }else{
            let pulseRateData = CoreDataPulseRate.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            completionHandler(pulseRateData, nil)
        }
	}
	
	func fetch(by timestamp: Double, completionHandler: @escaping (CoreDataPulseRate?, MovanoError?) -> Void) {
		let query = GetPulseRateQuery(pulse_rate_timestamp: timestamp)
		
		_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let items = graphQLResult.data?.getPulseRate {
					CoreDataPulseRate.add(timeInterval: items.pulseRateTimestamp, value: items.pulseRateValue)
					let reading = CoreDataPulseRate.fetch(timeStamp: items.pulseRateTimestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				let reading = CoreDataPulseRate.fetch(timeStamp: timestamp)
				completionHandler(reading, nil)
//				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func fetchAll(completionHandler: @escaping ([CoreDataPulseRate]?, MovanoError?) -> Void) {
		let query = ListPulseRatesQuery()
		
		_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let items = graphQLResult.data?.listPulseRates?.items {
					for each in items where each != nil {
						CoreDataPulseRate.add(timeInterval: each!.pulseRateTimestamp, value: each!.pulseRateValue)
					}
					let readings = CoreDataPulseRate.fetch()
					completionHandler(readings, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func delete(model: PulseRate, completionHandler: @escaping (Bool?, MovanoError?) -> Void) {
		let query = DeletePulseRateMutation(DeletePulseRateInput: DeletePulseRateInput(pulseRateTimestamp: model.timestamp))
		
		_ = self.graphQL.client!.perform(mutation: query) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.deletePulseRate {
					CoreDataPulseRate.delete(for: Date(timeIntervalSince1970: item.pulseRateTimestamp))
					completionHandler(true, nil)
				} else {
					completionHandler(false, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
		
	}
	
	func add(model: PulseRate, completionHandler: @escaping (CoreDataPulseRate?, MovanoError?) -> Void) {
		let query = CreatePulseRateMutation(CreatePulseRateInput: CreatePulseRateInput(pulseRateTimestamp: model.timestamp, pulseRateValue: model.value))
		
		_ = self.graphQL.client!.perform(mutation: query) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.createPulseRate {
					CoreDataPulseRate.add(timeInterval: item.pulseRateTimestamp, value: item.pulseRateValue)
					let reading = CoreDataPulseRate.fetch(timeStamp: item.pulseRateTimestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func update(model: PulseRate, completionHandler: @escaping (CoreDataPulseRate?, MovanoError?) -> Void) {
		let query = UpdatePulseRateMutation(UpdatePulseRateInput: UpdatePulseRateInput(pulseRateTimestamp: model.timestamp, pulseRateValue: model.value))
		
		_ = self.graphQL.client!.perform(mutation: query) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.updatePulseRate {
					CoreDataOxygen.add(timeInterval: item.pulseRateTimestamp, value: item.pulseRateValue)
					let reading = CoreDataPulseRate.fetch(timeStamp: item.pulseRateTimestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func add(models: [PulseRate], completionHandler: @escaping ([CoreDataPulseRate]?, MovanoError?) -> Void) {
		var data = [CreatePulseRateInput]()
		for each in models {
			data.append(CreatePulseRateInput(pulseRateTimestamp: each.timestamp, pulseRateValue: each.value))
		}
		let mutation = CreateMultiplePulseRateMutation(CreatePulseRateInput: data)
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let items = graphQLResult.data?.createMultiplePulseRate {
					for each in items where each != nil {
						CoreDataPulseRate.add(timeInterval: each!.pulseRateTimestamp, value: each!.pulseRateValue)
					}
					if items.first! != nil {
						let reading = CoreDataPulseRate.fetch(for: DateRange(Date(timeIntervalSince1970: items.first!!.pulseRateTimestamp), type: .daily).dateRange)
						completionHandler(reading, nil)
					} else {
						completionHandler(nil, nil)
					}
				} else {
					Logger.shared.addLog("Current time is -\(Date())")
					Logger.shared.addLog( "Data is not in [CreateMultiplePulseRate] format")
					Logger.shared.addLog( "Graphql result of - \(graphQLResult.data?.createMultiplePulseRate)")
					completionHandler(nil, nil)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					if PulseRateWebService.refreshTokenCount == 0 {
						PulseRateWebService.refreshTokenCount = 1
						self.requestAccessToken { [weak self] response, error in
							if (response != nil) {
								self?.graphQL.setDefaultClient()
								self?.add(models: models, completionHandler: completionHandler)
							}
							if let error = error {
								completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
							}
						}
					}
				} else {
					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				}
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func fetchIntensityPulseData(weekStartdate: String, weekEndDate: String, completionHandler: @escaping ([CoreDataActivityIntensity]?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.fetchIntensity)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		var header = Header()
		header.addHeader(key: "Authorization", value: KeyChain.shared.idToken)
		header.addHeader(key: "AccessToken", value: KeyChain.shared.accessToken!)
		
		let parameters: [String: Any] = ["week_start": weekStartdate,
										 "week_end": weekEndDate]
		let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters)
		
		service.request(apiType: APIType.FETCH_INTENSITY_DATA, request: request) { (result) in
			switch result {
			case .success(let response):
				do {
					let responseModel = try JSONDecoder().decode([ActivityIntensityModel].self, from: response.rawData())
					for x in 0..<responseModel.count {
						let timeInterval = responseModel[x].date.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970
						CoreDataActivityIntensity.add(timeInterval: timeInterval ?? 0, avgActivityIntensity: responseModel[x].avg_activity_intensity ?? 0, avgPulseRate: responseModel[x].avg_pulse_rate ?? 0)
					}
					let newResponselModel = CoreDataActivityIntensity.fetch(for: Date(timeIntervalSince1970: weekStartdate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0) ..< Date(timeIntervalSince1970: weekEndDate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0))
					completionHandler(newResponselModel, nil)
				} catch let error {
					print("\(error)")
					let newResponselModel = CoreDataActivityIntensity.fetch(for: Date(timeIntervalSince1970: weekStartdate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0) ..< Date(timeIntervalSince1970: weekEndDate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0))
					completionHandler(newResponselModel, nil)
//					completionHandler(nil, MovanoError.invalidResponseModel)
				}
//				if let responseModel = try? JSONDecoder().decode([ActivityIntensityModel].self, from: response.rawData()) {
//					completionHandler(responseModel, nil)
//				} else {
//					completionHandler(nil, MovanoError.invalidResponseModel)
//				}
			case .failure(let error):
				let newResponselModel = CoreDataActivityIntensity.fetch(for: Date(timeIntervalSince1970: weekStartdate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0) ..< Date(timeIntervalSince1970: weekEndDate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0))
				completionHandler(newResponselModel, nil)
//				completionHandler(nil, error)
			}
		}
	}
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
