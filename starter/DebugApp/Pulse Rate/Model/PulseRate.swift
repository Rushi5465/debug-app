//
//  PulseRate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 09/09/21.
//

import Foundation

public class PulseRate:NSObject, NSCoding {
    public var timestamp: TimeInterval = 0
    public var value: Int = 0
    
    enum Key:String{
        case timestamp = "timestamp"
        case value = "value"
    }
    
    public override init() {
        super.init()
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(timestamp,forKey: Key.timestamp.rawValue)
        aCoder.encode(value,forKey: Key.value.rawValue)

    }
  
    required public convenience init(coder aDecoder: NSCoder)  {
        let mTimestamp = aDecoder.decodeDouble(forKey: Key.timestamp.rawValue)
        let mValue = aDecoder.decodeInteger(forKey: Key.value.rawValue)
        self.init(timeStamp:mTimestamp, value:mValue)
    }
    
    init(timeStamp:TimeInterval, value:Int) {
        self.timestamp = timeStamp
        self.value = value
    }
}

public class PulseRateCoreData:NSObject, NSCoding{
    public var pulseRate : [PulseRate] = []
    
    enum Key:String{
        case pulseRate = "pulseRate"
    }
    
    init(pulseRate:[PulseRate]){
        self.pulseRate = pulseRate
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(pulseRate,forKey: Key.pulseRate.rawValue)
    }
    
    required public convenience init(coder aDecoder: NSCoder)  {
        let mPulseRate = aDecoder.decodeObject(forKey: Key.pulseRate.rawValue) as! [PulseRate]
        self.init(pulseRate:mPulseRate)
    }
}

