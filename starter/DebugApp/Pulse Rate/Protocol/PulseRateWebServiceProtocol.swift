//
//  PulseRateWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 20/07/21.
//

import Foundation

protocol PulseRateWebServiceProtocol {
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataPulseRate]?, MovanoError?) -> Void)
	
	func fetch(by timestamp: Double, completionHandler: @escaping (CoreDataPulseRate?, MovanoError?) -> Void)
	
	func fetchAll(completionHandler: @escaping ([CoreDataPulseRate]?, MovanoError?) -> Void)
	
	func add(model: PulseRate, completionHandler: @escaping (CoreDataPulseRate?, MovanoError?) -> Void)
	
	func add(models: [PulseRate], completionHandler: @escaping ([CoreDataPulseRate]?, MovanoError?) -> Void)
	
	func update(model: PulseRate, completionHandler: @escaping (CoreDataPulseRate?, MovanoError?) -> Void)
	
	func delete(model: PulseRate, completionHandler: @escaping (Bool?, MovanoError?) -> Void)
	
	func fetchIntensityPulseData(weekStartdate: String, weekEndDate: String, completionHandler: @escaping ([CoreDataActivityIntensity]?, MovanoError?) -> Void)
	
//	func fetchAverageData(forDate:String,completionHandler: @escaping (SleepAverageDataModel?, MovanoError?) -> Void)
}
