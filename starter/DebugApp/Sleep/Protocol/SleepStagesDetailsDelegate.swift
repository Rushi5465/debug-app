//
//  SleepStagesDetailsDelegate.swift
//  BloodPressureApp
//
//  Created by Rushikant on 15/09/21.
//

import Foundation

protocol SleepStagesDetailsDelegate: AnyObject {
	
	func fetchSleepStageSummary(chartElements: [ChartElement], value: Int, chartElementsForWeek:[ChartElement])
	func fetchSleepStageSummaryForWeek(chartElements: [[ChartElement]], value: Int)
	func fetchSleepSummary(result: Result<SleepStageModel, MovanoError>)
	func fetchOxygenData(result: Result<[CoreDataOxygen], MovanoError>, customDateRange: DateRange)
	func sleepDataTiming(sleepStartTime: String?, sleepEndTime: String?)
	func afterSleepDataResponseFetched(result: Result<[SleepDataModel], MovanoError>)
	func fetchSleepStageSummaryForWeek(error: MovanoError)
}
