//
//  SleepDetailDelegate.swift
//  BloodPressureApp
//
//  Created by Rushikant on 01/09/21.
//

import Foundation

protocol SleepDetailDelegate: AnyObject {
	
	func fetchPulseRateData(result: Result<[CoreDataPulseRate], MovanoError>, customDateRange: DateRange)
	func fetchBreathingRateData(result: Result<[CoreDataBreathingRate], MovanoError>, customDateRange: DateRange)
	func fetchTemperatureData(result: Result<[CoreDataSkinTemperature], MovanoError>, customDateRange: DateRange)
	func fetchOxygenData(result: Result<[CoreDataOxygen], MovanoError>, customDateRange: DateRange)
	func fetchBPData(result: Result<[CoreDataBPReading], MovanoError>, customDateRange: DateRange)
	func fetchHRVData(result: Result<[CoreDataHRV], MovanoError>, customDateRange: DateRange)
	func sleepDataTiming(sleepStartTime: String?, sleepEndTime: String?)
	func afterSleepDataResponseFetched(result: Result<[SleepDataModel], MovanoError>)
	func updateBreathingRateChart(breathingRateAvg: Double, customDateRange: DateRange, type: DateRangeType)
	func updatePulserateChart(pulseCountAvg: Double, customDateRange: DateRange, type: DateRangeType)
	func updateTempChart(tempAvg: Double, customDateRange: DateRange, type: DateRangeType)
	func updateOxygenChart(oxygenAvg: Double, customDateRange: DateRange, type: DateRangeType)
	func updateHRVChart(hrvAvg: Double, customDateRange: DateRange, type: DateRangeType)
	func updateBPChart(bpDiastolicAvg: Double, bpSystolicAvg: Double, customDateRange: DateRange, type: DateRangeType)
	func errorReceivedWhileChartData(isChartDataAvailable: Bool, dataType: DetailDataType!)
}
