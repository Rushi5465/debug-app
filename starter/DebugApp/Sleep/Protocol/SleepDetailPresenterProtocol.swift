//
//  SleepDetailPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 01/09/21.
//

import Foundation

// swiftlint:disable all
protocol SleepDetailPresenterProtocol {
	init(breathingRateService: BreathingRateWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, temperatureService: SkinTemperatureWebServiceProtocol, oxygenService: OxygenDataWebServiceProtocol, bpService: BloodPressureWebServiceProtocol, sleepService: SleepWebServiceProtocol, hrvService: HRVWebServiceProtocol, delegate: SleepDetailDelegate)
	
	func fetchBreathingRateData(timeStampRange: Range<Double>, customDateRange: DateRange)
	func fetchTemperatureData(timeStampRange: Range<Double>, customDateRange: DateRange)
	func fetchOxygenData(timeStampRange: Range<Double>, customDateRange: DateRange)
	func fetchPulseRateData(timeStampRange: Range<Double>, customDateRange: DateRange)
	func fetchBPReadingData(timeStampRange: Range<Double>, customDateRange: DateRange)
	func fetchHRVData(timeStampRange: Range<Double>, customDateRange: DateRange)
}
