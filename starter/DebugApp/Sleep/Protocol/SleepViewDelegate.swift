//
//  SleepViewDelegate.swift
//  BloodPressureApp
//
//  Created by Rushikant on 26/08/21.
//

import Foundation

protocol SleepViewDelegate: AnyObject {
	
	func bpChart(chartElements: [ChartElement], systolic: Int?, diastolic: Int?,metrciModel:MetricIndicatorModel?)
    func pulseRate(average value: Int?, unit: String,metrciModel:MetricIndicatorModel?)
	func oxygenSaturation(average value: Int?, unit: String,metrciModel:MetricIndicatorModel?)
	func skinTemperature(average value: Double?, unit: String, isPositive: Bool,metrciModel:MetricIndicatorModel?)
	func breathingRate(average value: Int?, unit: String,metrciModel:MetricIndicatorModel?)
	func hrvRate(average value: Int?, unit: String,metrciModel:MetricIndicatorModel?)
	func sleepStageChart(chartElements: [ChartElement], value: Int)
	func sleepDataTiming(sleepStartTime: String?, sleepEndTime: String?)
	func afterSleepDataResponseFetched(result: Result<[SleepDataModel], MovanoError>)
    func sleepStageDataFetched(result: Result<[CoreDataSleepStage],MovanoError>)
	func parameter(list: [SleepParameter])
}
