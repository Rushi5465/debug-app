//
//  SleepStagesDetailPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 14/09/21.
//

import UIKit
protocol SleepStagesDetailPresenterProtocol {
	init(sleepStageService: SleepStageDetailWebService, oxygenService: OxygenDataWebServiceProtocol, delegate: SleepStagesDetailsDelegate)
}
