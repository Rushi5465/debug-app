//
//  SleepStagesWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 14/09/21.
//

import UIKit

protocol SleepStagesWebServiceProtocol {
	
	func fetchSleepStageSummary(dateRange: Range<TimeInterval>, dataType: String, date: String, completionHandler: @escaping (SleepStageModel?, MovanoError?) -> Void)
	func getSleepDataByTimeStamp(timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataSleepStage]?, MovanoError?) -> Void)
	func fetchAverageData(forDate:String,completionHandler: @escaping (SleepAverageDataModel?, MovanoError?) -> Void)
	func fetchData(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataSleepTime]?, MovanoError?) -> Void)
}
