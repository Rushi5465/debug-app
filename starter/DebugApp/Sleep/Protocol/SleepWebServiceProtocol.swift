//
//  SleepWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 26/08/21.
//

import UIKit

protocol SleepWebServiceProtocol {
	
	func fetch(by range: DateRange, completionHandler: @escaping ([CoreDataSleepStage]?, MovanoError?) -> Void)
	func fetchAverageData(forDate:String,completionHandler: @escaping (SleepAverageDataModel?, MovanoError?) -> Void)
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataSleepTime]?, MovanoError?) -> Void)
	func fetchData(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataSleepTime]?, MovanoError?) -> Void)
}
