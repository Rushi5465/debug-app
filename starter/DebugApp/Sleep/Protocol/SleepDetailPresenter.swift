//
//  SleepDetailPresenter.swift
//  BloodPressureApp
//
//  Created by Rushikant on 01/09/21.
//

import UIKit

// swiftlint:disable all
class SleepDetailPresenter: SleepDetailPresenterProtocol {
	
	private weak var delegate: SleepDetailDelegate?
	private var breathingRateService: BreathingRateWebServiceProtocol
	private var pulseRateService: PulseRateWebServiceProtocol
	private var temperatureService: SkinTemperatureWebServiceProtocol
	private var oxygenService: OxygenDataWebServiceProtocol
	private var bpService: BloodPressureWebServiceProtocol
	private var sleepService: SleepWebServiceProtocol
	private var hrvService: HRVWebServiceProtocol
	
	required init(breathingRateService: BreathingRateWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, temperatureService: SkinTemperatureWebServiceProtocol, oxygenService: OxygenDataWebServiceProtocol, bpService: BloodPressureWebServiceProtocol, sleepService: SleepWebServiceProtocol, hrvService: HRVWebServiceProtocol, delegate: SleepDetailDelegate) {
		self.breathingRateService = breathingRateService
		self.pulseRateService = pulseRateService
		self.temperatureService = temperatureService
		self.oxygenService = oxygenService
		self.bpService = bpService
		self.sleepService = sleepService
		self.hrvService = hrvService

		self.delegate = delegate
	}
	
	func fetchBreathingRateData(timeStampRange: Range<Double>, customDateRange: DateRange) {
		self.breathingRateService.fetch(by: timeStampRange) { [weak self] (item, error) in
			if let error = error {
				self?.delegate?.fetchBreathingRateData(result: .failure(error), customDateRange: customDateRange)
				return
			}
			
			if let item = item {
				self?.delegate?.fetchBreathingRateData(result: .success(item), customDateRange: customDateRange)
			}
		}
	}
	
	func fetchHRVData(timeStampRange: Range<Double>, customDateRange: DateRange) {
		self.hrvService.fetch(by: timeStampRange) { [weak self] (item, error) in
			if let error = error {
				self?.delegate?.fetchHRVData(result: .failure(error), customDateRange: customDateRange)
				return
			}
			
			if let item = item {
				self?.delegate?.fetchHRVData(result: .success(item), customDateRange: customDateRange)
			}
		}
	}
	
	func fetchPulseRateData(timeStampRange: Range<Double>, customDateRange: DateRange) {
		self.pulseRateService.fetch(by: timeStampRange) { [weak self] (item, error) in
			if let error = error {
				self?.delegate?.fetchPulseRateData(result: .failure(error), customDateRange: customDateRange)
				return
			}
			
			if let item = item {
				self?.delegate?.fetchPulseRateData(result: .success(item), customDateRange: customDateRange)
			}
		}
		
	}
	
	func fetchTemperatureData(timeStampRange: Range<Double>, customDateRange: DateRange) {
		self.temperatureService.fetch(by: timeStampRange) { [weak self] (item, error) in
			if let error = error {
				self?.delegate?.fetchTemperatureData(result: .failure(error), customDateRange: customDateRange)
				return
			}
			
			if let item = item {
				self?.delegate?.fetchTemperatureData(result: .success(item), customDateRange: customDateRange)
			}
		}
	}
	
	func fetchOxygenData(timeStampRange: Range<Double>, customDateRange: DateRange) {
		self.oxygenService.fetch(by: timeStampRange) { [weak self] (item, error) in
			if let error = error {
				self?.delegate?.fetchOxygenData(result: .failure(error), customDateRange: customDateRange)
				return
			}
			
			if let item = item {
				self?.delegate?.fetchOxygenData(result: .success(item), customDateRange: customDateRange)
			}
		}
	}
	
	func fetchBPReadingData(timeStampRange: Range<Double>, customDateRange: DateRange) {
		self.bpService.listBloodPressures(range: timeStampRange) { [weak self] (item, error) in
			if let error = error {
				self?.delegate?.fetchBPData(result: .failure(error), customDateRange: customDateRange)
				return
			}
			
			if let item = item {
				self?.delegate?.fetchBPData(result: .success(item), customDateRange: customDateRange)
			}
		}
	}
	
	func fetchSleepTimeData(forDate: Date) {
		self.sleepService.fetchAverageData(forDate:forDate.dateToString(format: DateFormat.serverDateFormat)) { sleepRepo, error in
			if let error = error {
				self.delegate?.afterSleepDataResponseFetched(result: .failure(error))
				return
			}
			
			if let sleepRepo = sleepRepo {
				self.delegate?.sleepDataTiming(sleepStartTime: sleepRepo.sleep_start_time, sleepEndTime: sleepRepo.sleep_end_time)
			}
		}
	}
	
	func fetchSleepTimeDataWeek(forDate: Date) {
		self.sleepService.fetchAverageData(forDate:forDate.dateToString(format: DateFormat.serverDateFormat)) { sleepRepo, error in
			if let error = error {
				self.delegate?.afterSleepDataResponseFetched(result: .failure(error))
				return
			}
			
			if let sleepRepo = sleepRepo {
//				self.delegate?.sleepDataTiming(sleepStartTime: sleepRepo.sleep_start_time, sleepEndTime: sleepRepo.sleep_end_time)
			}
		}
	}
	
	func fetchSleepDataForWeek(breathingRateAvg: Double, bpDiastolicAvg: Double? = 0, bpSystolicAvg: Double? = 0, forRange:Range<Double>, type: DateRangeType, dataType: DetailDataType!) {
		self.sleepService.fetchData(by: forRange) { sleepRepo, error in
			if let error = error {
				self.delegate?.errorReceivedWhileChartData(isChartDataAvailable: false, dataType: dataType)
				print(error)
			}
			
			if let sleepRepo = sleepRepo {
				self.delegate?.errorReceivedWhileChartData(isChartDataAvailable: true, dataType: dataType)
				if dataType == .breathingRateReading {
					self.delegate?.updateBreathingRateChart(breathingRateAvg: breathingRateAvg, customDateRange: DateRange(start: Date(timeIntervalSince1970: forRange.lowerBound), end: Date(timeIntervalSince1970: forRange.upperBound), type: type), type: type)
				} else if dataType == .heartRateReading {
					self.delegate?.updatePulserateChart(pulseCountAvg: breathingRateAvg, customDateRange: DateRange(start: Date(timeIntervalSince1970: forRange.lowerBound), end: Date(timeIntervalSince1970: forRange.upperBound), type: type), type: type)
				} else if dataType == .spO2Reading {
					self.delegate?.updateOxygenChart(oxygenAvg: breathingRateAvg, customDateRange: DateRange(start: Date(timeIntervalSince1970: forRange.lowerBound), end: Date(timeIntervalSince1970: forRange.upperBound), type: type), type: type)
				} else if dataType == .tempratureReading {
					self.delegate?.updateTempChart(tempAvg: breathingRateAvg, customDateRange: DateRange(start: Date(timeIntervalSince1970: forRange.lowerBound), end: Date(timeIntervalSince1970: forRange.upperBound), type: type), type: type)
				} else if dataType == .hrvReading {
					self.delegate?.updateHRVChart(hrvAvg: breathingRateAvg, customDateRange: DateRange(start: Date(timeIntervalSince1970: forRange.lowerBound), end: Date(timeIntervalSince1970: forRange.upperBound), type: type), type: type)
				} else if dataType == .bpReading {
					self.delegate?.updateBPChart(bpDiastolicAvg: bpDiastolicAvg ?? 0, bpSystolicAvg: bpSystolicAvg ?? 0, customDateRange: DateRange(start: Date(timeIntervalSince1970: forRange.lowerBound), end: Date(timeIntervalSince1970: forRange.upperBound), type: type), type: type)
				}
			}
		}
	}
}
