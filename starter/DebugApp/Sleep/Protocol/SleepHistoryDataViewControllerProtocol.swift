//
//  SleepHistoryDataViewControllerProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 21/10/21.
//

import UIKit

protocol SleepHistoryDataViewControllerProtocol {
	func setParameterData(parameter: [SleepParameter])
}
