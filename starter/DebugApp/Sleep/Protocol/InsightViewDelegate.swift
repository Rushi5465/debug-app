//
//  InsightViewDelegate.swift
//  BloodPressureApp
//
//  Created by Rushikant on 29/10/21.
//

import Foundation

protocol InsightViewDelegate: AnyObject {
	
	func fetchInsightList(list: [InsightModel])
	func fetchSleepStageSummary(chartElements: [ChartElement], value: Int)
}
