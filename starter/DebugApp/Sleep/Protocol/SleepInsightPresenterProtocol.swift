//
//  SleepInsightPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 29/10/21.
//

import Foundation

// swiftlint:disable all
protocol SleepInsightPresenterProtocol {
	
	init(breathingRateService: BreathingRateWebServiceProtocol, oxygenService: OxygenDataWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, sleepService: SleepStagesWebServiceProtocol, hrvService:HRVWebServiceProtocol ,delegate: InsightViewDelegate)

	func initialiseCardiacModel()
	func initializePulmonaryModel()
	func fetchPulmonaryData(range: DateRange)
	func fetchCardiacData(range: DateRange)
	func fetchSleepStages(range: DateRange)
	func fetchBreathingRateData(range: DateRange)
	func fetchOxygenData(range: DateRange)
	func fetchHeartRateData(range: DateRange)
	func fetchHRVData(range: DateRange)
}
