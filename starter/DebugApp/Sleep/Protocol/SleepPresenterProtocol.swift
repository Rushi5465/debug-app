//
//  SleepPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 26/08/21.
//

import Foundation

// swiftlint:disable all
protocol SleepPresenterProtocol {
	
	init(sleepService: SleepWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, oxygenService: OxygenDataWebServiceProtocol, temperatureService: SkinTemperatureWebServiceProtocol, breathingRateService: BreathingRateWebServiceProtocol, bloodPressureService: BloodPressureWebServiceProtocol, hrvService:HRVWebServiceProtocol, delegate: SleepViewDelegate)
	
	func fetchData(range: DateRange)
	func fetchPulseRate(range: DateRange)
	func fetchOxygenData(range: DateRange)
	func fetchSkinTemperature(range: DateRange)
	func fetchBreathingRate(range: DateRange)
}
