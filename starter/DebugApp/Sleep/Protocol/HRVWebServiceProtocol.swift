//
//  HRVWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 14/11/21.
//

import Foundation

protocol HRVWebServiceProtocol {
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataHRV]?, MovanoError?) -> Void)
}
