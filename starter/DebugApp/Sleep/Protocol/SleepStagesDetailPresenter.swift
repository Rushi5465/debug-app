//
//  SleepStagesDetailPresenter.swift
//  BloodPressureApp
//
//  Created by Rushikant on 14/09/21.
//

import UIKit

// swiftlint:disable all
class SleepStagesDetailPresenter: SleepStagesDetailPresenterProtocol {
	private weak var delegate: SleepStagesDetailsDelegate?
	private var sleepStageService: SleepStageDetailWebService
	private var oxygenService: OxygenDataWebServiceProtocol
	
	required init(sleepStageService: SleepStageDetailWebService, oxygenService: OxygenDataWebServiceProtocol, delegate: SleepStagesDetailsDelegate) {
		self.sleepStageService = sleepStageService
		self.oxygenService = oxygenService
		
		self.delegate = delegate
	}
	
	func fetchOxygenData(timeStampRange: Range<Double>, customDateRange: DateRange) {
		self.oxygenService.fetch(by: timeStampRange) { [weak self] (item, error) in
			if let error = error {
				self?.delegate?.fetchOxygenData(result: .failure(error), customDateRange: customDateRange)
				return
			}
			
			if let item = item {
				self?.delegate?.fetchOxygenData(result: .success(item), customDateRange: customDateRange)
			}
		}
	}
	
	func fetchSleepSummary(timeStampRange: Range<Double>, dataType: String, date: String) {
		self.sleepStageService.fetchSleepStageSummary(dateRange: timeStampRange, dataType: dataType, date: date) { [weak self] (item, error) in
			if let error = error {
				self?.delegate?.fetchSleepSummary(result: .failure(error))
				return
			}
			
			if let item = item {
				self?.delegate?.fetchSleepSummary(result: .success(item))
			}
		}
	}
	
	func fetchSleepStages(range: DateRange) {
		
		self.sleepStageService.getSleepDataByTimeStamp(timestampRange: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				var averageSleep = item.reduce(0) { (result: Double, nextItem: CoreDataSleepStage) -> Double in
					return result + nextItem.value.doubleValue
				}
				if !item.isEmpty {
					averageSleep = averageSleep / Double((item.count))
				}
				
				var chartElements = [ChartElement]()
				if range.type == .daily {
					chartElements = CoreDataSleepStage.fetchSleepStageChartDataForSingleDay(dateRange: range)
					let chartElementsForWeek = CoreDataSleepStage.fetchSleepStageChartData(dateRange: range)
					self?.delegate?.fetchSleepStageSummary(chartElements: chartElements, value: Int(averageSleep), chartElementsForWeek: chartElementsForWeek)
				} else {
					let dateList = Date.dates(from: range.start, to: range.end, interval: 1, component: .day)
					self?.fetchSleepDataForWeek(range: range, type: range.type)
				}
			}
		}
	}
	
	func updateSleepStagesWeekChart(range: DateRange) {
		let chartElements = CoreDataSleepTime.fetchSleepWeekAvgChartData(dateRange: range)
		self.delegate?.fetchSleepStageSummaryForWeek(chartElements: chartElements, value: 0)
	}
	
	func fetchSleepTimeData(forDate: Date) {
		self.sleepStageService.fetchAverageData(forDate:forDate.dateToString(format: DateFormat.serverDateFormat)) { sleepRepo, error in
			if let error = error {
				self.delegate?.afterSleepDataResponseFetched(result: .failure(error))
				return
			}
			
			if let sleepRepo = sleepRepo {
				self.delegate?.sleepDataTiming(sleepStartTime: sleepRepo.sleep_start_time, sleepEndTime: sleepRepo.sleep_end_time)
			}
		}
	}
	
	func fetchSleepTimeDataWeek(forDate: Date) {
		self.sleepStageService.fetchAverageData(forDate:forDate.dateToString(format: DateFormat.serverDateFormat)) { sleepRepo, error in
			if let error = error {
				self.delegate?.afterSleepDataResponseFetched(result: .failure(error))
				return
			}
			
			if let sleepRepo = sleepRepo {
//				self.delegate?.sleepDataTiming(sleepStartTime: sleepRepo.sleep_start_time, sleepEndTime: sleepRepo.sleep_end_time)
			}
		}
	}
	
	func fetchSleepDataForWeek(range: DateRange, type: DateRangeType) {
		self.sleepStageService.fetchData(by: range.start.timeIntervalSince1970 ..< range.end.timeIntervalSince1970) { sleepRepo, error in
			if let error = error {
				print(error)
				self.delegate?.fetchSleepStageSummaryForWeek(error: error)
			}
			
			if sleepRepo != nil {
				self.updateSleepStagesWeekChart(range: range)
			}
		}
	}
}
