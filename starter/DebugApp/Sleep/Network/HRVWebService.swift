//
//  HRVWebService.swift
//  BloodPressureApp
//
//  Created by Rushikant on 14/11/21.
//

import Foundation

class HRVWebService: HRVWebServiceProtocol {
	private var graphQL: ApolloClientService
	private var service: RestService
	
	init(graphQL: ApolloClientService = ApolloClientService(),service: RestService = RestService.shared) {
		self.graphQL = graphQL
		self.service = service
	}
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataHRV]?, MovanoError?) -> Void) {
        if(Utility.makeAPICall(timeIntervalRange:timestampRange) || !CoreDataHRV.isReadingAvailable(range: timestampRange)){
            let query = QueryHeartRateVariabilitiesByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
            
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
                switch result {
                case .success(let graphQLResult):
                    if let items = graphQLResult.data?.queryHeartRateVariabilitiesByTimestampRange?.items {
                        for each in items where each != nil {
                            CoreDataHRV.add(timeInterval: each!.hrvTimestamp, value: each!.hrvValue)
                        }
                        let hrvData = CoreDataHRV.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                        completionHandler(hrvData, nil)
                    } else {
                        completionHandler(nil, .invalidResponseModel)
                    }
                case .failure(let error):
                    if error.localizedDescription == "Received error response: Unauthorized" ||
                        error.localizedDescription == "Received error response: Token has expired." {
                        
                    } else {
                        
                    }
					let hrvData = CoreDataHRV.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
					completionHandler(hrvData, nil)
//					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                    Logger.shared.addLog(error.localizedDescription)
                }
            }
        }else{
            let hrvData = CoreDataHRV.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            completionHandler(hrvData, nil)
        }
	}
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
