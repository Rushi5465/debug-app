//
//  SleepStageDetailWebService.swift
//  BloodPressureApp
//
//  Created by Rushikant on 15/09/21.
//

import UIKit

// swiftlint:disable all
class SleepStageDetailWebService: SleepStagesWebServiceProtocol {
	
	private var graphQL: ApolloClientService
	private var service: RestService
	
	init(service: RestService = RestService.shared, graphQL: ApolloClientService = ApolloClientService()) {
		self.graphQL = graphQL
		self.service = service
	}
	
	func getSleepDataByTimeStamp(timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataSleepStage]?, MovanoError?) -> Void) {
        if(Utility.makeAPICall(timeIntervalRange: timestampRange) || !CoreDataSleepStage.isSleepStageAvailable(range: timestampRange)){
            let query = QuerySleepDataByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
            
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
                switch result {
                case .success(let graphQLResult):
                    if let items = graphQLResult.data?.querySleepDataByTimestampRange?.items {
							for each in items where each != nil {
								CoreDataSleepStage.addSleepStage(timeInterval: each!.sleepTimestamp, value: each!.sleepStage)
							}
                        let sleepStageData = CoreDataSleepStage.fetchSleepStage(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                        completionHandler(sleepStageData, nil)
                    } else {
                        completionHandler(nil, .invalidResponseModel)
                    }
                case .failure(let error):
                    if error.localizedDescription == "Received error response: Unauthorized" ||
                        error.localizedDescription == "Received error response: Token has expired." {
                        let sleepTempData = CoreDataSleepStage.fetchSleepStage(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                        completionHandler(sleepTempData, nil)
                    } else {
						let sleepTempData = CoreDataSleepStage.fetchSleepStage(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
						completionHandler(sleepTempData, nil)
//                        completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                    }
                    Logger.shared.addLog(error.localizedDescription)
                }
            }
        }else{
            let sleepTempData = CoreDataSleepStage.fetchSleepStage(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            completionHandler(sleepTempData, nil)
        }
	}
	
	func fetchSleepStageSummary(dateRange: Range<TimeInterval>, dataType: String, date: String, completionHandler: @escaping (SleepStageModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getUserSummary)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		var header = Header()
		header.addHeader(key: "Authorization", value: KeyChain.shared.idToken)
		header.addHeader(key: "AccessToken", value: KeyChain.shared.accessToken!)
		
		let parameters: [String: Any] = ["date": date,
										 "type": dataType,
										 "datapoint": "sleep data"]
		let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters)
		
		service.request(apiType: APIType.FETCH_SLEEP_STAGE_SUMMARY_API, request: request) { (result) in
			switch result {
			case .success(let response):
				if let responseModel = try? JSONDecoder().decode(SleepStageModel.self, from: response.rawData()) {
					completionHandler(responseModel, nil)
				} else {
					completionHandler(nil, MovanoError.invalidResponseModel)
				}
			case .failure(let error):
				completionHandler(nil, error)
			}
		}
	}
	
	func fetchAverageData(forDate:String,completionHandler: @escaping (SleepAverageDataModel?, MovanoError?) -> Void){
        let date = forDate.toDate(format: DateFormat.serverDateFormat) ?? Date()
        if(Utility.makeAPICall(date: date) || !CoreDataSleepTime.isSleepTimeAvailable(for: date)){
            let query = GetDailyAveragesQuery(daily_averages_date: forDate)
            
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { [weak self] (result) in
                switch result {
                case .success(let graphQLResult):
                    if let item = graphQLResult.data?.getDailyAverages, let avgData = graphQLResult.data?.getDailyAverages{
                        let avgPulseRate = convertStringToDictionary(text: avgData.averagePulseRate ?? "")
                        let dailyAvgPulseRateData = DailyAverageModel(average: (avgPulseRate?["average"] as? Double))
                        
                        let avgHRV = convertStringToDictionary(text: avgData.averageHrv ?? "")
                        let dailyAvgHRVData = DailyAverageModel(average: (avgHRV?["average"] as? Double))
                        
                        let avgOxygenRate = convertStringToDictionary(text: avgData.averageSpO2 ?? "")
                        let dailyAvgOxygenData = DailyAverageModel(average: (avgOxygenRate?["average"] as? Double))
                        
                        let avgBreathingRate = convertStringToDictionary(text: avgData.averageBreathingRate ?? "")
                        let dailyAvgBreathingRateData = DailyAverageModel(average: (avgBreathingRate?["average"] as? Double))
                        
                        let avgSystolicRate = convertStringToDictionary(text: avgData.averageSystolic ?? "")
                        let dailyAvgSystolicData = DailyAverageModel(average: (avgSystolicRate?["average"] as? Double))
                        
                        let avgDiastolicRate = convertStringToDictionary(text: avgData.averageDiastolic ?? "")
                        let dailyAvgDiastolicData = DailyAverageModel(average: (avgDiastolicRate?["average"] as? Double))
                        
                        let avgSkinTempRate = convertStringToDictionary(text: avgData.averageTemperature ?? "")
                        let dailyAvgTempData = DailyAverageModel(average: (avgSkinTempRate?["average"] as? Double))
						
						let totalStepData = convertStringToDictionary(text: avgData.totalStepCount ?? "")
						let dailyTotalStepData = DailyAverageModel(average: (totalStepData?["total"] as? Double))
						
						let totalCaloriesData = convertStringToDictionary(text: avgData.totalCaloriesBurnt ?? "")
						let dailyTotalCaloriesData = DailyAverageModel(average: (totalCaloriesData?["total"] as? Double))
                        
                        if let date = forDate.toDate(format: DateFormat.serverDateFormat), item.sleepStartTime != nil && item.sleepEndTime != nil {
                            CoreDataSleepTime.addSleepTime(date: date, sleepStartTime: item.sleepStartTime ?? "0", sleepEndTime: item.sleepEndTime ?? "0", deepValue: item.sleepData?[0] ?? 0, lightValue: item.sleepData?[1] ?? 0, remValue: item.sleepData?[2] ?? 0, awakeValue: item.sleepData?[3] ?? 0, avgHR: dailyAvgPulseRateData.average ?? 0.0, avgHRV: dailyAvgHRVData.average ?? 0.0, avgOxygen: dailyAvgOxygenData.average ?? 0.0, avgBreathingRate: dailyAvgBreathingRateData.average ?? 0.0, avgSystolicBP: dailyAvgSystolicData.average ?? 0.0, avgDiastolicBP: dailyAvgDiastolicData.average ?? 0.0, avgSkinTempVar: dailyAvgTempData.average ?? 0.0, totalStepCount: Int(dailyTotalStepData.total ?? 0), totalCaloriesBurnt: dailyTotalCaloriesData.total ?? 0.0, healthScore: item.healthScore ?? 0.0)
                        }
                        let dailyAvgData = SleepAverageDataModel(averageHrv: item.averageHrv, sleep_start_time: item.sleepStartTime, sleep_end_time: item.sleepEndTime)
                        completionHandler(dailyAvgData, nil)
                    } else {
                        completionHandler(nil, .invalidResponseModel)
                    }
                        
                    case .failure(let error):
                        if error.localizedDescription == "Received error response: Unauthorized" ||
                            error.localizedDescription == "Received error response: Token has expired." {
                            completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                            let coreDataSleeTime = CoreDataSleepTime.fetch(forDate: date)
                            let dailyAvgData = SleepAverageDataModel(averageHrv: "0", sleep_start_time: coreDataSleeTime?.sleepStartTime, sleep_end_time: coreDataSleeTime?.sleepEndTime)
                            completionHandler(dailyAvgData,nil)
                        } else {
							let coreDataSleeTime = CoreDataSleepTime.fetch(forDate: date)
							let dailyAvgData = SleepAverageDataModel(averageHrv: "0", sleep_start_time: coreDataSleeTime?.sleepStartTime, sleep_end_time: coreDataSleeTime?.sleepEndTime)
							completionHandler(dailyAvgData,nil)
//                            completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                        }
                        Logger.shared.addLog(error.localizedDescription)
                }
            }
        }else{
            let coreDataSleeTime = CoreDataSleepTime.fetch(forDate: date)
            let dailyAvgData = SleepAverageDataModel(averageHrv: "0", sleep_start_time: coreDataSleeTime?.sleepStartTime, sleep_end_time: coreDataSleeTime?.sleepEndTime)
            completionHandler(dailyAvgData,nil)
        }
	}
	
	func fetchData(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataSleepTime]?, MovanoError?) -> Void) {
		if(Utility.makeAPICall(timeIntervalRange: timestampRange) || !CoreDataExercise.isReadingAvailable(range: timestampRange)){
			
			let fromTs = Date(timeIntervalSince1970: timestampRange.lowerBound).dateToString(format: DateFormat.serverDateFormat)
			let toTs = Date(timeIntervalSince1970: timestampRange.upperBound).dateToString(format: DateFormat.serverDateFormat)
			let query = QueryDailyAveragesByTimestampRangeQuery(from_ts: fromTs, to_ts: toTs)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let item = graphQLResult.data?.queryDailyAveragesByTimestampRange?.items {
						for each in item where each != nil {
							if each!.sleepStartTime != nil {
								let avgPulseRate = convertStringToDictionary(text: each!.averagePulseRate ?? "")
								let dailyAvgPulseRateData = DailyAverageModel(average: (avgPulseRate?["average"] as? Double))
								
								let avgHRV = convertStringToDictionary(text: each!.averageHrv ?? "")
								let dailyAvgHRVData = DailyAverageModel(average: (avgHRV?["average"] as? Double))
								
								let avgOxygenRate = convertStringToDictionary(text: each!.averageSpO2 ?? "")
								let dailyAvgOxygenData = DailyAverageModel(average: (avgOxygenRate?["average"] as? Double))
								
								let avgBreathingRate = convertStringToDictionary(text: each!.averageBreathingRate ?? "")
								let dailyAvgBreathingRateData = DailyAverageModel(average: (avgBreathingRate?["average"] as? Double))
								
								let avgSystolicRate = convertStringToDictionary(text: each!.averageSystolic ?? "")
								let dailyAvgSystolicData = DailyAverageModel(average: (avgSystolicRate?["average"] as? Double))
								
								let avgDiastolicRate = convertStringToDictionary(text: each!.averageDiastolic ?? "")
								let dailyAvgDiastolicData = DailyAverageModel(average: (avgDiastolicRate?["average"] as? Double))
								
								let avgSkinTempRate = convertStringToDictionary(text: each!.averageTemperature ?? "")
								let dailyAvgTempData = DailyAverageModel(average: (avgSkinTempRate?["average"] as? Double))
								
								let totalStepData = convertStringToDictionary(text: each!.totalStepCount ?? "")
								let dailyTotalStepData = DailyAverageModel(average: (totalStepData?["total"] as? Double))
								
								let totalCaloriesData = convertStringToDictionary(text: each!.totalCaloriesBurnt ?? "")
								let dailyTotalCaloriesData = DailyAverageModel(average: (totalCaloriesData?["total"] as? Double))
								
                                CoreDataSleepTime.addSleepTime(date: each!.dailyAveragesDate.toDate(format: DateFormat.serverDateFormat) ?? Date(), sleepStartTime: each!.sleepStartTime!, sleepEndTime: each!.sleepEndTime!, deepValue: each!.sleepData![0]!, lightValue: each!.sleepData![1]!, remValue: each!.sleepData![2]!, awakeValue: each!.sleepData![3]!, avgHR: dailyAvgPulseRateData.average ?? 0.0, avgHRV: dailyAvgHRVData.average ?? 0.0, avgOxygen: dailyAvgOxygenData.average ?? 0.0, avgBreathingRate: dailyAvgBreathingRateData.average ?? 0.0, avgSystolicBP: dailyAvgSystolicData.average ?? 0.0, avgDiastolicBP: dailyAvgDiastolicData.average ?? 0.0, avgSkinTempVar: dailyAvgTempData.average ?? 0.0, totalStepCount: Int(dailyTotalStepData.total ?? 0), totalCaloriesBurnt: dailyTotalCaloriesData.total ?? 0.0, healthScore: each?.healthScore ?? 0.0)
								
							}
						}
						let averageData = CoreDataSleepTime.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
						completionHandler(averageData, nil)
					} else {
						let averageData = CoreDataSleepTime.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
						completionHandler(averageData, nil)
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						
					} else {
						
					}
					let averageData = CoreDataSleepTime.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
					completionHandler(averageData, nil)
//					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}else{
			let averageData = CoreDataSleepTime.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
			completionHandler(averageData, nil)
		}
	}
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
