//
//  SleepInsightPresenter.swift
//  BloodPressureApp
//
//  Created by Rushikant on 29/10/21.
//

import Foundation

// swiftlint:disable all
class SleepInsightPresenter: SleepInsightPresenterProtocol {
	
	private weak var delegate: InsightViewDelegate?
	private var breathingRateService: BreathingRateWebServiceProtocol
	private var pulseRateService: PulseRateWebServiceProtocol
	private var oxygenService: OxygenDataWebServiceProtocol
	private var sleepService: SleepStagesWebServiceProtocol
	private var hrvService: HRVWebServiceProtocol
	
	var insightList = [InsightModel]() {
		didSet {
			self.delegate?.fetchInsightList(list: insightList)
		}
	}
	
	var isCardiacData = Bool()
	
	private var cardiacInsightList = [InsightModel]() {
		didSet {
			if isCardiacData {
                hud.dismiss()
				self.insightList = cardiacInsightList
			}
		}
	}
	
	private var pulmonaryInsightList = [InsightModel]() {
		didSet {
			if !isCardiacData {
                hud.dismiss()
				self.insightList = pulmonaryInsightList
			}
		}
	}
	
	required init(breathingRateService: BreathingRateWebServiceProtocol, oxygenService: OxygenDataWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, sleepService: SleepStagesWebServiceProtocol, hrvService: HRVWebServiceProtocol, delegate: InsightViewDelegate) {
		self.breathingRateService = breathingRateService
		self.pulseRateService = pulseRateService
		self.oxygenService = oxygenService
		self.delegate = delegate
		self.sleepService = sleepService
		self.hrvService = hrvService
	}
	
	func fetchCardiacData(range: DateRange) {
		self.isCardiacData = true
		self.initialiseCardiacModel()
		fetchSleepStages(range: range)
		fetchHeartRateData(range: range)
		fetchHRVData(range: range)
	}
	
	func fetchPulmonaryData(range: DateRange) {
		self.isCardiacData = false
		self.initializePulmonaryModel()
		fetchSleepStages(range: range)
		fetchOxygenData(range: range)
		fetchBreathingRateData(range: range)
	}
	
	func initialiseCardiacModel() {
		let sleepModel = InsightModel(parameter: "Sleep Stages", avgValue: 0, unit: nil, chartData: InsightChartData(min: 0, max: 0, limitLines: [], elements: []),isSleepChart: true)
		let hrModel = InsightModel(parameter: "Heart Rate", avgValue: 0, unit: "bpm", chartData: InsightChartData(min: 50, max: 100, limitLines: [], elements: []),isSleepChart: false)
		let hrvModel = InsightModel(parameter: "Heart Rate Variation", avgValue: 0, unit: "ms", chartData: InsightChartData(min: 25, max: 55, limitLines: [], elements: []),isSleepChart: false)
		
		cardiacInsightList = [sleepModel, hrModel, hrvModel]
	}
	
	func initializePulmonaryModel() {
		let sleepModel = InsightModel(parameter: "Sleep Stages", avgValue: 0, unit: nil, chartData: InsightChartData(min: 0, max: 0, limitLines: [], elements: []), isSleepChart: true)
		let spO2Model = InsightModel(parameter: "Oxygen", avgValue: 0, unit: "%", chartData: InsightChartData(min: 90, max: 105, limitLines: [], elements: []),isSleepChart: false)
		let breathingModel = InsightModel(parameter: "Breathing Rate", avgValue: 0, unit: "brpm", chartData: InsightChartData(min: 10, max: 30, limitLines: [], elements: []),isSleepChart: false)
		
		pulmonaryInsightList = [sleepModel, spO2Model, breathingModel]
	}
	
	func fetchSleepStages(range: DateRange) {
		
		self.sleepService.getSleepDataByTimeStamp(timestampRange: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				var averageSleep = item.reduce(0) { (result: Double, nextItem: CoreDataSleepStage) -> Double in
					return result + nextItem.value.doubleValue
				}
				if !item.isEmpty {
					averageSleep = averageSleep / Double((item.count))
				}
				let chartElements = CoreDataSleepStage.fetchSleepStageChartDataForSingleDay(dateRange: range)
				let chartData = InsightChartData(min: NSNumber(value: 10), max: NSNumber(value: 15), limitLines: [], elements: chartElements)
				let model = InsightModel(parameter: "Sleep Stages", avgValue: 0, unit: nil, chartData: chartData, isSleepChart: true)
//				self?.delegate?.fetchSleepStageSummary(chartElements: chartElements, value: Int(averageSleep))
				if (self?.isCardiacData == true) {
					self?.cardiacInsightList[0] = model
				} else {
					self?.pulmonaryInsightList[0] = model
				}
			}
		}
	}
	
	func fetchBreathingRateData(range: DateRange) {
		self.breathingRateService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				var averageBreathingRate = item.reduce(0) { (result: Int, nextItem: CoreDataBreathingRate) -> Int in
					return result + nextItem.value.intValue
				}
				let breathingRateRange = UserDefault.shared.goals.breathingRate.value as! Range<Int>
				let limitLines = [breathingRateRange.lowerBound, breathingRateRange.upperBound]
				if !item.isEmpty {
					let elements = CoreDataBreathingRate.fetchBreathingRateChartData(dateRange: range)
					let chartData = InsightChartData(min: NSNumber(value: 10), max: NSNumber(value: 15), limitLines: limitLines, elements: elements)
					averageBreathingRate = averageBreathingRate / (item.count)
					let model = InsightModel(parameter: "Breathing Rate", avgValue: averageBreathingRate, unit: "brpm", chartData: chartData, isSleepChart: true)
					self?.pulmonaryInsightList[2] = model
				} else {
					let chartData = InsightChartData(min: NSNumber(value: 10), max: NSNumber(value: 15), limitLines: limitLines, elements: [])
					let model = InsightModel(parameter: "Breathing Rate", avgValue: 0, unit: "brpm", chartData: chartData, isSleepChart: true)
					self?.pulmonaryInsightList[2] = model
				}
			}
		}
	}
	
	func fetchOxygenData(range: DateRange) {
		self.oxygenService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				var averageOxygenRate = item.reduce(0) { (result: Int, nextItem: CoreDataOxygen) -> Int in
					return result + nextItem.value.intValue
				}
				let sPo2RateRange = UserDefault.shared.goals.oxygen.value as! Range<Int>
				let limitLines = [sPo2RateRange.lowerBound, sPo2RateRange.upperBound]
				if !item.isEmpty {
					let elements = CoreDataOxygen.fetchOxygenReadingChartData(dateRange: range)
					let chartData = InsightChartData(min: NSNumber(value: 90), max: NSNumber(value: 100), limitLines: limitLines, elements: elements)
					averageOxygenRate = averageOxygenRate / (item.count)
					let model = InsightModel(parameter: "SpO2", avgValue: averageOxygenRate, unit: "%", chartData: chartData, isSleepChart: true)
					self?.pulmonaryInsightList[1] = model
				} else {
					let chartData = InsightChartData(min: NSNumber(value: 90), max: NSNumber(value: 100), limitLines: limitLines, elements: [])
					let model = InsightModel(parameter: "SpO2", avgValue: 0, unit: "%", chartData: chartData, isSleepChart: true)
					self?.pulmonaryInsightList[1] = model
				}
			}
		}
	}
	
	func fetchHeartRateData(range: DateRange) {
		self.pulseRateService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				var averageHeartRate = item.reduce(0) { (result: Int, nextItem: CoreDataPulseRate) -> Int in
					return result + nextItem.value.intValue
				}
				let heartRateRange = UserDefault.shared.goals.pulseRate.value as! Range<Int>
				let limitLines = [heartRateRange.lowerBound, heartRateRange.upperBound]
				if !item.isEmpty {
					let elements = CoreDataPulseRate.fetchPulseRateChartData(dateRange: range)
					let chartData = InsightChartData(min: NSNumber(value: 90), max: NSNumber(value: 100), limitLines: limitLines, elements: elements)
					averageHeartRate = averageHeartRate / (item.count)
					let model = InsightModel(parameter: "Heart Rate", avgValue: averageHeartRate, unit: "bpm", chartData: chartData, isSleepChart: true)
					self?.cardiacInsightList[1] = model
				} else {
					let chartData = InsightChartData(min: NSNumber(value: 90), max: NSNumber(value: 100), limitLines: limitLines, elements: [])
					let model = InsightModel(parameter: "Heart Rate", avgValue: 0, unit: "bpm", chartData: chartData, isSleepChart: true)
					self?.cardiacInsightList[1] = model
				}
			}
		}
	}
	
	func fetchHRVData(range: DateRange) {
		self.hrvService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				var averageHRVRate = item.reduce(0) { (result: Int, nextItem: CoreDataHRV) -> Int in
					return result + nextItem.value.intValue
				}
				let hrvRange = UserDefault.shared.goals.breathingRate.value as! Range<Int>
				let limitLines = [25, 55]
				if !item.isEmpty {
					let elements = CoreDataHRV.fetchHRVChartData(dateRange: range)
					let chartData = InsightChartData(min: NSNumber(value: 25), max: NSNumber(value: 55), limitLines: limitLines, elements: elements)
					averageHRVRate = averageHRVRate / (item.count)
					let model = InsightModel(parameter: "Heart Rate Variation", avgValue: averageHRVRate, unit: "ms", chartData: chartData, isSleepChart: true)
					self?.cardiacInsightList[2] = model
				} else {
					let chartData = InsightChartData(min: NSNumber(value: 25), max: NSNumber(value: 55), limitLines: limitLines, elements: [])
					let model = InsightModel(parameter: "Heart Rate Variation", avgValue: 0, unit: "ms", chartData: chartData, isSleepChart: true)
					self?.cardiacInsightList[2] = model
				}
			}
		}
	}
	
}
