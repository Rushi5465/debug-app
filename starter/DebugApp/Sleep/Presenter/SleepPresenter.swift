//
//  SleepPresenter.swift
//  BloodPressureApp
//
//  Created by Rushikant on 26/08/21.
//

import UIKit

// swiftlint:disable all
class SleepPresenter: SleepPresenterProtocol {
	
	private weak var delegate: SleepViewDelegate?
	private var sleepService: SleepWebServiceProtocol
	private var pulseRateService: PulseRateWebServiceProtocol
	private var	oxygenService: OxygenDataWebServiceProtocol
	private var	temperatureService: SkinTemperatureWebServiceProtocol
	private var	breathingRateService: BreathingRateWebServiceProtocol
	private var	bloodPressureService: BloodPressureWebServiceProtocol
	private var hrvService: HRVWebServiceProtocol
	
	required init(sleepService: SleepWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, oxygenService: OxygenDataWebServiceProtocol, temperatureService: SkinTemperatureWebServiceProtocol, breathingRateService: BreathingRateWebServiceProtocol, bloodPressureService: BloodPressureWebServiceProtocol, hrvService:HRVWebServiceProtocol, delegate: SleepViewDelegate) {
		self.sleepService = sleepService
		self.pulseRateService = pulseRateService
		self.oxygenService = oxygenService
		self.temperatureService = temperatureService
		self.breathingRateService = breathingRateService
		self.bloodPressureService = bloodPressureService
		self.hrvService = hrvService
		self.delegate = delegate
	}
	
	func fetchPulseRate(range: DateRange) {
		self.pulseRateService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
                //Calculate Pulse Rate Score
                var metrciModel : MetricIndicatorModel?
                if let goalPulseRate = UserDefault.shared.goals.pulseRate.value as? Range<Int>{
                    if(item.count > 0){
                        let valuesInGoalRange = Double(item.filter{ Int(truncating: $0.value) >= goalPulseRate.lowerBound && Int(truncating: $0.value) <= goalPulseRate.upperBound}.count)

                        let pulseRateScore = ((valuesInGoalRange/Double(item.count)) * 100) / 10
                        if(pulseRateScore < 8){
                            let metrciImpactStr = "Your Heart Rate was outside your target range for \(String(format: "%.01f", (10-pulseRateScore)*10)) % of time."

							metrciModel = MetricIndicatorModel(indicatorImage: (pulseRateScore < 6 ? .alertRed : .alertYellow), metricType: .pulseRate,metricNegativeScore:metrciImpactStr)
                        }

                    }
                }
				var averagePulseRate = item.reduce(0) { (result: Double, nextItem: CoreDataPulseRate) -> Double in
					return result + nextItem.value.doubleValue
				}
				if !item.isEmpty {
					averagePulseRate = (Double(averagePulseRate) / Double(item.count)).rounded()
                    self?.delegate?.pulseRate(average: Int(averagePulseRate), unit: "bpm", metrciModel: metrciModel)
				} else {
					self?.delegate?.pulseRate(average: nil, unit: "",metrciModel: nil)
				}
				
			}
		}
	}
	
	func fetchOxygenData(range: DateRange) {
		self.oxygenService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
                var metrciModel : MetricIndicatorModel? = nil

                if let gloalSpo2 = UserDefault.shared.goals.oxygen.value as? Range<Int>{
                    if(item.count > 0){
                        let valuesInSpo2Range = item.filter{ Int(truncating: $0.value) >= gloalSpo2.lowerBound && Int(truncating: $0.value) <= gloalSpo2.upperBound}.count

                        let spo2Score = ((Double(valuesInSpo2Range)/Double(item.count)) * 100) / 10
                        if(spo2Score < 8){
                            let metrciImpactStr = "Your Oxygen rate was outside your target range for \(String(format: "%.01f", (10-spo2Score)*10)) % of time."

                            metrciModel = MetricIndicatorModel(indicatorImage: (spo2Score < 6 ? .alertRed : .alertYellow), metricType: .oxygen, metricNegativeScore: metrciImpactStr)
                        }

                    }
                }
				var averageOxygenSaturation = item.reduce(0) { (result: Double, nextItem: CoreDataOxygen) -> Double in
					return result + nextItem.value.doubleValue
				}
				if !item.isEmpty {
					averageOxygenSaturation = (Double(averageOxygenSaturation) / Double(item.count)).rounded()
                    self?.delegate?.oxygenSaturation(average: Int(averageOxygenSaturation), unit: "%", metrciModel: metrciModel)
				} else {
                    self?.delegate?.oxygenSaturation(average: nil, unit: "", metrciModel: nil)
				}
				
			}
		}
	}
	
	func fetchSkinTemperature(range: DateRange) {
		self.temperatureService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
                //Calculate temperature score
                var metrciModel : MetricIndicatorModel?

                if let goalTemp = UserDefault.shared.goals.temperature.value as? Range<Int>{
                    if(item.count > 0){
                        let valuesInGoalRange = item.filter{
                            Double(truncating: $0.value) >= Double(goalTemp.lowerBound) && Double(truncating: $0.value) <= Double(goalTemp.upperBound)
                        }.count

                        let temperatureScore = ((Double(valuesInGoalRange)/Double(item.count)) * 100) / 10


                        if(temperatureScore < 8){
                            let metrciImpactStr = "Your Skin Temperature was outside your target range for \(String(format: "%.01f", (10-temperatureScore)*10)) % of time."
                            metrciModel = MetricIndicatorModel(indicatorImage: (temperatureScore < 6 ? .alertRed : .alertYellow), metricType: .temperature,metricNegativeScore: metrciImpactStr)
                        }

                    }

                }
				var averageSkinTemp = item.reduce(0) { (result: Double, nextItem: CoreDataSkinTemperature) -> Double in
					return abs(result) + abs(Utility.getTempVariation(temperature: nextItem.value.doubleValue))
				}
				if !item.isEmpty {
					averageSkinTemp = Double(averageSkinTemp) / Double((item.count))
					let avgVariation = averageSkinTemp
					let tempAvg = (((avgVariation * 10).rounded()) / 10)
					
					let totalBaselLineSum = (Double(item.count) * (KeyChain.shared.baselineBodyTemprature?.toDouble ?? 0.0))
					let totalTempSum = item.reduce(0) { (result: Double, nextItem: CoreDataSkinTemperature) -> Double in
						return result + nextItem.value.doubleValue
					}
					
                    self?.delegate?.skinTemperature(average: tempAvg.roundToDecimal(2), unit: "°f", isPositive: (totalTempSum > totalBaselLineSum), metrciModel: metrciModel)
				} else {
                    self?.delegate?.skinTemperature(average: nil, unit: "", isPositive: true, metrciModel: nil)
				}
				
			}
		}
	}
	
	func fetchBloodPressure(range: DateRange) {
		self.bloodPressureService.listBloodPressures(range: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
                //Calculate Systolic and Diastolic blood pressure score
                var metrciModel : MetricIndicatorModel?

                if let systolicGoalBPVlaue = UserDefault.shared.goals.systolic.value as? Range<Int>,let diastolicGoalValue = UserDefault.shared.goals.diastolic.value as? Range<Int>{
                    if(item.count > 0){
                        let systolicValuesInGoalRange = item.filter{ Int(truncating: $0.systolic) >= systolicGoalBPVlaue.lowerBound && Int(truncating: $0.systolic) <= systolicGoalBPVlaue.upperBound}.count

                        let diastolicValuesInGoalRange = item.filter{ Int(truncating: $0.diastolic) >= diastolicGoalValue.lowerBound && Int(truncating: $0.diastolic) <= diastolicGoalValue.upperBound}.count

                        let systolicBPScore = ((Double(systolicValuesInGoalRange)/Double(item.count)) * 100) / 10
                        let diastolicBPScore = ((Double(diastolicValuesInGoalRange)/Double(item.count)) * 100) / 10

                        var metrciImpactStr = ""

                        if(diastolicBPScore < 8 && systolicBPScore < 8){
                            metrciImpactStr = "Your Diastolic BP was outside your target range for \(String(format: "%.01f", (10-diastolicBPScore)*10)) % of time and your Systolic BP was outside your target range for \(String(format: "%.01f", (10-systolicBPScore)*10)) % of time."
                        }else if(diastolicBPScore < 8){
                            metrciImpactStr = "Your Diastolic BP was outside your target range for \(String(format: "%.01f", (10-diastolicBPScore)*10)) % of time."
                        }else if(systolicBPScore < 8){
                            metrciImpactStr = "Your Systolic BP was outside your target range for \(String(format: "%.01f", (10-systolicBPScore)*10)) % of time."
                        }
                        
                        if(diastolicBPScore < 6 || systolicBPScore < 6){
							metrciModel = MetricIndicatorModel(indicatorImage: .alertRed, metricType: .bloodPressure, metricNegativeScore: metrciImpactStr)
						} else if(diastolicBPScore < 8 || systolicBPScore < 8){
							metrciModel = MetricIndicatorModel(indicatorImage: .alertYellow, metricType: .bloodPressure, metricNegativeScore: metrciImpactStr)
						}
            
                    }
                }
                
				var averageSystolic = item.reduce(0) { (result: Double, nextItem: CoreDataBPReading) -> Double in
					return result + nextItem.systolic.doubleValue
				}
				var averageDiastolic = item.reduce(0) { (result: Double, nextItem: CoreDataBPReading) -> Double in
					return result + nextItem.diastolic.doubleValue
				}
				if !item.isEmpty {
					averageSystolic = (Double(averageSystolic) / Double(item.count)).rounded()
					averageDiastolic = (Double(averageDiastolic) / Double(item.count)).rounded()
				}
				let chartElements = CoreDataBPReading.fetchBloodPressureChartData(dateRange: range)
                self?.delegate?.bpChart(chartElements: chartElements.0, systolic: Int(averageSystolic), diastolic: Int(averageDiastolic), metrciModel: metrciModel)
			}
		}
	}
	
	func fetchBreathingRate(range: DateRange) {
		self.breathingRateService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
                //Calculate breathing rate score
                var metrciModel : MetricIndicatorModel?

                if let goalBreathingRate = UserDefault.shared.goals.breathingRate.value as? Range<Int>{
                    if(item.count > 0){
                        let valuesInGoalRange = item.filter{ Int(truncating: $0.value) >= goalBreathingRate.lowerBound && Int(truncating: $0.value) <= goalBreathingRate.upperBound}.count

                       let breathingRateScore = ((Double(valuesInGoalRange)/Double(item.count)) * 100) / 10

                        if(breathingRateScore < 8){
                            let metrciImpactStr = "Your Breathing Rate was outside your target range for \(String(format: "%.01f", (10-breathingRateScore)*10)) % of time."

                            metrciModel = MetricIndicatorModel(indicatorImage: (breathingRateScore < 6 ? .alertRed : .alertYellow), metricType: .breathingRate,metricNegativeScore: metrciImpactStr)
                        }

                    }

                }
                
				var averageBreathingRate = item.reduce(0) { (result: Double, nextItem: CoreDataBreathingRate) -> Double in
					return result + nextItem.value.doubleValue
				}
				if !item.isEmpty {
					averageBreathingRate = (Double(averageBreathingRate) / Double(item.count)).rounded()
                    self?.delegate?.breathingRate(average: Int(averageBreathingRate), unit: "brpm", metrciModel: metrciModel)
				} else {
                    self?.delegate?.breathingRate(average: nil, unit: "", metrciModel: nil)
				}
			}
		}
	}
	
	func fetchHRVRate(range: DateRange) {
		self.hrvService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				var averageHRV = item.reduce(0) { (result: Double, nextItem: CoreDataHRV) -> Double in
					return result + nextItem.value.doubleValue
				}
				if !item.isEmpty {
					averageHRV = (Double(averageHRV) / Double(item.count)).rounded()
                    self?.delegate?.hrvRate(average: Int(averageHRV), unit: "brpm", metrciModel: nil)
				} else {
                    self?.delegate?.hrvRate(average: nil, unit: "", metrciModel: nil)
				}
			}
		}
	}
	
	func fetchSleepStages(range: DateRange) {
		self.sleepService.fetch(by: range) { [weak self] (item, error) in
            
            if let _error = error{
                self?.delegate?.sleepStageDataFetched(result: .failure(_error))
            }
			if let _item = item {
                self?.delegate?.sleepStageDataFetched(result: .success(_item))
            }
		}
	}
    
//    func fetchSleepStages(range: DateRange) {
//        self.sleepService.fetch(by: range) { [weak self] (item, error) in
//            if let item = item {
//                var averageSleep = item.reduce(0) { (result: Double, nextItem: CoreDataSleepStage) -> Double in
//                    return result + nextItem.value.doubleValue
//                }
//                if !item.isEmpty {
//                    averageSleep = averageSleep / Double((item.count))
//                }
//
//                let chartElements = CoreDataSleepStage.fetchSleepStageChartData(dateRange: range)
//                self?.delegate?.sleepStageChart(chartElements: chartElements, value: Int(averageSleep))
//
//            }
//        }
//    }
    
	func fetchSleepTimeData(forDate: Date) {
		self.sleepService.fetchAverageData(forDate:forDate.dateToString(format: DateFormat.serverDateFormat)) { sleepRepo, error in
			if let error = error {
				self.delegate?.afterSleepDataResponseFetched(result: .failure(error))
				return
			}
			
			if let sleepRepo = sleepRepo {
				if sleepRepo.sleep_start_time != "0" && sleepRepo.sleep_end_time != "0" {
					self.delegate?.sleepDataTiming(sleepStartTime: sleepRepo.sleep_start_time, sleepEndTime: sleepRepo.sleep_end_time)
				} else {
					let tempSleepStartTime = "0"
					let tempSleepEndTime = "0"
					self.delegate?.sleepDataTiming(sleepStartTime: tempSleepStartTime, sleepEndTime: tempSleepEndTime)
				}
			}
		}
	}
	
	func fetchParameters() {
		let bp = SleepParameter(name: "Blood Pressure", reading: nil, unit: "mmHg", icon: "sleep-BPIcon",metrciModel: nil)
		let heartRate = SleepParameter(name: "Heart Rate", reading: nil, unit: "bpm", icon: "sleep-HeartIcon",metrciModel: nil)
		let skinTemp = SleepParameter(name: "Skin Temp Var.", reading: nil, unit: "°f", icon: "sleep_SkintempIcon",metrciModel: nil)
		let breathingRate = SleepParameter(name: "Breathing Rate", reading: nil, unit: "brpm", icon: "sleep-BreathingIcon",metrciModel: nil)
		let oxygenRate = SleepParameter(name: "Oxygen", reading: nil, unit: "%", icon: "sleep-OxygenIcon",metrciModel: nil)
		let hrvRate = SleepParameter(name: "Heart Rate Var.", reading: nil, unit: "ms", icon: "sleep-HRVIcon",metrciModel: nil)
		self.delegate?.parameter(list: [heartRate, hrvRate, oxygenRate, breathingRate, bp, skinTemp])
	}
	
	func fetchData(range: DateRange) {
		self.fetchParameters()
		self.fetchPulseRate(range: range)
		self.fetchOxygenData(range: range)
		self.fetchSkinTemperature(range: range)
		self.fetchBreathingRate(range: range)
		self.fetchHRVRate(range: range)
		self.fetchBloodPressure(range: range)
		self.fetchSleepStages(range: range)
	}
}
