//
//  SleepParameter.swift
//  BloodPressureApp
//
//  Created by Rushikant on 21/10/21.
//

import Foundation

struct SleepParameter {
	
	let name: String
	var reading: String?
	let unit: String?
	let icon: String?
    var metrciModel:MetricIndicatorModel?
	
	init(name:String) {
		self.name = name
		self.reading = "-"
		self.unit = "-"
		self.icon = ""
        self.metrciModel = nil
	}
	
    init(name:String,reading:String?,unit:String?,icon:String?,metrciModel:MetricIndicatorModel?) {
		self.name = name
		self.reading = reading
		self.unit = unit
		self.icon = icon
        self.metrciModel = metrciModel
	}
}
