//
//  DetailDataType.swift
//  BloodPressureApp
//
//  Created by Rushikant on 01/09/21.
//

import Foundation

enum DetailDataType {
	case heartRateReading
	case spO2Reading
	case tempratureReading
	case breathingRateReading
	case bpReading
	case hrvReading
}
