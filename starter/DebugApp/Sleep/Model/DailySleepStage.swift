//
//  DailySleepStage.swift
//  BloodPressureApp
//
//  Created by Rushikant on 21/09/21.
//

import Foundation

typealias DailySleepStage = [DailySleepStageElement]

// MARK: - DailySleepStageElement
struct DailySleepStageElement: Codable {
	let date: String
	let sleepData: [Int]

	enum CodingKeys: String, CodingKey {
		case date
		case sleepData = "values"
	}
}

// MARK: - SleepData
struct SleepData: Codable {
	let sleepStage: SleepStage
	let totalTime: String
	
	enum CodingKeys: String, CodingKey {
		case sleepStage = "sleep_stage"
		case totalTime = "total_time"
	}
}

// MARK: - SleepStage
enum SleepStage: String, Codable {
	case awake
	case deep
	case sleep3
	case sleep4
}

// MARK: - Welcome
struct SleepStageModel: Codable {
	let weekly: [Int]?
	let data: [SleepDataModel]
}

// MARK: - Datum
struct SleepDataModel: Codable {
	let date: String
	let values: [Int]
}
