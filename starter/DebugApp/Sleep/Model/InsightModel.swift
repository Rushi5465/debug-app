//
//  InsightModel.swift
//  BloodPressureApp
//
//  Created by Rushikant on 29/10/21.
//

import Foundation
import UIKit

struct InsightModel {
	let parameter: String
	let avgValue: Int
	let unit: String?
	let chartData: InsightChartData
	var isSleepChart: Bool = false
}

struct InsightChartData {
	var min: NSNumber?
	var max: NSNumber?
	var limitLines: [Int]
	var elements: [ChartElement]
}

typealias Insight = [InsightModel]
