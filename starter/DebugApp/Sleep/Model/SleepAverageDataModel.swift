//
//  SleepAverageDataModel.swift
//  BloodPressureApp
//
//  Created by Rushikant on 18/10/21.
//

import Foundation

struct SleepAverageDataModel {
	let averageHrv: String?
	let sleep_start_time: String?
	let sleep_end_time: String?
	
}

struct SleepAverageModel: Codable {
	let average:Double?
	let total:Double?
	let count:Double?
	
	init() {
		average = 0.0
		total = 0.0
		count = 0.0
	}
	enum CodingKeys: String, CodingKey {
		case average = "average"
		case total = "total"
		case count = "count"
	}
}

enum MetricParamList:Int{
    case heartRate = 0
    case hrvRate = 1
    case oxygenRate = 2
    case breathingRate = 3
    case bp = 4
    case skinTemp = 5
    
}
