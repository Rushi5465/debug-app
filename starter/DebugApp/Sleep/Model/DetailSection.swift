//
//  DetailSection.swift
//  BloodPressureApp
//
//  Created by Rushikant on 01/09/21.
//

import Foundation

struct DetailSection {
	var date: Date
	var data: [Any]
}
