//
//  SleepStagesDetailsViewController.swift
//  BloodPressureApp
//
//  Created by Rushikant on 30/08/21.
//

import UIKit
import HGCircularSlider
import Highcharts
import Alamofire

// swiftlint:disable all
class SleepStagesDetailsViewController: UIViewController {
	
	@IBOutlet weak var headerView: HeaderView!
	
	@IBOutlet weak var circularSliderAwake: CircularSlider!
	@IBOutlet weak var circularSliderLight: CircularSlider!
	@IBOutlet weak var circularSliderRem: CircularSlider!
	@IBOutlet weak var circularSliderDeep: CircularSlider!
	@IBOutlet weak var completedStepsLabel: MovanoLabel!
	@IBOutlet weak var barSegmentedControl: UISegmentedControl!
	
	@IBOutlet weak var remLabel: MovanoLabel!
	@IBOutlet weak var remValueLabel: MovanoLabel!
	@IBOutlet weak var lightLabel: MovanoLabel!
	@IBOutlet weak var lightValueLabel: MovanoLabel!
	@IBOutlet weak var deepLabel: MovanoLabel!
	@IBOutlet weak var deepValueLabel: MovanoLabel!
    
    @IBOutlet weak var alertIndicatorView: UIView!
    @IBOutlet weak var leftIndicationLabel: MovanoLabel!
    @IBOutlet weak var leftIndicationImageView: UIImageView!
    // Charts View
	@IBOutlet weak var sleepStagesUIView: UIView!
	
	// Week UIView
	@IBOutlet weak var weekUIView: UIView!
	@IBOutlet weak var weekChartView: HIChartView!
	@IBOutlet weak var weekNameLbel: MovanoLabel!
	
	// Day UIView
	@IBOutlet weak var dayUIView: UIView!
	@IBOutlet weak var dayBarChartView: HIChartView!
	@IBOutlet weak var heightConstraintForDayBarChart: NSLayoutConstraint!
	@IBOutlet weak var dayLineChartView: HIChartView!
	@IBOutlet weak var heightConstraintForDayLineChart: NSLayoutConstraint!
	
	// New UI Design
	@IBOutlet weak var timeInBedLabel: MovanoLabel!
	@IBOutlet weak var timeInBedValueLabel: MovanoLabel!
	@IBOutlet weak var avgRestingHRLabel: MovanoLabel!
	@IBOutlet weak var avgRestingHRValueLabel: MovanoLabel!
	
	// Circular Indicators
	@IBOutlet weak var remCircle: MovanoLabel!
	@IBOutlet weak var deepCircle: MovanoLabel!
	@IBOutlet weak var lightCircle: MovanoLabel!
	
	@IBOutlet weak var segmentedControlUIView: UIView!
	
	// Circular Indicators on graphs
	@IBOutlet weak var awakeCircleForGraph: MovanoLabel!
	@IBOutlet weak var remCircleForGraph: MovanoLabel!
	@IBOutlet weak var deepCircleForGraph: MovanoLabel!
	@IBOutlet weak var lightCircleForGraph: MovanoLabel!
	
	// ChartView for PieChart
	@IBOutlet weak var sleepStaegsPieChart: HIChartView!
	
	@IBOutlet weak var currentDateLabel: MovanoLabel!
	
	// IBOutlets for NoChartdata
	@IBOutlet weak var noChatDataWeekUIView: UIView!
	@IBOutlet weak var noChartDataDayUIView: UIView!
	
	// IBOutlet for time view
	@IBOutlet weak var timeView: UIView!
	
	var elementColor: UIColor!
	
	var presenter: SleepStagesDetailPresenter?
	var dataRangeType: DateRangeType = .daily
    var metricIndicatorModel:MetricIndicatorModel?
	
	var selectedDateRange: DateRange! {
		didSet {
            hud.show(in: self.view)
			//let range = selectedDateRange.intervalRange
			self.barSegmentedControl.selectedSegmentIndex = 0
//			self.switchView(barSegmentedControl)
			
			weekNameLbel.isHidden = (barSegmentedControl.selectedSegmentIndex == 0)
			
			if !weekNameLbel.isHidden {
				let range = DateRange(selectedDateRange.start, type: .weekly)
				let startDate = range.start.dateToString(format: "MMM dd").uppercased()
				let endDate = range.end.dateToString(format: "MMM dd").uppercased()
//				let selectedYear = range.end.dateToString(format: "yyyy")
				weekNameLbel.text = "\(startDate) - \(endDate)"
			}
			
			if dataRangeType == .daily {
				//let dateBefore = headerView.selectedDate.end.dateBefore(days: 1)
				//currentSleepStartDate = dateBefore.setTime(hour: 22, min: 0, sec: 0)
				//currentSleepEndDate = headerView.selectedDate.end.setTime(hour: 6, min: 0, sec: 0)
                
				//let dateRange = DateRange(start: currentSleepStartDate!, end: currentSleepEndDate!, type: .daily)
				self.presenter?.fetchSleepTimeData(forDate: selectedDateRange.end)
			} else {
				self.presenter?.fetchSleepStages(range: DateRange(headerView.selectedDate.start, type: .weekly))
			}
//			let dataType = (dataRangeType == .daily) ? "daily" : "weekly"
//			let date = selectedDateRange.start.dateToString(format: DateFormat.serverDateFormat)
//			self.presenter?.fetchSleepSummary(timeStampRange: range, dataType: dataType, date: date)
			
		}
	}
	
	var dataModelForOxygenChart = [ActivityChartModel]() {
		didSet {
			if dataModelForOxygenChart.count != 0 {
//				loadOxygenChartForDay(chartData: dataModelForOxygenChart[0].chartData, limitLineValues: [90.0])
//				loadDayChartForSleep(chartData: dataModelForOxygenChart[0].chartData)
			}
		}
	}
	
	var dummyDataForStages: SleepStageModel?
	var currentSleepStartDate: Date?
	var currentSleepEndDate: Date?
	var avgHeartRate: String?
	
	override func viewDidLoad() {
		super.viewDidLoad()
				
		self.headerView.currentController = self
		self.headerView.isInNavigation = true
		self.headerView.updateDate = false
		
		barSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
		barSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .normal)
		
		setupCircularSlider()
		self.registerPresenters()
		
		self.selectedDateRange = DateRange(DateManager.current.selectedDate.start, type: .daily)
		
		heightConstraintForDayBarChart.constant = 250
		heightConstraintForDayLineChart.constant = 0
		
		// make circular rings
		
		remCircle.clipsToBounds = true
		remCircle.layer.cornerRadius = 1.0
		remCircle.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		
		deepCircle.clipsToBounds = true
		deepCircle.layer.cornerRadius = 1.0
		deepCircle.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		
		lightCircle.clipsToBounds = true
		lightCircle.layer.cornerRadius = 1.0
		lightCircle.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		
		segmentedControlUIView.clipsToBounds = true
		segmentedControlUIView.layer.cornerRadius = 2.0
		segmentedControlUIView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		
		awakeCircleForGraph.clipsToBounds = true
		awakeCircleForGraph.layer.cornerRadius = 1.0
		awakeCircleForGraph.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		
		remCircleForGraph.clipsToBounds = true
		remCircleForGraph.layer.cornerRadius = 1.0
		remCircleForGraph.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		
		deepCircleForGraph.clipsToBounds = true
		deepCircleForGraph.layer.cornerRadius = 1.0
		deepCircleForGraph.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		
		lightCircleForGraph.clipsToBounds = true
		lightCircleForGraph.layer.cornerRadius = 1.0
		lightCircleForGraph.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		
		timeView.clipsToBounds = true
		timeView.borderColor = UIColor.hexColor(hex: "#6262BA")
		timeView.borderWidth = 0.5
		
		// Custom header view
		headerView.leftArrow.isHidden = true
		headerView.rightArrow.isHidden = true
		headerView.calendarView.isHidden = true
		
		currentDateLabel.text = headerView.selectedDate.end.dateToString(format: "d MMM, yyyy")
		dayBarChartView.isUserInteractionEnabled = false
        setupScoreAffectingMetrics()
//		weekChartView.isUserInteractionEnabled = false
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.calendarDateSelected), name: NSNotification.Name("calendarDateSelected"), object: nil)
	}
		
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name("calendarDateSelected"), object: nil)
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let oxygenService = OxygenDataWebService()
			self.presenter = SleepStagesDetailPresenter(sleepStageService: SleepStageDetailWebService(), oxygenService: oxygenService, delegate: self)
		}
	}
	
    func setupScoreAffectingMetrics(){
        if let metricModel =  self.metricIndicatorModel{
            alertIndicatorView.isHidden = false
			leftIndicationImageView.image = UIImage(named: metricModel.indicatorImage.rawValue)
            leftIndicationLabel.text = metricModel.metricNegativeScore
        }else{
            alertIndicatorView.isHidden = true
        }
    }
	func setupCircularSlider() {
		circularSliderAwake.isUserInteractionEnabled = false
		circularSliderLight.isUserInteractionEnabled = false
		circularSliderRem.isUserInteractionEnabled = false
		circularSliderDeep.isUserInteractionEnabled = false
		
		circularSliderAwake.maximumValue = 360.0
		circularSliderAwake.endPointValue = 192.0
		
		circularSliderLight.maximumValue = 360.0
		circularSliderLight.endPointValue = 172.0
		
		circularSliderRem.maximumValue = 360.0
		circularSliderRem.endPointValue = 66.0
		
		circularSliderDeep.maximumValue = 360.0
		circularSliderDeep.endPointValue = 50.0
	}
	
	func setupSleepUI(deep: Double?, awake: Double?, light: Double?, rem: Double?) {
		// avg heart rate
		let attrsForTimeInBed = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.semiBold(withSize: 18),
								 convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.hexColor(hex: "#F9F9F9")]
		let attrsForTimeInBedUnit = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.regular(withSize: 12),
									 convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.hexColor(hex: "#979797")]
		
		/* Avg Resting HR Text*/
		let avgRestingHRText = NSMutableAttributedString(string: "\(avgHeartRate ?? "0") ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForTimeInBed))
		avgRestingHRText.append(NSAttributedString(string: "bpm", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForTimeInBedUnit)))
		avgRestingHRValueLabel.attributedText = avgRestingHRText
		
        let tempAwake = awake ?? 0.0
        let tempDeep  = deep ?? 0.0
        let tempRem =  rem ?? 0.0
        let tempLight = light ?? 0.0
        let tempTime = tempAwake + tempRem + tempDeep + tempLight
		let totalTime = Int(tempTime)
		pieChartForStages(rem: (rem ?? 0), deep: (deep ?? 0), light: (light ?? 0))
		
//		if Int(awake ?? 0) > 60 {
//			awakeValueLabel.text = "\(Int(awake!) / 60)h \(Int(awake!) % 60)m"
//		} else {
//			awakeValueLabel.text = "\(Int(awake ?? 0)) m"
//		}
		
//		if totalTime != 0 {
//			awakeValueLabel.text = "\(awakeValueLabel.text!) | \((Int(awake ?? 0) * 100)/totalTime) %"
//		}
		
		let attrsForHour = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.semiBold(withSize: 16),
							convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.hexColor(hex: "#F9F9F9")]
		let attrsForUnit = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.regular(withSize: 12),
							convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.hexColor(hex: "#979797")]
		
		if Int(rem ?? 0) > 60 {
			
			let customeTotalSleepText = NSMutableAttributedString(string: "\(Int(rem!) / 60) ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForHour))
			customeTotalSleepText.append(NSAttributedString(string: "h   ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForUnit)))
			customeTotalSleepText.append(NSAttributedString(string: "\(Int(rem!) % 60) ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForHour)))
			customeTotalSleepText.append(NSMutableAttributedString(string: "m", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForUnit)))
			
			lightValueLabel.attributedText = customeTotalSleepText
		} else {
			let customeTotalSleepText = NSMutableAttributedString(string: "\(Int(rem!) % 60) ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForHour))
			customeTotalSleepText.append(NSAttributedString(string: "m", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForUnit)))
			
			lightValueLabel.attributedText = customeTotalSleepText
		}
		
		/*
		if totalTime != 0 {
			lightValueLabel.text = "\(lightValueLabel.text!) | \((Int(rem ?? 0) * 100)/totalTime) %"
		}
		 */
		
		if Int(light ?? 0) > 60 {
			let customeTotalSleepText = NSMutableAttributedString(string: "\(Int(light!) / 60) ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForHour))
			customeTotalSleepText.append(NSAttributedString(string: "h   ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForUnit)))
			customeTotalSleepText.append(NSAttributedString(string: "\(Int(light!) % 60) ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForHour)))
			customeTotalSleepText.append(NSMutableAttributedString(string: "m", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForUnit)))
			
			remValueLabel.attributedText = customeTotalSleepText
		} else {
			let customeTotalSleepText = NSMutableAttributedString(string: "\(Int(light!) % 60) ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForHour))
			customeTotalSleepText.append(NSMutableAttributedString(string: "m", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForUnit)))
			
			remValueLabel.attributedText = customeTotalSleepText
		}
		
		/*
		if totalTime != 0 {
			remValueLabel.text = "\(remValueLabel.text!) | \(Int((light ?? 0) * 100)/totalTime) %"
		}
		 */
		
		if Int(deep ?? 0) > 60 {
			let customeTotalSleepText = NSMutableAttributedString(string: "\(Int(deep!) / 60) ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForHour))
			customeTotalSleepText.append(NSAttributedString(string: "h   ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForUnit)))
			customeTotalSleepText.append(NSAttributedString(string: "\(Int(deep!) % 60) ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForHour)))
			customeTotalSleepText.append(NSMutableAttributedString(string: "m", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForUnit)))
			
			deepValueLabel.attributedText = customeTotalSleepText
			
		} else {
			let customeTotalSleepText = NSMutableAttributedString(string: "\(Int(deep!) % 60)" , attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForHour))
			customeTotalSleepText.append(NSMutableAttributedString(string: "m", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForUnit)))
			
			deepValueLabel.attributedText = customeTotalSleepText
		}
		
		/*
		if totalTime != 0 {
			deepValueLabel.text = "\(deepValueLabel.text!) | \(Int((deep ?? 0) * 100)/totalTime) %"
		}
		 */
		
		// Time in Bed
		let hours = totalTime / 60
		let minutes = totalTime % 60
		
		// Total Sleep
        let tempSleep = (rem ?? 0.0) + (light ?? 0.0) + (deep ?? 0.0)
		let totalSleep = Int(tempSleep)
		let hoursForSleep = totalSleep / 60
		let minutesForSleep = totalSleep % 60
		
		// Attributed text for total sleep
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.semiBold(withSize: 24),
					  convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.hexColor(hex: "#F9F9F9")]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.regular(withSize: 14),
					  convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor(red: 255, green: 255, blue: 255, alpha: 0.7)]
		
		let customeTotalSleepText = NSMutableAttributedString(string: "\(hoursForSleep)", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1))
		customeTotalSleepText.append(NSAttributedString(string: "h  ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		customeTotalSleepText.append(NSAttributedString(string: "\(minutesForSleep)", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
		customeTotalSleepText.append(NSMutableAttributedString(string: "m", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		
		completedStepsLabel.attributedText = customeTotalSleepText
		
//		completedStepsLabel.text = "\(hoursForSleep) h \(minutesForSleep) m"
		
		// Time in Bed
		let customeTimeInBedText = NSMutableAttributedString(string: "\(hours) ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForTimeInBed))
		customeTimeInBedText.append(NSAttributedString(string: "h    ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForTimeInBedUnit)))
		customeTimeInBedText.append(NSAttributedString(string: "\(minutes) ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForTimeInBed)))
		customeTimeInBedText.append(NSMutableAttributedString(string: "m", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrsForTimeInBedUnit)))
		
		timeInBedValueLabel.attributedText = customeTimeInBedText
	}
	
	@IBAction func switchView(_ sender: UISegmentedControl) {
        hud.show(in: self.view)
		var range: DateRange!
//		var dataType = ""
		if sender.selectedSegmentIndex == 0 {
			range = headerView.selectedDate
			dataRangeType = .daily
//			dataType = "daily"
			dayUIView.isHidden = false
			weekUIView.isHidden = true
			weekNameLbel.isHidden = true
		} else {
			range = DateRange(headerView.selectedDate.start, type: .weekly)
			dataRangeType = .weekly
//			dataType = "weekly"
			dayUIView.isHidden = true
			weekUIView.isHidden = false
			weekNameLbel.isHidden = false
			
			let startDate = range.start.dateToString(format: "MMM dd").uppercased()
			let endDate = range.end.dateToString(format: "MMM dd").uppercased()
//			let selectedYear = selectedDateRange.end.dateToString(format: "yyyy")
			weekNameLbel.text = "\(startDate) - \(endDate)"
			
		}
//		self.presenter?.fetchSleepSummary(timeStampRange: range.intervalRange, dataType: dataType, date: range.start.dateToString(format: DateFormat.serverDateFormat))
		
		if dataRangeType == .daily {
			let dateBefore = headerView.selectedDate.end.dateBefore(days: 1)
			//currentSleepStartDate = dateBefore.setTime(hour: 22, min: 0, sec: 0)
			//currentSleepEndDate = headerView.selectedDate.end.setTime(hour: 6, min: 0, sec: 0)
			//let dateRange = DateRange(start: currentSleepStartDate!, end: currentSleepEndDate!, type: .daily)
			self.presenter?.fetchSleepTimeData(forDate: selectedDateRange.end)
		} else {
			self.presenter?.fetchSleepStages(range: DateRange(headerView.selectedDate.start, type: .weekly))
		}
		
	}
	
	func loadOxygenChartForDay(chartData: ActivityChartData, limitLineValues:[Double] = [120, 85]) {
		
		var xAxis = [TimeInterval]()
		var limitLineData = [[[Double?]]]()
		xAxis = chartData.elements.map({ (element) -> TimeInterval in
			return element.date.timeIntervalSince1970
		})
		
		var data = [[Double?]]()
		
		for x in 0..<limitLineValues.count {
			limitLineData.append([[Double?]]())
			for (pointIndex, each) in chartData.elements.enumerated() {
				data.append([xAxis[pointIndex] * 1000, each.value])
				limitLineData[x].append(([xAxis[pointIndex] * 1000, limitLineValues[x]]))
			}
		}
		
		let values = [data]
		let chart = HIChart()
		chart.type = "line"
		chart.marginBottom = 70
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
		
		if dataRangeType == .daily {
			hixAxis.dateTimeLabelFormats.day = HIDay()
			hixAxis.dateTimeLabelFormats.day.main = "%l %p"
			hixAxis.dateTimeLabelFormats.hour = HIHour()
			hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
		} else if dataRangeType == .weekly {
			hixAxis.dateTimeLabelFormats.day = HIDay()
			hixAxis.dateTimeLabelFormats.day.main = "%a"
		}
		
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		
		hixAxis.tickWidth = 2
		hixAxis.lineWidth = 0
		
		if dataRangeType == .daily {
			hixAxis.minRange = NSNumber(value: 3600 * 500)
			hixAxis.tickInterval = NSNumber(value: 3600 * 1000 * 3)
		} else if dataRangeType == .weekly {
			hixAxis.minRange = NSNumber(value: 3600 * 24000)
			hixAxis.tickInterval = NSNumber(value: 3600 * 24000)
		}
		
		hixAxis.lineWidth = 0
		
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.overflow = "justify"
		yAxis.labels.style = HICSSObject()
		yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		yAxis.labels.style.fontSize = "12"
		yAxis.labels.style.color = "#a5a5b2"
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotoptions = HIPlotOptions()
		plotoptions.line = HILine()
		plotoptions.line.states = HIStates()
		plotoptions.line.states.inactive = HIInactive()
		plotoptions.line.states.inactive.opacity = NSNumber(value: true)
		plotoptions.line.animation = HIAnimationOptionsObject()
		plotoptions.line.animation.duration = NSNumber(value: 0)
		
		hixAxis.labels.enabled = true
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		var finalSeries:[HISeries] = []
		let series = HISeries()
		let uiColors = [chartData.barColor.hexString]
		for y in 0..<limitLineData.count {
			series.data = values[0] as [Any]
			series.lineWidth = 4.0
			series.threshold = NSNumber(value: limitLineValues[y])
			series.negativeColor = ((y == 1) ? HIColor(uiColor: .barPink) : HIColor(uiColor: .barPurple))
				
			finalSeries.append(series)
		}
		
		let marker = HIMarker()
		marker.enabled = false
		series.marker = HIMarker()
		
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		for y in 0..<limitLineData.count {
			let limitLineSeries = HISeries()
			limitLineSeries.data = limitLineData[y]
			limitLineSeries.dashStyle = "shortdot"
			limitLineSeries.color = HIColor(uiColor: UIColor.textColor)
			limitLineSeries.marker = marker
			
			finalSeries.append(limitLineSeries)
		}
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = finalSeries
		options.colors = uiColors
		options.plotOptions = plotoptions
		options.time = time
		options.legend = legend
		
		self.dayLineChartView.options = options
	}
	
	func loadSleepChartForWeek(chartData: ActivityChartData, limitLineValues:[Double] = [120, 85]) {
		
		var xAxis = [TimeInterval]()
		xAxis = chartData.elements.map({ (element) -> TimeInterval in
			return element.date.timeIntervalSince1970
		})
		
		var data = [[Double?]]()
		for (pointIndex, each) in chartData.elements.enumerated() {
			data.append([xAxis[pointIndex] * 1000, each.value])
		}
		
		let values = [data]
		let chart = HIChart()
		chart.type = "area"
		chart.marginBottom = 70
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
//		chart.events.load = HIFunction(jsFunction: "function() {this.series.forEach(series => {series.points.forEach(point => {point.update({marker: {enabled: false}})})})}")
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
		
		if dataRangeType == .daily {
			hixAxis.dateTimeLabelFormats.day = HIDay()
			hixAxis.dateTimeLabelFormats.day.main = "%l %p"
			hixAxis.dateTimeLabelFormats.hour = HIHour()
			hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
			
			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.isFirst || this.isLast) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + ' ' + ampm; return strTime } else { return '' } }")
		} else if dataRangeType == .weekly {
			hixAxis.dateTimeLabelFormats.day = HIDay()
			hixAxis.dateTimeLabelFormats.day.main = "%a"
			
			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ let weekDaysArray = ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa']; if(this.isFirst || this.isLast) { let theDay = new Date(this.value).getDay(); return weekDaysArray[theDay] } else { return '' } }")
		}
		
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		
		hixAxis.tickWidth = 2
		hixAxis.lineWidth = 0
		
		if dataRangeType == .daily {
			hixAxis.minRange = NSNumber(value: 1800000)//3600 * 500
			hixAxis.tickInterval = NSNumber(value: 10800000)//3600 * 1000 * 3
		} else if dataRangeType == .weekly {
			hixAxis.minRange = NSNumber(value: 86400000)//3600 * 24000
			hixAxis.tickInterval = NSNumber(value: 86400000)//3600 * 24000
		}
		
		hixAxis.lineWidth = 0
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.overflow = "justify"
		yAxis.labels.style = HICSSObject()
		yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		yAxis.labels.style.fontSize = "12"
		yAxis.labels.style.color = "#a5a5b2"
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotoptions = HIPlotOptions()
		plotoptions.area = HIArea()
		plotoptions.area.states = HIStates()
		plotoptions.area.states.inactive = HIInactive()
		plotoptions.area.states.inactive.opacity = NSNumber(value: true)
		plotoptions.area.animation = HIAnimationOptionsObject()
		plotoptions.area.animation.duration = NSNumber(value: 0)
		
		hixAxis.labels.enabled = true
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		let series = HISeries()
		let uiColors = [chartData.barColor.hexString]
		series.data = values[0] as [Any]
		series.lineWidth = 4.0
		
		let marker = HIMarker()
		marker.enabled = false
		series.marker = HIMarker()
		
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = [series]
		options.colors = uiColors
		options.plotOptions = plotoptions
		options.time = time
		options.legend = legend
		
		self.weekChartView.options = options
	}
	
	func areaChartDemo(chartData: ActivityChartData) {
		var xAxis = [TimeInterval]()
		xAxis = chartData.elements.map({ (element) -> TimeInterval in
			return element.date.timeIntervalSince1970
		})
		
		var data = [[Double?]]()
		for (pointIndex, each) in chartData.elements.enumerated() {
			data.append([xAxis[pointIndex] * 1000, each.value])
			/*  let eachValue = (each.value == 0) ? nil : each.value
			 data.append([xAxis[pointIndex] * 1000, eachValue]) */
		}
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
		
		let range: DateRange
		
		if dataRangeType == .daily {
			hixAxis.dateTimeLabelFormats.day = HIDay()
			hixAxis.dateTimeLabelFormats.day.main = "%l %p"
			hixAxis.dateTimeLabelFormats.hour = HIHour()
			hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
			
//			hixAxis.minRange = NSNumber(value: 1800000)//3600 * 500
//			hixAxis.tickInterval = NSNumber(value: 10800000)//3600 * 1000 * 3
			
			range = DateRange(headerView.selectedDate.start, type: .daily)
		} else {
			hixAxis.dateTimeLabelFormats.day = HIDay()
			hixAxis.dateTimeLabelFormats.day.main = "%a"
			
			hixAxis.minRange = NSNumber(value: 86400000)//3600 * 24000
			hixAxis.tickInterval = NSNumber(value: 86400000)//3600 * 24000
			hixAxis.tickAmount = 7
			
			range = DateRange(headerView.selectedDate.start, type: .weekly)
		}
		
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		
		hixAxis.tickWidth = 0

//		hixAxis.min = NSNumber(value: ((range.start.startOfDay.timeIntervalSince1970)) * 1000)
//		hixAxis.max = NSNumber(value: ((range.end.startOfDay.timeIntervalSince1970)) * 1000)
		hixAxis.lineWidth = 0
		
		hixAxis.tickColor = HIColor(uiColor: UIColor.clear)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		if dataRangeType == .daily {
			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.isFirst || this.isLast) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + ' ' + ampm; return strTime } else { return '' } }")
		} else if dataRangeType == .weekly {
			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ let weekDaysArray = ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa']; let theDay = new Date(this.value).getDay(); return weekDaysArray[theDay]  }")
		}
		
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.overflow = "justify"
		yAxis.labels.style = HICSSObject()
		yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		yAxis.labels.style.fontSize = "12"
		yAxis.labels.style.color = "#a5a5b2"
		yAxis.gridLineWidth = 0
		yAxis.minTickInterval = 120
		yAxis.tickInterval = 120
		yAxis.labels.formatter = HIFunction(jsFunction: "function(){ return ((this.value / 60) + 'h') }")
		
		hixAxis.labels.enabled = true
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		
		let options = HIOptions()
		let values = [data]
		let chart = HIChart()
		chart.type = (dataRangeType == .daily) ? "line": "area"
		chart.backgroundColor = HIColor(uiColor: .clear)
		options.chart = chart
		
		let title = HITitle()
		title.text = ""
		options.title = title
		
		let subtitle = HISubtitle()
		subtitle.text = ""
		options.subtitle = subtitle
		
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotOptions = HIPlotOptions()
		
		if dataRangeType == .daily {
			plotOptions.line = HILine()
			plotOptions.line.states = HIStates()
			plotOptions.line.states.inactive = HIInactive()
			plotOptions.line.states.inactive.opacity = NSNumber(value: true)
			plotOptions.line.animation = HIAnimationOptionsObject()
			plotOptions.line.animation.duration = NSNumber(value: 0)
//			plotOptions.series = HISeries()
//			plotOptions.series.pointStart = 2010
			plotOptions.line.marker = HIMarker()
			plotOptions.line.marker.enabled = false
			options.plotOptions = plotOptions
		} else {
			plotOptions.area = HIArea()
			plotOptions.area.lineColor = HIColor(hexValue: "ffffff")
			plotOptions.area.lineWidth = 1
			plotOptions.area.marker = HIMarker()
			plotOptions.area.marker.lineWidth = 0
			plotOptions.area.marker.symbol = "triangle"
			plotOptions.area.marker.enabled = false
			plotOptions.area.marker.states = HIStates()
			plotOptions.area.marker.states.hover = HIHover()
			plotOptions.area.marker.states.hover.enabled = false
			plotOptions.area.accessibility = HISeriesAccessibility()
			plotOptions.area.accessibility.pointDescriptionFormatter = HIFunction(jsFunction: "function (point) { function round(x) { return Math.round(x * 100) / 100; } return (point.index + 1) + ', ' + point.category + ', ' + point.y + ' millions, ' + round(point.percentage) + '%, ' + point.series.name; }")
			options.plotOptions = plotOptions
		}
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		var valuesArray = [[Int]](repeating: [], count:7)
		var awakeArray = [[Double?]](repeating: [], count:7)
		var remArray = [[Double?]](repeating: [], count:7)
		var lightArray = [[Double?]](repeating: [], count:7)
		var deepArray = [[Double?]](repeating: [], count:7)
		let dateList = Date.dates(from: range.start, to: range.end, interval: 1, component: .day)
		for x in 0..<chartData.elements.count {
			valuesArray[(chartData.elements[x].date.dayNumberOfWeek()! - 1)].append(Int(chartData.elements[x].value!))
		}
	
		if dataRangeType == .weekly {
			for x in 0..<dateList.count {
				let countItems = valuesArray[x].map{($0,1)}
				let counts = Dictionary(countItems, uniquingKeysWith: +)
				// deep light rem awake
				let awakeSleep = (counts[1] ?? 0) + (counts[2] ?? 0) + (counts[3] ?? 0) + (counts[4] ?? 0)
				let remSleep = (counts[2] ?? 0) + (counts[3] ?? 0) + (counts[1] ?? 0)
				let lightSleep = (counts[1] ?? 0) + (counts[2] ?? 0)
				let deepSleep = (counts[1] ?? 0)
				
//				let awakeSleep = (counts[4] ?? 0)
//				let remSleep = (counts[3] ?? 0) + (counts[4] ?? 0)
//				let lightSleep = (counts[2] ?? 0) + (counts[3] ?? 0) + (counts[4] ?? 0)
//				let deepSleep = (counts[1] ?? 0) + (counts[2] ?? 0) + (counts[3] ?? 0) + (counts[4] ?? 0)
				
				awakeArray[x] = ([dateList[x].timeIntervalSince1970 * 1000, Double(awakeSleep * 5)])
				remArray[x] = ([dateList[x].timeIntervalSince1970 * 1000, Double(remSleep * 5)])
				lightArray[x] = ([dateList[x].timeIntervalSince1970 * 1000, Double(lightSleep * 5)])
				deepArray[x] = ([dateList[x].timeIntervalSince1970 * 1000, Double(deepSleep * 5)])
			}
		} else {
			let countItems = valuesArray[(range.end.dayNumberOfWeek()! - 1)].map{($0,1)}
			let counts = Dictionary(countItems, uniquingKeysWith: +)
			awakeArray[0] = ([dateList[0].timeIntervalSince1970 * 1000, Double(counts[4] ?? 0)])
			remArray[0] = ([dateList[0].timeIntervalSince1970 * 1000, Double(counts[3] ?? 0)])
			lightArray[0] = ([dateList[0].timeIntervalSince1970 * 1000, Double(counts[2] ?? 0)])
			deepArray[0] = ([dateList[0].timeIntervalSince1970 * 1000, Double(counts[1] ?? 0)])
			
			let yesterdayTime = ((range.end.dayNumberOfWeek()! - 1) == 0) ? 6 : (range.end.dayNumberOfWeek()! - 2)
			let yesterdayCountItems = valuesArray[yesterdayTime].map{($0,1)}
			let yesterdayCounts = Dictionary(yesterdayCountItems, uniquingKeysWith: +)
			awakeArray[1] = ([dateList[0].timeIntervalSince1970 * 1000, Double(yesterdayCounts[4] ?? 0)])
			remArray[1] = ([dateList[0].timeIntervalSince1970 * 1000, Double(yesterdayCounts[3] ?? 0)])
			lightArray[1] = ([dateList[0].timeIntervalSince1970 * 1000, Double(yesterdayCounts[2] ?? 0)])
			deepArray[1] = ([dateList[0].timeIntervalSince1970 * 1000, Double(yesterdayCounts[1] ?? 0)])
		}
		
		for x in 0..<dateList.count {
			awakeArray[x][1] = (awakeArray[x].last == Double(0)) ? nil : awakeArray[x][1]
			remArray[x][1] = (remArray[x].last == Double(0)) ? nil : remArray[x][1]
			lightArray[x][1] = (lightArray[x].last == Double(0)) ? nil : lightArray[x][1]
			deepArray[x][1] = (deepArray[x].last == Double(0)) ? nil : deepArray[x][1]
		}
		
		let deep = HIArea()
		deep.marker = HIMarker()
		deep.marker.lineWidth = 0
		deep.marker.symbol = "triangle"
		deep.marker.states = HIStates()
		deep.marker.states.hover = HIHover()
		deep.marker.states.hover.enabled = false
		deep.marker.enabled = false
		deep.name = "Deep"
		deep.data = deepArray
		deep.color = HIColor(uiColor: UIColor.init(named: "Deep Color"))
		deep.lineWidth = 0

		let awake = HIArea()
		awake.marker = HIMarker()
		awake.marker.lineWidth = 0
		awake.marker.symbol = "triangle"
		awake.marker.enabled = false
		awake.marker.states = HIStates()
		awake.marker.states.hover = HIHover()
		awake.marker.states.hover.enabled = false
		awake.name = "Awake"
		awake.data = awakeArray
		awake.color = HIColor(uiColor: UIColor.init(named: "Awake Color"))
		awake.lineWidth = 0

		let rem = HIArea()
		rem.marker = HIMarker()
		rem.marker.lineWidth = 0
		rem.marker.symbol = "triangle"
		rem.marker.enabled = false
		rem.marker.states = HIStates()
		rem.marker.states.hover = HIHover()
		rem.marker.states.hover.enabled = false
		rem.name = "Rem"
		rem.data = remArray
		rem.color = HIColor(uiColor: UIColor.init(named: "Rem Color"))
		rem.lineWidth = 0

		let light = HIArea()
		light.marker = HIMarker()
		light.marker.symbol = "triangle"
		light.marker.lineWidth = 0
		light.marker.enabled = false
		light.marker.states = HIStates()
		light.marker.states.hover = HIHover()
		light.marker.states.hover.enabled = false
		light.name = "Light"
		light.data = lightArray
		light.color = HIColor(uiColor: UIColor.init(named: "Light Color"))
		light.lineWidth = 0
		
		let dailySeries = HISeries()
		dailySeries.data = values
		dailySeries.step = "center"
		dailySeries.shadow = 5
		let marker = HIMarker()
		marker.enabled = false
		dailySeries.marker = HIMarker()
		
		dailySeries.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
								[0, "rgb(178 184 229)"],
								[0.35, "rgb(100 71 252)"],
								[0.7, "rgb(141 100 196)"],
								[1, "rgb(124 43 144)"]])
		
		if dataRangeType == .daily {
			let totalDeepSleepForTime = ((deepArray[0][1] ?? 0) * 5) + ((deepArray[1][1] ?? 0) * 5)
			let totalAwakeSleepForTime = ((awakeArray[0][1] ?? 0) * 5) + ((awakeArray[1][1] ?? 0) * 5)
			let totalLightSleepForTime = ((lightArray[0][1] ?? 0) * 5) + ((lightArray[1][1] ?? 0) * 5)
			let totalRemSleepForTime = ((remArray[0][1] ?? 0) * 5) + ((remArray[1][1] ?? 0) * 5)
			
			setupSleepUI(deep: totalDeepSleepForTime, awake: totalAwakeSleepForTime, light: totalLightSleepForTime, rem: totalRemSleepForTime)
			options.series = [dailySeries]
		} else {
			options.series = [awake, rem, light, deep]
		}
		
		options.tooltip = tooltip
		options.legend = legend
		options.credits = credits
		options.exporting = exporting
		options.time = time
		
		if dataRangeType == .daily {
			options.responsive = HIResponsive()
			
			let rule1 = HIRules()
			rule1.condition = HICondition()
			rule1.condition.maxWidth = 500
			rule1.chartOptions = [
				legend: [
					"layout": "horizontal",
					"align": "center",
					"verticalAlign": "bottom"
				]
			]
			
			options.responsive.rules = [HIRules()]
		} else {
			self.weekChartView.options = options
			self.weekChartView.sizeToFit()
		}
		
	}
	
	func areaChartDemoUsingDailyAverages(chartData: [ActivityChartData]) {
		
		var xAxis = [TimeInterval]()
		xAxis = chartData[0].elements.map({ (element) -> TimeInterval in
			return element.date.timeIntervalSince1970
		})

		var deepData = [[Double?]]()
		var lightData = [[Double?]]()
		var remData = [[Double?]]()
		var awakeData = [[Double?]]()

		for (pointIndex, each) in chartData[0].elements.enumerated() {
			deepData.append([xAxis[pointIndex] * 1000, each.value])
		}
		
		for (pointIndex, each) in chartData[1].elements.enumerated() {
			lightData.append([xAxis[pointIndex] * 1000, each.value])
		}

		for (pointIndex, each) in chartData[2].elements.enumerated() {
			remData.append([xAxis[pointIndex] * 1000, each.value])
		}

		for (pointIndex, each) in chartData[3].elements.enumerated() {
			awakeData.append([xAxis[pointIndex] * 1000, each.value])
		}
		
		for x in 0..<7 {
			// deep light rem awake
			if deepData.count > x {
				let awakeSleep = (deepData[x][1] ?? 0.0) + (lightData[x][1] ?? 0.0) + (remData[x][1] ?? 0.0) + (awakeData[x][1] ?? 0.0)
				let remSleep = (deepData[x][1] ?? 0.0) + (lightData[x][1] ?? 0.0) + (remData[x][1] ?? 0.0)
				let lightSleep = (deepData[x][1] ?? 0.0) + (lightData[x][1] ?? 0.0)
				let deepSleep = (deepData[x][1] ?? 0.0)
				
				awakeData[x] = (awakeSleep != 0) ? [(xAxis[x] * 1000), awakeSleep] : [(xAxis[x] * 1000), nil]
				remData[x] = (remSleep != 0) ? [(xAxis[x] * 1000), remSleep] : [(xAxis[x] * 1000), nil]
				lightData[x] = (remSleep != 0) ? [(xAxis[x] * 1000), lightSleep] : [(xAxis[x] * 1000), nil]
				deepData[x] = (remSleep != 0) ? [(xAxis[x] * 1000), deepSleep] : [(xAxis[x] * 1000), nil]
			}
		}
		
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
		hixAxis.dateTimeLabelFormats.day = HIDay()
		hixAxis.dateTimeLabelFormats.day.main = "%a"
		hixAxis.minRange = NSNumber(value: 86400000)//3600 * 24000
		hixAxis.tickInterval = NSNumber(value: 86400000)//3600 * 24000
		hixAxis.tickAmount = 7
		
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		hixAxis.tickWidth = 0
		hixAxis.lineWidth = 0
		hixAxis.tickColor = HIColor(uiColor: UIColor.clear)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ let weekDaysArray = ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa']; let theDay = new Date(this.value).getDay(); return weekDaysArray[theDay]  }")
		
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.overflow = "justify"
		yAxis.labels.style = HICSSObject()
		yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		yAxis.labels.style.fontSize = "12"
		yAxis.labels.style.color = "#a5a5b2"
		yAxis.gridLineWidth = 0
		yAxis.minTickInterval = 120
		yAxis.tickInterval = 120
		yAxis.labels.formatter = HIFunction(jsFunction: "function(){ return ((this.value / 60) + 'h') }")
		
		hixAxis.labels.enabled = true
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		
		let minPlotLine = HIPlotLines()
//		minPlotLine.value = 0
		minPlotLine.width = 1
		minPlotLine.color = HIColor(uiColor: .white)
		yAxis.plotLines = [minPlotLine]
		
		let options = HIOptions()
		let chart = HIChart()
		chart.type = "area"
		chart.backgroundColor = HIColor(uiColor: .clear)
		options.chart = chart
		
		let title = HITitle()
		title.text = ""
		options.title = title
		
		let subtitle = HISubtitle()
		subtitle.text = ""
		options.subtitle = subtitle
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotOptions = HIPlotOptions()
		plotOptions.area = HIArea()
		plotOptions.area.lineColor = HIColor(hexValue: "ffffff")
		plotOptions.area.lineWidth = 1
		plotOptions.area.marker = HIMarker()
		plotOptions.area.marker.lineWidth = 0
		plotOptions.area.marker.symbol = "triangle"
		plotOptions.area.marker.enabled = false
		plotOptions.area.marker.states = HIStates()
		plotOptions.area.marker.states.hover = HIHover()
		plotOptions.area.marker.states.hover.enabled = false
		plotOptions.area.accessibility = HISeriesAccessibility()
		plotOptions.area.accessibility.pointDescriptionFormatter = HIFunction(jsFunction: "function (point) { function round(x) { return Math.round(x * 100) / 100; } return (point.index + 1) + ', ' + point.category + ', ' + point.y + ' millions, ' + round(point.percentage) + '%, ' + point.series.name; }")
		options.plotOptions = plotOptions
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		let deep = HIArea()
		deep.marker = HIMarker()
		deep.marker.lineWidth = 0
		deep.marker.symbol = "triangle"
		deep.marker.states = HIStates()
		deep.marker.states.hover = HIHover()
		deep.marker.states.hover.enabled = false
		deep.marker.enabled = false
		deep.name = "Deep"
		deep.data = deepData
		deep.color = HIColor(uiColor: UIColor.init(named: "Deep Color"))
		deep.lineWidth = 0
		
		let awake = HIArea()
		awake.marker = HIMarker()
		awake.marker.lineWidth = 0
		awake.marker.symbol = "triangle"
		awake.marker.enabled = false
		awake.marker.states = HIStates()
		awake.marker.states.hover = HIHover()
		awake.marker.states.hover.enabled = false
		awake.name = "Awake"
		awake.data = awakeData
		awake.color = HIColor(uiColor: UIColor.init(named: "Awake Color"))
		awake.lineWidth = 0
		
		let rem = HIArea()
		rem.marker = HIMarker()
		rem.marker.lineWidth = 0
		rem.marker.symbol = "triangle"
		rem.marker.enabled = false
		rem.marker.states = HIStates()
		rem.marker.states.hover = HIHover()
		rem.marker.states.hover.enabled = false
		rem.name = "Rem"
		rem.data = remData
		rem.color = HIColor(uiColor: UIColor.init(named: "Rem Color"))
		rem.lineWidth = 0
		
		let light = HIArea()
		light.marker = HIMarker()
		light.marker.symbol = "triangle"
		light.marker.lineWidth = 0
		light.marker.enabled = false
		light.marker.states = HIStates()
		light.marker.states.hover = HIHover()
		light.marker.states.hover.enabled = false
		light.name = "Light"
		light.data = lightData
		light.color = HIColor(uiColor: UIColor.init(named: "Light Color"))
		light.lineWidth = 0
		
		options.series = [awake, rem, light, deep]
		options.tooltip = tooltip
		options.legend = legend
		options.credits = credits
		options.exporting = exporting
		options.time = time
		
		self.weekChartView.options = options
		self.weekChartView.sizeToFit()
		
	}
	
	func loadDayChartForSleep(chartData: ActivityChartData) {
		var xAxis = [TimeInterval]()
		xAxis = chartData.elements.map({ (element) -> TimeInterval in
			return element.date.timeIntervalSince1970
		})

		var data = [[Double?]]()

		for (pointIndex, each) in chartData.elements.enumerated() {
			data.append([xAxis[pointIndex] * 1000, each.value])
		}
//
		var values = [data]
		let chart = HIChart()
		chart.type = "line"
//		chart.marginBottom = 70
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
		
		hixAxis.dateTimeLabelFormats.day = HIDay()
		hixAxis.dateTimeLabelFormats.day.main = "%l %p"
		hixAxis.dateTimeLabelFormats.hour = HIHour()
		hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
		
		if currentSleepStartDate != nil {
			let hour = Calendar.current.component(.hour, from: currentSleepStartDate!)
			currentSleepEndDate?.setTime(hour: hour, min: 0, sec: 0)
			hixAxis.min = NSNumber(value: (1000 * currentSleepStartDate!.timeIntervalSince1970))
		}
		hixAxis.tickInterval = NSNumber(value: 6000)//3600 * 1000 * 3
		
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		hixAxis.gridLineWidth = 0
		hixAxis.minorGridLineWidth = 0
		hixAxis.lineWidth = 0
		hixAxis.tickColor = HIColor(uiColor: UIColor.clear)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		hixAxis.labels.rotation = NSNumber(value: 0)
		if dataRangeType == .daily {
			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.isFirst || this.isLast) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + ' ' + ampm; return strTime } else { return '' } }")
			
//			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ let theHours = new Date(this.value); console.log(theHours); return theHours }")
		}
		
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.overflow = "justify"
		yAxis.labels.style = HICSSObject()
		yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		yAxis.labels.style.fontSize = "12"
		yAxis.labels.style.color = "#a5a5b2"
		yAxis.gridLineWidth = 0
		yAxis.minorGridLineWidth = 0
		yAxis.min = 0.5
		yAxis.max = 4
		yAxis.minTickInterval = 0.5
		yAxis.tickInterval = 0.5
		
		let minPlotLine = HIPlotLines()
		minPlotLine.value = yAxis.min
		minPlotLine.width = 1
		minPlotLine.color = HIColor(uiColor: .white)
		yAxis.plotLines = [minPlotLine]
		
		if dataRangeType == .daily {
			yAxis.labels.formatter = HIFunction(jsFunction: "function(){ let weekDaysArray = ['', 'D', 'L', 'R', 'A']; return weekDaysArray[this.value]  }")
		}
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotoptions = HIPlotOptions()
		plotoptions.line = HILine()
		plotoptions.line.states = HIStates()
		plotoptions.line.states.inactive = HIInactive()
		plotoptions.line.states.inactive.opacity = NSNumber(value: true)
		plotoptions.line.animation = HIAnimationOptionsObject()
		plotoptions.line.animation.duration = NSNumber(value: 0)
//		plotoptions.series = HISeries()
//		plotoptions.series.pointStart = 2010
		plotoptions.line.marker = HIMarker()
		plotoptions.line.marker.enabled = false
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		let series = HISeries()
		let series1 = HISeries()
		let series2 = HISeries()
		let series3 = HISeries()
		let series4 = HISeries()
		let series5 = HISeries()
		let series6 = HISeries()
		let series7 = HISeries()
		let series8 = HISeries()
		let series9 = HISeries()
		let series10 = HISeries()
		let series11 = HISeries()
		let series12 = HISeries()
		let series13 = HISeries()
		let uiColors = [chartData.barColor.hexString]
		
		let dataTemp1 = chartData.elements.map({ (element) -> Double in
			return ((element.value! - 0.01))
		})
		
		let dataTemp2 = chartData.elements.map({ (element) -> Double in
			return ((element.value! - 0.02))
		})
		
		let dataTemp3 = chartData.elements.map({ (element) -> Double in
			return ((element.value! - 0.03))
		})
		
		let dataTemp4 = chartData.elements.map({ (element) -> Double in
			return ((element.value! - 0.04))
		})
		
		let dataTemp5 = chartData.elements.map({ (element) -> Double in
			return ((element.value! - 0.05))
		})
		
		let dataTemp6 = chartData.elements.map({ (element) -> Double in
			return ((element.value! - 0.06))
		})
		
		let dataTemp7 = chartData.elements.map({ (element) -> Double in
			return ((element.value! + 0.00))
		})
		
		let dataTemp8 = chartData.elements.map({ (element) -> Double in
			return ((element.value! - 0.07))
		})
		
		let dataTemp9 = chartData.elements.map({ (element) -> Double in
			return ((element.value! - 0.08))
		})
		
		let dataTemp10 = chartData.elements.map({ (element) -> Double in
			return ((element.value! - 0.09))
		})
		
		let dataTemp11 = chartData.elements.map({ (element) -> Double in
			return ((element.value! - 0.10))
		})
		
		let dataTemp12 = chartData.elements.map({ (element) -> Double in
			return ((element.value! - 0.11))
		})
		
		let dataTemp13 = chartData.elements.map({ (element) -> Double in
			return ((element.value! - 0.12))
		})
		
		var data1 = [[Double?]]()
		var data2 = [[Double?]]()
		var data3 = [[Double?]]()
		var data4 = [[Double?]]()
		var data5 = [[Double?]]()
		var data6 = [[Double?]]()
		var data7 = [[Double?]]()
		var data8 = [[Double?]]()
		var data9 = [[Double?]]()
		var data10 = [[Double?]]()
		var data11 = [[Double?]]()
		var data12 = [[Double?]]()
		var data13 = [[Double?]]()
		
		for (pointIndex, _) in chartData.elements.enumerated() {
			data1.append([xAxis[pointIndex] * 1000, dataTemp1[pointIndex]])
			data2.append([xAxis[pointIndex] * 1000, dataTemp2[pointIndex]])
			data3.append([xAxis[pointIndex] * 1000, dataTemp3[pointIndex]])
			data4.append([xAxis[pointIndex] * 1000, dataTemp4[pointIndex]])
			data5.append([xAxis[pointIndex] * 1000, dataTemp5[pointIndex]])
			data6.append([xAxis[pointIndex] * 1000, dataTemp6[pointIndex]])
			data7.append([xAxis[pointIndex] * 1000, dataTemp7[pointIndex]])
			data8.append([xAxis[pointIndex] * 1000, dataTemp8[pointIndex]])
			data9.append([xAxis[pointIndex] * 1000, dataTemp9[pointIndex]])
			data10.append([xAxis[pointIndex] * 1000, dataTemp10[pointIndex]])
			data11.append([xAxis[pointIndex] * 1000, dataTemp11[pointIndex]])
			data12.append([xAxis[pointIndex] * 1000, dataTemp12[pointIndex]])
			data13.append([xAxis[pointIndex] * 1000, dataTemp13[pointIndex]])
		}

		for x in 0..<values[0].count {
			values[0][x][1] = (values[0][x][1] == Double(0)) ? nil : values[0][x][1]
			data1[x][1] = (data1[x][1] == Double(0 - 0.01)) ? nil : data1[x][1]
			data2[x][1] = (data2[x][1] == Double(0 - 0.02)) ? nil : data2[x][1]
			data3[x][1] = (data3[x][1] == Double(0 - 0.03)) ? nil : data3[x][1]
			data4[x][1] = (data4[x][1] == Double(0 - 0.04)) ? nil : data4[x][1]
			data5[x][1] = (data5[x][1] == Double(0 - 0.05)) ? nil : data5[x][1]
			data6[x][1] = (data6[x][1] == Double(0 - 0.06)) ? nil : data6[x][1]
			data7[x][1] = (data7[x][1] == Double(0)) ? nil : data7[x][1]
			data8[x][1] = (data8[x][1] == Double(0 - 0.07)) ? nil : data8[x][1]
			data9[x][1] = (data9[x][1] == Double(0 - 0.08)) ? nil : data9[x][1]
			data10[x][1] = (data10[x][1] == Double(0 - 0.09)) ? nil : data10[x][1]
			data11[x][1] = (data11[x][1] == Double(0 - 0.10)) ? nil : data11[x][1]
			data12[x][1] = (data12[x][1] == Double(0 - 0.11)) ? nil : data12[x][1]
			data13[x][1] = (data13[x][1] == Double(0 - 0.12)) ? nil : data13[x][1]
		}
		
		let marker = HIMarker()
		marker.enabled = false
		series.marker = HIMarker()
		series.data = values[0] as [Any]
		series.step = "center"
		series.shadow = 5
		series.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series.lineWidth = 1
		
		series1.marker = HIMarker()
		series1.data = data1
		series1.step = "center"
//		series1.shadow = 5
		series1.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series1.lineWidth = 1
		
		series2.marker = HIMarker()
		series2.data = data2
		series2.step = "center"
//		series2.shadow = 5
		series2.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series2.lineWidth = 1
		
		series3.marker = HIMarker()
		series3.data = data3
		series3.step = "center"
//		series3.shadow = 5
		series3.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series3.lineWidth = 1
		
		series4.marker = HIMarker()
		series4.data = data4
		series4.step = "center"
//		series4.shadow = 5
		series4.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series4.lineWidth = 1
		
		series5.marker = HIMarker()
		series5.data = data5
		series5.step = "center"
//		series5.shadow = 5
		series5.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series5.lineWidth = 1
		
		series6.marker = HIMarker()
		series6.data = data6
		series6.step = "center"
//		series6.shadow = 5
		series6.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series6.lineWidth = 1
		
		series7.marker = HIMarker()
		series7.data = data7
		series7.step = "center"
//		series7.shadow = 5
		series7.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series7.lineWidth = 1
		
		series8.marker = HIMarker()
		series8.data = data8
		series8.step = "center"
//		series8.shadow = 5
		series8.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series8.lineWidth = 1
		
		series9.marker = HIMarker()
		series9.data = data9
		series9.step = "center"
//		series9.shadow = 5
		series9.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series9.lineWidth = 1
		
		series10.marker = HIMarker()
		series10.data = data10
		series10.step = "center"
//		series10.shadow = 5
		series10.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series10.lineWidth = 1
		
		series11.marker = HIMarker()
		series11.data = data11
		series11.step = "center"
//		series11.shadow = 5
		series11.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series11.lineWidth = 1
		
		series12.marker = HIMarker()
		series12.data = data12
		series12.step = "center"
//		series12.shadow = 5
		series12.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series12.lineWidth = 1
		
		series13.marker = HIMarker()
		series13.data = data13
		series13.step = "center"
//		series13.shadow = 5
		series13.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "rgb(197 203 248)"],
			[0.35, "rgb(54 93 228)"],
			[0.7, "rgb(103 12 174)"],
			[1, "rgb(183 12 210)"]])
		series13.lineWidth = 1
	
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = [series1, series2, series3, series4, series5, series6, series7, series8, series9, series10, series11, series12, series13]
		options.colors = uiColors
		options.plotOptions = plotoptions
		options.time = time
		options.legend = legend
		options.responsive = HIResponsive()
		
		let rule1 = HIRules()
		rule1.condition = HICondition()
		rule1.condition.maxWidth = 500
		rule1.chartOptions = [
			legend: [
				"layout": "horizontal",
				"align": "center",
				"verticalAlign": "bottom"
			]
		]
		
		options.responsive.rules = [HIRules()]
		
		self.dayBarChartView.options = options
	}
	
	func pieChartForStages(rem: Double, deep: Double, light: Double) {
		let totalSleep = rem + deep + light
		
		let chart = HIChart()
		chart.type = "pie"
//		chart.marginBottom = 70
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotoptions = HIPlotOptions()
		plotoptions.pie = HIPie()
		plotoptions.pie.states = HIStates()
		plotoptions.pie.states.inactive = HIInactive()
		plotoptions.pie.states.inactive.opacity = NSNumber(value: true)
		plotoptions.pie.animation = HIAnimationOptionsObject()
		plotoptions.pie.animation.duration = NSNumber(value: 0)
		plotoptions.series = HISeries()
		plotoptions.series.pointStart = 2010
		plotoptions.pie.marker = HIMarker()
		plotoptions.pie.marker.enabled = false
		
		let pie = HIPie()
		pie.name = ""
		
		if totalSleep > 0 {
			pie.data = [["name": "Rem", "y": rem/totalSleep], ["name": "Light", "y": light/totalSleep], ["name": "Deep", "y": deep/totalSleep]]
			plotoptions.pie.colors = [UIColor.init(named: "Rem Color")!.hexString,
									  UIColor.init(named: "Light Color")!.hexString,
									  UIColor.init(named: "Deep Color")!.hexString]
		} else {
			pie.data = [["name": "Rem", "y": 40], ["name": "Light", "y": 25], ["name": "Deep", "y": 40]]
//			plotoptions.pie.color = HIColor.init(uiColor: UIColor.systemGray3)
			plotoptions.pie.colors = ["#ABB0B8", "#ABB0B8", "#ABB0B8"]
		}
		
		pie.innerSize = "84%"
		pie.marker = HIMarker()
		pie.marker.enabled = false
		pie.marker.states = HIStates()
		pie.marker.states.hover = HIHover()
		pie.marker.states.hover.enabled = false
		pie.marker.states.select = HISelect()
		pie.marker.states.select.enabled = false
		pie.showInLegend = false
		pie.dataLabels = HIDataLabelsOptionsObject()
		pie.dataLabels.enabled = false
		pie.borderWidth = 0
		pie.enableMouseTracking = false
		pie.size = 132
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = [pie]
		options.plotOptions = plotoptions
		options.legend = legend
		options.responsive = HIResponsive()
		
		self.sleepStaegsPieChart.options = options
	}
	
	func barChart(isVisible: Bool, type: DateRangeType) {
		if type == .daily {
			self.dayBarChartView.isHidden = !isVisible
			self.noChartDataDayUIView.isHidden = isVisible
//			self.pieChartForStages(rem: 0, deep: 0, light: 0)
		} else {
			self.weekChartView.isHidden = !isVisible
			self.noChatDataWeekUIView.isHidden = isVisible
		}
	}
}

// MARK: - Calendar Callbacks
extension SleepStagesDetailsViewController {
	@objc private func calendarDateSelected(notification: NSNotification) {
		guard let object = notification.object as? DateRange else {
			return
		}
        if(object.type == .daily){
		self.selectedDateRange = object
        }
	}
}

extension SleepStagesDetailsViewController: SleepStagesDetailsDelegate {
	func fetchSleepSummary(result: Result<SleepStageModel, MovanoError>) {
		switch result {
		case .success(let dataList):
			self.dummyDataForStages = dataList
//			self.setupSleepUI()
		case .failure(let error):
			print(error)
		}
	}
	
	func fetchSleepStageSummary(chartElements: [ChartElement], value: Int, chartElementsForWeek:[ChartElement] ) {
		let chartData = ActivityChartData(barColor: .barPurple, elements: chartElements)
		let sleepStageModel = ActivityChartModel(type: "Sleep Stages", value: NSNumber(value: value), goal: nil, chartData: chartData)
//		self.loadSleepChartForWeek(chartData: sleepStageModel.chartData, limitLineValues: [])
		
		let thisdata = sleepStageModel.chartData.elements.reduce(0.0, { partialResult, element in
			return partialResult + (element.value ?? 0)
		})

		if dataRangeType == .daily {
			let chartDataWeek = ActivityChartData(barColor: .barPurple, elements: chartElementsForWeek)
			let sleepStageModelForWeek = ActivityChartModel(type: "Sleep Stages", value: NSNumber(value: value), goal: nil, chartData: chartDataWeek)
			areaChartDemo(chartData: sleepStageModelForWeek.chartData)
			loadDayChartForSleep(chartData: chartData)
			self.barChart(isVisible: (thisdata == 0) ? false : true, type: .daily)
		} else {
//			self.areaChartDemo(chartData: sleepStageModel.chartData)
			if currentSleepStartDate != nil && currentSleepEndDate != nil {
				self.barChart(isVisible: (thisdata == 0) ? false : true, type: .weekly)
			} else {
				self.barChart(isVisible: false, type: .weekly)
			}
		}
        hud.dismiss()
//		self.loadDayChartForSleep(chartData: ActivityChartData(barColor: .barPurple, elements: chartElements))
	}
	
	func fetchSleepStageSummaryForWeek(chartElements: [[ChartElement]], value: Int) {
		noChatDataWeekUIView.isHidden = true
		let deepChartData = ActivityChartData(barColor: .barPurple, elements: chartElements[0])
		let deepSleepStageModel = ActivityChartModel(type: "Sleep Stages", value: NSNumber(value: value), goal: nil, chartData: deepChartData)
		//		self.loadSleepChartForWeek(chartData: sleepStageModel.chartData, limitLineValues: [])
		
		let lightChartData = ActivityChartData(barColor: .barPurple, elements: chartElements[1])
		let lightSleepStageModel = ActivityChartModel(type: "Sleep Stages", value: NSNumber(value: value), goal: nil, chartData: lightChartData)
		
		let remChartData = ActivityChartData(barColor: .barPurple, elements: chartElements[2])
		let remSleepStageModel = ActivityChartModel(type: "Sleep Stages", value: NSNumber(value: value), goal: nil, chartData: remChartData)
		
		let awakeChartData = ActivityChartData(barColor: .barPurple, elements: chartElements[3])
		let awakeSleepStageModel = ActivityChartModel(type: "Sleep Stages", value: NSNumber(value: value), goal: nil, chartData: awakeChartData)
		
		self.areaChartDemoUsingDailyAverages(chartData: [deepSleepStageModel.chartData, lightSleepStageModel.chartData, remSleepStageModel.chartData, awakeSleepStageModel.chartData])
		
        hud.dismiss()
//		self.areaChartDemo(chartData: sleepStageModel.chartData)
//		if currentSleepStartDate != nil && currentSleepEndDate != nil {
//			self.barChart(isVisible: (thisdata == 0) ? false : true, type: .weekly)
//		} else {
//			self.barChart(isVisible: false, type: .weekly)
//		}
		
		//		self.loadDayChartForSleep(chartData: ActivityChartData(barColor: .barPurple, elements: chartElements))
	}
	
	func fetchOxygenData(result: Result<[CoreDataOxygen], MovanoError>, customDateRange: DateRange) {
		switch result {
		case .success(let dataList):
			dataModelForOxygenChart = []
			let totalOxygenSum = dataList.reduce(0) { (result: Double, nextItem: CoreDataOxygen) -> Double in
				return result + nextItem.value.doubleValue
			}
			
			let oxygenAvg = (dataList.count != 0) ? Int(totalOxygenSum)/dataList.count : 0
			
			let oxygenReadingElements = CoreDataOxygen.fetchOxygenReadingChartData(dateRange: customDateRange)
			let chartData = ActivityChartData(barColor: .barPurple, elements: oxygenReadingElements)
			let oxygenModel = ActivityChartModel(type: "spO2", value: NSNumber(value: oxygenAvg), chartData: chartData)
			
			dataModelForOxygenChart = [oxygenModel]
			
		case .failure(let error):
			print(error)
		}
        hud.dismiss()
	}
	
	func afterSleepDataResponseFetched(result: Result<[SleepDataModel], MovanoError>) {
		if currentSleepStartDate != nil && currentSleepEndDate != nil {
			let dateRange = DateRange(start: currentSleepStartDate!, end: currentSleepEndDate!, type: .daily)
			self.presenter?.fetchSleepStages(range: dateRange)
		} else {
            hud.dismiss()
			self.dayBarChartView.isHidden = true
			self.noChartDataDayUIView.isHidden = false
			self.pieChartForStages(rem: 0, deep: 0, light: 0)
		}
	}
	
	func sleepDataTiming(sleepStartTime: String?, sleepEndTime: String?) {
		if let sleepStartTimeInDouble = Double(sleepStartTime ?? "0"), let sleepEndTimeInDouble = Double(sleepEndTime ?? "0") {
            
            if(sleepStartTimeInDouble != 0) {
                let dateFormatter = DateFormatter()
                dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                dateFormatter.timeZone = .current
                let localStartDate = dateFormatter.string(from: Date(timeIntervalSince1970: sleepStartTimeInDouble))
                let localEndDate = dateFormatter.string(from: Date(timeIntervalSince1970: sleepEndTimeInDouble))
                
                   let dateRange = DateRange(start: Date(timeIntervalSince1970: sleepStartTimeInDouble), end: Date(timeIntervalSince1970: sleepEndTimeInDouble), type: .daily)
                
                self.presenter?.fetchSleepStages(range: dateRange)
            } else {
				self.dayBarChartView.isHidden = true
				self.noChartDataDayUIView.isHidden = false
				self.pieChartForStages(rem: 0, deep: 0, light: 0)
                hud.dismiss()
            }
//			let toTs = currentSleepEndDate?.timeIntervalSince1970 ?? 0
//			let fromTs = currentSleepStartDate?.timeIntervalSince1970 ?? 0
//			self.presenter?.fetchOxygenData(timeStampRange: Range(uncheckedBounds: (fromTs, toTs)), customDateRange: dateRange!)
			
		}
	}
	
	func fetchSleepStageSummaryForWeek(error: MovanoError) {
		noChatDataWeekUIView.isHidden = false
		hud.dismiss(animated: true)
	}
}
