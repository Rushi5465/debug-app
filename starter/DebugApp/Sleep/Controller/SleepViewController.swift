//
//  SleepViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 26/08/21.
//

import UIKit
import SwiftUI

// swiftlint:disable all
class SleepViewController: UIViewController {

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var bpView: SleepDashboardView!
	@IBOutlet weak var sleepStageView: SleepDashboardView!
	@IBOutlet weak var spO2View: UIView!
	@IBOutlet weak var heartRateView: UIView!
	@IBOutlet weak var temperatureView: UIView!
	@IBOutlet weak var breathingRateView: UIView!
	@IBOutlet weak var insightsView: UIView!
	@IBOutlet weak var averagePulseRate: MovanoLabel!
	@IBOutlet weak var saturatedOxygenLabel: MovanoLabel!
	@IBOutlet weak var temperatureLabel: MovanoLabel!
	@IBOutlet weak var breathingRateLabel: MovanoLabel!
	@IBOutlet weak var newInsightsView: UIView!
	
	@IBOutlet weak var dataCollectionView: UICollectionView!
	@IBOutlet weak var collectionHeight: NSLayoutConstraint!
	
    @IBOutlet weak var sleepDialView: UIView!
    @IBOutlet weak var wakeupTimeLable: MovanoLabel!
    
    @IBOutlet weak var bedTimeLable: MovanoLabel!
    var sleepDialData = SleepDialData(sleepStartTime: 0.0, sleepEndTime: 0.0, goalSleepTime:0.0 , awakeTimeList: [], totalSleep: "0h 0m")
	var presenter: SleepPresenter?
	var dataRangeType: DateRangeType = .daily
	var selectedDate: DateRange! {
		didSet {
                hud.show(in: self.parent?.parent?.view ?? self.view)
            
			let dateBefore = selectedDate.end.dateBefore(days: 1)
			//currentSleepStartDate = dateBefore.setTime(hour: 22, min: 0, sec: 0)
			//currentSleepEndDate = selectedDate.end.setTime(hour: 6, min: 0, sec: 0)
            sleepDialData = SleepDialData(sleepStartTime: 0.0, sleepEndTime: 0.0, goalSleepTime:0.0 , awakeTimeList: [], totalSleep: "0h 0m")
            sleepMetricIndicationModel = nil
			self.presenter?.fetchSleepTimeData(forDate: selectedDate.end)
			
		}
	}
	
	var currentSleepStartDate: Date?
	var currentSleepEndDate: Date?
    var sleepMetricIndicationModel:MetricIndicatorModel?
	
	var parameterList = [SleepParameter]() {
		didSet {
			self.dataCollectionView.reloadData()
		}
	}
	
	var avgSpO2: String?
	
	override func viewDidLoad() {
		super.viewDidLoad()

		self.registerPresenters()
		self.selectedDate = DateManager.current.selectedDate
		
		self.bpView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bpChartTapped(tap:))))
		self.bpView.isUserInteractionEnabled = true
		
		self.sleepStageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sleepStagesTapped(tap:))))
		self.sleepStageView.isUserInteractionEnabled = true
		
		self.newInsightsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(insightsViewTapped(tap:))))
		self.newInsightsView.isUserInteractionEnabled = true
		
		self.registerCollectionCell()
		self.assignDelegates()
		
		self.dataCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
		self.dataCollectionView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 5)
	}
	
	override func viewDidAppear(_ animated: Bool) {
//		self.presenter?.fetchParameters()
	}
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
        if(!self.selectedDate.start.isEqual(to: DateManager.current.selectedDate.start, toGranularity: .day)){
            self.selectedDate = DateManager.current.selectedDate
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.calendarDateSelected), name: NSNotification.Name("calendarDateSelected"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateDataAsPerGoalChange), name: NSNotification.Name("updateDataAsPerGoalChange"), object: nil)
		
		self.heartRateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(heartRateHistoryTapped(tap:))))
		self.heartRateView.isUserInteractionEnabled = true
		
		self.spO2View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(spO2HistoryTapped(tap:))))
		self.spO2View.isUserInteractionEnabled = true
		
		self.temperatureView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(temperatureHistoryTapped(tap:))))
		self.temperatureView.isUserInteractionEnabled = true
		
		self.breathingRateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(breathingRateHistoryTapped(tap:))))
		self.breathingRateView.isUserInteractionEnabled = true
		
//		if currentSleepEndDate != nil && currentSleepEndDate != nil {
//			let dateRange = DateRange(start: selectedDate.start, end: selectedDate.end, type: .daily)
//			self.presenter?.fetchData(range: dateRange)
//		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("calendarDateSelected"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("updateDataAsPerGoalChange"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("calendarDateSelected"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("updateDataAsPerGoalChange"), object: nil)
    }
	
	func registerPresenters() {
		if self.presenter == nil {
			let breathingRateService = BreathingRateWebService()
			let pulseRateService = PulseRateWebService()
			let temperatureService = SkinTemperatureWebService()
			let oxygenService = OxygenDataWebService()
			let sleepService = SleepWebService()
			let bloodPressureService = BloodPressureWebService()
			let hrvService = HRVWebService()
			self.presenter = SleepPresenter(sleepService: sleepService, pulseRateService: pulseRateService, oxygenService: oxygenService, temperatureService: temperatureService, breathingRateService: breathingRateService, bloodPressureService: bloodPressureService, hrvService: hrvService, delegate: self)
			
		}
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.collectionHeight.constant = self.dataCollectionView.contentSize.height + 20
	}
	
	func registerCollectionCell() {
		let parameterCell = UINib.init(nibName: "SleepParameterCollectionViewCell", bundle: nil)
		self.dataCollectionView.register(parameterCell, forCellWithReuseIdentifier: SleepParameterCollectionViewCell.IDENTIFIER)
	}
	
	func assignDelegates() {
		self.dataCollectionView.delegate = self
		self.dataCollectionView.dataSource = self
	}
	
	@objc func bpChartTapped(tap: UITapGestureRecognizer) {
        if currentSleepStartDate != nil {
        let controller = SleepStagesDetailsViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepStartDate = self.currentSleepStartDate
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.metricIndicatorModel = self.sleepMetricIndicationModel
        if parameterList.count > 0 {
            controller.avgHeartRate = parameterList[0].reading ?? "0"
        }
		parent?.navigationController?.pushViewController(controller, animated: true)
        }
	}
	
	@objc func sleepStagesTapped(tap: UITapGestureRecognizer) {
		let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
		controller.dataType = .bpReading
        parent?.navigationController?.pushViewController(controller, animated: true)
	}
	
	@objc func insightsViewTapped(tap: UITapGestureRecognizer) {
		if currentSleepStartDate != nil {
			let controller = InsightsDetailViewController.instantiate(fromAppStoryboard: .sleep)
			controller.currentSleepStartDate = self.currentSleepStartDate
			controller.currentSleepEndDate = self.currentSleepEndDate
			if parameterList.count > 0 {
				controller.avgHeartRate = parameterList[0].reading ?? "0"
				controller.avgSpO2 = parameterList[2].reading ?? "0"
			}
			parent?.navigationController?.pushViewController(controller, animated: true)
		}
	}

	@objc func heartRateHistoryTapped(tap: UITapGestureRecognizer) {
		let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
		controller.dataType = .heartRateReading
        parent?.navigationController?.pushViewController(controller, animated: true)
	}
	
	@objc func spO2HistoryTapped(tap: UITapGestureRecognizer) {
		let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
		controller.dataType = .spO2Reading
        parent?.navigationController?.pushViewController(controller, animated: true)
	}
	
	@objc func temperatureHistoryTapped(tap: UITapGestureRecognizer) {
		let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
		controller.dataType = .tempratureReading
        parent?.navigationController?.pushViewController(controller, animated: true)
	}
	
	@objc func breathingRateHistoryTapped(tap: UITapGestureRecognizer) {
		let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
		controller.dataType = .breathingRateReading
        parent?.navigationController?.pushViewController(controller, animated: true)
	}
    
    func addSwiftUICuastomView(){
        for view in self.sleepDialView.subviews{
            view.removeFromSuperview()
        }
        let childView = UIHostingController(rootView:  SleepDailView(sleepDialData: self.sleepDialData))
        childView.view.frame = self.sleepDialView.bounds
        childView.view.backgroundColor = UIColor.clear
        self.sleepDialView.addSubview(childView.view)
    }
	
	@objc private func updateDataAsPerGoalChange() {
		hud.show(in: self.parent?.parent?.view ?? self.view)
		sleepDialData = SleepDialData(sleepStartTime: 0.0, sleepEndTime: 0.0, goalSleepTime:0.0 , awakeTimeList: [], totalSleep: "0h 0m")
		sleepMetricIndicationModel = nil
		self.presenter?.fetchSleepTimeData(forDate: selectedDate.end)
	}
}

// MARK: - UICollectionViewDelegate
extension SleepViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: (collectionView.bounds.width / 2.0) - 9, height: 76)
	}
	
}

// MARK: - UICollectionViewDataSource
extension SleepViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.parameterList.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = SleepParameterCollectionViewCell.dequeueReusableCell(collectionView: collectionView, indexPath: indexPath)
		
		cell.layer.cornerRadius = 4.0
		cell.clipsToBounds = true
		cell.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
		
		let parameter = self.parameterList[indexPath.row]
		
		if let reading = parameter.reading {
			if reading != "-/-" {
				cell.readingLabel.attributedText = cell.readingData(value: reading, unit: parameter.unit, iconName: "")
			}
		} else {
			cell.readingLabel.text = "-"
		}
		if let metrcisObj = parameter.metrciModel{
			cell.imageIndicator.isHidden = false
			if metrcisObj.indicatorImage == .alertRed {
				cell.imageIndicator.image = UIImage(named: "SleepErrorRed")
			} else if metrcisObj.indicatorImage == .alertYellow {
				cell.imageIndicator.image = UIImage(named: "SleepErrorYellow")
			}
		}else{
            cell.imageIndicator.isHidden = true
        }
		cell.parameterLabel.text = parameter.name
		cell.paramaterIcon.image = UIImage(named: parameter.icon ?? "")
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if currentSleepStartDate != nil {
			let parameterName = self.parameterList[indexPath.row].name
			let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
			controller.currentSleepStartDate = self.currentSleepStartDate
			controller.currentSleepEndDate = self.currentSleepEndDate
			controller.avgValueForDay = self.parameterList[indexPath.row].reading
			if parameterName == "Blood Pressure" {
				controller.dataType = .bpReading
				controller.parameterList = parameterList
				controller.delegate = self
				parent?.navigationController?.pushViewController(controller, animated: true)
			} else if parameterName == "Heart Rate" {
				controller.dataType = .heartRateReading
				controller.parameterList = parameterList
				controller.delegate = self
				parent?.navigationController?.pushViewController(controller, animated: true)
			} else if parameterName == "Skin Temp Var." {
				controller.dataType = .tempratureReading
				controller.parameterList = parameterList
				controller.delegate = self
				parent?.navigationController?.pushViewController(controller, animated: true)
			} else if parameterName == "Breathing Rate" {
				controller.dataType = .breathingRateReading
				controller.parameterList = parameterList
				controller.delegate = self
				parent?.navigationController?.pushViewController(controller, animated: true)
			} else if parameterName == "Oxygen" {
				controller.dataType = .spO2Reading
				controller.parameterList = parameterList
				controller.delegate = self
				parent?.navigationController?.pushViewController(controller, animated: true)
			} else if parameterName == "Heart Rate Var." {
				controller.dataType = .hrvReading
				controller.parameterList = parameterList
				controller.delegate = self
				parent?.navigationController?.pushViewController(controller, animated: true)
			} else {
				
			}
		}
	}
}

// MARK: - SleepViewDelegate
extension SleepViewController: SleepViewDelegate {
    
	func bpChart(chartElements: [ChartElement], systolic: Int?, diastolic: Int?,metrciModel:MetricIndicatorModel?) {
		
		var value: NSNumber?
		if let systolic = systolic {
			value = NSNumber(value: systolic)
		} else {
			value = nil
		}
		
		if ((systolic != 0) && (systolic != nil) && (diastolic != 0) && (diastolic != nil)) {
			self.parameterList[4].reading = "\(systolic ?? 0)/\(diastolic ?? 0)"
		} else {
			self.parameterList[4].reading = "-/-"
		}
		
        self.parameterList[4].metrciModel = metrciModel
		let chartData = ActivityChartData(barColor: .barPurple, elements: chartElements)
		let systolicModel = ActivityChartModel(type: "Nocturnal Blood Pressure", value: value, goal: nil, unit: "mm Hg", chartData: chartData)
		let goal = UserDefault.shared.goals
		let systolic = goal.systolic.value as! Range<Int>
		let limitLineValues = [systolic.upperBound]
		sleepStageView.loadChartView(model: systolicModel, limitLineValues: limitLineValues)
	}
	
	func sleepStageChart(chartElements: [ChartElement], value: Int) {
        
		let chartData = ActivityChartData(barColor: .barPurple, elements: chartElements)
		let sleepStageModel = ActivityChartModel(type: "Sleep Stages", value: NSNumber(value: value), goal: nil, chartData: chartData)
		//bpView.loadChartView(model: sleepStageModel, isSleep: true)
        

	}
    
    func sleepStageDataFetched(result: Result<[CoreDataSleepStage],MovanoError>){
        switch result {
        case .success(let list):
            let achievedSleepMinutes = list.count * 5
            self.sleepDialData.totalSleep = Utility.convertminutesToHMFormat(achievedSleepMinutes)
            if let sleepHr = UserDefault.shared.goals.sleepHour.value as? Range<Int>{
                let goalSleepMinutes = sleepHr.upperBound * 60
                let goalSleepHrs =  Utility.convertminutesToHMFormat(goalSleepMinutes)
               // let valuesInSleepRange = sleepDataPoints.filter { $0?.sleepStage ?? 0 >= goalSleepMinutes }.count
                var sleepScore = 0.0
                if(achievedSleepMinutes >= goalSleepMinutes){
                    sleepScore = 10.0
                }else{
                    sleepScore = ((Double(achievedSleepMinutes)/Double(goalSleepMinutes)) * 100) / 10
                }
                
                if(sleepScore < 8){
                    let sleepStr = "You slept for \(Utility.convertminutesToHMFormat(achievedSleepMinutes)), your target sleep was \(goalSleepHrs)"
                    
                    self.sleepMetricIndicationModel = MetricIndicatorModel(indicatorImage: (sleepScore < 6 ? .alertRed : .alertYellow), metricType: .sleep, metricNegativeScore: sleepStr)
                }else{
                    self.sleepMetricIndicationModel = nil
                }
            }
            let achvSleep =  Utility.convertMinutesSleepDialTimeFormat(achievedSleepMinutes)
            // sleepDialData.sleepEndTime = CGFloat(sleepDialData.sleepStartTime + achvSleep) >= 13 ? CGFloat(sleepDialData.sleepStartTime + achvSleep - 12.0) : CGFloat(sleepDialData.sleepStartTime + achvSleep)
            sleepDialData.sleepEndTime = CGFloat(achvSleep)
            let awakeList = list.compactMap{ $0 }.filter{ $0.value == 4}
            var calendar = Calendar.current
            calendar.timeZone = TimeZone.current
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
            dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
            dateFormatter.timeZone = .current
            let fiveMinute = 5/60.0
            sleepDialData.awakeTimeList.removeAll()
            for eachItem in awakeList{
              print(dateFormatter.string(from: eachItem.date))
                let awakeTimeHour = CGFloat(calendar.component(.hour, from: eachItem.date))
                let awakeTimeMinute = Double(calendar.component(.minute, from: eachItem.date))/60
                let awakeTime = CGFloat((awakeTimeHour >= 13 ? awakeTimeHour - 12 : awakeTimeHour) + CGFloat(awakeTimeMinute))
                sleepDialData.awakeTimeList.append((awakeTime,awakeTime+CGFloat(fiveMinute)))
            }
    
        case .failure(let error):
            print("\(error)")
        }
       
        
        self.addSwiftUICuastomView()
        hud.dismiss()

    }
	
	func averageAttributeLabel(value: String, unit: String) -> NSAttributedString {
		let str = NSMutableAttributedString()
		
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.semiBold(withSize: 20), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textColor]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.regular(withSize: 12), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textColor]
		
		str.append(NSMutableAttributedString(string: value + " ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
		str.append(NSMutableAttributedString(string: unit, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		
		return str
		
	}
	
	func pulseRate(average value: Int?, unit: String,metrciModel:MetricIndicatorModel?) {
		guard let pulse = value else {
			self.averagePulseRate.text = "-"
			return
		}
        self.parameterList[MetricParamList.heartRate.rawValue].reading = "\(value ?? 0)"
        self.parameterList[MetricParamList.heartRate.rawValue].metrciModel = metrciModel
		self.averagePulseRate.attributedText = averageAttributeLabel(value: "\(pulse)", unit: unit)
	}
	
	func oxygenSaturation(average value: Int?, unit: String,metrciModel:MetricIndicatorModel?) {
		guard let oxygen = value else {
			self.saturatedOxygenLabel.text = "-"
			return
		}
        self.parameterList[MetricParamList.oxygenRate.rawValue].reading = "\(value ?? 0)"
        self.parameterList[MetricParamList.oxygenRate.rawValue].metrciModel = metrciModel
		self.saturatedOxygenLabel.attributedText = averageAttributeLabel(value: "\(oxygen)", unit: unit)
	}
	
	func skinTemperature(average value: Double?, unit: String, isPositive: Bool,metrciModel:MetricIndicatorModel?) {
		guard let temp = value else {
			self.temperatureLabel.text = "-"
			return
		}
        self.parameterList[MetricParamList.skinTemp.rawValue].reading = isPositive ? "+ \(value ?? 0)" : "- \(value ?? 0)"
        self.parameterList[MetricParamList.skinTemp.rawValue].metrciModel = metrciModel
		self.temperatureLabel.attributedText = (temp > 0) ? averageAttributeLabel(value: "+ \(temp)", unit: unit) : averageAttributeLabel(value: "\(temp)", unit: unit)
	}
	
	func breathingRate(average value: Int?, unit: String,metrciModel:MetricIndicatorModel?) {
		guard let breathingRate = value else {
			self.breathingRateLabel.text = "-"
			return
		}
        self.parameterList[MetricParamList.breathingRate.rawValue].reading = "\(value ?? 0)"
        self.parameterList[MetricParamList.breathingRate.rawValue].metrciModel = metrciModel
		self.breathingRateLabel.attributedText = averageAttributeLabel(value: "\(breathingRate)", unit: unit)
	}
	
	func sleepData(average value: Int?, unit: String) {
		guard let sleepValue = value else {
			self.sleepStageView.valueLabel.text = "-"
			return
		}
		self.parameterList[2].reading = "\(value ?? 0)"
		self.sleepStageView.valueLabel.attributedText = averageAttributeLabel(value: "\(sleepValue)", unit: unit)
	}
	
	func hrvRate(average value: Int?, unit: String,metrciModel:MetricIndicatorModel?) {
		guard let hrvRate = value else {
			self.breathingRateLabel.text = "-"
			return
		}
        self.parameterList[MetricParamList.hrvRate.rawValue].reading = "\(value ?? 0)"
		self.breathingRateLabel.attributedText = averageAttributeLabel(value: "\(hrvRate)", unit: unit)
	}
	
	func sleepDataTiming(sleepStartTime: String?, sleepEndTime: String?) {
		if let sleepStartTimeInDouble = Double(sleepStartTime ?? "0"), let sleepEndTimeInDouble = Double(sleepEndTime ?? "0") {
            
            if(sleepStartTimeInDouble != 0){
                let dateFormatter = DateFormatter()
                dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                dateFormatter.timeZone = .current
                var calendar = Calendar.current
                calendar.timeZone = TimeZone.current
                let startTimeHour = Double(calendar.component(.hour, from: Date(timeIntervalSince1970: sleepStartTimeInDouble)))
                let startTimeMinute = Double(calendar.component(.minute, from: Date(timeIntervalSince1970: sleepStartTimeInDouble)))/60
                sleepDialData.sleepStartTime = Double(startTimeHour >= 13 ? startTimeHour - 12 : startTimeHour) + startTimeMinute

                let bedTime = Date(timeIntervalSince1970: sleepStartTimeInDouble).dateToReadableTime()
                    
                bedTimeLable.text = bedTime
                let wakeupTime = Date(timeIntervalSince1970: sleepEndTimeInDouble).dateToReadableTime()
                wakeupTimeLable.text = wakeupTime
                //May required it later
               // let endTimeHour = Double(calendar.component(.hour, from: Date(timeIntervalSince1970: sleepEndTimeInDouble)))
                //let endTimeMinute = ((Double(calendar.component(.minute, from: Date(timeIntervalSince1970: sleepEndTimeInDouble))) * 100)/60)/10
                //sleepDialData.sleepEndTime = CGFloat((endTimeHour >= 13 ? endTimeHour - 12 : endTimeHour) + endTimeMinute)
                
                if let sleepHr = UserDefault.shared.goals.sleepHour.value as? Range<Int>{
                    let goalSleepHrs =  Utility.convertMinutesSleepDialTimeFormat(sleepHr.upperBound * 60)
                   // sleepDialData.goalSleepTime = CGFloat(sleepDialData.sleepStartTime + goalSleepHrs) >= 13 ? CGFloat(sleepDialData.sleepStartTime + goalSleepHrs - 12.0) : CGFloat(sleepDialData.sleepStartTime + goalSleepHrs)
                    sleepDialData.goalSleepTime = CGFloat(goalSleepHrs)
                }
                let dateRange = DateRange(start: Date(timeIntervalSince1970: sleepStartTimeInDouble), end: Date(timeIntervalSince1970: sleepEndTimeInDouble), type: .daily)
                currentSleepStartDate = Date(timeIntervalSince1970: sleepStartTimeInDouble)
                currentSleepEndDate = Date(timeIntervalSince1970: sleepEndTimeInDouble)
                self.presenter?.fetchData(range: dateRange)
            }else{
                bedTimeLable.text = "--"
                wakeupTimeLable.text = "--"
                sleepDialData.goalSleepTime = 0
                sleepDialData.sleepStartTime = 0
                sleepDialData.awakeTimeList = []
                sleepDialData.totalSleep = "0h 0m"
                addSwiftUICuastomView()
                currentSleepStartDate = nil
                currentSleepEndDate = nil
                self.presenter?.fetchParameters()
                hud.dismiss()
            }
		}
	}
  
	func afterSleepDataResponseFetched(result: Result<[SleepDataModel], MovanoError>) {
        bedTimeLable.text = "--"
        wakeupTimeLable.text = "--"
        sleepDialData.goalSleepTime = 0
        sleepDialData.sleepStartTime = 0
        sleepDialData.awakeTimeList = []
        sleepDialData.totalSleep = "0h 0m"
        self.sleepMetricIndicationModel = nil
        addSwiftUICuastomView()
        self.presenter?.fetchParameters()
        currentSleepStartDate = nil
        currentSleepEndDate = nil
        hud.dismiss()
//		if currentSleepStartDate != nil && currentSleepEndDate != nil {
//			let dateRange = DateRange(start: currentSleepStartDate!, end: currentSleepEndDate!, type: .daily)
//			self.presenter?.fetchData(range: dateRange)
//		}
	}
	
	func parameter(list: [SleepParameter]) {
		self.parameterList = list
	}
}

// MARK: - Calendar Callbacks
extension SleepViewController {
	
	@objc private func calendarDateSelected(notification: NSNotification) {
		guard let object = notification.object as? DateRange else {
			return
		}
        if(object.type == .daily){
            self.selectedDate = object

        }

	}
}

extension SleepViewController: SleepHistoryDataViewControllerProtocol {
	func setParameterData(parameter: [SleepParameter]) {
		self.parameterList = parameter
	}
}
