//
//  SleepHistoryDataViewController.swift
//  BloodPressureApp
//
//  Created by Rushikant on 01/09/21.
//

import UIKit

// swiftlint:disable all
class SleepHistoryDataViewController: UIViewController {
	
	@IBOutlet weak var headerView: HeaderView!
	@IBOutlet weak var barSegmentedControl: UISegmentedControl!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var tableHeight: NSLayoutConstraint!
	
	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var currentDateLabel: MovanoLabel!
	
	var dataModel = [ActivityChartModel]() {
		didSet {
			
		}
	}
	
	var presenter: SleepDetailPresenter?
	var dataType: DetailDataType!
	var dataRangeType: DateRangeType = .daily
	var parameterList: [SleepParameter]?
	var delegate: SleepHistoryDataViewControllerProtocol?
	var isChartDataAvailable = false
	var customDateRangeForTemp: DateRange?
	var avgValueForDay: String?
	
	var selectedDateRange: DateRange! {
		didSet {
            hud.show(in: self.view)
			let range = selectedDateRange.intervalRange
			let tempRange: DateRange
			
			if dataRangeType == .daily {
				let dateBefore = Date(timeIntervalSince1970: range.upperBound).dateBefore(days: 1)
				//currentSleepStartDate = dateBefore.setTime(hour: 22, min: 0, sec: 0)
				//currentSleepEndDate = Date(timeIntervalSince1970: range.upperBound).setTime(hour: 6, min: 0, sec: 0)
				if (currentSleepStartDate != nil && currentSleepEndDate != nil){
					let dateRange = DateRange(start: currentSleepStartDate!, end: currentSleepEndDate!, type: .daily)
					tempRange = dateRange
					self.presenter?.fetchSleepTimeData(forDate: selectedDateRange.end)
				} else {
					if dataType == .bpReading {
						dataModel = [ActivityChartModel(type: "Diastolic Blood Pressure", value: NSNumber(value: 0), chartData: ActivityChartData(barColor: .clear, elements: []))]
						dataModel.append(ActivityChartModel(type: "Systolic Blood Pressure", value: NSNumber(value: 0), chartData: ActivityChartData(barColor: .clear, elements: [])))
					} else if dataType == .breathingRateReading {
						dataModel = [ActivityChartModel(type: "Breathing Rate", value: NSNumber(value: 0), chartData: ActivityChartData(barColor: .clear, elements: []))]
					} else if dataType == .heartRateReading {
						dataModel = [ActivityChartModel(type: "Heart Rate", value: NSNumber(value: 0), chartData: ActivityChartData(barColor: .clear, elements: []))]
					} else if dataType == .spO2Reading {
						dataModel = [ActivityChartModel(type: "SpO2", value: NSNumber(value: 0), chartData: ActivityChartData(barColor: .clear, elements: []))]
					} else if dataType == .tempratureReading {
						dataModel = [ActivityChartModel(type: "Body Temperature", value: NSNumber(value: 0), chartData: ActivityChartData(barColor: .clear, elements: []))]
					} else if dataType == .hrvReading {
						dataModel = [ActivityChartModel(type: "HRV    ", value: NSNumber(value: 0), chartData: ActivityChartData(barColor: .clear, elements: []))]
					}
					tableView.reloadData()
				}
			} else {
				tempRange = selectedDateRange
			}
		}
	}
	
	var selectedDateRangeForDaily: DateRange!
	var currentSleepStartDate: Date?
	var currentSleepEndDate: Date?
	var selectedItemForCell = 0
    var metricModel : MetricIndicatorModel?

	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.headerView.currentController = self
		self.headerView.isInNavigation = true
		self.headerView.updateDate = false
		
		dataRangeType = .daily
		self.headerView.selectedDate = DateRange(DateManager.current.selectedDate.start, type: dataRangeType)
		
		barSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(named: "Background")!], for: .selected)
		barSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
		
		self.registerCellToListView()
		self.assignDelegate()
		
		self.registerPresenters()
		selectedDateRange = DateRange(DateManager.current.selectedDate.start, type: dataRangeType)
		self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.backBtntapped), name: NSNotification.Name("backBtntapped"), object: nil)
		
		currentDateLabel.text = headerView.selectedDate.end.dateToString(format: "d MMM, yyyy")
		
		if dataType == .bpReading {
			titleLabel.text = "Blood Pressure"
            metricModel = self.metricModel ?? parameterList?[MetricParamList.bp.rawValue].metrciModel
		} else if dataType == .breathingRateReading {
			titleLabel.text = "Breathing Rate"
            metricModel = self.metricModel ?? parameterList?[MetricParamList.breathingRate.rawValue].metrciModel
		} else if dataType == .heartRateReading {
			titleLabel.text = "Heart Rate"
            metricModel = self.metricModel ?? parameterList?[MetricParamList.heartRate.rawValue].metrciModel
		} else if dataType == .spO2Reading {
			titleLabel.text = "Oxygen"
            metricModel = self.metricModel ?? parameterList?[MetricParamList.oxygenRate.rawValue].metrciModel
		} else if dataType == .tempratureReading {
			titleLabel.text = "Skin Temp Variation"
            metricModel = self.metricModel ?? parameterList?[MetricParamList.skinTemp.rawValue].metrciModel
		} else if dataType == .hrvReading {
			titleLabel.text = "Heart Rate Variablity"
            metricModel = self.metricModel ?? parameterList?[MetricParamList.hrvRate.rawValue].metrciModel
		}
		
		// Custom header view
		headerView.leftArrow.isHidden = true
		headerView.rightArrow.isHidden = true
		headerView.calendarView.isHidden = true
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.calendarDateSelected), name: NSNotification.Name("calendarDateSelected"), object: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name("calendarDateSelected"), object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name("backBtntapped"), object: nil)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
//		self.tableHeight.constant = self.tableView.contentSize.height
	}
	
	func registerCellToListView() {
		let detailNib = UINib.init(nibName: "SleepDetailTableViewCell", bundle: nil)
		self.tableView.register(detailNib, forCellReuseIdentifier: SleepDetailTableViewCell.IDENTIFIER)
	}
	
	func assignDelegate() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let breathingRateService = BreathingRateWebService()
			let pulseRateService = PulseRateWebService()
			let temperatureService = SkinTemperatureWebService()
			let oxygenService = OxygenDataWebService()
			let bpService = BloodPressureWebService()
			let sleepService = SleepWebService()
			let hrvService = HRVWebService()
			self.presenter = SleepDetailPresenter(breathingRateService: breathingRateService, pulseRateService: pulseRateService, temperatureService: temperatureService, oxygenService: oxygenService, bpService: bpService, sleepService: sleepService, hrvService: hrvService, delegate: self)
		}
	}
	
	@IBAction func switchChartView(_ sender: UISegmentedControl) {
		if sender.selectedSegmentIndex == 0 {
			dataRangeType = .daily
		} else {
			dataRangeType = .weekly
		}
		
		selectedDateRange = DateRange(DateManager.current.selectedDate.start, type: dataRangeType)
		
		if selectedDateRangeForDaily != nil && selectedDateRangeForDaily.start != selectedDateRange.start && dataRangeType == .daily {
			selectedDateRange = selectedDateRangeForDaily
		}
		
		let tempRange: DateRange
		if dataRangeType == .daily {
			tempRange = DateRange(start: currentSleepStartDate!, end: currentSleepEndDate!, type: .daily)
		} else {
			tempRange = selectedDateRange
		}
		
		if dataType == .breathingRateReading {
			self.presenter?.fetchBreathingRateData(timeStampRange: selectedDateRange.intervalRange, customDateRange: tempRange)
		} else if dataType == .heartRateReading {
			self.presenter?.fetchPulseRateData(timeStampRange: selectedDateRange.intervalRange, customDateRange: tempRange)
		} else if dataType == .spO2Reading {
			self.presenter?.fetchOxygenData(timeStampRange: selectedDateRange.intervalRange, customDateRange: tempRange)
		} else if dataType == .tempratureReading {
			self.presenter?.fetchTemperatureData(timeStampRange: selectedDateRange.intervalRange, customDateRange: tempRange)
		} else if dataType == .hrvReading {
			self.presenter?.fetchHRVData(timeStampRange: selectedDateRange.intervalRange, customDateRange: tempRange)
		} else {
			self.presenter?.fetchBPReadingData(timeStampRange: selectedDateRange.intervalRange, customDateRange: tempRange)
		}
	}
	
	@objc private func backBtntapped() {
		self.delegate?.setParameterData(parameter: parameterList!)
		self.navigationController?.popViewController(animated: true)
	}
}

extension SleepHistoryDataViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 409.0
	}
}

extension SleepHistoryDataViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return (self.dataModel.count == 2) ? 1 : self.dataModel.count
	}
	
	func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
		return false
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = SleepDetailTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
		
		cell.customDateRangeForTemp = self.customDateRangeForTemp
		let goal = UserDefault.shared.goals
		var limitLineValues = [Int]()
//		let allElements = dataModel[indexPath.item].chartData.elements
		
		var allElements = [ChartElement]()
		for m in 0..<dataModel[indexPath.item].chartData.elements.count {
			if dataModel[indexPath.item].chartData.elements[m].value != nil && dataModel[indexPath.item].chartData.elements[m].value != 0 {
				allElements.append(dataModel[indexPath.item].chartData.elements[m])
			}
		}
		
		var avgTemp = 0.0
		var nonNilTempCount = 0
		for x in 0 ..< dataModel[indexPath.item].chartData.elements.count {
			if dataModel[indexPath.item].chartData.elements[x].value != 0.0 && dataModel[indexPath.item].chartData.elements[x].value != nil {
				avgTemp += Utility.getTempVariation(temperature: (dataModel[indexPath.item].chartData.elements[x].value ?? 0))
				nonNilTempCount += 1
			}
		}
		
		if nonNilTempCount > 0 {
			avgTemp = ((avgTemp/Double(nonNilTempCount)) * 10).rounded()/10
		}
		
		if allElements.count > 0 {
            if let metricMod = self.metricModel{
                cell.errorLabelHeight.constant = 12
                cell.descriptionHeight.constant = 52
				
				if metricMod.indicatorImage == .alertRed {
					cell.errorIndicatorImageView.image = UIImage.init(named: "ErrorOutlineRed")
				} else if metricMod.indicatorImage == .alertYellow {
					cell.errorIndicatorImageView.image = UIImage.init(named: "ErrorOutlineYellow")
				}
                
                cell.descriptionLabel.text = metricMod.metricNegativeScore
			} else {
				cell.errorLabelHeight.constant = 0
				cell.descriptionHeight.constant = 0
			}
			let maxValue = Int(allElements.reduce(allElements.last!) { aggregate, element in
				(aggregate.value ?? 0) < (element.value ?? 0) ? element : aggregate
			}.value ?? 0)
			
			let maxValueInouble = (allElements.reduce(allElements.last!) { aggregate, element in
				(aggregate.value ?? 0) < (element.value ?? 0) ? element : aggregate
			}.value ?? 0)
			
			let minValue = Int(allElements.reduce(allElements.last!) { aggregate, element in
				(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
			}.value ?? 0)
			
			var maxValueDiastolic = 0
			if dataType == .bpReading {
				let allNewElements = dataModel[1].chartData.elements
				maxValueDiastolic = Int(allNewElements.reduce(allNewElements.last!) { aggregate, element in
					(aggregate.value ?? 0) < (element.value ?? 0) ? element : aggregate
				}.value ?? 0)
			}
			
			// Attributed hint label
			let str = NSMutableAttributedString()
			let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.semiBold(withSize: 16), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textColor]
			let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.regular(withSize: 14), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.systemGray3]
			let attrs3 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.regular(withSize: 16), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.systemGray3]
			
			if dataType == .breathingRateReading {
				cell.titleLabel.text = "Breathing Rate"
				cell.unitLabel.text = "brpm"
				let breathingRateRange = goal.breathingRate.value as! Range<Int>
				limitLineValues = [breathingRateRange.lowerBound, breathingRateRange.upperBound]
				
				// Hint Label
				str.append(NSMutableAttributedString(string: "Peak: ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs3)))
				str.append(NSMutableAttributedString(string: "\(maxValue)" + " ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
				str.append(NSMutableAttributedString(string: "brpm", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
				cell.hintLabel.attributedText = str

			} else if dataType == .heartRateReading {
				cell.titleLabel.text = "Heart Rate"
				cell.unitLabel.text = "bpm"
				
				let pulseRateRange = goal.pulseRate.value as! Range<Int>
				limitLineValues = [pulseRateRange.lowerBound, pulseRateRange.upperBound]
				
				// Hint Label
				str.append(NSMutableAttributedString(string: "Low: ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs3)))
				str.append(NSMutableAttributedString(string: "\(minValue)" + " ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
				str.append(NSMutableAttributedString(string: "bpm", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
				cell.hintLabel.attributedText = str
				
			} else if dataType == .spO2Reading {
				cell.titleLabel.text = "Oxygen"
				cell.unitLabel.text = "%"
				let oxygenRange = goal.oxygen.value as! Range<Int>
				limitLineValues = [oxygenRange.lowerBound, oxygenRange.upperBound]
				
				// Hint Label
				str.append(NSMutableAttributedString(string: "Low: ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs3)))
				str.append(NSMutableAttributedString(string: "\(minValue)" + " ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
				str.append(NSMutableAttributedString(string: "%", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
				cell.hintLabel.attributedText = str
			} else if dataType == .tempratureReading {
				cell.titleLabel.text = "Skin Temp Variation"
				cell.unitLabel.text = "°f"
				let temperatureRange = goal.temperature.value as! Range<Int>
				limitLineValues = [temperatureRange.lowerBound, temperatureRange.upperBound]
				let differenceValue = Utility.getTempVariation(temperature: maxValueInouble)
				
				// Hint Label
				let hintText = (differenceValue > 0) ? "+\((differenceValue * 10).rounded()/10)" : "\((differenceValue * 10).rounded()/10)"
				str.append(NSMutableAttributedString(string: "Peak: ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs3)))
				str.append(NSMutableAttributedString(string: "\(hintText)" + " ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
				str.append(NSMutableAttributedString(string: "°f", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
				cell.hintLabel.attributedText = str
			} else if dataType == .bpReading {
				let sysRateRange = goal.systolic.value as! Range<Int>
				let diaRateRange = goal.diastolic.value as! Range<Int>
				if dataModel[indexPath.item].type == "Diastolic Blood Pressure" {
					cell.titleLabel.text = "Blood Pressure"
					limitLineValues = [diaRateRange.upperBound, sysRateRange.upperBound]
					
					// Hint Label
					str.append(NSMutableAttributedString(string: "Peak: ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs3)))
					str.append(NSMutableAttributedString(string: "\(maxValue)/\(maxValueDiastolic)" + " ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
					str.append(NSMutableAttributedString(string: "mm Hg", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
					cell.hintLabel.attributedText = str
				} else {
					cell.titleLabel.text = "Blood Pressure"
					cell.hintLabel.text = "Peak: \(maxValue)/\(maxValueDiastolic) mm Hg"
					limitLineValues = [diaRateRange.upperBound, sysRateRange.upperBound]
					
					// Hint Label
					str.append(NSMutableAttributedString(string: "Peak: ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs3)))
					str.append(NSMutableAttributedString(string: "\(maxValue)/\(maxValueDiastolic)" + " ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
					str.append(NSMutableAttributedString(string: "mm Hg", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
					cell.hintLabel.attributedText = str
				}
				cell.unitLabel.text = "mm hg"
			} else if dataType == .hrvReading {
				cell.titleLabel.text = "Heart Rate Variability"
				cell.unitLabel.text = "ms"
				cell.hintLabel.text = "Low: \(minValue) ms"
				let breathingRateRange = goal.breathingRate.value as! Range<Int>
				limitLineValues = [25, 50]
				
				// Hint Label
				str.append(NSMutableAttributedString(string: "Low: ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs3)))
				str.append(NSMutableAttributedString(string: "\(minValue)" + " ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
				str.append(NSMutableAttributedString(string: "ms", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
				cell.hintLabel.attributedText = str
			} else {
				let cell = UITableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
				return cell
			}
			cell.dataRangeType = dataRangeType
			cell.barSegmentedControl.tag = indexPath.item
			//		cell.barSegmentedControl.addTarget(self, action: #selector(loadSelectedChart(_:)), for: .allTouchEvents)
			cell.barSegmentedControl.addTarget(self, action: #selector(loadSelectedChart(_:)), for: .valueChanged)
			
			if isChartDataAvailable {
				cell.noChartDataUIview.isHidden = true
				if dataType == .bpReading {
					cell.loadChartView(model: dataModel[indexPath.item], limitLineValues: limitLineValues, chartDataForBP: dataModel)
				} else {
					cell.loadChartView(model: dataModel[indexPath.item], limitLineValues: limitLineValues)
				}
			} else {
				cell.loadChartView(model: ActivityChartModel(type: dataModel[indexPath.item].type, value: 0, goal: 0, unit: dataModel[indexPath.item].unit, chartData: ActivityChartData(barColor: .clear, elements: [])), limitLineValues: [])
				cell.valueLabel.text = "0"
				cell.hintLabel.text = ""
				cell.noChartDataUIview.isHidden = false
			}
			
		} else {
			if isChartDataAvailable {
				cell.noChartDataUIview.isHidden = true
				cell.loadChartView(model: dataModel[indexPath.item], limitLineValues: limitLineValues)
			} else {
				cell.loadChartView(model: ActivityChartModel(type: dataModel[indexPath.item].type, value: 0, goal: 0, unit: dataModel[indexPath.item].unit, chartData: ActivityChartData(barColor: .clear, elements: [])), limitLineValues: [])
				cell.valueLabel.text = "0"
				cell.hintLabel.text = ""
				cell.noChartDataUIview.isHidden = false
			}
		}
		cell.currentSleepStartDate = currentSleepStartDate
		cell.currentSleepEndDate = currentSleepEndDate
		if dataType == .tempratureReading && dataRangeType == .weekly && nonNilTempCount < 7 {
			if avgTemp == -0 {
				cell.valueLabel.text = "0.0"
			} else {
				cell.valueLabel.text = "\(avgTemp)"
			}
		}
		if ((avgValueForDay != nil) && (dataRangeType == .daily)){
			cell.valueLabel.text = avgValueForDay!
		}
		return cell
	}
	
	@objc func loadSelectedChart(_ sender: UISegmentedControl) {
		selectedItemForCell = sender.tag
		self.switchChartView(sender)
//		tableView.reloadData()
	}
}

extension SleepHistoryDataViewController: SleepDetailDelegate {
	func fetchPulseRateData(result: Result<[CoreDataPulseRate], MovanoError>, customDateRange: DateRange) {
		switch result {
		case .success(let dataList):
			dataModel = []
			let totalPulseSum = dataList.reduce(0) { (result: Double, nextItem: CoreDataPulseRate) -> Double in
				return result + nextItem.value.doubleValue
			}
			
			let pulseCountAvg = (dataList.count != 0) ? (totalPulseSum)/Double(dataList.count) : 0
			
			self.presenter?.fetchSleepDataForWeek(breathingRateAvg: pulseCountAvg, forRange: customDateRange.start.timeIntervalSince1970 ..< customDateRange.end.timeIntervalSince1970, type: customDateRange.type, dataType: .heartRateReading)
		case .failure(let error):
			print(error)
		}
	}
	
	func updatePulserateChart(pulseCountAvg: Double, customDateRange: DateRange, type: DateRangeType) {
		var pulseRateElements = [ChartElement(date: Date())]
		
		if customDateRange.type == .weekly {
			pulseRateElements = CoreDataSleepTime.fetchWeekAvgChartData(dateRange: customDateRange, dataType: .heartRateReading)
			let chartData = ActivityChartData(barColor: UIColor.barPurple, elements: pulseRateElements)
			let pulseRateModel = ActivityChartModel(type: "Heart Rate", value: NSNumber(value: Int((pulseCountAvg * 100).rounded() / 100)), chartData: chartData)
			
			dataModel = [pulseRateModel]
		} else {
			pulseRateElements = CoreDataPulseRate.fetchPulseRateChartData(dateRange: customDateRange)
			let  pulseCoreData = CoreDataSleepTime.fetch(for: selectedDateRange.dateRange)
			let pulseAvg = (pulseCoreData != nil) ? pulseCoreData[0].avgHR : pulseCountAvg
			let chartData = ActivityChartData(barColor: UIColor.barPurple, elements: pulseRateElements)
			let pulseRateModel = ActivityChartModel(type: "Heart Rate", value: NSNumber(value: Int(pulseAvg.rounded())), chartData: chartData)
			
			dataModel = [pulseRateModel]
		}
		tableView.reloadData()
		hud.dismiss(animated: true)
	}
		
	func fetchBreathingRateData(result: Result<[CoreDataBreathingRate], MovanoError>, customDateRange: DateRange) {
		switch result {
		case .success(let dataList):
			dataModel = []
			let totalBreathingSum = dataList.reduce(0) { (result: Double, nextItem: CoreDataBreathingRate) -> Double in
				return result + nextItem.value.doubleValue
			}
			
			let breathingRateAvg = (dataList.count != 0) ? (totalBreathingSum)/Double(dataList.count) : 0
			
			let dateList = Date.dates(from: customDateRange.start, to: customDateRange.end, interval: 1, component: .day)
			self.presenter?.fetchSleepDataForWeek(breathingRateAvg: breathingRateAvg, forRange: customDateRange.start.timeIntervalSince1970 ..< customDateRange.end.timeIntervalSince1970, type: customDateRange.type, dataType: .breathingRateReading)
		case .failure(let error):
			print(error)
		}
        
	}
	
	func updateBreathingRateChart(breathingRateAvg: Double, customDateRange: DateRange, type: DateRangeType) {
		var breathingRateElements = [ChartElement(date: Date())]
		
		if customDateRange.type == .weekly {
			breathingRateElements = CoreDataSleepTime.fetchWeekAvgChartData(dateRange: customDateRange, dataType: .breathingRateReading)
			let chartData = ActivityChartData(barColor: .barPurple, elements: breathingRateElements)
			let fetchBreathingRateModel = ActivityChartModel(type: "Breathing Rate", value: NSNumber(value: Int((breathingRateAvg * 100).rounded() / 100)), chartData: chartData)
			
			dataModel = [fetchBreathingRateModel]
		} else {
			breathingRateElements = CoreDataBreathingRate.fetchBreathingRateChartData(dateRange: customDateRange)
			let  breathingCoreData = CoreDataSleepTime.fetch(for: selectedDateRange.dateRange)
			let breathingAvg = (breathingCoreData != nil) ? breathingCoreData[0].avgBreathingRate : breathingRateAvg
			let chartData = ActivityChartData(barColor: UIColor.barPurple, elements: breathingRateElements)
			let fetchBreathingRateModel = ActivityChartModel(type: "Breathing Rate", value: NSNumber(value: Int(breathingAvg.rounded())), chartData: chartData)
			dataModel = [fetchBreathingRateModel]
		}
		
		tableView.reloadData()
		hud.dismiss(animated: true)
	}
	
	func fetchTemperatureData(result: Result<[CoreDataSkinTemperature], MovanoError>, customDateRange: DateRange) {
		switch result {
		case .success(let dataList):
			dataModel = []
//			let totalTempSum = dataList.reduce(0) { (result: Double, nextItem: CoreDataSkinTemperature) -> Double in
//				return result + nextItem.value.doubleValue
//			}
			
			var totalTempSum = dataList.reduce(0) { (result: Double, nextItem: CoreDataSkinTemperature) -> Double in
				return abs(result) + abs(Utility.getTempVariation(temperature: nextItem.value.doubleValue))
			}
			
			let avgVariation = Double(totalTempSum) / Double((dataList.count))
			let tempAvg = (dataList.count != 0) ? (((avgVariation * 10).rounded()) / 10) : 0
			
			self.presenter?.fetchSleepDataForWeek(breathingRateAvg: tempAvg, forRange: customDateRange.start.timeIntervalSince1970 ..< customDateRange.end.timeIntervalSince1970, type: customDateRange.type, dataType: .tempratureReading)
		case .failure(let error):
			print(error)
		}
	}
	
	func updateTempChart(tempAvg: Double, customDateRange: DateRange, type: DateRangeType) {
		var tempReadingElements = [ChartElement(date: Date())]
		
		if customDateRange.type == .weekly {
			tempReadingElements = CoreDataSleepTime.fetchWeekAvgChartData(dateRange: customDateRange, dataType: .tempratureReading)
			let chartData = ActivityChartData(barColor: .barPurple, elements: tempReadingElements)
			let pulseRateModel = ActivityChartModel(type: "Body Temperature", value: NSNumber(value: ((tempAvg * 10).rounded() / 10)), chartData: chartData)
			
			dataModel = [pulseRateModel]
		} else {
			tempReadingElements = CoreDataSkinTemperature.fetchSkinTemperatureChartData(dateRange: customDateRange)
			let  tempCoreData = CoreDataSleepTime.fetch(for: selectedDateRange.dateRange)[0].avgSkinTempVar
			let tempData = Utility.getTempVariation(temperature: tempCoreData)
			let tempDataAvg = (tempCoreData != nil) ? ((tempData * 10).rounded() / 10) : ((tempAvg * 10).rounded() / 10)
			let chartData = ActivityChartData(barColor: .barPurple, elements: tempReadingElements)
			let pulseRateModel = ActivityChartModel(type: "Body Temperature", value: NSNumber(value: tempAvg), chartData: chartData)
			customDateRangeForTemp = customDateRange
			dataModel = [pulseRateModel]
		}
		tableView.reloadData()
		hud.dismiss(animated: true)
	}
		
	func fetchOxygenData(result: Result<[CoreDataOxygen], MovanoError>, customDateRange: DateRange) {
		switch result {
		case .success(let dataList):
			dataModel = []
			let totalOxygenSum = dataList.reduce(0) { (result: Double, nextItem: CoreDataOxygen) -> Double in
				return result + nextItem.value.doubleValue
			}
			
			let oxygenAvg = (dataList.count != 0) ? (totalOxygenSum)/Double(dataList.count) : 0
			
			self.presenter?.fetchSleepDataForWeek(breathingRateAvg: oxygenAvg, forRange: customDateRange.start.timeIntervalSince1970 ..< customDateRange.end.timeIntervalSince1970, type: customDateRange.type, dataType: .spO2Reading)
		case .failure(let error):
			print(error)
		}
	}
	
	func updateOxygenChart(oxygenAvg: Double, customDateRange: DateRange, type: DateRangeType) {
		var oxygenReadingElements = [ChartElement(date: Date())]
		
		if customDateRange.type == .weekly {
			oxygenReadingElements = CoreDataSleepTime.fetchWeekAvgChartData(dateRange: customDateRange, dataType: .spO2Reading)
			let chartData = ActivityChartData(barColor: .barPurple, elements: oxygenReadingElements)
			let oxygenModel = ActivityChartModel(type: "SpO2", value: NSNumber(value: Int(oxygenAvg.rounded())), chartData: chartData)
			
			dataModel = [oxygenModel]
		} else {
			oxygenReadingElements = CoreDataOxygen.fetchOxygenReadingChartData(dateRange: customDateRange)
			let  oxygenCoreData = CoreDataSleepTime.fetch(for: selectedDateRange.dateRange)
			let spO2Avg = (oxygenCoreData != nil) ? oxygenCoreData[0].avgOxygen : oxygenAvg
			let chartData = ActivityChartData(barColor: .barPurple, elements: oxygenReadingElements)
			let oxygenModel = ActivityChartModel(type: "SpO2", value: NSNumber(value: Int(spO2Avg.rounded())), chartData: chartData)
			
			dataModel = [oxygenModel]
		}
		
		tableView.reloadData()
		hud.dismiss(animated: true)
	}
		
	func fetchBPData(result: Result<[CoreDataBPReading], MovanoError>, customDateRange: DateRange) {
		switch result {
		case .success(let dataList):
			dataModel = []
			let totalBPDiastolic = dataList.reduce(0) { (result: Double, nextItem: CoreDataBPReading) -> Double in
				return result + Double(truncating: nextItem.diastolic)
			}
			
			let totalBPSystolic = dataList.reduce(0) { (result: Double, nextItem: CoreDataBPReading) -> Double in
				return result + Double(truncating: nextItem.systolic)
			}
			
			var bpDiastolicAvg = 0.0
			var bpSystolicAvg = 0.0
			if dataList.count != 0 {
				bpDiastolicAvg = (totalBPDiastolic)/Double(dataList.count)
				bpSystolicAvg = (totalBPSystolic)/Double(dataList.count)
			}
			
			self.presenter?.fetchSleepDataForWeek(breathingRateAvg: 0.0, bpDiastolicAvg: bpDiastolicAvg, bpSystolicAvg: bpSystolicAvg, forRange: customDateRange.start.timeIntervalSince1970 ..< customDateRange.end.timeIntervalSince1970, type: customDateRange.type, dataType: .bpReading)

		case .failure(let error):
			print(error)
		}
	}
	
	func updateBPChart(bpDiastolicAvg: Double, bpSystolicAvg: Double, customDateRange: DateRange, type: DateRangeType) {
		if customDateRange.type == .weekly {
			let diastolicElements = CoreDataSleepTime.fetchWeekAvgChartData(dateRange: customDateRange, dataType: .bpReading, isSys: false)
			let chartDataDiastolic = ActivityChartData(barColor: .barPurple, elements: diastolicElements)
			let systolicElements = CoreDataSleepTime.fetchWeekAvgChartData(dateRange: customDateRange, dataType: .bpReading)
			let chartDataSystolic = ActivityChartData(barColor: .barPurple, elements: systolicElements)
			
			let bpDiastolicModel = ActivityChartModel(type: "Diastolic Blood Pressure", value: NSNumber(value: Int(bpDiastolicAvg.rounded())), chartData: chartDataDiastolic)
			let bpSystolicModel = ActivityChartModel(type: "Systolic Blood Pressure", value: NSNumber(value: Int(bpSystolicAvg.rounded())), chartData: chartDataSystolic)
			
			dataModel = [bpSystolicModel]
			dataModel.append(bpDiastolicModel)
			
		} else {
			let bPReadingElements = CoreDataBPReading.fetchBloodPressureChartData(dateRange: customDateRange)
			Int((bpDiastolicAvg * 100).rounded() / 100)
			
			// Diastolic
			let chartDataDiastolic = ActivityChartData(barColor: .barPurple, elements: bPReadingElements.0)
			let bpDiastolicModel = ActivityChartModel(type: "Diastolic Blood Pressure", value: NSNumber(value: Int(bpDiastolicAvg.rounded())), chartData: chartDataDiastolic)
			
			// Systolic
			let chartDataSystolic = ActivityChartData(barColor: .barPurple, elements: bPReadingElements.1)
			let bpSystolicModel = ActivityChartModel(type: "Systolic Blood Pressure", value: NSNumber(value: Int(bpSystolicAvg.rounded())), chartData: chartDataSystolic)
			
			dataModel = [bpSystolicModel]
			dataModel.append(bpDiastolicModel)
		}
		
		tableView.reloadData()
		hud.dismiss()
		
	}
	
	func fetchHRVData(result: Result<[CoreDataHRV], MovanoError>, customDateRange: DateRange) {
		switch result {
		case .success(let dataList):
			dataModel = []
			let totalHRVSum = dataList.reduce(0) { (result: Double, nextItem: CoreDataHRV) -> Double in
				return result + nextItem.value.doubleValue
			}
			
			let hrvAvg = (dataList.count != 0) ? (totalHRVSum)/Double(dataList.count) : 0
			
			self.presenter?.fetchSleepDataForWeek(breathingRateAvg: hrvAvg, forRange: customDateRange.start.timeIntervalSince1970 ..< customDateRange.end.timeIntervalSince1970, type: customDateRange.type, dataType: .hrvReading)
		case .failure(let error):
			print(error)
		}
	}
	
	func updateHRVChart(hrvAvg: Double, customDateRange: DateRange, type: DateRangeType) {
		var hrvElements = [ChartElement(date: Date())]
		
		if customDateRange.type == .weekly {
			hrvElements = CoreDataSleepTime.fetchWeekAvgChartData(dateRange: customDateRange, dataType: .hrvReading)
			let chartData = ActivityChartData(barColor: .barPurple, elements: hrvElements)
			let fetchHRVModel = ActivityChartModel(type: "HRV    ", value: NSNumber(value: Int((hrvAvg * 100).rounded() / 100)), chartData: chartData)
			
			dataModel = [fetchHRVModel]
		} else {
			hrvElements = CoreDataHRV.fetchHRVChartData(dateRange: customDateRange)
			let  hrvCoreData = CoreDataSleepTime.fetch(for: selectedDateRange.dateRange)
			let hrvDataAvg = (hrvCoreData != nil) ? hrvCoreData[0].avgHRV : hrvAvg
			let chartData = ActivityChartData(barColor: .barPurple, elements: hrvElements)
			let fetchHRVModel = ActivityChartModel(type: "HRV    ", value: NSNumber(value: Int(hrvDataAvg.rounded())), chartData: chartData)
			
			dataModel = [fetchHRVModel]
		}
		tableView.reloadData()
		hud.dismiss(animated: true)
	}
		
	func sleepDataTiming(sleepStartTime: String?, sleepEndTime: String?) {
		if let sleepStartTimeInDouble = Double(sleepStartTime ?? "0"), let sleepEndTimeInDouble = Double(sleepEndTime ?? "0") {
			let dateFormatter = DateFormatter()
			dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
			dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
			dateFormatter.timeZone = .current
			let localStartDate = dateFormatter.string(from: Date(timeIntervalSince1970: sleepStartTimeInDouble))
			let localEndDate = dateFormatter.string(from: Date(timeIntervalSince1970: sleepEndTimeInDouble))
			let dateRange: DateRange?
			if sleepStartTime == nil {
				dateRange = DateRange(start: currentSleepStartDate!, end: currentSleepEndDate!, type: .daily)
			} else {
				dateRange = DateRange(start: Date(timeIntervalSince1970: sleepStartTimeInDouble), end: Date(timeIntervalSince1970: sleepEndTimeInDouble), type: .daily)
			}
			
			if dataType == .breathingRateReading {
				self.presenter?.fetchBreathingRateData(timeStampRange: dateRange!.intervalRange, customDateRange: dateRange!)
			} else if dataType == .heartRateReading {
				self.presenter?.fetchPulseRateData(timeStampRange: dateRange!.intervalRange, customDateRange: dateRange!)
			} else if dataType == .spO2Reading {
				self.presenter?.fetchOxygenData(timeStampRange: dateRange!.intervalRange, customDateRange: dateRange!)
			} else if dataType == .tempratureReading {
				self.presenter?.fetchTemperatureData(timeStampRange: dateRange!.intervalRange, customDateRange: dateRange!)
			} else if dataType == .hrvReading {
				self.presenter?.fetchHRVData(timeStampRange: selectedDateRange.intervalRange, customDateRange: dateRange!)
			} else {
				self.presenter?.fetchBPReadingData(timeStampRange: dateRange!.intervalRange, customDateRange: dateRange!)
			}
		}
	}
	
	func afterSleepDataResponseFetched(result: Result<[SleepDataModel], MovanoError>) {
		if dataRangeType == .daily {
			let dateRange = DateRange(start: currentSleepStartDate!, end: currentSleepEndDate!, type: .daily)
			if dataType == .breathingRateReading {
				self.presenter?.fetchBreathingRateData(timeStampRange: dateRange.intervalRange, customDateRange: dateRange)
			} else if dataType == .heartRateReading {
				self.presenter?.fetchPulseRateData(timeStampRange: dateRange.intervalRange, customDateRange: dateRange)
			} else if dataType == .spO2Reading {
				self.presenter?.fetchOxygenData(timeStampRange: dateRange.intervalRange, customDateRange: dateRange)
			} else if dataType == .tempratureReading {
				self.presenter?.fetchTemperatureData(timeStampRange: dateRange.intervalRange, customDateRange: dateRange)
			} else if dataType == .hrvReading {
				self.presenter?.fetchHRVData(timeStampRange: selectedDateRange.intervalRange, customDateRange: dateRange)
			} else {
				self.presenter?.fetchBPReadingData(timeStampRange: dateRange.intervalRange, customDateRange: dateRange)
			}
		}
	}
	
	func errorReceivedWhileChartData(isChartDataAvailable: Bool = false, dataType: DetailDataType!) {
		self.isChartDataAvailable = isChartDataAvailable
		var type = ""
		var unit = ""
		if dataType == .breathingRateReading {
			type = "Breathing Rate"
			unit = "brpm"
		} else if dataType == .heartRateReading {
			type = "Heart Rate"
			unit = "bpm"
		} else if dataType == .spO2Reading {
			type = "Oxygen"
			unit = "%"
		} else if dataType == .tempratureReading {
			type = "Skin Temp Variation"
			unit = "°f"
		} else if dataType == .bpReading {
			type = "Blood Pressure"
			unit = "mm hg"
		} else if dataType == .hrvReading {
			type = "Heart Rate Variability"
			unit = "ms"
		}
		if !isChartDataAvailable {
			self.dataModel = [ActivityChartModel(type: type, chartData: ActivityChartData(barColor: .clear, elements: []))]
			self.tableView.reloadData()
		}
	}
}

// MARK: - Calendar Callbacks
extension SleepHistoryDataViewController {
	
	@objc private func calendarDateSelected(notification: NSNotification) {
		guard let object = notification.object as? DateRange else {
			return
		}
		
		self.selectedDateRange = object
		selectedDateRangeForDaily = object
		
		selectedDateRange = DateRange(object.start, type: dataRangeType)
		
		let tempRange: DateRange
		if dataRangeType == .daily {
			let dateBefore = selectedDateRange.end.dateBefore(days: 1)
			let dateBeforeWithTimeComp = dateBefore.setTime(hour: 22, min: 0, sec: 0)
			let currentSelectedDate = selectedDateRange.end.setTime(hour: 6, min: 0, sec: 0)
			let dateRange = DateRange(start: dateBeforeWithTimeComp!, end: currentSelectedDate!, type: .daily)
			tempRange = dateRange
		} else {
			tempRange = selectedDateRange
		}
		
		if dataType == .breathingRateReading {
			self.presenter?.fetchBreathingRateData(timeStampRange: tempRange.intervalRange, customDateRange: tempRange)
		} else if dataType == .heartRateReading {
			self.presenter?.fetchPulseRateData(timeStampRange: tempRange.intervalRange, customDateRange: tempRange)
		} else if dataType == .spO2Reading {
			self.presenter?.fetchOxygenData(timeStampRange: tempRange.intervalRange, customDateRange: tempRange)
		} else if dataType == .tempratureReading {
			self.presenter?.fetchTemperatureData(timeStampRange: tempRange.intervalRange, customDateRange: tempRange)
		} else if dataType == .hrvReading {
			self.presenter?.fetchHRVData(timeStampRange: selectedDateRange.intervalRange, customDateRange: tempRange)
		} else {
			self.presenter?.fetchBPReadingData(timeStampRange: tempRange.intervalRange, customDateRange: tempRange)
		}
	}
}
