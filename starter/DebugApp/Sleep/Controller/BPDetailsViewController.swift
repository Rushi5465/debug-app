//
//  BPDetailsViewController.swift
//  BloodPressureApp
//
//  Created by Rushikant on 30/08/21.
//

import UIKit

class BPDetailsViewController: UIViewController {
	
	@IBOutlet weak var headerView: HeaderView!
	@IBOutlet weak var barSegmentedControl: UISegmentedControl!
	@IBOutlet weak var bpChartTableView: UITableView!
	
	let titleextAttributes = [
		NSAttributedString.Key.strokeColor: UIColor.black,
		   NSAttributedString.Key.font: UIFont(name: "OpenSans-Semibold", size: 16)!,
		   NSAttributedString.Key.foregroundColor: UIColor.white
		   ] as [NSAttributedString.Key: Any]
	
	let subtitleextAttributes = [
		NSAttributedString.Key.strokeColor: UIColor.black,
		   NSAttributedString.Key.font: UIFont(name: "OpenSans-Semibold", size: 14)!,
		   NSAttributedString.Key.foregroundColor: UIColor.white
		   ] as [NSAttributedString.Key: Any]
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.headerView.currentController = self
		self.headerView.isInNavigation = true
		self.headerView.updateDate = false
		
		barSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(named: "Background")!], for: .selected)
		barSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
		
//		self.registerPresenters()
		self.registerCellToListView()
		self.assignDelegate()
	}
	
	@IBAction func switchView(_ sender: UISegmentedControl) {
		if sender.selectedSegmentIndex == 0 {
		} else {
		}
	}
	
	func assignDelegate() {
		bpChartTableView.delegate = self
		bpChartTableView.dataSource = self
	}
	
	func registerCellToListView() {
		let menuNib = UINib.init(nibName: "SleepDashboardTableViewCell", bundle: nil)
		self.bpChartTableView.register(menuNib, forCellReuseIdentifier: SleepDashboardTableViewCell.IDENTIFIER)
	}
}

extension BPDetailsViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 300
	}
}

extension BPDetailsViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 2
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = SleepDashboardTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
		cell.tagButton.isHidden = false
		if indexPath.row == 0 {
			cell.titleLabel.attributedText = NSAttributedString(string: "Systolic blood pressure", attributes: titleextAttributes)
			cell.valueLabel.attributedText = NSAttributedString(string: "120", attributes: subtitleextAttributes)
			cell.unitLabel.attributedText = NSAttributedString(string: "mm hg", attributes: subtitleextAttributes)
			cell.fetchActivityChart()
		} else {
			cell.titleLabel.attributedText = NSAttributedString(string: "Diastolic blood pressure", attributes: titleextAttributes)
			cell.valueLabel.attributedText = NSAttributedString(string: "80", attributes: subtitleextAttributes)
			cell.unitLabel.attributedText = NSAttributedString(string: "mm hg", attributes: subtitleextAttributes)
			cell.fetchActivityChart()
		}
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
	}
}
