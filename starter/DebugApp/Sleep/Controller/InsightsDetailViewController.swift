//
//  InsightsDetailViewController.swift
//  BloodPressureApp
//
//  Created by Rushikant on 22/09/21.
//

import UIKit

// swiftlint:disable all
class InsightsDetailViewController: UIViewController {
		
	@IBOutlet weak var segmentView: CustomSegmentView!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var headerView: HeaderView!
	@IBOutlet weak var tableHeight: NSLayoutConstraint!
	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var circleForFirstLabel: MovanoLabel!
	@IBOutlet weak var circleForSecondLabel: MovanoLabel!
	@IBOutlet weak var firstLabel: MovanoLabel!
	@IBOutlet weak var secondLabel: MovanoLabel!
	
	let paginationTabs = ["", "Cardiac", "Pulmonary", "Insights", ""]
	
	var presenter: SleepInsightPresenter?
	
	var list = Insight() {
		didSet {
			setSummaryPoint(list: list)
			tableView.reloadData()
		}
	}
	
	var selectedDate: DateRange! {
		didSet {
//			self.fetchData()
		}
	}
	
	var currentSleepStartDate: Date?
	var currentSleepEndDate: Date?
	var avgHeartRate: String?
	var avgSpO2: String?
	var timeForMinHR: Date?
	var timeForMinSpO2: Date?
	var minHR: Double?
	var minSPO2: Double?
	var sleepAtMinHR: Double?
	var sleepAtMinSpO2: Double?
	
	let cellSpacingHeight: CGFloat = 1
	
	override func viewDidLoad() {
		self.segmentView.delegate = self
		self.segmentView.segmentList = ["Cardiac", "Pulmonary"]
		
		self.headerView.currentController = self
		self.headerView.isInNavigation = true
		self.headerView.updateDate = false
		
		// Custom header view
		headerView.leftArrow.isHidden = true
		headerView.rightArrow.isHidden = true
		headerView.calendarView.isHidden = true
		
		self.assignDelegate()
		self.registerPresenters()
		self.registerCellToListView()
		
		circleForFirstLabel.clipsToBounds = true
		circleForFirstLabel.layer.cornerRadius = 4.0
		circleForFirstLabel.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		circleForSecondLabel.clipsToBounds = true
		circleForSecondLabel.layer.cornerRadius = 4.0
		circleForSecondLabel.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.calendarDateSelected), name: NSNotification.Name("calendarDateSelected"), object: nil)
		
		self.selectedDate = DateManager.current.selectedDate
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name("calendarDateSelected"), object: nil)
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let breathingRateService = BreathingRateWebService()
			let pulseRateService = PulseRateWebService()
			let sleepService = SleepStageDetailWebService()
			let oxygenService = OxygenDataWebService()
			let hrvService = HRVWebService()
			self.presenter = SleepInsightPresenter(breathingRateService: breathingRateService, oxygenService: oxygenService, pulseRateService: pulseRateService, sleepService: sleepService, hrvService: hrvService, delegate: self)
		}
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.tableHeight.constant = self.tableView.contentSize.height
	}
	
	func registerCellToListView() {
		let detailNib = UINib.init(nibName: "InsightDetailTableViewCell", bundle: nil)
		self.tableView.register(detailNib, forCellReuseIdentifier: InsightDetailTableViewCell.IDENTIFIER)
	}
	
	func assignDelegate() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
	}
	
	func fetchData() {
		if currentSleepStartDate != nil && currentSleepEndDate != nil {
			let range = DateRange(start: currentSleepStartDate!, end: currentSleepEndDate!, type: .daily)
			if self.segmentView.selectedSegment == 0 {
				self.presenter?.fetchCardiacData(range: range)
			} else if self.segmentView.selectedSegment == 1 {
				self.presenter?.fetchPulmonaryData(range: range)
			}
		} else {
			if self.segmentView.selectedSegment == 0 {
				self.presenter?.isCardiacData = true
				self.presenter?.initialiseCardiacModel()
			} else if self.segmentView.selectedSegment == 1 {
				self.presenter?.isCardiacData = false
				self.presenter?.initializePulmonaryModel()
			}
		}
	}
	
	func setSummaryPoint(list: [InsightModel]) {
		for x in 0..<list.count {
			if list[x].chartData.elements.count > x {
				if list[x].parameter == "Heart Rate" {
					self.timeForMinHR = (list[x].chartData.elements.reduce(list[x].chartData.elements.last!) { aggregate, element in
						(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
					}.date)
					
					self.minHR = (list[x].chartData.elements.reduce(list[x].chartData.elements.last!) { aggregate, element in
						(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
					}.value)
				} else if list[x].parameter == "SpO2" {
					self.timeForMinSpO2 = (list[x].chartData.elements.reduce(list[x].chartData.elements.last!) { aggregate, element in
						(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
					}.date)
					self.minSPO2 = (list[x].chartData.elements.reduce(list[x].chartData.elements.last!) { aggregate, element in
						(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
					}.value)
				}
			}
		}
		
		for x in 0..<list.count {
			if list[x].parameter == "Sleep Stages" {
				let data = list[x].chartData.elements
				for z in 0..<data.count {
					if data[z].date == timeForMinHR {
						self.sleepAtMinHR = data[z].value
					}
					if data[z].date == timeForMinSpO2 {
						self.sleepAtMinSpO2 = data[z].value
					}
				}
			}
		}
		
		let sleepArray = ["", "DEEP", "LIGHT", "REM", "AWAKE"]
		let sleepatMinHr =  Int((self.sleepAtMinHR ?? 0))
		let sleepatMinSpo2 = Int((self.sleepAtMinSpO2 ?? 0))
		if self.segmentView.selectedSegment == 0 {
			firstLabel.text = "Your heart rate was the lowest in \(sleepArray[sleepatMinHr]) stage"
			secondLabel.text = "The average heart rate during sleep was \(self.avgHeartRate ?? "0") bpm"
		} else {
			firstLabel.text = "Your lowest SPO2 (\(Int(self.minSPO2 ?? 0))%)was in \(sleepArray[sleepatMinSpo2]) stage"
			secondLabel.text = "The average SPO2 during sleep was \(self.avgSpO2 ?? "0")%"
		}
	}
	
}

extension InsightsDetailViewController: InsightViewDelegate {
	func fetchInsightList(list: [InsightModel]) {
		self.list = list
	}
	
	func fetchSleepStageSummary(chartElements: [ChartElement], value: Int) {
		
	}
	
}

extension InsightsDetailViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 216
	}
	
	func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
		return 216
	}
}

extension InsightsDetailViewController: UITableViewDataSource {
	func numberOfSections(in tableView: UITableView) -> Int {
		return self.list.count
	}
	
	func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
		return false
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}
	
//	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//		return cellSpacingHeight
//	}
//
//	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//		return 1
//	}
	
//	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//		let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 2))
//		headerView.backgroundColor = UIColor.black
//		return headerView
//	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = InsightDetailTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
		let item = self.list[indexPath.section]
		
		cell.chartTitleLabel.text = item.parameter
//		cell.titleLabelHeightConstraint.constant = 0
		if currentSleepEndDate == nil && currentSleepStartDate == nil {
			cell.titleLabelHeightConstraint.constant = 0
		}
		
		if item.parameter == "Sleep Stages" {
			cell.loadDayChartForSleep(chartData: item.chartData)
		} else {
			cell.fetchChart(chartData: item.chartData, parameter: item.parameter)
		}
		
		cell.currentSleepEndDate = self.currentSleepEndDate
		cell.currentSleepStartDate = self.currentSleepStartDate
		return cell
	}
}

extension InsightsDetailViewController: CustomSegmentViewDelegate {
	
	func segmentChanged(to indexpath: IndexPath) {
        hud.show(in: self.view)
		self.titleLabel.text = (indexpath.item == 0) ? "SLEEP vs HR vs HRV" : "SLEEP vs SPO2 vs BR"
		self.fetchData()
	}
}

// MARK: - Calendar Callbacks
extension InsightsDetailViewController {
	
	@objc private func calendarDateSelected(notification: NSNotification) {
		guard let object = notification.object as? DateRange else {
			return
		}
		
		self.selectedDate = object
	}
}
