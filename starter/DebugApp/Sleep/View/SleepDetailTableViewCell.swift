//
//  SleepDetailTableViewCell.swift
//  BloodPressureApp
//
//  Created by Rushikant on 01/09/21.
//

import UIKit
import Highcharts
import Apollo

// swiftlint:disable all
class SleepDetailTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var valueLabel: MovanoLabel!
	@IBOutlet weak var subTitleLabel: MovanoLabel!
	@IBOutlet weak var chartView: HIChartView!
	@IBOutlet weak var unitLabel: MovanoLabel!
	@IBOutlet weak var tagButton: MovanoButton!
	
	@IBOutlet weak var errorIndicatorImageView: UIImageView!
	@IBOutlet weak var hintLabel: MovanoLabel!
	@IBOutlet weak var descriptionLabel: MovanoLabel!
	
	@IBOutlet weak var segmentedControlUIView: UIView!
	@IBOutlet weak var barSegmentedControl: UISegmentedControl!
	
	// Height Constraints
	@IBOutlet weak var descriptionHeight: NSLayoutConstraint!
	@IBOutlet weak var errorLabelHeight: NSLayoutConstraint!
	
	//No Chart Data View
	@IBOutlet weak var noChartDataUIview: UIView!
	
	var dataRangeType: DateRangeType = .daily
	var nonNilValues = 0
	
	var currentSleepStartDate: Date?
	var currentSleepEndDate: Date?
	var customDateRangeForTemp: DateRange?
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		nonNilValues = 0
		
		/*
		subTitleLabel.layer.borderWidth = 1.0
		subTitleLabel.layer.borderColor = UIColor.hexColor(hex: "#ffffff")?.cgColor
		subTitleLabel.layer.opacity = 0.5
		subTitleLabel.layer.cornerRadius = 2.0
		subTitleLabel.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		 */
		descriptionLabel.sizeToFit()
		
		barSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
		barSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .normal)
		
		segmentedControlUIView.clipsToBounds = true
		segmentedControlUIView.layer.cornerRadius = 2.0
		segmentedControlUIView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		
		descriptionHeight.constant = 0
		errorLabelHeight.constant = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
		if selected {
			self.selectedBackgroundView?.backgroundColor = UIColor.hexColor(hex: "#1C1C36")
			contentView.backgroundColor = UIColor.hexColor(hex: "#1C1C36")
		} else {
			self.selectedBackgroundView?.backgroundColor = UIColor.hexColor(hex: "#1C1C36")
			contentView.backgroundColor = UIColor.hexColor(hex: "#1C1C36")
		}
	}
	
	@IBAction func rangeSelected(_ sender: UISegmentedControl) {
		
	}
	
	func loadChartView(model: ActivityChartModel, limitLineValues: [Int],  chartDataForBP: [ActivityChartModel]? = [ActivityChartModel(type: "", chartData: ActivityChartData(barColor: .clear, elements: []))]) {
		let modelType = model.type
		
		let thisdata = model.chartData.elements.reduce(0.0, { partialResult, element in
			return partialResult + (element.value ?? 0)
		})
		self.barChart(isVisible: (thisdata == 0) ? false : true)
		
		if thisdata != 0 {
			if (modelType == "Systolic Blood Pressure" || modelType == "Diastolic Blood Pressure") {
				if ((chartDataForBP![0].chartData.elements.count < 8) && (chartDataForBP![1].chartData.elements.count < 8)) {
					let totalDiastolicSum = chartDataForBP?[0].chartData.elements.reduce(0) { (result: Double, nextItem: ChartElement) -> Double in
						return result + (nextItem.value ?? 0.0)
					} ?? 0.0
					
					let totalSystolicSum = chartDataForBP?[1].chartData.elements.reduce(0) { (result: Double, nextItem: ChartElement) -> Double in
						return result + (nextItem.value ?? 0.0)
					} ?? 0.0
					
					var nonZeroDiaCount = 0
					var nonZeroSysCOunt = 0
					for x in 0..<(chartDataForBP![0].chartData.elements.count) {
						if ((chartDataForBP![0].chartData.elements[x].value != 0) && chartDataForBP![0].chartData.elements[x].value != nil) {
							nonZeroDiaCount = nonZeroDiaCount + 1
						}
					}
					
					for x in 0..<(chartDataForBP![1].chartData.elements.count) {
						if ((chartDataForBP![1].chartData.elements[x].value != 0) && chartDataForBP![1].chartData.elements[x].value != nil) {
							nonZeroSysCOunt = nonZeroSysCOunt + 1
						}
					}
					
					let avgDiastolic = (nonZeroDiaCount != 0) ? totalDiastolicSum / Double(nonZeroDiaCount) : 0
					let avgSystolic = (nonZeroSysCOunt != 0) ? totalSystolicSum / Double(nonZeroSysCOunt) : 0
					
					let avgDiastolicInt = (nonZeroDiaCount != 0) ? Int(avgDiastolic.rounded()) : 0
					let avgSystolicInt = (nonZeroSysCOunt != 0) ? Int(avgSystolic.rounded()) : 0
					
					self.valueLabel.text = "\(avgDiastolicInt)/\(avgSystolicInt)"
				} else {
					self.valueLabel.text = "\(chartDataForBP?[0].value?.stringValue ?? "-")/\(chartDataForBP?[1].value?.stringValue ?? "-")"
				}
			} else {
				if (modelType == "Body Temperature") {
					if model.chartData.elements.count < 8 {
						let totalTempSum = model.chartData.elements.reduce(0) { (result: Double, nextItem: ChartElement) -> Double in
							return result + (nextItem.value ?? 0.0)
						}
						
						var nonZeroCount = 0
						for x in 0..<(model.chartData.elements.count) {
							if ((model.chartData.elements[x].value != 0) && (model.chartData.elements[x].value != nil)) {
								nonZeroCount = nonZeroCount + 1
							}
						}
						
						let avgTemp = (nonZeroCount != 0) ?  (totalTempSum / Double(nonZeroCount)) : 0
						let avgTempVar = Utility.getTempVariation(temperature: avgTemp)
						let roundedAvg = ((avgTempVar * 10).rounded() / 10)
						if roundedAvg == -0 {
							self.valueLabel.text = "0.0"
						} else {
							self.valueLabel.text = "\(roundedAvg)"
						}
						
					} else {
						if customDateRangeForTemp != nil {
							let newItem = CoreDataSkinTemperature.fetch(for: customDateRangeForTemp!.start ..< customDateRangeForTemp!.end)
							var averageSkinTemp = newItem.reduce(0) { (result: Double, nextItem: CoreDataSkinTemperature) -> Double in
								return abs(result) + abs(Utility.getTempVariation(temperature: nextItem.value.doubleValue))
							}
							averageSkinTemp = Double(averageSkinTemp) / Double((newItem.count))
							let avgVariation = averageSkinTemp
							let tempAvg = (((avgVariation * 10).rounded()) / 10)
							
							let totalBaselLineSum = (Double(newItem.count) * (KeyChain.shared.baselineBodyTemprature?.toDouble ?? 0.0))
							let totalTempSum = newItem.reduce(0) { (result: Double, nextItem: CoreDataSkinTemperature) -> Double in
								return result + nextItem.value.doubleValue
							}
							let isPositive = (totalTempSum > totalBaselLineSum)
							if (tempAvg == -0 || tempAvg == 0) {
								self.valueLabel.text = "0"
							} else {
								self.valueLabel.text = isPositive ? "\(tempAvg)" : "-\(tempAvg)"
							}
							
						} else {
							
							let item = model.chartData.elements
							let totalBaselLineSum = (Double(item.count) * (KeyChain.shared.baselineBodyTemprature?.toDouble ?? 0.0))
							let totalTempSum = item.reduce(0) { (result: Double, nextItem: ChartElement) -> Double in
								return result + (nextItem.value ?? 0.0)
							}
							let isPositive = (totalTempSum > totalBaselLineSum)
							
							if let avgVal = model.value {
								let avgValue = (Double(avgVal) * 10).rounded() / 10
								if (avgVal == -0 || avgVal == 0) {
									self.valueLabel.text = "0"
								} else {
									self.valueLabel.text = isPositive ? "\(avgValue)" : "-\(avgValue)"
								}
								
							}
						}
					}
				} else {
					if model.chartData.elements.count < 8 {
						let totalSum = model.chartData.elements.reduce(0) { (result: Double, nextItem: ChartElement) -> Double in
							return result + (nextItem.value ?? 0.0)
						}
						
						var nonZeroCount = 0
						for x in 0..<(model.chartData.elements.count) {
							if ((model.chartData.elements[x].value != 0) && (model.chartData.elements[x].value != nil)) {
								nonZeroCount = nonZeroCount + 1
							}
						}
						
						let avgTemp = (nonZeroCount != 0) ?  (totalSum / Double(nonZeroCount)) : 0
						self.valueLabel.text = "\(Int(avgTemp.rounded()))"
					} else {
						self.valueLabel.text = model.value?.stringValue
					}
				}
			}
			
			self.fetchActivityChart(chartData: model.chartData, limitLineValues: limitLineValues, modelType: model.type, chartDataForBP: chartDataForBP)
		}
	}
	
	func barChart(isVisible: Bool) {
		self.chartView.isHidden = !isVisible
		self.noChartDataUIview.isHidden = isVisible
	}
	
	private func fetchActivityChart(chartData: ActivityChartData, limitLineValues:[Int] = [], modelType: String, chartDataForBP: [ActivityChartModel]? = [ActivityChartModel(type: "", chartData: ActivityChartData(barColor: .clear, elements: []))]) {
		
		let thisdata = chartData.elements.reduce(0.0, { partialResult, element in
			return partialResult + (element.value ?? 0)
		})

		self.barChart(isVisible: (thisdata == 0) ? false : true)
		let baselineTemp = KeyChain.shared.baselineBodyTemprature?.toDouble ?? 0.0
		
		if thisdata != 0 {
			
			var xAxis = [TimeInterval]()
			var limitLineData = [[[Double?]]]()
			xAxis = chartData.elements.map({ (element) -> TimeInterval in
				return element.date.timeIntervalSince1970
			})
			
			var data = [[Double?]]()
			var data2 = [[Double?]]()
			nonNilValues = 0
			
			var tempChartElements = [ChartElement]()
			for m in 0..<chartData.elements.count {
				if chartData.elements[m].value != nil && chartData.elements[m].value != 0 {
					tempChartElements.append(chartData.elements[m])
				}
			}
			
			var minValueForSysBP = 0
			var maxValueForSysBP = 0
			var timeForMinValueForSysBP = Date()
			var minValueForDiaBP = 0
			var maxValueForDiaBP = 0
			var timeForMinValueForDiaBP = Date()
			
			var minValueOtherThBP = 0.0
			var maxValueOtherThanBP = 0.0
			var timeForMinValuesOtherThanBP = Date()
			
			if tempChartElements.last != nil {
				minValueOtherThBP = tempChartElements.reduce(tempChartElements.last!) { aggregate, element in
					(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
				}.value ?? 0
				
				maxValueOtherThanBP = tempChartElements.reduce(tempChartElements.last!) { aggregate, element in
					(aggregate.value ?? 0) < (element.value ?? 0) ? element : aggregate
				}.value ?? 0
				
				timeForMinValuesOtherThanBP = (tempChartElements.reduce(tempChartElements.last!) { aggregate, element in
					(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
				}.date)
			}
			
			if chartDataForBP!.count > 0 {
				if chartDataForBP?.count == 1 {
					var tempChartDiaBpElements = [ChartElement]()
					for m in 0..<chartDataForBP![0].chartData.elements.count {
						if chartDataForBP![0].chartData.elements[m].value != nil && chartDataForBP![0].chartData.elements[m].value != 0 {
							tempChartDiaBpElements.append(chartDataForBP![0].chartData.elements[m])
						}
					}
					if tempChartDiaBpElements.last != nil {
					
					minValueForDiaBP = Int(tempChartDiaBpElements.reduce(tempChartDiaBpElements.last!) { aggregate, element in
						(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
					}.value ?? 0)
					
					maxValueForDiaBP = Int(tempChartDiaBpElements.reduce(tempChartDiaBpElements.last!) { aggregate, element in
						(aggregate.value ?? 0) < (element.value ?? 0) ? element : aggregate
					}.value ?? 0)
					
					timeForMinValueForDiaBP = (tempChartDiaBpElements.reduce(tempChartDiaBpElements.last!) { aggregate, element in
						(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
					}.date)
					}
				} else {
					var tempChartSysBpElements = [ChartElement]()
					for m in 0..<chartDataForBP![1].chartData.elements.count {
						if chartDataForBP![1].chartData.elements[m].value != nil && chartDataForBP![1].chartData.elements[m].value != 0 {
							tempChartSysBpElements.append(chartDataForBP![1].chartData.elements[m])
						}
					}
					if tempChartSysBpElements.last != nil {
						minValueForSysBP = Int(tempChartSysBpElements.reduce(tempChartSysBpElements.last!) { aggregate, element in
							(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
						}.value ?? 0)
						
						maxValueForSysBP = Int(tempChartSysBpElements.reduce(tempChartSysBpElements.last!) { aggregate, element in
							(aggregate.value ?? 0) < (element.value ?? 0) ? element : aggregate
						}.value ?? 0)
						
						timeForMinValueForSysBP = (tempChartSysBpElements.reduce(tempChartSysBpElements.last!) { aggregate, element in
							(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
						}.date)
					}
				}
			}
			
			if (modelType == "Systolic Blood Pressure" || modelType == "Diastolic Blood Pressure") {
				for bp in 0..<chartDataForBP!.count {
					for (pointIndex, each) in chartDataForBP![bp].chartData.elements.enumerated() {
						if bp == 0 {
							data.append([xAxis[pointIndex] * 1000, each.value])
						} else {
							data2.append([xAxis[pointIndex] * 1000, each.value])
						}
					}
				}
			} else {
				for (pointIndex, each) in chartData.elements.enumerated() {
					if (modelType == "Body Temperature") {
						if each.value != nil && each.value != 0.0 {
							data.append([xAxis[pointIndex] * 1000, (each.value ?? 0.0)])
						} else {
							data.append([xAxis[pointIndex] * 1000, nil])
						}
					} else {
						data.append([xAxis[pointIndex] * 1000, each.value])
					}
				}
			}
			
			if (modelType == "SpO2") {
				limitLineData.append([[Double?]]())
				for pointIndex in chartData.elements.indices {
					limitLineData[0].append(([xAxis[pointIndex] * 1000, Double(limitLineValues[0])]))
				}
			} else if (modelType == "Body Temperature") {
				for x in 0..<2 {
					limitLineData.append([[Double?]]())
					for pointIndex in chartData.elements.indices {
						let xval = (x == 0) ? (baselineTemp - 0.5) : (baselineTemp + 0.5)
						limitLineData[x].append(([xAxis[pointIndex] * 1000, xval]))
					}
				}
			} else {
				for x in 0..<limitLineValues.count {
					limitLineData.append([[Double?]]())
					for pointIndex in chartData.elements.indices {
						limitLineData[x].append(([xAxis[pointIndex] * 1000, Double(limitLineValues[x])]))
					}
				}
			}
			
			var values = [data]
			let chart = HIChart()
			chart.type = "line"
//			chart.marginBottom = 40
			chart.backgroundColor = HIColor(uiColor: UIColor.clear)
			
			let hititle = HITitle()
			hititle.text = ""
			
			let hisubtitle = HISubtitle()
			hisubtitle.text = ""
			
			let hixAxis = HIXAxis()
			hixAxis.type = "datetime"
			hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
			
			if dataRangeType == .daily {
				hixAxis.dateTimeLabelFormats.day = HIDay()
				hixAxis.dateTimeLabelFormats.day.main = "%l %p"
				hixAxis.dateTimeLabelFormats.hour = HIHour()
				hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
			} else if dataRangeType == .weekly {
				hixAxis.dateTimeLabelFormats.day = HIDay()
				hixAxis.dateTimeLabelFormats.day.main = "%a"
			}
			
			let hixAxisTitle = HITitle()
			hixAxisTitle.text = ""
			hixAxis.title = hixAxisTitle
			
			hixAxis.tickWidth = 0
			hixAxis.lineWidth = 0
			
			if dataRangeType == .daily {
				if currentSleepStartDate != nil {
					let hour = Calendar.current.component(.hour, from: currentSleepStartDate!)
					currentSleepEndDate?.setTime(hour: hour, min: 0, sec: 0)
					hixAxis.min = NSNumber(value: (1000 * currentSleepStartDate!.timeIntervalSince1970))
				}
				hixAxis.tickInterval = NSNumber(value: 6000)//3600 * 1000 * 3
			} else if dataRangeType == .weekly {
				hixAxis.minRange = NSNumber(value: 3600 * 24000)
				hixAxis.tickInterval = NSNumber(value: 3600 * 24000)
			}
			
			hixAxis.lineWidth = 0
			hixAxis.gridLineWidth = 0
			hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
			hixAxis.labels = HILabels()
			hixAxis.labels.style = HICSSObject()
			hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			hixAxis.labels.style.fontSize = "12"
			hixAxis.labels.style.color = "#a5a5b2"
			hixAxis.labels.rotation = NSNumber(value:0)
			
			if dataRangeType == .daily {
				hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.isFirst || this.isLast) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + ' ' + ampm; return strTime } else { return '' } }")
			} else if dataRangeType == .weekly {
				hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ let weekDaysArray = ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa']; let theDay = new Date(this.value).getDay(); return weekDaysArray[theDay] }")
			}
			//		if(this.isFirst || this.isLast) { let theDay = new Date(this.value).getDay(); return weekDaysArray[theDay] } else { return '' }
			
			let yAxis = HIYAxis()
			yAxis.title = HITitle()
			yAxis.title.text = ""
			yAxis.labels = HILabels()
			yAxis.labels.overflow = "justify"
			yAxis.labels.style = HICSSObject()
			yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			yAxis.labels.style.fontSize = "12"
			yAxis.labels.style.color = "#a5a5b2"
			
			if limitLineValues.count > 1 {
				if limitLineValues[1] > (Int(maxValueOtherThanBP) + 5) {
					yAxis.max = NSNumber(value: limitLineValues[1] + 5)
				} else {
					yAxis.max = NSNumber(value: Int(maxValueOtherThanBP) + 5)
				}
				
				if limitLineValues[0] < (Int(minValueOtherThBP) - 5) {
					yAxis.min = NSNumber(value: limitLineValues[0] - 5)
				} else {
					yAxis.min = NSNumber(value: Int(minValueOtherThBP) - 5)
				}
			} else if limitLineValues.count == 1 {
				if limitLineValues[0] < (Int(minValueOtherThBP) - 5) {
					yAxis.min = NSNumber(value: limitLineValues[0] - 5)
				} else {
					yAxis.min = NSNumber(value: Int(minValueOtherThBP) - 5)
				}
			}
			
			if modelType == "Systolic Blood Pressure" {
				yAxis.gridLineWidth = 0
				
				if limitLineValues[0] < (Int(minValueForSysBP) - 5) {
					yAxis.min = NSNumber(value: limitLineValues[0] - 5)
				} else {
					yAxis.min = NSNumber(value: Int(minValueForSysBP) - 5)
				}
				
				if limitLineValues[1] > (Int(maxValueForSysBP) + 5) {
					yAxis.max = NSNumber(value: limitLineValues[1] + 5)
				} else {
					yAxis.max = NSNumber(value: Int(maxValueForSysBP) + 5)
				}
				
				let plotLines1 = HIPlotLines()
				plotLines1.value = yAxis.min
				plotLines1.width = 1
				plotLines1.color = HIColor(hexValue: "808080")
				plotLines1.label = HILabel()
				plotLines1.label.text = "\(yAxis.min!)"
				plotLines1.label.align = "left"
				plotLines1.label.x = -27
				plotLines1.label.y = 5
				plotLines1.label.style = HICSSObject()
				plotLines1.label.style.color = "#a5a5b2"
				
				let plotLines2 = HIPlotLines()
				plotLines2.value = yAxis.max
				plotLines2.width = 1
				plotLines2.color = HIColor(hexValue: "808080")
				plotLines2.label = HILabel()
				plotLines2.label.text = "\(yAxis.max!)"
				plotLines2.label.align = "left"
				plotLines2.label.x = -27
				plotLines2.label.y = 5
				plotLines2.label.style = HICSSObject()
				plotLines2.label.style.color = "#a5a5b2"
				
				let plotLines3 = HIPlotLines()
				plotLines3.value = yAxis.min
				plotLines3.width = 1
				plotLines3.label = HILabel()
				plotLines3.label.text = "\(limitLineValues[0])"
				plotLines3.label.align = "left"
				plotLines3.label.x = -27
				plotLines3.label.y = 5
				plotLines3.label.style = HICSSObject()
				plotLines3.label.style.color = "#a5a5b2"
				
				let plotLines4 = HIPlotLines()
				plotLines4.value = yAxis.max
				plotLines4.width = 1
				plotLines4.label = HILabel()
				plotLines4.label.text = "\(limitLineValues[1])"
				plotLines4.label.align = "left"
				plotLines4.label.x = -27
				plotLines4.label.y = 5
				plotLines4.label.style = HICSSObject()
				plotLines4.label.style.color = "#a5a5b2"
				
				chart.marginLeft = 30
				yAxis.plotLines = [plotLines1, plotLines2]
				
				hixAxis.crosshair = HICrosshair()
				hixAxis.crosshair.color = HIColor(rgb: 255, green: 255, blue: 255)
			} else if modelType == "Diastolic Blood Pressure" {
				yAxis.gridLineWidth = 0
				
				hixAxis.crosshair = HICrosshair()
				hixAxis.crosshair.color = HIColor(rgb: 255, green: 255, blue: 255)
			} else if modelType == "Heart Rate" {
				yAxis.gridLineWidth = 0
				yAxis.min = NSNumber(value: 0)
				yAxis.max = NSNumber(value: 200)
				
				let plotLines1 = HIPlotLines()
				plotLines1.value = yAxis.min
				plotLines1.width = 1
				plotLines1.color = HIColor(hexValue: "808080")
				
				plotLines1.label = HILabel()
				plotLines1.label.text = "0"
				plotLines1.label.align = "left"
				plotLines1.label.x = -27
				plotLines1.label.y = 3
				plotLines1.label.style = HICSSObject()
				plotLines1.label.style.color = "#a5a5b2"
				
				let plotLines2 = HIPlotLines()
				plotLines2.value = yAxis.max
				plotLines2.width = 1
				plotLines2.color = HIColor(hexValue: "808080")
				plotLines2.label = HILabel()
				plotLines2.label.text = "200"
				plotLines2.label.align = "left"
				plotLines2.label.x = -27
				plotLines2.label.y = 3
				plotLines2.label.style = HICSSObject()
				plotLines2.label.style.color = "#a5a5b2"
				
				chart.marginLeft = 30
				yAxis.plotLines = [plotLines1, plotLines2]
				
			} else if modelType == "SpO2" {
				yAxis.gridLineWidth = 0
				yAxis.startOnTick = false
				yAxis.endOnTick = false
				yAxis.min = NSNumber(value: 80)
				yAxis.max = NSNumber(value: 100)
				
				let plotLines1 = HIPlotLines()
				plotLines1.value = yAxis.min
				plotLines1.width = 1
				plotLines1.color = HIColor(hexValue: "808080")
				plotLines1.label = HILabel()
				plotLines1.label.text = "\(yAxis.min!)"
				plotLines1.label.align = "left"
				plotLines1.label.x = -27
				plotLines1.label.y = 3
				plotLines1.label.style = HICSSObject()
				plotLines1.label.style.color = "#a5a5b2"
				
				let plotLines2 = HIPlotLines()
				plotLines2.value = 100
				plotLines2.width = 1
				plotLines2.color = HIColor(hexValue: "808080")
				
				let plotLines3 = HIPlotLines()
				plotLines3.value = NSNumber(value: limitLineValues[0])
				plotLines3.width = 1
				plotLines3.label = HILabel()
				plotLines3.label.text = "Low"
				plotLines3.label.align = "left"
				plotLines3.label.x = -27
				plotLines3.label.y = 3
				plotLines3.label.style = HICSSObject()
				plotLines3.label.style.color = "#a5a5b2"
				
				chart.marginLeft = 35
				yAxis.plotLines = [plotLines1, plotLines2]
			} else if modelType == "Body Temperature" {
				let minValTemp = minValueOtherThBP
				let maxValTemp = maxValueOtherThanBP
				let minLimitLineTemp = (baselineTemp - 0.5)
				let maxLimitLineTemp = (baselineTemp + 0.5)
				
				if maxLimitLineTemp > (maxValTemp) {
					yAxis.max = NSNumber(value: maxLimitLineTemp + 0.5)
				} else {
					yAxis.max = NSNumber(value: maxValTemp + 0.5)
				}
				
				if minLimitLineTemp < (minValTemp) {
					yAxis.min = NSNumber(value: minLimitLineTemp - 0.5)
				} else {
					yAxis.min = NSNumber(value: minValTemp - 0.5)
				}
				
//				if Double(yAxis.max) != maxLimitLineTemp {
//					yAxis.labels.formatter = HIFunction(jsFunction: "function(){if(this.isFirst || this.isLast) {return this.value} else { return '' } }")
//				} else {
//					yAxis.labels.formatter = HIFunction(jsFunction: "function(){if(this.isFirst) {return this.value} else { return '' } }")
//				}
				
				yAxis.gridLineWidth = 0
			} else if modelType == "Breathing Rate" {
				yAxis.gridLineWidth = 0
				yAxis.gridLineColor = HIColor(uiColor: .clear)
				if abs(limitLineValues[0] - Int(yAxis.min)) < 3 {
					let min = Int(yAxis.min) - 3
					yAxis.min = NSNumber(value: min)
				}
				
				if abs(limitLineValues[1] - Int(yAxis.max)) < 3 {
					let max = Int(yAxis.max) + 3
					yAxis.max = NSNumber(value: max)
				}
				
				let plotLines1 = HIPlotLines()
				plotLines1.value = NSNumber(value: limitLineValues[0])
				plotLines1.dashStyle = "dash"
				plotLines1.color = HIColor(hexValue: "808080")
				plotLines1.width = 1
				plotLines1.label = HILabel()
				plotLines1.label.text = "Low"
				plotLines1.label.align = "left"
				plotLines1.label.x = -27
				plotLines1.label.y = 3
				plotLines1.label.style = HICSSObject()
				plotLines1.label.style.color = "#a5a5b2"
				
				let plotLines2 = HIPlotLines()
				plotLines2.value = NSNumber(value: limitLineValues[1])
				plotLines2.color = HIColor(hexValue: "808080")
				plotLines2.dashStyle = "dash"
				plotLines2.width = 1
				plotLines2.label = HILabel()
				plotLines2.label.text = "High"
				plotLines2.label.align = "left"
				plotLines2.label.x = -27
				plotLines2.label.y = 3
				plotLines2.label.style = HICSSObject()
				plotLines2.label.style.color = "#a5a5b2"
				
				let plotLines3 = HIPlotLines()
				plotLines3.value = yAxis.min
				plotLines3.width = 1
				plotLines3.color = HIColor(hexValue: "808080")
				plotLines3.label = HILabel()
				plotLines3.label.text = "\(yAxis.min!)"
				plotLines3.label.align = "left"
				plotLines3.label.x = -27
				plotLines3.label.y = 3
				plotLines3.label.style = HICSSObject()
				plotLines3.label.style.color = "#a5a5b2"
				
				let plotLines4 = HIPlotLines()
				plotLines4.value = yAxis.max
				plotLines4.width = 1
				plotLines4.color = HIColor(hexValue: "808080")
				plotLines4.label = HILabel()
				plotLines4.label.text = "\(yAxis.max!)"
				plotLines4.label.align = "left"
				plotLines4.label.x = -27
				plotLines4.label.y = 3
				plotLines4.label.style = HICSSObject()
				plotLines4.label.style.color = "#a5a5b2"
				
				yAxis.plotLines = []
				
				if yAxis.min != NSNumber(value: limitLineValues[0]) {
					yAxis.plotLines.append(plotLines1)
					yAxis.plotLines.append(plotLines3)
					if yAxis.max != NSNumber(value: limitLineValues[1]) {
						yAxis.plotLines.append(plotLines2)
						yAxis.plotLines.append(plotLines4)
					} else {
						yAxis.plotLines.append(plotLines4)
					}
				} else {
					yAxis.plotLines.append(plotLines3)
					if yAxis.max != NSNumber(value: limitLineValues[1]) {
						yAxis.plotLines.append(plotLines2)
						yAxis.plotLines.append(plotLines4)
					} else {
						yAxis.plotLines.append(plotLines4)
					}
				}
				
				chart.marginLeft = 30
			} else if modelType == "HRV    " {
				yAxis.gridLineWidth = 1
				yAxis.gridLineColor = HIColor(hexValue: "808080")
				if abs(limitLineValues[0] - Int(yAxis.min)) < 3 {
					let min = Int(yAxis.min) - 3
					yAxis.min = NSNumber(value: min)
				}
				
				if abs(limitLineValues[1] - Int(yAxis.max)) < 3 {
					let max = Int(yAxis.max) + 3
					yAxis.max = NSNumber(value: max)
				}
				
				let plotLines1 = HIPlotLines()
				plotLines1.value = NSNumber(value: limitLineValues[0])
				plotLines1.width = 1
				plotLines1.color = HIColor(hexValue: "808080")
				plotLines1.dashStyle = "dash"
				
				let plotLines3 = HIPlotLines()
				plotLines3.value = yAxis.min
				plotLines3.width = 1
				plotLines3.color = HIColor(hexValue: "808080")
				plotLines3.label = HILabel()
				plotLines3.label.text = "\(yAxis.min!)"
				plotLines3.label.align = "left"
				plotLines3.label.x = -27
				plotLines3.label.y = 3
				plotLines3.label.style = HICSSObject()
				plotLines3.label.style.color = "#a5a5b2"
				
				let plotLines4 = HIPlotLines()
				plotLines4.value = yAxis.max
				plotLines4.width = 1
				plotLines4.color = HIColor(hexValue: "808080")
				plotLines4.label = HILabel()
				plotLines4.label.text = "\(yAxis.max!)"
				plotLines4.label.align = "left"
				plotLines4.label.x = -27
				plotLines4.label.y = 3
				plotLines4.label.style = HICSSObject()
				plotLines4.label.style.color = "#a5a5b2"
				
				yAxis.plotLines = [plotLines3, plotLines4]
				chart.marginLeft = 30
			}
			
			yAxis.endOnTick = false

//			if Int(yAxis.max) != limitLineValues[1] {
//				yAxis.labels.formatter = HIFunction(jsFunction: "function(){if(this.isFirst || this.isLast) {return this.value} else { return '' } }")
//			} else {
//				yAxis.labels.formatter = HIFunction(jsFunction: "function(){if(this.isFirst) {return this.value} else { return '' } }")
//			}
			
			yAxis.labels.formatter = HIFunction(jsFunction: "function(){if(this.value) { return '' } }")
			
			let tooltip = HITooltip()
			tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
			tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
			tooltip.footerFormat = "</table>"
			tooltip.shared = true
			tooltip.useHTML = true
			tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
			
			let legend = HILegend()
			legend.enabled = NSNumber(value: false)
			
			let plotoptions = HIPlotOptions()
			plotoptions.line = HILine()
			plotoptions.line.states = HIStates()
			plotoptions.line.states.inactive = HIInactive()
			plotoptions.line.states.inactive.opacity = NSNumber(value: true)
			plotoptions.line.animation = HIAnimationOptionsObject()
			plotoptions.line.animation.duration = NSNumber(value: 0)
			
			hixAxis.labels.enabled = true
			hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
			
			let credits = HICredits()
			credits.enabled = NSNumber(value: false)
			
			let exporting = HIExporting()
			exporting.enabled = false
			
			var finalSeries:[HISeries] = []
			let series = HISeries()
			var uiColors = [UIColor.barPink.hexString]
			
			series.marker = HIMarker()
			series.marker.states = HIStates()
			series.marker.states.hover = HIHover()
			series.marker.states.hover.enabled = false
			
			let seriesSys = HISeries()
			let seriesDia = HISeries()
			
			if(modelType != "Body Temperature") {
				for x in 0..<values[0].count {
					values[0][x][1] = (values[0][x][1] == Double(0)) ? nil : values[0][x][1]
					data[x][1] = (data[x][1] == Double(0)) ? nil : data[x][1]
					if data2.count > x {
						data2[x][1] = (data2[x][1] == Double(0)) ? nil : data2[x][1]
					}
				}
			} else {
				for x in 0..<values[0].count {
					values[0][x][1] = (values[0][x][1] == (baselineTemp * -1)) ? nil : values[0][x][1]
					data[x][1] = (data[x][1] == (baselineTemp * -1)) ? nil : data[x][1]
					if data2.count > x {
						data2[x][1] = (data2[x][1] == (baselineTemp * -1)) ? nil : data2[x][1]
					}
				}
			}
			
			if (modelType == "Systolic Blood Pressure" || modelType == "Diastolic Blood Pressure") {
				seriesSys.marker = HIMarker()
				seriesSys.marker.states = HIStates()
				seriesSys.marker.states.hover = HIHover()
				seriesSys.marker.states.hover.enabled = false
				
				seriesDia.marker = HIMarker()
				seriesDia.marker.states = HIStates()
				seriesDia.marker.states.hover = HIHover()
				seriesDia.marker.states.hover.enabled = false
				if limitLineData.count > 0 {
					seriesSys.data = data as [Any]
					seriesSys.lineWidth = 4.0
					seriesSys.threshold = NSNumber(value: limitLineValues[1])
					seriesSys.color = HIColor(uiColor: .barPink)
					seriesSys.negativeColor = HIColor(uiColor: .barPurple)
					finalSeries.append(seriesSys)
					
					seriesDia.data = data2 as [Any]
					seriesDia.lineWidth = 4.0
					seriesDia.threshold = NSNumber(value: limitLineValues[0])
					seriesDia.color = HIColor(uiColor: .barPink)
					seriesDia.negativeColor = HIColor(hexValue: "428BCA")
					finalSeries.append(seriesDia)
				} else {
					series.data = values[0] as [Any]
					series.lineWidth = 4.0
					finalSeries.append(series)
				}
			} else if (modelType == "SpO2") {
				uiColors = [UIColor.barPurple.hexString]
				if limitLineData.count > 0 {
					series.data = values[0] as [Any]
					series.lineWidth = 4.0
					series.threshold = NSNumber(value: limitLineValues[0])
					series.negativeColor = HIColor(uiColor: .barPink)
					
					finalSeries.append(series)
				} else {
					series.data = values[0] as [Any]
					series.lineWidth = 4.0
					finalSeries.append(series)
				}
			} else if (modelType == "Body Temperature") {
				let baselineTemp = KeyChain.shared.baselineBodyTemprature?.toDouble ?? 0.0
				let newLimitLineValues = [(baselineTemp - 0.5), (baselineTemp + 0.5)]
				uiColors = [UIColor.barPink.hexString]
				if limitLineData.count > 0 {
					for y in 0..<limitLineData.count {
						series.data = values[0] as [Any]
						series.lineWidth = 4.0
						series.threshold = NSNumber(value: newLimitLineValues[y])
						series.color = HIColor(uiColor: .barPink)
						if (y == 1) {
							series.negativeColor = HIColor(uiColor: .barPurple)
						} else {
							series.negativeColor = HIColor(uiColor: .barPink)
						}
						finalSeries.append(series)
					}
				} else {
					series.data = values[0] as [Any]
					series.lineWidth = 4.0
					finalSeries.append(series)
				}
			} else if modelType == "HRV    " {
				uiColors = [UIColor.barPurple.hexString]
				if limitLineData.count > 0 {
					series.data = values[0] as [Any]
					series.lineWidth = 4.0
					series.threshold = NSNumber(value: limitLineValues[0])
					series.negativeColor = HIColor(uiColor: .barPink)
					
					finalSeries.append(series)
				} else {
					series.data = values[0] as [Any]
					series.lineWidth = 4.0
					finalSeries.append(series)
				}
			} else {
				if limitLineData.count > 0 {
					for y in 0..<limitLineData.count {
						series.data = values[0] as [Any]
						series.lineWidth = 4.0
						series.threshold = NSNumber(value: limitLineValues[y])
						series.negativeColor = ((y == 1) ? HIColor(uiColor: .barPurple) : HIColor(uiColor: .barPink))
						
						finalSeries.append(series)
					}
				} else {
					series.data = values[0] as [Any]
					series.lineWidth = 4.0
					finalSeries.append(series)
				}
			}
			
			let marker = HIMarker()
			marker.enabled = false
			
			if (modelType == "Systolic Blood Pressure" || modelType == "Diastolic Blood Pressure") {
				nonNilValues = 0
				for z in 0..<seriesDia.data.count {
					if ("\((seriesDia.data[z] as? [Any])!.last!)" != "nil") {
						nonNilValues += 1
					}
				}
				seriesDia.marker = (nonNilValues != 1) ? marker : HIMarker()
				
				nonNilValues = 0
				for z in 0..<seriesSys.data.count {
					if ("\((seriesSys.data[z] as? [Any])!.last!)" != "nil") {
						nonNilValues += 1
					}
				}
				seriesSys.marker = (nonNilValues != 1) ? marker : HIMarker()
			} else {
				for z in 0..<series.data.count {
					if ("\((series.data[z] as? [Any])!.last!)" != "nil") {
						nonNilValues += 1
					}
				}
				series.marker = (nonNilValues != 1) ? marker : HIMarker()
			}
			
			let time =  HITime()
			time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
			
//			for y in 0..<limitLineData.count {
//				let limitLineSeries = HISeries()
//				limitLineSeries.data = limitLineData[y]
//				limitLineSeries.dashStyle = "shortdot"
//				limitLineSeries.color = HIColor(uiColor: UIColor.textColor)
//				limitLineSeries.marker = marker
//				limitLineSeries.marker.states = HIStates()
//				limitLineSeries.marker.states.hover = HIHover()
//				limitLineSeries.marker.states.hover.enabled = false
//
//				finalSeries.append(limitLineSeries)
//			}
			
			if (modelType == "Body Temperature") {
				let values = [(baselineTemp - 0.5), (baselineTemp + 0.5), baselineTemp, Double(truncating: yAxis.min)]
				for y in 0..<values.count {
					let limitLine = HIPlotLines()
					let value = ((values[y] * 10).rounded()/10)
					limitLine.value = NSNumber(value: value)
					limitLine.width = 1
					limitLine.dashStyle = ((values[y] == baselineTemp) || (values[y] == Double(yAxis.min))) ? "line" : "dash"
					limitLine.color = HIColor(hexValue: "808080")
					limitLine.label = HILabel()
					if y == 0 {
						limitLine.label.text = "-0.5"
						limitLine.label.x = -28
					} else if y == 1 {
						limitLine.label.text = "+0.5"
						limitLine.label.x = -28
					} else if y == 2 {
						limitLine.label.text = "0.0"//"\(baselineTemp)"
						limitLine.label.x = -24
					} else {
						let temp = ((Double(yAxis.min) - baselineTemp) * 10).rounded() / 10
						limitLine.label.text = "\(temp)"
						limitLine.label.x = -28
					}
//					limitLine.label.text = (value == baselineTemp) ? "\(baselineTemp)" : "\(((value - baselineTemp) * 10).rounded() / 10)"
					limitLine.label.align = "left"
					limitLine.label.y = 5
					limitLine.label.style = HICSSObject()
					limitLine.label.style.fontFamily = Utility.getFontFamilyForCharts()
					limitLine.label.style.fontSize = "12"
					limitLine.label.style.color = "#a5a5b2"
					
					if yAxis.plotLines != nil {
						yAxis.plotLines.append(limitLine)
					} else {
						yAxis.plotLines = [limitLine]
					}
				}
				let maxValue = Utility.getTempVariation(temperature: Double(yAxis.max))
				var newPlotLinesArray: [Double]
				if maxValue > 0.5 {
					newPlotLinesArray = Array(stride(from: 0.5, to: maxValue, by: 0.5))
				} else {
					newPlotLinesArray = []
				}
				if (((newPlotLinesArray.last ?? 0) < maxValue) && ((maxValue - (newPlotLinesArray.last ?? 0)) < 0.5)) {
					newPlotLinesArray.append(newPlotLinesArray.last! + 0.5)
				}
				
				// Addd upper plotLines
				for y in 1..<newPlotLinesArray.count {
					let limitLine = HIPlotLines()
					limitLine.value = NSNumber(value: (baselineTemp + newPlotLinesArray[y]))
					limitLine.width = 1
					limitLine.dashStyle = "line"
					limitLine.color = HIColor(hexValue: "808080")
					limitLine.label = HILabel()
					limitLine.label.text = "+\(newPlotLinesArray[y])"
					limitLine.label.x = -28
					limitLine.label.align = "left"
					limitLine.label.y = 5
					limitLine.label.style = HICSSObject()
					limitLine.label.style.fontFamily = Utility.getFontFamilyForCharts()
					limitLine.label.style.fontSize = "12"
					limitLine.label.style.color = "#a5a5b2"
					yAxis.plotLines.append(limitLine)
				}
				
				chart.marginLeft = 30
			} else {
				if modelType != "Breathing Rate" {
					for y in 0..<limitLineValues.count {
						let limitLine = HIPlotLines()
						limitLine.value = NSNumber(value: limitLineValues[y])
						limitLine.width = 1
						limitLine.dashStyle = "dash"
						limitLine.color = HIColor(hexValue: "808080")
						limitLine.label = HILabel()
						limitLine.label.text = "\(limitLineValues[y])"
						limitLine.label.align = "left"
						limitLine.label.x = -27
						limitLine.label.y = 5
						limitLine.label.style = HICSSObject()
						limitLine.label.style.fontFamily = Utility.getFontFamilyForCharts()
						limitLine.label.style.fontSize = "12"
						limitLine.label.style.color = "#a5a5b2"
						
						if (modelType == "Systolic Blood Pressure" || modelType == "Diastolic Blood Pressure") {
							limitLine.color = (y == 0) ? HIColor(hexValue: "428BCA") : HIColor(uiColor: .barPurple)
						}
						
						if yAxis.min != limitLine.value {
							if yAxis.plotLines != nil {
								yAxis.plotLines.append(limitLine)
							} else {
								yAxis.plotLines = [limitLine]
							}
						}
					}
				}
			}
			yAxis.endOnTick = false
			yAxis.gridLineWidth = 0
			yAxis.minTickInterval = 0.5
			yAxis.tickInterval = 0.5
			
			let options = HIOptions()
			options.chart = chart
			options.title = hititle
			options.subtitle = hisubtitle
			options.xAxis = [hixAxis]
			options.yAxis = [yAxis]
			options.tooltip = tooltip
			options.credits = credits
			options.exporting = exporting
			options.series = finalSeries
			options.colors = uiColors
			options.plotOptions = plotoptions
			options.time = time
			options.legend = legend
			
			//		self.chartView.sizeToFit()
			self.chartView.isUserInteractionEnabled = false
			self.chartView.options = options
		}
	}
    
	override func layoutSubviews() {
		super.layoutSubviews()
		contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 6, left: 0, bottom: 6, right: 0))
	}
}
