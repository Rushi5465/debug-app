//
//  SleepParameterCollectionViewCell.swift
//  BloodPressureApp
//
//  Created by Rushikant on 21/10/21.
//

import UIKit

class SleepParameterCollectionViewCell: UICollectionViewCell {
	
    @IBOutlet weak var imageIndicator: UIImageView!
    @IBOutlet weak var readingLabel: MovanoLabel!
	@IBOutlet weak var parameterLabel: MovanoLabel!
	@IBOutlet weak var paramaterIcon: UIImageView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
		
//		self.layoutMargins = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
	}

	func readingData(value: String, unit: String?, iconName: String?) -> NSAttributedString {
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.semiBold(withSize: 20)]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.regular(withSize: 12),
					  convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.hexColor(hex: "#B7B7C7")]
		
		let attributedString = NSMutableAttributedString(string: value, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1))
		if let unit = unit {
			attributedString.append(NSMutableAttributedString(string: "  " + unit, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		}
		return attributedString
	}
}
