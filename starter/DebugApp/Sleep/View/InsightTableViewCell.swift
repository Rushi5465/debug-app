//
//  InsightTableViewCell.swift
//  BloodPressureApp
//
//  Created by Rushikant on 01/11/21.
//

import UIKit
import Highcharts

// swiftlint:disable all
class InsightTableViewCell: UITableViewCell {

	@IBOutlet weak var chartView: HIChartView!
	@IBOutlet weak var chartHeight: NSLayoutConstraint!
	
	var indexPath: IndexPath!
	
//	var isOpen: Bool = false {
//		didSet {
//			if self.isOpen {
//				self.chartHeight.constant = 250
//			} else {
//				self.chartHeight.constant = 200
//			}
//		}
//	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		
//		self.isOpen = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func fetchChart(chartData: InsightChartData) {
		
		var xAxis = [TimeInterval]()
		var limitLineData = [[[Double?]]]()
		xAxis = chartData.elements.map({ (element) -> TimeInterval in
			return element.date.timeIntervalSince1970
		})
		
		var data = [[Double?]]()
		
		for (pointIndex, each) in chartData.elements.enumerated() {
			data.append([xAxis[pointIndex] * 1000, each.value])
		}
		
		if chartData.limitLines.count > 0 {
			for x in 0 ..< chartData.limitLines.count {
				limitLineData.append([[Double?]]())
				for pointIndex in chartData.elements.indices {
					limitLineData[x].append(([xAxis[pointIndex] * 1000, Double(chartData.limitLines[x])]))
				}
			}
		}
		
		let values = [data]
		let chart = HIChart()
		chart.type = "line"
		chart.marginBottom = 70
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
		hixAxis.dateTimeLabelFormats.day = HIDay()
		hixAxis.dateTimeLabelFormats.day.main = "%e"
		
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		
		hixAxis.tickWidth = 0
		hixAxis.lineWidth = 0
		
		hixAxis.tickInterval = NSNumber(value: 3600 * 1000 * 24 * 2)
		
		hixAxis.lineWidth = 0
		
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.overflow = "justify"
		yAxis.labels.style = HICSSObject()
		yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		yAxis.labels.style.fontSize = "12"
		yAxis.labels.style.color = "#a5a5b2"
		yAxis.min = chartData.min
		yAxis.max = chartData.max
		yAxis.gridLineWidth = NSNumber(value: 0)
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotoptions = HIPlotOptions()
		plotoptions.line = HILine()
		plotoptions.line.states = HIStates()
		plotoptions.line.states.inactive = HIInactive()
		plotoptions.line.states.inactive.opacity = NSNumber(value: true)
		plotoptions.line.animation = HIAnimationOptionsObject()
		plotoptions.line.animation.duration = NSNumber(value: 0)
		
		hixAxis.labels.enabled = true
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		var finalSeries:[HISeries] = []
		let series = HISeries()
		let uiColors = [UIColor.textColor.hexString]
		if limitLineData.count > 0 {
			for y in 0..<limitLineData.count {
				series.data = values[0] as [Any]
				series.lineWidth = 4.0
				series.threshold = NSNumber(value: chartData.limitLines[y])
				series.negativeColor = ((y == 1) ? HIColor(uiColor: .barPink) : HIColor(uiColor: .barPurple))
				finalSeries.append(series)
			}
		} else {
				series.data = values[0] as [Any]
				series.lineWidth = 4.0
				finalSeries.append(series)
		}
		
		let marker = HIMarker()
		marker.enabled = false
		series.marker = HIMarker()
		
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		for y in 0 ..< limitLineData.count {
			let limitLineSeries = HISeries()
			limitLineSeries.data = limitLineData[y]
			limitLineSeries.dashStyle = "shortdot"
			limitLineSeries.color = HIColor(uiColor: UIColor.textColor)
			limitLineSeries.marker = marker
			
			finalSeries.append(limitLineSeries)
		}
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = finalSeries
		options.colors = uiColors
		options.plotOptions = plotoptions
		options.time = time
		options.legend = legend
		
		self.chartView.options = options
		
	}
    
}
