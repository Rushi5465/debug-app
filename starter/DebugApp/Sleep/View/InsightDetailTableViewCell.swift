//
//  InsightDetailTableViewCell.swift
//  BloodPressureApp
//
//  Created by Rushikant on 08/11/21.
//

import UIKit
import Highcharts

// swiftlint:disable all
class InsightDetailTableViewCell: UITableViewCell {

	//MARK: - IBOutlets
	
	@IBOutlet weak var chartTitleLabel: MovanoLabel!
	@IBOutlet weak var chartView: HIChartView!
	@IBOutlet weak var titleLabelHeightConstraint: NSLayoutConstraint!
	@IBOutlet weak var chartViewHeightConstraint: NSLayoutConstraint!
	@IBOutlet weak var noChartView: UIView!
	
	var indexPath: IndexPath!
	
//	var isSleepChart: Bool = false {
//		didSet {
//			if self.isSleepChart {
//				self.chartViewHeightConstraint.constant = 180
//			} else {
//				self.chartViewHeightConstraint.constant = 180
//			}
//		}
//	}
	
	var currentSleepStartDate: Date?
	var currentSleepEndDate: Date?
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		
//		self.isSleepChart = false
//		self.chartViewHeightConstraint.constant = 160
//		contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0))
    }

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
		if selected {
			contentView.backgroundColor = UIColor.hexColor(hex: "#1C1C36")
		} else {
			contentView.backgroundColor = UIColor.hexColor(hex: "#1C1C36")
		}
	}
    
	func fetchChart(chartData: InsightChartData, parameter: String) {
		
		let thisdata = chartData.elements.reduce(0.0, { partialResult, element in
			return partialResult + (element.value ?? 0)
		})
		
		self.barChart(isVisible: (thisdata == 0) ? false : true)
		if thisdata != 0 {
			var xAxis = [TimeInterval]()
			var limitLineData = [[[Double?]]]()
			xAxis = chartData.elements.map({ (element) -> TimeInterval in
				return element.date.timeIntervalSince1970
			})
			
			let minValue = Int(chartData.elements.reduce(chartData.elements.last!) { aggregate, element in
				(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
			}.value ?? 0)
			
			let maxValue = Int(chartData.elements.reduce(chartData.elements.last!) { aggregate, element in
				(aggregate.value ?? 0) < (element.value ?? 0) ? element : aggregate
			}.value ?? 0)
			
			let timeForMinValues = (chartData.elements.reduce(chartData.elements.last!) { aggregate, element in
				(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
			}.date)
			
			var indexForMinValues = 0
			for x in 0..<chartData.elements.count {
				if chartData.elements[x].value == Double(minValue) {
					indexForMinValues = x
					break
				}
			}
			
			var data = [[Double?]]()
			
			for (pointIndex, each) in chartData.elements.enumerated() {
				data.append([xAxis[pointIndex] * 1000, each.value])
			}
			
			if chartData.limitLines.count > 0 {
				for x in 0 ..< chartData.limitLines.count {
					limitLineData.append([[Double?]]())
					for pointIndex in chartData.elements.indices {
						limitLineData[x].append(([xAxis[pointIndex] * 1000, Double(chartData.limitLines[x])]))
					}
				}
			}
			
			let values = [data]
			let chart = HIChart()
			chart.type = "line"
			//		chart.marginBottom = 70
//			chart.marginLeft = 64
			chart.backgroundColor = HIColor(uiColor: UIColor.clear)
			
			let hititle = HITitle()
			hititle.text = ""
			
			let hisubtitle = HISubtitle()
			hisubtitle.text = ""
			
			let hixAxis = HIXAxis()
			hixAxis.type = "datetime"
			hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
			hixAxis.dateTimeLabelFormats.day = HIDay()
			hixAxis.dateTimeLabelFormats.day.main = "%l %p"
			hixAxis.dateTimeLabelFormats.hour = HIHour()
			hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
			
			let hixAxisTitle = HITitle()
			hixAxisTitle.text = ""
			hixAxis.title = hixAxisTitle
			
			hixAxis.tickWidth = 0
			hixAxis.lineWidth = 0
			
			if currentSleepStartDate != nil {
				let hour = Calendar.current.component(.hour, from: currentSleepStartDate!)
				currentSleepEndDate?.setTime(hour: hour, min: 0, sec: 0)
				hixAxis.min = NSNumber(value: (1000 * currentSleepStartDate!.timeIntervalSince1970))
			}
			hixAxis.tickInterval = NSNumber(value: 6000)//3600 * 1000 * 3
			
			hixAxis.lineWidth = 0
			
			hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
			hixAxis.labels = HILabels()
			hixAxis.labels.style = HICSSObject()
			hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			hixAxis.labels.style.fontSize = "10"
			hixAxis.labels.style.color = "#a5a5b2"
			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.isFirst || this.isLast) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + ' ' + ampm; return strTime } else { return '' } }")
			hixAxis.crosshair = HICrosshair()
			hixAxis.crosshair.color = HIColor(rgb: 255, green: 255, blue: 255)
			hixAxis.crosshair.zIndex = 3
			hixAxis.labels.rotation = NSNumber(value: 0)
//			hixAxis.labels.enabled = ((parameter == "Heart Rate") || (parameter == "SpO2")) ? NSNumber(value: 0) : NSNumber(value: 1)
			
			let yAxis = HIYAxis()
			yAxis.title = HITitle()
			yAxis.title.text = ""
			yAxis.labels = HILabels()
			yAxis.labels.overflow = "justify"
			yAxis.labels.style = HICSSObject()
			yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			yAxis.labels.style.fontSize = "10"
			yAxis.labels.style.color = "#a5a5b2"
			yAxis.min = NSNumber(value: minValue - 5)
			
			if parameter == "Breathing Rate" {
				if chartData.limitLines.count == 2 {
					if chartData.limitLines[1] > maxValue {
						yAxis.max = NSNumber(value: chartData.limitLines[1] + 1)
					} else {
						yAxis.max = NSNumber(value: maxValue + 1)
					}
					
					if chartData.limitLines[0] < (minValue) {
						yAxis.min = NSNumber(value: chartData.limitLines[0] - 1)
					} else {
						yAxis.min = NSNumber(value: minValue - 1)
					}
				} else if chartData.limitLines.count == 1 {
					if chartData.limitLines[0] < (minValue) {
						yAxis.min = NSNumber(value: chartData.limitLines[0] - 1)
					} else {
						yAxis.min = NSNumber(value: minValue - 1)
					}
				}
			} else {
				if chartData.limitLines.count > 1 {
					if chartData.limitLines[1] > (maxValue + 5) {
						yAxis.max = NSNumber(value: chartData.limitLines[1] + 5)
					} else {
						if (chartData.limitLines[1]) > maxValue {
							if ( (2 * (chartData.limitLines[1])) - maxValue) > 3 {
								yAxis.max = NSNumber(value: (chartData.limitLines[1]) + 3 )
							} else {
								yAxis.max = NSNumber(value: ( (2 * (chartData.limitLines[1])) - maxValue) + 3 )
							}
						} else {
							if ( (2 * maxValue) - (chartData.limitLines[1])) > 3 {
								yAxis.max = NSNumber(value: maxValue + 3 )
							} else {
								yAxis.max = NSNumber(value: ( (2 * maxValue) - (chartData.limitLines[1])) + 3 )
							}
							
						}
					}
					
					if chartData.limitLines[0] < (minValue - 5) {
						yAxis.min = NSNumber(value: chartData.limitLines[0] - 5)
					} else {
						if (chartData.limitLines[0]) < minValue {
							if ((2 * (chartData.limitLines[0])) - minValue) > 3 {
								yAxis.min = NSNumber(value: (chartData.limitLines[0]) - 3)
							} else {
								yAxis.min = NSNumber(value: ( (2 * (chartData.limitLines[0])) - minValue) - 3 )
							}
						} else {
							if ( (2 * minValue) - (chartData.limitLines[0])) > 3 {
								yAxis.min = NSNumber(value: minValue - 3)
							} else {
								yAxis.min = NSNumber(value: ( (2 * minValue) - (chartData.limitLines[0])) - 3 )
							}
							
						}
						
					}
				} else if chartData.limitLines.count == 1 {
					if chartData.limitLines[0] < (minValue - 5) {
						yAxis.min = NSNumber(value: chartData.limitLines[0] - 5)
					} else {
						yAxis.min = NSNumber(value: minValue - 5)
					}
				}
			}
			
			if parameter == "Heart Rate" {
				yAxis.min = NSNumber(value: 0)
				yAxis.max = NSNumber(value: 200)
			} else if parameter == "SpO2" {
				yAxis.min = NSNumber(value: 80)
				yAxis.max = NSNumber(value: 100)
			}
			
			yAxis.gridLineWidth = NSNumber(value: 0)
			yAxis.endOnTick = false
//			if chartData.limitLines.count == 1 {
//				yAxis.labels.formatter = HIFunction(jsFunction: "function(){if(this.isFirst || this.isLast || (this.value == '\(chartData.limitLines[0])')) {return this.value} else { return '' } }")
//			} else if chartData.limitLines.count == 2{
//				yAxis.labels.formatter = HIFunction(jsFunction: "function(){if(this.isFirst || this.isLast || (this.value == '\(chartData.limitLines[0])') || (this.value == '\(chartData.limitLines[1])')) {return this.value} else { return '' } }")
//			}
			
			yAxis.labels.formatter = HIFunction(jsFunction: "function(){if(this.isFirst) {return this.value} else { return '' } }")
			
//			yAxis.labels.y = 5
//			yAxis.labels.x = -26
//			yAxis.labels.enabled = NSNumber(value: 0)
			yAxis.minTickInterval = 1
			yAxis.tickInterval = 1
			
			if parameter == "Heart Rate" {
				yAxis.minTickInterval = 20
				yAxis.tickInterval = 20
			} else if parameter == "SpO2" {
				yAxis.minTickInterval = 10
				yAxis.tickInterval = 10
			}
			
			let minPlotLine = HIPlotLines()
			minPlotLine.value = yAxis.min
			minPlotLine.width = 1
			minPlotLine.color = HIColor(hexValue: "808080")
			minPlotLine.label = HILabel()
//			minPlotLine.label.text = "Low"
			minPlotLine.label.align = "left"
//			minPlotLine.label.x = -26
//			minPlotLine.label.y = 5
			
			let maxPlotLines = HIPlotLines()
			maxPlotLines.value = yAxis.max
			maxPlotLines.width = 1
			maxPlotLines.color = HIColor(hexValue: "808080")
			maxPlotLines.label = HILabel()
			maxPlotLines.label.text = "\(yAxis.max!)"
			maxPlotLines.label.align = "left"
			maxPlotLines.label.x = -26
			maxPlotLines.label.y = 5
			maxPlotLines.label.style = HICSSObject()
			maxPlotLines.label.style.fontFamily = Utility.getFontFamilyForCharts()
			maxPlotLines.label.style.fontSize = "10"
			maxPlotLines.label.style.color = "#a5a5b2"
			
			let limitLine1 = HIPlotLines()
			limitLine1.value = NSNumber(value: chartData.limitLines[0])
			limitLine1.width = 1
			limitLine1.color = HIColor(hexValue: "808080")
			limitLine1.label = HILabel()
			limitLine1.label.text = "\(chartData.limitLines[0])"
			limitLine1.label.align = "left"
			limitLine1.label.x = -26
			limitLine1.label.y = 5
			limitLine1.label.style = HICSSObject()
			limitLine1.label.style.fontFamily = Utility.getFontFamilyForCharts()
			limitLine1.label.style.fontSize = "10"
			limitLine1.label.style.color = "#a5a5b2"
			
			let limitLine2 = HIPlotLines()
			limitLine2.value = NSNumber(value: chartData.limitLines[1])
			limitLine2.width = 1
			limitLine2.color = HIColor(hexValue: "808080")
			limitLine2.label = HILabel()
			limitLine2.label.text = "\(chartData.limitLines[1])"
			limitLine2.label.align = "left"
			limitLine2.label.x = -26
			limitLine2.label.y = 5
			limitLine2.label.style = HICSSObject()
			limitLine2.label.style.fontFamily = Utility.getFontFamilyForCharts()
			limitLine2.label.style.fontSize = "10"
			limitLine2.label.style.color = "#a5a5b2"
			
			if Int(yAxis.max) != chartData.limitLines[1] {
				yAxis.plotLines = [minPlotLine, maxPlotLines, limitLine1, limitLine2 ]
			} else {
				yAxis.plotLines = [minPlotLine, maxPlotLines, limitLine1 ]
			}
			
			let tooltip = HITooltip()
			tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
			tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
			tooltip.footerFormat = "</table>"
			tooltip.shared = true
			tooltip.useHTML = true
			tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
			
			let legend = HILegend()
			legend.enabled = NSNumber(value: false)
			
			let plotoptions = HIPlotOptions()
			plotoptions.line = HILine()
			plotoptions.line.states = HIStates()
			plotoptions.line.states.inactive = HIInactive()
			plotoptions.line.states.inactive.opacity = NSNumber(value: true)
			plotoptions.line.animation = HIAnimationOptionsObject()
			plotoptions.line.animation.duration = NSNumber(value: 0)
			
//			hixAxis.labels.enabled = true
			hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
			
			let credits = HICredits()
			credits.enabled = NSNumber(value: false)
			
			let exporting = HIExporting()
			exporting.enabled = false
			
			var finalSeries:[HISeries] = []
			let series = HISeries()
			let uiColors = [UIColor.textColor.hexString]
			var colorForPeakPoints = ""
			if parameter == "Heart Rate" || parameter == "SpO2"{
				series.color = HIColor(uiColor: .barPurple)
				colorForPeakPoints = UIColor.barPink.hexString
			} else if parameter == "Heart Rate Variability" || parameter == "Breathing Rate"{
				series.color = HIColor(uiColor: .barPurple)
				colorForPeakPoints = UIColor.barPurple.hexString
			} else {
				series.color = HIColor(uiColor: .barPurple)
				colorForPeakPoints = UIColor.barPurple.hexString
			}
			
			if limitLineData.count > 0 {
				for y in 0..<limitLineData.count {
					series.data = values[0] as [Any]
//					series.data[indexForMinValues] = [ "x": Double("\(chartData.elements[indexForMinValues].date.timeIntervalSince1970 * 1000)"), "y": Double("\(minValue)"), "marker": ["enabled": true, "radius": 7, "lineWidth": 2, "fillColor": "#000000", "lineColor": "\(colorForPeakPoints)"]]
					series.lineWidth = 4.0
					//				series.threshold = NSNumber(value: chartData.limitLines[y])
					//				series.negativeColor = ((y == 1) ? HIColor(uiColor: .barPink) : HIColor(uiColor: .barPurple))
					finalSeries.append(series)
				}
			} else {
				series.data = values[0] as [Any]
				series.lineWidth = 4.0
				finalSeries.append(series)
			}
			
			let marker = HIMarker()
			marker.enabled = false
			series.marker = marker
			series.marker.symbol = ""
			series.marker.states = HIStates()
			series.marker.states.hover = HIHover()
			series.marker.states.hover.enabled = false
			
			let time =  HITime()
			time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
			
//			for y in 0 ..< limitLineData.count {
//				let limitLineSeries = HISeries()
//				limitLineSeries.data = limitLineData[y]
//				//			limitLineSeries.dashStyle = "shortdot"
//				limitLineSeries.color = HIColor(hexValue: "808080")
//				limitLineSeries.lineWidth = 1
//				limitLineSeries.marker = marker
//				limitLineSeries.marker.symbol = ""
//				limitLineSeries.marker.states = HIStates()
//				limitLineSeries.marker.states.hover = HIHover()
//				limitLineSeries.marker.states.hover.enabled = false
//
//				finalSeries.append(limitLineSeries)
//			}
			
			let options = HIOptions()
			options.chart = chart
			options.title = hititle
			options.subtitle = hisubtitle
			options.xAxis = [hixAxis]
			options.yAxis = [yAxis]
			options.tooltip = tooltip
			options.credits = credits
			options.exporting = exporting
			options.series = finalSeries
			options.colors = uiColors
			options.plotOptions = plotoptions
			options.time = time
			options.legend = legend
			
			self.chartView.isUserInteractionEnabled = false
			self.chartView.options = options
		}
		
	}
	
	func loadDayChartForSleep(chartData: InsightChartData) {
		var chartData = chartData
		let thisdata = chartData.elements.reduce(0.0, { partialResult, element in
			return partialResult + (element.value ?? 0)
		})
		
		self.barChart(isVisible: (thisdata == 0) ? false : true)
		if thisdata != 0 {
			var xAxis = [TimeInterval]()
			xAxis = chartData.elements.map({ (element) -> TimeInterval in
				return element.date.timeIntervalSince1970
			})
			
			var data = [[Double?]]()
			
			for (pointIndex, each) in chartData.elements.enumerated() {
				data.append([xAxis[pointIndex] * 1000, each.value])
			}
			//
			var values = [data]
			
			let chart = HIChart()
			chart.type = "line"
//			chart.marginBottom = 70
			chart.backgroundColor = HIColor(uiColor: UIColor.clear)
			
			let hititle = HITitle()
			hititle.text = ""
			
			let hisubtitle = HISubtitle()
			hisubtitle.text = ""
			
			let hixAxis = HIXAxis()
			hixAxis.type = "datetime"
			hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
			
			hixAxis.dateTimeLabelFormats.day = HIDay()
			hixAxis.dateTimeLabelFormats.day.main = "%l %p"
			hixAxis.dateTimeLabelFormats.hour = HIHour()
			hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
			
			if currentSleepStartDate != nil {
				let hour = Calendar.current.component(.hour, from: currentSleepStartDate!)
				currentSleepEndDate?.setTime(hour: hour, min: 0, sec: 0)
				hixAxis.min = NSNumber(value: (1000 * currentSleepStartDate!.timeIntervalSince1970))
			}
			hixAxis.tickInterval = NSNumber(value: 6000)//3600 * 1000 * 3
			
			let hixAxisTitle = HITitle()
			hixAxisTitle.text = ""
			hixAxis.title = hixAxisTitle
			
			hixAxis.gridLineWidth = 0
			hixAxis.minorGridLineWidth = 0
			
			//		hixAxis.min = NSNumber(value: ((range.start.startOfDay.timeIntervalSince1970)) * 1000)
			//		hixAxis.max = NSNumber(value: ((range.end.startOfDay.timeIntervalSince1970)) * 1000)
			hixAxis.lineWidth = 0
			
			hixAxis.tickColor = HIColor(uiColor: UIColor.clear)
			hixAxis.labels = HILabels()
			hixAxis.labels.style = HICSSObject()
			hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			hixAxis.labels.style.fontSize = "10"
			hixAxis.labels.style.color = "#a5a5b2"
			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.isFirst || this.isLast) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + ' ' + ampm; return strTime } else { return '' } }")
			hixAxis.crosshair = HICrosshair()
			hixAxis.crosshair.color = HIColor(rgb: 255, green: 255, blue: 255)
			hixAxis.crosshair.zIndex = 3
			hixAxis.labels.rotation = NSNumber(value: 0)
			hixAxis.labels.padding = NSNumber(-10)
			
			let yAxis = HIYAxis()
			yAxis.title = HITitle()
			yAxis.title.text = ""
			yAxis.labels = HILabels()
			yAxis.labels.overflow = "justify"
			yAxis.labels.style = HICSSObject()
			yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			yAxis.labels.style.fontSize = "10"
			yAxis.labels.style.color = "#a5a5b2"
			yAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.value < 5) {if (this.value == 1){return 'D'} else if (this.value == 2) {return 'L'} else if (this.value == 3) {return 'R'} else if (this.value == 4) {return 'A'} else {return ''}} else { return '' } }")
			yAxis.gridLineWidth = 0
			yAxis.minorGridLineWidth = 0
			//		yAxis.startOnTick = 0
			//		yAxis.endOnTick = 0
			yAxis.min = 0.5
			yAxis.max = 4
			yAxis.minTickInterval = 0.5
			yAxis.tickInterval = 0.5
			
			let minPlotLine = HIPlotLines()
			minPlotLine.value = yAxis.min
			minPlotLine.width = 1
			minPlotLine.color = HIColor(hexValue: "808080")
			yAxis.plotLines = [minPlotLine]
			
			let tooltip = HITooltip()
			tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
			tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
			tooltip.footerFormat = "</table>"
			tooltip.shared = true
			tooltip.useHTML = true
			tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
			
			let legend = HILegend()
			legend.enabled = NSNumber(value: false)
			
			let plotoptions = HIPlotOptions()
			plotoptions.line = HILine()
			plotoptions.line.states = HIStates()
			plotoptions.line.states.inactive = HIInactive()
			plotoptions.line.states.inactive.opacity = NSNumber(value: true)
			plotoptions.line.animation = HIAnimationOptionsObject()
			plotoptions.line.animation.duration = NSNumber(value: 0)
			//		plotoptions.series = HISeries()
			//		plotoptions.series.pointStart = 2010
			plotoptions.line.marker = HIMarker()
			plotoptions.line.marker.enabled = false
			
			let credits = HICredits()
			credits.enabled = NSNumber(value: false)
			
			let exporting = HIExporting()
			exporting.enabled = false
			
			let series = HISeries()
			let series1 = HISeries()
			let series2 = HISeries()
			let series3 = HISeries()
			let series4 = HISeries()
			let series5 = HISeries()
			let series6 = HISeries()
			let series7 = HISeries()
			let series8 = HISeries()
			let series9 = HISeries()
			let series10 = HISeries()
			let series11 = HISeries()
			let series12 = HISeries()
			let series13 = HISeries()
			let uiColors = [UIColor.barPurple.hexString]
			
			let dataTemp1 = chartData.elements.map({ (element) -> Double in
				return ((element.value! - 0.01))
			})
			
			let dataTemp2 = chartData.elements.map({ (element) -> Double in
				return ((element.value! - 0.02))
			})
			
			let dataTemp3 = chartData.elements.map({ (element) -> Double in
				return ((element.value! - 0.03))
			})
			
			let dataTemp4 = chartData.elements.map({ (element) -> Double in
				return ((element.value! - 0.04))
			})
			
			let dataTemp5 = chartData.elements.map({ (element) -> Double in
				return ((element.value! - 0.05))
			})
			
			let dataTemp6 = chartData.elements.map({ (element) -> Double in
				return ((element.value! - 0.06))
			})
			
			let dataTemp7 = chartData.elements.map({ (element) -> Double in
				return ((element.value! + 0.00))
			})
			
			let dataTemp8 = chartData.elements.map({ (element) -> Double in
				return ((element.value! - 0.07))
			})
			
			let dataTemp9 = chartData.elements.map({ (element) -> Double in
				return ((element.value! - 0.08))
			})
			
			let dataTemp10 = chartData.elements.map({ (element) -> Double in
				return ((element.value! - 0.09))
			})
			
			let dataTemp11 = chartData.elements.map({ (element) -> Double in
				return ((element.value! - 0.10))
			})
			
			let dataTemp12 = chartData.elements.map({ (element) -> Double in
				return ((element.value! - 0.11))
			})
			
			let dataTemp13 = chartData.elements.map({ (element) -> Double in
				return ((element.value! - 0.12))
			})
			
			var data1 = [[Double?]]()
			var data2 = [[Double?]]()
			var data3 = [[Double?]]()
			var data4 = [[Double?]]()
			var data5 = [[Double?]]()
			var data6 = [[Double?]]()
			var data7 = [[Double?]]()
			var data8 = [[Double?]]()
			var data9 = [[Double?]]()
			var data10 = [[Double?]]()
			var data11 = [[Double?]]()
			var data12 = [[Double?]]()
			var data13 = [[Double?]]()
			
			for (pointIndex, _) in chartData.elements.enumerated() {
				data1.append([xAxis[pointIndex] * 1000, dataTemp1[pointIndex]])
				data2.append([xAxis[pointIndex] * 1000, dataTemp2[pointIndex]])
				data3.append([xAxis[pointIndex] * 1000, dataTemp3[pointIndex]])
				data4.append([xAxis[pointIndex] * 1000, dataTemp4[pointIndex]])
				data5.append([xAxis[pointIndex] * 1000, dataTemp5[pointIndex]])
				data6.append([xAxis[pointIndex] * 1000, dataTemp6[pointIndex]])
				data7.append([xAxis[pointIndex] * 1000, dataTemp7[pointIndex]])
				data8.append([xAxis[pointIndex] * 1000, dataTemp8[pointIndex]])
				data9.append([xAxis[pointIndex] * 1000, dataTemp9[pointIndex]])
				data10.append([xAxis[pointIndex] * 1000, dataTemp10[pointIndex]])
				data11.append([xAxis[pointIndex] * 1000, dataTemp11[pointIndex]])
				data12.append([xAxis[pointIndex] * 1000, dataTemp12[pointIndex]])
				data13.append([xAxis[pointIndex] * 1000, dataTemp13[pointIndex]])
			}
			
			for x in 0..<values[0].count {
				values[0][x][1] = (values[0][x][1] == Double(0)) ? nil : values[0][x][1]
			 data1[x][1] = (data1[x][1] == Double(0 - 0.01)) ? nil : data1[x][1]
			 data2[x][1] = (data2[x][1] == Double(0 - 0.02)) ? nil : data2[x][1]
			 data3[x][1] = (data3[x][1] == Double(0 - 0.03)) ? nil : data3[x][1]
			 data4[x][1] = (data4[x][1] == Double(0 - 0.04)) ? nil : data4[x][1]
			 data5[x][1] = (data5[x][1] == Double(0 - 0.05)) ? nil : data5[x][1]
			 data6[x][1] = (data6[x][1] == Double(0 - 0.06)) ? nil : data6[x][1]
			 data7[x][1] = (data7[x][1] == Double(0)) ? nil : data7[x][1]
			 data8[x][1] = (data8[x][1] == Double(0 - 0.07)) ? nil : data8[x][1]
			 data9[x][1] = (data9[x][1] == Double(0 - 0.08)) ? nil : data9[x][1]
			 data10[x][1] = (data10[x][1] == Double(0 - 0.09)) ? nil : data10[x][1]
			 data11[x][1] = (data11[x][1] == Double(0 - 0.10)) ? nil : data11[x][1]
			 data12[x][1] = (data12[x][1] == Double(0 - 0.11)) ? nil : data12[x][1]
			 data13[x][1] = (data13[x][1] == Double(0 - 0.12)) ? nil : data13[x][1]
		 }
			
			let marker = HIMarker()
			marker.enabled = false
			series.marker = HIMarker()
			series.marker.states = HIStates()
			series.marker.states.hover = HIHover()
			series.marker.states.hover.enabled = false
			series.data = values[0] as [Any]
			series.step = "center"
			series.shadow = 5
			series.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series.lineWidth = 1
			
			series1.marker = HIMarker()
			series1.marker.states = HIStates()
			series1.marker.states.hover = HIHover()
			series1.marker.states.hover.enabled = false
			series1.data = data1
			series1.step = "center"
			//		series1.shadow = 5
			series1.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series1.lineWidth = 1
			
			series2.marker = HIMarker()
			series2.marker.states = HIStates()
			series2.marker.states.hover = HIHover()
			series2.marker.states.hover.enabled = false
			series2.data = data2
			series2.step = "center"
			//		series2.shadow = 5
			series2.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series2.lineWidth = 1
			
			series3.marker = HIMarker()
			series3.marker.states = HIStates()
			series3.marker.states.hover = HIHover()
			series3.marker.states.hover.enabled = false
			series3.data = data3
			series3.step = "center"
			//		series3.shadow = 5
			series3.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series3.lineWidth = 1
			
			series4.marker = HIMarker()
			series4.marker.states = HIStates()
			series4.marker.states.hover = HIHover()
			series4.marker.states.hover.enabled = false
			series4.data = data4
			series4.step = "center"
			//		series4.shadow = 5
			series4.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series4.lineWidth = 1
			
			series5.marker = HIMarker()
			series5.marker.states = HIStates()
			series5.marker.states.hover = HIHover()
			series5.marker.states.hover.enabled = false
			series5.data = data5
			series5.step = "center"
			//		series5.shadow = 5
			series5.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series5.lineWidth = 1
			
			series6.marker = HIMarker()
			series6.marker.states = HIStates()
			series6.marker.states.hover = HIHover()
			series6.marker.states.hover.enabled = false
			series6.data = data6
			series6.step = "center"
			//		series6.shadow = 5
			series6.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series6.lineWidth = 1
			
			series7.marker = HIMarker()
			series7.marker.states = HIStates()
			series7.marker.states.hover = HIHover()
			series7.marker.states.hover.enabled = false
			series7.data = data7
			series7.step = "center"
			//		series7.shadow = 5
			series7.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series7.lineWidth = 1
			
			series8.marker = HIMarker()
			series8.marker.states = HIStates()
			series8.marker.states.hover = HIHover()
			series8.marker.states.hover.enabled = false
			series8.data = data8
			series8.step = "center"
			//		series8.shadow = 5
			series8.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series8.lineWidth = 1
			
			series9.marker = HIMarker()
			series9.marker.states = HIStates()
			series9.marker.states.hover = HIHover()
			series9.marker.states.hover.enabled = false
			series9.data = data9
			series9.step = "center"
			//		series9.shadow = 5
			series9.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series9.lineWidth = 1
			
			series10.marker = HIMarker()
			series10.marker.states = HIStates()
			series10.marker.states.hover = HIHover()
			series10.marker.states.hover.enabled = false
			series10.data = data10
			series10.step = "center"
			//		series10.shadow = 5
			series10.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series10.lineWidth = 1
			
			series11.marker = HIMarker()
			series11.marker.states = HIStates()
			series11.marker.states.hover = HIHover()
			series11.marker.states.hover.enabled = false
			series11.data = data11
			series11.step = "center"
			//		series11.shadow = 5
			series11.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series11.lineWidth = 1
			
			series12.marker = HIMarker()
			series12.marker.states = HIStates()
			series12.marker.states.hover = HIHover()
			series12.marker.states.hover.enabled = false
			series12.data = data12
			series12.step = "center"
			//		series12.shadow = 5
			series12.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series12.lineWidth = 1
			
			series13.marker = HIMarker()
			series13.marker.states = HIStates()
			series13.marker.states.hover = HIHover()
			series13.marker.states.hover.enabled = false
			series13.data = data13
			series13.step = "center"
			//		series13.shadow = 5
			series13.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
				[0, "rgb(197 203 248)"],
				[0.35, "rgb(54 93 228)"],
				[0.7, "rgb(103 12 174)"],
				[1, "rgb(183 12 210)"]])
			series13.lineWidth = 1
			
			let time =  HITime()
			time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
			
			let options = HIOptions()
			options.chart = chart
			options.title = hititle
			options.subtitle = hisubtitle
			options.xAxis = [hixAxis]
			options.yAxis = [yAxis]
			options.tooltip = tooltip
			options.credits = credits
			options.exporting = exporting
			options.series = [series1, series2, series3, series4, series5, series6, series7, series8, series9, series10, series11, series12, series13]
			options.colors = uiColors
			options.plotOptions = plotoptions
			options.time = time
			options.legend = legend
			options.responsive = HIResponsive()
			
			let rule1 = HIRules()
			rule1.condition = HICondition()
			rule1.condition.maxWidth = 500
			rule1.chartOptions = [
				legend: [
					"layout": "horizontal",
					"align": "center",
					"verticalAlign": "bottom"
				]
			]
			
			options.responsive.rules = [HIRules()]
			
			self.chartView.isUserInteractionEnabled = false
			self.chartView.options = options
		}
	}
	
	func barChart(isVisible: Bool) {
		self.chartView.isHidden = !isVisible
		self.noChartView.isHidden = isVisible
	}
}
