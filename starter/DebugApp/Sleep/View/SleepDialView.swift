//
//  SleepDailView.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 27/10/21.
//

import Foundation
import SwiftUI
import Combine

struct SleepDialData {
    var sleepStartTime:Double
    var sleepEndTime:CGFloat
    var goalSleepTime:CGFloat
    var awakeTimeList:[(from:CGFloat,to:CGFloat)]
    var totalSleep:String
}

struct SleepDailView: View {
    var sleepDialData:SleepDialData
    
    func tick(at tick: Int) -> some View {
               VStack {
                   Rectangle()
                       .fill(Color(UIColor(named: "Tickmark") ?? UIColor.lightGray))
                    .cornerRadius(2)
                    .opacity(tick % 5 == 0 ? 1 : 0)
                       .frame(width: 4, height: tick % 5 == 0 ? 12 : 7)
                   Spacer()
           }.rotationEffect(Angle.degrees(Double(tick)/(60) * 360))
    }
    
    var body: some View {
        return ZStack {
            let startTimeError:CGFloat = 0.01
            let endTimeError:CGFloat = 0.01

            let sleepTime =  sleepDialData.sleepStartTime
            let adjSleepTime = sleepTime - 3.0
            let Sleepangle = adjSleepTime * 30
            
            //Tick marks
            ZStack{
                ForEach(0..<60) { tick in
                    self.tick(at: tick)
                }
            }.frame(width: 160, height: 160, alignment: .center)
           
            //Background circle
            Circle()
                    .stroke(lineWidth: 13.0)
                    .foregroundColor(Color(UIColor(named: "SleepBackground") ?? UIColor.darkGray))
            
            //Goal sleep cirle
            if(sleepDialData.goalSleepTime != 0.0){
                Circle()
                    .trim(from: 0.01, to: sleepDialData.goalSleepTime/12)
                        .stroke(style: StrokeStyle(lineWidth: 13.0, lineCap: .round, lineJoin: .round))
                        .foregroundColor(Color(UIColor(named: "SleepGoal") ?? UIColor.lightGray))
                        .rotationEffect(Angle(degrees: Sleepangle))
            }
           
            
            //Actual sleep cirle
            if(sleepDialData.sleepStartTime != 0.0){
                Circle()
                    .trim(from: 0.01, to: sleepDialData.sleepEndTime/12)
                        .stroke(style: StrokeStyle(lineWidth: 13.0, lineCap: .round, lineJoin: .round))
                        .foregroundColor(Color(UIColor(named: "SleepAchieved") ?? UIColor.primaryColor))
                        .rotationEffect(Angle(degrees: Sleepangle))
            }
            
            //Awake time cirles
            ForEach(0..<sleepDialData.awakeTimeList.count){ index in
                Circle()
                    .trim(from: (1.0/12*sleepDialData.awakeTimeList[index].from), to: (1.0/12*sleepDialData.awakeTimeList[index].to))
                    .stroke(style: StrokeStyle(lineWidth: 13.0, lineCap: .square, lineJoin: .round))
					.foregroundColor(Color(UIColor(red: 55, green: 55, blue: 125, alpha: 1)))
                    .rotationEffect(Angle(degrees: -90))
            }
            
            //Total Sleep text
            VStack {
                Text(sleepDialData.totalSleep)
                    .foregroundColor(Color.white)
                    .lineLimit(nil)
                    .font(.system(size: 20))
                    .animation(nil)
            }.frame(width: 100, height: 100, alignment: .center)
            
            }.frame(width: 200, height: 200, alignment: .center)
    }
}
