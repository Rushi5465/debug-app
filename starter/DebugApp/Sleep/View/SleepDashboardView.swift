//
//  SleepDashboardView.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 03/09/21.
//

import UIKit
import Highcharts

// swiftlint:disable all
class SleepDashboardView: UIView {

	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var valueLabel: MovanoLabel!
	@IBOutlet weak var bodyLabel: MovanoLabel!
	@IBOutlet weak var chartView: HIChartView!
	@IBOutlet weak var noChartView: UIView!
	
	override func prepareForInterfaceBuilder() {
		commonInit()
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		commonInit()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		
		commonInit()
	}
	
	func commonInit() {
		let viewFromXib = Bundle.main.loadNibNamed("SleepDashboardView", owner: self, options: nil)![0] as! UIView
		viewFromXib.frame = self.bounds
		addSubview(viewFromXib)
	}
	
	func loadChartView(model: ActivityChartModel, limitLineValues: [Int] = [], isSleep: Bool = false) {
		self.titleLabel.text = model.type
		
		self.valueLabel.text = model.value?.stringValue
		if let goal = model.goal {
			self.valueLabel.text?.append("/" + goal.stringValue)
		}
		if let unit = model.unit {
			self.valueLabel.attributedText = self.unitAttributeLabel(value: self.valueLabel.text ?? "0", unit: unit)
		}
		self.fetchActivityChart(chartData: model.chartData, limitLineValues: limitLineValues, isSleep: isSleep)
	}
	
	private func fetchActivityChart(chartData: ActivityChartData) {
		
		var xAxis = [TimeInterval]()
		xAxis = chartData.elements.map({ (element) -> TimeInterval in
			return element.date.timeIntervalSince1970
		})
		
		var data = [[Double?]]()
		
		for (pointIndex, each) in chartData.elements.enumerated() {
			data.append([xAxis[pointIndex] * 1000, each.value])
		}
		let values = [data]
		let chart = HIChart()
		chart.type = "column"
		chart.marginBottom = 70
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
		hixAxis.dateTimeLabelFormats.day = HIDay()
		hixAxis.dateTimeLabelFormats.day.main = "%l %p"
		hixAxis.dateTimeLabelFormats.hour = HIHour()
		hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
	
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		
		hixAxis.tickWidth = 2
		hixAxis.minRange =  NSNumber(value: 3600 * 24000)
		hixAxis.tickInterval = NSNumber(value: 3600 * 1000)
		
		if let elementDate = chartData.elements.first {
			hixAxis.min = NSNumber(value: (elementDate.date.startOfDay.timeIntervalSince1970) * 1000)
			hixAxis.max = NSNumber(value: (elementDate.date.endOfDay.timeIntervalSince1970) * 1000)
		}

		hixAxis.lineWidth = 0
		
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.overflow = "justify"
		yAxis.labels.style = HICSSObject()
		yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		yAxis.labels.style.fontSize = "12"
		yAxis.labels.style.color = "#a5a5b2"
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotoptions = HIPlotOptions()
		plotoptions.column = HIColumn()
		plotoptions.column.states = HIStates()
		plotoptions.column.states.inactive = HIInactive()
		plotoptions.column.states.inactive.opacity = NSNumber(value: true)
		hixAxis.labels.enabled = true
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		plotoptions.column.animation = HIAnimationOptionsObject()
		plotoptions.column.animation.duration = NSNumber(value: 0)
//		plotoptions.column.pointWidth = 300
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		var series = [HISeries]()
		let bar = HIColumn()
		bar.name = "Data"
		bar.data = values[0] as [Any]
		series.append(bar)
		
		let uiColors = [chartData.barColor.hexString]
		
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = series
		options.colors = uiColors
		options.plotOptions = plotoptions
		options.time = time
		options.legend = legend
		
		self.chartView.options = options
	}

	private func fetchActivityChart(chartData: ActivityChartData, limitLineValues: [Int], isSleep: Bool = false) {
		
		let data = chartData.elements.reduce(0.0, { partialResult, element in
			return partialResult + (element.value ?? 0)
		})
		if data == 0 {
			self.chartView.isHidden = true
			self.noChartView.isHidden = false
			
		} else {
			self.chartView.isHidden = false
			self.noChartView.isHidden = true
			var xAxis = [TimeInterval]()
			var limitLineData = [[[Double?]]]()
			xAxis = chartData.elements.map({ (element) -> TimeInterval in
				return element.date.timeIntervalSince1970
			})
			
			var data = [[Double?]]()
			
			for (pointIndex, each) in chartData.elements.enumerated() {
				data.append([xAxis[pointIndex] * 1000, each.value])
			}
			
			if limitLineValues.count > 0 {
				for x in 0..<limitLineValues.count {
					limitLineData.append([[Double?]]())
					for pointIndex in chartData.elements.indices {
						limitLineData[x].append(([xAxis[pointIndex] * 1000, Double(limitLineValues[x])]))
					}
				}
			}
			
			let values = [data]
			let chart = HIChart()
			chart.type = "line"
			chart.marginBottom = 70
			chart.backgroundColor = HIColor(uiColor: UIColor.clear)
			
			let hititle = HITitle()
			hititle.text = ""
			
			let hisubtitle = HISubtitle()
			hisubtitle.text = ""
			
			let hixAxis = HIXAxis()
			hixAxis.type = "datetime"
			hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
			
			hixAxis.dateTimeLabelFormats.day = HIDay()
			hixAxis.dateTimeLabelFormats.day.main = "%l %p"
			hixAxis.dateTimeLabelFormats.hour = HIHour()
			hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
			
			let hixAxisTitle = HITitle()
			hixAxisTitle.text = ""
			hixAxis.title = hixAxisTitle
			
			hixAxis.tickWidth = 0
			hixAxis.lineWidth = 0
			
			hixAxis.minRange = NSNumber(value: 3600 * 500)
			hixAxis.tickInterval = NSNumber(value: 3600 * 1000 * 1)
			
			hixAxis.lineWidth = 0
			
			hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
			hixAxis.labels = HILabels()
			hixAxis.labels.style = HICSSObject()
			hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			hixAxis.labels.style.fontSize = "10"
			hixAxis.labels.style.color = "#a5a5b2"
			
			//		if dataRangeType == .daily {
			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.isFirst || this.isLast) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + '' + ampm; return strTime } else { return '' } }")
			//		} else if dataRangeType == .weekly {
			//			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ let weekDaysArray = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']; if(this.isFirst || this.isLast) { let theDay = new Date(this.value).getDay(); return weekDaysArray[theDay] } else { return '' } }")
			//		}
			
			let yAxis = HIYAxis()
			yAxis.title = HITitle()
			yAxis.title.text = ""
			yAxis.labels = HILabels()
			yAxis.labels.overflow = "justify"
			yAxis.labels.style = HICSSObject()
			yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			yAxis.labels.style.fontSize = "12"
			yAxis.labels.style.color = "#a5a5b2"
			yAxis.visible = false
			//		yAxis.gridLineColor = HIColor(rgba: 0, green: 0, blue: 0, alpha: 0)
			//		if isSleep {
			//			yAxis.min = 0
			//			yAxis.max = 4
			//			yAxis.minTickInterval = 1
			//			yAxis.minRange = 4
			//
			//		}
			
			let tooltip = HITooltip()
			tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
			tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
			tooltip.footerFormat = "</table>"
			tooltip.shared = true
			tooltip.useHTML = true
			tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
			
			let legend = HILegend()
			legend.enabled = NSNumber(value: false)
			
			let plotoptions = HIPlotOptions()
			plotoptions.line = HILine()
			plotoptions.line.states = HIStates()
			plotoptions.line.states.inactive = HIInactive()
			plotoptions.line.states.inactive.opacity = NSNumber(value: true)
			plotoptions.line.animation = HIAnimationOptionsObject()
			plotoptions.line.animation.duration = NSNumber(value: 0)
			
			hixAxis.labels.enabled = true
			hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
			
			let credits = HICredits()
			credits.enabled = NSNumber(value: false)
			
			let exporting = HIExporting()
			exporting.enabled = false
			
			var finalSeries:[HISeries] = []
			let series = HISeries()
			let uiColors = [chartData.barColor.hexString]
			if limitLineData.count > 0 {
				for y in 0..<limitLineData.count {
					series.data = values[0] as [Any]
					series.lineWidth = 4.0
					series.threshold = NSNumber(value: limitLineValues[y])
					series.negativeColor = ((y == 1) ? HIColor(uiColor: .barPink) : HIColor(uiColor: .barPurple))
					finalSeries.append(series)
				}
			} else {
				series.data = values[0] as [Any]
				series.lineWidth = 4.0
				finalSeries.append(series)
			}
			
			let marker = HIMarker()
			marker.enabled = false
			series.marker = HIMarker()
			
			let time =  HITime()
			time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
			
			for y in 0..<limitLineData.count {
				let limitLineSeries = HISeries()
				limitLineSeries.data = limitLineData[y]
				limitLineSeries.dashStyle = "shortdot"
				limitLineSeries.color = HIColor(uiColor: UIColor.textColor)
				limitLineSeries.marker = marker
				
				finalSeries.append(limitLineSeries)
			}
			
			let options = HIOptions()
			options.chart = chart
			options.title = hititle
			options.subtitle = hisubtitle
			options.xAxis = [hixAxis]
			options.yAxis = [yAxis]
			options.tooltip = tooltip
			options.credits = credits
			options.exporting = exporting
			options.series = finalSeries
			options.colors = uiColors
			options.plotOptions = plotoptions
			options.time = time
			options.legend = legend
			
			self.chartView.options = options
		}
	}
	
	func unitAttributeLabel(value: String, unit: String) -> NSAttributedString {
		let str = NSMutableAttributedString()
		
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.semiBold(withSize: 16), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textColor]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.regular(withSize: 12), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textColor]
		
		str.append(NSMutableAttributedString(string: value + " ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
		str.append(NSMutableAttributedString(string: unit, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		
		return str
		
	}
}
