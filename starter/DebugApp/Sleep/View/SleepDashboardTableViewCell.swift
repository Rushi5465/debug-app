//
//  SleepDashboardTableViewCell.swift
//  BloodPressureApp
//
//  Created by Rushikant on 26/08/21.
//

import UIKit
import Highcharts

class SleepDashboardTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var valueLabel: MovanoLabel!
	@IBOutlet weak var subTitleLabel: MovanoLabel!
	@IBOutlet weak var chartView: HIChartView!
	@IBOutlet weak var unitLabel: MovanoLabel!
	@IBOutlet weak var tagButton: MovanoButton!
	@IBOutlet weak var noChartView: UIView!
	
	let data = [
		[1629267364000.0, 50.0],
		[1629263764000.0, 400.0],
		[1629260164000.0, 543.0],
		[1629256564000.0, 167.0],
		[1629252964000.0, 212.0],
		[1629249364000.0, 700.0],
		[1629245764000.0, 321.0],
		[1629242164000.0, 66.0],
		[1629238564000.0, 544.0],
		[1629234964000.0, 233.0],
		[1629231364000.0, 432.0],
		[1629227764000.0, 123.0]
	]
	
	var lineDataForSleep = [
		[1629267364000.0, 50.0]
	]
	
	let limitLineData = [
		[1629267364000.0, 400.0],
		[1629263764000.0, 400.0],
		[1629260164000.0, 400.0],
		[1629256564000.0, 400.0],
		[1629252964000.0, 400.0],
		[1629249364000.0, 400.0],
		[1629245764000.0, 400.0],
		[1629242164000.0, 400.0],
		[1629238564000.0, 400.0],
		[1629234964000.0, 400.0],
		[1629231364000.0, 400.0],
		[1629227764000.0, 400.0]
	]
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
	
	override func layoutSubviews() {
		super.layoutSubviews()
		let margins = UIEdgeInsets(top: 0, left: 0, bottom: 0.5, right: 0)
		contentView.frame = contentView.frame.inset(by: margins)
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func changeGraphDataForLine() {
		var increaseOrDecrease = +100000.0
		for j in 0..<(data.count-1) {
			if Int(data[j+1].last!) > Int(data[j].last!) {
				increaseOrDecrease = +100000.0
			} else {
				increaseOrDecrease = -100000.0
			}
			
			for i in stride(from: (data[j].first)! as Double, to: data[j+1].first!, by: Double(increaseOrDecrease) as Double) {
				lineDataForSleep.append([Double(i), data[j].last!])
			}
		}
	}
    
	func fetchActivityChart() {
		let chart = HIChart()
		chart.type = "line"
		chart.marginLeft = 70
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
		let options = HIOptions()
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
		hixAxis.dateTimeLabelFormats.day = HIDay()
		hixAxis.dateTimeLabelFormats.day.main = "%e %b"
	
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		
		hixAxis.tickWidth = 2
		hixAxis.minRange =  NSNumber(value: 3600 * 24000)
		hixAxis.tickInterval = NSNumber(value: 3600 * 1000)
		
		hixAxis.min = NSNumber(value: (("18/08/2021".toDate(format: "dd/MM/yyyy")?.startOfDay.timeIntervalSince1970)!) * 1000)
		hixAxis.max = NSNumber(value: (("18/08/2021".toDate(format: "dd/MM/yyyy")?.endOfDay.timeIntervalSince1970)!) * 1000)

		hixAxis.lineWidth = 0
		
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotoptions = HIPlotOptions()
		plotoptions.line = HILine()
		plotoptions.line.states = HIStates()
		plotoptions.line.states.inactive = HIInactive()
		plotoptions.line.states.inactive.opacity = NSNumber(value: true)
		
		hixAxis.labels.enabled = true
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
//		plotoptions.column.pointWidth = 300
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		let installation = HISeries()
		installation.name = ""
		installation.showInLegend = 0
		installation.data = data
		installation.threshold = 400
		installation.negativeColor = HIColor(uiColor: UIColor.barPink)
		plotoptions.series = installation
		
		let limitLineSeries = HISeries()
		limitLineSeries.data = limitLineData
		limitLineSeries.dashStyle = "shortdot"
		limitLineSeries.color = HIColor(uiColor: UIColor.textColor)
		
		let marker = HIMarker()
		marker.enabledThreshold = 1000 // For removing caps from points
		plotoptions.series.marker = marker
		
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		options.series = [installation, limitLineSeries]
		options.title = hititle
		options.subtitle = hisubtitle
		options.chart = chart
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.plotOptions = plotoptions
		options.time = time
		options.legend = legend
		var colors: [String] = []
		for _ in 0..<data.count {
			colors.append(UIColor.barPurple.hexString)
		}
		options.colors = colors
		chartView.options = options
	}
	
	func fetchSleepBarChart() {
		let values = [data]
		let chart = HIChart()
		chart.type = "column"
		chart.marginBottom = 70
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
		hixAxis.dateTimeLabelFormats.day = HIDay()
		hixAxis.dateTimeLabelFormats.day.main = "%e %b"
	
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		
		hixAxis.tickWidth = 2
		hixAxis.minRange =  NSNumber(value: 3600 * 24000)
		hixAxis.tickInterval = NSNumber(value: 3600 * 1000)
		
		hixAxis.min = NSNumber(value: (("18/08/2021".toDate(format: "dd/MM/yyyy")?.startOfDay.timeIntervalSince1970)!) * 1000)
		hixAxis.max = NSNumber(value: (("18/08/2021".toDate(format: "dd/MM/yyyy")?.endOfDay.timeIntervalSince1970)!) * 1000)

		hixAxis.lineWidth = 0
		
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.overflow = "justify"
		yAxis.labels.style = HICSSObject()
		yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		yAxis.labels.style.fontSize = "12"
		yAxis.labels.style.color = "#a5a5b2"
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotoptions = HIPlotOptions()
		plotoptions.column = HIColumn()
		plotoptions.column.states = HIStates()
		plotoptions.column.states.inactive = HIInactive()
		plotoptions.column.states.inactive.opacity = NSNumber(value: true)
//		plotoptions.column.pointWidth = 25
		hixAxis.labels.enabled = true
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		var series = [HISeries]()
		let bar = HIColumn()
		bar.name = "Data"
		bar.borderWidth = 0
		bar.data = values[0] as [Any]
		series.append(bar)
		
		var colors: [String] = []
		for _ in 0..<data.count {
			colors.append(#colorLiteral(red: 0.38540982634, green: 0.04054072499, blue: 0.1968257725, alpha: 1).hexString)
		}
		
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		// Function for sleep line data
		changeGraphDataForLine()
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = series
		options.colors = colors
		options.plotOptions = plotoptions
		options.time = time
		options.legend = legend
		
		self.chartView.options = options
	}
}
