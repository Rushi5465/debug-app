//
//  CustomSegmentView.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 05/10/21.
//

import UIKit

protocol CustomSegmentViewDelegate: AnyObject {
	func segmentChanged(to indexpath: IndexPath)
}

class CustomSegmentView: UIView {

	@IBOutlet weak var segmentCollection: UICollectionView!
	weak var delegate: CustomSegmentViewDelegate?
	
	var segmentList = [String]() {
		didSet {
			self.segmentCollection.reloadData()
		}
	}
	
	var selectedSegment: Int = 0 {
		didSet {
			self.selectSegment(at: selectedSegment)
		}
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		commonInit()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		
		commonInit()
	}
	
	func commonInit() {
		let viewFromXib = Bundle.main.loadNibNamed("CustomSegmentView", owner: self, options: nil)![0] as! UIView
		viewFromXib.frame = self.bounds
		addSubview(viewFromXib)
		
		registerCollectionCell()
		assignDelegates()
		DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1)) {
			self.selectedSegment = 0
		}
	}
	
	override func prepareForInterfaceBuilder() {
		commonInit()
	}
	
	private func registerCollectionCell() {
		let parameterCell = UINib.init(nibName: "SegmentCollectionViewCell", bundle: nil)
		self.segmentCollection.register(parameterCell, forCellWithReuseIdentifier: SegmentCollectionViewCell.IDENTIFIER)
	}
	
	private func assignDelegates() {
		self.segmentCollection.delegate = self
		self.segmentCollection.dataSource = self
	}
	
	private func selectSegment(at index: Int) {
		let indexpath = IndexPath(item: index, section: 0)
		self.segmentCollection.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
		self.delegate?.segmentChanged(to: indexpath)
	}
}

extension CustomSegmentView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: 130, height: 40)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return UIEdgeInsets(top: 0, left: (UIScreen.main.bounds.width / 2) - 20, bottom: 0, right:  (UIScreen.main.bounds.width / 2) - 20)
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		self.selectedSegment = indexPath.item
	}
}

extension CustomSegmentView: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.segmentList.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell = SegmentCollectionViewCell.dequeueReusableCell(collectionView: collectionView, indexPath: indexPath)
		
		let item = self.segmentList[indexPath.row]
		cell.titleLabel.text = item
		
		return cell
	}
}
