//
//  HistoryListPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/08/21.
//

import Foundation

class HistoryListPresenter: HistoryListPresenterProtocol {
	
	private weak var delegate: HistoryDataDelegate?
	private var exerciseService: ExerciseDataWebServiceProtocol
	private var pauseService: PauseDataWebServiceProtocol
	private var bpService: BloodPressureWebServiceProtocol
	
	var exerciseSessions = [HistorySection]()
	var takeAPauseSessions = [HistorySection]()
	var sectionList = [HistorySection]()
	
	required init(exerciseService: ExerciseDataWebServiceProtocol, pauseService: PauseDataWebServiceProtocol, bpService: BloodPressureWebServiceProtocol, delegate: HistoryDataDelegate) {
		self.exerciseService = exerciseService
		self.pauseService = pauseService
		self.bpService = bpService
		self.delegate = delegate
	}
	
	func fetchExerciseData(range: DateRange) {
		self.exerciseService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if var item = item {
				
				var sectionList = [HistorySection]()
				item = item.sorted { $0.endTime < $1.endTime }
				let dateArray = Date.dates(from: range.start, to: range.end, interval: 1, component: .day)
				for each in dateArray {
					var segregatedDateList = item.filter { (account) -> Bool in
						return account.endTime.dateToString(format: "dd/MM/yyyy") == each.dateToString(format: "dd/MM/yyyy")
					}
					
					segregatedDateList = segregatedDateList.sorted { $0.endTime > $1.endTime }
					if !segregatedDateList.isEmpty {
						sectionList.append(HistorySection(date: each.startOfDay, data: segregatedDateList))
					}
				}
				sectionList = sectionList.sorted { $0.date > $1.date }
				
				self?.delegate?.historyData(list: sectionList)
			}
		}
	}
	
	func fetchPauseData(range: DateRange) {
		
		self.pauseService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if var item = item {
				var sectionList = [HistorySection]()
				item = item.sorted { $0.endTime < $1.endTime }
				let dateArray = Date.dates(from: range.start, to: range.end, interval: 1, component: .day)
				for each in dateArray {
					var segregatedDateList = item.filter { (account) -> Bool in
						return account.endTime.dateToString(format: "dd/MM/yyyy") == each.dateToString(format: "dd/MM/yyyy")
					}
					
					segregatedDateList = segregatedDateList.sorted { $0.endTime > $1.endTime }
					if !segregatedDateList.isEmpty {
						sectionList.append(HistorySection(date: each.startOfDay, data: segregatedDateList))
					}
				}
				sectionList = sectionList.sorted { $0.date > $1.date }
				
				self?.delegate?.historyData(list: sectionList)
			}
		}
	}
	
	func fetchBloodPressureData(range: DateRange) {
		// Exercise sessions
		self.bpService.listBloodPressures(range: range.intervalRange) { [weak self] (item, error) in
			if var item = item {
				var sectionList = [HistorySection]()
				item = item.sorted { $0.date < $1.date }
				let dateArray = Date.dates(from: range.start, to: range.end, interval: 1, component: .day)
				for each in dateArray {
					var segregatedDateList = item.filter { (account) -> Bool in
						return account.date.dateToString(format: "dd/MM/yyyy") == each.dateToString(format: "dd/MM/yyyy")
					}
					
					segregatedDateList = segregatedDateList.sorted { $0.date > $1.date }
					if !segregatedDateList.isEmpty {
						sectionList.append(HistorySection(date: each.startOfDay, data: segregatedDateList))
					}
				}
				sectionList = sectionList.sorted { $0.date > $1.date }
				
				self?.delegate?.historyData(list: sectionList)
			}
	}
	}
	
	func fetchBloodPressureDataAll(range: DateRange) {
		// Exercise sessions
		self.exerciseService.fetch(by: range.intervalRange) { [weak self] (exerciseitem, exerciseerror) in
			
			if var item = exerciseitem {
				item = item.sorted { $0.endTime < $1.endTime }
				let dateArray = Date.dates(from: range.start, to: range.end, interval: 1, component: .day)
				for each in dateArray {
					var segregatedDateList = item.filter { (account) -> Bool in
						return account.endTime.dateToString(format: "dd/MM/yyyy") == each.dateToString(format: "dd/MM/yyyy")
					}
					
					segregatedDateList = segregatedDateList.sorted { $0.endTime > $1.endTime }
					if !segregatedDateList.isEmpty {
						self?.exerciseSessions.append(HistorySection(date: each.startOfDay, data: segregatedDateList))
					}
				}
				self?.exerciseSessions = (self?.exerciseSessions.sorted { $0.date > $1.date }) ?? [HistorySection]()
			}
			
			self?.sectionList = self?.exerciseSessions ?? [HistorySection]()
			// Take a pause sessions
			self?.pauseService.fetch(by: range.intervalRange) { [weak self] (pauseitem, error) in
				if var item = pauseitem {
					item = item.sorted { $0.endTime < $1.endTime }
					let dateArray = Date.dates(from: range.start, to: range.end, interval: 1, component: .day)
					for each in dateArray {
						var segregatedDateList = item.filter { (account) -> Bool in
							return account.endTime.dateToString(format: "dd/MM/yyyy") == each.dateToString(format: "dd/MM/yyyy")
						}
						
						segregatedDateList = segregatedDateList.sorted { $0.endTime > $1.endTime }
						if !segregatedDateList.isEmpty {
							self?.takeAPauseSessions.append(HistorySection(date: each.startOfDay, data: segregatedDateList))
							self?.sectionList.append(HistorySection(date: each.startOfDay, data: segregatedDateList))
						}
					}
					self?.takeAPauseSessions = (self?.takeAPauseSessions.sorted { $0.date > $1.date }) ?? [HistorySection]()
				}
				
				self?.sectionList = (self?.sectionList.sorted { $0.date > $1.date }) ?? [HistorySection]()
				var newSectionList = self?.sectionList
				var repeatedArrayList: [Int] = []
				var historyData: [Any] = []
//
//				if ((self?.sectionList.count)!) > 1 {
//					for x in 0..<((self?.sectionList.count)! - 2) {
//						for m in 1..<((self?.sectionList.count)! - 1) {
//							if self?.sectionList[x].date == self?.sectionList[m].date {
//								historyData.append(self?.sectionList[m].data)
//								repeatedArrayList.append(m)
//							}
//						}
//					}
//				}
				
				if ((self?.sectionList.count) != 0) {
					for l in 0..<((self?.sectionList.count)! - 1) {
						if self?.sectionList[l].date == self?.sectionList[l + 1].date {
							repeatedArrayList.append(l)
						}
					}
				}
				
				for z in 0..<repeatedArrayList.count {
					var data = self?.sectionList[(repeatedArrayList[z])].data
					let newData = self?.sectionList[(repeatedArrayList[z] + 1)].data
					data?.append(newData!)
					self?.sectionList[(repeatedArrayList[z]) + 1].data = data!
				}
				
				for k in 0..<repeatedArrayList.count {
//					if k < repeatedArrayList.count {
						self?.sectionList.remove(at: (repeatedArrayList[k]))
						repeatedArrayList = repeatedArrayList.map { $0 - 1 }
//					}
				}
				
				self?.sectionList = (self?.sectionList.sorted { $0.date > $1.date }) ?? [HistorySection]()
				self?.delegate?.historyData(list: (self?.sectionList ?? [HistorySection]()))
			}
			
		}
	}
}
