//
//  ActivityPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 18/08/21.
//

import Foundation

// swiftlint:disable all
class ActivityPresenter: ActivityPresenterProtocol {
	
	private weak var delegate: ActivityViewDelegate?
	private var stepCountService: StepCountWebServiceProtocol
	private var exerciseService: ExerciseDataWebServiceProtocol
	private var pauseReadingService: PauseDataWebServiceProtocol
	private var sleepService: SleepWebServiceProtocol
	
	required init(stepCountService: StepCountWebServiceProtocol, exerciseService: ExerciseDataWebServiceProtocol, pauseReadingService: PauseDataWebServiceProtocol, sleepService: SleepWebServiceProtocol, delegate: ActivityViewDelegate) {
		self.stepCountService = stepCountService
		self.exerciseService = exerciseService
		self.pauseReadingService = pauseReadingService
		self.sleepService = sleepService

		self.delegate = delegate
	}
	
	func fetchStepCount(range: DateRange) {
		
		self.stepCountService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				let totalStepCount = item.reduce(0) { (result: Int, nextItem: CoreDataStepCount) -> Int in
					return result + nextItem.value.intValue
				}
				
				let totalCalorie = item.reduce(0) { (result: Double, nextItem: CoreDataStepCount) -> Double in
					return result + (nextItem.calories?.doubleValue ?? 0)
				}
				
				let calorieElements = CoreDataStepCount.fetchCalorieChartData(dateRange: range)
				self?.delegate?.calorieCountChart(chartElements: calorieElements, totalCalorie: totalCalorie, totalStepCount: totalStepCount)
			}
			
			if let error = error {
				self?.delegate?.removeActivityIndicator()
			}
		}
	}
    
	func fetchActivityIntensityData(range: DateRange,completionHandler: @escaping ([CoreDataSleepStage]?, MovanoError?) -> Void){
		self.stepCountService.fetchActivtyIntensity(by: range.intervalRange){ [weak self] (item, error) in
            if item != nil {
                let intensityElements = CoreDataActivity.fetchActivityChartData(dateRange: range, isAverage: true)
				self?.delegate?.activityIntensityChart(chartElements: intensityElements)
				completionHandler(nil,nil)
			} else {
                completionHandler(nil,nil)
			}
		}
	}
	
	func fetchExerciseData(range: DateRange) {
		
		self.exerciseService.fetch(by: range.intervalRange) { [weak self] (item, error) in
            var exerciseList:[ExerciseSessionDataModel] = []
			if let item = item {
                let dataList = item.sorted { $0.startTime < $1.startTime }
				var exerciseTime = 0
                    for each in dataList {
						let sessionTime = each.startTime.dateToReadableTime()
                        let sessionStartTime = each.startTime
//							.dateToString(format: "yyyy-MM-dd'T'HH:mm:ss Z")
						let sessionEndTime = each.endTime
//							.dateToString(format: "yyyy-MM-dd'T'HH:mm:ss Z")
                        exerciseTime = Date.timeIntervalBetween(initialDate: each.startTime, finalDate: each.endTime)
                        let (hour,minute,second) = Date.secondsToHoursMinutesSeconds(seconds: exerciseTime)
                           
                        let stepCount = each.stepCount?.stringValue ?? "-"
                        let calBurned = each.caloriesBurnt?.stringValue ?? "-"
                        let skinTemp = each.skinTemperature?.stringValue ?? "-"
						let oxygen = each.oxygen?.stringValue ?? "-"
                        let distance = each.distanceCovered?.stringValue ?? "-"
                        let breathingRate = each.breathingRate?.stringValue ?? "-"
                        
						exerciseList.append(ExerciseSessionDataModel(sessionTime: sessionTime, stepCount: stepCount, sessionTimeInHrs: String(hour), sessionTimeInMinutes:String(format: "%02d", minute), sessionTimeInSeconds: String(second), breathingRate: breathingRate, caloriesBurned: calBurned, skinTemp: skinTemp, distance: distance, oxygen: oxygen,startTime: sessionStartTime, endTime: sessionEndTime))
                        //let summary = DateComponentsFormatter().difference(from: TimeInterval(exerciseTime))
                        
                       
                    }
                
            }
            self?.delegate?.exerciseSummary(exerciseList: exerciseList)

		}
	}
	
	func fetchPauseReadingData(range: DateRange) {
		
		self.pauseReadingService.fetch(by: range.intervalRange) { [weak self] (item, error) in
            var pauseDataList:[TakePauseSessionDataModel] = []
			if let item = item {
				let dataList = item.sorted { $0.startTime < $1.startTime }
                    for eachItem in dataList{
                        let systolicBp = eachItem.systolic?.stringValue ?? "-"
                        let diastolicBp = eachItem.diastolic?.stringValue ?? "-"
                        let heartRate = eachItem.pulseRate?.stringValue ?? "-"
                        let sessionTime = eachItem.startTime.dateToReadableTime()
                        let skinTemp = eachItem.skinTemperature?.stringValue ?? "-"
						let breathingRate = eachItem.breathingRate?.stringValue ?? "-"
                        pauseDataList.append(TakePauseSessionDataModel(sessionTime: sessionTime, heartRateValue: heartRate, bloodPressureValue: "\(systolicBp)/\(diastolicBp)",skinTemp:skinTemp,breathingRate:breathingRate))
                    }
            }
            self?.delegate?.pauseData(reading: pauseDataList)

		}
	}
	
	func fetchSleepTimeData(forDate: Date) {
		self.sleepService.fetchAverageData(forDate:forDate.dateToString(format: DateFormat.serverDateFormat)) { sleepRepo, error in
			if let error = error {
				self.delegate?.afterSleepDataResponseFetched(result: .failure(error))
				return
			}
			
			if sleepRepo != nil {
//				self.delegate?.sleepDataTiming(sleepStartTime: sleepRepo.sleep_start_time, sleepEndTime: sleepRepo.sleep_end_time)
			}
		}
	}
	
	func fetchAllActivityData(range: DateRange) {
		
		self.stepCountService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				let totalStepCount = item.reduce(0) { (result: Int, nextItem: CoreDataStepCount) -> Int in
					return result + nextItem.value.intValue
				}
				
				let totalCalorie = item.reduce(0) { (result: Double, nextItem: CoreDataStepCount) -> Double in
					return result + (nextItem.calories?.doubleValue ?? 0)
				}
				
				let calorieElements = CoreDataStepCount.fetchCalorieChartData(dateRange: range)
				self?.delegate?.calorieCountChart(chartElements: calorieElements, totalCalorie: totalCalorie, totalStepCount: totalStepCount)
			}
			
			self?.exerciseService.fetch(by: range.intervalRange) { [weak self] (item, error) in
				
				var exerciseList:[ExerciseSessionDataModel] = []
				if let item = item {
					let dataList = item.sorted { $0.startTime < $1.startTime }
					var exerciseTime = 0
					for each in dataList {
						let sessionTime = each.startTime.dateToReadableTime()
						let sessionStartTime = each.startTime
						//							.dateToString(format: "yyyy-MM-dd'T'HH:mm:ss Z")
						let sessionEndTime = each.endTime
						//							.dateToString(format: "yyyy-MM-dd'T'HH:mm:ss Z")
						exerciseTime = Date.timeIntervalBetween(initialDate: each.startTime, finalDate: each.endTime)
						let (hour, minute, second) = Date.secondsToHoursMinutesSeconds(seconds: exerciseTime)
						
						let stepCount = each.stepCount?.stringValue ?? "-"
						let calBurned = each.caloriesBurnt?.stringValue ?? "-"
                        let tempValue1 = (Utility.getTempVariation(temperature: (each.skinTemperature?.doubleValue ?? 0.0)) * 100) / 100
                        let skinTemp = String(format: "%.2f", (each.skinTemperature?.doubleValue ?? 0.0))
						let oxygen = each.oxygen?.stringValue ?? "-"
						let distance = each.distanceCovered?.stringValue ?? "-"
						let breathingRate = each.breathingRate?.stringValue ?? "-"
						
						exerciseList.append(ExerciseSessionDataModel(sessionTime: sessionTime, stepCount: stepCount, sessionTimeInHrs: String(hour), sessionTimeInMinutes:String(format: "%02d", minute), sessionTimeInSeconds: String(second), breathingRate: breathingRate, caloriesBurned: calBurned, skinTemp: skinTemp, distance: distance, oxygen: oxygen, startTime: sessionStartTime, endTime: sessionEndTime))
						//let summary = DateComponentsFormatter().difference(from: TimeInterval(exerciseTime))
					}
					
				}
				self?.delegate?.exerciseSummary(exerciseList: exerciseList)
                let dayAfter = range.end.dateAfter(days: 1)
                self?.sleepService.fetchAverageData(forDate:dayAfter.dateToString(format: DateFormat.serverDateFormat)) {[weak self] sleepRepo, error in
                    if let error = error {
                        self?.delegate?.afterSleepDataResponseFetched(result: .failure(error))
                        self?.delegate?.removeActivityIndicator()
                    }else{
                        self?.delegate?.fetchIntensityData()
                    }
                    
                }
			}
		}
	}
}
