//
//  ActivityIntensityDetailsPresenter.swift
//  BloodPressureApp
//
//  Created by Rushikant on 26/11/21.
//

import Foundation

// swiftlint:disable all
class ActivityIntensityDetailsPresenter: ActivityIntensityDetailsPresenterProtocol {
	
	private weak var delegate: ActivityIntensityDetailsDelegate?
	private var pulseRateService: PulseRateWebServiceProtocol
	private var stepCountService: StepCountWebServiceProtocol
	
	required init(pulseRateService: PulseRateWebServiceProtocol, stepCountService: StepCountWebServiceProtocol, delegate: ActivityIntensityDetailsDelegate) {
		self.pulseRateService = pulseRateService
		self.stepCountService = stepCountService
		self.delegate = delegate
	}
	
	func fetchPulseRateData(timeStampRange: Range<Double>, customDateRange: DateRange) {
		self.pulseRateService.fetch(by: timeStampRange) { [weak self] (item, error) in
			if let error = error {
				self?.delegate?.fetchPulseRateData(result: .failure(error), customDateRange: customDateRange)
				return
			}
			
			if let item = item {
				self?.delegate?.fetchPulseRateData(result: .success(item), customDateRange: customDateRange)
			}
		}
		
	}
	
	func fetchIntensityData(timeStampRange: Range<Double>, customDateRange: DateRange) {
		self.stepCountService.fetchActivtyIntensity(by: timeStampRange) { [weak self] (item, error) in
			if let error = error {
				self?.delegate?.fetchIntensityData(result: .failure(error), customDateRange: customDateRange)
				return
			}
			
			if let item = item {
				self?.delegate?.fetchIntensityData(result: .success(item), customDateRange: customDateRange)
			}
		}
		
	}
	
	func fetchWeeklyData(weekStartdate: String, weekEndDate: String,activityChartSelected:Bool) {
		self.pulseRateService.fetchIntensityPulseData(weekStartdate: weekStartdate, weekEndDate: weekEndDate) { [weak self] (item, error) in
			var elementList = [ChartElement]()
			if let item = item {
				
				if activityChartSelected {
					if item.count == 7 {
						for x in 0..<item.count {
							if item[x].avgActivityIntensity == 0 {
								elementList.append(ChartElement(date: Date(timeIntervalSince1970: item[x].date.timeIntervalSince1970 ?? 0), value: nil))
							} else {
								elementList.append(ChartElement(date: Date(timeIntervalSince1970: item[x].date.timeIntervalSince1970 ?? 0), value: Double(item[x].avgActivityIntensity)))
							}
						}
					} else {
						let dateArray = Date.dates(from: Date(timeIntervalSince1970: weekStartdate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0), to: Date(timeIntervalSince1970: weekEndDate.toDate(format: DateFormat.serverDateFormat)?.timeIntervalSince1970 ?? 0), interval: 1, component: .day)
						
						var tempChartElemnt = [ChartElement]()
						for x in 0..<dateArray.count {
							tempChartElemnt.append(ChartElement(date: dateArray[x], value: nil))
						}
						
						for y in 0..<item.count {
							let day = Date(timeIntervalSince1970: item[y].date.timeIntervalSince1970 ?? 0).dayNumberOfWeek() ?? 0
							tempChartElemnt[day - 1] = ChartElement(date: tempChartElemnt[day - 1].date, value: Double(item[y].avgActivityIntensity))
						}
						elementList = tempChartElemnt
					}
				} else {
					for x in 0..<item.count {
						if item[x].avgActivityIntensity == 0 {
							elementList.append(ChartElement(date: Date(timeIntervalSince1970: item[x].date.timeIntervalSince1970 ?? 0), value: nil))
						} else {
						elementList.append(ChartElement(date: Date(timeIntervalSince1970: item[x].date.timeIntervalSince1970 ?? 0), value: Double(item[x].avgPulseRate)))
						}
					}
				}
				self?.delegate?.loadWeeklyChart(chartElements: elementList)
			}
		}
	}
}
