//
//  ActivityDetailPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/08/21.
//

import UIKit
import Highcharts

// swiftlint:disable all
class ActivityDetailPresenter: ActivityDetailPresenterProtocol {
	
	private weak var delegate: ActivityDetailViewDelegate?
	private weak var chartDelegate: ChartDelegate?
	private var stepCountService: StepCountWebServiceProtocol
	
	required init(stepCountService: StepCountWebServiceProtocol, chartDelegate: ChartDelegate, delegate: ActivityDetailViewDelegate) {
		self.stepCountService = stepCountService
		self.chartDelegate = chartDelegate
		self.delegate = delegate
	}
	
	func fetchData(range: DateRange, elementColor: UIColor, dataType: ActivityDataType, currentAwakeStartDate: Date, currentAwakeEndDate: Date, isUpdateRequired: Bool, isCameFromDashboard: Bool = false) {
		self.stepCountService.fetch(by: range.intervalRange) {  [weak self] (item, error) in
			if item != nil {
				self?.loadData(range: range, elementColor: elementColor, dataType: dataType, currentAwakeStartDate: currentAwakeStartDate, currentAwakeEndDate: currentAwakeEndDate, isUpdateRequired: isUpdateRequired, isCameFromDashboard: isCameFromDashboard)
            }else{
                self?.delegate?.errorOccured()
            }
		}
	}
	
	private func fetchGoals(for type: ActivityDataType) {
		let goal = UserDefault.shared.goals
		if type == .stepCount {
			self.delegate?.goal(type: type, targetValue: goal.stepCountGoal.value as! Int)
		} else if type == .calories {
			self.delegate?.goal(type: type, targetValue: goal.caloriesBurntGoal.value as! Int)
		}
	}
	
	private func fetchDistance(data: [CoreDataStepCount]) {
		let totalDistance = data.reduce(0) { (result: Double, nextItem: CoreDataStepCount) -> Double in
			return result + (nextItem.distance?.doubleValue ?? 0)
		}
		self.delegate?.distance(value: totalDistance, unit: "miles")
	}
	
	private func fetchTotalCount(data: [CoreDataStepCount], dataType: ActivityDataType) {
		var totalCount = Double()
		if dataType == .stepCount {
			totalCount = data.reduce(0) { (result: Double, nextItem: CoreDataStepCount) -> Double in
				return result + Double(nextItem.value)
			}
		} else {
			totalCount = data.reduce(0) { (result: Double, nextItem: CoreDataStepCount) -> Double in
				return result + Double((nextItem.calories ?? 0))
			}
		}
		let finalTotalCalories = ((totalCount * 10).rounded() / 10)
		self.delegate?.totalCount(value: finalTotalCalories, unit: nil)
	}
	
	private func fetchCaloriesCount(data: [CoreDataStepCount]) {
		let totalCalories = data.reduce(0) { (result: Double, nextItem: CoreDataStepCount) -> Double in
			return result + Double((nextItem.calories ?? 0))
		}
		let finalTotalCalories = ((totalCalories * 10).rounded() / 10)
		if(finalTotalCalories > 1000){
			let formatter = NumberFormatter()
			formatter.maximumFractionDigits = 1
			formatter.roundingMode = .down
			let totalCalorieInString = formatter.string(from: NSNumber.init(value: Double(finalTotalCalories)))
			self.delegate?.caloriesCount(value: (totalCalorieInString?.toDouble?.rounded() ?? 0), unit: "Cal")
		}else{
			self.delegate?.caloriesCount(value: finalTotalCalories, unit: "Cal")
		}
	}
	
	private func loadData(range: DateRange, elementColor: UIColor, dataType: ActivityDataType, currentAwakeStartDate: Date, currentAwakeEndDate: Date, isUpdateRequired: Bool, isCameFromDashboard: Bool = false) {
		
		self.fetchGoals(for: dataType)
		
		let data = CoreDataStepCount.fetch(for: range.dateRange)
		if isUpdateRequired {
			self.fetchCaloriesCount(data: data)
			self.fetchTotalCount(data: data, dataType: dataType)
			self.fetchDistance(data: data)
		}
		var elements = [ChartElement]()
		var unit: String?
		if dataType == .stepCount {
			elements = CoreDataStepCount.fetchStepCountChartData(dateRange: range)
			unit = ""
		} else {
			elements = CoreDataStepCount.fetchCalorieChartData(dateRange: range)
			unit = "Cal"
		}
		if !isCameFromDashboard {
			var startOfRange: Date?
			var endOfRange: Date?
			var newelementData = [ChartElement]()
			for x in 0..<elements.count {
				if elements[x].value != nil {
					newelementData.append(elements[x])
				}
			}
			
			if newelementData[0].date < currentAwakeStartDate {
				startOfRange = newelementData[0].date
			} else {
				startOfRange = currentAwakeStartDate
			}
			
			if currentAwakeEndDate.startOfDay == Date().startOfDay {
				endOfRange = Date()
			} else {
				if newelementData.last!.date > currentAwakeEndDate {
					endOfRange = newelementData.last?.date
				} else {
					endOfRange = currentAwakeEndDate
				}
			}
			
			if dataType == .stepCount {
				if range.type == .weekly {
					elements = CoreDataStepCount.fetchStepCountChartData(dateRange: range)
				} else {
					elements = CoreDataStepCount.fetchStepCountChartData(dateRange: DateRange(start: startOfRange!, end: endOfRange!, type: range.type))
				}
				unit = ""
			} else {
				if range.type == .weekly {
					elements = CoreDataStepCount.fetchCalorieChartData(dateRange: range)
				} else {
					elements = CoreDataStepCount.fetchCalorieChartData(dateRange: DateRange(start: startOfRange!, end: endOfRange!, type: range.type))
				}
				unit = "Cal"
			}
		}
		
		let chartData = ActivityChartData(barColor: elementColor, elements: elements)
		self.fetchActivityChart(range: range, unit: unit, chartData: chartData)
	}
	
	private func fetchActivityChart(range: DateRange, unit: String?, chartData: ActivityChartData) {
		
		let thisdata = chartData.elements.reduce(0.0, { partialResult, element in
			return partialResult + (element.value ?? 0)
		})

		self.delegate?.barChart(isVisible: (thisdata == 0) ? false : true)
		
		if thisdata != 0 {
			var xAxis = [TimeInterval]()
			xAxis = chartData.elements.map({ (element) -> TimeInterval in
				return element.date.timeIntervalSince1970
			})
			
			var data = [[Double?]]()
			
			for (pointIndex, each) in chartData.elements.enumerated() {
				data.append([xAxis[pointIndex] * 1000, each.value])
			}
			let values = [data]
			let chart = HIChart()
			chart.type = "column"
			chart.backgroundColor = HIColor(uiColor: UIColor.clear)
			
			let hititle = HITitle()
			hititle.text = ""
			
			let hisubtitle = HISubtitle()
			hisubtitle.text = ""
			
			let hixAxis = HIXAxis()
			hixAxis.type = "datetime"
			if range.type == .daily {
				hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
				hixAxis.dateTimeLabelFormats.day = HIDay()
				hixAxis.dateTimeLabelFormats.day.main = "%l %p"
				hixAxis.dateTimeLabelFormats.hour = HIHour()
				hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
			} else if range.type == .weekly {
				hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
				hixAxis.dateTimeLabelFormats.day = HIDay()
				hixAxis.dateTimeLabelFormats.day.main = "%a"
			}
			
			let hixAxisTitle = HITitle()
			hixAxisTitle.text = ""
			hixAxis.title = hixAxisTitle
			
			hixAxis.tickWidth = 0
			hixAxis.lineWidth = 0
			
			if range.type == .daily {
				hixAxis.minRange = NSNumber(value: 3600 * 500)
				hixAxis.tickInterval = NSNumber(value: 3600 * 1000 * 3)
			} else if range.type == .weekly {
				hixAxis.minRange = NSNumber(value: 3600 * 24000)
				hixAxis.tickInterval = NSNumber(value: 3600 * 24000)
			}
			
			hixAxis.tickColor = HIColor(hexValue: "808080")
			hixAxis.labels = HILabels()
			hixAxis.labels.style = HICSSObject()
			hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			hixAxis.labels.style.fontSize = "12"
			hixAxis.labels.style.color = "#a5a5b2"
			if range.type == .weekly {
				hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ let weekDaysArray = ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa']; let theDay = new Date(this.value).getDay(); return weekDaysArray[theDay] }")
			} else {
				hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.value) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + ' ' + ampm; return strTime } else { return '' } }")
			}
			
			let yAxis = HIYAxis()
			yAxis.title = HITitle()
			yAxis.title.text = ""
			yAxis.lineColor = HIColor(uiColor: .white)
			yAxis.gridLineColor = HIColor(hexValue: "808080")
			yAxis.labels = HILabels()
			yAxis.labels.overflow = "justify"
			yAxis.labels.style = HICSSObject()
			yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			yAxis.labels.style.fontSize = "12"
			yAxis.labels.style.color = "#a5a5b2"
			
			let plotLines = HIPlotLines()
			plotLines.dashStyle = "dash"
			plotLines.width = 1
			plotLines.color = HIColor(uiColor: UIColor.white)
//			plotLines.label = HILabel()
//			plotLines.label.style = HICSSObject()
//			plotLines.label.style.color = "#a5a5b2"
			
			let goal = UserDefault.shared.goals
			if unit != "Cal" {
				plotLines.value = NSNumber(value:(goal.stepCountGoal.value as! Int))
			} else {
				plotLines.value = NSNumber(value:(goal.caloriesBurntGoal.value as! Int))
			}
			yAxis.plotLines = []
			
			let minPlotLine = HIPlotLines()
			minPlotLine.value = 0
			minPlotLine.width = 1
			minPlotLine.color = HIColor(uiColor: .white)
			minPlotLine.zIndex = NSNumber(value: 5)
			yAxis.plotLines.append(minPlotLine)
			
			if range.type == .weekly {
				yAxis.plotLines.append(plotLines)
			}
			
			let tooltip = HITooltip()
			tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
			tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
			tooltip.footerFormat = "</table>"
			tooltip.shared = true
			tooltip.useHTML = true
			tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
			
			let legend = HILegend()
			legend.enabled = NSNumber(value: false)
			
			let plotoptions = HIPlotOptions()
			plotoptions.column = HIColumn()
			plotoptions.column.states = HIStates()
			plotoptions.column.states.inactive = HIInactive()
			plotoptions.column.states.inactive.opacity = NSNumber(value: true)
			hixAxis.labels.enabled = true
			hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
			plotoptions.column.animation = HIAnimationOptionsObject()
			plotoptions.column.animation.duration = NSNumber(value: 0)
			plotoptions.column.borderWidth = 0
			plotoptions.column.stacking = "normal"
			//		plotoptions.column.pointWidth = 300
			
			let credits = HICredits()
			credits.enabled = NSNumber(value: false)
			
			let exporting = HIExporting()
			exporting.enabled = false
			
			var series = [HISeries]()
			let bar = HIColumn()
			bar.name = "Data"
			bar.data = values[0] as [Any]
			
			let maxValue = Int(chartData.elements.reduce(chartData.elements.last!) { aggregate, element in
				(aggregate.value ?? 0) < (element.value ?? 0) ? element : aggregate
			}.value ?? 0)
			
			let peakSeries = HIColumn()
			var newData = [[Double?]]()
			
			for (pointIndex, each) in chartData.elements.enumerated() {
				if Int(each.value ?? 0.0) == maxValue {
					newData.append([xAxis[pointIndex] * 1000, (Double(maxValue)/30.0)])
				}
				newData.append([xAxis[pointIndex] * 1000, nil])
			}
			let newValues = [newData]
			peakSeries.data = newValues[0] as [Any]
			
			series.append(peakSeries)
			series.append(bar)
			let uiColors = (unit != "Cal") ? [UIColor.barPink.hexString, chartData.barColor.hexString] : [UIColor.barPurple.hexString, chartData.barColor.hexString]
			
			let time =  HITime()
			time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
			
			let options = HIOptions()
			options.chart = chart
			options.title = hititle
			options.subtitle = hisubtitle
			options.xAxis = [hixAxis]
			options.yAxis = [yAxis]
			options.tooltip = tooltip
			options.credits = credits
			options.exporting = exporting
			options.series = series
			options.colors = uiColors
			options.plotOptions = plotoptions
			options.time = time
			options.legend = legend
			
			let attributeForValue = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.semiBold(withSize: 14), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textColor]
			let attributeForUnit = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.regular(withSize: 10), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.hexColor(hex: "#ffffff")?.withAlphaComponent(0.7)]
			let attributeForText = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.regular(withSize: 14), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.hexColor(hex: "#ffffff")?.withAlphaComponent(0.7)]
			
			let unitString = (unit == "Cal") ? "Cal" : "Steps"
			if maxValue > 9999 {
				let peakValue = (unit == "Cal") ? "\(maxValue) " : "\(maxValue/1000) k"
				let attributedText = NSMutableAttributedString(string: "PEAK: ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attributeForText))
				attributedText.append(NSAttributedString(string: peakValue, attributes: convertToOptionalNSAttributedStringKeyDictionary(attributeForValue)))
				attributedText.append(NSAttributedString(string: unitString, attributes: convertToOptionalNSAttributedStringKeyDictionary(attributeForUnit)))
				
				self.chartDelegate?.chart(with: options, peakValue: attributedText)
			} else {
				let attributedText = NSMutableAttributedString(string: "PEAK: ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attributeForText))
				attributedText.append(NSAttributedString(string: "\(maxValue) ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attributeForValue)))
				attributedText.append(NSAttributedString(string: unitString, attributes: convertToOptionalNSAttributedStringKeyDictionary(attributeForUnit)))
				
				self.chartDelegate?.chart(with: options, peakValue: attributedText)
			}
			
			var elements = chartData.elements
			elements = elements.filter { (element) -> Bool in
				return element.value != nil
			}
			
			var averageValue = elements.reduce(0) { (result: Double, nextItem: ChartElement) -> Double in
				return result + nextItem.value!
			}
			
			averageValue = averageValue / Double(elements.count)
			self.delegate?.averageGraphData(data: averageValue, unit: unit, range: range)
		} else {
			self.delegate?.averageGraphData(data: 0, unit: unit, range: range)
		}
	}
}
