//
//  ActivityViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 18/08/21.
//

import Foundation

protocol ActivityViewDelegate: AnyObject {
	
	func stepCountChart(chartElements: [ChartElement], totalStepCount: Int)
	func calorieCountChart(chartElements: [ChartElement], totalCalorie: Double, totalStepCount: Int)
	func exerciseSummary(exerciseList:[ExerciseSessionDataModel])
	func pauseData(reading: [TakePauseSessionDataModel])
    func activityIntensityChart(chartElements: ([ChartElement], [ChartElement]))
	func afterSleepDataResponseFetched(result: Result<[SleepDataModel], MovanoError>)
	func removeActivityIndicator()
	//func fetchIntensityData(completionHandler: @escaping ([CoreDataSleepStage]?, MovanoError?) -> Void)
    func fetchIntensityData()

	func showActivityIndicator()
}
