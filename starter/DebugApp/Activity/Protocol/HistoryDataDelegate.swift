//
//  HistoryDataDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/08/21.
//

import Foundation

protocol HistoryDataDelegate: AnyObject {
	
	func historyData(list: [HistorySection])
}
