//
//  ActivityPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 18/08/21.
//

import Foundation

// swiftlint:disable all
protocol ActivityPresenterProtocol {
	
	init(stepCountService: StepCountWebServiceProtocol, exerciseService: ExerciseDataWebServiceProtocol, pauseReadingService: PauseDataWebServiceProtocol, sleepService: SleepWebServiceProtocol, delegate: ActivityViewDelegate)
	func fetchStepCount(range: DateRange)
	func fetchExerciseData(range: DateRange)
	func fetchPauseReadingData(range: DateRange)
    func fetchActivityIntensityData(range: DateRange, completionHandler: @escaping ([CoreDataSleepStage]?, MovanoError?) -> Void)
}
