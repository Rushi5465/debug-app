//
//  ActivityDetailPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/08/21.
//

import Foundation
import UIKit

protocol ActivityDetailPresenterProtocol {
	
	init(stepCountService: StepCountWebServiceProtocol, chartDelegate: ChartDelegate, delegate: ActivityDetailViewDelegate)
	func fetchData(range: DateRange, elementColor: UIColor, dataType: ActivityDataType, currentAwakeStartDate: Date, currentAwakeEndDate: Date, isUpdateRequired: Bool, isCameFromDashboard: Bool)
}
