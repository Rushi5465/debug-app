//
//  ActivityIntensityDetailsDelegate.swift
//  BloodPressureApp
//
//  Created by Rushikant on 26/11/21.
//

import Foundation

protocol ActivityIntensityDetailsDelegate: AnyObject {
	
	func fetchPulseRateData(result: Result<[CoreDataPulseRate], MovanoError>, customDateRange: DateRange)
	func updatePulserateChart(pulseCountAvg: Double, customDateRange: DateRange, type: DateRangeType)
	
	func fetchIntensityData(result: Result<[CoreDataActivity], MovanoError>, customDateRange: DateRange)
	func updateIntensityChart(IntensityAvg: Double, customDateRange: DateRange, type: DateRangeType)
	
	func loadWeeklyChart(chartElements: [ChartElement])
}
