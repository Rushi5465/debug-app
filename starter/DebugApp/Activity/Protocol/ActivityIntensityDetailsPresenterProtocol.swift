//
//  ActivityIntensityDetailsPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 26/11/21.
//

import Foundation

protocol ActivityIntensityDetailsPresenterProtocol {
	init(pulseRateService: PulseRateWebServiceProtocol, stepCountService: StepCountWebServiceProtocol, delegate: ActivityIntensityDetailsDelegate)
	
	func fetchPulseRateData(timeStampRange: Range<Double>, customDateRange: DateRange)
	func fetchIntensityData(timeStampRange: Range<Double>, customDateRange: DateRange)
}
