//
//  HistoryListPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/08/21.
//

import Foundation

protocol HistoryListPresenterProtocol {
	
	init(exerciseService: ExerciseDataWebServiceProtocol, pauseService: PauseDataWebServiceProtocol, bpService: BloodPressureWebServiceProtocol, delegate: HistoryDataDelegate)
	func fetchExerciseData(range: DateRange)
	func fetchPauseData(range: DateRange)
	func fetchBloodPressureData(range: DateRange)
}
