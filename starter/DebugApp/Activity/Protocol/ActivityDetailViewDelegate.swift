//
//  ActivityDetailViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/08/21.
//

import Foundation

protocol ActivityDetailViewDelegate: AnyObject {
	
	func goal(type: ActivityDataType, targetValue: Int)
	func distance(value: Double, unit: String)
	func totalCount(value: Double, unit: String?)
	func caloriesCount(value: Double, unit: String)
	func averageGraphData(data: Double, unit: String?, range: DateRange)
	func barChart(isVisible: Bool)
    func errorOccured()
}
