//
//  ActivitySessionTableViewCell.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 03/11/21.
//

import UIKit

class ExerciseSessionTableViewCell: UITableViewCell {
    //IB Outlets
    
    @IBOutlet weak var moreSessionsButton: MovanoButton!
    @IBOutlet weak var sessionTypeImageView: UIImageView!
    @IBOutlet weak var sessionNameLabel: MovanoLabel!
    @IBOutlet weak var sessionHrsLabel: MovanoLabel!
    
    @IBOutlet weak var stepCountLabel: MovanoLabel!
    @IBOutlet weak var sessionMinutesLabel: MovanoLabel!
    @IBOutlet weak var noSessionsView: UIView!
    @IBOutlet weak var sessionTimeLabel: MovanoLabel!
    
    @IBOutlet weak var sessionTypeView: UIView!
	
	// Constraints for exercise labels
	@IBOutlet weak var sessionHoursLabelConstraint: NSLayoutConstraint!
	
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		
		moreSessionsButton.layer.cornerRadius = 4.0
		moreSessionsButton.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		moreSessionsButton.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 6, left: 0, bottom: 6, right: 0))
    }
    
}
