//
//  TakePauseTableViewCell.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 08/11/21.
//

import UIKit

class TakePauseTableViewCell: UITableViewCell {
    @IBOutlet weak var moreSessionsButton: MovanoButton!
    @IBOutlet weak var sessionTypeImageView: UIImageView!
    @IBOutlet weak var sessionNameLabel: MovanoLabel!
    @IBOutlet weak var sessionTypeView: UIView!
    
    @IBOutlet weak var noSessionsView: UIView!
    @IBOutlet weak var heartValuelabel: MovanoLabel!
    @IBOutlet weak var bpValue: MovanoLabel!
    @IBOutlet weak var sessionTimeLabel: MovanoLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 6, left: 0, bottom: 6, right: 0))
    }
}
