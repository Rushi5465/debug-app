//
//  HistoryHeaderView.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/08/21.
//

import UIKit

class HistoryHeaderView: UITableViewHeaderFooterView {
	
	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var lineView: UIView!
	
}
