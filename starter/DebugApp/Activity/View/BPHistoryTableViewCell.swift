//
//  BPHistoryTableViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 01/10/21.
//

import UIKit

// swiftlint:disable all
class BPHistoryTableViewCell: UITableViewCell {

	@IBOutlet weak var detailsUIView: UIView!
	@IBOutlet weak var timeValueLabel: MovanoLabel!
	@IBOutlet weak var bpValueLabel: MovanoLabel!
	
	@IBOutlet weak var lineView: UIView!
	@IBOutlet weak var viewTitleLabel: MovanoLabel!
	
	override func prepareForReuse() {
		lineView.layer.sublayers = nil
	}
	
	func unitAttributeLabel(value: String, unit: String) -> NSAttributedString {
		let str = NSMutableAttributedString()
		
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.semiBold(withSize: 16), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textColor]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.regular(withSize: 12), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textColor]
		
		str.append(NSMutableAttributedString(string: value + " ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
		str.append(NSMutableAttributedString(string: unit, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		
		return str
		
	}
    
}
