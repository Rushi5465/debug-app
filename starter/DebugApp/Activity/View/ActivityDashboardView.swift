//
//  ActivityDashboardView.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 27/08/21.
//

import UIKit
import Highcharts

// swiftlint:disable all
class ActivityDashboardView: UIView {

	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var valueLabel: MovanoLabel!
	@IBOutlet weak var chartView: HIChartView!
	@IBOutlet weak var noChartView: UIView!
	
	var currentAwakeStartDate: Date?
	var currentAwakeEndDate: Date?
	
	override func prepareForInterfaceBuilder() {
		commonInit()
	}
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		commonInit()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		
		commonInit()
	}
	
	func commonInit() {
		let viewFromXib = Bundle.main.loadNibNamed("ActivityDashboardView", owner: self, options: nil)![0] as! UIView
		viewFromXib.frame = self.bounds
		addSubview(viewFromXib)
	}
	
	func loadChartView(model: ActivityChartModel) {
		self.titleLabel.text = model.type
		
		self.valueLabel.text = model.value?.stringValue
		if let goal = model.goal {
			self.valueLabel.text?.append("/" + goal.stringValue)
		}
		if let unit = model.unit {
			self.valueLabel.text?.append(" " + unit)
		}
		self.fetchActivityChart(chartData: model.chartData)
	}
	
	private func fetchActivityChart(chartData: ActivityChartData) {
		
		let data = chartData.elements.reduce(0.0, { partialResult, element in
			return partialResult + (element.value ?? 0)
		})
		if data == 0 {
			self.chartView.isHidden = true
			self.noChartView.isHidden = false
			
		} else {
			self.chartView.isHidden = false
			self.noChartView.isHidden = true
			
			var xAxis = [TimeInterval]()
			xAxis = chartData.elements.map({ (element) -> TimeInterval in
				return element.date.timeIntervalSince1970
			})
			
			var data = [[Double?]]()
			
			for (pointIndex, each) in chartData.elements.enumerated() {
				data.append([xAxis[pointIndex] * 1000, each.value])
			}
			let values = [data]
			let chart = HIChart()
			chart.type = "column"
			//chart.marginBottom = 70
			chart.backgroundColor = HIColor(uiColor: UIColor.clear)
			chart.style = HICSSObject()
			chart.style.fontFamily = Utility.getFontFamilyForCharts()
			
			let hititle = HITitle()
			hititle.text = ""
			
			let hisubtitle = HISubtitle()
			hisubtitle.text = ""
			
			let hixAxis = HIXAxis()
			hixAxis.type = "datetime"
			hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
			hixAxis.dateTimeLabelFormats.day = HIDay()
			hixAxis.dateTimeLabelFormats.day.main = "%l %p"
			hixAxis.dateTimeLabelFormats.hour = HIHour()
			hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
			
			let hixAxisTitle = HITitle()
			hixAxisTitle.text = ""
			hixAxis.title = hixAxisTitle
			
//			hixAxis.minRange =  NSNumber(value: 3600 * 24000)
			hixAxis.tickInterval = NSNumber(value: 1000)
			hixAxis.minTickInterval = NSNumber(value: 1000)
			
//			if let elementDate = chartData.elements.first {
//				hixAxis.min = NSNumber(value: (elementDate.date.startOfDay.timeIntervalSince1970) * 1000)
//				hixAxis.max = NSNumber(value: (elementDate.date.endOfDay.timeIntervalSince1970) * 1000)
//			}
			
			if currentAwakeStartDate != nil {
				let hour = Calendar.current.component(.hour, from: currentAwakeStartDate!)
				currentAwakeStartDate?.setTime(hour: hour, min: 0, sec: 0)
				hixAxis.min = NSNumber(value: (1000 * currentAwakeStartDate!.timeIntervalSince1970))
			}
			
			if currentAwakeEndDate != nil {
				let hour = Calendar.current.component(.hour, from: currentAwakeEndDate!)
				currentAwakeEndDate?.setTime(hour: (hour + 1), min: 0, sec: 0)
				hixAxis.max = NSNumber(value: (1000 * currentAwakeEndDate!.timeIntervalSince1970))
			}
			
			hixAxis.tickWidth = 0
			hixAxis.lineWidth = 0
			
            hixAxis.tickColor = HIColor(uiColor: chartData.barColor)
			hixAxis.labels = HILabels()
			hixAxis.labels.style = HICSSObject()
			hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			hixAxis.labels.style.fontSize = "12"
			hixAxis.labels.style.color = "#a5a5b2"
//			hixAxis.labels.rotation = NSNumber(value: 0.00001)
			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.isFirst || this.isLast) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + '' + ampm; return strTime } }")
//            hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.isFirst || this.isLast) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + '' + ampm; return strTime } else { return '' } }")
			let yAxis = HIYAxis()
			yAxis.title = HITitle()
			yAxis.title.text = ""
			yAxis.labels = HILabels()
            yAxis.labels.enabled = true
			yAxis.labels.overflow = "justify"
			yAxis.labels.style = HICSSObject()
			yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			yAxis.labels.style.fontSize = "12"
			yAxis.labels.style.color = "#a5a5b2"
            yAxis.min = 0
            yAxis.max = 10
            yAxis.tickInterval = NSNumber(value: 1)
            yAxis.tickColor = HIColor(uiColor: UIColor.clear)
            yAxis.gridLineColor = HIColor(uiColor: UIColor.clear)
            //yAxis.lineColor = HIColor(uiColor: UIColor.red)
            yAxis.lineWidth = 0
            yAxis.tickLength = 0
            yAxis.tickPixelInterval = NSNumber(value: 1)
            yAxis.labels.formatter = HIFunction(jsFunction: "function(){ if (this.value == 3){ return 'Low' } else if(this.value == 7){ return 'Med'} else if(this.value == 10){ return 'High'} else{return ''}}")
			
			let plotLine1 = HIPlotLines()
			plotLine1.value = NSNumber(value: 5)
			plotLine1.width = 1
			plotLine1.color = HIColor(uiColor: UIColor(red: 255, green: 255, blue: 255, alpha: 0.5))
			
			let plotLine2 = HIPlotLines()
			plotLine2.value = NSNumber(value: 8.5)
			plotLine2.width = 1
			plotLine2.color = HIColor(uiColor: UIColor(red: 255, green: 255, blue: 255, alpha: 0.5))
			
			let plotLine3 = HIPlotLines()
			plotLine3.value = NSNumber(value: 0)
			plotLine3.width = 1
			plotLine3.color = HIColor(uiColor: UIColor(red: 255, green: 255, blue: 255, alpha: 0.5))
			plotLine3.zIndex = NSNumber(value: -1)
			
			yAxis.plotLines = [plotLine1, plotLine2, plotLine3]
			
			let tooltip = HITooltip()
			tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
			tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
			tooltip.footerFormat = "</table>"
			tooltip.shared = true
			tooltip.useHTML = true
			tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
			
			let legend = HILegend()
			legend.enabled = NSNumber(value: false)
			
			let plotoptions = HIPlotOptions()
			plotoptions.column = HIColumn()
			plotoptions.column.states = HIStates()
			plotoptions.column.states.inactive = HIInactive()
			plotoptions.column.states.inactive.opacity = NSNumber(value: true)
			hixAxis.labels.enabled = true
			hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
			plotoptions.column.animation = HIAnimationOptionsObject()
			plotoptions.column.animation.duration = NSNumber(value: 0)
			//		plotoptions.column.pointWidth = 300
			let credits = HICredits()
			credits.enabled = NSNumber(value: false)
			
			let exporting = HIExporting()
			exporting.enabled = false
			
			var series = [HISeries]()
			let bar = HIColumn()
			bar.name = "Data"
			bar.data = values[0] as [Any]
            bar.borderColor = HIColor(uiColor: UIColor.clear)
            let zone1 = HIZones()
            zone1.value = NSNumber(value: 7)
            zone1.color = HIColor(uiColor: chartData.barColor)
            bar.zones = [zone1]
            bar.zoneAxis = "y"
            bar.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
                                    [0, "rgb(205 32 170)"],
                                    [0.25, "rgb(89 63 223)"]])
			series.append(bar)
            
            let uiColors = [UIColor.barPink.hexString]
			
			let time =  HITime()
			time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
			
			let options = HIOptions()
			options.chart = chart
			options.title = hititle
			options.subtitle = hisubtitle
			options.xAxis = [hixAxis]
			options.yAxis = [yAxis]
			options.tooltip = tooltip
			options.credits = credits
			options.exporting = exporting
			options.series = series
			options.colors = uiColors
			options.plotOptions = plotoptions
			options.time = time
			options.legend = legend
			
			self.chartView.options = options
		}
		NotificationCenter.default.post(name: NSNotification.Name("removeIndicator"), object: nil)
	}

}
