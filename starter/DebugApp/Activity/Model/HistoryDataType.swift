//
//  HistoryDataType.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/08/21.
//

import Foundation

enum HistoryDataType {
	case pauseReading
	case exerciseReading
	case bpReading
}
