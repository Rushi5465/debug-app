//
//  FetchActivityResponseModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 19/08/21.
//

import Foundation

// MARK: - FetchActivityResponseModel
struct FetchActivityResponseModel: Codable {
	let data: FetchActivityData
}

// MARK: - FetchActivityData
struct FetchActivityData: Codable {
	let listExerciseData: ListExerciseData
	let queryStepCountByTimestampRange: QueryStepCountByTimestampRange
}

// MARK: - ListExerciseData
struct ListExerciseData: Codable {
	let items: [ListExerciseDataItem]
}

// MARK: - ListExerciseDataItem
struct ListExerciseDataItem: Codable {
	let averageSkinTemprature: Double?
	let averageBreathingRate: Int?
	let averageSystolic: Int?
	let averagePulseRate: Int?
	let endTime: Double
	let userID: String
	let totalDistanceCovered: Double?
	let totalStepCount: Int?
	let totalCaloriesBurnt: Int?
	let startTime: Double
	let averageDiastolic: Int?
	let id: String

	enum CodingKeys: String, CodingKey {
		case averageSkinTemprature = "average_skin_temprature"
		case averageBreathingRate = "average_breathing_rate"
		case averageSystolic = "average_systolic"
		case averagePulseRate = "average_pulse_rate"
		case endTime = "end_time"
		case userID = "user_id"
		case totalDistanceCovered = "total_distance_covered"
		case totalStepCount = "total_step_count"
		case totalCaloriesBurnt = "total_calories_burnt"
		case startTime = "start_time"
		case averageDiastolic = "average_diastolic"
		case id
	}
}

// MARK: - QueryStepCountByTimestampRange
struct QueryStepCountByTimestampRange: Codable {
	let items: [QueryStepCountByTimestampRangeItem]
}

// MARK: - QueryStepCountByTimestampRangeItem
struct QueryStepCountByTimestampRangeItem: Codable {
	let distanceCovered: Double?
	let stepCountValue: Int
	let caloriesBurnt: Double?
	let stepCountTimestamp: Double
	let userID: String

	enum CodingKeys: String, CodingKey {
		case distanceCovered = "distance_covered"
		case stepCountValue = "step_count_value"
		case caloriesBurnt = "calories_burnt"
		case stepCountTimestamp = "step_count_timestamp"
		case userID = "user_id"
	}
}
