//
//  ActivityDataType.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/08/21.
//

import Foundation

enum ActivityDataType {
	case stepCount
	case calories
}
