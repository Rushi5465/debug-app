//
//  ActivitySessionsDataModels.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 08/11/21.
//

import Foundation

// swiftlint:disable all
struct ExerciseSessionDataModel {
    let sessionTime: String
    let stepCount:String
    let sessionTimeInHrs:String
    let sessionTimeInMinutes:String
    let sessionTimeInSeconds:String
    let breathingRate:String
    let caloriesBurned:String
    let skinTemp:String
    let distance:String
    let oxygen:String
	let startTime: Date
	let endTime: Date
    
    init(sessionTime:String,stepCount:String,sessionTimeInHrs:String,sessionTimeInMinutes:String,sessionTimeInSeconds:String,breathingRate:String,caloriesBurned:String,skinTemp:String,distance:String,oxygen:String,startTime: Date,endTime: Date) {
        self.sessionTime = sessionTime
        self.stepCount = stepCount
        self.sessionTimeInHrs = sessionTimeInHrs
        self.sessionTimeInMinutes = sessionTimeInMinutes
        self.sessionTimeInSeconds = sessionTimeInSeconds
        self.breathingRate = breathingRate
        self.caloriesBurned = caloriesBurned
        self.skinTemp = skinTemp
        self.distance = distance
        self.oxygen = oxygen
		self.startTime = startTime
		self.endTime = endTime
    }
}

struct TakePauseSessionDataModel {
    let sessionTime: String
    let heartRateValue:String
    let bloodPressureValue:String
    let skinTemp:String
    let breathingRate:String
}
