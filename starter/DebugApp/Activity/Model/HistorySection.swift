//
//  HistorySection.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/08/21.
//

import Foundation

struct HistorySection {
	var date: Date
	var data: [Any]
}
