//
//  Activity.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 18/08/21.
//

import Foundation
import UIKit

struct Activity {
	
	var model: Any
}

struct ActivityModel {
	
	var type: String
	var value: NSNumber?
}

struct ActivityChartModel {
	
	var type: String
	var value: NSNumber?
	var goal: NSNumber?
	var unit: String?
	var chartData: ActivityChartData
}

struct ActivityChartData {
	var barColor: UIColor
	var elements: [ChartElement]
}

struct ChartElement {
	var date: Date
	var value: Double?
}

// MARK: - Datum

struct FetchActivityModel: Decodable {
	var message: String
	var status_code: Int
	var data: [ActivityIntensityModel]
}

struct ActivityIntensityModel: Codable {
	let date: String
	let avg_activity_intensity: Double?
	let avg_pulse_rate: Double?
}

/*
"date": "11-04-2021",
		"awake_time": "1.6359912E9",
		"sleep_time": "1.63605E9",
		"avg_activity_intensity": 3.38,
		"avg_pulse_rate": 44
*/
