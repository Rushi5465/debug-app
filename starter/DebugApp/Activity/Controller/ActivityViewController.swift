//
//  ActivityViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/08/21.
//

import UIKit

// swiftlint:disable all
class ActivityViewController: UIViewController {

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var activityChartView: ActivityDashboardView!
    @IBOutlet weak var exerciseTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var takePauseTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var activityDateLabel: MovanoLabel!
    @IBOutlet weak var takePauseSessionTableView: UITableView!
    @IBOutlet weak var exerciseSessionTableView: UITableView!
    @IBOutlet weak var totalStepCountLabel: MovanoLabel!
    @IBOutlet weak var stepsView: UIView!
    
    @IBOutlet weak var caloriesBurnedView: UIView!
    @IBOutlet weak var caloriesBurnedCountLabel: MovanoLabel!
    //@IBOutlet weak var calorieChartView: ActivityDashboardView!
	//@IBOutlet weak var exerciseSummaryView: UIView!
	//@IBOutlet weak var idleTimeView: UIView!
	//@IBOutlet weak var pauseReadingView: UIView!
	//@IBOutlet weak var exerciseSummaryLabel: MovanoLabel!
	//@IBOutlet weak var idleTimeLabel: MovanoLabel!
	//@IBOutlet weak var bpReadingLabel: MovanoLabel!
	//@IBOutlet weak var pulseRateLabel: MovanoLabel!
	var noOfExerciseRows = 1
    var noOfTakePauseRows = 1
    var hideExerciseSessionMoreButton = false
    var hideTakePauseSessionMoreButton = false
    var exerciseSessionDataModels:[ExerciseSessionDataModel] = []
    var takePauseSessionDataModels:[TakePauseSessionDataModel] = []
    var presenter: ActivityPresenter?
	var currentAwakeStartDate: Date?
	var currentAwakeEndDate: Date?
	var chartDataForDailyIntensity: ActivityChartData?
	var highActivityTime = 0
	var mediumActivityTime = 0
	var lowActivityTime = 0
	var inactiveActivityTime = 0
	var isSessionExpanded = false
	
	var selectedDate: DateRange! {
		didSet {
			hud.show(in: self.parent?.parent?.view ?? self.view)
			self.activityDateLabel.text = "On \(selectedDate.start.formattedDateWithSuffix)"
			let dayAfter = selectedDate.end.dateAfter(days: 1)
			
			// Step counnt start and end time
			let data = CoreDataSleepTime.fetch(forDate: selectedDate.start)
			if dayAfter < Date() {
				var awakeEndTime = ""
				if let awakeTime = CoreDataSleepTime.fetch(forDate: dayAfter)?.sleepStartTime, awakeTime != "0" {
					awakeEndTime = awakeTime
				} else {
					if let dayEnd = selectedDate.end.setTime(hour: 23, min: 59, sec: 59)?.timeIntervalSince1970 {
						awakeEndTime = "\(dayEnd)"
					}
				}
				
				var awakeStartTime = ""
				if let awakeTime = data?.sleepEndTime.toDouble, awakeTime != 0 {
					awakeStartTime = "\(awakeTime)"
				} else {
					if let dayStart = selectedDate.start.setTime(hour: 0, min: 0, sec: 0)?.timeIntervalSince1970 {
						awakeStartTime = "\(dayStart)"
					}
				}
				
				if let startTime = awakeStartTime.toDouble, let endTime = awakeEndTime.toDouble {
					self.currentAwakeEndDate = Date(timeIntervalSince1970: endTime)
					self.currentAwakeStartDate = Date(timeIntervalSince1970: startTime)
					let range = DateRange(start: Date(timeIntervalSince1970: startTime), end: Date(timeIntervalSince1970: endTime), type: .daily)
					self.presenter?.fetchAllActivityData(range: selectedDate)
				} else {
					self.presenter?.fetchAllActivityData(range: selectedDate)
				}
			} else {
				if let startTime = data?.sleepEndTime.toDouble{
					self.currentAwakeEndDate = Date()
					
					if(startTime != 0){
						self.currentAwakeStartDate = Date(timeIntervalSince1970: startTime)
					}else{
						self.currentAwakeStartDate = Date().setTime(hour: 0, min: 0, sec: 0)
					}
					let range = DateRange(start: currentAwakeStartDate!, end: Date(), type: .daily)
					self.presenter?.fetchAllActivityData(range: range)
				} else {
					self.currentAwakeStartDate = selectedDate.start
					self.currentAwakeEndDate = Date()
					let range = DateRange(start: selectedDate.start, end: Date(), type: .daily)
					self.presenter?.fetchAllActivityData(range: range)
				}
			}
		}
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		self.registerPresenters()
        self.registerNibs()
		self.selectedDate = DateManager.current.selectedDate
		
		self.stepsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(stepCountChartTapped(tap:))))
		self.stepsView.isUserInteractionEnabled = true
		
		self.caloriesBurnedView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(calorieChartTapped(tap:))))
		self.caloriesBurnedView.isUserInteractionEnabled = true
		
        self.activityChartView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(activityChartTapped(tap:))))
        self.caloriesBurnedView.isUserInteractionEnabled = true
		//self.exerciseSummaryView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exerciseHistoryTapped(tap:))))
		//self.exerciseSummaryView.isUserInteractionEnabled = true
		
		//self.pauseReadingView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(pauseReadingHistoryTapped(tap:))))
		//self.pauseReadingView.isUserInteractionEnabled = true
        
        setupUIElements()
		
		

		takePauseSessionTableView.isHidden = true
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		isSessionExpanded = false
        if(!self.selectedDate.start.isEqual(to: DateManager.current.selectedDate.start, toGranularity: .day) || (Date().isEqual(to: self.selectedDate.start, toGranularity: .day))){
            self.selectedDate = DateManager.current.selectedDate
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("updateStepCount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateStepCount), name: NSNotification.Name("updateStepCount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.calendarDateSelected), name: NSNotification.Name("calendarDateSelected"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeActivityIndicator), name: NSNotification.Name("removeIndicator"), object: nil)
		
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("calendarDateSelected"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("updateStepCount"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("removeIndicator"), object: nil)
	}
	
	deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("calendarDateSelected"), object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name("updateStepCount"), object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name("removeIndicator"), object: nil)
	}
	
	@objc private func updateStepCount() {
		self.selectedDate = DateManager.current.selectedDate
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let stepCountService = StepCountWebService()
			let exerciseService = ExerciseDataWebService()
			let pauseReadingService = PauseDataWebService()
			let sleepService = SleepWebService()
			
			self.presenter = ActivityPresenter(stepCountService: stepCountService, exerciseService: exerciseService, pauseReadingService: pauseReadingService, sleepService: sleepService, delegate: self)
		}
	}
	
    func setupUIElements(){
        caloriesBurnedView.layer.cornerRadius = 5;
        caloriesBurnedView.layer.masksToBounds = true;
        stepsView.layer.cornerRadius = 5
        stepsView.layer.masksToBounds = true
    }
    
	@objc func activityChartTapped(tap: UITapGestureRecognizer) {
		if currentAwakeStartDate != nil && currentAwakeEndDate != nil && activityChartView.noChartView.isHidden {
			let controller = ActivityIntensityDetailsViewController.instantiate(fromAppStoryboard: .activity)
			controller.currentAwakeStartDate = self.currentAwakeStartDate
			controller.currentAwakeEndDate = self.currentAwakeEndDate
			controller.chartDataForDailyIntensity = self.chartDataForDailyIntensity
			controller.inactiveActivityTime = self.inactiveActivityTime
			controller.lowActivityTime = self.lowActivityTime
			controller.mediumActivityTime = self.mediumActivityTime
			controller.highActivityTime = self.highActivityTime
			parent?.navigationController?.pushViewController(controller, animated: true)
		}
	}
    
	@objc func stepCountChartTapped(tap: UITapGestureRecognizer) {
		if totalStepCountLabel.text != "0" {
			let controller = ActivityDetailViewController.instantiate(fromAppStoryboard: .activity)
			controller.dataType = .stepCount
			controller.elementColor = .barPurple
			controller.currentAwakeStartDate = currentAwakeStartDate
			controller.currentAwakeEndDate = currentAwakeEndDate
			parent?.navigationController?.pushViewController(controller, animated: true)
		}
	}
	
	@objc func calorieChartTapped(tap: UITapGestureRecognizer) {
		if caloriesBurnedCountLabel.text != "0" {
			let controller = ActivityDetailViewController.instantiate(fromAppStoryboard: .activity)
			controller.dataType = .calories
			controller.elementColor = .barPink
			controller.currentAwakeStartDate = currentAwakeStartDate
			controller.currentAwakeEndDate = currentAwakeEndDate
			parent?.navigationController?.pushViewController(controller, animated: true)
		}
	}
	
	@objc func exerciseHistoryTapped(tap: UITapGestureRecognizer) {
		let controller = HistoryDataViewController.instantiate(fromAppStoryboard: .activity)
		controller.dataType = .exerciseReading
        parent?.navigationController?.pushViewController(controller, animated: true)
	}
	
	@objc func pauseReadingHistoryTapped(tap: UITapGestureRecognizer) {
		let controller = HistoryDataViewController.instantiate(fromAppStoryboard: .activity)
		controller.dataType = .pauseReading
        parent?.navigationController?.pushViewController(controller, animated: true)
	}
}

// MARK: - ActivityViewDelegate
extension ActivityViewController: ActivityViewDelegate {
    
	/*func fetchIntensityData(completionHandler: @escaping ([CoreDataSleepStage]?, MovanoError?) -> Void)*/
    func fetchIntensityData(){
		let dayAfter = selectedDate.end.dateAfter(days: 1)
		// Intensity graph
		let data = CoreDataSleepTime.fetch(forDate: selectedDate.start)
		if dayAfter < Date() {
			var awakeEndTime = ""
			if let awakeTime = CoreDataSleepTime.fetch(forDate: dayAfter)?.sleepStartTime, awakeTime != "0" {
				awakeEndTime = awakeTime
			} else {
				if let dayEnd = selectedDate.end.setTime(hour: 23, min: 59, sec: 59)?.timeIntervalSince1970 {
					awakeEndTime = "\(dayEnd)"
				}
			}
			
			var awakeStartTime = ""
			if let awakeTime = data?.sleepEndTime.toDouble, awakeTime != 0 {
				awakeStartTime = "\(awakeTime)"
			} else {
				if let dayStart = selectedDate.start.setTime(hour: 0, min: 0, sec: 0)?.timeIntervalSince1970 {
					awakeStartTime = "\(dayStart)"
				}
			}
			
			if let startTime = awakeStartTime.toDouble, let endTime = awakeEndTime.toDouble {
				self.currentAwakeEndDate = Date(timeIntervalSince1970: endTime)
				self.currentAwakeStartDate = Date(timeIntervalSince1970: startTime)
				let range = DateRange(start: Date(timeIntervalSince1970: startTime), end: Date(timeIntervalSince1970: endTime), type: .daily)
				self.presenter?.fetchActivityIntensityData(range: range, completionHandler: {  [weak self] (item, error) in
                    self?.removeActivityIndicator()
				})
			} else {
				self.removeActivityIndicator()
			}
		} else {
			if let startTime = data?.sleepEndTime.toDouble{
				self.currentAwakeEndDate = Date()
                
                if(startTime != 0){
                    self.currentAwakeStartDate = Date(timeIntervalSince1970: startTime)
                }else{
                    self.currentAwakeStartDate = Date().setTime(hour: 0, min: 0, sec: 0)
                }
                let range = DateRange(start: currentAwakeStartDate!, end: Date(), type: .daily)
				self.presenter?.fetchActivityIntensityData(range: range, completionHandler: {  [weak self] (item, error) in
                    self?.removeActivityIndicator()
				})
			} else {
				activityChartView.chartView.isHidden = true
				activityChartView.noChartView.isHidden = false
				self.removeActivityIndicator()
			}
		}
	}
	
	func afterSleepDataResponseFetched(result: Result<[SleepDataModel], MovanoError>) {
		print("Movano all activity tasks are done")
		activityChartView.loadChartView(model: ActivityChartModel(type: "Activity", chartData: ActivityChartData(barColor: .purple, elements: [])))
	}
	
	@objc func removeActivityIndicator() {
		hud.dismiss()
	}
	
	func showActivityIndicator() {
            hud.show(in: self.parent?.parent?.view ?? self.view)
        
	}
	
	func activityIntensityChart(chartElements: ([ChartElement], [ChartElement])) {
		var valueArray = [Int]()
		for x in 0..<chartElements.1.count {
			valueArray.append((Int(chartElements.1[x].value!)))
		}
		
		let countItems = valueArray.map{($0,1)}
		let counts = Dictionary(countItems, uniquingKeysWith: +)
		
		self.inactiveActivityTime = ((counts[0] ?? 0) * 5)
		self.lowActivityTime = (((counts[1] ?? 0) + (counts[2] ?? 0) + (counts[3] ?? 0)) * 5)
		self.mediumActivityTime = (((counts[4] ?? 0) + (counts[5] ?? 0) + (counts[6] ?? 0) + (counts[7] ?? 0)) * 5)
		self.highActivityTime = (((counts[8] ?? 0) + (counts[9] ?? 0) + (counts[10] ?? 0)) * 5)
				
//		totalStepCountLabel.text = "8"
		let chartData = ActivityChartData(barColor: .barPurple, elements: chartElements.0)
		let stepCountModel = ActivityChartModel(type: "Activity", value: NSNumber(value: 8), goal: NSNumber(value: UserDefault.shared.goals.stepCountGoal.intValue), chartData: chartData)
		activityChartView.currentAwakeStartDate = self.currentAwakeStartDate
		activityChartView.currentAwakeEndDate = self.currentAwakeEndDate
		self.chartDataForDailyIntensity = stepCountModel.chartData
		activityChartView.loadChartView(model: stepCountModel)
	}
    
   
	func stepCountChart(chartElements: [ChartElement], totalStepCount: Int) {
        totalStepCountLabel.text = String(totalStepCount)
		let chartData = ActivityChartData(barColor: .barPurple, elements: chartElements)
		let stepCountModel = ActivityChartModel(type: "Activity", value: NSNumber(value: totalStepCount), goal: NSNumber(value: UserDefault.shared.goals.stepCountGoal.intValue), chartData: chartData)
		activityChartView.loadChartView(model: stepCountModel)
	}
	
	func calorieCountChart(chartElements: [ChartElement], totalCalorie: Double, totalStepCount: Int) {
        if(totalCalorie > 1000){
            let formatter = NumberFormatter()
            formatter.maximumFractionDigits = 1
            formatter.roundingMode = .down
            let totalCalorieInString = formatter.string(from: NSNumber.init(value: Double(totalCalorie)))
			caloriesBurnedCountLabel.text = "\(Int(totalCalorieInString?.toDouble?.rounded() ?? 0))"
        }else{
//            caloriesBurnedCountLabel.text = String(format: "%.01f", totalCalorie)
			caloriesBurnedCountLabel.text = "\(Int(totalCalorie.rounded()))"
        }
		totalStepCountLabel.text = String(totalStepCount)
		let calorieData = ActivityChartData(barColor: .barPink, elements: chartElements)
		let unit = (totalCalorie > 1000) ? "Cal" : "Cal"
		let calorieModel = ActivityChartModel(type: "Calorie Burnt", value: NSNumber(value: totalCalorie), unit: unit, chartData: calorieData)
		//calorieChartView.loadChartView(model: calorieModel)
	}
	
	func exerciseSummary(exerciseList:[ExerciseSessionDataModel]) {
        self.exerciseSessionDataModels.removeAll()
        self.noOfExerciseRows = 1
        hideExerciseSessionMoreButton = false
        self.exerciseSessionDataModels = exerciseList
		isSessionExpanded = false
        self.exerciseSessionTableView.reloadData()
	}
	
	func pauseData(reading: [TakePauseSessionDataModel]) {
//        hud.dismiss()
        self.noOfTakePauseRows = 1
        hideTakePauseSessionMoreButton = false
            self.takePauseSessionDataModels.removeAll()
            takePauseSessionDataModels = reading
            takePauseSessionTableView.reloadData()
	}
}

// MARK: - Calendar Callbacks
extension ActivityViewController {
	
	@objc private func calendarDateSelected(notification: NSNotification) {
		guard let object = notification.object as? DateRange else {
			return
		}
        if(object.type == .daily){
		self.selectedDate = object
        }
	}
}

//MARK: - Tableview datasource delegate
extension ActivityViewController: UITableViewDelegate,UITableViewDataSource{
    
    func registerNibs() {
        self.exerciseSessionTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
//        self.takePauseSessionTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)

        exerciseSessionTableView.delegate = self
        exerciseSessionTableView.dataSource = self
//        takePauseSessionTableView.delegate = self
//        takePauseSessionTableView.dataSource = self
        let exerciseSessionCellNib = UINib(nibName: "ExerciseSessionTableViewCell", bundle: nil)
        exerciseSessionTableView.register(exerciseSessionCellNib, forCellReuseIdentifier: ExerciseSessionTableViewCell.IDENTIFIER)
        
//        let takePauseSessionCellNib = UINib(nibName: "TakePauseTableViewCell", bundle: nil)
//
//        takePauseSessionTableView.register(takePauseSessionCellNib, forCellReuseIdentifier: TakePauseTableViewCell.IDENTIFIER)

    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        exerciseSessionTableView.layer.removeAllAnimations()
        takePauseSessionTableView.layer.removeAllAnimations()

        exerciseTableHeightConstraint.constant = exerciseSessionTableView.contentSize.height
        takePauseTableHeightConstraint.constant = takePauseSessionTableView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
        }

    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == exerciseSessionTableView){
            return noOfExerciseRows
        }else if(tableView == takePauseSessionTableView){
            return noOfTakePauseRows
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return 120
        }else{
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == exerciseSessionTableView){
            let cell = ExerciseSessionTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
            cell.contentView.layer.cornerRadius = 5
            cell.contentView.layer.masksToBounds = true
            cell.sessionTimeLabel.text = ""
            if(indexPath.row == 0){
                cell.sessionTypeView.isHidden = false
				cell.sessionHoursLabelConstraint.constant = 8
            }else{
				cell.sessionHoursLabelConstraint.constant = 16
                cell.sessionTypeView.isHidden = true
            }
            if(exerciseSessionDataModels.count>0){
                let exerciseSessionObj:ExerciseSessionDataModel = exerciseSessionDataModels[indexPath.row]
                
                cell.moreSessionsButton.addTarget(self, action: #selector(moreSessionButtonClicked(sender:)), for: .touchUpInside)
                cell.moreSessionsButton.tag = 0
				cell.moreSessionsButton.isHidden = true
                if(exerciseSessionDataModels.count > 1){
//                    cell.moreSessionsButton.isHidden = hideExerciseSessionMoreButton
					cell.moreSessionsButton.isHidden = (indexPath.row == 0) ? false : true
					if isSessionExpanded {
						cell.moreSessionsButton.setImage(UIImage(named: "dropUp"), for: .normal)
						cell.moreSessionsButton.setImage(UIImage(named: "dropUp"), for: .selected)
					} else {
						cell.moreSessionsButton.setImage(UIImage(named: "dropDown"), for: .normal)
						cell.moreSessionsButton.setImage(UIImage(named: "dropDown"), for: .selected)
					}
                    cell.moreSessionsButton.setTitle("+ \(exerciseSessionDataModels.count - 1)", for: .normal)
                }
                cell.sessionTimeLabel.text = exerciseSessionObj.sessionTime
                cell.stepCountLabel.text = exerciseSessionObj.stepCount
                cell.sessionHrsLabel.text = exerciseSessionObj.sessionTimeInHrs
                cell.sessionMinutesLabel.text = exerciseSessionObj.sessionTimeInMinutes
                cell.noSessionsView.isHidden = true

            }else{
                //Show no sessions recoreded lable
                cell.noSessionsView.isHidden = false
            }
            
            return cell
        }else if tableView == takePauseSessionTableView{
            let cell = TakePauseTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
            cell.contentView.layer.cornerRadius = 5
            cell.contentView.layer.masksToBounds = true
            cell.sessionTimeLabel.text = ""
            if(indexPath.row == 0){
                cell.sessionTypeView.isHidden = false
            }else{
                cell.sessionTypeView.isHidden = true
            }
            if(takePauseSessionDataModels.count>0){
                let pauseSessionObj:TakePauseSessionDataModel = takePauseSessionDataModels[indexPath.row]
                cell.moreSessionsButton.addTarget(self, action: #selector(moreSessionButtonClicked(sender:)), for: .touchUpInside)
                cell.moreSessionsButton.tag = 1
                cell.sessionTimeLabel.text = pauseSessionObj.sessionTime
                cell.bpValue.text = pauseSessionObj.bloodPressureValue
                cell.heartValuelabel.text = pauseSessionObj.heartRateValue
                cell.moreSessionsButton.isHidden = true
                if(takePauseSessionDataModels.count > 1){
                    cell.moreSessionsButton.isHidden = hideTakePauseSessionMoreButton
                    cell.moreSessionsButton.setTitle("+ \(takePauseSessionDataModels.count - 1)", for: .normal)
                }
                cell.noSessionsView.isHidden = true

            }else{
                //Show no session label
                cell.noSessionsView.isHidden = false
            }
           
            return cell
        }
        return UITableViewCell()

    }
    
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if(tableView == exerciseSessionTableView){
			if(exerciseSessionDataModels.count>0){
				let exerciseSessionObj:ExerciseSessionDataModel = exerciseSessionDataModels[indexPath.row]
				if exerciseSessionObj.stepCount != "-" {
					let controller = ExerciseDataViewController.instantiate(fromAppStoryboard: .dashboard)
					controller.caloriesBurnt = Float(exerciseSessionObj.caloriesBurned.toDouble ?? 0)
					controller.stepCount = Int(exerciseSessionObj.stepCount.toDouble ?? 0)
					controller.spo2 = Int(exerciseSessionObj.oxygen.toDouble ?? 0)
					controller.distance = (exerciseSessionObj.distance.toDouble ?? 0).inchToMile()
					controller.breathingRate = Int(exerciseSessionObj.breathingRate.toDouble ?? 0)
					controller.temperatureReading = (exerciseSessionObj.skinTemp.toDouble ?? 0)
					//				controller.heartRate = Int(exerciseSessionObj.toDouble ?? 0)
					controller.isCameFromActivitySession = true
					controller.totalTime = ((Int(exerciseSessionObj.sessionTimeInHrs.toDouble ?? 0) * 3600) + (Int(exerciseSessionObj.sessionTimeInMinutes.toDouble ?? 0) * 60) + Int(exerciseSessionObj.sessionTimeInSeconds.toDouble ?? 0))
					controller.startTimeForGraph = exerciseSessionObj.startTime
					controller.endTimeForGraph = exerciseSessionObj.endTime
					parent?.navigationController?.pushViewController(controller, animated: true)
				}
			}
		}else if tableView == takePauseSessionTableView{
			if(takePauseSessionDataModels.count>0){
				let pauseSessionObj:TakePauseSessionDataModel = takePauseSessionDataModels[indexPath.row]
				if pauseSessionObj.heartRateValue != "-" && pauseSessionObj.bloodPressureValue != "-/-" {
					let controller = BPReadingViewController.instantiate(fromAppStoryboard: .dashboard)
					controller.breathingRateReading = Int(pauseSessionObj.breathingRate.toDouble ?? 0)
					controller.temperatureReading = (pauseSessionObj.skinTemp.toDouble ?? 0)
					controller.heartRate = pauseSessionObj.heartRateValue
					controller.bpReading = pauseSessionObj.bloodPressureValue
					controller.isCameFromActivitySession = true
					parent?.navigationController?.pushViewController(controller, animated: true)
				}
			}
		}
	}
    
    @objc func moreSessionButtonClicked(sender: MovanoButton) {
        if(sender.tag == 0){
            noOfExerciseRows = exerciseSessionDataModels.count
            hideExerciseSessionMoreButton = true
			isSessionExpanded = !isSessionExpanded
			if !isSessionExpanded {
				noOfExerciseRows = 1
			}
            exerciseSessionTableView.reloadData()
        }else if(sender.tag == 1){
            noOfTakePauseRows = takePauseSessionDataModels.count
            hideTakePauseSessionMoreButton = true
            takePauseSessionTableView.reloadData()
        }
        sender.isHidden = true

    }
}
