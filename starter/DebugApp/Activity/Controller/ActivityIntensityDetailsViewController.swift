//
//  ActivityIntensityDetailsViewController.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 24/11/21.
//

import UIKit
import Highcharts

// swiftlint:disable all
class ActivityIntensityDetailsViewController: UIViewController {

    //MARK:IB Outlets
    
    @IBOutlet weak var ghraphTypeLabel: MovanoLabel!
    @IBOutlet weak var avgValueLable: MovanoLabel!
    @IBOutlet weak var barSegmentedControl: UISegmentedControl!
    @IBOutlet weak var selectionIndicatorRight: UIView!
    @IBOutlet weak var selectionIndicatorLeft: UIView!
    @IBOutlet weak var slidingView: UIView!
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var graphContainerView: UIView!
    @IBOutlet weak var dateLabel: MovanoLabel!
	@IBOutlet weak var chartView: HIChartView!
	@IBOutlet weak var noChartDataView: UIView!
	@IBOutlet weak var highTimeLabel: MovanoLabel!
	@IBOutlet weak var mediumTimeLabel: MovanoLabel!
	@IBOutlet weak var lowTimeLabel: MovanoLabel!
	@IBOutlet weak var inactiveTimeLabel: MovanoLabel!
	@IBOutlet weak var avgStaticLabel: MovanoLabel!
	@IBOutlet weak var segmentedControlUIView: UIView!
	
    var activityChartSelected = true
	var dataRangeType: DateRangeType = .daily
	var dataModel = [ActivityChartModel]()
	var dataType: DetailDataType!
	var presenter: ActivityIntensityDetailsPresenter?
	var delegate: SleepHistoryDataViewControllerProtocol?
	var selectedDateRangeForDaily: DateRange!
	var currentAwakeStartDate: Date?
	var currentAwakeEndDate: Date?
	var chartDataForDailyIntensity: ActivityChartData?
	var chartDataForWeeklyIntensity: ActivityChartData?
	var chartDataForWeek: ActivityChartData?
	var avgHeartRate = 0
	var avgIntensity = 0
	var highActivityTime = 0
	var mediumActivityTime = 0
	var lowActivityTime = 0
	var inactiveActivityTime = 0
	
	var selectedDateRange: DateRange! {
		didSet {
//			hud.show(in: self.view)
			if dataRangeType == .daily {
				if dataType == .heartRateReading {
					dataModel = [ActivityChartModel(type: "Heart Rate", value: NSNumber(value: 0), chartData: ActivityChartData(barColor: .clear, elements: []))]
				}
			} else if dataRangeType == .weekly {
				
			}
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerView.currentController = self
        self.headerView.isInNavigation = true
        self.headerView.updateDate = false
        
        // Custom header view
        headerView.leftArrow.isHidden = true
        headerView.rightArrow.isHidden = true
        headerView.calendarView.isHidden = true
        
        dateLabel.text = headerView.selectedDate.end.dateToString(format: "d MMM, yyyy")
        
        barSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        barSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .normal)
		/* Disabled swipe gesture
		 addSwipwGesture()
		 */
		self.registerPresenters()
		self.updateActivityTime()
		
		var sum = 0
		for z in 0..<(self.chartDataForDailyIntensity?.elements.count ?? 0) {
			sum = sum + Int(chartDataForDailyIntensity?.elements[z].value?.rounded() ?? 0)
		}
		let averageValue = (self.chartDataForDailyIntensity?.elements.count != 0) ? sum / (chartDataForDailyIntensity?.elements.count ?? 0) : 0
		self.avgValueLable.text = (averageValue > 3) ? ((averageValue > 7) ? "High" : "Medium") : "Low"
		self.ghraphTypeLabel.text = "Activity Intensity"
		fetchActivityIntensityChart(chartData: self.chartDataForDailyIntensity ?? ActivityChartData(barColor: .purple, elements: []))
		
		segmentedControlUIView.clipsToBounds = true
		segmentedControlUIView.layer.cornerRadius = 2.0
		segmentedControlUIView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		
		/*
		 // avg label layer
		 avgStaticLabel.layer.borderWidth = 1.0
		 avgStaticLabel.layer.borderColor = UIColor.hexColor(hex: "#ffffff")?.cgColor
		 avgStaticLabel.layer.opacity = 0.5
		 avgStaticLabel.layer.cornerRadius = 2.0
		 avgStaticLabel.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		 */
    }
    
    func addSwipwGesture(){
        slidingView.isUserInteractionEnabled = true
        let swipeRecognizerRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeDetected(_:)))
        swipeRecognizerRight.direction = .right
        slidingView.addGestureRecognizer(swipeRecognizerRight)
        
        let swipeRecognizerLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeDetected(_:)))
        swipeRecognizerLeft.direction = .left
        slidingView.addGestureRecognizer(swipeRecognizerLeft)
    }
    
	func registerPresenters() {
		if self.presenter == nil {
			let stepCountService = StepCountWebService()
			let pulseRateService = PulseRateWebService()
			
			self.presenter = ActivityIntensityDetailsPresenter(pulseRateService: pulseRateService, stepCountService: stepCountService, delegate: self)
		}
	}
	
	@objc func swipeDetected(_ sender: UIGestureRecognizer) {
		hud.show(in: self.view)
		if let swipeGesture = sender as? UISwipeGestureRecognizer {
			switch swipeGesture.direction {
			case UISwipeGestureRecognizer.Direction.right:
				//SELECTED ACTIVITY GRAPH
				self.activityChartSelected = true
				print("Swiped right")
				
			case UISwipeGestureRecognizer.Direction.left:
				//SELECTED HEART RATE GRAPH
				self.activityChartSelected = false
				print("Swiped left")
				
			default:
				break
			}
			updateChartViews()
		}
	}
    
    func updateChartViews(){
        if(activityChartSelected){
            selectionIndicatorLeft.backgroundColor = UIColor.systemBlue
            selectionIndicatorRight.backgroundColor = UIColor.white
            ghraphTypeLabel.text = "Activity Intensity"

            //Set ghraph data depending on weekly or daily for activity
			if dataRangeType == .daily {
				var sum = 0
				for x in 0..<(self.chartDataForDailyIntensity?.elements.count ?? 0) {
					sum = sum + Int(self.chartDataForDailyIntensity?.elements[x].value?.rounded() ?? 0)
				}
				let averageValue = (self.chartDataForDailyIntensity?.elements.count != 0) ? sum / (self.chartDataForDailyIntensity?.elements.count ?? 0) : 0
				self.avgValueLable.text = (averageValue > 3) ? ((averageValue > 7) ? "High" : "Medium") : "Low"
				self.ghraphTypeLabel.text = "Activity Intensity"
				self.fetchActivityIntensityChart(chartData: self.chartDataForDailyIntensity!)
			} else {
				if chartDataForWeeklyIntensity != nil {
					var sum = 0
					for x in 0..<(self.chartDataForWeeklyIntensity?.elements.count ?? 0) {
						sum = sum + Int(self.chartDataForWeeklyIntensity?.elements[x].value?.rounded() ?? 0)
					}
					let averageValue = (self.chartDataForWeeklyIntensity?.elements.count != 0) ? sum / (self.chartDataForWeeklyIntensity?.elements.count ?? 0) : 0
					self.avgValueLable.text = (averageValue > 3) ? ((averageValue > 7) ? "High" : "Medium") : "Low"
					self.ghraphTypeLabel.text = "Activity Intensity"
					
					self.fetchActivityIntensityChart(chartData: self.chartDataForWeeklyIntensity!)
				} else {
					self.presenter?.fetchWeeklyData(weekStartdate: selectedDateRange.start.dateToString(format: DateFormat.serverDateFormat), weekEndDate: selectedDateRange.end.dateToString(format: DateFormat.serverDateFormat), activityChartSelected: self.activityChartSelected)
				}
			}
            
        }else{
            selectionIndicatorLeft.backgroundColor = UIColor.white
            selectionIndicatorRight.backgroundColor = UIColor.systemBlue
            ghraphTypeLabel.text = "Heart Rate"

            //Set ghraph data depending on weekly or daily for heart rate
			if dataRangeType == .daily {
				let tempRange = DateRange(start: currentAwakeStartDate!, end: currentAwakeEndDate!, type: .daily)
				self.presenter?.fetchPulseRateData(timeStampRange: tempRange.intervalRange, customDateRange: tempRange)
			} else {
				let tempRange = DateRange(currentAwakeStartDate!, type: dataRangeType)
				self.presenter?.fetchWeeklyData(weekStartdate: tempRange.start.dateToString(format: DateFormat.serverDateFormat), weekEndDate: tempRange.end.dateToString(format: DateFormat.serverDateFormat), activityChartSelected: self.activityChartSelected)
			}
			
        }
    }
	
	@IBAction func switchChartView(_ sender: UISegmentedControl) {
		hud.show(in: self.view)
		let tempRange: DateRange
		if sender.selectedSegmentIndex == 0 {
			dataRangeType = .daily
			tempRange = DateRange(start: currentAwakeStartDate!, end: currentAwakeEndDate!, type: .daily)
			selectedDateRange = tempRange
			updateChartViews()
		} else {
			dataRangeType = .weekly
			tempRange = DateRange(currentAwakeStartDate!, type: dataRangeType)
			selectedDateRange = tempRange
			self.presenter?.fetchWeeklyData(weekStartdate: tempRange.start.dateToString(format: DateFormat.serverDateFormat), weekEndDate: tempRange.end.dateToString(format: DateFormat.serverDateFormat), activityChartSelected: self.activityChartSelected)
		}
		
		if selectedDateRangeForDaily != nil && selectedDateRangeForDaily.start != selectedDateRange.start && dataRangeType == .daily {
			selectedDateRange = selectedDateRangeForDaily
		}
	}
	
	private func fetchActivityChart(chartData: ActivityChartData, limitLineValues: [Int] = []) {
		
		var xAxis = [TimeInterval]()
		xAxis = chartData.elements.map({ (element) -> TimeInterval in
			return element.date.timeIntervalSince1970
		})
		
		var data = [[Double?]]()
		
		for (pointIndex, each) in chartData.elements.enumerated() {
			data.append([xAxis[pointIndex] * 1000, each.value])
		}
		
		var tempChartElements = [ChartElement]()
		for m in 0..<chartData.elements.count {
			if chartData.elements[m].value != nil && chartData.elements[m].value != 0 {
				tempChartElements.append(chartData.elements[m])
			}
		}
		
		let minValueForHR = tempChartElements.reduce(tempChartElements.last!) { aggregate, element in
			(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
		}.value ?? 0
		
		let maxValueForHR = chartData.elements.reduce(chartData.elements.last!) { aggregate, element in
			(aggregate.value ?? 0) < (element.value ?? 0) ? element : aggregate
		}.value ?? 0
		
		let values = [data]
		let chart = HIChart()
		chart.type = "line"
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
		if dataRangeType == .daily {
			hixAxis.dateTimeLabelFormats.day = HIDay()
			hixAxis.dateTimeLabelFormats.day.main = "%l %p"
			hixAxis.dateTimeLabelFormats.hour = HIHour()
			hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
			hixAxis.minRange = NSNumber(value: 3600 * 6000)
			hixAxis.tickInterval = NSNumber(value: 3600 * 6000)
		} else if dataRangeType == .weekly {
			hixAxis.dateTimeLabelFormats.day = HIDay()
			hixAxis.dateTimeLabelFormats.day.main = "%a"
			hixAxis.minRange = NSNumber(value: 3600 * 24000)
			hixAxis.tickInterval = NSNumber(value: 3600 * 24000)
		}
		
		if dataRangeType == .daily {
			if currentAwakeStartDate != nil {
				let hour = Calendar.current.component(.hour, from: currentAwakeStartDate!)
				let newAwakeStart = currentAwakeStartDate?.setTime(hour: hour, min: 0, sec: 0)
				hixAxis.min = NSNumber(value: (1000 * newAwakeStart!.timeIntervalSince1970))
			}
			
			if currentAwakeEndDate != nil {
				let hour = Calendar.current.component(.hour, from: currentAwakeEndDate!)
				let newAwakeEnd = currentAwakeEndDate?.setTime(hour: (hour + 1), min: 0, sec: 0)
				hixAxis.max = NSNumber(value: (1000 * newAwakeEnd!.timeIntervalSince1970))
			}
		}
	
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		hixAxis.tickWidth = 2
		hixAxis.lineWidth = 0
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		
		if dataRangeType == .daily {
			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.isFirst || this.isLast) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + ' ' + ampm; return strTime } else { return '' } }")
		} else if dataRangeType == .weekly {
			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ let weekDaysArray = ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa']; let theDay = new Date(this.value).getDay(); return weekDaysArray[theDay] }")
		}
		
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.overflow = "justify"
		yAxis.labels.style = HICSSObject()
		yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		yAxis.labels.style.fontSize = "12"
		yAxis.labels.style.color = "#a5a5b2"
		yAxis.gridLineWidth = 0
		
		yAxis.max = NSNumber(value: Int(maxValueForHR) + 1)
		yAxis.min = NSNumber(value: Int(minValueForHR) - 1)
		yAxis.tickInterval = NSNumber(value: 1)
		yAxis.minTickInterval = NSNumber(value: 1)
		
		let plotLines1 = HIPlotLines()
		plotLines1.value = yAxis.min
		plotLines1.width = 1
		plotLines1.color = HIColor(uiColor: .white)
		yAxis.plotLines = [plotLines1]
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
//
		hixAxis.labels.enabled = true
		hixAxis.gridLineWidth = 0
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		
		let plotoptions = HIPlotOptions()
		plotoptions.line = HILine()
		plotoptions.line.states = HIStates()
		plotoptions.line.states.inactive = HIInactive()
		plotoptions.line.states.inactive.opacity = NSNumber(value: true)
		plotoptions.line.animation = HIAnimationOptionsObject()
		plotoptions.line.animation.duration = NSNumber(value: 0)
		plotoptions.line.marker = HIMarker()
		plotoptions.line.marker.enabled = false
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		let marker = HIMarker()
		marker.enabled = true
		
		var series = [HISeries]()
		let line = HILine()
		line.name = "Data"
		line.data = values[0] as [Any]
		if line.data.count == 1 {
			line.marker = marker
		}
		series.append(line)
		
		let uiColors = [chartData.barColor.hexString]
		
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = series
		options.colors = uiColors
		options.plotOptions = plotoptions
		options.time = time
		options.legend = legend
		
		self.chartView.options = options
		
		hud.dismiss(animated: true)
	}
	
	private func fetchActivityIntensityChart(chartData: ActivityChartData) {
		
		let data = chartData.elements.reduce(0.0, { partialResult, element in
			return partialResult + (element.value ?? 0)
		})
		if data == 0 {
			self.chartView.isHidden = true
			self.noChartDataView.isHidden = false
			
		} else {
			self.chartView.isHidden = false
			self.noChartDataView.isHidden = true
			
			var xAxis = [TimeInterval]()
			xAxis = chartData.elements.map({ (element) -> TimeInterval in
				return element.date.timeIntervalSince1970
			})
			
			var data = [[Double?]]()
			
			for (pointIndex, each) in chartData.elements.enumerated() {
				data.append([xAxis[pointIndex] * 1000, each.value])
			}
			let values = [data]
			let chart = HIChart()
			chart.type = "column"
			chart.backgroundColor = HIColor(uiColor: UIColor.clear)
			
			let hititle = HITitle()
			hititle.text = ""
			
			let hisubtitle = HISubtitle()
			hisubtitle.text = ""
			
			let hixAxis = HIXAxis()
			hixAxis.type = "datetime"
			hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
			if dataRangeType == .daily {
				hixAxis.dateTimeLabelFormats.day = HIDay()
				hixAxis.dateTimeLabelFormats.day.main = "%l %p"
				hixAxis.dateTimeLabelFormats.hour = HIHour()
				hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
				hixAxis.minRange = NSNumber(value: 3600 * 3000)
				hixAxis.tickInterval = NSNumber(value: 3600 * 3000)
			} else if dataRangeType == .weekly {
				hixAxis.dateTimeLabelFormats.day = HIDay()
				hixAxis.dateTimeLabelFormats.day.main = "%a"
				hixAxis.minRange = NSNumber(value: 3600 * 500)
				hixAxis.tickInterval = NSNumber(value: 3600 * 24000)
			}
			
			let hixAxisTitle = HITitle()
			hixAxisTitle.text = ""
			hixAxis.title = hixAxisTitle
			
			if dataRangeType == .weekly {
//				hixAxis.min = NSNumber(value: ((chartData.elements.first?.date.startOfDay.timeIntervalSince1970)!) * 1000)
//				hixAxis.max = NSNumber(value: ((chartData.elements.last?.date.endOfDay.timeIntervalSince1970)!) * 1000)
			} else {
				if currentAwakeStartDate != nil {
					let hour = Calendar.current.component(.hour, from: currentAwakeStartDate!)
					let newAwakeStart = currentAwakeStartDate?.setTime(hour: hour, min: 0, sec: 0)
					hixAxis.min = NSNumber(value: (1000 * newAwakeStart!.timeIntervalSince1970))
				}
				
				if currentAwakeEndDate != nil {
					let hour = Calendar.current.component(.hour, from: currentAwakeEndDate!)
					let newAwakeEnd = currentAwakeEndDate?.setTime(hour: (hour + 1), min: 0, sec: 0)
					hixAxis.max = NSNumber(value: (1000 * newAwakeEnd!.timeIntervalSince1970))
				}
			}
			hixAxis.tickWidth = 2
			hixAxis.lineWidth = 0
			hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
			
			hixAxis.labels = HILabels()
			hixAxis.labels.style = HICSSObject()
			hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			hixAxis.labels.style.fontSize = "12"
			hixAxis.labels.style.color = "#a5a5b2"
//			hixAxis.labels.rotation = NSNumber(value: 0.00001)
//			hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.isFirst || this.isLast || this.value) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + '' + ampm; return strTime } else { return '' } }")
			
			if dataRangeType == .daily {
				hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ if(this.isFirst || this.isLast) { let theHours = new Date(this.value).getHours(); var ampm = ((theHours >= 12) ? 'pm' : 'am'); theHours = ((theHours > 12) ? (theHours-12) : ((theHours == 0) ? 12 : theHours)); var strTime = theHours + ' ' + ampm; return strTime } else { return '' } }")
			} else if dataRangeType == .weekly {
				hixAxis.labels.formatter = HIFunction(jsFunction: "function(){ let weekDaysArray = ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa']; let theDay = new Date(this.value).getDay(); return weekDaysArray[theDay] }")
			}
			
			let yAxis = HIYAxis()
			yAxis.title = HITitle()
			yAxis.title.text = ""
			yAxis.labels = HILabels()
			yAxis.labels.enabled = true
			yAxis.labels.overflow = "justify"
			yAxis.labels.style = HICSSObject()
			yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			yAxis.labels.style.fontSize = "12"
			yAxis.labels.style.color = "#a5a5b2"
			yAxis.min = 0
			yAxis.max = 10
			yAxis.tickInterval = NSNumber(value: 1)
			yAxis.tickColor = HIColor(uiColor: UIColor.clear)
			yAxis.gridLineColor = HIColor(uiColor: UIColor.clear)
			//yAxis.lineColor = HIColor(uiColor: UIColor.red)
			yAxis.lineWidth = 0
			yAxis.tickLength = 0
			yAxis.tickPixelInterval = NSNumber(value: 1)
			yAxis.labels.formatter = HIFunction(jsFunction: "function(){ if (this.value == 3){ return 'Low' } else if(this.value == 7){ return 'Med'} else if(this.value == 10){ return 'High'} else{return ''}}")
			
			let plotLines1 = HIPlotLines()
			plotLines1.value = yAxis.min
			plotLines1.width = 1
			plotLines1.color = HIColor(uiColor: .white)
			yAxis.plotLines = [plotLines1]
			let tooltip = HITooltip()
			tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
			tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
			tooltip.footerFormat = "</table>"
			tooltip.shared = true
			tooltip.useHTML = true
			tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
			
			let legend = HILegend()
			legend.enabled = NSNumber(value: false)
			
			let plotoptions = HIPlotOptions()
			plotoptions.column = HIColumn()
			plotoptions.column.states = HIStates()
			plotoptions.column.states.inactive = HIInactive()
			plotoptions.column.states.inactive.opacity = NSNumber(value: true)
			hixAxis.labels.enabled = true
			hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
			plotoptions.column.animation = HIAnimationOptionsObject()
			plotoptions.column.animation.duration = NSNumber(value: 0)
			//		plotoptions.column.pointWidth = 300
			let credits = HICredits()
			credits.enabled = NSNumber(value: false)
			
			let exporting = HIExporting()
			exporting.enabled = false
			
			var series = [HISeries]()
			let bar = HIColumn()
			bar.name = "Data"
			bar.data = values[0] as [Any]
			bar.borderColor = HIColor(uiColor: UIColor.clear)
			let zone1 = HIZones()
			zone1.value = NSNumber(value: 7)
			zone1.color = HIColor(uiColor: chartData.barColor)
			bar.zones = [zone1]
			bar.zoneAxis = "y"
			bar.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
									[0, "rgb(205 32 170)"],
									[0.25, "rgb(89 63 223)"]])
			series.append(bar)
			
			let uiColors = [UIColor.barPink.hexString]
			
			let time =  HITime()
			time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
			
			let options = HIOptions()
			options.chart = chart
			options.title = hititle
			options.subtitle = hisubtitle
			options.xAxis = [hixAxis]
			options.yAxis = [yAxis]
			options.tooltip = tooltip
			options.credits = credits
			options.exporting = exporting
			options.series = series
			options.colors = uiColors
			options.plotOptions = plotoptions
			options.time = time
			options.legend = legend
			
			self.chartView.options = options
		}
		hud.dismiss(animated: true)
	}
	
	func updateActivityTime() {
		if highActivityTime > 60 {
			let hour: NSMutableAttributedString = NSMutableAttributedString(attributedString: readingData(value: "\(highActivityTime / 60)", unit: "h", iconName: ""))
			let min = readingData(value: " \(highActivityTime % 60)", unit: "m", iconName: "")
			hour.append(min)
			highTimeLabel.attributedText = hour
		} else {
			let min = readingData(value: "\(highActivityTime)", unit: "m", iconName: "")
			highTimeLabel.attributedText = min
		}
		
		if mediumActivityTime > 60 {
			let hour: NSMutableAttributedString = NSMutableAttributedString(attributedString: readingData(value: "\(mediumActivityTime / 60)", unit: "h", iconName: ""))
			let min = readingData(value: " \(mediumActivityTime % 60)", unit: "m", iconName: "")
			hour.append(min)
			mediumTimeLabel.attributedText = hour
		} else {
			let min = readingData(value: "\(mediumActivityTime)", unit: "m", iconName: "")
			mediumTimeLabel.attributedText = min
		}
		
		if lowActivityTime > 60 {
			let hour: NSMutableAttributedString = NSMutableAttributedString(attributedString: readingData(value: "\(lowActivityTime / 60)", unit: "h", iconName: ""))
			let min = readingData(value: " \(lowActivityTime % 60)", unit: "m", iconName: "")
			hour.append(min)
			lowTimeLabel.attributedText = hour
		} else {
			let min = readingData(value: "\(lowActivityTime)", unit: "m", iconName: "")
			lowTimeLabel.attributedText = min
		}
		
		if inactiveActivityTime > 60 {
			let hour: NSMutableAttributedString = NSMutableAttributedString(attributedString: readingData(value: "\(inactiveActivityTime / 60)", unit: "h", iconName: ""))
			let min = readingData(value: " \(inactiveActivityTime % 60)", unit: "m", iconName: "")
			hour.append(min)
			inactiveTimeLabel.attributedText = hour
			
		} else {
			let min = readingData(value: "\(inactiveActivityTime)", unit: "m", iconName: "")
			inactiveTimeLabel.attributedText = min
		}
	}
}

extension ActivityIntensityDetailsViewController: ActivityIntensityDetailsDelegate {
	func fetchPulseRateData(result: Result<[CoreDataPulseRate], MovanoError>, customDateRange: DateRange) {
		switch result {
		case .success(let dataList):
			dataModel = []
			let totalPulseSum = dataList.reduce(0) { (result: Double, nextItem: CoreDataPulseRate) -> Double in
				return result + nextItem.value.doubleValue
			}
			
			let pulseCountAvg = (dataList.count != 0) ? (totalPulseSum)/Double(dataList.count) : 0
			avgHeartRate = Int(pulseCountAvg.rounded())
			
			self.updatePulserateChart(pulseCountAvg: pulseCountAvg, customDateRange: customDateRange, type: .daily)
		case .failure(let error):
			print(error)
		}
		hud.dismiss()
	}
	
	func updatePulserateChart(pulseCountAvg: Double, customDateRange: DateRange, type: DateRangeType) {
		var pulseRateElements = [ChartElement(date: Date())]
		
		if customDateRange.type == .weekly {
			pulseRateElements = CoreDataSleepTime.fetchWeekAvgChartData(dateRange: customDateRange, dataType: .heartRateReading)
			let chartData = ActivityChartData(barColor: UIColor.barPurple, elements: pulseRateElements)
			let pulseRateModel = ActivityChartModel(type: "Heart Rate", value: NSNumber(value: Int((pulseCountAvg * 100).rounded() / 100)), chartData: chartData)
			
			dataModel = [pulseRateModel]
		} else {
			pulseRateElements = CoreDataPulseRate.fetchPulseRateChartData(dateRange: customDateRange)
			var newPulseRateElements:[ChartElement] = []
			for x in 0..<pulseRateElements.count {
				if pulseRateElements[x].value != nil {
					newPulseRateElements.append(pulseRateElements[x])
				}
			}
			
			let dataList = CoreDataPulseRate.fetch(for: customDateRange.dateRange)
			let totaPulseSum = dataList.reduce(0) { (result: Double, nextItem: CoreDataPulseRate) -> Double in
				return result + nextItem.value.doubleValue
			}
			
			let pulseAvg = (dataList.count != 0) ? (totaPulseSum)/Double(dataList.count) : 0
			let chartData = ActivityChartData(barColor: UIColor.barPurple, elements: newPulseRateElements)
			let pulseRateModel = ActivityChartModel(type: "Heart Rate", value: NSNumber(value: Int(pulseAvg.rounded())), chartData: chartData)
			
			dataModel = [pulseRateModel]
		}
		if dataModel[0].chartData.elements.count > 0 {
			self.avgValueLable.attributedText = readingData(value: "\(avgHeartRate)", unit: "bpm", iconName: "")
			self.ghraphTypeLabel.text = "Heart Rate"
			self.fetchActivityChart(chartData: dataModel[0].chartData)
		}
	}
	
	func fetchIntensityData(result: Result<[CoreDataActivity], MovanoError>, customDateRange: DateRange) {
		
	}
	
	func updateIntensityChart(IntensityAvg: Double, customDateRange: DateRange, type: DateRangeType) {
		
	}
	
	func loadWeeklyChart(chartElements: [ChartElement]) {
		let chartData = ActivityChartData(barColor: UIColor.barPurple, elements: chartElements)
		let pulseRateModel = ActivityChartModel(type: "Heart Rate", value: NSNumber(value: 40), chartData: chartData)
		
		dataModel = [pulseRateModel]
		var averageValue = 0
		var sum = 0
		for z in 0..<chartElements.count {
			sum = sum + Int(chartElements[z].value?.rounded() ?? 0)
		}
		averageValue = (chartElements.count != 0) ? sum / chartElements.count : 0
		if activityChartSelected {
			self.avgValueLable.text = (averageValue > 3) ? ((averageValue > 7) ? "High" : "Medium") : "Low"
			self.ghraphTypeLabel.text = "Activity Intensity"
		} else {
			self.avgValueLable.attributedText = readingData(value: "\(averageValue)", unit: "bpm", iconName: "")
			self.ghraphTypeLabel.text = "Heart Rate"
		}
		
		if dataModel[0].chartData.elements.count > 0 {
			if activityChartSelected {
				self.chartDataForWeeklyIntensity = chartData
				if chartDataForWeeklyIntensity != nil {
					self.fetchActivityIntensityChart(chartData: self.chartDataForWeeklyIntensity!)
				}
			} else {
				self.fetchActivityChart(chartData: dataModel[0].chartData)
			}
		} else {
			self.chartDataForWeeklyIntensity = chartData
			if chartDataForWeeklyIntensity != nil {
				self.fetchActivityIntensityChart(chartData: self.chartDataForWeeklyIntensity!)
			}

			hud.dismiss(animated: true)
		}
	}
	
	func readingData(value: String, unit: String?, iconName: String?) -> NSAttributedString {
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.semiBold(withSize: 20)]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.regular(withSize: 12),
					  convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.hexColor(hex: "#B7B7C7")]
		
		let attributedString = NSMutableAttributedString(string: value, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1))
		if let unit = unit {
			attributedString.append(NSMutableAttributedString(string: "  " + unit, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		}
		return attributedString
	}
}

// MARK: - Calendar Callbacks
extension ActivityIntensityDetailsViewController {
	
	@objc private func calendarDateSelected(notification: NSNotification) {
		guard let object = notification.object as? DateRange else {
			return
		}
		
		self.selectedDateRange = object
		selectedDateRangeForDaily = object
		
		selectedDateRange = DateRange(object.start, type: dataRangeType)
		
		let tempRange: DateRange
		if dataRangeType == .daily {
			let dateRange = DateRange(start: currentAwakeStartDate!, end: currentAwakeEndDate!, type: .daily)
			tempRange = dateRange
		} else {
			tempRange = selectedDateRange
		}
		
		self.presenter?.fetchPulseRateData(timeStampRange: tempRange.intervalRange, customDateRange: tempRange)
	}
}
