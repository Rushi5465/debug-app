//
//  ActivityDetailViewController.swift
//  BloodPressureApp
//
//  Created by Rushikant on 18/08/21.
//

import UIKit
import HGCircularSlider
import Highcharts

// swiftlint:disable all
class ActivityDetailViewController: UIViewController {
	
	@IBOutlet weak var headerView: HeaderView!
	@IBOutlet weak var barSegmentedControl: UISegmentedControl!
	@IBOutlet weak var chartSuperView: UIView!
	@IBOutlet weak var barChartView: HIChartView!
	@IBOutlet weak var avgStepsLabel: MovanoLabel!
	@IBOutlet weak var distanceLabel: MovanoLabel!
	@IBOutlet weak var calorieLabel: MovanoLabel!
	@IBOutlet weak var stepCountInfoView: UIView!
	@IBOutlet weak var goalLabel: MovanoLabel!
	@IBOutlet weak var activityTypeLabel: MovanoLabel!
	@IBOutlet weak var noChartView: UIView!
	
	// Circular Slider
	@IBOutlet weak var circularGoalSlider: CircularSlider!
	@IBOutlet weak var completedStepsLabel: MovanoLabel!
	
	// ChartView for PieChart
	@IBOutlet weak var goalPieChart: HIChartView!
	
	@IBOutlet weak var currentDateLabel: MovanoLabel!
	@IBOutlet weak var segmentedControlUIView: UIView!
	@IBOutlet weak var averageLabel: MovanoLabel!
	@IBOutlet weak var peakLabel: MovanoLabel!
	@IBOutlet weak var stepCountInfoHeightConstraint: NSLayoutConstraint!
	@IBOutlet weak var screenTitleLabel: MovanoLabel!
	
	@IBOutlet weak var graphGoalLabel: MovanoLabel!
	@IBOutlet weak var graphGoalHeightConstraint: NSLayoutConstraint!
	
	@IBOutlet weak var stepCountGapConstraint: NSLayoutConstraint!
	
	var dataType: ActivityDataType!
	var elementColor: UIColor!
	var totalGoalValue: Int?
	var currentAwakeStartDate: Date?
	var currentAwakeEndDate: Date?
	var isCameFromDashboard = false
	
	var presenter: ActivityDetailPresenter?
	
	var selectedDateRange: DateRange! {
		didSet {
            hud.show(in: self.view)
			self.barSegmentedControl.selectedSegmentIndex = 0
			
			if currentAwakeStartDate != nil && currentAwakeEndDate != nil {
//				let dateRange = DateRange(start: currentAwakeStartDate!, end: currentAwakeEndDate!, type: .daily)
				self.presenter?.fetchData(range: selectedDateRange, elementColor: elementColor, dataType: dataType,currentAwakeStartDate: currentAwakeStartDate!, currentAwakeEndDate: currentAwakeEndDate!, isUpdateRequired: true)
			} else {
				if selectedDateRange.start == Date().startOfDay {
					let newRange = DateRange(start: selectedDateRange.start, end: Date(), type: selectedDateRange.type)
					self.presenter?.fetchData(range: newRange, elementColor: elementColor, dataType: dataType,currentAwakeStartDate: currentAwakeStartDate ?? Date(), currentAwakeEndDate: currentAwakeEndDate ?? Date(), isUpdateRequired: true, isCameFromDashboard: true)
				} else {
					self.presenter?.fetchData(range: selectedDateRange, elementColor: elementColor, dataType: dataType,currentAwakeStartDate: currentAwakeStartDate ?? Date(), currentAwakeEndDate: currentAwakeEndDate ?? Date(), isUpdateRequired: true, isCameFromDashboard: true)
				}
			}
			
		}
	}
	
	var activityChartData: ActivityChartData!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.circularGoalSlider.trackFillColor = elementColor
		
		self.headerView.currentController = self
		self.headerView.isInNavigation = true
		self.headerView.updateDate = false
		
		// Custom header view
		headerView.leftArrow.isHidden = true
		headerView.rightArrow.isHidden = true
		headerView.calendarView.isHidden = true
		
		currentDateLabel.text = headerView.selectedDate.end.dateToString(format: "d MMM, yyyy")
		
		self.stepCountInfoView.isHidden = (dataType == .stepCount) ? false : true
		self.activityTypeLabel.text = (dataType == .stepCount) ? "Steps" : "Calories Burned"
		self.stepCountInfoHeightConstraint.constant = (dataType == .stepCount) ? 60 : 0
		
		barSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
		barSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .normal)
		
		circularGoalSlider.isUserInteractionEnabled = false
		
		self.registerPresenters()
		
		self.selectedDateRange = DateRange(DateManager.current.selectedDate.start, type: .daily)
		
		segmentedControlUIView.clipsToBounds = true
		segmentedControlUIView.layer.cornerRadius = 2.0
		segmentedControlUIView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		
		/*
		 averageLabel.layer.borderWidth = 1.0
		 averageLabel.layer.borderColor = UIColor.hexColor(hex: "#ffffff")?.cgColor
		 averageLabel.layer.opacity = 0.5
		 averageLabel.layer.cornerRadius = 2.0
		 averageLabel.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		 */
		
		screenTitleLabel.text = (dataType == .stepCount) ? "Steps" : "Calories Burned"
//		barChartView.isUserInteractionEnabled = false
		graphGoalHeightConstraint.constant = 0
		stepCountGapConstraint.constant = (dataType == .stepCount) ? 24 : 84
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.calendarDateSelected), name: NSNotification.Name("calendarDateSelected"), object: nil)
	}
		
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		isCameFromDashboard = false
		
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name("calendarDateSelected"), object: nil)
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let stepCountService = StepCountWebService()
			self.presenter = ActivityDetailPresenter(stepCountService: stepCountService, chartDelegate: self, delegate: self)
		}
	}
	
	@IBAction func switchView(_ sender: UISegmentedControl) {
		var range: DateRange!
		if sender.selectedSegmentIndex == 0 {
			if currentAwakeStartDate != nil && currentAwakeEndDate != nil {
//				range = DateRange(start: currentAwakeStartDate!, end: currentAwakeEndDate!, type: .daily)
				range = headerView.selectedDate
			} else {
				range = headerView.selectedDate
			}
			graphGoalHeightConstraint.constant = 0
		} else {
			range = DateRange(headerView.selectedDate.start, type: .weekly)
			graphGoalHeightConstraint.constant = 24
		}
		hud.show(in: self.view)
		if !isCameFromDashboard {
			self.presenter?.fetchData(range: range, elementColor: elementColor, dataType: dataType, currentAwakeStartDate: currentAwakeStartDate!, currentAwakeEndDate: currentAwakeEndDate!, isUpdateRequired: false)
		} else {
			if selectedDateRange.start == Date().startOfDay {
				var newRange = DateRange(start: selectedDateRange.start, end: Date(), type: (sender.selectedSegmentIndex == 0) ? .daily : .weekly)
				if sender.selectedSegmentIndex == 0 {
					newRange = DateRange(start: selectedDateRange.start, end: Date(), type: (sender.selectedSegmentIndex == 0) ? .daily : .weekly)
				} else {
					newRange = DateRange(headerView.selectedDate.start, type: .weekly)
				}
				
				self.presenter?.fetchData(range: newRange, elementColor: elementColor, dataType: dataType,currentAwakeStartDate: currentAwakeStartDate ?? Date(), currentAwakeEndDate: currentAwakeEndDate ?? Date(), isUpdateRequired: false, isCameFromDashboard: true)
			} else {
				self.presenter?.fetchData(range: range, elementColor: elementColor, dataType: dataType,currentAwakeStartDate: currentAwakeStartDate ?? Date(), currentAwakeEndDate: currentAwakeEndDate ?? Date(), isUpdateRequired: false, isCameFromDashboard: true)
			}
		}
		
	}
	
	func readingData(value: String, unit: String?) -> NSAttributedString {
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.semiBold(withSize: 18)]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.regular(withSize: 12),
					  convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.hexColor(hex: "#ffffff")?.withAlphaComponent(0.7)]
		
		let attributedString = NSMutableAttributedString(string: value, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1))
		if let unit = unit {
			attributedString.append(NSMutableAttributedString(string: " " + unit, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		}
		return attributedString
	}
}

// MARK: - Calendar ActivityDetailViewDelegate
extension ActivityDetailViewController: ActivityDetailViewDelegate {
    func errorOccured() {
        hud.dismiss()
    }
    
	func goal(type: ActivityDataType, targetValue: Int) {
		let goalText = (type == .stepCount) ? "TOTAL STEPS" : "TOTAL CALORIES"
		totalGoalValue = targetValue
		self.goalLabel.text = goalText
		
		let attributeForValue = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.semiBold(withSize: 14), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textColor]
		let attributeForText = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.regular(withSize: 14), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.hexColor(hex: "#ffffff")?.withAlphaComponent(0.7)]
		
		let goal = UserDefault.shared.goals
		let attributedText = NSMutableAttributedString(string: "GOAL: ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attributeForText))
		
		if (type == .stepCount) {
			attributedText.append(NSAttributedString(string: "\(goal.stepCountGoal.value)", attributes: convertToOptionalNSAttributedStringKeyDictionary(attributeForValue)))
		} else {
			attributedText.append(NSAttributedString(string: "\(goal.caloriesBurntGoal.value)", attributes: convertToOptionalNSAttributedStringKeyDictionary(attributeForValue)))
		}
		graphGoalLabel.attributedText = attributedText
		self.circularGoalSlider.maximumValue = CGFloat(targetValue)
	}
	
	func distance(value: Double, unit: String) {
		let distanceValueText = readingData(value: "\(value.inchToMile())", unit: unit)
		self.distanceLabel.attributedText = distanceValueText
	}
	
	func totalCount(value: Double, unit: String?) {
		if dataType == .stepCount {
			self.circularGoalSlider.endPointValue = CGFloat(Int(value))
			self.completedStepsLabel.text = "\(Int(value))"
		} else {
			self.circularGoalSlider.endPointValue = CGFloat(value)
			self.completedStepsLabel.text = "\(Int(value.rounded()))"
		}
		
		pieChartForGoal(goal: Double(totalGoalValue ?? 100), achievedValue: Double(value))
	}
	
	func caloriesCount(value: Double, unit: String) {
		let calorieInInt = Int(value.rounded())
		let caloriesValueText = readingData(value: "\(calorieInInt)", unit: unit)
		self.calorieLabel.attributedText = caloriesValueText
	}
	
	func averageGraphData(data: Double, unit: String?, range: DateRange) {
		var averageString = "\(Int(data))"
		if let unit = unit {
			averageString.append(" " + unit)
		}
//		if range.type != .daily {
//			averageString.append(" / day" )
//		}
		
		let valueText = readingData(value: "\(Int(data))", unit: unit)
		self.avgStepsLabel.attributedText = valueText
        
        hud.dismiss()
	}
	
	func barChart(isVisible: Bool) {
		self.barChartView.isHidden = !isVisible
		self.noChartView.isHidden = isVisible
	}
	
	func pieChartForGoal(goal: Double, achievedValue: Double) {
		
		let chart = HIChart()
		chart.type = "pie"
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotoptions = HIPlotOptions()
		plotoptions.pie = HIPie()
		plotoptions.pie.states = HIStates()
		plotoptions.pie.states.inactive = HIInactive()
		plotoptions.pie.states.inactive.opacity = NSNumber(value: true)
		plotoptions.pie.animation = HIAnimationOptionsObject()
		plotoptions.pie.animation.duration = NSNumber(value: 0)
		plotoptions.pie.marker = HIMarker()
		plotoptions.pie.marker.enabled = false
		
		let pie = HIPie()
		pie.name = ""
		pie.data = [["name": "AchievedGoal", "y": achievedValue/goal], ["name": "RemainingGoal", "y": (goal - achievedValue)/goal]]
		plotoptions.pie.colors = [elementColor.hexString, "#2A2547"]
//		2A2547
		
		pie.innerSize = "84%"
		pie.marker = HIMarker()
		pie.marker.enabled = false
		pie.marker.states = HIStates()
		pie.marker.states.hover = HIHover()
		pie.marker.states.hover.enabled = false
		pie.marker.states.select = HISelect()
		pie.marker.states.select.enabled = false
		pie.showInLegend = false
		pie.dataLabels = HIDataLabelsOptionsObject()
		pie.dataLabels.enabled = false
		pie.borderWidth = 0
		pie.enableMouseTracking = false
		pie.size = 220
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = [pie]
		options.plotOptions = plotoptions
		options.legend = legend
		options.responsive = HIResponsive()
		
		self.goalPieChart.options = options
		self.goalPieChart.sizeToFit()
	}
}

// MARK: - Calendar ChartDelegate
extension ActivityDetailViewController: ChartDelegate {
	func chart(with options: HIOptions, peakValue: String) {
		
	}
	
	
	func chart(with options: HIOptions, peakValue: NSAttributedString) {
		self.barChartView.isUserInteractionEnabled = false
		self.barChartView.options = options
		self.peakLabel.attributedText = peakValue
	}
}

// MARK: - Calendar Callbacks
extension ActivityDetailViewController {
	@objc private func calendarDateSelected(notification: NSNotification) {
		guard let object = notification.object as? DateRange else {
			return
		}
		
		self.selectedDateRange = object
	}
}
