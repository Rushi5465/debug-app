//
//  HistoryDataViewController.swift
//  BloodPressureApp
//
//  Created by Rushikant on 25/08/21.
//

import UIKit

class HistoryDataViewController: UIViewController {
	
	@IBOutlet weak var headerView: HeaderView!
	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var tableHeight: NSLayoutConstraint!
	
	var sectionList = [HistorySection]() {
		didSet {
			tableView.reloadData()
		}
	}
	
	var presenter: HistoryListPresenter?
	var dataType: HistoryDataType!
	
	var selectedDateRange: DateRange! {
		didSet {
			hud.show(in: self.view)
			if dataType == .exerciseReading {
				self.presenter?.fetchExerciseData(range: selectedDateRange)
			} else if dataType == .pauseReading {
				self.presenter?.fetchPauseData(range: selectedDateRange)
			} else {
				self.presenter?.fetchBloodPressureData(range: selectedDateRange)
			}
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if dataType == .exerciseReading {
			self.titleLabel.text = "Exercise History"
		} else if dataType == .pauseReading {
			self.titleLabel.text = "Pause History"
		} else {
			self.titleLabel.text = "BP Log"
		}
		
		self.registerPresenters()
		selectedDateRange = DateRange(DateManager.current.selectedDate.start, type: .weekly)
		self.headerView.currentController = self
		self.headerView.isInNavigation = true
		self.headerView.updateDate = false
		self.headerView.selectedDate = selectedDateRange
		self.registerCellToListView()
		self.assignDelegate()
		
		self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.calendarDateSelected), name: NSNotification.Name("calendarDateSelected"), object: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name("calendarDateSelected"), object: nil)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.tableHeight.constant = self.tableView.contentSize.height
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let pauseReadingService = PauseDataWebService()
			let exerciseService = ExerciseDataWebService()
			let bpService = BloodPressureWebService()
			self.presenter = HistoryListPresenter(exerciseService: exerciseService, pauseService: pauseReadingService, bpService: bpService, delegate: self)
		}
	}
	
	func registerCellToListView() {
		let menuNib = UINib.init(nibName: "ActivityHistoryTableViewCell", bundle: nil)
		self.tableView.register(menuNib, forCellReuseIdentifier: ActivityHistoryTableViewCell.IDENTIFIER)
		
		let pauseHistoryNib = UINib.init(nibName: "PauseHistoryTableViewCell", bundle: nil)
		self.tableView.register(pauseHistoryNib, forCellReuseIdentifier: PauseHistoryTableViewCell.IDENTIFIER)
		
		let bpHistoryNib = UINib.init(nibName: "BPHistoryTableViewCell", bundle: nil)
		self.tableView.register(bpHistoryNib, forCellReuseIdentifier: BPHistoryTableViewCell.IDENTIFIER)
		
		let headerNib = UINib(nibName: "HistoryHeaderView", bundle: nil)
		tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HistoryHeaderView")
	}
	
	func assignDelegate() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
	}
}

extension HistoryDataViewController: HistoryDataDelegate {
	
	func historyData(list: [HistorySection]) {
		self.sectionList = list
		hud.dismiss(animated: true)
	}
}

extension HistoryDataViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
		guard let headerView = view as? UITableViewHeaderFooterView else { return }
		headerView.tintColor = .clear //use any color you want here .red, .black etc
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		
		let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HistoryHeaderView") as! HistoryHeaderView

		headerView.titleLabel.text = self.sectionList[section].date.dateToString(format: "d MMM")
		
		let path = UIBezierPath()
		path.move(to: CGPoint(x: 10, y: 0))
		path.addLine(to: CGPoint(x: 10, y: 189))
		
		let shapeLayer = CAShapeLayer()
		shapeLayer.path = path.cgPath
		shapeLayer.strokeColor = UIColor(named: "Counter Color")?.cgColor
		shapeLayer.fillColor = UIColor.clear.cgColor
		shapeLayer.lineWidth = 3
		
		// Add that `CAShapeLayer` to your view's layer:
		headerView.lineView.layer.addSublayer(shapeLayer)
		
		let circleLayer = CAShapeLayer()
		circleLayer.path = UIBezierPath(ovalIn: CGRect(x: 2, y: 2, width: 16, height: 16)).cgPath
		circleLayer.strokeColor = UIColor(named: "Counter Color")?.cgColor
		circleLayer.lineWidth = 4.0
		headerView.lineView.layer.addSublayer(circleLayer)
		
		return headerView
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 35
	}
}

extension HistoryDataViewController: UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		self.sectionList.count
	}
	
	func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
		return false
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.sectionList[section].data.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let item = self.sectionList[indexPath.section].data[indexPath.row]
		
		if let exerciseItem = item as? CoreDataExercise {
			let cell = ActivityHistoryTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
			
			cell.timeValueLabel.text = exerciseItem.startTime.dateToString(format: "h:mm a")
			
			if let systolic = exerciseItem.systolic?.stringValue, let diastolic = exerciseItem.diastolic?.stringValue {
				cell.bpValueLabel.attributedText = cell.unitAttributeLabel(value: systolic + "/" + diastolic, unit: "mm Hg")
			} else {
				cell.bpValueLabel.text = "-"
			}
			
			if let pulseRate = exerciseItem.pulseRate?.stringValue {
				cell.heartRateLabel.attributedText = cell.unitAttributeLabel(value: pulseRate, unit: "bpm")
			} else {
				cell.heartRateLabel.text = "-"
			}
			
			if let caloriesBurnt = exerciseItem.caloriesBurnt?.stringValue {
				cell.caloriesValueLabel.attributedText = cell.unitAttributeLabel(value: caloriesBurnt, unit: "Cal")
			} else {
				cell.caloriesValueLabel.text = "-"
			}
			
			let dateComponentFormatter = DateComponentsFormatter()
			let exerciseTime = exerciseItem.endTime.timeIntervalSince1970 - exerciseItem.startTime.timeIntervalSince1970
			cell.totalTimeValueLabel.text = dateComponentFormatter.difference(from: TimeInterval(exerciseTime))
			
			// Vertical Line Path
			let path = UIBezierPath()
			path.move(to: CGPoint(x: 10, y: 0))
			path.addLine(to: CGPoint(x: 10, y: 189))
			
			// Create a `CAShapeLayer` that uses that `UIBezierPath`:
			let shapeLayer = CAShapeLayer()
			shapeLayer.path = path.cgPath
			shapeLayer.strokeColor = UIColor(named: "Counter Color")?.cgColor
			shapeLayer.fillColor = UIColor.clear.cgColor
			shapeLayer.lineWidth = 3
			
			// Add that `CAShapeLayer` to your view's layer:
			cell.lineView.layer.addSublayer(shapeLayer)
			
			return cell
		} else if let pauseItem = item as? CoreDataPauseReading {
			
			let cell = PauseHistoryTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
			
			cell.timeValueLabel.text = pauseItem.startTime.dateToString(format: "h:mm a")
			
			if let systolic = pauseItem.systolic?.stringValue, let diastolic = pauseItem.diastolic?.stringValue {
				cell.bpValueLabel.attributedText = cell.unitAttributeLabel(value: systolic + "/" + diastolic, unit: "mm Hg")
			} else {
				cell.bpValueLabel.text = "-"
			}
			
			if let pulseRate = pauseItem.pulseRate?.stringValue {
				cell.heartRateLabel.attributedText = cell.unitAttributeLabel(value: pulseRate, unit: "bpm")
			} else {
				cell.heartRateLabel.text = "-"
			}
			
			if let skinTemperature = pauseItem.skinTemperature?.stringValue {
				cell.skinTemperatureLabel.attributedText = cell.unitAttributeLabel(value: skinTemperature, unit: "°F")
			} else {
				cell.skinTemperatureLabel.text = "-"
			}
			
			cell.breathingRateLabel.text = "-"
			
			// Vertical Line Path
			let path = UIBezierPath()
			path.move(to: CGPoint(x: 10, y: 0))
			path.addLine(to: CGPoint(x: 10, y: 189))
			
			// Create a `CAShapeLayer` that uses that `UIBezierPath`:
			let shapeLayer = CAShapeLayer()
			shapeLayer.path = path.cgPath
			shapeLayer.strokeColor = UIColor(named: "Counter Color")?.cgColor
			shapeLayer.fillColor = UIColor.clear.cgColor
			shapeLayer.lineWidth = 3
			
			// Add that `CAShapeLayer` to your view's layer:
			cell.lineView.layer.addSublayer(shapeLayer)
			
			return cell
		} else if let bpItem = item as? CoreDataBPReading {
			let cell = BPHistoryTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
			
			cell.timeValueLabel.text = bpItem.date.dateToString(format: "h:mm a")
			cell.bpValueLabel.attributedText = cell.unitAttributeLabel(value: bpItem.systolic.stringValue + "/" + bpItem.diastolic.stringValue, unit: "mm Hg")
			
			// Vertical Line Path
			let path = UIBezierPath()
			path.move(to: CGPoint(x: 10, y: 0))
			path.addLine(to: CGPoint(x: 10, y: 189))
			
			// Create a `CAShapeLayer` that uses that `UIBezierPath`:
			let shapeLayer = CAShapeLayer()
			shapeLayer.path = path.cgPath
			shapeLayer.strokeColor = UIColor(named: "Counter Color")?.cgColor
			shapeLayer.fillColor = UIColor.clear.cgColor
			shapeLayer.lineWidth = 3
			
			// Add that `CAShapeLayer` to your view's layer:
			cell.lineView.layer.addSublayer(shapeLayer)
			
			return cell
		} else {
			return UITableViewCell()
		}
	}
}

// MARK: - Calendar Callbacks
extension HistoryDataViewController {
	
	@objc private func calendarDateSelected(notification: NSNotification) {
		guard let object = notification.object as? DateRange else {
			return
		}
		
		self.selectedDateRange = object
	}
}
