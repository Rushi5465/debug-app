//
//  OxygenDataWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 26/08/21.
//

import Foundation
protocol OxygenDataWebServiceProtocol {
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataOxygen]?, MovanoError?) -> Void)
	
	func fetch(by timestamp: Double, completionHandler: @escaping (CoreDataOxygen?, MovanoError?) -> Void)
	
	func add(model: SaturatedOxygen, completionHandler: @escaping (CoreDataOxygen?, MovanoError?) -> Void)
	
	func add(models: [SaturatedOxygen], completionHandler: @escaping ([CoreDataOxygen]?, MovanoError?) -> Void)
	
	func update(model: SaturatedOxygen, completionHandler: @escaping (CoreDataOxygen?, MovanoError?) -> Void)
	
	func delete(model: SaturatedOxygen, completionHandler: @escaping (Bool?, MovanoError?) -> Void)
	
//	func fetchAverageData(forDate:String,completionHandler: @escaping (SleepAverageDataModel?, MovanoError?) -> Void)
}
