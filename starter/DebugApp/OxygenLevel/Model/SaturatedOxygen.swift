//
//  SaturatedOxygen.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 09/09/21.
//

import Foundation

public class SaturatedOxygen:NSObject, NSCoding{
    
    public var timestamp: TimeInterval = 0
    public var value: Int = 0
    
    enum Key:String{
        case timestamp = "timestamp"
        case value = "value"
    }
    
    public override init() {
        super.init()
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(timestamp,forKey: Key.timestamp.rawValue)
        aCoder.encode(value,forKey: Key.value.rawValue)

    }
  
    required public convenience init(coder aDecoder: NSCoder)  {
        let mTimestamp = aDecoder.decodeDouble(forKey: Key.timestamp.rawValue)
        let mValue = aDecoder.decodeInteger(forKey: Key.value.rawValue)
        self.init(timeStamp:mTimestamp, value:mValue)
    }
    
    init(timeStamp:TimeInterval, value:Int) {
        self.timestamp = timeStamp
        self.value = value
    }
}

public class SaturatedOxygenCoreData:NSObject, NSCoding{
    public var saturatedOxygen : [SaturatedOxygen] = []
    
    enum Key:String{
        case saturatedOxygen = "saturatedOxygen"
    }
    
    init(saturatedOxygen:[SaturatedOxygen]){
        self.saturatedOxygen = saturatedOxygen
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(saturatedOxygen,forKey: Key.saturatedOxygen.rawValue)
    }
    
    required public convenience init(coder aDecoder: NSCoder)  {
        let mStepCount = aDecoder.decodeObject(forKey: Key.saturatedOxygen.rawValue) as! [SaturatedOxygen]
        
        self.init(saturatedOxygen:mStepCount)
    }
}


