//
//  OxygenDataWebService.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 26/08/21.
//

import Foundation

class OxygenDataWebService: OxygenDataWebServiceProtocol {
	
	private var graphQL: ApolloClientService
	private var service: RestService
	public static var refreshTokenCount = 0
	
	init(graphQL: ApolloClientService = ApolloClientService(),service: RestService = RestService.shared) {
		self.graphQL = graphQL
		self.service = service
	}
	
	func fetch(by timestamp: Double, completionHandler: @escaping (CoreDataOxygen?, MovanoError?) -> Void) {
		let query = GetSpO2Query(sp_o2_timestamp: timestamp)
		
		_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.getSpO2 {
					CoreDataOxygen.add(timeInterval: item.spO2Timestamp, value: item.spO2Value)
					let reading = CoreDataOxygen.fetch(timeStamp: item.spO2Timestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataOxygen]?, MovanoError?) -> Void) {
        if(Utility.makeAPICall(timeIntervalRange: timestampRange) || !CoreDataOxygen.isReadingAvailable(range: timestampRange)){
            
            let query = QuerySpO2ByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
            
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
                switch result {
                case .success(let graphQLResult):
                    if let item = graphQLResult.data?.querySpO2ByTimestampRange?.items {
                        for each in item where each != nil {
                            CoreDataOxygen.add(timeInterval: each!.spO2Timestamp, value: each!.spO2Value)
                        }
                        
                        let oxygenData = CoreDataOxygen.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                        completionHandler(oxygenData, nil)
                    } else {
                        completionHandler(nil, .invalidResponseModel)
                    }
                case .failure(let error):
                    if error.localizedDescription == "Received error response: Unauthorized" ||
                        error.localizedDescription == "Received error response: Token has expired." {
                        
                    } else {
                        
                    }
					let oxygenData = CoreDataOxygen.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
					completionHandler(oxygenData, nil)
//					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                    Logger.shared.addLog(error.localizedDescription)
                }
            }
        }else{
            let oxygenData = CoreDataOxygen.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            completionHandler(oxygenData, nil)
        }
	}
	
	func delete(model: SaturatedOxygen, completionHandler: @escaping (Bool?, MovanoError?) -> Void) {
		
		let input = DeleteSpO2Input(spO2Timestamp: model.timestamp)
		let mutation = DeleteSpO2Mutation(DeleteSpO2Input: input)
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.deleteSpO2 {
					CoreDataOxygen.delete(for: Date(timeIntervalSince1970: item.spO2Timestamp))
					completionHandler(true, nil)
				} else {
					completionHandler(false, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func add(model: SaturatedOxygen, completionHandler: @escaping (CoreDataOxygen?, MovanoError?) -> Void) {
		let input = CreateSpO2Input(spO2Timestamp: model.timestamp, spO2Value: model.value)
		let mutation = CreateSpO2Mutation(CreateSpO2Input: input)
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.createSpO2 {
					CoreDataOxygen.add(timeInterval: item.spO2Timestamp, value: item.spO2Value)
					let reading = CoreDataOxygen.fetch(timeStamp: item.spO2Timestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func update(model: SaturatedOxygen, completionHandler: @escaping (CoreDataOxygen?, MovanoError?) -> Void) {
		
		let input = UpdateSpO2Input(spO2Timestamp: model.timestamp, spO2Value: model.value)
		let mutation = UpdateSpO2Mutation(UpdateSpO2Input: input)
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.updateSpO2 {
					CoreDataOxygen.add(timeInterval: item.spO2Timestamp, value: item.spO2Value)
					let reading = CoreDataOxygen.fetch(timeStamp: item.spO2Timestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func add(models: [SaturatedOxygen], completionHandler: @escaping ([CoreDataOxygen]?, MovanoError?) -> Void) {
		
		var data = [CreateSpO2Input]()
		for each in models {
			data.append(CreateSpO2Input(spO2Timestamp: each.timestamp, spO2Value: each.value))
		}
		let mutation = CreateMultipleSpO2Mutation(CreateSpO2Input: data)
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let items = graphQLResult.data?.createMultipleSpO2 {
					for each in items where each != nil {
						CoreDataOxygen.add(timeInterval: each!.spO2Timestamp, value: each!.spO2Value)
					}
                    if let firstItem = items.first, let timestamp = firstItem?.spO2Timestamp{
                        let reading = CoreDataOxygen.fetch(for: DateRange(Date(timeIntervalSince1970: timestamp), type: .daily).dateRange)
                        completionHandler(reading, nil)
                    }else{
                        completionHandler(nil, nil)
                    }
				} else {
					Logger.shared.addLog("Current time is -\(Date())")
					Logger.shared.addLog( "Data is not in [CreateMultipleSpO2] format")
					Logger.shared.addLog( "Graphql result of spo2 - \(graphQLResult.data?.createMultipleSpO2)")
					completionHandler(nil, nil)
					
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					if OxygenDataWebService.refreshTokenCount == 0 {
						OxygenDataWebService.refreshTokenCount = 1
						self.requestAccessToken { [weak self] response, error in
							if (response != nil) {
								self?.graphQL.setDefaultClient()
								self?.add(models: models, completionHandler: completionHandler)
							}
							if let error = error {
								completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
							}
						}
					}
				} else {
					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				}
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
