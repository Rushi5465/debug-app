//
//  ExerciseDataWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 20/07/21.
//

import Foundation

protocol ExerciseDataWebServiceProtocol {
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataExercise]?, MovanoError?) -> Void)
	
	func fetch(by id: String, completionHandler: @escaping (CoreDataExercise?, MovanoError?) -> Void)
	
	func add(model: ExerciseReading, completionHandler: @escaping (CoreDataExercise?, MovanoError?) -> Void)
}
