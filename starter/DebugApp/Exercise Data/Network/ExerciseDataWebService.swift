//
//  ExerciseDataWebService.swift
//  BloodPressureApp
//
//  Created by Rushikant on 20/07/21.
//

import Foundation

// swiftlint:disable all
class ExerciseDataWebService: ExerciseDataWebServiceProtocol {
	
	private var graphQL: ApolloClientService
	private var service: RestService
	
	init(graphQL: ApolloClientService = ApolloClientService(),service: RestService = RestService.shared) {
		self.graphQL = graphQL
		self.service = service
	}
	
	func fetch(by id: String, completionHandler: @escaping (CoreDataExercise?, MovanoError?) -> Void) {
		let query = GetExerciseDataQuery(id: id)
		
		_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.getExerciseData {
					CoreDataExercise.add(startTime: item.startTime, endTime: item.endTime, systolic: item.averageSystolic, diastolic: item.averageDiastolic, breathingRate: item.averageBreathingRate, skinTemperature: item.averageSkinTemprature, pulseRate: item.averagePulseRate, caloriesBurnt: item.totalCaloriesBurnt, stepCount: item.totalStepCount, distanceCovered: item.totalDistanceCovered, oxygen: item.averageSpo2)
					let reading = CoreDataExercise.fetch(timeStamp: item.endTime)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
    
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataExercise]?, MovanoError?) -> Void) {
        if(Utility.makeAPICall(timeIntervalRange: timestampRange) || !CoreDataExercise.isReadingAvailable(range: timestampRange)){
            let userIdInput = TableStringFilterInput(eq: KeyChain.shared.userId)
            let dateRangeInput = TableFloatFilterInput(between: [timestampRange.lowerBound, timestampRange.upperBound])
            
            let filter = TableExerciseDataFilterInput(userId: userIdInput, endTime: dateRangeInput)
            let query = ListExerciseDataQuery(filter: filter, limit: nil)
            
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
                switch result {
                case .success(let graphQLResult):
                    if let item = graphQLResult.data?.listExerciseData?.items {
                        for each in item where each != nil {
							CoreDataExercise.add(startTime: each!.startTime, endTime: each!.endTime, systolic: each!.averageSystolic, diastolic: each!.averageDiastolic, breathingRate: each!.averageBreathingRate, skinTemperature: each!.averageSkinTemprature, pulseRate: each!.averagePulseRate, caloriesBurnt: each!.totalCaloriesBurnt, stepCount: each!.totalStepCount, distanceCovered: each!.totalDistanceCovered, oxygen: each!.averageSpo2)
                        }
                        let exerciseData = CoreDataExercise.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                        completionHandler(exerciseData, nil)
                    } else {
                        completionHandler(nil, .invalidResponseModel)
                    }
                case .failure(let error):
                    if error.localizedDescription == "Received error response: Unauthorized" ||
                        error.localizedDescription == "Received error response: Token has expired." {
						let exerciseData = CoreDataExercise.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
						completionHandler(exerciseData, nil)
                    } else {
						let exerciseData = CoreDataExercise.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
						completionHandler(exerciseData, nil)
//                        completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                    }
                    Logger.shared.addLog(error.localizedDescription)
                }
            }
        }else{
            let exerciseData = CoreDataExercise.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            completionHandler(exerciseData, nil)
        }
	}
	
	func add(model: ExerciseReading, completionHandler: @escaping (CoreDataExercise?, MovanoError?) -> Void) {
		let query = CreateExerciseDataMutation(CreateExerciseDataInput: CreateExerciseDataInput(startTime: model.startTime, endTime: model.endTime))
		
		_ = self.graphQL.client!.perform(mutation: query) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.createExerciseData {
					CoreDataExercise.add(startTime: item.startTime, endTime: item.endTime, systolic: item.averageSystolic, diastolic: item.averageDiastolic, breathingRate: item.averageBreathingRate, skinTemperature: item.averageSkinTemprature, pulseRate: item.averagePulseRate, caloriesBurnt: item.totalCaloriesBurnt, stepCount: item.totalStepCount, distanceCovered: item.totalDistanceCovered, oxygen: item.averageSpo2)
					let reading = CoreDataExercise.fetch(timeStamp: item.endTime)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, nil)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
