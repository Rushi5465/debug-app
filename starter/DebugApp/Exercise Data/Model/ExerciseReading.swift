//
//  ExerciseReading.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 13/09/21.
//

import Foundation

struct ExerciseReading {
	
	let startTime: TimeInterval
	let endTime: TimeInterval
}
