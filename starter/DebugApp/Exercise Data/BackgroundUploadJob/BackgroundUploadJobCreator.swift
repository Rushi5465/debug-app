//
//  BackgroundUploadJobCreator.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 28/12/21.
//

import UIKit
import SwiftQueue

class DataUploadJobCreator: JobCreator {
    
    // Base on type, return the actual job implementation
    func create(type: String, params: [String: Any]?) -> Job {
        // check for job and params type
        switch type {
        case UploadSessionTimeJob.type:
            return UploadSessionTimeJob(params:params ?? [:])
        case UploadStepsDataJob.type:
        return UploadStepsDataJob(params:params ?? [:])
        case UploadPulseRateDataJob.type:
            return UploadPulseRateDataJob(params:params ?? [:])
        case UploadBreathingRateDataJob.type:
            return UploadBreathingRateDataJob(params:params ?? [:])
        case UploadTempeartureDataJob.type:
            return UploadTempeartureDataJob(params:params ?? [:])
        case UploadOxygenDataJob.type:
            return UploadOxygenDataJob(params:params ?? [:])
		case RequestAccessToken.type:
			return RequestAccessToken(params:params ?? [:])
        default:
            // Nothing match
            // You can use `fatalError` or create an empty job to report this issue.
            fatalError("No Job !")
        }
       
    }
}

class NoPersister: JobPersister {

    public static let shared = NoPersister()

    private init() {}

    func restore() -> [String] { return [] }

    func restore(queueName: String) -> [String] { return [] }

    func put(queueName: String, taskId: String, data: String) {}

    func remove(queueName: String, taskId: String) {}

    func clearAll() {}

}
