//
//  BackgroundUploadJobs.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 28/12/21.
//

import UIKit
import SwiftQueue
import Dispatch
import Alamofire


let dataUploadJobManager = SwiftQueueManagerBuilder(creator: DataUploadJobCreator()).set(persister: NoPersister.shared).build()
let accessTokenJobManager = SwiftQueueManagerBuilder(creator: DataUploadJobCreator()).set(persister: NoPersister.shared).build()

protocol ServerReachabilty: AnyObject {
    func whenServerIsUp()
    func whenServerIsDown()
}

class UploadSessionTimeJob: Job {
   
    //Type to know which Job to return in job creator
    static let type = "UploadSessionTimeJob"
    static let SessionDataModel = "SessionTimeModel"
    static let BackgroundDataUploadWebServices = "BackgroundDataUploadWebServices"
   
    private var sessionDataModel : ExerciseSessionData?
    private var uploadWebServices : BackgroundDataUploadWebServices?
    
    init(params:[String:Any]) {
        self.sessionDataModel = params[UploadSessionTimeJob.SessionDataModel] as? ExerciseSessionData
        self.uploadWebServices = params[UploadSessionTimeJob.BackgroundDataUploadWebServices] as? BackgroundDataUploadWebServices
    }
    
    func onRun(callback: JobResult) {
        //Called after, when we schedule a job
       uploadExerciseSessionTime(callback: callback)
    }
    
    func onRetry(error: Error) -> RetryConstraint {
        // Called when Job fails to execute
        return RetryConstraint.retry(delay: 0) // immediate retry
    }
    
    func onRemove(result: JobCompletion) {
         switch result {
             case .success:
                 print("onRemove : Success UploadSessionTimeJob")
                 
             break
         case .fail(let error):
                print("onRemove : Failure")
                print(error)
                switch error {
                case SwiftQueue.SwiftQueueError.canceled:
                    print("Job is canceled")
                default:
                    print("Case default")
                }
               
             break
        
         }
    }

    // MARK: Server Call (Helper)
      private func uploadExerciseSessionTime(callback: JobResult) {
        if let model = self.sessionDataModel?.timingModel, let exerciseService = self.uploadWebServices?.exerciseService{
            exerciseService.add(model: model) { [weak self] (response, error) in
                guard let presenterVC = self else {return}
                if let error = error {
                    //Failure
                    Logger.shared.addLog("Call fail for uploadExerciseSessionTime, rescheduling job. Error occured: \(error)")

                    callback.done(.success)
                    presenterVC.scheduleSessionTimeUploadJob()
                }else{
                    //Success
                    Logger.shared.addLog("Successfully uploaded session data")
                    callback.done(.success)
                    presenterVC.afterAllUploads()
                }
                
              
            }
        }
      }
    
    func scheduleStepsUploadJob(){
                   //dataUploadJobManager.cancelAllOperations()
                    JobBuilder(type: UploadStepsDataJob.type)
                    .priority(priority: .veryHigh)
                    .service(quality: .background)
                    .internet(atLeast: .cellular)
                        .with(params: [UploadSessionTimeJob.SessionDataModel:self.sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:self.uploadWebServices as Any])
                    .schedule(manager: dataUploadJobManager)
    }
    
    func afterAllUploads(){
        //After all successful uploads delete session data from local database
        if let lower = self.sessionDataModel?.timingModel.startTime, let upper = self.sessionDataModel?.timingModel.endTime{
            let dLower = Date(timeIntervalSince1970: lower)
            let dUpper = Date(timeIntervalSince1970: upper)
            Logger.shared.addLog("Deleting session data after successful upload")
            CoreDataExerciseSession.delete(for: dLower ..< dUpper)

        }
    }
    func scheduleSessionTimeUploadJob(){
                    //dataUploadJobManager.cancelAllOperations()
                    JobBuilder(type: UploadSessionTimeJob.type)
                    .priority(priority: .veryHigh)
                    .service(quality: .background)
                    .internet(atLeast: .cellular)
                        .with(params: [UploadSessionTimeJob.SessionDataModel:self.sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:self.uploadWebServices as Any])
                    .schedule(manager: dataUploadJobManager)
    }
}

class UploadStepsDataJob: Job {
   
    //Type to know which Job to return in job creator
    static let type = "UploadStepsDataJob"
   
    private var sessionDataModel : ExerciseSessionData?
    private var uploadWebServices : BackgroundDataUploadWebServices?
    
    init(params:[String:Any]) {
        self.sessionDataModel = params[UploadSessionTimeJob.SessionDataModel] as? ExerciseSessionData
        self.uploadWebServices = params[UploadSessionTimeJob.BackgroundDataUploadWebServices] as? BackgroundDataUploadWebServices
    }
    
    func onRun(callback: JobResult) {
        //Called after, when we schedule a job
        uploadStepCountData(callback: callback)
    }
    
    func onRetry(error: Error) -> RetryConstraint {
        // Called when Job fails to execute
        return RetryConstraint.retry(delay: 0) // immediate retry
    }
    
    func onRemove(result: JobCompletion) {
         switch result {
             case .success:
                 print("onRemove : Success UploadStepsDataJob")
                 
             break
         case .fail(let error):
                switch error {
                case SwiftQueue.SwiftQueueError.canceled:
                    print("Job is canceled")
                default:
                    print("Case default")
                }
               
             break
         }
    }

    // MARK: Server Call (Helper)
      private func uploadStepCountData(callback: JobResult) {
        if let stepCounts = self.sessionDataModel?.stepsData, let stepCountService = self.uploadWebServices?.stepCountService{
            stepCountService.add(models: stepCounts) { [weak self](response, error) in
                guard let presenterVC = self else {return}
                if(error != nil){
                 // Failure
                    callback.done(.success)
                    presenterVC.scheduleStepsUploadJob()
                    Logger.shared.addLog("Call fail for uploadStepCountData, rescheduling job. Error occured: \(String(describing: error))")
                    //Repeate the call to upload step count data
                }else{
                    // Success
                    Logger.shared.addLog("Successfully uploaded step count data")
                    callback.done(.success)
                    presenterVC.schedulePulseRateDataUploadJob()
                }
            }
        }else{
            //What if serivce is empty?
        }
        
      }
    
    
    func scheduleStepsUploadJob(){
                   //dataUploadJobManager.cancelAllOperations()
                    JobBuilder(type: UploadStepsDataJob.type)
                    .priority(priority: .veryHigh)
                    .service(quality: .background)
                    .internet(atLeast: .cellular)
                        .with(params: [UploadSessionTimeJob.SessionDataModel:self.sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:self.uploadWebServices as Any])
                    .schedule(manager: dataUploadJobManager)
    }
    
    func schedulePulseRateDataUploadJob(){
       // dataUploadJobManager.cancelAllOperations()
        JobBuilder(type: UploadPulseRateDataJob.type)
        .priority(priority: .veryHigh)
        .service(quality: .background)
        .internet(atLeast: .cellular)
        .with(params: [UploadSessionTimeJob.SessionDataModel:self.sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:self.uploadWebServices as Any])
        .schedule(manager: dataUploadJobManager)
    }
}



class UploadPulseRateDataJob: Job {
   
    //Type to know which Job to return in job creator
    static let type = "UploadPulseRateDataJob"
    static let PulseRateService = "PulseRateService"
    

    private var sessionDataModel : ExerciseSessionData?
    private var uploadWebServices : BackgroundDataUploadWebServices?
    
    init(params:[String:Any]) {
        self.sessionDataModel = params[UploadSessionTimeJob.SessionDataModel] as? ExerciseSessionData
        self.uploadWebServices = params[UploadSessionTimeJob.BackgroundDataUploadWebServices] as? BackgroundDataUploadWebServices
    }
    
    func onRun(callback: JobResult) {
        //Called after, when we schedule a job
       uploadPulseRateData(callback: callback)
    }
    
    func onRetry(error: Error) -> RetryConstraint {
        // Called when Job fails to execute
        return RetryConstraint.retry(delay: 0) // immediate retry
    }
    
    func onRemove(result: JobCompletion) {
         switch result {
             case .success:
                 print("onRemove : Success UploadPulseRateDataJob")
                 
             break
         case .fail(let error):
                print("onRemove : Failure")
                print(error)
                switch error {
                case SwiftQueue.SwiftQueueError.canceled:
                    print("Job is canceled")
                default:
                    print("Case default")
                }
               
             break
        
         }
    }

    // MARK: Server Call (Helper)
      private func uploadPulseRateData(callback: JobResult) {
        if let pulseData = self.sessionDataModel?.pulseRateData, let pulserateService = self.uploadWebServices?.pulseRateService{
            pulserateService.add(models: pulseData) { [weak self](response, error) in
                guard let presenterVC = self else {return}
                
                if(error != nil){
                 // Failure
                    callback.done(.success)
                    Logger.shared.addLog("Call fail for uploadPulseRateData, rescheduling job. Error occured: \(String(describing: error))")
                    presenterVC.schedulePulseRateDataUploadJob()
                }else{
                    // Success
                    callback.done(.success)
                    Logger.shared.addLog("Call successful for uploadPulseRateData")

                    presenterVC.scheduleBreathingRateUploadJob()

                }
            }
        }else{
            
        }
        
      }
    
    func scheduleBreathingRateUploadJob(){
        //dataUploadJobManager.cancelAllOperations()
        JobBuilder(type: UploadBreathingRateDataJob.type)
        .priority(priority: .veryHigh)
        .service(quality: .background)
        .internet(atLeast: .cellular)
        .with(params: [UploadSessionTimeJob.SessionDataModel:self.sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:self.uploadWebServices as Any])
        .schedule(manager: dataUploadJobManager)
    }
    
    func schedulePulseRateDataUploadJob(){
       //dataUploadJobManager.cancelAllOperations()
        JobBuilder(type: UploadPulseRateDataJob.type)
        .priority(priority: .veryHigh)
        .service(quality: .background)
        .internet(atLeast: .cellular)
        .with(params: [UploadSessionTimeJob.SessionDataModel:self.sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:self.uploadWebServices as Any])
        .schedule(manager: dataUploadJobManager)
    }
    
}

class UploadBreathingRateDataJob: Job {
   
    //Type to know which Job to return in job creator
    static let type = "UploadBreathingRateDataJob"
    static let BreathingRateService = "BreathingRateService"
   
    private var sessionDataModel : ExerciseSessionData?
    private var uploadWebServices : BackgroundDataUploadWebServices?
    
    init(params:[String:Any]) {
        self.sessionDataModel = params[UploadSessionTimeJob.SessionDataModel] as? ExerciseSessionData
        self.uploadWebServices = params[UploadSessionTimeJob.BackgroundDataUploadWebServices] as? BackgroundDataUploadWebServices
    }
    
    
    func onRun(callback: JobResult) {
        //Called after, when we schedule a job
       checkServerReachability(callback: callback)
    }
    
    func onRetry(error: Error) -> RetryConstraint {
        // Called when Job fails to execute
        return RetryConstraint.retry(delay: 0) // immediate retry
    }
    
    func onRemove(result: JobCompletion) {
         switch result {
             case .success:
                 print("onRemove : Success UploadBreathingRateDataJob")
                 
             break
         case .fail(let error):
                print("onRemove : Failure")
                print(error)
                switch error {
                case SwiftQueue.SwiftQueueError.canceled:
                    print("Job is canceled")
                default:
                    print("Case default")
                }
               
             break
        
         }
    }

    // MARK: Server Call (Helper)
      private func checkServerReachability(callback: JobResult) {
        if let uploadData = self.sessionDataModel?.breathingRateData, let webSrvice = self.uploadWebServices?.breathingRateService{
            webSrvice.add(models: uploadData) { [weak self](response, error) in
                guard let presenterVC = self else {return}
                if(error != nil){
                 // Failure
                    callback.done(.success)
                    Logger.shared.addLog("Call fail for upload breathing rate data, rescheduling job. Error occured: \(String(describing: error))")
                    presenterVC.scheduleBreathingRateUploadJob()
                }else{
                    // Success
                    callback.done(.success)
                    Logger.shared.addLog("Call successful for upload breathing rate data")
                    presenterVC.scheduleTemperatureDataUploadJob()
                }
            }
        }else{
            
        }
      }
    
    func scheduleTemperatureDataUploadJob(){
        //dataUploadJobManager.cancelAllOperations()
        JobBuilder(type: UploadTempeartureDataJob.type)
        .priority(priority: .veryHigh)
        .service(quality: .background)
        .internet(atLeast: .cellular)
        .with(params: [UploadSessionTimeJob.SessionDataModel:self.sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:self.uploadWebServices as Any])
        .schedule(manager: dataUploadJobManager)
    }
    
    func scheduleBreathingRateUploadJob(){
        //dataUploadJobManager.cancelAllOperations()
        JobBuilder(type: UploadBreathingRateDataJob.type)
        .priority(priority: .veryHigh)
        .service(quality: .background)
        .internet(atLeast: .cellular)
        .with(params: [UploadSessionTimeJob.SessionDataModel:self.sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:self.uploadWebServices as Any])
        .schedule(manager: dataUploadJobManager)
    }
}

class UploadTempeartureDataJob: Job {
   
    //Type to know which Job to return in job creator
    static let type = "UploadTempeartureDataJob"
    static let TemperatureService = "TemperatureService"
   
    private var sessionDataModel : ExerciseSessionData?
    private var uploadWebServices : BackgroundDataUploadWebServices?
    
    init(params:[String:Any]) {
        self.sessionDataModel = params[UploadSessionTimeJob.SessionDataModel] as? ExerciseSessionData
        self.uploadWebServices = params[UploadSessionTimeJob.BackgroundDataUploadWebServices] as? BackgroundDataUploadWebServices
    }
    
    func onRun(callback: JobResult) {
        //Called after, when we schedule a job
       checkServerReachability(callback: callback)
    }
    
    func onRetry(error: Error) -> RetryConstraint {
        // Called when Job fails to execute
        return RetryConstraint.retry(delay: 0) // immediate retry
    }
    
    func onRemove(result: JobCompletion) {
         switch result {
             case .success:
                 print("onRemove : Success UploadTempeartureDataJob")
                 
             break
         case .fail(let error):
                print("onRemove : Failure")
                print(error)
                switch error {
                case SwiftQueue.SwiftQueueError.canceled:
                    print("Job is canceled")
                default:
                    print("Case default")
                }
               
             break
        
         }
    }

    // MARK: Server Call (Helper)
    private func checkServerReachability(callback: JobResult) {
      if let uploadData = self.sessionDataModel?.temperatureData, let webSrvice = self.uploadWebServices?.temperatureService{
          webSrvice.add(models: uploadData) { [weak self](response, error) in
              guard let presenterVC = self else {return}
              if(error != nil){
               // Failure
                callback.done(.success)
                Logger.shared.addLog("Call fail for upload temperature rate data, rescheduling job. Error occured: \(String(describing: error))")

                presenterVC.scheduleTemperatureDataUploadJob()
              }else{
                Logger.shared.addLog("Call successful for upload temperature rate data")
                  // Success
                callback.done(.success)

                presenterVC.scheduleOxygenDataUploadJob()
              }
          }
      }else{
          
      }
    }
  
    func scheduleOxygenDataUploadJob(){
        //dataUploadJobManager.cancelAllOperations()
        JobBuilder(type: UploadOxygenDataJob.type)
        .priority(priority: .veryHigh)
        .service(quality: .background)
        .internet(atLeast: .cellular)
        .with(params: [UploadSessionTimeJob.SessionDataModel:self.sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:self.uploadWebServices as Any])
        .schedule(manager: dataUploadJobManager)
    }

  func scheduleTemperatureDataUploadJob(){
      //dataUploadJobManager.cancelAllOperations()
      JobBuilder(type: UploadTempeartureDataJob.type)
      .priority(priority: .veryHigh)
      .service(quality: .background)
      .internet(atLeast: .cellular)
      .with(params: [UploadSessionTimeJob.SessionDataModel:self.sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:self.uploadWebServices as Any])
      .schedule(manager: dataUploadJobManager)
  }
}

class UploadOxygenDataJob: Job {
   
    //Type to know which Job to return in job creator
    static let type = "UploadOxygenDataJob"
    static let OxygenRateService = "OxygenRateService"
   
    private var sessionDataModel : ExerciseSessionData?
    private var uploadWebServices : BackgroundDataUploadWebServices?
    
    init(params:[String:Any]) {
        self.sessionDataModel = params[UploadSessionTimeJob.SessionDataModel] as? ExerciseSessionData
        self.uploadWebServices = params[UploadSessionTimeJob.BackgroundDataUploadWebServices] as? BackgroundDataUploadWebServices
    }
    
    func onRun(callback: JobResult) {
        //Called after, when we schedule a job
       checkServerReachability(callback: callback)
    }
    
    func onRetry(error: Error) -> RetryConstraint {
        // Called when Job fails to execute
        return RetryConstraint.retry(delay: 0) // immediate retry
    }
    
    func onRemove(result: JobCompletion) {
         switch result {
             case .success:
                 print("onRemove : Success UploadOxygenDataJob")
                 
             break
         case .fail(let error):
                print("onRemove : Failure")
                print(error)
                switch error {
                case SwiftQueue.SwiftQueueError.canceled:
                    print("Job is canceled")
                default:
                    print("Case default")
                }
               
             break
        
         }
    }

    // MARK: Server Call (Helper)
      private func checkServerReachability(callback: JobResult) {
        if let uploadData = self.sessionDataModel?.oxygenData, let webSrvice = self.uploadWebServices?.oxygenRateService{
            webSrvice.add(models: uploadData) { [weak self](response, error) in
                guard let presenterVC = self else {return}
                if(error != nil){
                 // Failure
                    callback.done(.success)
                    Logger.shared.addLog("Call fail for upload oxygen data, rescheduling job. Error occured: \(String(describing: error))")
                    presenterVC.scheduleOxygenDataUploadJob()

                }else{
                    // Success
                    callback.done(.success)
                    Logger.shared.addLog("Call successful for upload oxygen data")
                    presenterVC.scheduleSessionTimeUploadJob()
                }
            }
        }else{
            
        }
      }
    
    func scheduleSessionTimeUploadJob(){
                    //dataUploadJobManager.cancelAllOperations()
                    JobBuilder(type: UploadSessionTimeJob.type)
                    .priority(priority: .veryHigh)
                    .service(quality: .background)
                    .internet(atLeast: .cellular)
                        .with(params: [UploadSessionTimeJob.SessionDataModel:self.sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:self.uploadWebServices as Any])
                    .schedule(manager: dataUploadJobManager)
    }
    
    func scheduleOxygenDataUploadJob(){
        //dataUploadJobManager.cancelAllOperations()
        JobBuilder(type: UploadOxygenDataJob.type)
        .priority(priority: .veryHigh)
        .service(quality: .background)
        .internet(atLeast: .cellular)
        .with(params: [UploadSessionTimeJob.SessionDataModel:self.sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:self.uploadWebServices as Any])
        .schedule(manager: dataUploadJobManager)
    }
}

class RequestAccessToken: Job {
   
	//Type to know which Job to return in job creator
	static let type = "RequestAccessToken"
	static let AuthorizationWebService = "AuthorizationWebService"
	static let SessionExpiryTime = "sessionExpiryTime"
   
	private var sessionExpiryTime : Double
	private var authWebServices :  AuthorizationWebService?
	
	init(params:[String:Any]) {
		self.sessionExpiryTime = params[RequestAccessToken.SessionExpiryTime] as! Double
		self.authWebServices = params[RequestAccessToken.AuthorizationWebService] as? AuthorizationWebService
	}
	
	func onRun(callback: JobResult) {
		//Called after, when we schedule a job
	   checkServerReachability(callback: callback)
	}
	
	func onRetry(error: Error) -> RetryConstraint {
		// Called when Job fails to execute
		return RetryConstraint.retry(delay: 0) // immediate retry
	}
	
	func onRemove(result: JobCompletion) {
		 switch result {
			 case .success:
				 print("onRemove : Success request access token")
				 
			 break
		 case .fail(let error):
				print("onRemove : Failure")
				print(error)
				switch error {
				case SwiftQueue.SwiftQueueError.canceled:
					print("Job is canceled")
				default:
					print("Case default")
				}
			   
			 break
		
		 }
	}

	// MARK: Server Call (Helper)
	  private func checkServerReachability(callback: JobResult) {
		if let authSrvice = self.authWebServices{
			authSrvice.requestAccessToken { [weak self](response, error) in
				guard let presenterVC = self else {return}
				if(error != nil){
				 // Failure
					callback.done(.success)
					Logger.shared.addLog("Call fail for request access token, rescheduling job. Error occured: \(String(describing: error))")
					presenterVC.requestAccessTokenJob()

				}else{
					// Success
					callback.done(.success)
					Logger.shared.addLog("Call successful for request access token")
					presenterVC.scheduleRequestAccessTokenJob()
				}
			}
		}else{
			
		}
	  }
	
	func requestAccessTokenJob(){
					JobBuilder(type: RequestAccessToken.type)
					.priority(priority: .veryHigh)
					.service(quality: .background)
					.internet(atLeast: .cellular)
//					.periodic(limit: .unlimited, interval: 3600)
					.with(params: [RequestAccessToken.SessionExpiryTime:sessionExpiryTime as Any, RequestAccessToken.AuthorizationWebService:authWebServices as Any])
					.schedule(manager: accessTokenJobManager)
	}
	
	func scheduleRequestAccessTokenJob(){
		//dataUploadJobManager.cancelAllOperations()
		JobBuilder(type: RequestAccessToken.type)
		.priority(priority: .veryHigh)
		.service(quality: .background)
		.internet(atLeast: .cellular)
		.delay(time: sessionExpiryTime)
		.with(params: [RequestAccessToken.SessionExpiryTime:sessionExpiryTime as Any, RequestAccessToken.AuthorizationWebService:authWebServices as Any])
//		.periodic(limit: .unlimited, interval: 3600)
		.schedule(manager: accessTokenJobManager)
	}
}


