//
//  CarouselItem.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/07/21.
//

import Foundation
import UIKit

struct CarouselItem {
	
	var title: String
	var image: UIImage
}
