//
//  AccessTokenResponseModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 20/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

struct AccessTokenResponseModel: Decodable {
	
	let data: UserSession
	let status_code: Int
	let message: String
	
}
