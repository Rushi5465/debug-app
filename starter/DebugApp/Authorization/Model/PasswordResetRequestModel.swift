//
//  PasswordResetRequestModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

struct PasswordResetRequestModel: Encodable {
	
	var password: String
	var username: String
	var code: String
}
