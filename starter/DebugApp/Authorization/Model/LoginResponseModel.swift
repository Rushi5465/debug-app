//
//  LoginResponseModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 17/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

struct LoginResponseModel: Decodable {
	
	let data: UserSessionModel
	let status_code: Int
	let message: String
	
}
