//
//  UserSessionModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 17/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

struct UserSessionModel: Decodable {
	
	let refresh_token: String
	let token_type: String
	let access_token: String
	let expires_in: Int
	let id_token: String
	let username_for_srp: String
	let MFA_enabled: Bool
}
