//
//  SignUpViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol SignUpViewDelegate: AnyObject {
	func registerUserSuccess(with responseModel: EmptyResponseModel, user: User)
	func registerUserErrorHandler(error: MovanoError)
	
}
