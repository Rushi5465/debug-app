//
//  ResetPasswordViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol ResetPasswordViewDelegate: AnyObject {
	func forgotPasswordSuccess(with responseModel: EmptyResponseModel)
	func forgotPasswordErrorHandler(error: MovanoError)
	
}
