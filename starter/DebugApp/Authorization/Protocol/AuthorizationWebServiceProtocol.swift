//
//  AuthorizationWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol AuthorizationWebServiceProtocol {
	func inviteUser(code: String, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void)
	func registerUser(user: User, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void)
	func verifyPhone(phoneNumber: String, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void)
	func confirmForgotPassword(request: PasswordResetRequestModel, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void)
	func verifyUser(code: String, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void)
	func confirmRegistration(code: String, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void)
	func loginUser(username: String, password: String, completionHandler: @escaping (LoginResponseModel?, MovanoError?) -> Void)
	func resendVerificationLink(completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void)
	func forgotPassword(username: String, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void)
//	func changePasswordLink(request: ChangePasswordRequestModel, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void)
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void)
	
}
