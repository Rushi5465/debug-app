//
//  RegistrationCodeViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 15/01/21.
//  Copyright © 2021 Movano. All rights reserved.
//

import Foundation

protocol RegistrationCodeViewDelegate: AnyObject {
	func inviteUserSuccess(with responseModel: EmptyResponseModel)
	func inviteUserErrorHandler(error: MovanoError)
	
}
