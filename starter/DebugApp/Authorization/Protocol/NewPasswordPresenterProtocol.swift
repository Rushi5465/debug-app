//
//  NewPasswordPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol NewPasswordPresenterProtocol {
	init(webservice: AuthorizationWebServiceProtocol, delegate: NewPasswordViewDelegate)
	func confirmForgotPassword(request: PasswordResetRequestModel)
}
