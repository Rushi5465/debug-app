//
//  ResetPasswordPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol ResetPasswordPresenterProtocol {
	init(webservice: AuthorizationWebServiceProtocol, delegate: ResetPasswordViewDelegate)
	func forgotPassword(username: String)
	
}
