//
//  RegistrationCodePresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 15/01/21.
//  Copyright © 2021 Movano. All rights reserved.
//

import Foundation

protocol RegistrationCodePresenterProtocol {
	init(webservice: AuthorizationWebServiceProtocol, delegate: RegistrationCodeViewDelegate)
	func inviteUser(code: String)
	
}
