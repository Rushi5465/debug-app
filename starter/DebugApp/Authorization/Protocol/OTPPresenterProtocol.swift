//
//  OTPPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol OTPPresenterProtocol {
	init(webservice: AuthorizationWebServiceProtocol, delegate: OTPViewDelegate)
	func verifyUser(code: String)
	func confirmRegistration(code: String)
	func loginUser(username: String, password: String)
	func autoLoginUser(username: String, password: String)
	func resendVerificationLink()
	func forgotPassword(username: String)
}
