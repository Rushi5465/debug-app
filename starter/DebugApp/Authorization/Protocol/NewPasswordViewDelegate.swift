//
//  NewPasswordViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol NewPasswordViewDelegate: AnyObject {
	func confirmPasswordSuccess(with responseModel: EmptyResponseModel)
	func confirmPasswordErrorHandler(error: MovanoError)
	
}
