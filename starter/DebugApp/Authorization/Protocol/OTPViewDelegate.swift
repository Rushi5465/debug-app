//
//  OTPViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol OTPViewDelegate: AnyObject {
	func verifyUserSuccess(with responseModel: EmptyResponseModel)
	func verifyUserErrorHandler(error: MovanoError)
	func confirmRegistrationSuccess(with responseModel: EmptyResponseModel)
	func confirmRegistrationErrorHandler(error: MovanoError)
	func loginUserSuccess(with responseModel: LoginResponseModel)
	func loginUserErrorHandler(error: MovanoError)
	func autoLoginUserSuccess(with responseModel: LoginResponseModel)
	func autoLoginUserErrorHandler(error: MovanoError)
	func resendVerificationLinkSuccess(with responseModel: EmptyResponseModel)
	func resendVerificationLinkErrorHandler(error: MovanoError)
	func forgotPasswordSuccess(with responseModel: EmptyResponseModel)
	func forgotPasswordErrorHandler(error: MovanoError)
	
}
