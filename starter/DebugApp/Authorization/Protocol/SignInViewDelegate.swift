//
//  SignInViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol SignInViewDelegate: AnyObject {
	func loginUserSuccess(with responseModel: LoginResponseModel)
	func loginUserErrorHandler(error: MovanoError)
	
}
