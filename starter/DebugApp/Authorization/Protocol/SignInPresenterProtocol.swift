//
//  SignInPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol SignInPresenterProtocol {
	init(webservice: AuthorizationWebServiceProtocol, delegate: SignInViewDelegate)
	func loginUser(username: String, password: String)
	
}
