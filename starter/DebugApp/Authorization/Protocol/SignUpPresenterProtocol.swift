//
//  SignUpPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol SignUpPresenterProtocol {
	init(webservice: AuthorizationWebServiceProtocol, delegate: SignUpViewDelegate)
	func registerUser(user: User)
}
