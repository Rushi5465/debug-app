//
//  InitialPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/07/21.
//

import Foundation

protocol InitialPresenterProtocol {
	
	init(delegate: InitialViewDelegate)
	func fetchCarouselElements()
	func rescheduleTimer()
	func setCurrentOnboardingScreen(page: Int)
}
