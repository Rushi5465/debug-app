//
//  InitialViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/07/21.
//

import Foundation

protocol InitialViewDelegate: AnyObject {
	
	func fetchCarousel(items: [CarouselItem])
	func fetchCurrentCarouselItem(counter: Int)
}
