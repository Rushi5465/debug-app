//
//  SignupCompleteViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 06/12/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

class SignupCompleteViewController: UIViewController {
	
	@IBAction func pairButtonClicked(_ sender: Any) {
		self.showSecurityAlert()
	}
}
