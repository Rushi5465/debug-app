//
//  NewPasswordViewController.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 28/02/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

class NewPasswordViewController: UIViewController {
	
	@IBOutlet weak var password: MovanoTextField!
	@IBOutlet weak var reenterPasswordField: MovanoTextField!
	@IBOutlet weak var mincharButton: MovanoButton!
	@IBOutlet weak var upperCaseButton: MovanoButton!
	@IBOutlet weak var lowerCaseButton: MovanoButton!
	@IBOutlet weak var digitButton: MovanoButton!
	@IBOutlet weak var specialButton: MovanoButton!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var passwordRuleHeading: MovanoLabel!
	@IBOutlet weak var reenterPasswordErrorLabel: MovanoLabel!
	
	var emailToReset: String = ""
	var code: String = ""
	
	var presenter: NewPasswordPresenter?
	var passwordResetRequestModel: PasswordResetRequestModel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		registerPresenters()
		manageUI()
		_ = self.validatePassword(prospectiveText: self.password.text ?? "")
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		registerForKeyboardNotifications(self.scrollView)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		deregisterFromKeyboardNotifications()
	}
	
	@IBAction func resetPasswordClicked(_ sender: Any) {
		performResetPassword()
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
}

// MARK: - UITextFieldDelegate
extension NewPasswordViewController: UITextFieldDelegate {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		let currentText = textField.text ?? ""
		let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
		
		switch textField {
			case password:
				_ = validatePassword(prospectiveText: prospectiveText)
			default:
				break
		}
		return true
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		
		let nextTag = textField.tag + 1
		if let nextResponder = textField.superview?.viewWithTag(nextTag) {
			nextResponder.becomeFirstResponder()
		} else {
			textField.resignFirstResponder()
		}
		return true
	}
}

// MARK: - NewPasswordViewDelegate
extension NewPasswordViewController: NewPasswordViewDelegate {
	
	func confirmPasswordSuccess(with responseModel: EmptyResponseModel) {
		hud.dismiss()
		let resetController = ResetSuccessfulViewController.instantiate(fromAppStoryboard: .main)
		KeyChain.shared.clearTokens()
		UserDefault.shared.deleteAllUserDefault()
		NavigationUtility.setRootViewController(viewController: resetController)
	}
	
	func confirmPasswordErrorHandler(error: MovanoError) {
		hud.dismiss()
		self.showErrorAlert(error: error)
	}
}
