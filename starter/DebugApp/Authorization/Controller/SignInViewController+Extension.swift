//
//  SignInViewController+Extension.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit

@available(iOS 13.0, *)
extension SignInViewController {
	
	func registerPresenters() {
		if self.presenter == nil {
			let authService = AuthorizationWebService()
			self.presenter = SignInPresenter(webservice: authService, delegate: self)
		}
	}
	
	func assigningKeyboardTask() {
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
	}
	
	func manageUI() {
		email.text! =  KeyChain.shared.email ?? ""
	}
	
	func performLogin() {
		let emailValid = validateEmail(emailText: email.text ?? "")
		let passwordValid = validatePassword(passwordText: password.text ?? "")
		if emailValid && passwordValid {
			hud.show(in: self.view)
			self.presenter?.loginUser(username: email.text!, password: password.text!)
		}
	}
}
