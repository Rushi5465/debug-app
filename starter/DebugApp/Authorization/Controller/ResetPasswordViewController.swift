//
//  ResetPasswordViewController.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 22/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {
	
	@IBOutlet weak var email: MovanoTextField!
	@IBOutlet weak var emailErrorLabel: MovanoLabel!
	@IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backButton: MovanoButton!
	
	var presenter: ResetPasswordPresenter?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		registerPresenters()
		dismissKeyboardGesture()
		manageEmailField()
        manageBackButton()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		
		registerForKeyboardNotifications(self.scrollView)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		deregisterFromKeyboardNotifications()
	}
	
	@IBAction func resetPasswordClicked(_ sender: Any) {
		self.performResetPassword()
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
}

// MARK: - UITextFieldDelegate
extension ResetPasswordViewController: UITextFieldDelegate {
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		email.resignFirstResponder()
		return true
	}
	
}

// MARK: - ValidationDelegate
extension ResetPasswordViewController: ValidationDelegate {
	
	func validateEmail(emailText: String) -> Bool {
		if emailText.isEmpty == false && emailText.isValidEmail == true {
			emailErrorLabel.isHidden = true
			email.showNormalView(image: nil)
		} else {
			emailErrorLabel.isHidden = false
			email.showErrorView(image: nil)
			return false
		}
		return true
	}
	
}

// MARK: - ResetPasswordViewDelegate
extension ResetPasswordViewController: ResetPasswordViewDelegate {
	
	func forgotPasswordSuccess(with responseModel: EmptyResponseModel) {
		hud.dismiss()
		let otpController = OTPViewController.instantiate(fromAppStoryboard: .main)
		otpController.passwordResetRequestModel = PasswordResetRequestModel(password: "", username: self.email.text!, code: "")
		self.navigationController?.pushViewController(otpController, animated: true)
	}
	
	func forgotPasswordErrorHandler(error: MovanoError) {
		hud.dismiss()
		self.showErrorAlert(error: error)
	}
	
}
