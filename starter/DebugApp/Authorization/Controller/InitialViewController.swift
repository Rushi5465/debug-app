//
//  InitialViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 28/11/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    @IBOutlet weak var pageControl: CustomPagination!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var timer = Timer()
    private var counter = 0
    private var presenter: InitialPresenter?
    
    var carouselItems = [CarouselItem]() {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerPresenters()
        self.registerCollectionCell()
        self.assignDelegates()
        
        self.presenter?.fetchCarouselElements()
        self.presenter?.rescheduleTimer()
    }
    
    @IBAction func registerButtonClicked(_ sender: Any) {
        let controller = SignUpViewController.instantiate(fromAppStoryboard: .main)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func signinButtonClicked(_ sender: Any) {
        let controller = SignInViewController.instantiate(fromAppStoryboard: .main)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func registerCollectionCell() {
        let parameterCell = UINib.init(nibName: "OnboardCollectionViewCell", bundle: nil)
        self.collectionView.register(parameterCell, forCellWithReuseIdentifier: OnboardCollectionViewCell.IDENTIFIER)
    }
    
    func assignDelegates() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func registerPresenters() {
        if self.presenter == nil {
            self.presenter = InitialPresenter(delegate: self)
        }
    }
}

// MARK: - UICollectionViewDelegate
extension InitialViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
}

// MARK: - UICollectionViewDataSource
extension InitialViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.carouselItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = OnboardCollectionViewCell.dequeueReusableCell(collectionView: collectionView, indexPath: indexPath)
        
        let parameter = self.carouselItems[indexPath.row]
        
        cell.titleLabel.text = parameter.title
        cell.onboardImage.image = parameter.image
        
        return cell
    }
}

// MARK: - UIScrollViewDelegate
extension InitialViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.collectionView {
            let page = scrollView.contentOffset.x / scrollView.frame.size.width
            self.pageControl.currentPage = Int(page)
            self.presenter?.setCurrentOnboardingScreen(page: Int(page))
        }
    }
}

// MARK: - InitialViewDelegate
extension InitialViewController: InitialViewDelegate {
    
    func fetchCarousel(items: [CarouselItem]) {
        self.carouselItems = items
    }
    
    func fetchCurrentCarouselItem(counter: Int) {
        let index = IndexPath.init(row: (counter - 1), section: 0)
        self.collectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
        self.pageControl.currentPage = counter

    }
}
