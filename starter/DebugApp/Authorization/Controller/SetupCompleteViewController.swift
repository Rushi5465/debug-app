//
//  SetupCompleteViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 28/11/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

class SetupCompleteViewController: UIViewController {
	
	@IBAction func pairButtonClicked(_ sender: Any) {
		let controller = DeviceListViewController.instantiate(fromAppStoryboard: .bluetooth)
		controller.fromOnboarding = true
		self.navigationController?.pushViewController(controller, animated: true)
	}
}
