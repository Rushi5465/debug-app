//
//  OTPViewController.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 28/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController {
	
	@IBOutlet weak var subTitleLabel: MovanoLabel!
	@IBOutlet weak var pageControl: CustomPagination!
	@IBOutlet var proceedButton: MovanoButton!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var otpErrorLabel: MovanoLabel!
	@IBOutlet weak var backButton: MovanoButton!
	@IBOutlet var otpFields: [MovanoTextField]!
	
	var forgotPasswordEmail: String = ""
	var attributedButtonTitle: NSAttributedString?
	var isSignUp = Bool()
	var isSignIn = Bool()
	
	let attributes: [NSAttributedString.Key: Any] = [
		.font: UIFont.sfProDisplay.semiBold(withSize: 18),
		.foregroundColor: ColorConstants.kNavigationTitleColor
	]
	
	var showBack = Bool()
	var presenter: OTPPresenter?
	var passwordResetRequestModel: PasswordResetRequestModel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		registerPresenters()
		assigningNotificationTask()
		addToolBarToOTPFields()
		dismissKeyboardGesture()
		
		if self.passwordResetRequestModel != nil {
			self.subTitleLabel.text = "Enter the code sent on\n" + self.passwordResetRequestModel.username
		} else {
			let userEmail = KeyChain.shared.email != nil ? KeyChain.shared.email : ""
			self.subTitleLabel.text = "Enter the code sent on\n" + userEmail!
			self.passwordResetRequestModel = PasswordResetRequestModel(password: "", username: userEmail!, code: "")
		}
		
		self.backButton.isHidden = !showBack
		
		if isSignIn {
			hud.show(in: self.view)
			self.presenter?.resendVerificationLink()
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		registerForKeyboardNotifications(self.scrollView)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		for textField in otpFields {
			textField.becomeFirstResponder()
			return
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		deregisterFromKeyboardNotifications()
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func verifyOTP() {
		otpFields.sort { $0.tag < $1.tag}
		let allHaveText = otpFields.allSatisfy { $0.text?.isEmpty == false }
		if allHaveText {
			var otpValue = ""
			for item in otpFields {
				otpValue += item.text ?? ""
			}
			if isSignUp || (KeyChain.shared.userState ==  NavigationUtility.UserState.signedUp.rawValue) {
				//autoLogin()
				hud.show(in: self.view)
				self.presenter?.confirmRegistration(code: otpValue)
			} else {
				passwordResetRequestModel.code = otpValue
				let newController = NewPasswordViewController.instantiate(fromAppStoryboard: .main)
				newController.passwordResetRequestModel = passwordResetRequestModel
				self.navigationController?.pushViewController(newController, animated: true)
			}
		} else {
			self.otpErrorLabel.text = "Please enter 6 digit Passcode"
		}
	}
	
	@IBAction func resendOTP() {
		hud.show(in: self.view)
		self.presenter?.resendVerificationLink()
	}
}

// MARK: - OTPViewDelegate
extension OTPViewController: OTPViewDelegate {
	
	func forgotPasswordSuccess(with responseModel: EmptyResponseModel) {
		self.showAlert(title: "Movano", message: "Password has been sent on your email address")
	}
	
	func forgotPasswordErrorHandler(error: MovanoError) {
		self.showErrorAlert(error: error)
		hud.dismiss()
	}
	
	func resendVerificationLinkSuccess(with responseModel: EmptyResponseModel) {
		hud.dismiss()
		let message = responseModel.message
		self.showAlert(title: "Movano", message: message)
	}
	
	func resendVerificationLinkErrorHandler(error: MovanoError) {
		hud.dismiss()
		self.showErrorAlert(error: error)
	}
	
	func autoLoginUserSuccess(with responseModel: LoginResponseModel) {
		hud.dismiss()
		
		let mfaEnabled = responseModel.data.MFA_enabled
		KeyChain.shared.mfa = mfaEnabled
		if !mfaEnabled {
			KeyChain.shared.saveUserSession(model: responseModel.data)
		}
		KeyChain.shared.userState = NavigationUtility.UserState.verified.rawValue
		let controller = SignupCompleteViewController.instantiate(fromAppStoryboard: .main)
		self.navigationController?.pushViewController(controller, animated: false)
	}
	
	func autoLoginUserErrorHandler(error: MovanoError) {
		hud.dismiss()
		self.showErrorAlert(error: error)
	}
	
	func loginUserSuccess(with responseModel: LoginResponseModel) {
		hud.dismiss()
		
		let mfaEnabled = responseModel.data.MFA_enabled
		KeyChain.shared.mfa = mfaEnabled
		if !mfaEnabled {
			KeyChain.shared.saveUserSession(model: responseModel.data)
		}
		self.showAlert(title: "Movano", message: "OTP has been sent on your mobile number")
	}
	
	func loginUserErrorHandler(error: MovanoError) {
		hud.dismiss()
		self.showErrorAlert(error: error)
	}
	
	func verifyUserSuccess(with responseModel: EmptyResponseModel) {
		KeyChain.shared.userState = NavigationUtility.UserState.verified.rawValue
		hud.dismiss()
		NavigationUtility.setInitialController()
	}
	
	func verifyUserErrorHandler(error: MovanoError) {
		hud.dismiss()
		self.showErrorAlert(error: error)
	}
	
	func confirmRegistrationSuccess(with responseModel: EmptyResponseModel) {
		self.autoLogin()
	}
	
	func confirmRegistrationErrorHandler(error: MovanoError) {
		hud.dismiss()
		self.showErrorAlert(error: error)
	}
}
