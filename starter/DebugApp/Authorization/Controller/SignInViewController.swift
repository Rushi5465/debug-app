//
//  SignInViewController.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 21/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

@available(iOS 13.0, *)
class SignInViewController: UIViewController {
	
	@IBOutlet weak var email: PaddingTextField!
	@IBOutlet weak var password: PaddingTextField!
	@IBOutlet weak var emailErrorLabel: MovanoLabel!
	@IBOutlet weak var passwordErrorLabel: MovanoLabel!
	@IBOutlet weak var loginButton: MovanoButton!
	@IBOutlet weak var signupButton: MovanoButton!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var faceIDLoginButton: MovanoButton!
	
	@IBOutlet weak var faceIdButtonHeight: NSLayoutConstraint!
	@IBOutlet weak var faceIdButtonTop: NSLayoutConstraint!
	
	var presenter: SignInPresenter?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.registerPresenters()
		self.assigningKeyboardTask()
		self.navigationItem.hidesBackButton = true
		self.dismissKeyboardGesture()
		self.registerForKeyboardNotifications(self.scrollView)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.manageUI()
		
		if let user = KeyChain.shared.currentUser, user.isSecurityEnabled {
			self.faceIDLoginButton.isHidden = false
			self.faceIdButtonHeight.constant = 33
			self.faceIdButtonTop.constant = 22
		} else {
			self.faceIDLoginButton.isHidden = true
			self.faceIdButtonHeight.constant = 0
			self.faceIdButtonTop.constant = 0
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		deregisterFromKeyboardNotifications()
	}
	
	@IBAction func loginButtonClicked(_ sender: Any) {
		self.performLogin()
	}
	
	@IBAction func forgotPasswordClicked(_ sender: Any) {
		let controller = ResetPasswordViewController.instantiate(fromAppStoryboard: .main)
		self.navigationController?.pushViewController(controller, animated: true)
	}
    
	@IBAction func registerButtonClicked(_ sender: Any) {
		let controller = SignUpViewController.instantiate(fromAppStoryboard: .main)
		self.navigationController?.pushViewController(controller, animated: true)
	}
	
	@IBAction func loginWithFaceIDClicked(_ sender: Any) {
		checkForAuthenticationSupport()
	}
}

// MARK: - ValidationDelegate
@available(iOS 13.0, *)
extension SignInViewController: ValidationDelegate {
	
	func validateEmail(emailText: String) -> Bool {
		if emailText.isEmpty == false && emailText.isValidEmail == true {
			emailErrorLabel.isHidden = true
		} else {
			emailErrorLabel.isHidden = false
			return false
		}
		return true
	}
	
	func validatePassword(passwordText: String) -> Bool {
		if passwordText.isEmpty == false {
			passwordErrorLabel.isHidden = true
		} else {
			passwordErrorLabel.isHidden = false
			return false
		}
		return true
	}
}

// MARK: - UITextFieldDelegate
@available(iOS 13.0, *)
extension SignInViewController: UITextFieldDelegate {
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		
		let currentText = textField.text ?? ""
		let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
		
		switch textField {
			case email:
				_ = validateEmail(emailText: prospectiveText)
			case password:
				_ = validatePassword(passwordText: prospectiveText)
			default:
				break
		}
		return true
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == email {
			password.becomeFirstResponder()
		} else {
			password.resignFirstResponder()
			self.performLogin()
		}
		return true
	}
}

// MARK: - SignInViewDelegate
extension SignInViewController: SignInViewDelegate {
	
	func loginUserSuccess(with responseModel: LoginResponseModel) {
		
		let mfaEnabled = responseModel.data.MFA_enabled
		KeyChain.shared.mfa = mfaEnabled
		if !mfaEnabled {
			KeyChain.shared.saveUserSession(model: responseModel.data)
		}

		KeyChain.shared.email = self.email.text!
		KeyChain.shared.password = self.password.text!
		
		let allUsers = KeyChain.shared.userAccounts
		let index = allUsers.indexOf(KeyChain.shared.email!)
		if let index = index {
			KeyChain.shared.currentUser = allUsers[index]
		} else {
			KeyChain.shared.currentUser = UserAccount(username: self.email.text!, password: self.password.text!, isSecurityEnabled: false)
		}
		
		KeyChain.shared.userState =  NavigationUtility.UserState.onBoarded.rawValue
		
		let securedAccounts = KeyChain.shared.userAccounts.filter { (account) -> Bool in
			return account.isSecurityEnabled
		}
		if KeyChain.shared.currentUser?.remindLater != false && securedAccounts.isEmpty {
			self.showSecurityAlert()
		} else {
			NavigationUtility.setInitialController()
		}
	}
	
	func loginUserErrorHandler(error: MovanoError) {
		hud.dismiss()
		if error.errorMessage == "Confirm your user registration before proceeding" {
			var actions = [UIAlertAction]()
			actions.append(UIAlertAction(title: "Proceed", style: .cancel, handler: { (_) in
				KeyChain.shared.email = self.email.text!
				KeyChain.shared.password = self.password.text!
				
				KeyChain.shared.currentUser = UserAccount(username: self.email.text!, password: self.password.text!, isSecurityEnabled: false)
				KeyChain.shared.userState = NavigationUtility.UserState.signedUp.rawValue
				let successController = OTPViewController.instantiate(fromAppStoryboard: .main)
				successController.showBack = false
				successController.isSignUp = true
				successController.isSignIn = true
				successController.passwordResetRequestModel = PasswordResetRequestModel(password: self.password.text!, username: self.email.text!, code: "")
				self.navigationController?.pushViewController(successController, animated: true)
			}))
			self.showAlert(title: "Movano", message: error.errorMessage, actions: actions)
		} else {
			self.showErrorAlert(error: error)
		}
	}
	
}
