//
//  SignUpViewController.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 07/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import UIKit

@objc public protocol ValidationDelegate {
	@objc optional func validateName() -> Bool
	func validateEmail(emailText: String) -> Bool
	@objc optional func validatePassword(passwordText: String) -> Bool
}

class SignUpViewController: UIViewController {
	
	let emailTopConstant: CGFloat =  31.5
	let emailTopConstantWithOTP: CGFloat = 145.0
	
	@IBOutlet weak var scrollView: UIScrollView!
	
	@IBOutlet weak var firstName: MovanoTextField!
	@IBOutlet weak var lastName: MovanoTextField!
	@IBOutlet weak var email: MovanoTextField!
	@IBOutlet weak var password: PasswordTextField!
	
	@IBOutlet weak var mincharButton: MovanoButton!
	@IBOutlet weak var upperCaseButton: MovanoButton!
	@IBOutlet weak var lowerCaseButton: MovanoButton!
	@IBOutlet weak var digitButton: MovanoButton!
	@IBOutlet weak var specialButton: MovanoButton!
	
	@IBOutlet weak var emailErrorLabel: MovanoLabel!
	@IBOutlet weak var firstNameErrorLabel: MovanoLabel!
	@IBOutlet weak var lastNameErrorLabel: MovanoLabel!
	@IBOutlet weak var passwordErrorLabel: MovanoLabel!
	
	var countryCodeValid: Bool = false
	var isPhoneValid: Bool = false
	
	@IBOutlet weak var nextButton: StateButton!
	
	@IBOutlet weak var hideEmailErrorLabel: NSLayoutConstraint!
	@IBOutlet weak var hideFirstNameErrorLabel: NSLayoutConstraint!
	@IBOutlet weak var hideLastNameErrorLabel: NSLayoutConstraint!
	@IBOutlet weak var hidePasswordErrorLabel: NSLayoutConstraint!
	@IBOutlet weak var passwordRuleHeading: MovanoLabel!
	
	var verificationCode: String = ""
	var presenter: SignUpPresenter?
	
	var isMandatoryFieldFilled : Bool {
		let validFirstName = !(self.firstName.text?.trim.isEmpty)!
		let validLastName = !(self.lastName.text?.trim.isEmpty)!
		let validEmail = !(self.email.text?.trim.isEmpty)! && (self.email.text?.trim.isValidEmail)!
		let validPass = !(self.password.text?.trim.isEmpty)! && (self.password.text?.trim.isValidPassword)!
		
		return validFirstName && validLastName && validEmail && validPass
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if self.presenter == nil {
			let authService = AuthorizationWebService()
			self.presenter = SignUpPresenter(webservice: authService, delegate: self)
		}
		
		hideEmailErrorLabel.isActive = true
		hideFirstNameErrorLabel.isActive = true
		hideLastNameErrorLabel.isActive = true
		hidePasswordErrorLabel.isActive = true
		
		emailErrorLabel.text = ErrorString.invalidEmail
		
		firstName.addBottomBorder()
		firstName.addPadding(.left(10))
		lastName.addBottomBorder()
		lastName.addPadding(.left(10))
		email.addBottomBorder()
        email.addPadding(.left(10))
		password.addBottomBorder()
        password.addPadding(.left(10))
		
		self.nextButton.isInteractionEnabled = self.isMandatoryFieldFilled
        self.nextButton.setTitleColor(UIColor.init(named: "ButtonText"), for: .normal)
		
		self.passwordRuleHeading.addImage(image: #imageLiteral(resourceName: "checkmarkgreen-all"))
		self.managePasswordRules(self.password.text ?? "")
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		deregisterFromKeyboardNotifications()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		registerForKeyboardNotifications(self.scrollView)
	}
	
	@objc func toggleViewPassword() {
		password.isSecureTextEntry.toggle()
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popToRootViewController(animated: true)
	}
	
	@IBAction func register() {
		let firstNameValid = validateFirstName(firstNameText: firstName.text!.trim)
		let lastNameValid = validateLastName(lastNameText: lastName.text!.trim)
		let emailValid = validateEmail(emailText: email.text ?? "")
		
		let passwordValid = validatePassword(prospectiveText: password.text ?? "")
		
		if firstNameValid && lastNameValid && emailValid && passwordValid {
			let user = User(firstName: firstName.text!, lastName: lastName.text!, userName: email.text!, password: password.text!)
			
			hud.show(in: self.view)
			self.presenter?.registerUser(user: user)
		}
	}
}

// MARK: - ValidationDelegate
extension SignUpViewController: ValidationDelegate {
	
	func validateFirstName(firstNameText: String) -> Bool {
		if firstNameText.isEmpty == true {
			firstName.showErrorView()
			firstNameErrorLabel.text = ErrorString.blankFirstName
			lastNameErrorLabel.text = "    "
			firstNameErrorLabel.isHidden = false
			hideFirstNameErrorLabel.isActive = false
			lastNameErrorLabel.isHidden = false
			hideLastNameErrorLabel.isActive = false
			return false
		} else {
			
			if firstNameText.isAlphabetic {
				firstName.showNormalView()
				firstNameErrorLabel.isHidden = true
				hideFirstNameErrorLabel.isActive = true
			} else {
				firstName.showErrorView()
				firstNameErrorLabel.text = ErrorString.invalidFirstName
				lastNameErrorLabel.text = "    "
				firstNameErrorLabel.isHidden = false
				hideFirstNameErrorLabel.isActive = false
				lastNameErrorLabel.isHidden = false
				hideLastNameErrorLabel.isActive = false
			}
		}
		return true
	}
	
	func validateLastName(lastNameText: String) -> Bool {
		if lastNameText.isEmpty == true {
			lastName.showErrorView()
			lastNameErrorLabel.text = ErrorString.blankLastName
			firstNameErrorLabel.text = "    "
			lastNameErrorLabel.isHidden = false
			hideLastNameErrorLabel.isActive = false
			firstNameErrorLabel.isHidden = false
			hideFirstNameErrorLabel.isActive = false
			return false
		} else {
			if lastNameText.isAlphabetic {
				lastName.showNormalView()
				lastNameErrorLabel.isHidden = true
				hideLastNameErrorLabel.isActive = true
			} else {
				lastName.showErrorView()
				lastNameErrorLabel.text = ErrorString.invalidLastName
				firstNameErrorLabel.text = "    "
				lastNameErrorLabel.isHidden = false
				hideLastNameErrorLabel.isActive = false
				firstNameErrorLabel.isHidden = false
				hideFirstNameErrorLabel.isActive = false
			}
		}
		return true
	}
	
	func validateEmail(emailText: String) -> Bool {
		if emailText.isEmpty == false && emailText.isValidEmail == true {
			emailErrorLabel.isHidden = true
			hideEmailErrorLabel.isActive = true
			email.showNormalView(image: nil)
		} else {
			emailErrorLabel.isHidden = false
			hideEmailErrorLabel.isActive = false
			email.showErrorView(image: nil)
			return false
		}
		return true
	}
	
	fileprivate func managePasswordRules(_ prospectiveText: String) {
		if prospectiveText.count >= 8 {
			mincharButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			mincharButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		
		if prospectiveText.hasUpperCase {
			upperCaseButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			upperCaseButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		if prospectiveText.hasLowerCase {
			lowerCaseButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			lowerCaseButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		if prospectiveText.hasDigit {
			digitButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			digitButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		if prospectiveText.hasSpecialCharacter {
			specialButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			specialButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		
		mincharButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (mincharButton.bounds.width - 25), bottom: 0, right: 5)
		mincharButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((mincharButton.imageView?.frame.width)! + 25))
		
		upperCaseButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (upperCaseButton.bounds.width - 25), bottom: 0, right: 5)
		upperCaseButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((upperCaseButton.imageView?.frame.width)! + 25))
		
		lowerCaseButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (lowerCaseButton.bounds.width - 25), bottom: 0, right: 5)
		lowerCaseButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((lowerCaseButton.imageView?.frame.width)! + 25))
		
		digitButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (digitButton.bounds.width - 25), bottom: 0, right: 5)
		digitButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((digitButton.imageView?.frame.width)! + 25))
		
		specialButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (specialButton.bounds.width - 25), bottom: 0, right: 5)
		specialButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((specialButton.imageView?.frame.width)! + 25))
	}
	
	func validatePassword(prospectiveText: String) -> Bool {
		managePasswordRules(prospectiveText)
		if prospectiveText.isEmpty == false && prospectiveText.isValidPassword == true {
			password.showNormalView(image: nil)
			passwordErrorLabel.isHidden = true
			hidePasswordErrorLabel.isActive = true
		} else {
			password.showErrorView(image: nil)
			passwordErrorLabel.isHidden = false
			hidePasswordErrorLabel.isActive = false
			return false
		}
		return true
	}
}

// MARK: - UITextFieldDelegate
extension SignUpViewController: UITextFieldDelegate {
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		
		let currentText = textField.text ?? ""
		let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
		switch textField {
			case firstName:
				_ = validateFirstName(firstNameText: prospectiveText)
			case lastName:
				_ = validateLastName(lastNameText: prospectiveText)
			case email:
				_ = validateEmail(emailText: prospectiveText)
			case password:
				_ = validatePassword(prospectiveText: prospectiveText)
			default:
				break
		}
		self.nextButton.isInteractionEnabled = self.isMandatoryFieldFilled
        self.nextButton.setTitleColor(UIColor.init(named: "ButtonText"), for: .normal)
		return true
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		let nextTag = textField.tag + 1
		if let nextResponder = textField.superview?.viewWithTag(nextTag) {
			nextResponder.becomeFirstResponder()
		} else {
			textField.resignFirstResponder()
		}
		return true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		let prospectiveText = textField.text!
		switch textField {
			case firstName:
				_ = validateFirstName(firstNameText: prospectiveText)
			case lastName:
				_ = validateLastName(lastNameText: prospectiveText)
			case email:
				_ = validateEmail(emailText: prospectiveText)
			case password:
				_ = validatePassword(prospectiveText: prospectiveText)
			default:
				break
		}
		self.nextButton.isInteractionEnabled = self.isMandatoryFieldFilled
	}
}

// MARK: - SignUpViewDelegate
extension SignUpViewController: SignUpViewDelegate {
	
	func registerUserSuccess(with responseModel: EmptyResponseModel, user: User) {
		if let username =  KeyChain.shared.email {
			if username == user.userName {
				KeyChain.shared.setfalseOnBoardingComplete(user: username)
			}
		}
		let name = self.firstName.text! + " " + self.lastName.text!
		KeyChain.shared.fullName = name
		KeyChain.shared.firstName = user.firstName
		KeyChain.shared.lastName = user.lastName
		KeyChain.shared.email = user.userName
		KeyChain.shared.password = user.password
		KeyChain.shared.userState =  NavigationUtility.UserState.signedUp.rawValue
		
		KeyChain.shared.currentUser = UserAccount(username: user.userName, password: user.password, isSecurityEnabled: false)
		hud.dismiss()
		
		let successController = OTPViewController.instantiate(fromAppStoryboard: .main)
		successController.showBack = false
		successController.isSignUp = true
		successController.passwordResetRequestModel = PasswordResetRequestModel(password: password.text!, username: email.text!, code: "")
		self.navigationController?.pushViewController(successController, animated: true)
	}
	
	func registerUserErrorHandler(error: MovanoError) {
		hud.dismiss()
		self.showErrorAlert(error: error)
	}
}
