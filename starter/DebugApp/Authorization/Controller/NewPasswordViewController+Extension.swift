//
//  NewPasswordViewController+Extension.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 17/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

extension NewPasswordViewController {
	
	func registerPresenters() {
		if self.presenter == nil {
			let authService = AuthorizationWebService()
			self.presenter = NewPasswordPresenter(webservice: authService, delegate: self)
		}
	}
	
	func validatePassword(prospectiveText: String) -> Bool {
		if prospectiveText.count >= 8 {
			mincharButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			mincharButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		
		if prospectiveText.hasUpperCase {
			upperCaseButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			upperCaseButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		if prospectiveText.hasLowerCase {
			lowerCaseButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			lowerCaseButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		if prospectiveText.hasDigit {
			digitButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			digitButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		if prospectiveText.hasSpecialCharacter {
			specialButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			specialButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		
		mincharButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (mincharButton.bounds.width - 25), bottom: 0, right: 5)
		mincharButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((mincharButton.imageView?.frame.width)! + 25))
		
		upperCaseButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (upperCaseButton.bounds.width - 25), bottom: 0, right: 5)
		upperCaseButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((upperCaseButton.imageView?.frame.width)! + 25))
		
		lowerCaseButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (lowerCaseButton.bounds.width - 25), bottom: 0, right: 5)
		lowerCaseButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((lowerCaseButton.imageView?.frame.width)! + 25))
		
		digitButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (digitButton.bounds.width - 25), bottom: 0, right: 5)
		digitButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((digitButton.imageView?.frame.width)! + 25))
		
		specialButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (specialButton.bounds.width - 25), bottom: 0, right: 5)
		specialButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((specialButton.imageView?.frame.width)! + 25))
		
		if prospectiveText.isEmpty == false && prospectiveText.isValidPassword == true {
		} else {
			return false
		}
		return true
	}
	
	func performResetPassword() {
		
		if self.password.text!.trim.isEmpty {
			self.reenterPasswordErrorLabel.errorText(text: "Password Field can't be blank")
		} else if self.password.text == self.reenterPasswordField.text {
			let pwdValid = validatePassword(prospectiveText: password.text!)
			if  pwdValid {
				
				hud.show(in: self.view)
				self.passwordResetRequestModel.password = password.text!
				self.presenter?.confirmForgotPassword(request: self.passwordResetRequestModel)
			}
		} else {
			self.reenterPasswordErrorLabel.errorText(text: "Password doesn't match")
		}
	}
	
	func addFieldPadding() {
		password.addPadding(.left(10))
		reenterPasswordField.addPadding(.left(10))
	}
	
	func manageUI() {
		self.passwordRuleHeading.addImage(image: #imageLiteral(resourceName: "checkmarkgreen-all"))
		addFieldPadding()
		password.addBottomBorder()
		reenterPasswordField.addBottomBorder()
	}
}
