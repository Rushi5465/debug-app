//
//  ResetPasswordViewController+Extension.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 17/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit

extension ResetPasswordViewController {
	
	func registerPresenters() {
		if self.presenter == nil {
			let authService = AuthorizationWebService()
			self.presenter = ResetPasswordPresenter(webservice: authService, delegate: self)
		}
	}
	
	func manageEmailField() {
		email.addBottomBorder()
        email.addPadding(.left(15.0))
	}
	
	func performResetPassword() {
		if validateEmail(emailText: email.text!) {
			hud.show(in: self.view)
			self.presenter?.forgotPassword(username: email.text!)
		}
	}
    
    func manageBackButton() {
        backButton.layer.borderWidth = 2.0
        backButton.layer.borderColor = UIColor(named: "Button")?.cgColor
    }
}
