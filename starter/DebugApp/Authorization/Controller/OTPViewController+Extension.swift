//
//  OTPViewController+Extension.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 19/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

extension OTPViewController {
	
	func registerPresenters() {
		if presenter == nil {
			let authService = AuthorizationWebService()
			self.presenter = OTPPresenter(webservice: authService, delegate: self)
		}
	}
	
	func autoLogin() {
		if let user = KeyChain.shared.currentUser {
			 hud.show(in: self.view)
			self.presenter?.autoLoginUser(username: user.username, password: user.password)
		}
	}
	
	func assigningNotificationTask() {
		NotificationCenter.default.addObserver(self, selector: #selector(verifyOTP), name: NSNotification.Name(rawValue: "otpEntered"), object: nil)
		
	}
	
	func addToolBarToOTPFields() {
		for item in otpFields {
			item.showToolBar()
		}
	}
}
