//
//  AuthorizationWebService.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

class AuthorizationWebService: AuthorizationWebServiceProtocol {
	
	private var service: RestService
	
	init(service: RestService = RestService.shared) {
		self.service = service
	}
	
	func inviteUser(code: String, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.inviteCode)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		let parameters = ["verification_code": code]
		
		var header = Header()
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		
		let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters)
		service.request(apiType: APIType.INVITE_USER, request: request) { (result) in
			switch result {
				case .success(let response):
					if let responseModel = try? JSONDecoder().decode(EmptyResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
	
	func registerUser(user: User, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void) {
		
		do {
			let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.register)!
			
			guard let url = urlComponent.url else {
				completionHandler(nil, MovanoError.invalidRequestURL)
				return
			}
			
			let jsonData = try JSONEncoder().encode(user)
			var parameters = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [String: Any]
			parameters["preferred_timezone"] = TimeZone.current.identifier
			
			var header = Header()
			header.addHeader(key: "Authorization", value: APPURL.authToken)
			
			let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters)
			
			service.request(apiType: APIType.REGISTER_USER, request: request) { (result) in
				switch result {
					case .success(let response):
						if let responseModel = try? JSONDecoder().decode(EmptyResponseModel.self, from: response.rawData()) {
							completionHandler(responseModel, nil)
						} else {
							completionHandler(nil, MovanoError.invalidResponseModel)
					}
					case .failure(let error):
						completionHandler(nil, error)
				}
			}
		} catch {
		}
	}
	
	func verifyPhone(phoneNumber: String, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.phoneVerification)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		let parameters = ["phone_number": phoneNumber]
		
		var header = Header()
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		
		let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters as [String: Any])
		
		service.request(apiType: APIType.VERIFY_PHONE_API, request: request) { (result) in
			switch result {
				case .success(let response):
					if let responseModel = try? JSONDecoder().decode(EmptyResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
	
	func confirmForgotPassword(request: PasswordResetRequestModel, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void) {
		
		do {
			let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.confirmForgotPassword)!
			
			guard let url = urlComponent.url else {
				completionHandler(nil, MovanoError.invalidRequestURL)
				return
			}
			
			var header = Header()
			header.addHeader(key: "Authorization", value: APPURL.authToken)
			
			let jsonData = try JSONEncoder().encode(request)
			let parameters = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [String: Any]
			
			let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters)
			
			service.request(apiType: APIType.CONFIRM_FORGOT_PASSWORD_API, request: request) { (result) in
				switch result {
					case .success(let response):
						if let responseModel = try? JSONDecoder().decode(EmptyResponseModel.self, from: response.rawData()) {
							completionHandler(responseModel, nil)
						} else {
							completionHandler(nil, MovanoError.invalidResponseModel)
					}
					case .failure(let error):
						completionHandler(nil, error)
				}
			}
		} catch {
		}
	}
	
	func verifyUser(code: String, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.verify)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		var header = Header()
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		
		let challengeName = KeyChain.shared.challengeName
		let username = KeyChain.shared.usernameSrp
		let session = KeyChain.shared.sessionId
		let parameters = ["challange_name": challengeName,
						  "mfa_code": code,
						  "username_for_srp": username,
						  "session": session as Any] as [String: Any]
		
		let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters)
		
		service.request(apiType: APIType.VERIFY_USER_API, request: request) { (result) in
			switch result {
				case .success(let response):
					if let responseModel = try? JSONDecoder().decode(EmptyResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
	
	func confirmRegistration(code: String, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.confirmRegistration)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		let username =  KeyChain.shared.email
		let parameters = ["username": username, "code": code]
		
		var header = Header()
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		
		let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters as [String: Any])
		
		service.request(apiType: APIType.CONFIRM_REGISTRATION_CODE_API, request: request) { (result) in
			switch result {
				case .success(let response):
					if let responseModel = try? JSONDecoder().decode(EmptyResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
	
	func loginUser(username: String, password: String, completionHandler: @escaping (LoginResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.login)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		let parameters = ["username": username, "password": password, "device_id": "1234"]

		var header = Header()
		header.addHeader(key: "Authorization", value: APPURL.authToken)

		let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters as [String: Any])
		service.request(apiType: APIType.LOGIN_USER, request: request) { (result) in
			switch result {
				case .success(let response):
					if let responseModel = try? JSONDecoder().decode(LoginResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
	
	func resendVerificationLink(completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.resendLink)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		let userName = KeyChain.shared.currentUser?.username
		let parameters = ["username": userName]
		
		var header = Header()
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		
		let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters as [String: Any])
		
		service.request(apiType: APIType.RESEND_VERIFICATION_LINK_USER_API, request: request) { (result) in
			switch result {
				case .success(let response):
					if let responseModel = try? JSONDecoder().decode(EmptyResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
	
	func forgotPassword(username: String, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.forgotPassword)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		let parameters = ["username": username]
		
		var header = Header()
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		
		let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters as [String: Any])
		service.request(apiType: APIType.FORGOT_PASSWORD_API, request: request) { (result) in
			switch result {
				case .success(let response):
					if let responseModel = try? JSONDecoder().decode(EmptyResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}

//	func changePasswordLink(request: ChangePasswordRequestModel, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void) {
//
//		do {
//			let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.changePassword)!
//
//			guard let url = urlComponent.url else {
//				completionHandler(nil, MovanoError.invalidRequestURL)
//				return
//			}
//
//			let jsonData = try JSONEncoder().encode(request)
//			let parameters = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [String: Any]
//
//			var header = Header()
//			header.addHeader(key: "Authorization", value: KeyChain.shared.idToken)
//			header.addHeader(key: "AccessToken", value: KeyChain.shared.accessToken!)
//
//			let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters)
//			service.request(apiType: APIType.CHANGE_PASSWORD_LINK_API, request: request) { (result) in
//				switch result {
//					case .success(let response):
//						if let responseModel = try? JSONDecoder().decode(EmptyResponseModel.self, from: response.rawData()) {
//							completionHandler(responseModel, nil)
//						} else {
//							completionHandler(nil, MovanoError.invalidResponseModel)
//					}
//					case .failure(let error):
//						completionHandler(nil, error)
//				}
//			}
//		} catch {
//
//		}
//	}
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					//ApolloClientService().setDefaultClient()
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
