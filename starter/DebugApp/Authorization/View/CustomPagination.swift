//
//  CustomPagination.swift
//  BloodPressureApp
//
//  Created by Rushikant on 15/07/21.
//

import UIKit

@IBDesignable
class CustomPagination: UIView {

    // MARK: - IBOutlets
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var collectionViewPagination: UICollectionView!
	@IBOutlet weak var collectionWidth
		: NSLayoutConstraint!

	private var _paginationCount = Int()
	private var _currentPage = Int() {
		didSet {
			self.collectionViewPagination.reloadData()
		}
	}
    
    @IBInspectable
    public var paginationCount: Int = 0 {
        didSet {
            self._paginationCount = self.paginationCount
        }
    }
    
	@IBInspectable
	var currentPage: Int = 0 {
        didSet {
            self._currentPage = currentPage
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CustomPagination", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        initCollectionView()
		self.collectionViewPagination.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.collectionWidth.constant = self.collectionViewPagination.contentSize.width
	}
    
    private func initCollectionView() {
      let nib = UINib(nibName: "CustomCollectionViewCell", bundle: nil)
        collectionViewPagination.register(nib, forCellWithReuseIdentifier: "CustomCollectionViewCell")
		collectionViewPagination.delegate = self
        collectionViewPagination.dataSource = self
    }
}

extension CustomPagination: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: 30, height: 5)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

		let totalCellWidth = 30 * paginationCount
		let totalSpacingWidth = 5 * (paginationCount - 1)

		let leftInset = (200 - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
		let rightInset = leftInset

		return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
	}
}

extension CustomPagination: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paginationCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = CustomCollectionViewCell.dequeueReusableCell(collectionView: collectionView, indexPath: indexPath)
		
        if _currentPage >= indexPath.item {
            cell.lblTitle.backgroundColor = UIColor.avgColor
        } else {
            cell.lblTitle.backgroundColor = UIColor.textColor
        }
        
        return cell
    }
}
