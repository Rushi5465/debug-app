//
//  OnboardCollectionViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/07/21.
//

import UIKit

class OnboardCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var onboardImage: UIImageView!
	@IBOutlet weak var titleLabel: MovanoLabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
