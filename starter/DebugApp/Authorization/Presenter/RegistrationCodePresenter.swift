//
//  RegistrationCodePresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 15/01/21.
//  Copyright © 2021 Movano. All rights reserved.
//

import Foundation

class RegistrationCodePresenter: RegistrationCodePresenterProtocol {
	
	private var webservice: AuthorizationWebServiceProtocol
	private weak var delegate: RegistrationCodeViewDelegate?
	
	required init(webservice: AuthorizationWebServiceProtocol, delegate: RegistrationCodeViewDelegate) {
		self.webservice = webservice
		self.delegate = delegate
	}
	
	func inviteUser(code: String) {
		webservice.inviteUser(code: code) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.inviteUserErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.inviteUserSuccess(with: response)
			}
		}
	}
}
