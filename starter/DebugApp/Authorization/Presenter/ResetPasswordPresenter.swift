//
//  ResetPasswordPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

class ResetPasswordPresenter: ResetPasswordPresenterProtocol {
	
	private var webservice: AuthorizationWebServiceProtocol
	private weak var delegate: ResetPasswordViewDelegate?
	
	required init(webservice: AuthorizationWebServiceProtocol, delegate: ResetPasswordViewDelegate) {
		self.webservice = webservice
		self.delegate = delegate
	}
	
	func forgotPassword(username: String) {
		self.webservice.forgotPassword(username: username) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.forgotPasswordErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.forgotPasswordSuccess(with: response)
			}
		}
	}

}
