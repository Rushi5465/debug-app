//
//  NewPasswordPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

class NewPasswordPresenter: NewPasswordPresenterProtocol {
	
	private var webservice: AuthorizationWebServiceProtocol
	private weak var delegate: NewPasswordViewDelegate?
	
	required init(webservice: AuthorizationWebServiceProtocol, delegate: NewPasswordViewDelegate) {
		self.webservice = webservice
		self.delegate = delegate
	}
	
	func confirmForgotPassword(request: PasswordResetRequestModel) {
		self.webservice.confirmForgotPassword(request: request) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.confirmPasswordErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.confirmPasswordSuccess(with: response)
			}
		}
	}
}
