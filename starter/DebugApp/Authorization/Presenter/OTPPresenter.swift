//
//  OTPPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

class OTPPresenter: OTPPresenterProtocol {
	
	private var webservice: AuthorizationWebServiceProtocol
	private weak var delegate: OTPViewDelegate?
	
	required init(webservice: AuthorizationWebServiceProtocol, delegate: OTPViewDelegate) {
		self.webservice = webservice
		self.delegate = delegate
	}
	
	func verifyUser(code: String) {
		self.webservice.verifyUser(code: code) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.verifyUserErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.verifyUserSuccess(with: response)
			}
		}
	}
	
	func confirmRegistration(code: String) {
		self.webservice.confirmRegistration(code: code) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.confirmRegistrationErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.confirmRegistrationSuccess(with: response)
			}
		}
	}
	
	func loginUser(username: String, password: String) {
		self.webservice.loginUser(username: username, password: password) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.loginUserErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.loginUserSuccess(with: response)
			}
		}
	}
	
	func autoLoginUser(username: String, password: String) {
		self.webservice.loginUser(username: username, password: password) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.autoLoginUserErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.autoLoginUserSuccess(with: response)
			}
		}
	}
	
	func resendVerificationLink() {
		self.webservice.resendVerificationLink { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.resendVerificationLinkErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.resendVerificationLinkSuccess(with: response)
			}
		}
	}
	
	func forgotPassword(username: String) {
		self.webservice.forgotPassword(username: username) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.resendVerificationLinkErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.resendVerificationLinkSuccess(with: response)
			}
		}
	}
}
