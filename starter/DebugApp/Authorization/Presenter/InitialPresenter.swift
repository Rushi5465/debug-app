//
//  InitialPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/07/21.
//

import Foundation

class InitialPresenter: InitialPresenterProtocol {
	
	private weak var delegate: InitialViewDelegate?
	private var timer = Timer()
	private var counter = 0
	private var carouselItems = [CarouselItem]()
	
	required init(delegate: InitialViewDelegate) {
		self.delegate = delegate
	}
	
	func fetchCarouselElements() {
		let item1 = CarouselItem(title: "Understand your vitals", image: #imageLiteral(resourceName: "initialScreen-1"))
		let item2 = CarouselItem(title: "Track your exercise", image: #imageLiteral(resourceName: "initialScreen-2"))
		let item3 = CarouselItem(title: "View your sleep patterns", image: #imageLiteral(resourceName: "initialScreen-3"))
		
		self.carouselItems = [item1, item2, item3]
		self.delegate?.fetchCarousel(items: carouselItems)
	}
	
	func rescheduleTimer() {
		self.timer.invalidate()
		self.timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { (_) in
			if self.counter >= self.carouselItems.count {
				self.counter = 0
			}
			self.counter += 1
			self.delegate?.fetchCurrentCarouselItem(counter: self.counter)
		}
	}
	
	func setCurrentOnboardingScreen(page: Int) {
		self.counter = page
		self.rescheduleTimer()
	}
}
