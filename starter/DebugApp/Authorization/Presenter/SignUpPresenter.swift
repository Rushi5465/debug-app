//
//  SignUpPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

class SignUpPresenter: SignUpPresenterProtocol {
	
	private var webservice: AuthorizationWebServiceProtocol
	private weak var delegate: SignUpViewDelegate?
	
	required init(webservice: AuthorizationWebServiceProtocol, delegate: SignUpViewDelegate) {
		self.webservice = webservice
		self.delegate = delegate
	}
	
	func registerUser(user: User) {
		webservice.registerUser(user: user) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.registerUserErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.registerUserSuccess(with: response, user: user)
			}
		}
	}
}
