//
//  SignInPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

class SignInPresenter: SignInPresenterProtocol {
	
	private var webservice: AuthorizationWebServiceProtocol
	private weak var delegate: SignInViewDelegate?
	
	required init(webservice: AuthorizationWebServiceProtocol, delegate: SignInViewDelegate) {
		self.webservice = webservice
		self.delegate = delegate
	}
	
	func loginUser(username: String, password: String) {
		self.webservice.loginUser(username: username, password: password) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.loginUserErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.loginUserSuccess(with: response)
			}
		}
	}
	
}
