//
//  PauseReading.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 09/09/21.
//

import Foundation

struct PauseReading {
	
	let startTime: TimeInterval
	let endTime: TimeInterval
}
