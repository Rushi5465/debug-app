//
//  PauseDataWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/07/21.
//

import Foundation

protocol PauseDataWebServiceProtocol {
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataPauseReading]?, MovanoError?) -> Void)
	
	func fetch(by id: String, completionHandler: @escaping (CoreDataPauseReading?, MovanoError?) -> Void)
	
	func add(model: PauseReading, completionHandler: @escaping (CoreDataPauseReading?, MovanoError?) -> Void)
}
