//
//  PauseDataWebService.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/07/21.
//

import Foundation

// swiftlint:disable all
class PauseDataWebService: PauseDataWebServiceProtocol {
	
	private var graphQL: ApolloClientService
	private var service: RestService
	
	init(graphQL: ApolloClientService = ApolloClientService(),service: RestService = RestService.shared) {
		self.graphQL = graphQL
		self.service = service
	}
	
	func fetch(by id: String, completionHandler: @escaping (CoreDataPauseReading?, MovanoError?) -> Void) {
		let query = GetPauseDataQuery(id: id)
		
		_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.getPauseData {
					CoreDataPauseReading.add(startTime: item.startTime, endTime: item.endTime, systolic: item.averageSystolic, diastolic: item.averageDiastolic, skinTemperature: item.averageSkinTemprature, pulseRate: item.averagePulseRate, breathingRate: item.averageBreathingRate)
					let reading = CoreDataPauseReading.fetch(timeStamp: item.endTime)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataPauseReading]?, MovanoError?) -> Void) {
        if(Utility.makeAPICall(timeIntervalRange: timestampRange) || !CoreDataPauseReading.isReadingAvailable(range: timestampRange)){
            
            let userIdInput = TableStringFilterInput(eq: KeyChain.shared.userId)
            let dateRangeInput = TableFloatFilterInput(between: [timestampRange.lowerBound, timestampRange.upperBound])
            
            let filter = TablePauseDataFilterInput(userId: userIdInput, endTime: dateRangeInput)
            let query = ListPauseDataQuery(filter: filter, limit: nil)
            
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
                switch result {
                case .success(let graphQLResult):
                    if let item = graphQLResult.data?.listPauseData?.items {
                        for each in item where each != nil {
							CoreDataPauseReading.add(startTime: each!.startTime, endTime: each!.endTime, systolic: each!.averageSystolic, diastolic: each!.averageDiastolic, skinTemperature: each!.averageSkinTemprature, pulseRate: each!.averagePulseRate, breathingRate: each!.averageBreathingRate)
                        }
                        let breathingRateData = CoreDataPauseReading.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                        completionHandler(breathingRateData, nil)
                    } else {
                        completionHandler(nil, .invalidResponseModel)
                    }
                case .failure(let error):
                    if error.localizedDescription == "Received error response: Unauthorized" ||
                        error.localizedDescription == "Received error response: Token has expired." {
						let pauseData = CoreDataPauseReading.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
						completionHandler(pauseData, nil)
                    } else {
                        completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                    }
                    Logger.shared.addLog(error.localizedDescription)
                }
            }
        }else{
            let breathingRateData = CoreDataPauseReading.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            completionHandler(breathingRateData, nil)
        }
	}
	
	func add(model: PauseReading, completionHandler: @escaping (CoreDataPauseReading?, MovanoError?) -> Void) {
		let query = CreatePauseDataMutation(CreatePauseDataInput: CreatePauseDataInput(startTime: model.startTime, endTime: model.endTime))
		
		_ = self.graphQL.client!.perform(mutation: query) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.createPauseData {
					CoreDataPauseReading.add(startTime: item.startTime, endTime: item.endTime, systolic: item.averageSystolic, diastolic: item.averageDiastolic, skinTemperature: item.averageSkinTemprature, pulseRate: item.averagePulseRate, breathingRate: item.averageBreathingRate)
					let reading = CoreDataPauseReading.fetch(timeStamp: item.endTime)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
