//
//  DebugMovanoRingPresenter.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 06/01/22.
//

import Foundation
import CoreBluetooth

class DebugMovanoRingPresenter : BluetoothService{
    
    private weak var delegate: DebugViewDelegate?

    init(delegate:DebugViewDelegate){
        self.delegate = delegate
    }
    override func didUpdate(characteristic: CBCharacteristic) {
        guard let characteristicData = characteristic.value else { return }
        let timeStamp = Date().timeIntervalSince1970
        let byteArray = [UInt8](characteristicData)
        var spo2 = Int(byteArray[5])
        var heartRate = UInt(byteArray[6])
        var breathingRate = UInt(byteArray[7])
        var steps =  (UInt(byteArray[9]) * 256) + UInt(byteArray[8])
        var skinTempInDegree = ((Double(UInt(byteArray[4]) * 256)) + Double(UInt(byteArray[3]))) / 10
        var distanceCovered = (UInt(byteArray[11]) * 256) + UInt(byteArray[10])
        var calories = (Int(byteArray[13]) * 256) + Int(byteArray[12])
        let battery = UInt(byteArray[2])
        let chargingPercentage = UInt(byteArray[14])
//        if chargingPercentage != 0 {
//            UserDefaults.standard.set(chargingPercentage, forKey: "batteryPercentage")
//        } else {
//            if battery != 0 {
//                UserDefaults.standard.set(battery, forKey: "batteryPercentage")
//            }
//        }
        
        if battery != 0 {
            UserDefaults.standard.set(battery, forKey: "batteryPercentage")
        }
        UserDefaults.standard.set(chargingPercentage, forKey: "isBatteryCharging")
        
        Logger.shared.addLog("Session byteArray : \(byteArray)")
        Logger.shared.addLog("Session Breathing Rate : \(breathingRate)")
        Logger.shared.addLog("Session Heart Rate : \(heartRate)")
        Logger.shared.addLog("Session SpO2 : \(spo2)")
        Logger.shared.addLog("Session Steps : \(steps)")
        
        if spo2 < 90 {
            spo2 = 0
            heartRate = 0
            breathingRate = 0
            
//            skinTempInDegree = 0
//            distanceCovered = 0
//            calories = 0
//            steps = 0
        }
        var ledArray:[Int] = []
        for index in stride(from: 28, to: byteArray.count - 1, by: 2){
            let value = (UInt16(byteArray[index + 1]) * 256) + UInt16(byteArray[index])
            let value2 = Int(value)
            let doubleValue = value2 > 0x7fff ?  Int(value2 - 0x10000) : Int(value2)
             ledArray.append(doubleValue)
        }
       
        self.delegate?.setLedData(ledArray: ledArray)
        let skinTemp =  (skinTempInDegree * 9/5) + 32.0
        Logger.shared.addLog("Session Temp : \(skinTemp)")
        
        self.delegate?.fetchExerciseParameter(stepCount: Int(steps))
        self.delegate?.fetchExerciseParameter(heartRate: Int(heartRate))
        self.delegate?.fetchExerciseParameter(spo2: Int(spo2))
        self.delegate?.fetchExerciseParameter(skinTemp: Float(skinTempInDegree))


    }
    
}
