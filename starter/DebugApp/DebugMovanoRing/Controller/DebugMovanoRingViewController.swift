//
//  DebugMovanoRingViewController.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 05/01/22.
//

import UIKit
import Highcharts

// swiftlint:disable all
class DebugMovanoRingViewController: UIViewController {

    @IBOutlet weak var chartView: HIChartView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    var plotData:[Int] = []
	let dataDirectoryPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)  // The top level directory.
	
    var parameterList = [Parameter]() {
        didSet {
            self.collectionView.reloadData()
        }
    }
    var presenter: DebugMovanoRingPresenter?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCollectionCell()
        self.assignDelegates()
        self.loadData()
        self.registerPresenters()
        self.collectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        bluetoothManager.updateNotificationStateToTrue(uuid: TransferService.dateServiceUUID)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        self.collectionViewHeight.constant = self.collectionView.contentSize.height
    }
    
    @IBAction func stopBtnClicked(_ sender: Any) {
		// Write data in file
		// Create session directory.
		let pathString = self.dataDirectoryPath[0].path + "/\(KeyChain.shared.firstName)"
		let sessionDirName = timeStamp()
		let sessionDirectory = "\(pathString)" + "/\(sessionDirName)"
//		do {
//			try FileManager.default.createDirectory(atPath: sessionDirectory, withIntermediateDirectories: true, attributes: nil)
//		} catch {
//			print(error.localizedDescription)
//		}
//		
//		print("Rushi directory is - \(sessionDirectory)")
		
		let storeData = Logger.shared.printLogs().data(using: .utf8)
		let fileURL = "\(sessionDirectory)" + "data.txt"
		FileManager.default.createFile(atPath: fileURL, contents: storeData, attributes: [FileAttributeKey.creationDate: Date()])
		
        bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
        self.navigationController?.popToRootViewController(animated: true)

    }
    @IBAction func backBtnClicked(_ sender: Any) {
        bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        AppUtility.lockOrientation([.landscapeLeft])
//
//        let value = UIInterfaceOrientation.landscapeLeft.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
//        UINavigationController.attemptRotationToDeviceOrientation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        AppUtility.lockOrientation(.portrait)
//
//        let value = UIInterfaceOrientation.portrait.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
//        UINavigationController.attemptRotationToDeviceOrientation()
        
    }
    
    func registerCollectionCell() {
        let parameterCell = UINib.init(nibName: "ParameterCollectionViewCell", bundle: nil)
        self.collectionView.register(parameterCell, forCellWithReuseIdentifier: ParameterCollectionViewCell.IDENTIFIER)
    }

    func assignDelegates() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func loadData() {
        let step = Parameter(name: "STEPS", reading: nil, unit: nil, iconName: "sleep_StepsIcon")
        let heartRate = Parameter(name: "HEART RATE", reading: nil, unit: "bpm", iconName: "sleep-HeartIcon")
        let oxygen = Parameter(name: "OXYGEN", reading: nil, unit: "%", iconName: "sleep-OxygenIcon")
        let temp = Parameter(name: "SKIN TEMP VAR", reading: nil, unit: "°f", iconName: "sleep_SkintempIcon")
        
        self.parameterList.append(contentsOf: [heartRate, oxygen, step, temp])
    }
    
    func registerPresenters() {
        if self.presenter == nil {
            self.presenter = DebugMovanoRingPresenter(delegate: self)
            BluetoothManager.shared.setBluetoothService(serivce: presenter)
        }
    }
    
    func setupGraphView(data:[[String:Int]]){
        //let chartView = HIChartView(frame: graphView.bounds)
        let plotOptions = HIPlotOptions()

        let chartSeries = HISeries()
        plotOptions.series = chartSeries

        let chartPoint = HIPoint()
        plotOptions.series.point = chartPoint

        let chartEvents = HIEvents()
        plotOptions.series.point.events = chartEvents

        let chartFunction = HIFunction(closure: { context in
            guard let context = context else { return }

            let alertMessage = "Category: \(context.getProperty("this.category") ?? "unknown"), value: \(context.getProperty("this.y") ?? "unknown")"
            
            
        }, properties: ["this.category", "this.y"])

        plotOptions.series.point.events.load = chartFunction
        
        
            let options = HIOptions()
            let chart = HIChart()
            chart.type = "spline"
            chart.marginRight = 10
			chart.backgroundColor = HIColor(uiColor: .clear)
            chart.events = HIEvents()
            chart.events.load = HIFunction(jsFunction: "function () { var series = this.series[0]; setInterval(function () { var x = (new Date()).getTime(), y = Math.random(); series.addPoint([x, y], true, true); }, 1000); }")
            options.chart = chart

            let time = HITime()
            time.useUTC = true
            options.time = time

            let title = HITitle()
            title.text = ""
            options.title = title

            let accessibility = HIAccessibility()
            accessibility.announceNewData = HIAnnounceNewData()
            accessibility.announceNewData.enabled = true
            accessibility.announceNewData.minAnnounceInterval = 1000
            accessibility.announceNewData.announcementFormatter = HIFunction(jsFunction: "function (allSeries, newSeries, newPoint) { if (newPoint) { return 'New point added. Value: ' + newPoint.y; } return false; }")
            options.accessibility = accessibility

            let xAxis = HIXAxis()
           // xAxis.type = "datetime"
            xAxis.tickPixelInterval = 150
			xAxis.labels = HILabels()
			xAxis.labels.style = HICSSObject()
			xAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			xAxis.labels.style.fontSize = "12"
			xAxis.labels.style.color = "#ffffff"
		
            options.xAxis = [xAxis]

            let yAxis = HIYAxis()
            yAxis.title = HITitle()
            yAxis.title.text = ""
            let plotLines = HIPlotLines()
            plotLines.value = 0
            plotLines.width = 1
            plotLines.color = HIColor(hexValue: "808080")
            yAxis.plotLines = [plotLines]
			yAxis.labels = HILabels()
			yAxis.labels.style = HICSSObject()
			yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
			yAxis.labels.style.fontSize = "12"
			yAxis.labels.style.color = "#ffffff"
            options.yAxis = [yAxis]

            let tooltip = HITooltip()
            tooltip.headerFormat = "<b>{series.name}</b><br/>"
            tooltip.pointFormat = "{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}"
            options.tooltip = tooltip

            let legend = HILegend()
            legend.enabled = false
            options.legend = legend

            let exporting = HIExporting()
            exporting.enabled = false
            options.exporting = exporting
		
			let credits = HICredits()
			credits.enabled = NSNumber(value: false)
			options.credits = credits

            let spline = HISpline()
            spline.name = "Live data"
            spline.data = data
//            [
//              [ "x": 1, "y": 4000 ],
//              [ "x": 2, "y": 5000 ],
//              [ "x": 3, "y": 6000 ],
//              [ "x": 4, "y": 7000 ],
//              [ "x": 5, "y": 8000 ],
//              [ "x": 6, "y": 4000 ],
//              [ "x": 7, "y": 5000 ],
//              [ "x": 8, "y": 6000 ],
//              [ "x": 9, "y": 7000 ],
//              [ "x": 10, "y": 8000 ],
//              [ "x": 11, "y": -4000 ],
//              [ "x": 12, "y": -6000 ],
//              [ "x": 13, "y": -3000 ],
//              [ "x": 14, "y": -3000 ],
//              [ "x": 15, "y": -2000 ],
//              [ "x": 16, "y": -4000 ],
//              [ "x": 17, "y": -8000 ],
//              [ "x": 18, "y": -9000 ],
//              [ "x": 19, "y": -4000 ],
//              [ "x": 20, "y": -5000 ]
//            ]
            options.series = [spline]
             chartView.options = options
    }
	
	/**
	 Create a date and time string of the form "yyyy-MM-dd'T'HHmmss".
	 */
	func timeStamp() -> String {
		let date = Date()
		let format = DateFormatter()
		format.dateFormat = "yyyy-MM-dd'T'HHmmss"
		let timestamp = format.string(from: date)
		return "\(timestamp)"
	}
}

extension DebugMovanoRingViewController : DebugViewDelegate{
    
    func setLedData(ledArray: [Int]) {
        //self.plotData.append(contentsOf: ledArray)
        var data:[[String:Int]] = [[:]]
        for index in 0..<ledArray.count{
            data.append(["x":index + 1, "y": ledArray[index]])
        }
        self.setupGraphView(data: data)

    }
    
    func fetchExerciseParameter(stepCount: Int) {
        self.parameterList[2].reading = "\(stepCount)"
    }
    
    func fetchExerciseParameter(heartRate: Int) {
        self.parameterList[0].reading = "\(heartRate)"
    }
    
    func fetchExerciseParameter(spo2: Int) {
        self.parameterList[1].reading = "\(spo2)"
    }
    
    func fetchExerciseParameter(skinTemp: Float) {
        let tempSkin = ((skinTemp * 10).rounded() / 10)
        self.parameterList[3].reading = "\(tempSkin)"
    }
}

extension DebugMovanoRingViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width / 2.0) - 9, height: 75)
    }
}

extension DebugMovanoRingViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.parameterList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = ParameterCollectionViewCell.dequeueReusableCell(collectionView: collectionView, indexPath: indexPath)
        
        let parameter = self.parameterList[indexPath.row]
        if parameter.name == "CHARGING POWER" && (parameter.reading?.toDouble == 0 || parameter.reading?.toDouble == nil) {
            cell.parameterImage.image = UIImage(named: "")
            cell.parameterLabel.text = ""
            cell.readingLabel.text = ""
        } else {
            if let reading = parameter.reading {
                if parameter.name == "SKIN TEMP VAR" {
                    let value = abs(Double(reading) ?? 0)
                    if value == 32.0 {
                        cell.readingLabel.text = "-"
                    } else {
                        cell.readingLabel.attributedText = cell.readingData(value: "\(value)", unit: parameter.unit)
                    }
                } else {
                    if (reading.toDouble != 0 && reading.toDouble != nil) {
                        cell.readingLabel.attributedText = cell.readingData(value: reading, unit: parameter.unit)
                    } else {
                        cell.readingLabel.text = "-"
                    }
                }
            } else {
                cell.readingLabel.text = "-"
            }
            cell.parameterImage.image = UIImage(named: parameter.iconName ?? "")
            cell.parameterLabel.text = (parameter.name == "SKIN TEMP VAR") ? "SKIN TEMP" : parameter.name
        }
        
        return cell
    }
}




