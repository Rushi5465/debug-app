//
//  DebugViewDelegate.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 06/01/22.
//

import Foundation

protocol DebugViewDelegate : AnyObject{
    func fetchExerciseParameter(stepCount: Int)
    func fetchExerciseParameter(heartRate: Int)
    func fetchExerciseParameter(spo2: Int)
    func fetchExerciseParameter(skinTemp: Float)
    func setLedData(ledArray:[Int])
}
