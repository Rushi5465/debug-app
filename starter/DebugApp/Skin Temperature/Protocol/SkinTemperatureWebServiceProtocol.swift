//
//  SkinTemperatureWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 19/07/21.
//

import Foundation

protocol SkinTemperatureWebServiceProtocol {
    
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataSkinTemperature]?, MovanoError?) -> Void)
	
	func fetch(by timestamp: Double, completionHandler: @escaping (CoreDataSkinTemperature?, MovanoError?) -> Void)
	
	func fetchAll(completionHandler: @escaping ([CoreDataSkinTemperature]?, MovanoError?) -> Void)
	
	func add(model: SkinTemperature, completionHandler: @escaping (CoreDataSkinTemperature?, MovanoError?) -> Void)
	
	func add(models: [SkinTemperature], completionHandler: @escaping ([CoreDataSkinTemperature]?, MovanoError?) -> Void)
	
	func update(model: SkinTemperature, completionHandler: @escaping (CoreDataSkinTemperature?, MovanoError?) -> Void)
	
	func delete(model: SkinTemperature, completionHandler: @escaping (Bool?, MovanoError?) -> Void)
	
//	func fetchAverageData(forDate:String,completionHandler: @escaping (SleepAverageDataModel?, MovanoError?) -> Void)
}
