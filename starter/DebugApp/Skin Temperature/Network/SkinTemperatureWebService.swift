//
//  SkinTemperatureWebService.swift
//  BloodPressureApp
//
//  Created by Rushikant on 19/07/21.
//

import Foundation

class SkinTemperatureWebService: SkinTemperatureWebServiceProtocol {
	
	private var graphQL: ApolloClientService
	private var service: RestService
	public static var refreshTokenCount = 0
	
	init(graphQL: ApolloClientService = ApolloClientService(),service: RestService = RestService.shared) {
		self.graphQL = graphQL
		self.service = service
	}
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataSkinTemperature]?, MovanoError?) -> Void) {
		if(Utility.makeAPICall(timeIntervalRange: timestampRange) || !CoreDataSkinTemperature.isReadingAvailable(range: timestampRange)){
			
			let query = QuerySkinTemperatureByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
			
			_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
				switch result {
				case .success(let graphQLResult):
					if let items = graphQLResult.data?.querySkinTemperatureByTimestampRange?.items {
						for each in items where each != nil {
							CoreDataSkinTemperature.add(timeInterval: each!.temperatureTimestamp, value: each!.temperatureValue)
						}
						let skinTempData = CoreDataSkinTemperature.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
						completionHandler(skinTempData, nil)
					} else {
						completionHandler(nil, nil)
					}
				case .failure(let error):
					if error.localizedDescription == "Received error response: Unauthorized" ||
						error.localizedDescription == "Received error response: Token has expired." {
						
					} else {
						
					}
					let skinTempData = CoreDataSkinTemperature.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
					completionHandler(skinTempData, nil)
//					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
					Logger.shared.addLog(error.localizedDescription)
				}
			}
		}else{
			let skinTempData = CoreDataSkinTemperature.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
			completionHandler(skinTempData, nil)
		}
	}
	
	func fetch(by timestamp: Double, completionHandler: @escaping (CoreDataSkinTemperature?, MovanoError?) -> Void) {
		let query = GetSkinTemperatureQuery(temperature_timestamp: timestamp)
		
		_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let items = graphQLResult.data?.getSkinTemperature {
					CoreDataSkinTemperature.add(timeInterval: items.temperatureTimestamp, value: items.temperatureValue)
					let reading = CoreDataSkinTemperature.fetch(timeStamp: items.temperatureTimestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func fetchAll(completionHandler: @escaping ([CoreDataSkinTemperature]?, MovanoError?) -> Void) {
		let query = ListSkinTemperaturesQuery()
		
		_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let items = graphQLResult.data?.listSkinTemperatures?.items {
					for each in items where each != nil {
						CoreDataSkinTemperature.add(timeInterval: each!.temperatureTimestamp, value: each!.temperatureValue)
					}
					let readings = CoreDataSkinTemperature.fetch()
					completionHandler(readings, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func delete(model: SkinTemperature, completionHandler: @escaping (Bool?, MovanoError?) -> Void) {
		let query = DeleteSkinTemperatureMutation(DeleteSkinTemperatureInput: DeleteSkinTemperatureInput(temperatureTimestamp: model.timestamp))
		
		_ = self.graphQL.client!.perform(mutation: query) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.deleteSkinTemperature {
					CoreDataSkinTemperature.delete(for: Date(timeIntervalSince1970: item.temperatureTimestamp))
					completionHandler(true, nil)
				} else {
					completionHandler(false, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func add(model: SkinTemperature, completionHandler: @escaping (CoreDataSkinTemperature?, MovanoError?) -> Void) {
		let query = CreateSkinTemperatureMutation(CreateSkinTemperatureInput: CreateSkinTemperatureInput(temperatureTimestamp: model.timestamp, temperatureValue: model.value))
		
		_ = self.graphQL.client!.perform(mutation: query) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.createSkinTemperature {
					CoreDataSkinTemperature.add(timeInterval: item.temperatureTimestamp, value: item.temperatureValue)
					let reading = CoreDataSkinTemperature.fetch(timeStamp: item.temperatureTimestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func update(model: SkinTemperature, completionHandler: @escaping (CoreDataSkinTemperature?, MovanoError?) -> Void) {
		let query = UpdateSkinTemperatureMutation(UpdateSkinTemperatureInput: UpdateSkinTemperatureInput(temperatureTimestamp: model.timestamp, temperatureValue: model.value))
		
		_ = self.graphQL.client!.perform(mutation: query) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.updateSkinTemperature {
					CoreDataSkinTemperature.add(timeInterval: item.temperatureTimestamp, value: item.temperatureValue)
					let reading = CoreDataSkinTemperature.fetch(timeStamp: item.temperatureTimestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func add(models: [SkinTemperature], completionHandler: @escaping ([CoreDataSkinTemperature]?, MovanoError?) -> Void) {
		
		var data = [CreateSkinTemperatureInput]()
		for each in models {
			data.append(CreateSkinTemperatureInput(temperatureTimestamp: each.timestamp, temperatureValue: each.value))
		}
		let mutation = CreateMultipleSkinTemperatureMutation(CreateSkinTemperatureInput: data)
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let items = graphQLResult.data?.createMultipleSkinTemperature {
					for each in items where each != nil {
						CoreDataSkinTemperature.add(timeInterval: each!.temperatureTimestamp, value: each!.temperatureValue)
					}
                    if let firstItem = items.first, let timestamp = firstItem?.temperatureTimestamp{
                        let reading = CoreDataSkinTemperature.fetch(for: DateRange(Date(timeIntervalSince1970: timestamp), type: .daily).dateRange)
                        completionHandler(reading, nil)
                    }else{
                        completionHandler(nil, nil)
                    }
					
				} else {
					Logger.shared.addLog("Current time is -\(Date())")
					Logger.shared.addLog( "Data is not in [CreateMultipleSkinTemperature] format")
					Logger.shared.addLog( "Graphql result for skin temp -\(graphQLResult.data?.createMultipleSkinTemperature)")
					completionHandler(nil, nil)
					
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					if SkinTemperatureWebService.refreshTokenCount == 0 {
						SkinTemperatureWebService.refreshTokenCount = 1
						self.requestAccessToken { [weak self] response, error in
							if (response != nil) {
								self?.graphQL.setDefaultClient()
								self?.add(models: models, completionHandler: completionHandler)
							}
							if let error = error {
								completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
							}
						}
					}
				} else {
					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				}
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
