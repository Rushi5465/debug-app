//
//  SkinTemperature.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 09/09/21.
//

import Foundation


public class SkinTemperature:NSObject, NSCoding {
    public var timestamp: TimeInterval = 0
    public var value: Double = 0.0
    
    enum Key:String{
        case timestamp = "timestamp"
        case value = "value"
    }
    
    public override init() {
        super.init()
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(timestamp,forKey: Key.timestamp.rawValue)
        aCoder.encode(value,forKey: Key.value.rawValue)

    }
  
    required public convenience init(coder aDecoder: NSCoder)  {
        let mTimestamp = aDecoder.decodeDouble(forKey: Key.timestamp.rawValue)
        let mValue = aDecoder.decodeDouble(forKey: Key.value.rawValue)
        self.init(timeStamp:mTimestamp, value:mValue)
    }
    
    init(timeStamp:TimeInterval, value:Double) {
        self.timestamp = timeStamp
        self.value = value
    }
}

public class SkinTemperatureCoreData:NSObject, NSCoding{
    public var skinTemperature : [SkinTemperature] = []
    
    enum Key:String{
        case skinTemperature = "skinTemperature"
    }
    
    init(skinTemperature:[SkinTemperature]){
        self.skinTemperature = skinTemperature
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(skinTemperature,forKey: Key.skinTemperature.rawValue)
    }
    
    required public convenience init(coder aDecoder: NSCoder)  {
        let mSkinTemperature = aDecoder.decodeObject(forKey: Key.skinTemperature.rawValue) as! [SkinTemperature]
        self.init(skinTemperature:mSkinTemperature)
    }
}
