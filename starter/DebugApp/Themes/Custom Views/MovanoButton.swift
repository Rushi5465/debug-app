//
//  MovanoButton.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 10/01/22.
//

import UIKit

@objc class MovanoButton: UIButton {
    
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var isCircular: Bool = false {
        didSet {
            if(isCircular) {
                self.cornerRadius = 0.0
                let _cornerRadius: CGFloat = frame.size.height / 2.0
                layer.cornerRadius = _cornerRadius
            }
        }
    }
    @IBInspectable var roundAllCorners: Bool = false {
        didSet {
            if !isCircular && roundAllCorners {
                layer.cornerRadius = cornerRadius
            }
        }
    }
    @IBInspectable var buttonBackgroundColor: String = "" {
        didSet {
            backgroundColor = MovanoThemeManager.sharedThemeObject.getColor(fromString: buttonBackgroundColor)
        }
    }
    
    @IBInspectable var buttonTextColor: String = "" {
        didSet {
            setTitleColor(MovanoThemeManager.sharedThemeObject.getColor(fromString: buttonTextColor), for: .normal)
        }
    }
    
    @IBInspectable var buttonTintColor: String = "" {
        didSet {
            tintColor = MovanoThemeManager.sharedThemeObject.getColor(fromString: buttonTintColor)
        }
    }
    
    

     
    var _fontWeight: MovanoFontWeight = .regular
    @IBInspectable var fontSize: Int = 14
    @available(*, unavailable, message: "This property is only reserved for Interface Builder only. Use _fontWeight for direct access instead.")
    @IBInspectable var fontWeight: Int = 0 {
        willSet {
            if let _fw = MovanoFontWeight(rawValue: newValue) {
                _fontWeight = _fw
            } else {
                _fontWeight = .regular
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // setTitle(titleLabel?.text, for: .normal)
        titleLabel?.font = UIFont(name: Utility.getCustomMovanoFontName(fontWeight: _fontWeight), size: CGFloat(fontSize))
        
    }
   
}
