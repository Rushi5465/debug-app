//
//  MovanoView.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 10/01/22.
//

import UIKit

class MovanoView: UIView {
    var circularRadiusValue: CGFloat = 5.0
    
    // MARK: Rounding related properties
    @IBInspectable var isCircular: Bool = false {
        didSet {
            if(isCircular) {
                self.cornerRadius = 0.0
                let _cornerRadius: CGFloat = frame.size.height / 2.0
                layer.cornerRadius = _cornerRadius
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 5.0 {
        didSet {
            circularRadiusValue = cornerRadius
        }
    }
    
    @IBInspectable var roundAllCorners: Bool = false {
        didSet {
            if !isCircular && roundAllCorners {
                layer.cornerRadius = cornerRadius
            }
        }
    }
    
    @IBInspectable var roundTopCorners: Bool = false {
        didSet {
            if !isCircular && roundTopCorners {
                roundCorners(corners: [.topLeft, .topRight], radius: cornerRadius)
            }
        }
    }
    
    @IBInspectable var roundBottomCorners: Bool = false {
        didSet {
            if !isCircular && roundBottomCorners {
                roundCorners(corners: [.bottomLeft, .bottomRight], radius: cornerRadius)
            }
        }
    }
    
    @IBInspectable var roundRightCorners: Bool = false {
        didSet {
            if !isCircular && roundRightCorners {
                roundCorners(corners: [.bottomRight, .topRight], mask: [.layerMaxXMinYCorner, .layerMaxXMaxYCorner], cornerRadius: cornerRadius)
            }
        }
    }
    
    @IBInspectable var roundLeftCorners: Bool = false {
        didSet {
            if !isCircular && roundLeftCorners {
                roundCorners(corners: [.topLeft, .bottomLeft], mask: [.layerMinXMaxYCorner, .layerMinXMinYCorner], cornerRadius: cornerRadius)
            }
        }
    }
    

    // MARK: Other properties
    @IBInspectable var backgroundClr: String = "" {
        didSet {
            if !backgroundClr.isEmpty {
                backgroundColor = MovanoThemeManager.sharedThemeObject.getColor(fromString: backgroundClr)
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if !isCircular && roundAllCorners {
            roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: cornerRadius)
        }
        
        if !isCircular && roundTopCorners {
            roundCorners(corners: [.topLeft, .topRight], radius: cornerRadius)
        }
        
        if !isCircular && roundBottomCorners {
            roundCorners(corners: [.bottomLeft, .bottomRight], radius: cornerRadius)
        }
        
        if !isCircular && roundRightCorners {
            roundCorners(corners: [.bottomRight, .topRight], mask: [.layerMaxXMinYCorner, .layerMaxXMaxYCorner], cornerRadius: cornerRadius)
        }
        
        if !isCircular && roundLeftCorners {
            roundCorners(corners: [.topLeft, .bottomLeft], mask: [.layerMinXMaxYCorner, .layerMinXMinYCorner], cornerRadius: cornerRadius)
        }
        
        
    }
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath.init(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize.init(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.frame = bounds
        mask.path = path.cgPath
        layer.mask = mask
    }
    func roundCorners(corners: UIRectCorner, mask: CACornerMask, cornerRadius: CGFloat) {
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = CGFloat(cornerRadius)
            self.clipsToBounds = true
            self.layer.maskedCorners = mask
        } else {
            let path = UIBezierPath.init(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize.init(width: cornerRadius, height: cornerRadius))
            let mask = CAShapeLayer()
            mask.frame = bounds
            mask.path = path.cgPath
            layer.mask = mask
        }
    }
    
    func dropShadow(scale: Bool = true) {
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        layer.shadowOpacity = 0.85
        layer.shadowRadius = 4.0
        layer.masksToBounds = false
    }
}
