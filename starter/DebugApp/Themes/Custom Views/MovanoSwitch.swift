//
//  MovanoSwitch.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 10/01/22.
//

import UIKit

class MovanoSwitch: UISwitch {

    @IBInspectable var switchTintColor: String = "" {
        didSet {
            onTintColor = MovanoThemeManager.sharedThemeObject.getColor(fromString: switchTintColor)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
