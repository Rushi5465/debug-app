//
//  MovanoAppBar.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 10/01/22.
//

import UIKit

class MovanoAppBar: UIView {
    
    @IBInspectable var backgroundClr: String = "" {
        didSet {
            backgroundColor = MovanoThemeManager.sharedThemeObject.getColor(fromString: backgroundClr)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
