//
//  MovanoImageView.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 10/01/22.
//

import UIKit

class MovanoImageView: UIImageView {

    @IBInspectable var imageTintColor: String = "" {
        didSet {
            tintColor = MovanoThemeManager.sharedThemeObject.getColor(fromString: imageTintColor)
        }
    }
    @IBInspectable var isCurcular: Bool = false {
        didSet {
            if isCurcular {
                makeCircular()
            }
        }
    }
    
}
extension MovanoImageView {
    func makeCircular() {
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.size.height / 2
        self.layer.masksToBounds = true
    }
}


