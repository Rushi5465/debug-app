//
//  MovanoButton.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 10/01/22.
//

import UIKit
// swiftlint:disable line_length
class MovanoTextView: UITextView {
    @IBInspectable var bkgColor: String = "" {
        didSet {
            backgroundColor = MovanoThemeManager.sharedThemeObject.getColor(fromString: bkgColor)
        }
    }
    var placeHolderLabel: MovanoLabel = MovanoLabel()
    @IBInspectable var placeHolderText: String = "" {
        didSet {
            placeHolderLabel.font = font
            placeHolderLabel.text = placeHolderText
            placeHolderLabel.textColor = MovanoThemeManager.sharedThemeObject.getColor(fromString: "GreyLevel2")
            placeHolderLabel.isHidden = !(!placeHolderText.isEmpty && text.isEmpty)
            placeHolderLabel.sizeToFit()
            
            placeHolderLabel.frame = CGRect(x: placeHolderLabel.frame.origin.x + 4, y: placeHolderLabel.frame.origin.y + 7, width: placeHolderLabel.frame.size.width, height: placeHolderLabel.frame.size.height)
            addSubview(placeHolderLabel)
        }
    }
    var _fontWeight: MovanoFontWeight = .regular
    @IBInspectable var fontSize: Int = 14
    @available(*, unavailable, message: "This property is only reserved for Interface Builder only. Use _fontWeight for direct access instead.")
    @IBInspectable var fontWeight: Int = 0 {
        willSet {
            if let _fw = MovanoFontWeight(rawValue: newValue) {
                _fontWeight = _fw
            } else {
                _fontWeight = .regular
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        font = UIFont(name: Utility.getCustomMovanoFontName(fontWeight: _fontWeight), size: CGFloat(fontSize))
    }
    func displayPlaceHolder() {
        placeHolderLabel.isHidden = false
    }
    func hidePlaceHolder() {
        placeHolderLabel.isHidden = true
    }
}
