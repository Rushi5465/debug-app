//
//  MovanoLabel.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 10/01/22.
//

import UIKit

class MovanoLabel: UILabel {
    var isCapitalized = false
    @IBInspectable var txtColor: String = "" {
        didSet {
            if let colorCode = MovanoThemeManager.sharedThemeObject.colorDictionary[txtColor]{
                textColor = UIColor.init(hexString: colorCode)
            }
        }
    }
    
    @IBInspectable var labelBackgroundColor: String = "" {
        didSet {
            backgroundColor = MovanoThemeManager.sharedThemeObject.getColor(fromString: labelBackgroundColor)
        }
    }
    
    @IBInspectable var isLabelCapitalized: Bool = false {
        didSet {
            isCapitalized = isLabelCapitalized
        }
    }
    
    var _fontWeight: MovanoFontWeight = .regular
    @IBInspectable var fontSize: Int = 14
    @available(*, unavailable, message: "This property is only reserved for Interface Builder only. Use _fontWeight for direct access instead.")
    @IBInspectable var fontWeight: Int = 0 {
        willSet {
            if let _fw = MovanoFontWeight(rawValue: newValue) {
                _fontWeight = _fw
            } else {
                _fontWeight = .regular
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isCapitalized {
            text = text?.uppercased()
        }
        font = UIFont(name: Utility.getCustomMovanoFontName(fontWeight: _fontWeight), size: CGFloat(fontSize))
    }
    
}
