//
//  MovanoBackgroundView.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 10/01/22.
//

import UIKit

class MovanoBackgroundView: UIView {
    let lineGap: CGFloat = 8
    let lineWidth: CGFloat = 8
    let lineColor = MovanoThemeManager.sharedThemeObject.getColor(fromString: "GreyLevel11")
    
    @IBInspectable var drawStripeView: Bool = false {
        didSet {
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func draw(_ rect: CGRect) {
        if drawStripeView {
            let ctx = UIGraphicsGetCurrentContext()!
            
            ctx.scaleBy(x: 1, y: -1)
            ctx.translateBy(x: 0, y: -bounds.size.height)
            let renderRect = bounds.insetBy(dx: -lineWidth * 0.5, dy: -lineWidth * 0.5)
            
            let totalDistance = renderRect.size.width + renderRect.size.height
            for distance in stride(from: 0, through: totalDistance,
                                   by: (lineGap + lineWidth) / cos(.pi / 4)) {
                                    ctx.move(to: CGPoint(
                                        x: distance < renderRect.width ?
                                            renderRect.origin.x + distance :
                                            renderRect.origin.x + renderRect.width,
                                        y: distance < renderRect.width ?
                                            renderRect.origin.y :
                                            distance - (renderRect.width - renderRect.origin.x)
                                    ))
                                    
                                    ctx.addLine(to: CGPoint(
                                        x: distance < renderRect.height ?
                                            renderRect.origin.x :
                                            distance - (renderRect.height - renderRect.origin.y),
                                        y: distance < renderRect.height ?
                                            renderRect.origin.y + distance :
                                            renderRect.origin.y + renderRect.height
                                    ))
            }
            ctx.setStrokeColor(lineColor.cgColor)
            ctx.setLineWidth(lineWidth)
            ctx.strokePath()
        }
    }
}
