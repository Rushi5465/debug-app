//
//  MovanoTextField.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 10/01/22.
//

import UIKit

class MovanoTextField: UITextField {
    
    @IBInspectable var txtColor: String = "" {
        didSet {
            textColor = MovanoThemeManager.sharedThemeObject.getColor(fromString: txtColor)
        }
    }
    @IBInspectable var backgroundClr: String = "" {
        didSet {
            backgroundColor = MovanoThemeManager.sharedThemeObject.getColor(fromString: backgroundClr)
        }
    }
    
    
    @IBInspectable var placeholderColor: String = "" {
        didSet {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [.foregroundColor: MovanoThemeManager.sharedThemeObject.getColor(fromString: placeholderColor)])
        }
    }
    
    @IBInspectable var secondaryTag: String = ""
    
    var _fontWeight: MovanoFontWeight = .regular
    @IBInspectable var fontSize: Int = 14
    @available(*, unavailable, message: "This property is only reserved for Interface Builder only. Use _fontWeight for direct access instead.")
    @IBInspectable var fontWeight: Int = 0 {
        willSet {
            if let _fw = MovanoFontWeight(rawValue: newValue) {
                _fontWeight = _fw
            } else {
                _fontWeight = .regular
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        font = UIFont(name: Utility.getCustomMovanoFontName(fontWeight: _fontWeight), size: CGFloat(fontSize))
    }
}

