//
//  MovanoThemeManager.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 10/01/22.
//

import UIKit

@objc public enum MovanoFontWeight: Int, RawRepresentable {
    case ultralight = 0
    case thin = 1
    case light = 2
    case regular = 3
    case medium = 4
    case semibold = 5
    case bold = 6
    case heavy = 7
    case black = 8
    case italic = 9
    
    func toUIKitFontWeight() -> UIFont.Weight {
        switch self {
        case .ultralight:
            return .ultraLight
        case .thin:
            return .thin
        case .light:
            return .light
        case .regular:
            return .regular
        case .medium:
            return .medium
        case .semibold:
            return .semibold
        case .bold:
            return .bold
        case .heavy:
            return .heavy
        case .black:
            return .black
        case .italic:
            return .regular
        default:
            return .regular
        }
    }
}
class MovanoThemeManager: NSObject {
    
    // Default Theme
    public var commonColors: String = "Common Colors"
    //demo purpose
    public var currentTheme: String = "Default"
    public var themeVariant: String = "DarkTheme"
    
    private static var singleton = MovanoThemeManager()
    static var sharedThemeObject: MovanoThemeManager {
        return singleton
    }
    
    private var confPlistDictionary: Dictionary<String, Any>?
    private var colorCodeDictionary: Dictionary<String, Any>?
    public var colorDictionary: Dictionary<String, String> = Dictionary<String, String>.init()
    
    override init() {
        super.init()
        
        if let _selectedTheme = UserDefaults.standard.value(forKey: "selectedTheme") as? String {
            currentTheme = _selectedTheme
        }
        
        self.confPlistDictionary = Dictionary<String, Any>.init()
        self.colorCodeDictionary = Dictionary<String, Any>.init()
        
        self.readPlistData()
        self.setUpData()
    }
    
    func readPlistData() {
        var propertyListForamt = PropertyListSerialization.PropertyListFormat.xml
        
        let confPlistPath: String? = Bundle.main.path(forResource: "Configuration", ofType: "plist")!
        let confPlistXmlContents = FileManager.default.contents(atPath: confPlistPath!)!
        
        let colorPlistPath: String? = Bundle.main.path(forResource: "Color", ofType: "plist")!
        let colorPlistXmlContents = FileManager.default.contents(atPath: colorPlistPath!)!
        
        do {
            confPlistDictionary = try PropertyListSerialization.propertyList(from: confPlistXmlContents, options: .mutableContainersAndLeaves, format: &propertyListForamt) as? Dictionary<String, Any>
            
            colorCodeDictionary = try PropertyListSerialization.propertyList(from: colorPlistXmlContents, options: .mutableContainersAndLeaves, format: &propertyListForamt) as? Dictionary<String, Any>
        } catch {
            print("Error reading plist: \(error), format: \(propertyListForamt)")
        }
    }
    
    func setUpData() {
        var colorTheme: Dictionary<String, String> = Dictionary<String, String>.init()
        var defaultColorTheme: Dictionary<String, String> = Dictionary<String, String>.init()
        
        if let colorDict = colorCodeDictionary!["Color"] as? Dictionary<String, Any> {
            if let theme = colorDict[currentTheme] as? Dictionary<String, String> {
                colorTheme = theme
            }
            
            if let commonColors = colorDict[commonColors] as? Dictionary<String, String> {
                defaultColorTheme = commonColors
                for obj in defaultColorTheme.keys {
                    if let colorCode = defaultColorTheme[obj] {
                        if colorCode.starts(with: "#") {
                            self.colorDictionary.updateValue(colorCode, forKey: obj)
                        }
                    }
                }
            }
        }
        
        if let confDict = confPlistDictionary!["Themes"] as? Dictionary<String, Any> {
            if let lightThemeDict = confDict[themeVariant] as? Dictionary<String, Any> {
                for obj in lightThemeDict.keys {
                    if let colorCode = lightThemeDict[obj] as? String {
                        if colorCode.starts(with: "#") {
                            self.colorDictionary.updateValue(colorCode, forKey: obj)
                        } else {
                            if let colorValue = colorTheme[colorCode] {
                                self.colorDictionary.updateValue(colorValue, forKey: obj)
                            } else if let colorValue = defaultColorTheme[colorCode] {
                                self.colorDictionary.updateValue(colorValue, forKey: obj)
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    func getColor(fromString colorStr: String) -> UIColor {
        let str = colorDictionary[colorStr] ?? ""
        return UIColor.init(hexString: str)
    }
    
    func setupAPIDefaultTheme() {
        if let selectedTheme = UserDefaults.standard.value(forKey: "selectedTheme") as? String{
            currentTheme = selectedTheme
        }
        setUpData()
    }
}

public extension UIColor {
    convenience init(hex: Int, alpha: Double = 1.0) {
        self.init(red: CGFloat((hex>>16)&0xFF)/255.0, green: CGFloat((hex>>8)&0xFF)/255.0, blue: CGFloat((hex)&0xFF)/255.0, alpha: CGFloat(255 * alpha) / 255)
    }
    
    convenience init(hexString: String, alpha: Double = 1.0) {
        var hexNormalized = hexString.trimmingCharacters(in: .whitespacesAndNewlines)
        hexNormalized = hexNormalized.replacingOccurrences(of: "#", with: "")
        
        var rgb: UInt32 = 0
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0
        let length = hexNormalized.count
        
        Scanner(string: hexNormalized).scanHexInt32(&rgb)
        
        if length == 6 {
            r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
            g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
            b = CGFloat(rgb & 0x0000FF) / 255.0
        } else if length == 8 {
            r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
            g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
            b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
            a = CGFloat(rgb & 0x000000FF) / 255.0
        }
        self.init(red: r, green: g, blue: b, alpha: a)
    }
}
