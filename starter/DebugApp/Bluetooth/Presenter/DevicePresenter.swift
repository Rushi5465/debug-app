//
//  DevicePresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/07/21.
//

import Foundation
import CoreBluetooth
import DropDown
import Zip

class DevicePresenter: BluetoothService, DevicePresenterProtocol {
	
	private weak var delegate: DeviceViewDelegate?
	private var firmwareService: DeviceWebServiceProtocol
	
	required init(delegate: DeviceViewDelegate, firmwareService: DeviceWebServiceProtocol) {
		self.delegate = delegate
		self.firmwareService = firmwareService
		super.init()
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerValueUpdate), name: Notification.Name(BLE_BA_CharacteristicBatteryLevel.uuidString), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerValueUpdate), name: Notification.Name(BLE_DI_CharacteristicModelNumber.uuidString), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerValueUpdate), name: Notification.Name(BLE_DI_CharacteristicSerialNumber.uuidString), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerValueUpdate), name: Notification.Name(BLE_DI_CharacteristicFirmwareRevision.uuidString), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerValueUpdate), name: Notification.Name(TransferService.dateServiceUUID.uuidString), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerDeviceDidConnect), name: Notification.Name("deviceDidConnect"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerDeviceDidDisconnect), name: Notification.Name("deviceDidDisconnect"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerValueUpdate), name: Notification.Name(TransferService.chargingUUID), object: nil)
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self, name: Notification.Name(BLE_BA_CharacteristicBatteryLevel.uuidString), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name(BLE_DI_CharacteristicModelNumber.uuidString), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name(BLE_DI_CharacteristicSerialNumber.uuidString), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name(BLE_DI_CharacteristicFirmwareRevision.uuidString), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name(TransferService.chargingUUID), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name("deviceDidConnect"), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name("deviceDidDisconnect"), object: nil)
	}
	
	func fetchDeviceInformation() {
		bluetoothManager.readValue(uuid: BLE_DI_CharacteristicModelNumber)
		bluetoothManager.readValue(uuid: BLE_DI_CharacteristicSerialNumber)
		bluetoothManager.readValue(uuid: BLE_DI_CharacteristicFirmwareRevision)
	}
	
	func startCheckingBatteryLevel() {
		//bluetoothManager.updateNotificationStateToTrue(uuid: TransferService.dateServiceUUID)
        //Logger.shared.addLog("updateNotificationStateToTrue : \(String(describing: self))")
	}
	
	func stopCheckingBatteryLevel() {
		//bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
	}
	
	func startCheckingBatteryChargingStatus() {
		//bluetoothManager.updateNotificationStateToTrue(uuid: TransferService.dateServiceUUID)
       // Logger.shared.addLog("updateNotificationStateToTrue : \(String(describing: self))")

	}
	
	func stopCheckingBatteryChargingStatus() {
		//bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
	}
	
	func removeDevice() {
        bluetoothManager.isMovanoDeviceSelected = false
		UserDefault.shared.connectedDevice = nil
        bluetoothManager.explicitDisconnection = true
		disconnectDevice()
	}
	
	func showDropDown() {
		
		let dropDown = DropDown()
		var dataSource = [String]()
		if bluetoothManager.connectedPeripheral != nil {
			dataSource.append("Disconnect")
		} else {
			dataSource.append("Connect")
		}
		dataSource.append("Remove Device")
		
		if bluetoothManager.connectedPeripheral != nil {
			dataSource.append("Update Firmware")
		}
		
		dropDown.dataSource = dataSource
		dropDown.textFont = UIFont.gilroy.semiBold(withSize: 14)
		dropDown.textColor = UIColor.textColor
		dropDown.backgroundColor = UIColor.settingBackground
		dropDown.separatorColor = UIColor.clear
		dropDown.shadowColor = UIColor.clear
		dropDown.cornerRadius = 8.0
		
		dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
			dropDown.hide()
			
			self.delegate?.dropDownItemSelected(item)
		}
		dropDown.direction = .bottom

		self.delegate?.dropDownDidShowUp(dropDown: dropDown)
	}
	
	func updateFirmware(version: String) {
		firmwareService.updateFirmware(version: version) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.firmwareUpdateError(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.firmwareUpdateSuccess(data: response)
			}
		}
	}
	
	func connectDevice() {
        bluetoothManager.isMovanoDeviceSelected = true
        bluetoothManager.explicitDisconnection = false
        bluetoothManager.startScanning()
	}
	
	func disconnectDevice() {
        bluetoothManager.explicitDisconnection = true
		if let peripheral = bluetoothManager.connectedPeripheral {
			bluetoothManager.disconnect(peripheral)
		}
	}
	
	func downloadFirmware(urlString: String) {
		firmwareService.downloadDeviceFirmware(urlString: urlString) { [weak self] (fileUrl, error) in
			if let error = error {
				self?.delegate?.downloadFirmwareUpdateError(error)
				return
			}
			
			if let fileUrl = fileUrl {
				self?.delegate?.downloadFirmwareUpdateSuccess(fileUrl)
			}
		}
	}
	
	func sendNewFirmwareImageToDevice(_ firmwareImageUrl: URL) {
		//TODO: Check with Shailendra for the charactere uuid for which we have to update the firmware, do it in following commented code and then uncomment it
		/*
		do {
			
			let manager = DocumentManager(manager: .default)
			let fileUrl = try Zip.quickUnzipFile(firmwareImageUrl) { (progress) in
				if progress == 1.0 {
					manager.deleteFiles([firmwareImageUrl])
				}
			}
			
			let data = try Data(contentsOf: fileUrl)
			bluetoothManager.writeValue(uuid: BLE_DI_CharacteristicFirmwareRevision, data: data)
			
		} catch {
			
		}
	*/
	}
	
	override func didUpdate(characteristic: CBCharacteristic) {
		guard let characteristicData = characteristic.value else { return }
		let byteArray = [UInt8](characteristicData)
		let battery = UInt(byteArray[2])
		print("Battery percentage is -\(battery)")
		NotificationCenter.default.post(name: NSNotification.Name(TransferService.dateServiceUUID.uuidString), object: "\(battery)")
		let chargingPercentage = UInt(byteArray[14])
        delegate?.currentBattery(percentage: Int(chargingPercentage))
		print("Battery Charging percentage is -\(chargingPercentage)")
//		NotificationCenter.default.post(name: NSNotification.Name(TransferService.chargingUUID), object: "\(chargingPercentage)")
		
	}
}

// MARK: - Bluetooth related operation
extension DevicePresenter {
	
	@objc private func bluetoothManagerValueUpdate(notification: NSNotification) {
		if let object = notification.object as? String {
			if notification.name.rawValue == BLE_BA_CharacteristicBatteryLevel.uuidString {
				let value = Int(object)
//				self.delegate?.currentBattery(percentage: value!)
			} else if notification.name.rawValue == TransferService.dateServiceUUID.uuidString {
				let value = Int(object)
				//self.delegate?.currentBattery(percentage: value!)
			} else if notification.name.rawValue == BLE_DI_CharacteristicModelNumber.uuidString {
				self.delegate?.deviceInformation(modelNumber: object)
			} else if notification.name.rawValue == BLE_DI_CharacteristicSerialNumber.uuidString {
				self.delegate?.deviceInformation(serialNumber: object)
			} else if notification.name.rawValue == BLE_DI_CharacteristicFirmwareRevision.uuidString {
				self.delegate?.deviceInformation(firmwareRevision: object)
			} else if notification.name.rawValue == TransferService.chargingUUID {
				let value = Int(object)
				self.delegate?.currentChargingStatus(chargingPercentage: value!)
			}
		}
	}
	
	@objc private func bluetoothManagerDeviceDidConnect(notification: NSNotification) {
		if notification.name.rawValue == "deviceDidConnect" {
			if let batteryPercentage = (notification.object as? PeripheralDevice)?.detail.batteryPercentage {
				UserDefaults.standard.set(batteryPercentage, forKey: "batteryPercentage")
			}
			self.delegate?.updateDeviceStatus()
		}
	}
	
	@objc private func bluetoothManagerDeviceDidDisconnect(notification: NSNotification) {
		if notification.name.rawValue == "deviceDidDisconnect" {
			self.delegate?.updateDeviceStatus()
		}
	}
}
