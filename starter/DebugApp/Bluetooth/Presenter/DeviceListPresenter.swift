//
//  DeviceListPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 18/07/21.
//

import Foundation

class DeviceListPresenter: DeviceListPresenterProtocol {
	
	private weak var delegate: DeviceListViewDelegate?
	
	var connectedPeripheral: PeripheralDevice? {
		return bluetoothManager.connectedPeripheral
	}
	
	var peripherals = Set<PeripheralDevice>() {
		didSet {
			self.delegate?.devices(list: peripherals)
		}
	}
	
	required init(delegate: DeviceListViewDelegate) {
		self.delegate = delegate
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerConnectionStatus), name: Notification.Name("deviceConnectionStatus"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerDeviceScanningStopped), name: Notification.Name("deviceScanningStopped"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerDeviceDidDiscover), name: Notification.Name("deviceDidDiscover"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerDeviceDidConnect), name: Notification.Name("deviceDidConnect"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerDeviceDidDisconnect), name: Notification.Name("deviceDidDisconnect"), object: nil)
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self, name: Notification.Name("deviceConnectionStatus"), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name("deviceScanningStopped"), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name("deviceDidDiscover"), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name("deviceDidConnect"), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name("deviceDidDisconnect"), object: nil)
	}
	
	func startScanning(withAutoStop: Bool) {
		bluetoothManager.discoveredPeripherals.removeAll()
		bluetoothManager.startScanning()
		
		if withAutoStop {
			DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
				bluetoothManager.stopScanning()
			}
		}
	}
	
	func stopScanning() {
		bluetoothManager.stopScanning()
	}
	
	func connect(device: PeripheralDevice) {
        bluetoothManager.explicitDisconnection = false
        bluetoothManager.isMovanoDeviceSelected = true
		bluetoothManager.connect(device)
	}
	
	func disconnect(device: PeripheralDevice) {
        bluetoothManager.isMovanoDeviceSelected = false
		bluetoothManager.disconnect(device)
	}
}

// MARK: - Bluetooth related operation
extension DeviceListPresenter {
	
	@objc private func bluetoothManagerConnectionStatus(notification: NSNotification) {
		if let object = notification.object as? DeviceModel, notification.name.rawValue == "deviceConnectionStatus" {
			if let previousConnectingDevice = object.previousConnectingDevice {
				self.peripherals.remove(previousConnectingDevice)
			}
			self.peripherals.insert(object.newConnectingDevice)
		}
	}
	
	@objc private func bluetoothManagerDeviceScanningStopped(notification: NSNotification) {
		if notification.name.rawValue == "deviceScanningStopped" {
			self.delegate?.scanningStopped()
		}
	}
	
	@objc private func bluetoothManagerDeviceDidDiscover(notification: NSNotification) {
		if let object = notification.object as? Set<PeripheralDevice>, notification.name.rawValue == "deviceDidDiscover" {
			self.peripherals = Set(object.map { $0 })
		}
	}
	
	@objc private func bluetoothManagerDeviceDidConnect(notification: NSNotification) {
		if let object = notification.object as? PeripheralDevice, notification.name.rawValue == "deviceDidConnect" {
			self.peripherals.insert(object)
		}
	}
	
	@objc private func bluetoothManagerDeviceDidDisconnect(notification: NSNotification) {
		if let object = notification.object as? PeripheralDevice, notification.name.rawValue == "deviceDidDisconnect" {
			self.peripherals.remove(object)
		}
	}
}
