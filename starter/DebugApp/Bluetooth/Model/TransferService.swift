//
//  TransferService.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/06/21.
//

import Foundation
import CoreBluetooth

struct TransferService {
	static let serviceUUID = CBUUID(string: "16ACAE4A-A70C-B41A-7164-33233CC23E9B")
	static let characteristicUUID = CBUUID(string: "08590F7E-DB05-467E-8757-72F6FAEB13D4")
	static let dataCharactersticUUID = CBUUID(string: "08591F7E-DB05-467E-8757-72F6FAEB13D4")
	
	static let dateServiceUUID: CBUUID = CBUUID.init(string: "FFC32EC7-CD20-467F-961E-B8B810F28309")
	static let chargingUUID = "chargingUUID"
}
