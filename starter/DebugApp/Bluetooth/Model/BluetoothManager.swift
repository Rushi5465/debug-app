//
//  BluetoothManager.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/06/21.
//

import Foundation
import UIKit
import CoreBluetooth

// swiftlint:disable line_length
// swiftlint:disable all
struct DeviceModel {
    let newConnectingDevice: PeripheralDevice
    let previousConnectingDevice: PeripheralDevice?
}

// When scanning for peripherals, results can include all peripherals found, or just peripherals with certain UUIDs.
// When filtering peripherals by UUIDs is used during scanning, this is the list of UUIDs of interest.
// (Refer to BluetoothManager.isScanFiltered as to whether this list is used.)
let discoverPeripheralsWithServices = [
    BLE_MV_ServiceMovanoSensor
]

// When discovering characteristics, results can include all characteristics found, or just characteristics with certain UUIDs.
// When filtering characteristics by UUIDs is used during discovery, this is the list of UUIDs of interest.
// (Refer to BluetoothManager.isDiscoverFiltered as to whether this list is used.)
//let discoverPeripheralCharacteristics = [
//    BLE_GA_CharacteristicDeviceName,
//    BLE_GA_CharacteristicDeviceAppearance,
//    BLE_DI_CharacteristicModelNumber,
//    BLE_DI_CharacteristicSerialNumber,
//    BLE_DI_CharacteristicSystemID,
//    BLE_DI_CharacteristicFirmwareRevision,
//    BLE_DI_CharacteristicHardwareRevision,
//    BLE_DI_CharacteristicSoftwareRevision,
//    BLE_DI_CharacteristicManufacturerName,
//    BLE_MV_CharacteristicMovanoControl,
//    BLE_MV_CharacteristicMovanoData
//]

open class BluetoothManager: NSObject {
    var isBLEConnected = false
    var isMovanoDeviceSelected = false
    private static var singleInstance : BluetoothManager?
    
    // BLE related members.
    public var centralManager: CBCentralManager!                                   // Core Bluetooth central manager.
    
    // Peripheral we are connected to.
    public var connectedPeripheral: PeripheralDevice?
    var lastSessionStepCount = 0
    var autoConnect: Bool  = false {
        didSet {
            if autoConnect {
                self.startScanning()
            }
        }
    }
    
    public var discoveredServiceUuids: [CBUUID] = []                        // List of service UUIDs found during discovery for a connected peripheral.
    public var uuidDictionary: [CBUUID : String] = [:]                      // A descriptive string for a given UUID used when printing log messages.
    var soiDict: [CBUUID : Service] = [:]                                   // Dictionary of services of interest (SOI). (Services of interest are defined by a user via the initServices function.)
    var chDict: [CBUUID : CBCharacteristic] = [:]                           // Dictionary of discovered characteristics.
    var bluetoothDataService:BluetoothService?
    var connectingPeripheral: PeripheralDevice?
    var data = Data()
    var packet = 0
    private var connectedPeripheralCharacterstic: CBCharacteristic?
    
    // Members that can be observed.
    public var isBleCapable = false                              // Indicates whether the client device is BLE capable.
    public var isScanFiltered = false                            // Indicates whether scanning for peripherials should filter using discoverPeripheralsWithServices array or nil.
    public var isDiscoverFiltered = false                        // Indicates whether discovery of characteristics should filter using discoverPeripheralCharacteristics array or nil.

    public var discoveredPeripherals = Set<PeripheralDevice>()      // An array of scanned peripherals.
    
    public var bleString: [CBUUID : String] = [:]                // Dictionary containing BLE characteristic values as strings.
    public var bleData: [CBUUID : Data] = [:]                    // Dictionary containing BLE characteristic values as data.

    public var rssiString = "-100 dBm"                           // BLE RSSI in dB (-100 to 0)
        
    let dataDirectoryPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)  // The top level directory. Data is saved under this directory or subdirectories.
    
    var explicitDisconnection = Bool()
    
    static var shared: BluetoothManager {
            if singleInstance == nil {
                singleInstance = BluetoothManager()
            }
            
            return singleInstance!
    }
    
    override init() {
        super.init()
        self.centralManager = CBCentralManager(delegate: self, queue: nil)
        self.centralManager?.delegate = self
        
        //initServices()
    }

    // MARK: - View Functions
    /**
     Send a start notification for a characteristic.
     
     Call the characteristic's notifyStart() function (if any)  and then send a BLE notification so that the sensor will begin forwarding data.
     
     - Parameter uuid: CBUUID for the desired characteristic.
     */
    func updateNotificationStateToTrue(uuid: CBUUID) {
        guard connectedPeripheral != nil else {
            return
            
        }
        guard chDict[uuid] != nil else {
            return
            
        }
        
        let ch = chDict[uuid]!
        soiDict[ch.service!.uuid]?.notifyStart(characteristic: ch)
        connectedPeripheral?.peripheral?.setNotifyValue(true, for: ch)
    }

    /**
     Stop collecting data from a sensor.
     
     Send a BLE notification so that the peripheral will stop forwarding data, and then call the characteristic's notifySop() function (if any).
     
     - Parameter uuid: CBUUID for the desired characteristic.
     */
    func updateNotificationStateToFalse(uuid: CBUUID) {
        guard connectedPeripheral != nil else {
            return
        }
        guard chDict[uuid] != nil else { return }
        
        let ch = chDict[uuid]!
        connectedPeripheral?.peripheral?.setNotifyValue(false, for: ch)
        soiDict[ch.service!.uuid]?.notifyStop(characteristic: ch)
    }
    
    /**
     Indicates whether a peripheral supports a service.
     
     - Returns true if the service was discovered for a connected peripheral.
     */
    func serviceEnabled(uuid: CBUUID) -> Bool {
        if discoveredServiceUuids.contains(uuid) {
            return true
        }
        return false        // disabled
    }

    /**
     Indicates whether a discovered characteristic has a notify or indicate property.
     
     - Parameter uuid: CBUUID for the characteristic of interest.
     - Returns true if the characteristic indicated by the UUID has a notify or indicate property.
     */
    func notifyEnabled(uuid: CBUUID) -> Bool {
        guard chDict[uuid] != nil else { return false }
        let ch = chDict[uuid]!
        guard ch.properties.contains(.notify) || ch.properties.contains(.indicate) else { return false }
        return true     // enabled
    }

    /**
     Issues a read request for a given characteristic.
     
     The request is ignored if the characteristic is not available or doesn't have a read property.
     
     - Parameter uuid: CBUUID for the characteristic of interest.
     */
    func readValue(uuid: CBUUID) {
        guard chDict[uuid] != nil else { return }
        let ch = chDict[uuid]!
        guard ch.properties.contains(.read) else { return }
        connectedPeripheral?.peripheral?.readValue(for: ch)
    }
    
    func setBluetoothService(serivce:BluetoothService?){
        self.bluetoothDataService = serivce
    }
    /**
     Indicates whether a discovered characteristic has a read property.
     
     - Parameter uuid: CBUUID for the characteristic of interest.
     - Returns true if the characteristic indicated by the UUID has a read property.
     */
    func readEnabled(uuid: CBUUID) -> Bool {
        guard chDict[uuid] != nil else { return false }
        let ch = chDict[uuid]!
        guard ch.properties.contains(.read) else { return false }
        return true
    }

    /**
     Issues a write request for a given characteristic.
     
     The request is ignored if the characteristic is not available or doesn't have a write property.
     
     - Parameter uuid: CBUUID for the characteristic of interest.
     */
    func writeValue(uuid: CBUUID, data: Data) {
        guard chDict[uuid] != nil else { return }
        guard connectedPeripheral?.peripheral?.canSendWriteWithoutResponse != nil else { return }
        let characteristic = chDict[uuid]!
        if characteristic.properties.contains(.writeWithoutResponse) {
            connectedPeripheral?.peripheral?.writeValue(data, for: characteristic, type: .withoutResponse)
        } else {
            connectedPeripheral?.peripheral?.writeValue(data, for: characteristic, type: .withResponse)
        }
    }

    /**
     Indicates whether a discovered characteristic has a write property.
     
     - Parameter uuid: CBUUID for the characteristic of interest.
     - Returns true if the characteristic indicated by the UUID has a write property.
     */
    func writeEnabled(uuid: CBUUID) -> Bool {
        guard chDict[uuid] != nil else { return false }
        let ch = chDict[uuid]!
        guard ch.properties.contains(.write) || ch.properties.contains(.writeWithoutResponse) else { return false }
        return true
    }
    
    // MARK: - BLE Functions
    /**
     Start scanning
     Calls CBManager.scanForPeripherals
     - Parameter filter: If true, scan only for selected services, otherwise scan for all services. Selected services are listed in our **discoverPeripheralsWithServices** array.
     */
    func startScanning(filter: Bool = false) {
        if filter {
            self.centralManager?.scanForPeripherals(withServices: discoverPeripheralsWithServices, options: nil)
        } else {
            self.centralManager?.scanForPeripherals(withServices: nil, options: nil)
        }
     }

    /**
     Steps taken when the client is to stop scanning.
     */
    func stopScanning() {
        self.centralManager!.stopScan()
        NotificationCenter.default.post(name: NSNotification.Name("deviceScanningStopped"), object: nil)
    }

    /**
     Request disconnecting a connected peripheral.
     */
    func cancelPeripheral() {
        guard connectedPeripheral != nil else {
            return
        }
        centralManager?.cancelPeripheralConnection((self.connectedPeripheral?.peripheral!)!)
    }
    
    /**
     Initiates a connection to a peripheral.
     
     Calls CBManager.connect to initiate a connection.
    - Parameter peripheral: The peripheral to connect to.
    - Note:
     The input is a peripheralDevice structure. The peripheral to connect to is identified within the structure.
     */
    func connectPeripheral(peripheral: PeripheralDevice) {
        stopScanning()

        self.connectedPeripheral!.peripheral!.delegate = self       // Make sure we get the discovery callbacks.
        
        self.centralManager?.connect(peripheral.peripheral!, options: nil)
    }
    
    /**
     Return a characteristic's properties as a string.
     
     An example return string is "read, notify, indicate".
     Properties examined are read, notify, indicate, and write.
     
     - parameter characteristic: A given characteristic
     - returns Characteristic properties as a string
     */
    func characteristicString(characteristic: CBCharacteristic) -> String {
        // Express the characteristic.properties as a string.
        var properties = ""
        var separator = ""
        if characteristic.properties.contains(.read) {
            properties += separator
            properties += "read"
            separator = ", "
        }
        if characteristic.properties.contains(.notify) {
            properties += separator
            properties += "notify"
            separator = ", "
        }
        if characteristic.properties.contains(.indicate) {
            properties += separator
            properties += "indicate"
            separator = ", "
        }
        if characteristic.properties.contains(.write) {
            properties += separator
            properties += "write"
        }

        // See if we have a name for the UUID.
        let characteristicName1 = uuidDictionary[characteristic.uuid, default:"unknown"]
        
        let str = "UUID: \(characteristic.uuid), aka: \(characteristicName1), properties: \(String(format: "0x%x", characteristic.properties.rawValue)) = \(properties), isNotifying: \(characteristic.isNotifying)"
        return str
    }
    
    func discoverDescriptors(peripheral: CBPeripheral, characteristic: CBCharacteristic) {
        peripheral.discoverDescriptors(for: characteristic)
    }
    
    func connect(_ peripheral: PeripheralDevice) {
        self.centralManager?.connect(peripheral.peripheral!, options: nil)
        
        var previousConnectingDevice: PeripheralDevice?
        if self.connectingPeripheral != nil {
            previousConnectingDevice = self.connectingPeripheral
            disconnect(self.connectingPeripheral!)
            return
        }
        self.connectingPeripheral = peripheral
        
        let deviceModel = DeviceModel(newConnectingDevice: peripheral, previousConnectingDevice: previousConnectingDevice)
        //NotificationCenter.default.post(name: NSNotification.Name("deviceConnectionStatus"), object: deviceModel)
    }
    
    func disconnect(_ peripheral: PeripheralDevice) {
        data = Data()
        Logger.shared.addLog("User Explicitly disconnected the peripheral")
    //    explicitDisconnection = true
        self.centralManager?.cancelPeripheralConnection(peripheral.peripheral!)
        self.connectingPeripheral = nil
    }
    
    /*
     *  Call this when things either go wrong, or you're done with the connection.
     *  This cancels any subscriptions if there are any, or straight disconnects if not.
     *  (didUpdateNotificationStateForCharacteristic will cancel the connection if a subscription is involved)
     */
    private func cleanup() {
        lastSessionStepCount = 0
        Logger.shared.addLog("Request for resource cleanup")
        // Don't do anything if we're not connected
        guard let discoveredPeripheral = connectedPeripheral else { return }
        
        for service in (discoveredPeripheral.peripheral?.services ?? [] as [CBService]) {
            for characteristic in (service.characteristics ?? [] as [CBCharacteristic]) {
                if (characteristic.uuid == TransferService.characteristicUUID || characteristic.uuid == TransferService.dataCharactersticUUID) && characteristic.isNotifying {
                    // It is notifying, so unsubscribe
                    self.connectedPeripheral?.peripheral?.setNotifyValue(false, for: characteristic)
                }
            }
        }
        
        self.discoveredPeripherals.removeAll()
        self.connectedPeripheral = nil
        //initServices()
        discoveredServiceUuids = []
        chDict = [:]
        
        // If we've gotten this far, we're connected, but we're not subscribed, so we just disconnect
        //explicitDisconnection = true
        centralManager.cancelPeripheralConnection(discoveredPeripheral.peripheral!)
    }
}

extension BluetoothManager: CBCentralManagerDelegate {
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        isBleCapable = false
        switch (central.state) {
        case .poweredOn:
            isBleCapable = true
            //if self.autoConnect {
            //    self.startScanning()
            //}
            Logger.shared.addLog("Bluetooth is powered on")
        case .unsupported:
            Logger.shared.addLog("Bluetooth Low Energy is not supported on this device.")
        case .unauthorized:
            Logger.shared.addLog("The app is not authorized to use Bluetooth Low Energy.")
        case .poweredOff:
            Logger.shared.addLog("Bluetooth is currently powered off.")
        case .resetting:
            Logger.shared.addLog("Bluetooth Manager is resetting")
        case .unknown:
            Logger.shared.addLog("Unknown Bluetooth state.")
        default:
            Logger.shared.addLog("Bluetooth Error")
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        if peripheral.name != nil {
            // This is a new peripheral to us (it isn't in our array of scanned peripherals).
            // Check the RSSI.
            var rssi = RSSI.intValue
            if rssi > 0 {
                rssi = rssi - 128
            }

            // Figure out a name for this peripheral.
            var peripheralName = "Unknown"
            var peripheralNameFound = false
            
            // We could use peripheral.name but this might be <null> or it may return the name of the device that exists in its database as the GAP device name.
            // (See https://www.novelbits.io/intro-ble-mobile-development-ios-part-2/)
            // First check to see if a name is included in the advertisementData.
            if let name = advertisementData[CBAdvertisementDataLocalNameKey] as? String {
                peripheralName = name
                peripheralNameFound = true
            }
            var receivedManufatureData: [UInt8] = []
            if let manufatureData = advertisementData["kCBAdvDataManufacturerData"] as? Data{
                receivedManufatureData = Array(manufatureData)
            }
            // If we still don't have a name, see if peripheral.name has something.
            if !peripheralNameFound {
                if peripheral.name != nil {
                    peripheralName = peripheral.name!
                    peripheralNameFound = true
                }
            }
            // If we still don't have a name, see if the UUID is included in the advertisementData. We can then see what name we've assigned to this UUID (if any).
            if !peripheralNameFound {
                if let uuids = advertisementData[CBAdvertisementDataServiceUUIDsKey] as? [CBUUID] {
                    for uuid in uuids {
                        let name = uuidDictionary[uuid]
                        if name != nil {
                            peripheralName = name!
                            peripheralNameFound = true
                            break
                        }
                    }
                }
            }
            // If we still don't have a name, use the pheripheral's identifier as a name.
            if !peripheralNameFound {
                peripheralName = peripheral.identifier.uuidString
            }
            
            // Create a peripheralDevice object.
            // We use the length of peripherals array as our unique id for this peripheral.
            var batteryPercentage = 0
            var chargingPower = 0
            if(receivedManufatureData.count >= 4){
                batteryPercentage = Int(receivedManufatureData[2])
                chargingPower = Int(receivedManufatureData[3])
            }
            let detail = DeviceDetail(name: peripheralName,batteryPercentage: batteryPercentage, chargingPower: chargingPower, rssi: rssi)
            let newPeripheral = PeripheralDevice(peripheral: peripheral, detail: detail)
            
            // Add this peripheral to our scan list.
            let periphralNameInSmall = peripheralName.lowercased()
            if(periphralNameInSmall.contains("kingpin")){
                Logger.shared.addLog("didDiscover Peripheral: \(peripheral.name!), rssi: \(RSSI.stringValue)")
                if !self.discoveredPeripherals.contains(where: {($0.peripheral?.identifier == newPeripheral.peripheral?.identifier)}){
                    self.discoveredPeripherals.insert(newPeripheral)
                    NotificationCenter.default.post(name: NSNotification.Name("deviceDidDiscover"), object: self.discoveredPeripherals)
                }
//                else{
//                    let filteredPeripherals = discoveredPeripherals.filter{$0.peripheral?.identifier == peripheral.identifier}
//                        if(filteredPeripherals.count>0){
//                            var peripheralObj = filteredPeripherals.first
//                            peripheralObj?.detail.batteryPercentage = batteryPercentage
//                            peripheralObj?.detail.chargingPower = chargingPower
//                        }
//                    NotificationCenter.default.post(name: NSNotification.Name("deviceDidDiscover"), object: self.discoveredPeripherals)
//                    }
                }
            
          
            
            //if autoConnect {
            if(UserDefault.shared.connectedDevice != nil){
                var deviceArray = Array(discoveredPeripherals)
                deviceArray = deviceArray.filter { (account) -> Bool in
                    return account.detail.name == UserDefault.shared.connectedDevice?.name
                }
                
                if !deviceArray.isEmpty {
                    isBLEConnected = true
                    self.connect(deviceArray[0])
                }
            }
            //}
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        isBLEConnected = true
        Logger.shared.addLog("Peripheral Connected: " + peripheral.name!)
        
        self.autoConnect = false
        // Flag the connection
       // if autoConnect {
            self.stopScanning()
       // }
        for each in discoveredPeripherals where each.peripheral == peripheral {
            self.connectedPeripheral = each           // Save the peripheral.
            connectedPeripheral?.detail.addedOn = Date()
            UserDefault.shared.connectedDevice = connectedPeripheral?.detail
            if(self.isMovanoDeviceSelected){
                self.isMovanoDeviceSelected = false
                NotificationCenter.default.post(name: NSNotification.Name("ScanForBLEDevice"), object: nil)
            }
        }
        self.connectedPeripheral?.peripheral?.delegate = self       // Make sure we get the discovery callbacks.
        
        self.connectedPeripheral?.peripheral?.readRSSI()            // Get an update for the RSSI.
        
        for each in discoveredPeripherals where each.peripheral == peripheral {
            NotificationCenter.default.post(name: NSNotification.Name("deviceDidConnect"), object: each)
        }
        if let connectedPeriDevice = connectedPeripheral, let peripheralDevice = connectedPeriDevice.peripheral{
            peripheralDevice.discoverServices(nil)
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        isBLEConnected = false
        // Invoked whenever the central manager fails to create a connection with the peripheral.
        Logger.shared.addLog("Failed to connect to \(peripheral.name!)")
        // Don't do anything if we're not connected
        guard let connectedPeripheral = connectedPeripheral,
              case .connected = connectedPeripheral.detail.state
        else {
            cleanup()
            return
        }
        
        // This cancels any subscriptions if there are any, or straight disconnects if not.
        // (didUpdateNotificationStateForCharacteristic will cancel the connection if a subscription is involved)
        for service in (connectedPeripheral.peripheral?.services ?? [] as [CBService]) {
            for characteristic in (service.characteristics ?? [] as [CBCharacteristic]) where characteristic.isNotifying {
                self.connectedPeripheral?.peripheral?.setNotifyValue(false, for: characteristic)
            }
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        isBLEConnected = false
        // Invoked whenever an existing connection with the peripheral is torn down.
        // Clean up our local copy; reset local variables. Being disconnected, start scanning again.
        lastSessionStepCount = 0
        Logger.shared.addLog("Peripheral Disconnected: " + peripheral.name!)

        if let error = error {
            Logger.shared.addLog("Peripheral Disconnection Error: \(error.localizedDescription)")
        }
        
        guard connectedPeripheral?.peripheral != nil else { return }
        
        for each in discoveredPeripherals where each.peripheral == peripheral {
            cleanup()
            NotificationCenter.default.post(name: NSNotification.Name("deviceDidDisconnect"), object: each)
        }

        
        if !explicitDisconnection {
            NotificationCenter.default.post(name: NSNotification.Name("bluetoothServiceInvalidated"), object: nil)
        }
        let filteredPeripherals = discoveredPeripherals.filter{$0.peripheral?.identifier != peripheral.identifier}
        NotificationCenter.default.post(name: NSNotification.Name("deviceDidDiscover"), object: filteredPeripherals)
        
    }
}

extension BluetoothManager: CBPeripheralDelegate {
    
    /*
     *  The Transfer Service was discovered
     */
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        var str: String = ""
        switch peripheral.state {
        case .disconnected:
            str = "disconnected"
        case .connecting:
            str = "connecting"
        case .connected:
            str = "connected"
        case .disconnecting:
            str = ".disconnecting"
        default:
            break
        }
        str = String(format: "identifier: \(peripheral.identifier), name: \(peripheral.name ?? "(null))"), state: \(str)")
        
        if let error = error {
            Logger.shared.addLog("Error discovering services: \(error.localizedDescription)")
            cleanup()
            return
        }
        Logger.shared.addLog("Discover services for peripheral \(str)")
        
        // Loop through the peripheral.services array (just in case there's more than one) looking for characteristics.
        guard let peripheralServices = peripheral.services else { return }
        // For characteristics for the service, discover all characteristics (filter = nil) or just ones of interst (filter = discoverPeripheralCharacteristics).
        //var list: [CBUUID]?
//        if self.isDiscoverFiltered {
//            list = discoverPeripheralCharacteristics
//        }
        // For each service, discover characteristics.
        for service in peripheralServices {
            self.discoveredServiceUuids.append(service.uuid)            // Add this service to our list of discovered services
            //peripheral.discoverCharacteristics(list, for: service)      // Discover supported characteristics
            peripheral.discoverCharacteristics([BLE_MV_CharacteristicMovanoData], for: service)      // Discover supported characteristics

        }
    }
    
    /*
     *  The Transfer characteristic was discovered.
     *  Once this has been found, we want to subscribe to it, which lets the peripheral know we want the data it contains
     */
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        if let error = error {
            Logger.shared.addLog("Error discovering characteristics: \(error.localizedDescription)")
            cleanup()
            return
        }
        
        let serviceName = uuidDictionary[service.uuid, default:"\(service.uuid)"]
        Logger.shared.addLog("Discover characteristics for service \(serviceName)")

        guard let characteristics = service.characteristics else { return }
        
        for characteristic in characteristics {
            let str = characteristicString(characteristic: characteristic)
            Logger.shared.addLog("didDiscoverCharacteristicsFor characteristic: \(str)")
            
            let characteristicName = uuidDictionary[characteristic.uuid, default:"\(characteristic.uuid)"]
            Logger.shared.addLog("characteristic: \(characteristicName)")
            
            chDict[characteristic.uuid] = characteristic
        //    soiDict[service.uuid]?.didDiscoverCharacteristicAction(characteristic: characteristic)
            
            discoverDescriptors(peripheral: peripheral, characteristic: characteristic)
            
            //bluetoothManager.updateNotificationStateToTrue(uuid: TransferService.dateServiceUUID)
            //Logger.shared.addLog("updateNotificationStateToTrue : \(String(describing: self))")

        }
        
        NotificationCenter.default.post(name: NSNotification.Name("DeviceConnected"), object: nil)
    }
    
    /*
     *   This callback lets us know more data has arrived via notification on the characteristic
     */
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        // Invoked upon completion of a -[readValueForCharacteristic:] request or on the reception of a notification/indication.
        if let error = error {
            Logger.shared.addLog("Error discovering characteristics: \(error.localizedDescription)")
            return
        }

        let str = characteristicString(characteristic: characteristic)
        Logger.shared.addLog(str)
        
        // Handle the incoming data.
        soiDict[characteristic.service!.uuid]?.didUpdate(characteristic: characteristic)
        if let data = bleString[characteristic.service!.uuid] {
            NotificationCenter.default.post(name: NSNotification.Name(characteristic.uuid.uuidString), object: data)
        }
        
        if let dataService = self.bluetoothDataService{
            dataService.didUpdate(characteristic: characteristic)
        }
    }
    
    public func peripheralDidUpdateRSSI(_ peripheral: CBPeripheral, error: Error?) {
        self.rssiString = "-100 dBm"
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        var rssi = RSSI.intValue
        if rssi > 0 {
            rssi = rssi - 128
        }
        Logger.shared.addLog("didReadRSSI rssi= \(rssi)")
        self.rssiString = "\(rssi) dBm"
    }
    
    /*
     *  The peripheral letting us know whether our subscribe/unsubscribe happened or not
     */
    public func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        // Deal with errors (if any)
        if let error = error {
            Logger.shared.addLog("Error changing notification state: \(error.localizedDescription)")
            return
        }
        
        // Exit if it's not the transfer characteristic
        guard characteristic.uuid == TransferService.dataCharactersticUUID else { return }
        
        if characteristic.isNotifying {
            // Notification has started
            Logger.shared.addLog("Notification began on \(characteristic)")
        } else {
            // Notification has stopped, so disconnect from the peripheral
            Logger.shared.addLog("Notification stopped on \(characteristic). Disconnecting")
            cleanup()
        }
    }
    
    /*
    *  The peripheral letting us know when services have been invalidated.
    */
    public func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
        
        self.packet = 0
        self.data = Data()
        Logger.shared.addLog("Transfer service is invalidated - rediscover services")
        peripheral.discoverServices(nil)
        centralManager.cancelPeripheralConnection(peripheral)
        NotificationCenter.default.post(name: NSNotification.Name("bluetoothServiceInvalidated"), object: nil)
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
        guard let descriptors = characteristic.descriptors else { return }
     
        // Get user description descriptor
        if let userDescriptionDescriptor = descriptors.first(where: {
            return $0.uuid.uuidString == CBUUIDCharacteristicUserDescriptionString
        }) {
            // Read user description for characteristic
            peripheral.readValue(for: userDescriptionDescriptor)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?) {
        // Get and print user description for a given characteristic
        if descriptor.uuid.uuidString == CBUUIDCharacteristicUserDescriptionString,
            let userDescription = descriptor.value as? String {
            print("Characterstic \(descriptor.characteristic!.uuid.uuidString) is also known as \(userDescription)")
        }
    }
}

extension BluetoothManager {
    
    func initServices() {
        initGenericAccess(self)                     // Initialization for a generic service, "1800".
        initCurrentTime(self)                       // Initialization for a current time service, "1805".
        initHealthThermometer(self)                 // Initialization for a health thermometer service "1809".
        initDeviceInfo(self)                        // Initialization for a device service, "180A".
        initBattery(self)                           // Initialization for a battery service, "180F".
        initAutomationIo(self)                      // Initialization for an automation IO service, "1815".
        initEnvironmentalSensing(self)              // Initialization for an environmental sensing service, "181A".
        initAccelerationOrientation(self)           // Initialization for a SiLabs Thunderboard acceleration service, "A4E649F4-4BE5-11E5-885D-FEFF819CDC9F".
        initHallEffect(self)                        // Initialization for a SiLabs Thunderboard Hall effect service, "F598DBC5-2F00-4EC5-9936-B3D1AA4F957F".
        initPowerSource(self)                       // Initialization for a SiLabs Thunderboard power source service, "EC61A454-ED00-A5E8-B8F9-DE9EC026EC51".
//        initMovano(self)                            // Initialization for a Movano service, "129bda25-3a33-418d-92f0-4138fefb54d1".
    }
}

// MARK: - Reference Material

// References for BLE services and characteristics.
// https://www.bluetooth.com/specifications/gatt/BLE_Services/
// https://www.bluetooth.com/specifications/gatt/characteristics/
// https://www.silabs.com/documents/public/application-notes/AN980.pdf
