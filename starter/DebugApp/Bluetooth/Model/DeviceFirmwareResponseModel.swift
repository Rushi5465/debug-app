//
//  DeviceFirmwareResponseModel.swift
//  BloodPressureApp
//
//  Created by Rushikant on 10/08/21.
//

struct DeviceFirmwareResponseModel: Decodable {
	
	let body: DeviceFirmwareBodyModel
	let statusCode: Int
}

struct DeviceFirmwareBodyModel: Decodable {
	let version_status: String
	let latest_version_presigned_url: String
	let release_notes: String
}

enum version_status: String {
	case updateNeeded = "update_needed"
	case uptoDate = "up_to_date"
}
