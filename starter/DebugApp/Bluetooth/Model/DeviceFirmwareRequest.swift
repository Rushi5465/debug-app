//
//  DeviceFirmwareRequest.swift
//  BloodPressureApp
//
//  Created by Rushikant on 10/08/21.
//

import UIKit

struct DeviceFirmwareRequest: Codable {
	let version : String
}
