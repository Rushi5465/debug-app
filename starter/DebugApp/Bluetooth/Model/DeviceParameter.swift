//
//  DeviceParameter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/07/21.
//

import Foundation

struct DeviceParameter {
	
	let info: String
	var data: String?
}
