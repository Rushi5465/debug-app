//
//  PeripheralDevice.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/06/21.
//

import Foundation
import CoreBluetooth

enum PeripheralState: String, Codable {
	case connected
	case connecting
	case disconnected
	case disconnecting
}

// swiftlint:disable line_length
// MARK: - Structure for Scanned Peripherals
/**
 Structure associated with a scanned peripheral. In particular this records the latest RSSI when scanning. (This can be used to display a list of scanned peripherals and their associated RSSI values.) Other features are also saved.
 
 This captures peripheral features for a scanned peripheral:
 - name: A peripheral name (determined from multiple criteria, but seldom peripheral.name).
 - rssi: The received power level in dB, nominally -30 to -90.
 - periphearl: The peripheral object.
 - id: The order in which the given peripheral was found amongst the other peripherals.
 */
public struct PeripheralDevice: Hashable{
	
	public var peripheral: CBPeripheral?
	var detail: DeviceDetail
}

public struct DeviceDetail: Codable, Hashable {
	
	var name: String
  //  var identifier:CBUUID?
	var state: PeripheralState = .disconnected
	var addedOn: Date?
    var batteryPercentage:Int
    var chargingPower:Int
	var rssi: Int
}
