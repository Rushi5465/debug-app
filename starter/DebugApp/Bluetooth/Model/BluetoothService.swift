//
//  BluetoothService.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 30/11/21.
//

import Foundation
import CoreBluetooth

class BluetoothService {
    
    func didUpdate(characteristic: CBCharacteristic) {
        print("characteristic: \(characteristic)")
    }
    
}
