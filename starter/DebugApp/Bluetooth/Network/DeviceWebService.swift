//
//  DeviceWebService.swift
//  BloodPressureApp
//
//  Created by Rushikant on 10/08/21.
//

import Alamofire

class DeviceWebService: DeviceWebServiceProtocol {
	
	private var service: RestService
	
	init(service: RestService = RestService.shared) {
		self.service = service
	}
	
	func updateFirmware(version: String, completionHandler: @escaping (DeviceFirmwareResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.firmwareUpdate)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		let request = DeviceFirmwareRequest(version: version)
		
		do {
			let jsonData = try JSONEncoder().encode(request)
			let parameters = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [String: Any]
			
			var header = Header()
			header.addHeader(key: "Authorization", value: APPURL.authToken)
			
			let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters as [String: Any])
			service.request(apiType: APIType.UPDATE_FIRMWARE_API, request: request) { (result) in
				switch result {
				case .success(let response):
					if let responseModel = try? JSONDecoder().decode(DeviceFirmwareResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
					}
				case .failure(let error):
					completionHandler(nil, error)
				}
			}
			
		} catch {
			completionHandler(nil, MovanoError.invalidRequestURL)
		}
	}
	
	func downloadDeviceFirmware(urlString: String, completionHandler: @escaping (URL?, MovanoError?) -> Void) {
		
		let destination: DownloadRequest.Destination = { _, _ in
			var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
			documentsURL.appendPathComponent("updateFirmware.zip")
			
			return (documentsURL, [.removePreviousFile, .createIntermediateDirectories])
		}
		
		let urlComponent = URLComponents(string: urlString)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		let request = NetworkRequest(url: url, method: .get)
		service.downloadFile(apiType: APIType.DOWNLOAD_FIRMWARE_API, request: request, destination: destination) { (result) in
			switch result {
			case .success(let response):
				completionHandler(response, nil)
			case .failure(let error):
				completionHandler(nil, error)
			}
		}
	}
}
