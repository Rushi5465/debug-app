//
//  DevicePresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/07/21.
//

import Foundation

protocol DevicePresenterProtocol {
	
	init(delegate: DeviceViewDelegate, firmwareService: DeviceWebServiceProtocol)
	
	func startCheckingBatteryLevel()
	func stopCheckingBatteryLevel()
	func removeDevice()
	func updateFirmware(version: String)
	func connectDevice()
	func disconnectDevice()
	func showDropDown()
	func downloadFirmware(urlString: String)
}
