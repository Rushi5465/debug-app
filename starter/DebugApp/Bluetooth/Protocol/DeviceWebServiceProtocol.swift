//
//  DeviceWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Rushikant on 10/08/21.
//

import Foundation

protocol DeviceWebServiceProtocol {
	func updateFirmware(version: String, completionHandler: @escaping (DeviceFirmwareResponseModel?, MovanoError?) -> Void)
	func downloadDeviceFirmware(urlString: String, completionHandler: @escaping (URL?, MovanoError?) -> Void)
}
