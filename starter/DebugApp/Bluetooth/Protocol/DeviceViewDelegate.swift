//
//  DeviceViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/07/21.
//

import Foundation
import DropDown

protocol DeviceViewDelegate: AnyObject {
	
	func currentBattery(percentage: Int)
	func deviceInformation(modelNumber: String)
	func deviceInformation(serialNumber: String)
	func deviceInformation(firmwareRevision: String)
	func dropDownDidShowUp(dropDown: DropDown)
	func dropDownItemSelected(_ item: String)
	func firmwareUpdateSuccess(data: DeviceFirmwareResponseModel)
	func firmwareUpdateError(error: MovanoError)
	func downloadFirmwareUpdateSuccess(_ fileUrl: URL)
	func downloadFirmwareUpdateError(_ error: MovanoError)
	func updateDeviceStatus()
	func currentChargingStatus(chargingPercentage: Int)
}
