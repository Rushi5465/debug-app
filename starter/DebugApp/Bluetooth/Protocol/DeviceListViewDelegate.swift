//
//  DeviceListViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 18/07/21.
//

import Foundation

protocol DeviceListViewDelegate: AnyObject {
	
	func devices(list: Set<PeripheralDevice>)
	func scanningStopped()
}
