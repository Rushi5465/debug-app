//
//  DeviceListPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 18/07/21.
//

import Foundation

protocol DeviceListPresenterProtocol {
	
	init(delegate: DeviceListViewDelegate)
	func startScanning(withAutoStop: Bool)
	func stopScanning()
	func connect(device: PeripheralDevice)
	func disconnect(device: PeripheralDevice)
}
