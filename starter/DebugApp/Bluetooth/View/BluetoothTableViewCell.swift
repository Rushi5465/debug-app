//
//  BluetoothTableViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/06/21.
//

import UIKit

class BluetoothTableViewCell: UITableViewCell {

	@IBOutlet weak var nameLabel: MovanoLabel!
	@IBOutlet weak var nameView: UIView!
	@IBOutlet weak var movanoIcon: UIImageView!
	@IBOutlet weak var batteryImageView: UIImageView!
	@IBOutlet weak var batteryAndChargingPower: MovanoLabel!
	
	@IBOutlet weak var connectedLabel: MovanoLabel!
	
    override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
		
		batteryImageView.image = UIImage.init(named: "batteryEmpty")
	}
}
