//
//  DeviceInfoCollectionViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 16/07/21.
//

import UIKit

class DeviceInfoCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var infoLabel: MovanoLabel!
	@IBOutlet weak var dataLabel: MovanoLabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
