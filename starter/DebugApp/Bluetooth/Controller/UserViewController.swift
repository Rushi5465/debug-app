//
//  UserViewController.swift
//  starter
//
//  Created by Rushikant on 21/01/22.
//  Copyright © 2022 Indexnine Technologies. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import MessageUI
import NotificationBannerSwift

// swiftlint:disable all
class UserViewController: UIViewController {
	
	// MARK: IBOutlets
	@IBOutlet weak var textFieldView: UIView!
	@IBOutlet weak var userNameTextfield: UITextField!
	@IBOutlet weak var submitNamrButton: UIButton!
	@IBOutlet weak var heightConstraintForTextFieldView: NSLayoutConstraint!
	
	@IBOutlet weak var dataView: UIView!
	@IBOutlet weak var sendDataButton: UIButton!
	@IBOutlet weak var deleteDataButton: UIButton!
	@IBOutlet weak var heightConstraintForDataView: NSLayoutConstraint!
	
	@IBOutlet weak var userNameLabel: UILabel!
	
	let dataDirectoryPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)  // The top level directory.
	
	var selectedDocuments = [URL]()
	var isDelete = false
	var isSend = false
	var timer = Timer()
	
	override func viewWillAppear(_ animated: Bool) {
		NotificationCenter.default.addObserver(self, selector: #selector(self.deviceDidConnect), name: NSNotification.Name("deviceDidConnect"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.showPageWithoutUser), name: NSNotification.Name("showPageWithoutUser"), object: nil)
		if KeyChain.shared.firstName != "" {
			textFieldView.isHidden = true
			dataView.isHidden = false
			heightConstraintForTextFieldView.constant = 0
			heightConstraintForDataView.constant = 180
		} else {
			textFieldView.isHidden = false
			dataView.isHidden = true
			heightConstraintForTextFieldView.constant = 143
			heightConstraintForDataView.constant = 0
		}
		userNameLabel.text = "Hey \(KeyChain.shared.firstName), welcome to the clinical trial application. Please select the below option:"
	}
	
	override func viewDidLoad() {
		userNameTextfield.delegate = self
		scanForBLEDevice()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		NotificationCenter.default.removeObserver(self, name: Notification.Name("deviceDidConnect"), object: nil)
		NotificationCenter.default.removeObserver(self, name: Notification.Name("showPageWithoutUser"), object: nil)
	}
	
	// MARK: IBActions
	
	@IBAction func submitBtnTapped(_ sender: Any) {
		if userNameTextfield.text != "" && userNameTextfield.text != nil {
			KeyChain.shared.firstName = userNameTextfield.text!
			userNameLabel.text = "Hey \(KeyChain.shared.firstName), welcome to the clinical trial application. Please select the below option:"
			
			// Create subdirectory for user (if it doesn't already exist).
			let pathString = self.dataDirectoryPath[0].path + "/\(userNameTextfield.text!)"
			do {
				try FileManager.default.createDirectory(atPath: pathString, withIntermediateDirectories: true, attributes: nil)
			} catch {
				print(error.localizedDescription)
			}
			
			// Hide textfieldView & show data view
			view.layoutIfNeeded()
			UIView.animate(withDuration: 0.2, animations: { () -> Void in
				self.textFieldView.isHidden = true
				self.heightConstraintForTextFieldView.constant = 0
			    self.view.layoutIfNeeded()
				
				UIView.animate(withDuration: 0.4, animations: { () -> Void in
					self.dataView.isHidden = false
					self.heightConstraintForDataView.constant = 180
					self.view.layoutIfNeeded()
				})
		   })
			
			NavigationUtility.setRootViewController(viewController: self)
		} else {
			showAlert(title: "Invalid UserName", message: "Please input valid name", actions: [UIAlertAction(title: "Ok", style: .default, handler: nil)])
		}
	}
	
	@IBAction func sendDataClicked(_ sender: Any) {
		isSend = true
		isDelete = false
		let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePlainText as String], in: .import )
		documentPicker.delegate = self
		documentPicker.allowsMultipleSelection = true
		present(documentPicker, animated: true, completion: nil)
	}
	
	@IBAction func deleteDataClicked(_ sender: Any) {
		isSend = false
		isDelete = true
		let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePlainText as String], in: .import )
		documentPicker.delegate = self
		documentPicker.allowsMultipleSelection = true
		present(documentPicker, animated: true, completion: nil)
	}
	
	@IBAction func startExerciseClicked(_ sender: Any) {
		if bluetoothManager.connectedPeripheral != nil {
			let controller = DebugMovanoRingViewController.instantiate(fromAppStoryboard: .debug)
			self.navigationController?.pushViewController(controller, animated: false)
		} else {
			if UserDefault.shared.connectedDevice != nil{
				self.showMyDeviceScreenPopup()
			}else{
				self.showDevicePopup()
			}
		}
	}
	
	@objc private func deviceDidConnect(notification: NSNotification) {
		guard let object = notification.object as? PeripheralDevice else {
			return
		}
		
		let banner = FloatingNotificationBanner(title: "Device Connected!", subtitle: "Connected to " + object.detail.name + ".", titleFont: UIFont.gilroy.bold(withSize: 15), titleColor: .backgroundColor, titleTextAlign: .left, subtitleFont: UIFont.gilroy.regular(withSize: 14), subtitleColor: .backgroundColor, subtitleTextAlign: .left, leftView: nil, rightView: nil, style: .success, colors: nil, iconPosition: .top)
		banner.backgroundColor = UIColor.textColor
		banner.show()
	}
	
	@objc func scanForBLEDevice(){
		if UserDefault.shared.connectedDevice != nil {
			//bluetoothManager.autoConnect = true
			Logger.shared.addLog("Found BLE device info in user defaults : \(String(describing: UserDefault.shared.connectedDevice))")
			self.timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: {[weak self] _ in
				self?.checkAndConnect()
			   })
		}
	}
	
	func checkAndConnect(){
		if(bluetoothManager.explicitDisconnection == false){
			if(bluetoothManager.isBLEConnected == false){
				bluetoothManager.startScanning()
			}
		}
	}
	
	func stopTimer() {
		timer.invalidate()
	}
	
	@objc func showPageWithoutUser() {
		view.layoutIfNeeded()
		textFieldView.isHidden = false
		dataView.isHidden = true
		heightConstraintForTextFieldView.constant = 143
		heightConstraintForDataView.constant = 0
		view.layoutIfNeeded()
	}
	
	deinit {
		stopTimer()
	}
}

extension UserViewController: UIDocumentPickerDelegate {
	func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
		guard let selectedURL = urls.first else {
			return
		}
		selectedDocuments = urls
		
		if isDelete {
			for x in 0..<selectedDocuments.count {
				// Delete a particular file
				let path = dataDirectoryPath[0].path + "/\(KeyChain.shared.firstName)" + "/\(selectedDocuments[x].lastPathComponent)"
				if FileManager.default.fileExists(atPath: path) {
					do {
						try FileManager.default.removeItem(atPath: path)
					} catch {
						print("Could not delete file at \(path), probably read-only filesystem")
					}
				} else {
					print("Could not found file at \(path)")
				}
			}
		}
		
		if isSend {
			if selectedDocuments.count < 2 {
				for x in 0..<selectedDocuments.count {
					// Delete a particular file
					let path = dataDirectoryPath[0].path + "/\(KeyChain.shared.firstName)" + "/\(selectedDocuments[x].lastPathComponent)"
					var text = ""
					if FileManager.default.fileExists(atPath: path) {
						do {
							text = try String(contentsOf: URL(fileURLWithPath: path), encoding: .utf8)
						} catch {
							print(error)
							print("Could not read file at \(path), probably read-only filesystem")
						}
						if MFMailComposeViewController.canSendMail() {
							let mail = MFMailComposeViewController()
							mail.mailComposeDelegate = self
							mail.setToRecipients(["shital.sawant@indexnine.com","rushikant.birajdar@indexnine.com"])
	//						mail.setMessageBody(text, isHTML: false)
							mail.setSubject(Date().dateToString(format: "dd-MMM-yy-HH:mm") + " Session Data")
							
							if let fileData = NSData(contentsOfFile: path) {
								print("File data loaded.")
								mail.addAttachmentData(fileData as Data, mimeType: "application/txt", fileName: selectedDocuments[x].lastPathComponent)
							} else {
								print("no file data loaded")
							}
							
							self.present(mail, animated: true)
						} else {
							let activityViewController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
							activityViewController.excludedActivityTypes = [.airDrop]
							activityViewController.popoverPresentationController?.sourceView = self.view
							self.present(activityViewController, animated: true)
						}
					} else {
						print("Could not found file at \(path)")
					}
				}
			} else {
				
				if MFMailComposeViewController.canSendMail() {
					let mail = MFMailComposeViewController()
					mail.mailComposeDelegate = self
					mail.setToRecipients(["shital.sawant@indexnine.com","rushikant.birajdar@indexnine.com"])
//						mail.setMessageBody(text, isHTML: false)
					mail.setSubject(Date().dateToString(format: "dd-MMM-yy-HH:mm") + " Session Data")
					
					for x in 0..<selectedDocuments.count {
						let path = dataDirectoryPath[0].path + "/\(KeyChain.shared.firstName)" + "/\(selectedDocuments[x].lastPathComponent)"

						if let fileData = NSData(contentsOfFile: path) {
							print("File data loaded.")
							mail.addAttachmentData(fileData as Data, mimeType: "application/txt", fileName: selectedDocuments[x].lastPathComponent)
						} else {
							print("no file data loaded")
						}
					}
					
					self.present(mail, animated: true)
				} else {
					var text:[String] = []
					for x in 0..<selectedDocuments.count {
						let path = dataDirectoryPath[0].path + "/\(KeyChain.shared.firstName)" + "/\(selectedDocuments[x].lastPathComponent)"
						if FileManager.default.fileExists(atPath: path) {
							do {
								let tempText = try String(contentsOf: URL(fileURLWithPath: path), encoding: .utf8)
								text.append(tempText)
							} catch {
								print(error)
								print("Could not read file at \(path), probably read-only filesystem")
							}
						} else {
							print("Could not found file at \(path)")
						}
					}
					let activityViewController = UIActivityViewController(activityItems: text, applicationActivities: nil)
					activityViewController.excludedActivityTypes = [.airDrop]
					activityViewController.popoverPresentationController?.sourceView = self.view
					self.present(activityViewController, animated: true)
				}
			}
			
		}
		
		isDelete = false
		isSend = false
	}
}

extension UserViewController: MFMailComposeViewControllerDelegate {
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		controller.dismiss(animated: true)
	}
}

extension UserViewController: UITextFieldDelegate {
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		return true
	}
}
