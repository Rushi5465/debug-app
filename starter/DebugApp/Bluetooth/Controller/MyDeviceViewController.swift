//
//  MyDeviceViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/06/21.
//

import UIKit
import DropDown
import Alamofire

class MyDeviceViewController: UIViewController {

	@IBOutlet weak var navigationView: UIView!
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var collectionHeight: NSLayoutConstraint!
	@IBOutlet weak var deviceName: MovanoLabel!
	
	@IBOutlet weak var statusLabel: MovanoLabel!
	@IBOutlet weak var batteryLabel: MovanoLabel!
	@IBOutlet weak var dropDownButton: MovanoButton!
	
	var presenter: DevicePresenter?
	
	var parameterList = [DeviceParameter]() {
		didSet {
			self.collectionView.reloadData()
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()

		self.registerPresenters()
		self.registerCollectionCell()
		self.assignDelegates()
		self.loadData()
		
		self.collectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		self.presenter?.stopCheckingBatteryLevel()
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.collectionHeight.constant = self.collectionView.contentSize.height
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let deviceFirmwareService = DeviceWebService()
			self.presenter = DevicePresenter(delegate: self, firmwareService: deviceFirmwareService)
			
			BluetoothManager.shared.setBluetoothService(serivce: presenter)
		}
	}
	
	func loadData() {
		self.parameterList = []
		let addedOn = DeviceParameter(info: "Added on", data: UserDefault.shared.connectedDevice?.addedOn?.dateToString(format: "MM/dd/yyyy"))
		let modelNumber = DeviceParameter(info: "Model Number", data: nil)
		let serialNumber = DeviceParameter(info: "Serial Number", data: nil)
		let firmware = DeviceParameter(info: "Firmware Number", data: nil)
		
		self.parameterList.append(contentsOf: [addedOn, modelNumber, serialNumber, firmware])
		
		self.deviceName.text = UserDefault.shared.connectedDevice?.name ?? "-"
		self.statusLabel.text = (bluetoothManager.connectedPeripheral != nil) ? "Connected" : "Not Connected"
		if !(bluetoothManager.connectedPeripheral != nil) {
			self.batteryLabel.text = "-"
		} else {
			let percentage = UserDefaults.standard.object(forKey: "batteryPercentage") as? Int
			if (percentage != 0 && percentage != nil && (self.statusLabel.text == "Connected")) {
				self.batteryLabel.text = "\(percentage!)"
			} else {
				self.batteryLabel.text = "-"
			}
		}
		
	}
	
	func registerCollectionCell() {
		let parameterCell = UINib.init(nibName: "DeviceInfoCollectionViewCell", bundle: nil)
		self.collectionView.register(parameterCell, forCellWithReuseIdentifier: DeviceInfoCollectionViewCell.IDENTIFIER)
	}
	
	func assignDelegates() {
		self.collectionView.delegate = self
		self.collectionView.dataSource = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.presenter?.fetchDeviceInformation()
		self.presenter?.startCheckingBatteryLevel()
		self.navigationController?.setNavigationBarHidden(true, animated: animated)
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
        self.presenter?.stopCheckingBatteryLevel()
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func dropDownButtonClicked(_ sender: Any) {
		self.presenter?.showDropDown()
	}
}

extension MyDeviceViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: (collectionView.bounds.width / 2.0) - 9, height: 75)
	}
	
}

extension MyDeviceViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.parameterList.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = DeviceInfoCollectionViewCell.dequeueReusableCell(collectionView: collectionView, indexPath: indexPath)
		
		let parameter = self.parameterList[indexPath.row]
		
		cell.infoLabel.text = parameter.info
		if let data = parameter.data {
			cell.dataLabel.text = data
		} else {
			cell.dataLabel.text = "-"
		}
		
		return cell
	}
}

// MARK: - MyDeviceViewDelegate
extension MyDeviceViewController: DeviceViewDelegate {
	func currentBattery(percentage: Int) {
		if percentage != 0 {
			self.batteryLabel.text = "\(percentage)%"
			UserDefaults.standard.set(percentage, forKey: "batteryPercentage")
		} else {
			let percentage = UserDefaults.standard.object(forKey: "batteryPercentage") as? Int
			self.batteryLabel.text = "\(percentage ?? 0)"
		}
//		bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
	}
	
	func currentChargingStatus(chargingPercentage: Int) {
		if chargingPercentage != 0 {
//			self.batteryLabel.text = "\(chargingPercentage)%"
//			UserDefaults.standard.set(chargingPercentage, forKey: "batteryPercentage")
		} else {
			let percentage = UserDefaults.standard.object(forKey: "batteryPercentage") as? Int
			self.batteryLabel.text = "\(percentage ?? 0)"
		}
		UserDefaults.standard.set(chargingPercentage, forKey: "isBatteryCharging")
//		bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
	}
	
	func deviceInformation(modelNumber: String) {
		self.parameterList[1].data = modelNumber
	}
	
	func deviceInformation(serialNumber: String) {
		self.parameterList[2].data = serialNumber
	}
	
	func deviceInformation(firmwareRevision: String) {
		self.parameterList[3].data = firmwareRevision
	}
	
	func dropDownItemSelected(_ item: String) {
		
		if item == "Remove Device" {
			var actions = [UIAlertAction]()
			actions.append(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
				self.presenter?.removeDevice()
				self.navigationController?.popToRootViewController(animated: true)
			}))
			actions.append(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
			self.showAlert(title: "Movano", message: "Are you sure you want to remove this device?", actions: actions)
		} else if item == "Update Firmware" {
			if let firmwareVerison = self.parameterList[3].data {
				hud.show(in: self.view)
				self.presenter?.updateFirmware(version: firmwareVerison)
			}
		} else if item == "Disconnect" {
			self.presenter?.disconnectDevice()
			UserDefaults.standard.set(0, forKey: "batteryPercentage")
		} else if item == "Connect" {
			self.presenter?.connectDevice()
		}
	}
	
	// swiftlint:disable line_length
	func dropDownDidShowUp(dropDown: DropDown) {
		let dropView = UIView(frame: CGRect(x: self.dropDownButton.frame.origin.x + self.dropDownButton.frame.width - 150, y: self.dropDownButton.frame.origin.y + self.dropDownButton.frame.height + 10, width: 150, height: 0))
		
		self.view.addSubview(dropView)
		
		dropDown.anchorView = dropView
		dropDown.show()
	}
	
	func firmwareUpdateSuccess(data: DeviceFirmwareResponseModel) {
		if data.body.version_status == version_status.uptoDate.rawValue {
			self.showAlert(title: "Device Upto Date", message: "Your device is up to date.")
			hud.dismiss()
		} else {
			hud.show(in: self.view)
			self.presenter?.downloadFirmware(urlString: data.body.latest_version_presigned_url)
		}
	}
	
	func firmwareUpdateError(error: MovanoError) {
		self.showErrorAlert(error: error)
		hud.dismiss()
	}
	
	func downloadFirmwareUpdateSuccess(_ fileUrl: URL) {
		self.presenter?.sendNewFirmwareImageToDevice(fileUrl)
		hud.dismiss()
	}
	
	func downloadFirmwareUpdateError(_ error: MovanoError) {
		hud.dismiss()
	}
	
	func updateDeviceStatus() {
		self.loadData()
	}
}
