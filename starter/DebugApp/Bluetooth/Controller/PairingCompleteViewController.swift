//
//  PairingCompleteViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/06/21.
//

import UIKit

class PairingCompleteViewController: UIViewController {

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
	}
	
	@IBAction func calibrateButtonClicked(_ sender: Any) {
		KeyChain.shared.userState = NavigationUtility.UserState.onBoarded.rawValue
		NavigationUtility.setInitialController()
	}
}
