//
//  DeviceListViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/06/21.
//

import UIKit
import CoreBluetooth

// swiftlint:disable line_length
class DeviceListViewController: UIViewController {

    @IBOutlet weak var noDeviceFound: MovanoButton!
    @IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var nextButton: StateButton!
	@IBOutlet weak var nextButtonHeight: NSLayoutConstraint!
	@IBOutlet weak var tableHeight: NSLayoutConstraint!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var scanButton: MovanoButton!
	var fromOnboarding = Bool()
	//var peripherals = Set<PeripheralDevice>()
    var peripherals: [PeripheralDevice] = []
	var connectedPeripheral: PeripheralDevice? {
		didSet {
			if connectedPeripheral != nil {
				self.nextButton.isInteractionEnabled = true
			} else {
				self.nextButton.isInteractionEnabled = false
			}
            tableView.reloadData()
		}
	}
	var presenter: DeviceListPresenter?
	let refreshControl = UIRefreshControl()
	
	func registerPresenters() {
		if self.presenter == nil {
			self.presenter = DeviceListPresenter(delegate: self)
		}
	}
	
	func registerCellToListView() {
		let menuNib = UINib.init(nibName: "BluetoothTableViewCell", bundle: nil)
		self.tableView.register(menuNib, forCellReuseIdentifier: BluetoothTableViewCell.IDENTIFIER)
	}
	
	func assignDelegate() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
        noDeviceFound.isHidden = false
		self.registerPresenters()
		self.registerCellToListView()
		self.assignDelegate()
		
		scanButton.setTitle("", for: .normal)
		scanButton.setTitle("", for: .selected)
		
		if fromOnboarding {
			self.nextButton.setTitle("Connect device", for: .normal)
		} else {
			self.nextButton.setTitle("Done", for: .normal)
		}
		
		self.presenter?.startScanning(withAutoStop: true)
		
		self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
		self.addPullToRefresh()
	}
	
	func addPullToRefresh() {
		refreshControl.tintColor = UIColor.textColor
		refreshControl.attributedTitle = NSAttributedString(string: "Re-scanning", attributes: [.foregroundColor: UIColor.textColor, .font: UIFont.gilroy.semiBold(withSize: 15)])
		refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
		self.scrollView.refreshControl = refreshControl // not required when using UITableViewController
	}
	
	@objc func refresh(_ sender: AnyObject) {
		self.presenter?.startScanning(withAutoStop: true)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.tableHeight.constant = self.tableView.contentSize.height
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		hud.textLabel.text = "Loading"
		self.presenter?.stopScanning()
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
        if let selectedDevice = connectedPeripheral{
            self.presenter?.connect(device: selectedDevice)
        }
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func nextButtonClicked(_ sender: Any) {
		let bpViewController = UserViewController.instantiate(fromAppStoryboard: .bluetooth)
//		bpViewController.action = .debug
		if let selectedDevice = connectedPeripheral{
			self.presenter?.connect(device: selectedDevice)
			self.navigationController?.pushViewController(bpViewController, animated: true)
		}
	}
	
	@IBAction func rescanDevices(_ sender: Any) {
		hud.textLabel.text = "Re-scanning"
		hud.show(in: self.view)
		self.presenter?.startScanning(withAutoStop: true)
	}
}

extension DeviceListViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(peripherals[indexPath.row].detail.state == .disconnected){
            peripherals[indexPath.row].detail.state = .connected
            self.connectedPeripheral = peripherals[indexPath.row]
			UserDefaults.standard.set(peripherals[indexPath.row].detail.batteryPercentage, forKey: "batteryPercentage")
        }else if(peripherals[indexPath.row].detail.state == .connected){
            peripherals[indexPath.row].detail.state = .disconnected
            self.connectedPeripheral = nil
			UserDefaults.standard.set(0, forKey: "batteryPercentage")
        }
        
//		if let connectedPeripheral = connectedPeripheral {
//			if connectedPeripheral != peripheral {
//				self.presenter?.disconnect(device: connectedPeripheral)
//			}
//		}
//
//		if peripheral.peripheral?.state == .connected {
//			self.presenter?.disconnect(device: peripheral)
//			UserDefaults.standard.set(0, forKey: "batteryPercentage")
//		} else {
//			self.presenter?.connect(device: peripheral)
//			UserDefaults.standard.set(peripheral.detail.batteryPercentage, forKey: "batteryPercentage")
//		}
		self.tableView.deselectRow(at: indexPath, animated: true)
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
}

extension DeviceListViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return peripherals.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = BluetoothTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
		
        let peripheral = self.peripherals[indexPath.row]
		cell.nameLabel.text = peripheral.detail.name
		
		// Divider text
		let str = NSMutableAttributedString()
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.regular(withSize: 18), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textColor]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.regular(withSize: 18), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.hexColor(hex: "#696978")]
		
		str.append(NSMutableAttributedString(string: "\(peripheral.detail.batteryPercentage)%" + "  ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
		str.append(NSMutableAttributedString(string: "|  ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		str.append(NSMutableAttributedString(string: "\(peripheral.detail.chargingPower)mW" + "  ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
		str.append(NSMutableAttributedString(string: "|  ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		str.append(NSMutableAttributedString(string: "\(peripheral.detail.rssi)dBm" + "", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
		
		cell.batteryAndChargingPower.attributedText = str
//        cell.batteryAndChargingPower.text = "\(peripheral.detail.batteryPercentage)%  |  \(peripheral.detail.chargingPower)mW  |  \(peripheral.detail.rssi)dBm"
//		cell.nameView.layer.borderWidth = 1.0
//		cell.nameView.layer.cornerRadius = 2.0
		cell.nameView.layer.masksToBounds = true
		cell.contentView.backgroundColor = UIColor.hexColor(hex: "#1E1E45")
		cell.contentView.layer.backgroundColor = UIColor.hexColor(hex: "#1E1E45")?.cgColor
		
		if peripheral.detail.state == .connected {
			cell.nameView.layer.borderColor = UIColor.primaryColor.cgColor
			cell.connectedLabel.isHidden = false
			cell.connectedLabel.layer.borderWidth = 1.0
			cell.connectedLabel.layer.cornerRadius = 2.0
			cell.connectedLabel.layer.borderColor = UIColor.batterySelect.cgColor
			cell.batteryImageView.image = UIImage.init(named: "battery-full-black-24-dp")
			cell.movanoIcon.image = UIImage.init(named: "batteryIconSelectNew")
			cell.contentView.backgroundColor = UIColor.hexColor(hex: "#282875")
			cell.contentView.layer.backgroundColor = UIColor.hexColor(hex: "#282875")?.cgColor
		} else {
			cell.nameView.layer.borderColor = UIColor.textColor.cgColor
			cell.connectedLabel.isHidden = true
			cell.batteryImageView.image = UIImage.init(named: "batteryEmpty")
			cell.contentView.backgroundColor = UIColor.hexColor(hex: "#1E1E45")
			cell.contentView.layer.backgroundColor = UIColor.hexColor(hex: "#1E1E45")?.cgColor
			cell.movanoIcon.image = UIImage.init(named: "batteryIconDeselectNew")
		}
		
		return cell
	}
}

extension DeviceListViewController: DeviceListViewDelegate {
	
	func devices(list: Set<PeripheralDevice>) {
        let peripheralsArray = Array(list)
        self.peripherals = peripheralsArray.sorted { $0.detail.name < $1.detail.name }
        self.tableView.reloadSections(IndexSet(integer: 0), with: .none)
       // self.connectedPeripheral = self.presenter?.connectedPeripheral
        if(peripherals.count > 0){
            noDeviceFound.isHidden = true
        }else{
            noDeviceFound.isHidden = false
        }
		
	}
	
	func scanningStopped() {
		hud.dismiss(animated: true)
		self.refreshControl.endRefreshing()
	}
}
