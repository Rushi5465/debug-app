//
//  BreathingRateWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 20/07/21.
//

import Foundation

protocol BreathingRateWebServiceProtocol {
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataBreathingRate]?, MovanoError?) -> Void)
	
	func fetch(by timestamp: Double, completionHandler: @escaping (CoreDataBreathingRate?, MovanoError?) -> Void)
	
	func fetchAll(completionHandler: @escaping ([CoreDataBreathingRate]?, MovanoError?) -> Void)
	
	func add(model: BreathingRate, completionHandler: @escaping (CoreDataBreathingRate?, MovanoError?) -> Void)
	
	func add(models: [BreathingRate], completionHandler: @escaping ([CoreDataBreathingRate]?, MovanoError?) -> Void)
	
	func update(model: BreathingRate, completionHandler: @escaping (CoreDataBreathingRate?, MovanoError?) -> Void)
	
	func delete(model: BreathingRate, completionHandler: @escaping (Bool?, MovanoError?) -> Void)
	
//	func fetchAverageData(forDate:String,completionHandler: @escaping (SleepAverageDataModel?, MovanoError?) -> Void)
}
