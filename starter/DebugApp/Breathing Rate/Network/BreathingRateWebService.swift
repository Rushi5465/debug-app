//
//  BreathingRateWebService.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 20/07/21.
//

import Foundation

class BreathingRateWebService: BreathingRateWebServiceProtocol {
	
	private var graphQL: ApolloClientService
	private var service: RestService
	public static var refreshTokenCount = 0
	
	init(graphQL: ApolloClientService = ApolloClientService(),service: RestService = RestService.shared) {
		self.graphQL = graphQL
		self.service = service
	}
	
	func fetch(by timestampRange: Range<Double>, completionHandler: @escaping ([CoreDataBreathingRate]?, MovanoError?) -> Void) {
        if(Utility.makeAPICall(timeIntervalRange: timestampRange) || !CoreDataBreathingRate.isReadingAvailable(range: timestampRange)){
            let query = QueryBreathingRateByTimestampRangeQuery(from_ts: timestampRange.lowerBound, to_ts: timestampRange.upperBound)
            
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
                switch result {
                case .success(let graphQLResult):
                    if let items = graphQLResult.data?.queryBreathingRateByTimestampRange?.items {
                        for each in items where each != nil {
                            CoreDataBreathingRate.add(timeInterval: each!.breathingRateTimestamp, value: each!.breathingRateValue)
                        }
                        let breathingRateData = CoreDataBreathingRate.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                        completionHandler(breathingRateData, nil)
                    } else {
                        completionHandler(nil, .invalidResponseModel)
                    }
                case .failure(let error):
                    if error.localizedDescription == "Received error response: Unauthorized" ||
                        error.localizedDescription == "Received error response: Token has expired." {
                        
                    } else {
                        
                    }
					let breathingRateData = CoreDataBreathingRate.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
					completionHandler(breathingRateData, nil)
//					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                    Logger.shared.addLog(error.localizedDescription)
                }
            }
        }else{
            let breathingRateData = CoreDataBreathingRate.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            completionHandler(breathingRateData, nil)
        }
	}
	
	func fetch(by timestamp: Double, completionHandler: @escaping (CoreDataBreathingRate?, MovanoError?) -> Void) {
		let query = GetBreathingRateQuery(breathing_rate_timestamp: timestamp)
		
		_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let items = graphQLResult.data?.getBreathingRate {
					CoreDataBreathingRate.add(timeInterval: items.breathingRateTimestamp, value: items.breathingRateValue)
					let reading = CoreDataBreathingRate.fetch(timeStamp: items.breathingRateTimestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func fetchAll(completionHandler: @escaping ([CoreDataBreathingRate]?, MovanoError?) -> Void) {
		let query = ListBreathingRatesQuery()
		
		_ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let items = graphQLResult.data?.listBreathingRates?.items {
					for each in items where each != nil {
						
						CoreDataBreathingRate.add(timeInterval: each!.breathingRateTimestamp, value: each!.breathingRateValue)
					}
					let readings = CoreDataBreathingRate.fetch()
					completionHandler(readings, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func delete(model: BreathingRate, completionHandler: @escaping (Bool?, MovanoError?) -> Void) {
		let mutation = DeleteBreathingRateMutation(DeleteBreathingRateInput: DeleteBreathingRateInput(breathingRateTimestamp: model.timestamp))
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.deleteBreathingRate {
					CoreDataBreathingRate.delete(for: Date(timeIntervalSince1970: item.breathingRateTimestamp))
					completionHandler(true, nil)
				} else {
					completionHandler(false, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func add(model: BreathingRate, completionHandler: @escaping (CoreDataBreathingRate?, MovanoError?) -> Void) {
		let mutation = CreateBreathingRateMutation(CreateBreathingRateInput: CreateBreathingRateInput(breathingRateTimestamp: model.timestamp, breathingRateValue: model.value))
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.createBreathingRate {
					CoreDataBreathingRate.add(timeInterval: item.breathingRateTimestamp, value: item.breathingRateValue)
					let reading = CoreDataBreathingRate.fetch(timeStamp: item.breathingRateTimestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func update(model: BreathingRate, completionHandler: @escaping (CoreDataBreathingRate?, MovanoError?) -> Void) {
		let mutation = UpdateBreathingRateMutation(UpdateBreathingRateInput: UpdateBreathingRateInput(breathingRateTimestamp: model.timestamp, breathingRateValue: model.value))
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let item = graphQLResult.data?.updateBreathingRate {
					CoreDataBreathingRate.add(timeInterval: item.breathingRateTimestamp, value: item.breathingRateValue)
					let reading = CoreDataBreathingRate.fetch(timeStamp: item.breathingRateTimestamp)
					completionHandler(reading, nil)
				} else {
					completionHandler(nil, .invalidResponseModel)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					
				} else {
					
				}
				completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func add(models: [BreathingRate], completionHandler: @escaping ([CoreDataBreathingRate]?, MovanoError?) -> Void) {
		var data = [CreateBreathingRateInput]()
		for each in models {
			data.append(CreateBreathingRateInput(breathingRateTimestamp: each.timestamp, breathingRateValue: each.value))
		}
		let mutation = CreateMultipleBreathingRateMutation(CreateBreathingRateInput: data)
		
		_ = self.graphQL.client!.perform(mutation: mutation) { (result) in
			switch result {
			case .success(let graphQLResult):
				if let items = graphQLResult.data?.createMultipleBreathingRate {
					for each in items where each != nil {
						CoreDataBreathingRate.add(timeInterval: each!.breathingRateTimestamp, value: each!.breathingRateValue)
					}
                    
                    if let firstItem = items.first, let timestamp = firstItem?.breathingRateTimestamp{
                        let reading = CoreDataBreathingRate.fetch(for: DateRange(Date(timeIntervalSince1970: timestamp), type: .daily).dateRange)
                        completionHandler(reading, nil)
                    }else{
                        completionHandler(nil, nil)
                    }
					
				} else {
					Logger.shared.addLog("Current time is -\(Date())")
					Logger.shared.addLog( "Data is not in [CreateMultipleBreathingRate] format")
					Logger.shared.addLog( "Graphql result of breathing rate -\(graphQLResult.data?.createMultipleBreathingRate)")
					completionHandler(nil, nil)
				}
			case .failure(let error):
				if error.localizedDescription == "Received error response: Unauthorized" ||
					error.localizedDescription == "Received error response: Token has expired." {
					if BreathingRateWebService.refreshTokenCount == 0 {
						BreathingRateWebService.refreshTokenCount = 1
						self.requestAccessToken { [weak self] response, error in
							if (response != nil) {
								self?.graphQL.setDefaultClient()
								self?.add(models: models, completionHandler: completionHandler)
							}
							if let error = error {
								completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
							}
						}
					}
				} else {
					completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
				}
				Logger.shared.addLog(error.localizedDescription)
			}
		}
	}
	
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
