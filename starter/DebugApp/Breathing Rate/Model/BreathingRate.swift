//
//  BreathingRate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 08/09/21.
//

import Foundation


public class BreathingRate:NSObject, NSCoding {
    public var timestamp: TimeInterval = 0
    public var value: Double = 0.0
    
    enum Key:String{
        case timestamp = "timestamp"
        case value = "value"
    }
    
    public override init() {
        super.init()
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(timestamp,forKey: Key.timestamp.rawValue)
        aCoder.encode(value,forKey: Key.value.rawValue)

    }
  
    required public convenience init(coder aDecoder: NSCoder)  {
        let mTimestamp = aDecoder.decodeDouble(forKey: Key.timestamp.rawValue)
        let mValue = aDecoder.decodeDouble(forKey: Key.value.rawValue)
        self.init(timeStamp:mTimestamp, value:mValue)
    }
    
    init(timeStamp:TimeInterval, value:Double) {
        self.timestamp = timeStamp
        self.value = value
    }
}

public class BreathingRateCoreData:NSObject, NSCoding{
    public var breathingRate : [BreathingRate] = []
    
    enum Key:String{
        case breathingRate = "breathingRate"
    }
    
    init(breathingRate:[BreathingRate]){
        self.breathingRate = breathingRate
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(breathingRate,forKey: Key.breathingRate.rawValue)
    }
    
    required public convenience init(coder aDecoder: NSCoder)  {
        let mBreathingRate = aDecoder.decodeObject(forKey: Key.breathingRate.rawValue) as! [BreathingRate]
        self.init(breathingRate:mBreathingRate)
    }
}

