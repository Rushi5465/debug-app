//
//  PersonalDetailPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

class PersonalDetailPresenter: PersonalDetailPresenterProtocol {
	
	private var webservice: UserServiceProtocol
	private weak var delegate: PersonalDetailViewDelegate?
	
	required init(webservice: UserServiceProtocol, delegate: PersonalDetailViewDelegate) {
		self.webservice = webservice
		self.delegate = delegate
	}
	
	func postUserDetails(userDetail: UserDetails) {
		webservice.postUserDetails(userDetail: userDetail) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.postUserDetailErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.postUserDetailSuccess(with: response)
			}
		}
	}
}
