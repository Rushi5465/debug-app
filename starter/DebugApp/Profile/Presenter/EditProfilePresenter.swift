//
//  EditProfilePresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/21.
//

import Foundation

class EditProfilePresenter: EditProfilePresenterProtocol {
	
	private weak var delegate:  EditProfileViewDelegate?
	private var webservice: UserServiceProtocol
	
	required init(webservice: UserServiceProtocol, delegate: EditProfileViewDelegate) {
		self.webservice = webservice
		self.delegate = delegate
	}
	
	func updateUserInformation(userDetail: UserDetails) {
		webservice.postUserDetails(userDetail: userDetail) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.updateUserInformationErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.updateUserInformationSuccess(with: response)
			}
		}
	}
}
