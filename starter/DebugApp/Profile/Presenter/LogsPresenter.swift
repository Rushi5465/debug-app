//
//  LogsPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/07/21.
//

import Foundation
import MessageUI

class LogsPresenter: NSObject, LogsPresenterProtocol {
	
	private weak var delegate:  LogsViewDelegate?
	
	required init(delegate: LogsViewDelegate) {
		self.delegate = delegate
	}
	
	func fetchLogs() {
		self.delegate?.fetch(logs: Logger.shared.messages)
	}
	
	func shareLogs() {
		if MFMailComposeViewController.canSendMail() {
			let mail = MFMailComposeViewController()
			mail.mailComposeDelegate = self
			mail.setToRecipients(["shital.sawant@indexnine.com","rushikant.birajdar@indexnine.com"])
			mail.setMessageBody(Logger.shared.printLogs(), isHTML: false)
			mail.setSubject(Date().dateToString(format: "dd-MMM-yy-HH:mm") + " Movano Logs")

			self.delegate?.share(card: mail, canSendMail: true)
		} else {
			let activityViewController = UIActivityViewController(activityItems: [Logger.shared.printLogs()], applicationActivities: nil)
			activityViewController.excludedActivityTypes = [.airDrop]
			self.delegate?.share(card: activityViewController, canSendMail: false)
		}
	}
}

extension LogsPresenter: MFMailComposeViewControllerDelegate {
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		controller.dismiss(animated: true)
	}
}
