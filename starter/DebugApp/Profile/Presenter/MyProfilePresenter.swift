//
//  MyProfilePresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit

class MyProfilePresenter: MyProfilePresenterProtocol {
	
	private var webservice: UserServiceProtocol
	private weak var delegate: MyProfileViewDelegate?
	
	required init(webservice: UserServiceProtocol, delegate: MyProfileViewDelegate) {
		self.webservice = webservice
		self.delegate = delegate
	}
	
	func fetchData() {
		
		var section = [ProfileScreenModel]()
		let profileModel = ProfileModel(name: KeyChain.shared.fullName, email: KeyChain.shared.email!)
		section.append(ProfileScreenModel(data: profileModel, cellType: .profile))
		section.append(ProfileScreenModel(data: "My Details", cellType: .detailCell))
		section.append(ProfileScreenModel(data: "Goals", cellType: .detailCell))
		
		var securityString = ""
		if BiometricAuthentication.shared.isTouchIDSupported {
			securityString = "Use Touch ID"
		} else if BiometricAuthentication.shared.isFaceIDSupported {
			securityString = "Use Face ID"
		}
		section.append(ProfileScreenModel(data: securityString, cellType: .switchCell))
		
		if ServerManager.shared.currentServer.type != .prod {
			section.append(ProfileScreenModel(data: "Logs", cellType: .detailCell))
			section.append(ProfileScreenModel(data: "CLI Tool", cellType: .detailCell))
		}
		
		section.append(ProfileScreenModel(data: "Send Feedback", cellType: .detailCell))
		
		self.delegate?.fetchList(list: section)
	}
	
	func fetchUserInformation() {
		webservice.fetchUserDetails { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.fetchErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.fetchSuccess(with: response)
			}
		}
	}
	
	func logoutUser() {
        dataUploadJobManager.cancelAllOperations()
		accessTokenJobManager.cancelAllOperations()
		clearUserData()
		DateManager.current.resetManager()
		KeyChain.shared.userState =  NavigationUtility.UserState.logout.rawValue
		NavigationUtility.setInitialController()
	}
	
	private func clearUserData() {
		KeyChain.shared.clearTokens()
		UserDefault.shared.deleteAllUserDefault()
		CoreDataHandler.deleteAllCoreData()
	}
}
