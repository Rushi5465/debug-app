//
//  ProfileDetailPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/21.
//

import Foundation

class ProfileDetailPresenter: ProfileDetailPresenterProtocol {
	
	private weak var delegate: ProfileDetailViewDelegate?
	private var webservice: UserServiceProtocol
	
	required init(webservice: UserServiceProtocol, delegate: ProfileDetailViewDelegate) {
		self.webservice = webservice
		self.delegate = delegate
	}
	
	func fetchUserInformation() {
		webservice.fetchUserDetails { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.fetchErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.fetchSuccess(with: response)
			}
		}
	}
	
	func deleteUser() {
		webservice.deleteUser { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.deleteErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.deleteSuccess(with: response)
			}
		}
	}
	
	func fetchList(userDetail: UserDetails) {
		
		var aboutItems = [ProfileDetailItem]()
		aboutItems.append(ProfileDetailItem(key: "First name", value: userDetail.firstName))
		aboutItems.append(ProfileDetailItem(key: "Last name", value: userDetail.lastName))
		let about = ProfileDetailModel(title: "About", items: aboutItems)
		
		var personalItems = [ProfileDetailItem]()
		personalItems.append(ProfileDetailItem(key: "Date of Birth", value: userDetail.dateOfBirth))
		personalItems.append(ProfileDetailItem(key: "Gender", value: userDetail.gender))
		personalItems.append(ProfileDetailItem(key: "Height", value: "\(userDetail.heightInInches ?? 0)".formattedHeight))
		personalItems.append(ProfileDetailItem(key: "Weight", value: "\(userDetail.weight ?? 0)".formattedWeight))
		let personalInfo = ProfileDetailModel(title: "Personal details", items: personalItems)
		
		let changePwd = ProfileDetailModel(title: "Change password", items: nil)
		self.delegate?.userInformation(list: [about, personalInfo, changePwd])
	}
}
