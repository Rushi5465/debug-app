//
//  FeedbackPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 17/08/21.
//

import Foundation
import MessageUI

class FeedbackPresenter: NSObject, FeedbackPresenterProtocol {
	
	private weak var delegate: FeedbackViewDelegate?
	
	required init(delegate: FeedbackViewDelegate) {
		self.delegate = delegate
	}
	
	func shareFeedback(subject: String, message: String) {
		if MFMailComposeViewController.canSendMail() {
			let mail = MFMailComposeViewController()
			mail.mailComposeDelegate = self
			mail.setToRecipients(["info@movano.com"])
			mail.setSubject(subject)
			mail.setMessageBody(message, isHTML: true)
			
			self.delegate?.shareFeedback(result: .success(mail))
		} else {
			let error = MovanoError.canNotSendMail
			self.delegate?.shareFeedback(result: .failure(error))
		}
	}
}

extension FeedbackPresenter: MFMailComposeViewControllerDelegate {
	
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		
		var movanoError: MovanoError?
		if let error = error {
			movanoError = MovanoError.failedRequest(description: error.localizedDescription)
		}
		self.delegate?.feedbackSent(didFinishWith: result, error: movanoError)
		controller.dismiss(animated: true)
	}
}
