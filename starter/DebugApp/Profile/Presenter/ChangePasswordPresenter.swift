//
//  ChangePasswordPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

class ChangePasswordPresenter: ChangePasswordPresenterProtocol {
	
	private var webservice: AuthorizationWebServiceProtocol
	private weak var delegate: ChangePasswordViewDelegate?
	
	required init(webservice: AuthorizationWebServiceProtocol, delegate: ChangePasswordViewDelegate) {
		self.webservice = webservice
		self.delegate = delegate
	}
	
	func changePasswordLink(request: ChangePasswordRequestModel) {
//		webservice.changePasswordLink(request: request) { [weak self] (response, error) in
//			if let error = error {
//				self?.delegate?.changePasswordErrorHandler(error: error)
//				return
//			}
//
//			if let response = response {
//				self?.delegate?.changePasswordSuccess(with: response)
//			}
//		}
	}
}
