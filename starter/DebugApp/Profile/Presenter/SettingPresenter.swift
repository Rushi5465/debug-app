//
//  SettingPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 22/07/21.
//

import Foundation

class SettingPresenter: SettingPresenterProtocol {
	
	private weak var delegate: SettingViewDelegate?
	var cacheServiceService: CacheTechniqueWebService
	
	required init(delegate: SettingViewDelegate, cacheServiceService: CacheTechniqueWebService) {
		self.cacheServiceService = cacheServiceService
		self.delegate = delegate
	}
	
	func fetchInformation() {
		
		let profile = ProfileMenuModel(title: "My Profile", image: #imageLiteral(resourceName: "MyProfile"), type: .profile)
		let bpLog = ProfileMenuModel(title: "BP Log", image: #imageLiteral(resourceName: "BPLog"), type: .bpLog)
		let serverList = ProfileMenuModel(title: "Server List", image: #imageLiteral(resourceName: "ServerList"), type: .environment)
		let cacheData = ProfileMenuModel(title: "Download Data", image: #imageLiteral(resourceName: "DownloadData"), type: .cacheData)
		
		self.delegate?.fetchSettingList(list: [profile, bpLog, serverList, cacheData])
	}
}
