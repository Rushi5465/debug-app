//
//  UserService.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 29/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import SwiftyJSON
import KeychainSwift

class UserService: RestService {
	
	public class func loginUser(_ restService: RestService = .shared, username: String, password: String, completion : @escaping (Result<JSON, Error>) -> Void) {
		let parameters = ["username": username, "password": password, "device_id": "1234"]
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.login)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		
		let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters as [String: Any])
		restService.request(apiType: APIType.LOGIN_USER, request: request) { (result) in
			switch result {
				case .success(let response):
					let mfaEnabled = response["data"]["MFA_enabled"].boolValue
					KeyChain.shared.mfa = mfaEnabled
					if mfaEnabled == true {
						let userResponse = LoginResponse(json: response["data"])
						KeyChain.shared.saveLoginResponse(response: userResponse)
					} else {
						let userSession = UserSession(json: response["data"])
						KeyChain.shared.saveUserSession(response: userSession)
					}
					completion(.success("success"))
				case .failure(let error):
					completion(.failure(error))
			}
		}
	}
}
