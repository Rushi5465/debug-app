//
//  ProfileService.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

class ProfileService: UserServiceProtocol {
	
	private var service: RestService
	
	init(service: RestService = RestService.shared) {
		self.service = service
	}
	
	func fetchUserDetails(completionHandler: @escaping (UserResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getUserData)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		var header = Header()
		header.addHeader(key: "Authorization", value: KeyChain.shared.idToken)
		header.addHeader(key: "AccessToken", value: KeyChain.shared.accessToken!)
		
		let request = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.GET_USER_DATA, request: request) { (result) in
			switch result {
				case .success(let response):
                   do {
                     let responseModel = try JSONDecoder().decode(UserResponseModel.self, from: response.rawData())
                     completionHandler(responseModel, nil)
                    } catch let error {
                        print("\(error)")
                        completionHandler(nil, MovanoError.invalidResponseModel)
                    }
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
	
	func deleteUser(completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.delete)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		var header = Header()
		header.addHeader(key: "Authorization", value: KeyChain.shared.idToken)
		header.addHeader(key: "AccessToken", value: KeyChain.shared.accessToken!)
		
		let request = NetworkRequest(url: url, method: .delete, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.DELETE_USER, request: request) { (result) in
			switch result {
				case .success(let response):
					if let responseModel = try? JSONDecoder().decode(EmptyResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
	
	func postUserDetails(userDetail: UserDetails, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void) {
		
		do {
			let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.sendUserData)!
			
			guard let url = urlComponent.url else {
				completionHandler(nil, MovanoError.invalidRequestURL)
				return
			}
			
			let jsonData = try JSONEncoder().encode(userDetail)
			let parameters = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [String: Any]
			
			var header = Header()
			header.addHeader(key: "Authorization", value: KeyChain.shared.idToken)
			header.addHeader(key: "AccessToken", value: KeyChain.shared.accessToken!)
			
			let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters)
			
			service.request(apiType: APIType.UPDATE_USER_DATA, request: request) { (result) in
				switch result {
					case .success(let response):
						if let responseModel = try? JSONDecoder().decode(EmptyResponseModel.self, from: response.rawData()) {
							completionHandler(responseModel, nil)
						} else {
							completionHandler(nil, MovanoError.invalidResponseModel)
					}
					case .failure(let error):
						completionHandler(nil, error)
				}
			}
		} catch {
			
		}
		
	}
}
