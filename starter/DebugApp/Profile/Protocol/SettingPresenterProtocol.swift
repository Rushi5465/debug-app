//
//  SettingPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 22/07/21.
//

import Foundation

protocol SettingPresenterProtocol {
	init(delegate: SettingViewDelegate, cacheServiceService: CacheTechniqueWebService)
	func fetchInformation()
}
