//
//  ChangePasswordViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol ChangePasswordViewDelegate: AnyObject {
	func changePasswordSuccess(with responseModel: EmptyResponseModel)
	func changePasswordErrorHandler(error: MovanoError)
	
}
