//
//  ProfileDetailViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/21.
//

import Foundation

protocol ProfileDetailViewDelegate: AnyObject {
	func userInformation(list: [ProfileDetailModel])
	func fetchSuccess(with responseModel: UserResponseModel)
	func fetchErrorHandler(error: MovanoError)
	func deleteSuccess(with responseModel: EmptyResponseModel)
	func deleteErrorHandler(error: MovanoError)
}
