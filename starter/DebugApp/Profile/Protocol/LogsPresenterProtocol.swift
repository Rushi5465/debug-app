//
//  LogsPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/07/21.
//

import Foundation

protocol LogsPresenterProtocol {
	
	init(delegate: LogsViewDelegate)
}
