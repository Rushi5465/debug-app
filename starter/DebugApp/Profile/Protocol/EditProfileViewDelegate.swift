//
//  EditProfileViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/21.
//

import Foundation

protocol EditProfileViewDelegate: AnyObject {
	func updateUserInformationSuccess(with responseModel: EmptyResponseModel)
	func updateUserInformationErrorHandler(error: MovanoError)
}
