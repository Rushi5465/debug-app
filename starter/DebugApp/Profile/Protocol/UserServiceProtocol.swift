//
//  UserServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol UserServiceProtocol {
	func fetchUserDetails(completionHandler: @escaping (UserResponseModel?, MovanoError?) -> Void)
	func deleteUser(completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void)
	func postUserDetails(userDetail: UserDetails, completionHandler: @escaping (EmptyResponseModel?, MovanoError?) -> Void)
	
}
