//
//  PersonalDetailPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol PersonalDetailPresenterProtocol {
	init(webservice: UserServiceProtocol, delegate: PersonalDetailViewDelegate)
	func postUserDetails(userDetail: UserDetails)
}
