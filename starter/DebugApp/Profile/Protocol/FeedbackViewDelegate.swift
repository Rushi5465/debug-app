//
//  FeedbackViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 17/08/21.
//

import Foundation
import UIKit
import MessageUI

protocol FeedbackViewDelegate: AnyObject {
	
	func shareFeedback(result: Result<UIViewController, MovanoError>)
	func feedbackSent(didFinishWith result: MFMailComposeResult, error: MovanoError?)
}
