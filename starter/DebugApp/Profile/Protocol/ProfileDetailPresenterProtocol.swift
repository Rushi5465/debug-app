//
//  ProfileDetailPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/21.
//

import Foundation

protocol ProfileDetailPresenterProtocol {
	
	init(webservice: UserServiceProtocol, delegate: ProfileDetailViewDelegate)
	func fetchList(userDetail: UserDetails)
	func fetchUserInformation()
	func deleteUser()
}
