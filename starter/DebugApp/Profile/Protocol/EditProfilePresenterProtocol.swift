//
//  EditProfilePresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/21.
//

import Foundation

protocol EditProfilePresenterProtocol {
	
	init(webservice: UserServiceProtocol, delegate: EditProfileViewDelegate)
	func updateUserInformation(userDetail: UserDetails)
}
