//
//  ChangePasswordPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol ChangePasswordPresenterProtocol {
	init(webservice: AuthorizationWebServiceProtocol, delegate: ChangePasswordViewDelegate)
	func changePasswordLink(request: ChangePasswordRequestModel)
}
