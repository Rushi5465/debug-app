//
//  PersonalDetailViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol PersonalDetailViewDelegate: AnyObject {
	func postUserDetailSuccess(with responseModel: EmptyResponseModel)
	func postUserDetailErrorHandler(error: MovanoError)
	
}
