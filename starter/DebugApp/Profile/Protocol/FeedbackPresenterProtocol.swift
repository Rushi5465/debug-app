//
//  FeedbackPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 17/08/21.
//

import Foundation

protocol FeedbackPresenterProtocol {
	
	init(delegate: FeedbackViewDelegate)
	
	func shareFeedback(subject: String, message: String)
}
