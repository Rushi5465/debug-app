//
//  LogsViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 29/07/21.
//

import Foundation
import UIKit

protocol LogsViewDelegate: AnyObject {
	
	func fetch(logs: [Logs])
	func share(card: UIViewController, canSendMail: Bool)
}
