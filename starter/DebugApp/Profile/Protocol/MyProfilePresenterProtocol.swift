//
//  MyProfilePresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol MyProfilePresenterProtocol {
	
	init(webservice: UserServiceProtocol, delegate: MyProfileViewDelegate)
	func fetchData()
	func logoutUser()
	func fetchUserInformation()
}
