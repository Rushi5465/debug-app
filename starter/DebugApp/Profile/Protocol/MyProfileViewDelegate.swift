//
//  MyProfileViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

protocol MyProfileViewDelegate: AnyObject {

	func fetchSuccess(with responseModel: UserResponseModel)
	func fetchErrorHandler(error: MovanoError)
	func fetchList(list: [ProfileScreenModel])
}
