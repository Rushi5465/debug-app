//
//  ProfileDetailModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/21.
//

import Foundation

struct ProfileDetailModel {
	
	var title: String
	var items: [Any]?
}

struct ProfileDetailItem {
	
	var key: String
	var value: String?
}
