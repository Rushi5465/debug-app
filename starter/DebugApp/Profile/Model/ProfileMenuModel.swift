//
//  ProfileMenuModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 22/07/21.
//

import UIKit

struct ProfileMenuModel {
	
	var title: String
	var image: UIImage
	var type: ProfileMenuItem
}

enum ProfileMenuItem {
	case profile
	case myDevice
	case trend
	case bpLog
	case environment
	case cacheData
}
