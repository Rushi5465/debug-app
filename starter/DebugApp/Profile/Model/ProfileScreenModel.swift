//
//  ProfileScreenModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 27/10/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

struct ProfileScreenModel {
	
	var data: Any
	var cellType: ProfileCellType
}

struct ProfileModel {
	
	var name: String
	var email: String
}

enum ProfileCellType {
	
	case switchCell
	case profile
	case detailCell
}
