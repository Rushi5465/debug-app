//
//  ChangePasswordRequestModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

struct ChangePasswordRequestModel: Encodable {
	
	let old_password: String
	let new_password: String
	
}
