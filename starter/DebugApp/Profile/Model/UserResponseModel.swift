//
//  UserResponseModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/08/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation

struct UserResponseModel: Codable {
	var message: String
	var data: UserDetails
	var status_code: Int
    
    
}
