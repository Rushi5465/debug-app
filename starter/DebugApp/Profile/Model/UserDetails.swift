//
//  UserDetails.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 23/12/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserDetails: Codable {
	var dateOfBirth: String?
	var height: String?
	var heightInInches: Int?
	var weight: Int?
	var weightUnits: String?
	var gender: String?
	var lowGlucoseValue: Int?
	var highGlucoseValue: Int?
	var email: String?
	var firstName: String?
	var lastName: String?
	var phoneNumber: String?
	var usernameForSrp: String?
	var userId: String?
    var deviceId: String?
	var baselineBodyTemprature: Double?
    
	enum CodingKeys: String, CodingKey {
		case dateOfBirth = "birthdate"
		case heightInInches = "height_inch"
		case weight = "weight"
		case weightUnits = "weight_unit"
		case gender = "gender"
		case lowGlucoseValue = "low_blood_glucose"
		case highGlucoseValue = "high_blood_glucose"
		case phoneNumber = "phone_number"
		case email = "email"
		case firstName = "first_name"
		case lastName = "last_name"
		case usernameForSrp = "username_for_srp"
		case userId = "user_id"
        case deviceId = "device_id"
		case baselineBodyTemprature = "baseline_body_temprature"
	}
	
	init() {
		
	}
}

extension UserDetails {
	convenience init(json: JSON) {
		self.init()
		dateOfBirth = json["birthdate"].stringValue
		height = json["height_inch"].stringValue
		heightInInches = json["height_inch"].int
		weight = json["weight"].int
		weightUnits = json["weight_unit"].stringValue
		gender = json["gender"].stringValue
		lowGlucoseValue = json["low_blood_glucose"].int
		highGlucoseValue = json["high_blood_glucose"].int
		email = json["email"].stringValue
		firstName = json["first_name"].stringValue
		lastName = json["last_name"].stringValue
		phoneNumber = json["phone_number"].stringValue
		usernameForSrp = json["username_for_srp"].stringValue
		userId = json["user_id"].stringValue
        deviceId = json["device_id"].stringValue
		baselineBodyTemprature = json["baseline_body_temprature"].double
	}
}
