//
//  LogsViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 11/07/21.
//

import UIKit
import MessageUI

class LogsViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var tableHeight: NSLayoutConstraint!
	@IBOutlet weak var clearLogButton: MovanoButton!
	
	var presenter: LogsPresenter?
	
	var logs = [Logs]() {
		didSet {
			self.tableView.reloadData()
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()

		registerPresenters()
		registerCellToListView()
		assignDelegate()
		
		self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
		
		clearLogButton.clipsToBounds = true
		clearLogButton.layer.cornerRadius = 4.0
		clearLogButton.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.tableHeight.constant = self.tableView.contentSize.height
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			self.presenter = LogsPresenter(delegate: self)
		}
	}
	
	func registerCellToListView() {
		let logNib = UINib.init(nibName: "LogsTableViewCell", bundle: nil)
		self.tableView.register(logNib, forCellReuseIdentifier: LogsTableViewCell.IDENTIFIER)
	}
	
	func assignDelegate() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.presenter?.fetchLogs()
	}

	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func shareButtonClicked(_ sender: Any) {
		self.presenter?.shareLogs()
	}
	
	@IBAction func clearLogsTapped(_ sender: Any) {
		Logger.shared.messages = []
		self.logs = []
	}
	
}

// MARK: - UITableViewDelegate
extension LogsViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
}

// MARK: - UITableViewDataSource
extension LogsViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.logs.count
	}
	
	func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
		return false
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let item = logs[indexPath.row]
		
		let cell = LogsTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
		cell.titleLabel.text = Date(timeIntervalSince1970: item.timeStamp).dateToString(format: "dd MMM yy hh:mm:ss a") + " - "  + item.message
		return cell
	}
}

// MARK: - LogsViewDelegate
extension LogsViewController: LogsViewDelegate {
	
	func fetch(logs: [Logs]) {
		self.logs = logs
	}
	
	func share(card: UIViewController, canSendMail: Bool) {
		if !canSendMail {
			card.popoverPresentationController?.sourceView = self.view
		}
		self.present(card, animated: true)
	}
}
