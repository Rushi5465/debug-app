//
//  PersonalDetailsViewController+Extension.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/06/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import Foundation
import UIKit

extension PersonalDetailsViewController {
	
	func manageFieldPadding() {
		heightTextField.addPadding(.left(10))
		dateOfBirth.addPadding(.left(10))
		weightField.addPadding(.left(10))
		genderField.addPadding(.left(10))
	}
	
	func assignBirthFieldTask() {
		dateOfBirth.setInputView(isDatePicker: true, target: self, selector: #selector(tapDone))
	}
	
	func assignDelegate() {
		dateOfBirth.delegate = self
		genderField.delegate = self
		weightField.delegate = self
		heightTextField.delegate = self
		heightPicker.delegate = self
		heightPicker.dataSource = self
		heightPicker.toolbarDelegate = self
		genderPicker.delegate = self
		genderPicker.dataSource = self
		genderPicker.toolbarDelegate = self
	}
	
	func assignInputToFields() {
		heightTextField.inputView = heightPicker
		heightTextField.inputAccessoryView = heightPicker.toolbar
		genderField.inputView = genderPicker
		genderField.inputAccessoryView = genderPicker.toolbar
		weightField.showToolBar()
	}
	
	func manageUI() {
		dateOfBirth.addBottomBorder()
		genderField.addBottomBorder()
		heightTextField.addBottomBorder()
		weightField.addBottomBorder()
	}
	
	func getDataFromKeychain() {
		self.dateOfBirth.text = KeyChain.shared.dateOfBirth
		self.heightTextField.text = KeyChain.shared.height?.formattedHeight
		self.weightField.text = KeyChain.shared.weight?.formattedWeight
	}
	
	func performNextButtonAction() {
		if dateOfBirth.text?.isEmpty == true || heightTextField.text?.isEmpty == true || weightField.text?.isEmpty == true || genderField.text?.isEmpty == true {
			self.showAlert(title: "Movano", message: "Please fill all the details")
		} else if weightField.text?.toDouble == 0 {
			self.showAlert(title: "Movano", message: "Please put a valid weight value")
		} else {
			//API Call
			hud.show(in: self.view)
			let details = UserDetails()
			details.dateOfBirth = dateOfBirth.text
			details.heightInInches = Int(Utility.getHeightInInches(height: heightTextField.text!))
			details.gender = genderField.text!
			details.weight = Int(weightField.text?.weightInLbs ?? "0")
			
			self.details = details
			self.presenter?.postUserDetails(userDetail: details)
		}
	}
	
	@objc func tapDone() {
		if let datePicker = dateOfBirth.inputView as? UIDatePicker {
			let dateformatter = DateFormatter()
			dateformatter.dateStyle = .medium
			dateformatter.dateFormat = DateFormat.birthdateFormat
			dateOfBirth.text = dateformatter.string(from: datePicker.date)
		}
		dateOfBirth.resignFirstResponder()
	}
}
