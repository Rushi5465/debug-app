//
//  ChangePasswordViewController.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 22/01/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
	@IBOutlet weak var oldPassword: MovanoTextField!
	@IBOutlet weak var newPassword: PasswordTextField!
	@IBOutlet weak var retypePassword: MovanoTextField!
	
	@IBOutlet weak var mincharButton: MovanoButton!
	@IBOutlet weak var upperCaseButton: MovanoButton!
	@IBOutlet weak var lowerCaseButton: MovanoButton!
	@IBOutlet weak var digitButton: MovanoButton!
	@IBOutlet weak var specialButton: MovanoButton!
	@IBOutlet weak var passwordRuleHeading: MovanoLabel!
	
	var presenter: ChangePasswordPresenter?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if presenter == nil {
			let authService = AuthorizationWebService()
			self.presenter = ChangePasswordPresenter(webservice: authService, delegate: self)
		}
		
		manageFieldPadding()
		manageUI()
	}
	
	func manageUI() {
		oldPassword.addBottomBorder()
		newPassword.addBottomBorder()
		retypePassword.addBottomBorder()
		self.passwordRuleHeading.addImage(image: #imageLiteral(resourceName: "checkmarkgreen-all"))
	}
	
	func manageFieldPadding() {
		oldPassword.addPadding(.left(10))
		newPassword.addPadding(.left(10))
		retypePassword.addPadding(.left(10))
	}
	
	fileprivate func managePasswordRules(_ prospectiveText: String) -> Bool {
		if prospectiveText.count >= 8 {
			mincharButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			mincharButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		
		if prospectiveText.hasUpperCase {
			upperCaseButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			upperCaseButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		if prospectiveText.hasLowerCase {
			lowerCaseButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			lowerCaseButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		if prospectiveText.hasDigit {
			digitButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			digitButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		if prospectiveText.hasSpecialCharacter {
			specialButton.setImage(#imageLiteral(resourceName: "checkmarkgreen"), for: .normal)
		} else {
			specialButton.setImage(#imageLiteral(resourceName: "close-black-24-dp"), for: .normal)
		}
		
		mincharButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (mincharButton.bounds.width - 25), bottom: 0, right: 5)
		mincharButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((mincharButton.imageView?.frame.width)! + 25))
		
		upperCaseButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (upperCaseButton.bounds.width - 25), bottom: 0, right: 5)
		upperCaseButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((upperCaseButton.imageView?.frame.width)! + 25))
		
		lowerCaseButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (lowerCaseButton.bounds.width - 25), bottom: 0, right: 5)
		lowerCaseButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((lowerCaseButton.imageView?.frame.width)! + 25))
		
		digitButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (digitButton.bounds.width - 25), bottom: 0, right: 5)
		digitButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((digitButton.imageView?.frame.width)! + 25))
		
		specialButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: (specialButton.bounds.width - 25), bottom: 0, right: 5)
		specialButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((specialButton.imageView?.frame.width)! + 25))
		
		if !(!prospectiveText.isEmpty && prospectiveText.isValidPassword) {
			return false
		}
		return true
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func saveButtonClicked(_ sender: Any) {
		
		if newPassword.text != retypePassword.text {
			self.showAlert(title: "Error", message: "Password and New Password should be same.")
			return
		}
		
		let newValid = managePasswordRules(newPassword.text!)
		if newValid {
			hud.show(in: self.view)
			let request = ChangePasswordRequestModel(old_password: oldPassword.text!, new_password: newPassword.text!)
			self.presenter?.changePasswordLink(request: request)
		}
	}
}

// MARK: UITextFieldDelegate
extension ChangePasswordViewController: UITextFieldDelegate {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		
		let currentText = textField.text ?? ""
		let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
		
		if textField == newPassword {
			_ = managePasswordRules(prospectiveText)
		}
		return true
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		let nextTag = textField.tag + 1
		if let nextResponder = textField.superview?.viewWithTag(nextTag) {
			nextResponder.becomeFirstResponder()
		} else {
			textField.resignFirstResponder()
		}
		return true
	}
	
	func textFieldShouldClear(_ textField: UITextField) -> Bool {
		return true
	}
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
	}
}

extension ChangePasswordViewController: ChangePasswordViewDelegate {
	
	func changePasswordSuccess(with responseModel: EmptyResponseModel) {
		KeyChain.shared.currentUser?.password = newPassword.text!
		hud.dismiss()
		
		var actions = [UIAlertAction]()
		actions.append(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
			self.navigationController?.popViewController(animated: true)
		}))
		
		self.showAlert(title: "Success", message: "Password has been changed successfully.", actions: actions)
		
	}
	
	func changePasswordErrorHandler(error: MovanoError) {
		hud.dismiss()
		self.showErrorAlert(error: error)
	}
}
