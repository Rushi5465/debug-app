//
//  EditProfileViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/21.
//

import UIKit

class EditProfileViewController: UIViewController {

	@IBOutlet weak var backButton: MovanoButton!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var firstNameField: MovanoTextField!
	@IBOutlet weak var lastNameField: MovanoTextField!
	@IBOutlet weak var dobField: MovanoTextField!
	@IBOutlet weak var genderField: MovanoTextField!
	@IBOutlet weak var heightField: MovanoTextField!
	@IBOutlet weak var weightField: MovanoTextField!
	
	let feetRange = [Int](1...8)
	let inchRange = [Int](0...11)
	let genderRange = ["Female", "Male", "Other"]
	let heightPicker = ToolbarPickerView()
	let genderPicker = ToolbarPickerView()
	var selectedHeight: Float!
	var selectedGender: String = ""
	var selectedWeight: Float!
	
	var activeTextField = UITextField()
	
	var userDetails: UserDetails!
	var presenter: EditProfilePresenter?
	
	var delegate: UpdateUserDetailsProtocol? = nil
	
	override func viewDidLoad() {
		super.viewDidLoad()

		self.registerPresenters()
		self.loadData()
		self.setupIcons()
		
		dismissKeyboardGesture()
		
		assignBirthFieldTask()
		manageFieldPadding()
		assignDelegate()
		assignInputToFields()
		manageUI()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		registerForKeyboardNotifications(self.scrollView)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		deregisterFromKeyboardNotifications()
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let userService = ProfileService()
			self.presenter = EditProfilePresenter(webservice: userService, delegate: self)
		}
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: false)
	}
	
	@IBAction func saveButtonClicked(_ sender: Any) {
		
		if firstNameField.text?.isEmpty == true {
			self.showAlert(title: "Movano", message: "First name is mandatory and can not be blank")
		} else if lastNameField.text?.isEmpty == true {
			self.showAlert(title: "Movano", message: "Last name is mandatory and can not be blank")
		} else if genderField.text?.isEmpty == true {
			self.showAlert(title: "Movano", message: "Gender is mandatory and can not be blank")
		} else if dobField.text?.isEmpty == true {
			self.showAlert(title: "Movano", message: "Date of birth is mandatory and can not be blank")
		} else if (weightField.text?.split(separator: " "))?[0].toDouble == nil {
			self.showAlert(title: "Movano", message: "Weight can not be null")
		} else if (((weightField.text?.split(separator: " "))?[0].toDouble ?? 0) > 1400 || ((weightField.text?.split(separator: " "))?[0].toDouble ?? 0) < 10) {
			self.showAlert(title: "Movano", message: "Weight should be in range of 10 lbs to 1400 lbs")
		} else {
			hud.show(in: self.view)
			let details = UserDetails()
			details.firstName = firstNameField.text!
			details.lastName = lastNameField.text!
			details.dateOfBirth = (dobField.text?.toDate(format: DateFormat.birthdateFormat)?.dateToString(format: DateFormat.birthdateFormat))!
			details.gender = genderField.text!
			details.heightInInches = Int(Utility.getHeightInInches(height: heightField.text!))
			let weight = weightField.text?.components(separatedBy: " ")
			details.weight = Int(weight![0])
			userDetails = details
			
			self.presenter?.updateUserInformation(userDetail: details)
		}
	}
	
	func manageUI() {
		firstNameField.addBottomBorder()
		lastNameField.addBottomBorder()
		dobField.addBottomBorder()
		genderField.addBottomBorder()
		heightField.addBottomBorder()
		weightField.addBottomBorder()
	}
	
	func loadData() {
		self.firstNameField.text = userDetails.firstName
		self.lastNameField.text = userDetails.lastName
		self.dobField.text = userDetails.dateOfBirth
		self.genderField.text = userDetails.gender
		self.heightField.text = "\(userDetails.heightInInches ?? 0)".formattedHeight
		self.weightField.text = "\(userDetails.weight ?? 0)".formattedWeight
	}
	
	func setupIcons() {
		let dob = dobField.setView(.right, image: UIImage(named: "downArrow"))
		dob.addTarget(self, action: #selector(openDOBTextfield), for: .touchUpInside)
		
		let gen = genderField.setView(.right, image: UIImage(named: "downArrow"))
		gen.addTarget(self, action: #selector(openGenderTextfield), for: .touchUpInside)
	}
	
	@objc func openDOBTextfield() {
		dobField.becomeFirstResponder()
	}
	
	@objc func openGenderTextfield() {
		genderField.becomeFirstResponder()
	}
	
	func manageFieldPadding() {
		heightField.addPadding(.left(10))
		dobField.addPadding(.left(10))
		weightField.addPadding(.left(10))
		genderField.addPadding(.right(10))
		firstNameField.addPadding(.left(10))
		lastNameField.addPadding(.right(10))
	}
	
	func assignBirthFieldTask() {
		dobField.setInputView(isDatePicker: true, target: self, selector: #selector(tapDone))
		dobField.isEnabled = true
		dobField.isUserInteractionEnabled = true
	}
	
	func assignDelegate() {
		dobField.delegate = self
		genderField.delegate = self
		weightField.delegate = self
		heightField.delegate = self
		heightPicker.delegate = self
		heightPicker.dataSource = self
		heightPicker.toolbarDelegate = self
		genderPicker.delegate = self
		genderPicker.dataSource = self
		genderPicker.toolbarDelegate = self
	}
	
	func assignInputToFields() {
		heightField.inputView = heightPicker
		heightField.inputAccessoryView = heightPicker.toolbar
		genderField.inputView = genderPicker
		genderField.inputAccessoryView = genderPicker.toolbar
		weightField.showToolBar()
	}
	
	func getDataFromKeychain() {
		self.dobField.text = KeyChain.shared.dateOfBirth
		self.heightField.text = KeyChain.shared.height
		self.weightField.text = KeyChain.shared.weight
	}
	
	@objc func tapDone() {
		if let datePicker = dobField.inputView as? UIDatePicker {
			let dateformatter = DateFormatter()
			dateformatter.dateStyle = .medium
			dateformatter.dateFormat = DateFormat.birthdateFormat
			
			// Change of date of birth
			let oldDob = dobField.text?.toDate(format: "MM/dd/yyyy")
			let newDob = datePicker.date
			let components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: newDob, to: Date())
			if (components.year ?? 0) > (UtilityConstant.MINIMUM_BIRTH_AGE - 1) {
				dobField.text = dateformatter.string(from: datePicker.date)
				dobField.resignFirstResponder()
			} else {
				let actions = [UIAlertAction(title: "Ok", style: .default, handler: { (_) in
					self.dobField.resignFirstResponder()
				})]
				self.showAlert(title: "Movano", message: "Your age can not be less than \(UtilityConstant.MINIMUM_BIRTH_AGE)", actions: actions)
				dobField.text = oldDob?.dateToString(format: "MM/dd/yyyy")
			}
		}
	}
}

extension EditProfileViewController: EditProfileViewDelegate {
	
	func updateUserInformationSuccess(with responseModel: EmptyResponseModel) {
		
		self.delegate?.updateUserDetails(userDetails: self.userDetails)
		self.navigationController?.popViewController(animated: true)
		hud.dismiss()
	}
	
	func updateUserInformationErrorHandler(error: MovanoError) {
		hud.dismiss()
	}
}

// MARK: - UITextFieldDelegate
extension EditProfileViewController: UITextFieldDelegate {
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		self.activeTextField = textField
		if textField == weightField {
			textField.text = textField.text?.weightInLbs
		}
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		let currentText = textField.text ?? ""
		let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
		if textField == weightField {
			let returnValue = prospectiveText.containsOnlyCharactersIn(matchCharacters: "0123456789.") && prospectiveText.count <= 9
			return returnValue
		}
		return true
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == weightField {
			self.selectedWeight = Float(weightField.text!)
		}
		textField.resignFirstResponder()
		return true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		if textField == weightField {
			textField.text =  textField.text?.formattedWeight
		}
	}
}

// MARK: - UIPickerViewDataSource
extension EditProfileViewController: UIPickerViewDataSource {
	
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return (pickerView == heightPicker) ? 4 : 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		if (pickerView == heightPicker) {
			switch component {
			case 0:
				return feetRange.count
			case 1:
				return 1
			case 2:
				return inchRange.count
			case 3:
				return 1
			default:
				return 1
			}
		} else {
			return genderRange.count
		}
	}
}

// MARK: - UIPickerViewDelegate
extension EditProfileViewController: UIPickerViewDelegate {
	
	func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
		var title = ""
		if (pickerView == heightPicker) {
			switch component {
			case 0:
				title = String(feetRange[row])
			case 1:
				title =  "ft"
			case 2:
				title =  String(inchRange[row])
			case 3:
				title =  "inch"
			default:
				title = ""
			}} else {
				switch component {
				case 0:
					title = String(genderRange[row])
				default:
					title = ""
				}
			}
		
		let myTitle = NSAttributedString(string: title, attributes: [.foregroundColor: UIColor.textTitleColor, .font: UIFont(name: "OpenSans-Regular", size: 18)!])
		return myTitle
	}
	
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		if (pickerView == heightPicker) {
			let feetSelection = feetRange[pickerView.selectedRow(inComponent: 0)]
			let inchSelection = inchRange[pickerView.selectedRow(inComponent: 2)]
			
			self.selectedHeight = Float((feetSelection * 12) + inchSelection)
		} else {
			let genderSelection = genderRange[pickerView.selectedRow(inComponent: 0)]
			self.selectedGender = genderSelection
		}
	}
	
	func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
		return (pickerView == heightPicker) ? 80.0 : 130.0
	}
}

// MARK: - ToolbarPickerViewDelegate
extension EditProfileViewController: ToolbarPickerViewDelegate {
	
	func didTapDone() {
		if self.activeTextField == heightField {
			if self.selectedHeight != nil {
				self.heightField.text = self.selectedHeight.formattedHeight
			} else {
				self.heightField.text = Float(12.0).formattedHeight
			}
			self.heightField.resignFirstResponder()
		} else if self.activeTextField == dobField {
			self.dobField.text = self.selectedGender
			self.dobField.resignFirstResponder()
		} else if self.activeTextField == weightField {
			self.weightField.text = self.selectedWeight.formattedWeight
			self.weightField.resignFirstResponder()
		} else {
			self.genderField.text = self.selectedGender
			self.genderField.resignFirstResponder()
		}
	}
	
	func didTapCancel() {
		if self.activeTextField == heightField {
			self.heightField.resignFirstResponder()
		} else if self.activeTextField == dobField {
			self.dobField.resignFirstResponder()
		} else if self.activeTextField == weightField {
			self.weightField.resignFirstResponder()
		} else {
			self.genderField.resignFirstResponder()
		}
	}
}

protocol UpdateUserDetailsProtocol {
	func updateUserDetails(userDetails: UserDetails)
}
