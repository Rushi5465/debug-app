//
//  PersonalDetailsViewController.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 07/11/19.
//  Copyright © 2019 Movano. All rights reserved.
//

import UIKit

class PersonalDetailsViewController: UIViewController {
	
	@IBOutlet weak var dateOfBirth: MovanoTextField!
	@IBOutlet weak var genderField: MovanoTextField!
	@IBOutlet weak var heightTextField: MovanoTextField!
	@IBOutlet weak var weightField: MovanoTextField!
	@IBOutlet weak var scrollView: UIScrollView!
	
	let feetRange = [Int](1...8)
	let inchRange = [Int](0...11)
	let genderRange = ["Female", "Male", "Other"]
	let heightPicker = ToolbarPickerView()
	let genderPicker = ToolbarPickerView()
	var details: UserDetails?
	
	var activeTextField = UITextField()
	var selectedHeight: String = ""
	var selectedGender: String = ""
	var selectedWeight: Float!
	var feetSelection = Int()
	var inchSelection = Int()
	
	var presenter: PersonalDetailPresenter?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if self.presenter == nil {
			let userService = ProfileService()
			self.presenter = PersonalDetailPresenter(webservice: userService, delegate: self)
		}
		
		dismissKeyboardGesture()
		
		assignBirthFieldTask()
		manageFieldPadding()
		assignDelegate()
		assignInputToFields()
		manageUI()
		selectedHeight = "\(feetRange.first!) ft \(inchRange.first!) in"
		
		getDataFromKeychain()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		registerForKeyboardNotifications(self.scrollView)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		deregisterFromKeyboardNotifications()
	}
	
	func setupIcons() {
		let dob = dateOfBirth.setView(.right, image: UIImage(named: "downArrow"))
		dob.addTarget(self, action: #selector(openDOBTextfield), for: .touchUpInside)
		
		let gen = genderField.setView(.right, image: UIImage(named: "downArrow"))
		gen.addTarget(self, action: #selector(openGenderTextfield), for: .touchUpInside)
	}
	
	@objc func openDOBTextfield() {
		dateOfBirth.becomeFirstResponder()
	}
	
	@objc func openGenderTextfield() {
		genderField.becomeFirstResponder()
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func openCalendar() {
		dateOfBirth.endEditing(false)
		dateOfBirth.setInputView(isDatePicker: true, target: self, selector: #selector(tapDone))
	}
	
	@IBAction func sendPersonalDetails() {
		performNextButtonAction()
	}
}

// MARK: - UITextFieldDelegate
extension PersonalDetailsViewController: UITextFieldDelegate {
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		self.activeTextField = textField
		if textField == weightField {
			textField.text = textField.text?.weightInLbs
		}
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		let currentText = textField.text ?? ""
		let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
		if textField == weightField {
			let returnValue = prospectiveText.containsOnlyCharactersIn(matchCharacters: "0123456789.") && prospectiveText.count <= 9
			return returnValue
		}
		return true
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == weightField {
			self.selectedWeight = Float(weightField.text!)
		}
		textField.resignFirstResponder()
		return true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		if textField == weightField {
			textField.text =  textField.text?.formattedWeight
		}
	}
}

// MARK: - UIPickerViewDataSource
extension PersonalDetailsViewController: UIPickerViewDataSource {
	
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return (pickerView == heightPicker) ? 4 : 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		if (pickerView == heightPicker) {
			switch component {
			case 0:
				return feetRange.count
			case 1:
				return 1
			case 2:
				return inchRange.count
			case 3:
				return 1
			default:
				return 1
			}
		} else {
			return genderRange.count
		}
	}
}

// MARK: - UIPickerViewDelegate
extension PersonalDetailsViewController: UIPickerViewDelegate {
	
	func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
		var title = ""
		if (pickerView == heightPicker) {
			switch component {
			case 0:
				title = String(feetRange[row])
			case 1:
				title =  "ft"
			case 2:
				title =  String(inchRange[row])
			case 3:
				title =  "in"
			default:
				title = ""
			}} else {
				switch component {
				case 0:
					title = String(genderRange[row])
				default:
					title = ""
				}
			}
		
		let myTitle = NSAttributedString(string: title, attributes: [.foregroundColor: UIColor.textTitleColor, .font: UIFont(name: "OpenSans-Regular", size: 18)!])
		return myTitle
	}
	
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		if (pickerView == heightPicker) {
			let feetSelection = feetRange[pickerView.selectedRow(inComponent: 0)]
			let inchSelection = inchRange[pickerView.selectedRow(inComponent: 2)]
			self.feetSelection = feetSelection
			self.inchSelection = inchSelection
			selectedHeight = "\(feetSelection) ft \(inchSelection) in"
		} else {
			let genderSelection = genderRange[pickerView.selectedRow(inComponent: 0)]
			selectedGender = genderSelection
		}
	}
	
	func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
		return (pickerView == heightPicker) ? 80.0 : 130.0
	}
}

// MARK: - ToolbarPickerViewDelegate
extension PersonalDetailsViewController: ToolbarPickerViewDelegate {
	
	func didTapDone() {
		if self.activeTextField == heightTextField {
			self.heightTextField.text = self.selectedHeight
			self.heightTextField.resignFirstResponder()
		} else if self.activeTextField == dateOfBirth {
			self.dateOfBirth.text = self.selectedHeight
			self.dateOfBirth.resignFirstResponder()
		} else if self.activeTextField == weightField {
			self.weightField.text = self.selectedWeight.formattedWeight
			self.weightField.resignFirstResponder()
		} else {
			self.genderField.text = self.selectedGender
			self.genderField.resignFirstResponder()
		}
	}
	
	func didTapCancel() {
		if self.activeTextField == heightTextField {
			self.heightTextField.resignFirstResponder()
		} else if self.activeTextField == dateOfBirth {
			self.dateOfBirth.resignFirstResponder()
		} else if self.activeTextField == weightField {
			self.weightField.resignFirstResponder()
		} else {
			self.genderField.resignFirstResponder()
		}
	}
}

// MARK: - PersonalDetailViewDelegate
extension PersonalDetailsViewController: PersonalDetailViewDelegate {
	
	func postUserDetailSuccess(with responseModel: EmptyResponseModel) {
		hud.dismiss()
		KeyChain.shared.saveUserDetails(details: self.details!)
		let controller = FirstSetupGoalViewController.instantiate(fromAppStoryboard: .main)
		self.navigationController?.pushViewController(controller, animated: false)
	}
	
	func postUserDetailErrorHandler(error: MovanoError) {
		self.showErrorAlert(error: error)
		hud.dismiss()
	}
}
