//
//  ProfileDetailViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/21.
//

import UIKit

class ProfileDetailViewController: UIViewController {

	@IBOutlet weak var backButton: MovanoButton!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var collectionHeight: NSLayoutConstraint!
	
	var list = [ProfileDetailModel]() {
		didSet {
			self.tableView.reloadData()
		}
	}
	
	var userDetails: UserDetails!
	var presenter: ProfileDetailPresenter?
	var delegate: UpdateUserDetailsProtocol? = nil
	
	override func viewDidLoad() {
        super.viewDidLoad()

		self.registerPresenters()
		self.registerCellToListView()
		self.assignDelegate()
		
		hud.show(in: self.view)
		self.presenter?.fetchList(userDetail: self.userDetails)
		self.presenter?.fetchUserInformation()
		
		self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.collectionHeight.constant = self.tableView.contentSize.height
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let userService = ProfileService()
			self.presenter = ProfileDetailPresenter(webservice: userService, delegate: self)
		}
	}
	
	func registerCellToListView() {
		let glucoseCell = UINib.init(nibName: "ProfileDetailTableViewCell", bundle: nil)
		self.tableView.register(glucoseCell, forCellReuseIdentifier: ProfileDetailTableViewCell.IDENTIFIER)
		
		let menuCell = UINib.init(nibName: "MenuTableViewCell", bundle: nil)
		self.tableView.register(menuCell, forCellReuseIdentifier: MenuTableViewCell.IDENTIFIER)
	}
	
	func assignDelegate() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		if Connectivity.isConnectedToInternet() {
			self.delegate?.updateUserDetails(userDetails: self.userDetails)
		}
		
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func deleteProfileClicked(_ sender: Any) {
		showDeleteConfirmation()
	}
	
	func showDeleteConfirmation() {
		
		var actions = [UIAlertAction]()
		actions.append(UIAlertAction(title: "OK", style: .default, handler: { (_) in
			self.deleteProfile()
		}))
		actions.append(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
		self.showAlert(title: "Confirm", message: "Are you sure you want to delete this profile?", actions: actions)
	}
	
	func deleteProfile() {
		hud.show(in: self.view)
		self.presenter?.deleteUser()
	}
}

extension ProfileDetailViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		
		let item = list[indexPath.row]
		if item.items is [ProfileDetailItem] {
			return UITableView.automaticDimension
		}
		return 64
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let item = list[indexPath.row]
		if item.items == nil {
			let changePwdController = ChangePasswordViewController.instantiate(fromAppStoryboard: .profile) as ChangePasswordViewController
			self.navigationController?.pushViewController(changePwdController, animated: true)
		}
	}
}

extension ProfileDetailViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.list.count
	}
		
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let item = list[indexPath.row]
		
		if let list = item.items as? [ProfileDetailItem] {
			let cell = ProfileDetailTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
			
			cell.titleLabel.text = item.title
			cell.modelList = list
			cell.editButton.isHidden = (indexPath.row != 0)
			cell.delegate = self
			
			return cell
		} else {
			let cell = MenuTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
			
			cell.itemLabel.text = item.title
			return cell
		}
	}
}

extension ProfileDetailViewController: ProfileDetailTableViewCellDelegate {
	
	func editProfileInformation() {
		
		let controller = EditProfileViewController.instantiate(fromAppStoryboard: .profile)
		controller.userDetails = userDetails
		controller.delegate = self
		self.navigationController?.pushViewController(controller, animated: false)
	}
}

extension ProfileDetailViewController: ProfileDetailViewDelegate {
	
	func userInformation(list: [ProfileDetailModel]) {
		self.list = list
	}
	
	func fetchSuccess(with responseModel: UserResponseModel) {
		self.userDetails = responseModel.data
		KeyChain.shared.saveUserDetails(details: responseModel.data)
		self.presenter?.fetchList(userDetail: self.userDetails)
		hud.dismiss()
	}
	
	func fetchErrorHandler(error: MovanoError) {
		hud.dismiss()
	}
	
	func deleteSuccess(with responseModel: EmptyResponseModel) {
		self.showAlert(title: "Movano", message: "User Profile Deleted.")
		KeyChain.shared.clear()
		DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
			NavigationUtility.setInitialController()
		}
		hud.dismiss()
	}
	
	func deleteErrorHandler(error: MovanoError) {
		hud.dismiss()
	}
}

extension ProfileDetailViewController: UpdateUserDetailsProtocol {
	func updateUserDetails(userDetails: UserDetails) {
		self.userDetails = userDetails
		self.presenter?.fetchList(userDetail: self.userDetails)
		self.presenter?.fetchUserInformation()
		
		for i in 0..<list.count {
			if let newCell = self.tableView.cellForRow(at: IndexPath(item: i, section: 0)) as? ProfileDetailTableViewCell {
				newCell.collectionView.reloadData()
			}
		}
		
	}
}
