//
//  FeedbackSentViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 10/12/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

class FeedbackSentViewController: UIViewController {

	override func viewDidLoad() {
        super.viewDidLoad()
		
    }
    
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popToRootViewController(animated: true)
	}
}
