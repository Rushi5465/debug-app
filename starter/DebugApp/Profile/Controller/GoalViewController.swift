//
//  GoalViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/09/21.
//

import UIKit
import SwiftRangeSlider

class GoalViewController: UIViewController {

	@IBOutlet weak var navigationView: UIView!
	@IBOutlet weak var nextButton: MovanoButton!
	@IBOutlet weak var tableView: UITableView!
	
	@IBOutlet weak var tableHeight: NSLayoutConstraint!
	
	var goalList = [UserGoal]() {
		didSet {
			self.tableView.reloadData()
		}
	}
	
	var presenter: GoalPresenter?
	var goal: Goal!
	
	override func viewDidLoad() {
        super.viewDidLoad()

		self.registerPresenters()
		self.registerCellToListView()
		self.assignDelegate()
    }
	
	func registerCellToListView() {
		let rangeGoalNib = UINib.init(nibName: "RangeGoalTableViewCell", bundle: nil)
		self.tableView.register(rangeGoalNib, forCellReuseIdentifier: RangeGoalTableViewCell.IDENTIFIER)
		
		let goalNib = UINib.init(nibName: "GoalTableViewCell", bundle: nil)
		self.tableView.register(goalNib, forCellReuseIdentifier: GoalTableViewCell.IDENTIFIER)
	}
	
	func assignDelegate() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.goal = UserDefault.shared.goals
		self.presenter?.fetchGoal()
		self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		self.tableView.removeObserver(self, forKeyPath: "contentSize")
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.tableHeight.constant = self.tableView.contentSize.height
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let goalService = UserGoalsWebService()
			self.presenter = GoalPresenter(goalService: goalService, delegate: self)
		}
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func nextButtonClicked(_ sender: Any) {
		NotificationCenter.default.post(name: NSNotification.Name("updateDataAsPerGoalChange"), object: nil)
		self.presenter?.updateGoal(model: self.goal)
	}
}

// MARK: - UITableViewDelegate
extension GoalViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
}

// MARK: - UITableViewDataSource
extension GoalViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.goalList.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let item = goalList[indexPath.row]
		
		if item.value is Range<Int> {
			let cell = RangeGoalTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
			
			cell.loadView(goal: item)
			cell.delegate = self
			return cell
		} else {
			let cell = GoalTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
			
			cell.loadView(goal: item)
			cell.delegate = self
			return cell
		}
	}
}

// MARK: - GoalDelegate
extension GoalViewController: GoalDelegate {
	
	func goalView(newValue: UserGoal) {
		self.goal.updateValue(userGoal: newValue)
	}
}

// MARK: - GoalViewDelegate
extension GoalViewController: GoalViewDelegate {
	
	func fetchGoals(goals: [UserGoal]) {
		self.goalList = goals
	}
	
	func updateGoal(result: Result<Goal, Error>) {
		switch result {
		case .success:
			self.navigationController?.popViewController(animated: true)
		case .failure(let error):
			print(error)
		}
	}
}
