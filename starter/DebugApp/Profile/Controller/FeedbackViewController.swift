//
//  FeedbackViewController.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 12/03/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit
import MessageUI

class FeedbackViewController: UIViewController {
	
	let categories = ["General", "Contact Us"]
	fileprivate let categoryPicker = ToolbarPickerView()
	@IBOutlet weak var category: MovanoTextField!
	@IBOutlet weak var message: MovanoTextView!
	@IBOutlet weak var navigationView: UIView!
	@IBOutlet weak var scrollView: UIScrollView!
	
	var categorySelected: String = ""
	var selectedItem: String = ""
	
	var presenter: FeedbackPresenter?

	override func viewDidLoad() {
		super.viewDidLoad()
		message.showToolBar()
		category.addPadding(.left(10))
		category.text = categories.first
		
		categoryPicker.delegate = self
		categoryPicker.dataSource = self
		categoryPicker.toolbarDelegate = self
		categoryPicker.backgroundColor = UIColor.dashboardBackground
		category.inputView = categoryPicker
		category.inputAccessoryView = categoryPicker.toolbar
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		registerForKeyboardNotifications(self.scrollView)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		deregisterFromKeyboardNotifications()
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			self.presenter = FeedbackPresenter(delegate: self)
		}
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func sendFeedback() {
		self.presenter?.shareFeedback(subject: self.category.text!, message: self.message.text)
	}
}

// MARK: - FeedbackViewDelegate
extension FeedbackViewController: FeedbackViewDelegate {
	
	func shareFeedback(result: Result<UIViewController, MovanoError>) {
		switch  result {
		case .success(let controller):
			self.present(controller, animated: true)
		case .failure(let error):
			self.showErrorAlert(error: error)
		}
	}
	
	func feedbackSent(didFinishWith result: MFMailComposeResult, error: MovanoError?) {
		if let error = error {
			self.showErrorAlert(error: error)
			return
		}
		
		if result == .sent {
			let presentingController = FeedbackSentViewController.instantiate(fromAppStoryboard: .profile)
			self.navigationController?.pushViewController(presentingController, animated: true)
		}
	}
}

// MARK: - UIPickerViewDataSource
extension FeedbackViewController: UIPickerViewDataSource {
	
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return categories.count
	}
}

// MARK: - UIPickerViewDelegate
extension FeedbackViewController: UIPickerViewDelegate {
	
	func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
		let title = categories[row]
		let myTitle = NSAttributedString(string: title, attributes: [.foregroundColor: UIColor.white, .font: UIFont(name: "OpenSans-Regular", size: 16)!])
		return myTitle
	}
	
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		selectedItem = categories[pickerView.selectedRow(inComponent: component)]
	}
}

// MARK: - ToolbarPickerViewDelegate
extension FeedbackViewController: ToolbarPickerViewDelegate {
	
	func didTapDone() {
		self.category.text = selectedItem
		self.category.resignFirstResponder()
	}
	
	func didTapCancel() {
		self.category.resignFirstResponder()
	}
}
