//
//  SettingViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 22/07/21.
//

import UIKit

// swiftlint:disable all
class SettingViewController: UIViewController {
	
	@IBOutlet weak var backButton: MovanoButton!
	@IBOutlet weak var collectionView: UICollectionView!
	
	var presenter: SettingPresenter?
	
	var list = [ProfileMenuModel]() {
		didSet {
			self.collectionView.reloadData()
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()

		self.registerPresenters()
		self.registerCellToListView()
		self.assignDelegate()
		self.presenter?.fetchInformation()
    }
    
	func registerPresenters() {
		let cacheService = CacheTechniqueWebService()
		if self.presenter == nil {
			self.presenter = SettingPresenter(delegate: self, cacheServiceService: cacheService)
		}
	}
	
	func registerCellToListView() {
		
		let profileNib = UINib.init(nibName: "ProfileMenuCollectionViewCell", bundle: nil)
		self.collectionView.register(profileNib, forCellWithReuseIdentifier: ProfileMenuCollectionViewCell.IDENTIFIER)
	}
	
	func assignDelegate() {
		self.collectionView.delegate = self
		self.collectionView.dataSource = self
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	func getCacheData() {
		hud.show(in: self.view)
		let todaysDate = Date()
		//		let latestDate = Calendar.current.date(byAdding: .day, value: -1, to: todaysDate)!
		let modDate = Calendar.current.date(byAdding: .day, value: -30, to: todaysDate.startOfDay)!
		/*
		 let dayOfWeek = modDate.dayNumberOfWeek() ?? 0
		 if dayOfWeek > 1 {
		 modDate = Calendar.current.date(byAdding: .day, value: -(dayOfWeek - 1), to: modDate)!
		 }
		 */
		var range = DateRange(start: modDate, end: todaysDate, type: .monthly)
		
		if let upperRange = UserDefaults.standard.object(forKey: "cacheUpperRange") as? Double {
			if Date(timeIntervalSince1970: upperRange).compare(todaysDate) == .orderedAscending {
				let newLowerBound = upperRange
				let newUpperBound = todaysDate.timeIntervalSince1970
				range = DateRange(start: Date(timeIntervalSince1970: newLowerBound), end: Date(timeIntervalSince1970: newUpperBound), type: .daily)
				UserDefaults.standard.set(todaysDate.timeIntervalSince1970, forKey: "cacheUpperRange")
			} else if Date(timeIntervalSince1970: upperRange).compare(todaysDate) == .orderedSame {
				print("Its equal")
			}
		} else {
			UserDefaults.standard.set(todaysDate.timeIntervalSince1970, forKey: "cacheUpperRange")
		}
		
		// Create a dispatch queue
		let dispatchQueue = DispatchQueue(label: "cacheDataDownload")
		
		dispatchQueue.async {
			self.presenter?.cacheServiceService.fetchIntensityPulseData(weekStartdate: modDate.startOfDay.dateToString(format: DateFormat.serverDateFormat), weekEndDate: todaysDate.startOfDay.dateToString(format: DateFormat.serverDateFormat))
			print("ActivityIntensityModel  CoreData Updated For -\(range)")
		}
		
		dispatchQueue.async {
			self.presenter?.cacheServiceService.fetchActivtyIntensity(by: range.intervalRange)
			print("Activity Intensity CoreData Updated For -\(range)")
		}
		
		dispatchQueue.async {
			self.presenter?.cacheServiceService.fetch(sleepDataBy: range)
			print("Sleep data CoreData Updated For -\(range)")
		}

		dispatchQueue.async {
			self.presenter?.cacheServiceService.fetchAverageData(by: range.intervalRange)
			print("Average CoreData Updated For -\(range)")
		}
		
		dispatchQueue.async {
			self.presenter?.cacheServiceService.fetch(dailyAveragesBy: range.intervalRange)
			print("Daily avergae CoreData Updated For -\(range)")
		}
		
		dispatchQueue.async {
			self.presenter?.cacheServiceService.fetch(hrvBy: range.intervalRange)
			print("HRV CoreData Updated For -\(range)")
		}
		
		dispatchQueue.async {
			self.presenter?.cacheServiceService.fetch(oxygenBy: range.intervalRange)
			print("Oxygen CoreData Updated For -\(range)")
		}
		
		dispatchQueue.async {
			self.presenter?.cacheServiceService.fetch(pulseRateDataBy: range.intervalRange)
			print("Pulse Rate CoreData Updated For -\(range)")
		}
		
		dispatchQueue.async {
			self.presenter?.cacheServiceService.fetch(skinTempDataBy: range.intervalRange)
			print("Skin Temp CoreData Updated For -\(range)")
		}
		
		dispatchQueue.async {
			self.presenter?.cacheServiceService.fetch(breathingRateDataBy: range.intervalRange)
			print("Breathing CoreData Updated For -\(range)")
		}
		
		dispatchQueue.async {
			self.presenter?.cacheServiceService.listBloodPressures(range: range.intervalRange)
			print("BP CoreData Updated For -\(range)")
		}
		
		dispatchQueue.async {
			self.presenter?.cacheServiceService.fetch(exerciseDataBy: range.intervalRange)
			print("Exercise CoreData Updated For -\(range)")
		}
		
		dispatchQueue.async {
			self.presenter?.cacheServiceService.fetch(stepCountBy: range.intervalRange, completionHandler: { isComplete in
				DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
					hud.dismiss(animated: true)
					self.showAlert(title: "Successful", message: "30 Days data has been downloaded.", actions: [
						UIAlertAction(title: "OK", style: .default, handler: nil)])
				})
			})
			print("Step Count CoreData Updated For -\(range)")
		}
		
	}
	
}

extension SettingViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let width = (collectionView.bounds.width / 2.0) - 15
		return CGSize(width: width, height: width)
	}
}

extension SettingViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return list.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = ProfileMenuCollectionViewCell.dequeueReusableCell(collectionView: collectionView, indexPath: indexPath)
		
		let parameter = self.list[indexPath.item]
		
		cell.titleLabel.text = parameter.title
		cell.image.image = parameter.image
		
		return cell
	}
}

extension SettingViewController: SettingViewDelegate {
	
	func fetchSettingList(list: [ProfileMenuModel]) {
		self.list = list
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let item = self.list[indexPath.item]
		
		if item.type == .profile {
			let controller = ProfileViewController.instantiate(fromAppStoryboard: .profile)
			self.navigationController?.pushViewController(controller, animated: true)
		} else if item.type == .bpLog {
			let controller = HistoryDataViewController.instantiate(fromAppStoryboard: .activity)
			controller.dataType = .bpReading
			self.navigationController?.pushViewController(controller, animated: true)
		} else if item.type == .environment {
			let controller = ServerListViewController.instantiate(fromAppStoryboard: .profile)
			self.navigationController?.pushViewController(controller, animated: true)
		} else if item.type == .cacheData {
			if Connectivity.isConnectedToInternet() {
				getCacheData()
			} else {
				self.showAlert(title: "Error", message: MovanoError.noInternetConnection.errorMessage, actions: [
					UIAlertAction(title: "OK", style: .default, handler: nil)])
			}
		}
	}
}
