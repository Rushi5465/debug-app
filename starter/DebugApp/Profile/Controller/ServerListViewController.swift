//
//  ServerListViewController.swift
//  BloodPressureApp
//
//  Created by Rushikant on 03/12/21.
//

import UIKit

class ServerListViewController: UIViewController {

	@IBOutlet weak var navigationView: UIView!
	@IBOutlet weak var tableView: UITableView!
	
	var serverList = [Server]() {
		didSet {
			self.tableView.reloadData()
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()

		self.registerCellToListView()
		self.assignDelegate()
		self.manageNavigationBar()
		
		tableView.tableFooterView = UIView()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationController?.setNavigationBarHidden(true, animated: animated)
		self.serverList = ServerManager.shared.serverList
	}
	
	func manageNavigationBar() {
		navigationView.roundCorners([.layerMaxXMaxYCorner, .layerMinXMaxYCorner], radius: 20.0)
		self.navigationView.addShadow(offset: CGSize(width: 0.0, height: 1.0), color: UIColor.gray, opacity: 1.0)
	}
	
	func registerCellToListView() {
		
		let uploadNib = UINib.init(nibName: "ServerTableViewCell", bundle: nil)
		self.tableView.register(uploadNib, forCellReuseIdentifier: ServerTableViewCell.IDENTIFIER)
	}
	
	func assignDelegate() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
}

extension ServerListViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return "SELECT ENVIRONMENT"
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
		if let headerView = view as? UITableViewHeaderFooterView {
				headerView.textLabel?.textColor = .textColor
			}
	}
}

extension ServerListViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if self.serverList.isEmpty {
			return     1
		}
		return self.serverList.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let server = self.serverList[indexPath.row]
		
		let cell = ServerTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
		
		cell.nameLabel.text = server.type.rawValue
		if server.type == ServerManager.shared.currentServer.type {
			cell.accessoryType = .checkmark
			cell.accessoryView?.backgroundColor = .clear
			cell.layer.backgroundColor = UIColor.settingBackground.cgColor
		} else {
			cell.accessoryType = .none
		}
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		
		if self.serverList.count == 1 {
			cell.roundCorners([.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner], radius: 10)
		} else if self.serverList.count == 0 {
			cell.roundCorners([.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner], radius: 10)
		} else {
			if indexPath.row == 0 {
				cell.roundCorners([.layerMaxXMinYCorner, .layerMinXMinYCorner], radius: 10)
			} else if indexPath.row == self.serverList.count - 1 {
				cell.roundCorners([.layerMaxXMaxYCorner, .layerMinXMaxYCorner], radius: 10)
			}
		}
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if self.serverList[indexPath.row].type != ServerManager.shared.currentServer.type {
			var actions = [UIAlertAction]()
			actions.append(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
				Logger.shared.addLog("Server Environment changed to \(self.serverList[indexPath.row].type.rawValue)")
				KeyChain.shared.clearTokens()
				KeyChain.shared.userState =  NavigationUtility.UserState.logout.rawValue
				UserDefault.shared.deleteAllUserDefault()
				ServerManager.shared.currentServer = self.serverList[indexPath.row]
				print("Current config server is -\(Config.shared.currentServer)")
				self.tableView.reloadData()
				KeyChain.shared.firstName = ""
				KeyChain.shared.lastName = ""
				KeyChain.shared.email = ""
				NavigationUtility.setInitialController()
			}))
			actions.append(UIAlertAction(title: "No", style: .cancel, handler: nil))
			self.showAlert(title: "Movano", message: "Are you sure you want to select this server? You will be logged out from the app.", actions: actions)
		}
	}
}

