//
//  ProfileViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 27/10/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

// swiftlint:disable all
class ProfileViewController: UIViewController {

	@IBOutlet weak var navigationView: UIView!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var tableHeight: NSLayoutConstraint!
	@IBOutlet weak var versionLabel: MovanoLabel!
	
	var section = [ProfileScreenModel]() {
		didSet {
			self.tableView.reloadData()
		}
	}
	
	var userDetails = UserDetails()
	
	var presenter: MyProfilePresenter?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if presenter == nil {
			let userService = ProfileService()
			self.presenter = MyProfilePresenter(webservice: userService, delegate: self)
		}
		
		self.registerCellToListView()
		self.assignDelegate()
		hud.show(in: self.view)
		self.presenter?.fetchData()
		self.presenter?.fetchUserInformation()
		
		self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
		self.versionLabel.text = "\(Bundle.main.releaseVersionNumberPretty)"
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.tableHeight.constant = self.tableView.contentSize.height
	}
	
	func registerCellToListView() {
		
		let profileNib = UINib.init(nibName: "ProfileTableViewCell", bundle: nil)
		self.tableView.register(profileNib, forCellReuseIdentifier: ProfileTableViewCell.IDENTIFIER)
		
		let menuNib = UINib.init(nibName: "MenuTableViewCell", bundle: nil)
		self.tableView.register(menuNib, forCellReuseIdentifier: MenuTableViewCell.IDENTIFIER)
		
		let switchNib = UINib.init(nibName: "SecuritySwitchTableViewCell", bundle: nil)
		self.tableView.register(switchNib, forCellReuseIdentifier: SecuritySwitchTableViewCell.IDENTIFIER)
		
	}
	
	func assignDelegate() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationController?.setNavigationBarHidden(true, animated: animated)
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func logoutButtonClicked(_ sender: Any) {
		self.presenter?.logoutUser()
	}
}

// MARK: - UITableViewDelegate
extension ProfileViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let item = section[indexPath.row]
		if item.cellType == .detailCell {
			guard let dataString = item.data as? String else {
				fatalError("File Not Found")
			}
			if dataString == "My Details" {
				let controller = ProfileDetailViewController.instantiate(fromAppStoryboard: .profile)
				controller.userDetails = userDetails
				controller.delegate = self
				self.hideTabWhenPushed(controller: controller)
			} else if dataString == "Logs" {
				let controller = LogsViewController.instantiate(fromAppStoryboard: .profile)
				self.hideTabWhenPushed(controller: controller)
			} else if dataString == "Send Feedback" {
				let controller = FeedbackViewController.instantiate(fromAppStoryboard: .profile)
				self.hideTabWhenPushed(controller: controller)
			} else if dataString == "CLI Tool" {
//				let controller = CLIViewController.instantiate(fromAppStoryboard: .commandLineInterface)
//				self.hideTabWhenPushed(controller: controller)
			} else if dataString == "Goals" {
				let controller = GoalViewController.instantiate(fromAppStoryboard: .profile)
				self.hideTabWhenPushed(controller: controller)
			}
		}
	}
}

// MARK: - UITableViewDataSource
extension ProfileViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.section.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let item = section[indexPath.row]
		if item.cellType == .profile {
			let cell = ProfileTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
			cell.configure()
			return cell
		} else if item.cellType == .switchCell {
			let cell = SecuritySwitchTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
			cell.titleLabel.text = self.section[indexPath.row].data as? String
			cell.featureSwitch.isOn = KeyChain.shared.currentUser!.isSecurityEnabled
			cell.delegate = self
			cell.indexpath = indexPath
			return cell
		} else {
			let cell = MenuTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
			cell.itemLabel.text = self.section[indexPath.row].data as? String
			return cell
		}
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		
		let item = section[indexPath.row]
		if item.cellType == .profile {
			return UITableView.automaticDimension
		} else if item.cellType == .profile {
			return 66
		} else {
			return 70
		}
			
	}
}

// MARK: - SecuritySwitchTableViewCellDelegate
extension ProfileViewController: SecuritySwitchTableViewCellDelegate {
	
	func switchClicked(state: Bool, indexPath: IndexPath) {
		
		KeyChain.shared.currentUser?.remindLater = false
		if state {
			var allUsers = KeyChain.shared.userAccounts
			let securedAccount = allUsers.filter { (account) -> Bool in
				return account.isSecurityEnabled
			}
			if !securedAccount.isEmpty {
				
				var actions = [UIAlertAction]()
				actions.append(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
					for each in securedAccount {
						let account = UserAccount(username: each.username, password: each.password, isSecurityEnabled: false, remindLater: each.remindLater)
						allUsers = allUsers.add(account)
						KeyChain.shared.userAccounts = allUsers
					}
					KeyChain.shared.currentUser?.isSecurityEnabled = true
					self.firstTimeAuthentication()
				}))
				actions.append(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
					self.tableView.reloadData()
				}))
				
				var securityString = ""
				if BiometricAuthentication.shared.isTouchIDSupported {
					securityString = "Touch ID"
				} else if BiometricAuthentication.shared.isFaceIDSupported {
					securityString = "Face ID"
				}
				
				let message = "There is another account which is already linked with the \(securityString) on this device. Do you want to alter the current \(securityString) settings and link it with this current account?"
				self.showAlert(title: "Warning", message: message, actions: actions)
			} else {
				KeyChain.shared.currentUser?.isSecurityEnabled = true
				self.firstTimeAuthentication()
			}
		} else {
			KeyChain.shared.currentUser?.isSecurityEnabled = state
		}
	}
}

// MARK: - MyProfileViewDelegate
extension ProfileViewController: MyProfileViewDelegate {
	
	func fetchSuccess(with responseModel: UserResponseModel) {
		self.userDetails = responseModel.data
		if let firstName = self.userDetails.firstName {
			KeyChain.shared.firstName = firstName
		}
		if let lastName = self.userDetails.lastName {
			KeyChain.shared.lastName = lastName
		}
		hud.dismiss()
		self.tableView.refreshControl?.endRefreshing()
	}
	
	func fetchErrorHandler(error: MovanoError) {
		self.showErrorAlert(error: error)
		hud.dismiss()
	}
	
	func fetchList(list: [ProfileScreenModel]) {
		self.section = list
	}
}

extension ProfileViewController: UpdateUserDetailsProtocol {
	func updateUserDetails(userDetails: UserDetails) {
		self.userDetails = userDetails
		self.presenter?.fetchData()
		self.presenter?.fetchUserInformation()
	}
}
