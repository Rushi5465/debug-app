//
//  SecondSetupGoalViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/09/21.
//

import UIKit

class SecondSetupGoalViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var nextButton: MovanoButton!
	
	@IBOutlet weak var tableHeight: NSLayoutConstraint!
	
	var goalList = [UserGoal]() {
		didSet {
			self.tableView.reloadData()
		}
	}
	
	var presenter: SecondSetupGoalPresenter?
	var goal: Goal!
	
	override func viewDidLoad() {
		super.viewDidLoad()

		self.registerPresenters()
		self.registerCellToListView()
		self.assignDelegate()
		
		self.presenter?.fetchGoal()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		self.tableView.removeObserver(self, forKeyPath: "contentSize")
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.tableHeight.constant = self.tableView.contentSize.height
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let goalService = UserGoalsWebService()
			self.presenter = SecondSetupGoalPresenter(goalService: goalService, delegate: self)
		}
	}
	
	func registerCellToListView() {
		let rangeGoalNib = UINib.init(nibName: "RangeGoalTableViewCell", bundle: nil)
		self.tableView.register(rangeGoalNib, forCellReuseIdentifier: RangeGoalTableViewCell.IDENTIFIER)
		
		let goalNib = UINib.init(nibName: "GoalTableViewCell", bundle: nil)
		self.tableView.register(goalNib, forCellReuseIdentifier: GoalTableViewCell.IDENTIFIER)
	}
	
	func assignDelegate() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
	}
	
	@IBAction func nextButtonClicked(_ sender: Any) {
		self.presenter?.addGoal(model: self.goal)
	}
	
	@IBAction func backButtonClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: false)
	}
}

// MARK: - UITableViewDelegate
extension SecondSetupGoalViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
}

// MARK: - UITableViewDataSource
extension SecondSetupGoalViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.goalList.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let item = goalList[indexPath.row]
		
		if item.value is Range<Int> {
			let cell = RangeGoalTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
			
			cell.loadView(goal: item)
			cell.delegate = self
			return cell
		} else {
			let cell = GoalTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
			
			cell.loadView(goal: item)
			cell.delegate = self
			return cell
		}
	}
}

// MARK: - GoalDelegate
extension SecondSetupGoalViewController: GoalDelegate {
	
	func goalView(newValue: UserGoal) {
		self.goal.updateValue(userGoal: newValue)
	}
}

// MARK: - GoalViewDelegate
extension SecondSetupGoalViewController: SecondSetupGoalViewDelegate {
	
	func fetchGoals(goals: [UserGoal]) {
		self.goalList = goals
	}
	
	func addGoal(result: Result<Goal, Error>) {
		switch result {
		case .success:
			KeyChain.shared.currentOnboardingPage = OnBoardingStatus.success.rawValue
			NavigationUtility.setInitialController()
		case .failure(let error):
			print(error)
		}
	}
}
