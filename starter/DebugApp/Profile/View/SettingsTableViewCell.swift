//
//  SettingsTableViewCell.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 02/01/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
	
	let padding: CGFloat = 20.0
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	override var frame: CGRect {
		get {
			return super.frame
		}
		set (newFrame) {
			var frame = newFrame
			frame.origin.x += padding
			frame.size.width -= (2 * padding)
			super.frame = frame
		}
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}
	
}
