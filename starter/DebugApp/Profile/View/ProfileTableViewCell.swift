//
//  ProfileTableViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 27/10/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

	@IBOutlet weak var nameLabel: MovanoLabel!
	@IBOutlet weak var emailLabel: MovanoLabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	func configure() {
		self.nameLabel.text = KeyChain.shared.firstName + " " + KeyChain.shared.lastName
		self.emailLabel.text = KeyChain.shared.email
	}
}
