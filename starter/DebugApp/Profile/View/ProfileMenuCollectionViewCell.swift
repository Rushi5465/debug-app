//
//  ProfileMenuCollectionViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 22/07/21.
//

import UIKit

class ProfileMenuCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var image: UIImageView!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        
		self.layer.cornerRadius = 8.0
		self.layer.masksToBounds = true
    }

}
