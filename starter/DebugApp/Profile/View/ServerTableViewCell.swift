//
//  ServerTableViewCell.swift
//  BloodPressureApp
//
//  Created by Rushikant on 03/12/21.
//

import UIKit

class ServerTableViewCell: UITableViewCell {

	@IBOutlet weak var nameLabel: MovanoLabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
