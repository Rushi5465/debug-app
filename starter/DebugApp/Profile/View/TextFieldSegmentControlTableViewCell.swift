//
//  TextFieldSegmentControlTableViewCell.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 07/02/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

class TextFieldSegmentControlTableViewCell: UITableViewCell {
	
	@IBOutlet weak var valueLabel: MovanoLabel!
	@IBOutlet weak var valueTextField: MovanoTextField!
	//@IBOutlet weak var segment:UISegmentedControl!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}
	
}
