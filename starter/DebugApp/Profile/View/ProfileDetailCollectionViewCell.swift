//
//  ProfileDetailCollectionViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/21.
//

import UIKit

class ProfileDetailCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var valueLabel: MovanoLabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
