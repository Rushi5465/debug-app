//
//  DeviceTableViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 27/10/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

class DeviceTableViewCell: UITableViewCell {

	@IBOutlet weak var deviceNameLabel: MovanoLabel!
	@IBOutlet weak var timeStampLabel: MovanoLabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
