//
//  ProfileDetailTableViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 24/07/21.
//

import UIKit

protocol ProfileDetailTableViewCellDelegate {
	func editProfileInformation()
}

class ProfileDetailTableViewCell: UITableViewCell {
	
	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var collectionHeight: NSLayoutConstraint!
	@IBOutlet weak var editButton: MovanoButton!
	
	var modelList = [ProfileDetailItem]()
	var delegate: ProfileDetailTableViewCellDelegate?
	
    override func awakeFromNib() {
        super.awakeFromNib()
        
		self.registerCollectionCell()
		self.assignDelegates()
		
		self.collectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }

	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.collectionHeight.constant = self.collectionView.contentSize.height
	}
	
	func registerCollectionCell() {
		let parameterCell = UINib.init(nibName: "ProfileDetailCollectionViewCell", bundle: nil)
		self.collectionView.register(parameterCell, forCellWithReuseIdentifier: ProfileDetailCollectionViewCell.IDENTIFIER)
	}
	
	func assignDelegates() {
		self.collectionView.delegate = self
		self.collectionView.dataSource = self
	}
	
	@IBAction func editButtonClicked(_ sender: Any) {
		self.delegate?.editProfileInformation()
	}
}

extension ProfileDetailTableViewCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let width = (collectionView.bounds.width / 2.0) - 5
		return CGSize(width: width, height: 55)
	}
}

extension ProfileDetailTableViewCell: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		self.modelList.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell  = ProfileDetailCollectionViewCell.dequeueReusableCell(collectionView: collectionView, indexPath: indexPath)
		
		let item = self.modelList[indexPath.item]
		
		cell.titleLabel.text = item.key
		cell.valueLabel.text = item.value ?? "-"
		
		return cell
	}
}
