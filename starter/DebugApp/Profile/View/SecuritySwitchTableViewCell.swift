//
//  SecuritySwitchTableViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 27/10/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

protocol SecuritySwitchTableViewCellDelegate {
	func switchClicked(state: Bool, indexPath: IndexPath)
}

class SecuritySwitchTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var featureSwitch: UISwitch!
	
	var delegate: SecuritySwitchTableViewCellDelegate?
	var indexpath: IndexPath!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
	@IBAction func switchButtonClicked(_ sender: Any) {
		self.delegate?.switchClicked(state: featureSwitch.isOn, indexPath: self.indexpath)
	}
}
