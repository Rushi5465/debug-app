//
//  TextFieldTableViewCell.swift
//  BloodPressureApp
//
//  Created by Trupti Veer on 16/01/20.
//  Copyright © 2020 Movano. All rights reserved.
//

import UIKit

protocol TextFieldTableViewCellDelegate {
	func fieldChanged(updatedText: String, indexpath: IndexPath)
}

class TextFieldTableViewCell: UITableViewCell {
	
	@IBOutlet weak var valueLabel: MovanoLabel!
	@IBOutlet weak var valueTextField: MovanoTextField!
	
	var indexpath: IndexPath!
	var delegate: TextFieldTableViewCellDelegate?
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	@IBAction func textFieldEditingChanged(_ sender: Any) {
		self.delegate?.fieldChanged(updatedText: self.valueTextField.text!, indexpath: self.indexpath)
	}
}
