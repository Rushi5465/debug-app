//
//  ExerciseReadingPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/07/21.
//

import Foundation
import UIKit

// swiftlint:disable all
class ExerciseReadingPresenter: ExerciseReadingPresenterProtocol {
	
	private var bpService: BloodPressureWebServiceProtocol
	private var exerciseService: ExerciseDataWebServiceProtocol
	private var pulseRateService: PulseRateWebServiceProtocol
	private weak var delegate: ExerciseReadingViewDelegate?
	
	var timer = Timer()
	var timerCounter = 0
	
	required init(bpService: BloodPressureWebServiceProtocol, exerciseService: ExerciseDataWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, delegate: ExerciseReadingViewDelegate) {
		self.bpService = bpService
		self.exerciseService = exerciseService
		self.pulseRateService = pulseRateService
		self.delegate = delegate
	}
	
	func fetchParameters() {
		let caloriesBurned = Parameter(name: "CALORIES  BURNED", reading: nil, unit: "cal", iconName: "sleep-CalorieIcon")
		let steps = Parameter(name: "STEPS", reading: nil, unit: "", iconName: "sleep_StepsIcon")
		let floorClimbed = Parameter(name: "FLOORS CLIMBED", reading: nil, unit: "", iconName: "sleep-FloorsIcon")
		let distance = Parameter(name: "DISTANCE", reading: nil, unit: "miles", iconName: "sleep-DistanceIcon")
		let bodyTemperature = Parameter(name: "SKIN TEMP VAR", reading: nil, unit: "°f", iconName: "sleep_SkintempIcon")
		let breathingRate = Parameter(name: "BREATHING RATE", reading: nil, unit: "brpm", iconName: "sleep-BreathingIcon")
		let bp = Parameter(name: "BLOOD PRESSURE", reading: nil, unit: "mmHg", iconName: "sleep-BPIcon")
		let heartRate = Parameter(name: "PULSE RATE", reading: nil, unit: "bpm", iconName: "sleep-HeartIcon")
		let spo2 = Parameter(name: "OXYGEN", reading: nil, unit: "%", iconName: "sleep-OxygenIcon")
		
		self.delegate?.parameter(list: [caloriesBurned, steps, spo2, distance, bodyTemperature, breathingRate])
	}
	
	private func getBloodPressureDataByTimeStamp(for patient: Patient, timestamp: Double) {
		bpService.getBloodPressureDataByTimeStamp(for: patient, timestamp: timestamp) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.getBpByTimeStampErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.timer.invalidate()
				self?.delegate?.getBpByTimeStampSuccess(with: response)
			}
		}
	}
	
	func fetchReading(request: MessageToSnsRequestModel) {
		self.timerCounter = 0
		
		self.timer = Timer.scheduledTimer(withTimeInterval: 4, repeats: true) { (timer) in
			if self.timerCounter == 15 {
				timer.invalidate()
				hud.dismiss()
				return
			}
			self.timerCounter += 1
			let patient = Patient(id: KeyChain.shared.idToken, name: KeyChain.shared.firstName + "_" + KeyChain.shared.lastName)
			self.getBloodPressureDataByTimeStamp(for: patient, timestamp: Double(request.file_timestamp))
		}
	}
	
	func addPauseData(timeRange: TimeRange) {
		
		let model = ExerciseReading(startTime: timeRange.startTime, endTime: timeRange.endTime)
		self.exerciseService.add(model: model) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.postExerciseDataErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.postExerciseDataSuccess(with: response)
			}
		}
	}
	
	func fetchHeartRateData(range: DateRange) {
		self.pulseRateService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				var averageHeartRate = item.reduce(0) { (result: Int, nextItem: CoreDataPulseRate) -> Int in
					return result + nextItem.value.intValue
				}
				let heartRateRange = UserDefault.shared.goals.pulseRate.value as! Range<Int>
				let limitLines = [heartRateRange.lowerBound, heartRateRange.upperBound]
				if !item.isEmpty {
					var elements = [ChartElement]()
					for x in 0..<item.count {
						elements.append(ChartElement(date: item[x].date, value: Double(item[x].value)))
					}
					let chartData = ActivityChartData(barColor: UIColor.barPurple, elements: elements)
					averageHeartRate = averageHeartRate / (item.count)
					let model = ActivityChartModel(type: "Heart Rate", value: NSNumber(value: averageHeartRate), unit: "bpm", chartData: chartData)
					self?.delegate?.fetchHeartRateSuccess(model: model)
				} else {
					let chartData = ActivityChartData(barColor: UIColor.barPurple, elements: [])
					let model = ActivityChartModel(type: "Heart Rate", value: NSNumber(value: averageHeartRate), unit: "bpm", chartData: chartData)
					self?.delegate?.fetchHeartRateSuccess(model: model)
				}
			}
		}
	}
}
