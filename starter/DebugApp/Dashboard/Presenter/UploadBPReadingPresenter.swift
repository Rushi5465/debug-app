//
//  UploadBPReadingPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 01/07/21.
//

import Foundation
import UIKit
import CoreBluetooth

// swiftlint:disable all
class UploadBPReadingPresenter: UploadBPReadingPresenterProtocol {
	
	private var uploaderService: DataUploaderWebServiceProtocol
    private var temperatureService: SkinTemperatureWebServiceProtocol
	private var pauseDataService: PauseDataWebServiceProtocol
	private var pulseRateService: PulseRateWebServiceProtocol
	private var breathingRateService: BreathingRateWebServiceProtocol
	private weak var delegate: UploadBPReadingViewDelegate?
	private var timer = Timer()
	private var timerCounter = 0
	private var totalCountDown = 30
	let manager = DocumentManager(manager: .default)
	var temperatureReading = [Reading]()
	var pulseRateReading = [Reading]()
	var breathingRateReading = [Reading]()
	let dispatchGroup = DispatchGroup()
	var timeRange = TimeRange()
	
	required init(uploaderService: DataUploaderWebServiceProtocol, temperatureService: SkinTemperatureWebServiceProtocol, pauseDataService: PauseDataWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, breathingRateService: BreathingRateWebService, delegate: UploadBPReadingViewDelegate) {
		
		self.uploaderService = uploaderService
        self.temperatureService = temperatureService
		self.pauseDataService = pauseDataService
		self.pulseRateService = pulseRateService
		self.breathingRateService = breathingRateService
		self.delegate = delegate
		
		dispatchGroup.notify(queue: DispatchQueue.main, execute: {
			self.delegate?.removeActivityIndicator()
		})
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerValueUpdate), name: Notification.Name(BLE_HT_CharacteristicTemperature.uuidString), object: nil)
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self, name: Notification.Name(BLE_HT_CharacteristicTemperature.uuidString), object: nil)
	}
	
	func startTimer() {
		timeRange.startTime = Date().timeIntervalSince1970
		bluetoothManager.updateNotificationStateToTrue(uuid: TransferService.dateServiceUUID)
//		bluetoothManager.updateNotificationStateToTrue(uuid: BLE_HT_CharacteristicTemperature)
		
		self.delegate?.timer(current: self.totalCountDown - self.timerCounter)
		self.timerCounter = 0
		self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (_) in
			self.delegate?.timer(current: self.totalCountDown - (self.timerCounter + 1))
			self.timerCounter += 1
			self.updatePulseRate()
			self.updateTemperatureReading()
			self.updateBreathingRateReading()
			if self.timerCounter == self.totalCountDown {
				self.stopTimer()
				self.zipFile()
				return
			}
		}
	}
	
	func stopTimer() {
		timeRange.endTime = Date().timeIntervalSince1970
		bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
//		bluetoothManager.updateNotificationStateToFalse(uuid: BLE_HT_CharacteristicTemperature)
		
		self.timer.invalidate()
	}
    
	private func updatePulseRate() {
		
		let newCount = Int.random(in: 88..<93)
		self.pulseRateReading.append(Reading(timeStamp: Date().timeIntervalSince1970, reading: Double(newCount)))
	}
	
	func addPulseRateReading(for timeStamp: TimeInterval) {
		dispatchGroup.enter()
		let pulseRateData = pulseRateReading.getAverageDataArray(for: 5, component: .minute)
		var pulseRateCounts = [PulseRate]()
		for each in pulseRateData {
            pulseRateCounts.append(PulseRate(timeStamp: each.timeStamp, value: Int(each.reading)))
		}
		
		self.pulseRateService.add(models: pulseRateCounts) { [weak self] (response, error) in
            guard let presenterVC = self else {return}
                defer {
                    if(presenterVC.leaveGroup()){
                    presenterVC.dispatchGroup.leave()
                    }else{
                        presenterVC.delegate?.removeActivityIndicator()
                    }
                }
			if let error = error {
				self?.delegate?.postPulseRateErrorHandler(error: error)
				return
			}
			
			if let response = response {
			var reading: [CoreDataPulseRate] = []
				for each in pulseRateCounts {
					CoreDataPulseRate.add(timeInterval: each.timestamp, value: each.value)
					reading.append(CoreDataPulseRate.fetch(timeStamp: each.timestamp)!)
				}
				
				self?.delegate?.postPulseRateSuccess(with: reading)
			}
		}
	}
	
	private func updateTemperatureReading() {
		
		let newCount = Double.random(in: 96.5..<99.0)
		print(newCount)
		self.temperatureReading.append(Reading(timeStamp: Date().timeIntervalSince1970, reading: Double(newCount)))
	}
	
	func addTemperatureReading(for timeStamp: TimeInterval) {
		dispatchGroup.enter()
		let minGoal = UserDefault.shared.goals.temperature.minimum
		let maxGoal = UserDefault.shared.goals.temperature.maximum
		let avgGoal = Double(minGoal + maxGoal) / 2.0
		
		let sumArray = temperatureReading.reduce(0) { (result: Double, nextItem: Reading) -> Double in
			return (result + ((nextItem.reading) - avgGoal))
		}
		var averageTemp = sumArray / Double(temperatureReading.count)
		averageTemp = ((averageTemp * 10).rounded() / 10.0)
		
		if averageTemp.isNaN {
			averageTemp = 0
		}
		
		var readings = [SkinTemperature]()
        readings.append(SkinTemperature(timeStamp: timeStamp, value: (averageTemp + avgGoal)))
		
		self.temperatureService.add(models: readings) { [weak self] (response, error) in
            guard let presenterVC = self else {return}
                defer {
                    if(presenterVC.leaveGroup()){
                    presenterVC.dispatchGroup.leave()
                    }else{
                        presenterVC.delegate?.removeActivityIndicator()
                    }
                }
            if let error = error {
                self?.delegate?.postTemperatureReadingErrorHandler(error: error)
                return
            }
            
            if let response = response {
                self?.delegate?.postTemperatureReadingSuccess(with: response)
            }
        }
    }
	
	private func updateBreathingRateReading() {
		let newCount = Double.random(in: 12..<16)
		print(newCount)
		self.breathingRateReading.append(Reading(timeStamp: Date().timeIntervalSince1970, reading: Double(newCount)))
	}
	
	func addBreathingRateReading(for timeStamp: TimeInterval) {
		dispatchGroup.enter()
		let sumArray = breathingRateReading.reduce(0) { (result: Double, nextItem: Reading) -> Double in
			return result + (nextItem.reading)
		}
		var averageTemp = sumArray / Double(breathingRateReading.count)
		averageTemp = (((averageTemp) * 10).rounded() / 10.0)
		
		var readings = [BreathingRate]()
        readings.append(BreathingRate(timeStamp: timeStamp, value: averageTemp))
		
		self.breathingRateService.add(models: readings) { [weak self] (response, error) in
            guard let presenterVC = self else {return}
                defer {
                    if(presenterVC.leaveGroup()){
                    presenterVC.dispatchGroup.leave()
                    }else{
                        presenterVC.delegate?.removeActivityIndicator()
                    }
                }
			if let error = error {
				self?.delegate?.postBreathingRateErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.postBreathingRateSuccess(with: response)
			}
		}
	}
    
	func zipFile() {
		if let zippedFileUrl = self.manager.uploadDataURL(dataType: .movano, fileName: KeyChain.shared.firstName + "_" + KeyChain.shared.lastName) {
			do {
				let data = try Data(contentsOf: zippedFileUrl)
				
				self.delegate?.zipFile(data: data)
			} catch {
				self.delegate?.zipFile(error: .notAbleToZipFile)
			}
		} else {
			self.delegate?.zipFile(error: .notFetchingData)
		}
	}
	
	func postFileInfo(filename: String, username: String?) {
		dispatchGroup.enter()
		uploaderService.postFileInfo(filename: filename, username: username) { [weak self] (response, error) in
            guard let presenterVC = self else {return}
                defer {
                    if(presenterVC.leaveGroup()){
                    presenterVC.dispatchGroup.leave()
                    }else{
                        presenterVC.delegate?.removeActivityIndicator()
                    }
                }
			if let error = error {
				self?.delegate?.postFileInfoErrorHandler(error: error)
				return
			}
			
			if let response = response {
                self?.delegate?.postFileInfoSuccess(with: response, timeStamp: self?.timeRange.endTime ?? 0)
			}
		}
	}
	
	func uploadFileToPresigned(url: String, fileUrl: URL) {
        dispatchGroup.enter()
		uploaderService.uploadFileToPresigned(url: url, fileUrl: fileUrl) { [weak self] (isProgressCompleted, error) in
            
            guard let presenterVC = self else {return}
                defer {
                    if(presenterVC.leaveGroup()){
                    presenterVC.dispatchGroup.leave()
                    }else{
                        presenterVC.delegate?.removeActivityIndicator()
                    }
                }
            

			if let error = error {
                presenterVC.delegate?.uploadFileToPresignedErrorHandler(error: error)
				return
			}
			
			if isProgressCompleted {
                presenterVC.delegate?.uploadFileToPresignedCompleted()
			}
		}
	}
	
	func putMessageToSns(request: MessageToSnsRequestModel) {
        dispatchGroup.enter()
		uploaderService.putMessageToSns(request: request) { [weak self] (response, error) in
            guard let presenterVC = self else {return}
                defer {
                    if(presenterVC.leaveGroup()){
                    presenterVC.dispatchGroup.leave()
                    }else{
                        presenterVC.delegate?.removeActivityIndicator()
                    }
                }
			if let error = error {
				self?.delegate?.putMessageToSnsErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.putMessageToSnsSuccess(with: response, timeRange: self!.timeRange)
			}
		}
	}
    
    func leaveGroup()->Bool{
        let list = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
        var count = 0
        if(list.count > 0){
            count = Int(list[0]) ?? 0
        }
        
       let leaveGrp =  count > 0 ? true : false
        return leaveGrp
    }
}

// MARK: - Bluetooth related operation
extension UploadBPReadingPresenter {
	
	@objc private func bluetoothManagerValueUpdate(notification: NSNotification) {
		if let object = notification.object as? String, notification.name.rawValue == BLE_HT_CharacteristicTemperature.uuidString {
			let value = object.toDouble!
			self.temperatureReading.append(Reading(timeStamp: Date().timeIntervalSince1970, reading: value))
		}
	}
}
