//
//  ExercisePresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/07/21.
//

import Foundation
import CoreBluetooth
import SwiftQueue

// swiftlint:disable all
class ExercisePresenter: BluetoothService, ExercisePresenterProtocol {
	
	private var uploaderService: DataUploaderWebServiceProtocol
	private var temperatureService: SkinTemperatureWebServiceProtocol
	private var stepCountService: StepCountWebServiceProtocol
	private var pulseRateService: PulseRateWebServiceProtocol
	private var breathingRateService : BreathingRateWebServiceProtocol
	private var oxygenRateService: OxygenDataWebServiceProtocol
    private var exerciseService: ExerciseDataWebServiceProtocol
	private weak var delegate: ExerciseViewDelegate?
	private var timer = Timer()
	private var timerCounter = 0
	private var stepCount = 0
	private var heartRate = 0
	private var breathingRate = 0
	private var spO2 = 0
	private var skinTemp = Float(0)
	private var sessionStepCount = 0
	private var lastNonZeroStepCount = 0
	var temperatureReading = [Reading]()
	var stepCounts = [Reading]()
	var pulseRateReading = [Reading]()
	var breathingRateReading = [Reading]()
	var spO2Reading = [Reading]()
	var skinTempReading = [Reading]()
	let docManager = DocumentManager(manager: .default)
	var timeRange = TimeRange()
	let dispatchGroup = DispatchGroup()
    var didUpdateCalled = false

	required init(uploaderService: DataUploaderWebServiceProtocol, temperatureService: SkinTemperatureWebServiceProtocol, stepCountService: StepCountWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, breathingRateService: BreathingRateWebServiceProtocol, oxygenRateService: OxygenDataWebServiceProtocol, delegate: ExerciseViewDelegate,exerciseService: ExerciseDataWebServiceProtocol) {
		self.uploaderService = uploaderService
		self.temperatureService = temperatureService
		self.stepCountService = stepCountService
		self.pulseRateService = pulseRateService
		self.breathingRateService = breathingRateService
		self.oxygenRateService = oxygenRateService
        self.exerciseService = exerciseService
		self.delegate = delegate
        super.init()

        dispatchGroup.notify(queue: DispatchQueue.main, execute: {[weak self] in
            guard let weakSelf = self else{ return }
            weakSelf.delegate?.removeActivityIndicator()
        })
        NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerDeviceDidDisconnect), name: Notification.Name("deviceDidDisconnect"), object: nil)

		NotificationCenter.default.addObserver(self, selector: #selector(self.bluetoothManagerValueUpdate), name: Notification.Name(BLE_HT_CharacteristicTemperature.uuidString), object: nil)
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self, name: Notification.Name(BLE_HT_CharacteristicTemperature.uuidString), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("deviceDidDisconnect"), object: nil)

	}
    
    @objc private func bluetoothManagerDeviceDidDisconnect(notification: NSNotification) {
        if notification.name.rawValue == "deviceDidDisconnect" {
            self.pauseTimerAndGiveAlert()
            delegate?.showDeviceDisconnectedError()
        }
    }
    
	private var distanceInMiles: Double? {
		if let height = KeyChain.shared.height {
			let stride_length = Double(height)! *  0.413
			return (stride_length * Double(stepCount)).inchToMile()
		}
		return nil
	}
	
    func pauseTimerAndGiveAlert(){
        self.timer.invalidate()
        self.timeRange.endTime = Date().timeIntervalSince1970
        bluetoothManager.lastSessionStepCount = self.sessionStepCount
        bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
    }
	func startTimer() {
		bluetoothManager.updateNotificationStateToTrue(uuid: TransferService.dateServiceUUID)
        Logger.shared.addLog("updateNotificationStateToTrue : \(String(describing: self))")

//		bluetoothManager.updateNotificationStateToTrue(uuid: BLE_HT_CharacteristicTemperature)
		bluetoothManager.lastSessionStepCount = 0
        self.timeRange.startTime = Date().timeIntervalSince1970
        self.timerCounter = 0
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) {[weak self] (_) in
            if let weakSelf = self{
                weakSelf.timerCounter += 1
                if(weakSelf.didUpdateCalled){
                    weakSelf.delegate?.timer(currentStopWatchInstance: self?.timerCounter ?? 0)
                }else{
                    if(weakSelf.timerCounter >= 2){
                        weakSelf.invalidateTimer()
                        weakSelf.delegate?.showWrongDeviceError()
                    }
                }
               
            }else{
                self?.timer.invalidate()
            }
           
        }
	}
	
    override func didUpdate(characteristic: CBCharacteristic) {
        guard let characteristicData = characteristic.value else { return }
        didUpdateCalled = true
        let timeStamp = Date().timeIntervalSince1970
        let byteArray = [UInt8](characteristicData)
        var spo2 = UInt(byteArray[5])
        var heartRate = UInt(byteArray[6])
        var breathingRate = UInt(byteArray[7])
        var steps =  (UInt(byteArray[9]) * 256) + UInt(byteArray[8])
		var skinTempInDegree = ((Double(UInt(byteArray[4]) * 256)) + Double(UInt(byteArray[3]))) / 10
        var distanceCovered = (UInt(byteArray[11]) * 256) + UInt(byteArray[10])
        var calories = (Int(byteArray[13]) * 256) + Int(byteArray[12])
		let battery = UInt(byteArray[2])
		let chargingPercentage = UInt(byteArray[14])
//		if chargingPercentage != 0 {
//			UserDefaults.standard.set(chargingPercentage, forKey: "batteryPercentage")
//		} else {
//			if battery != 0 {
//				UserDefaults.standard.set(battery, forKey: "batteryPercentage")
//			}
//		}
		
		if battery != 0 {
			UserDefaults.standard.set(battery, forKey: "batteryPercentage")
		}
		UserDefaults.standard.set(chargingPercentage, forKey: "isBatteryCharging")
		
		Logger.shared.addLog("Session byteArray : \(byteArray)")
		Logger.shared.addLog("Session Breathing Rate : \(breathingRate)")
		Logger.shared.addLog("Session Heart Rate : \(heartRate)")
		Logger.shared.addLog("Session SpO2 : \(spo2)")
		Logger.shared.addLog("Session Steps : \(steps)")
		
		if spo2 < 90 {
			spo2 = 0
			heartRate = 0
			breathingRate = 0
			
//			skinTempInDegree = 0
//			distanceCovered = 0
//			calories = 0
//			steps = 0
		}
		let skinTemp =  (skinTempInDegree * 9/5) + 32.0
		Logger.shared.addLog("Session Temp : \(skinTemp)")
		
		self.updateStepCount(stepCount: Int(steps), timeStamp: timeStamp)
		self.delegate?.fetchExerciseParameter(stepCount: self.stepCount, lastNonZeroStepCount: self.lastNonZeroStepCount)
        
		self.updateHeartRate(heartRate: Int(heartRate), timeStamp: timeStamp)
        self.delegate?.fetchExerciseParameter(heartRate: self.heartRate)
        
        self.delegate?.fetchExerciseParameter(distance: self.distanceInMiles)
        
		self.updateBreathingRate(breathingRate: Int(breathingRate), timeStamp: timeStamp)
        self.delegate?.fetchExerciseParameter(breathingRate: self.breathingRate)
        
		self.updateSpO2(spo2: Int(spo2), timeStamp: timeStamp)
        self.delegate?.fetchExerciseParameter(spo2: self.spO2)
        
        self.updateSkinTemp(tempValue: Double(skinTemp), timeStamp: timeStamp)
        self.delegate?.fetchExerciseParameter(skinTemp: self.skinTemp)
        
        self.delegate?.fetchExerciseCalorieParameter(stepCount: self.stepCount)
		
		self.delegate?.fetchChargingCurrent(chargingCurrent: Int(chargingPercentage))
    }
    
	func stopTimer() {
        self.timer.invalidate()
		self.timeRange.endTime = Date().timeIntervalSince1970
        bluetoothManager.lastSessionStepCount = self.sessionStepCount
		bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
//		if self.stepCount == 0 {
//			self.stepCount = self.self.sessionStepCount
//		}
//		bluetoothManager.updateNotificationStateToFalse(uuid: BLE_HT_CharacteristicTemperature)
		callAllApisToUploadData()
	}
	
    func storeExerciseDataInDatabase(){
        // Add Step Counts
        let data = stepCounts.getStepCountArray(for: 5, component: .minute)
        var stepCounts = [StepCount]()
        for each in data {
            stepCounts.append(StepCount(timeStamp: each.timeStamp, stepCount: Int(each.reading)))
        }
        for each in stepCounts {
            // Convert steps to calories and
            let heightInInches = KeyChain.shared.height?.toDouble ?? 0.0
            let weight = ((KeyChain.shared.weight?.toDouble ?? 0.0) * 0.453592) // lbs to kg
            let strideLength = UtilityConstant.STRIDE_LENGTH_CONSTANT * heightInInches
            let tempDistanceCoveredKm = (strideLength * Double(each.stepCount)).inchToKm() // inch to KM
            let distanceInMile = (strideLength * Double(each.stepCount)).inchToMile()
            let finalCaloriesBurnt = ((tempDistanceCoveredKm * weight * 1.036) * 100.0) / 100
            
            CoreDataStepCount.add(timeInterval: each.timestamp, value: each.stepCount, calories: finalCaloriesBurnt, distance: distanceInMile)
        }
        self.delegate?.postStepsSuccess(with: lastNonZeroStepCount)
        
        // Add Pulse rate
        let data2 = pulseRateReading.getAverageDataArray(for: 5, component: .minute)
        var pulseReadingArray = [Reading]()
        for x in 0..<pulseRateReading.count {
            pulseReadingArray.append(Reading(timeStamp: pulseRateReading[x].timeStamp, reading: pulseRateReading[x].reading))
        }
        self.delegate?.createChartElement(reading: pulseReadingArray)
        var pulseRateCounts = [PulseRate]()
        for each in data2 {
            if each.reading != 0 {
                pulseRateCounts.append(PulseRate(timeStamp: each.timeStamp, value: Int(each.reading)))
            }
        }
        
        for each in pulseRateCounts {
            CoreDataPulseRate.add(timeInterval: each.timestamp, value: each.value)
        }
        
        // Add breathing rate
        let data3 = breathingRateReading.getAverageDataArray(for: 5, component: .minute)
        var breathingRateCount = [BreathingRate]()
        for each in data3 {
            breathingRateCount.append(BreathingRate(timeStamp: each.timeStamp, value: each.reading))
        }
        
        for each in breathingRateCount {
            CoreDataBreathingRate.add(timeInterval: each.timestamp, value: Double(each.value))
        }
        
        self.delegate?.postBreathingRateSuccess(with: data3)
        
        // Add temperature reading
        let data4 = skinTempReading.getAverageDataArray(for: 5, component: .minute)
        var skinTempReadings = [SkinTemperature]()
        for each in data4 {
            skinTempReadings.append(SkinTemperature(timeStamp: each.timeStamp, value: each.reading))
        }
        
        for each in skinTempReadings {
            CoreDataSkinTemperature.add(timeInterval: each.timestamp, value: Double(each.value))
        }
        self.delegate?.postTempSuccess(with: data4)
        
        // Add spO2 reading
        let data5 = spO2Reading.getAverageDataArray(for: 5, component: .minute)
        var spO2Readings = [SaturatedOxygen]()
        for each in data5 {
            spO2Readings.append(SaturatedOxygen(timeStamp: each.timeStamp, value: Int(each.reading)))
        }
        
        for each in spO2Readings {
            CoreDataOxygen.add(timeInterval: each.timestamp, value: each.value)
        }
        self.delegate?.postSpO2Success(with: data5)
        
        CoreDataExerciseSession.add(startTime: self.timeRange.startTime, endTime: self.timeRange.endTime, stepCount: StepCountCoreData(stepCount: stepCounts),pulseRateData: PulseRateCoreData(pulseRate: pulseRateCounts),breathingRateData: BreathingRateCoreData(breathingRate:breathingRateCount),temperatureData: SkinTemperatureCoreData(skinTemperature: skinTempReadings),oxygenData: SaturatedOxygenCoreData(saturatedOxygen: spO2Readings))
        
        let model = ExerciseReading(startTime: timeRange.startTime, endTime: timeRange.endTime)
         let sessionDataModel : ExerciseSessionData = ExerciseSessionData(timingModel: model, stepsData: stepCounts, pulseRateData: pulseRateCounts, breathingRateData: breathingRateCount, temperatureData: skinTempReadings, oxygenData: spO2Readings)
        scheduleSessionTimeUploadJob(sessionDataModel: sessionDataModel)
    }
    
	func callAllApisToUploadData() {
        storeExerciseDataInDatabase()
        afterAllDataUpdate(error: nil)
	}
    
    func scheduleSessionTimeUploadJob(sessionDataModel:ExerciseSessionData){
       
         let uploadWebServices : BackgroundDataUploadWebServices = BackgroundDataUploadWebServices()
        uploadWebServices.breathingRateService = self.breathingRateService
        uploadWebServices.exerciseService = self.exerciseService
        uploadWebServices.stepCountService = self.stepCountService
        uploadWebServices.pulseRateService = self.pulseRateService
        uploadWebServices.temperatureService = self.temperatureService
        uploadWebServices.oxygenRateService = self.oxygenRateService

                   // dataUploadJobManager.cancelAllOperations()
                    JobBuilder(type: UploadStepsDataJob.type)
                    .priority(priority: .veryHigh)
                    .service(quality: .background)
                    .internet(atLeast: .cellular)
                        .with(params: [UploadSessionTimeJob.SessionDataModel:sessionDataModel as Any, UploadSessionTimeJob.BackgroundDataUploadWebServices:uploadWebServices as Any])
                    .schedule(manager: dataUploadJobManager)

    }
    
    func scheduleBackroundTaskToDataUpload(){
        
        // Add Step Counts
        let data = stepCounts.getStepCountArray(for: 5, component: .minute)
        var stepCounts = [StepCount]()
        for each in data {
            stepCounts.append(StepCount(timeStamp: each.timeStamp, stepCount: Int(each.reading)))
        }

        self.stepCountService.add(models: stepCounts) { [weak self](response, error) in
            guard let presenterVC = self else {return}
            if let response = response {
                var reading: [CoreDataStepCount] = []
                for each in stepCounts {
                    // Convert steps to calories and
                    let heightInInches = KeyChain.shared.height?.toDouble ?? 0.0
                    let weight = ((KeyChain.shared.weight?.toDouble ?? 0.0) * 0.453592) // lbs to kg
                    let strideLength = UtilityConstant.STRIDE_LENGTH_CONSTANT * heightInInches
                    let tempDistanceCoveredKm = (strideLength * Double(each.stepCount)).inchToKm() // inch to KM
                    let distanceInMile = (strideLength * Double(each.stepCount)).inchToMile()
                    let finalCaloriesBurnt = ((tempDistanceCoveredKm * weight * 1.036) * 100.0) / 100
                    
                    CoreDataStepCount.add(timeInterval: each.timestamp, value: each.stepCount, calories: finalCaloriesBurnt, distance: distanceInMile)
                    reading.append(CoreDataStepCount.fetch(timeStamp: each.timestamp)!)
                }
                self?.delegate?.postStepsSuccess(with: presenterVC.lastNonZeroStepCount)
            }
            
            self?.addPulseRate()
        }
    }
    
    func addPulseRate(){
        // Add Pulse rate
        let data2 = pulseRateReading.getAverageDataArray(for: 5, component: .minute)
        var pulseReadingArray = [Reading]()
        for x in 0..<pulseRateReading.count {
            pulseReadingArray.append(Reading(timeStamp: pulseRateReading[x].timeStamp, reading: pulseRateReading[x].reading))
        }
        self.delegate?.createChartElement(reading: pulseReadingArray)
        var pulseRateCounts = [PulseRate]()
		for each in data2 {
			if each.reading != 0 {
                pulseRateCounts.append(PulseRate(timeStamp: each.timeStamp, value: Int(each.reading)))
			}
		}
        
        self.pulseRateService.add(models: pulseRateCounts) { [weak self](response, error) in
            guard let presenterVC = self else {return}
            if let response = response {
                var reading: [CoreDataPulseRate] = []
                for each in pulseRateCounts {
                    CoreDataPulseRate.add(timeInterval: each.timestamp, value: each.value)
                    reading.append(CoreDataPulseRate.fetch(timeStamp: each.timestamp)!)
                }
                
            
            }
            
            presenterVC.addBreathingRate()
        }
    }
    
    func invalidateTimer(){
		bluetoothManager.lastSessionStepCount = self.sessionStepCount
        bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
        self.timer.invalidate()
        
    }
    func addBreathingRate(){
        // Add breathing rate
        let data3 = breathingRateReading.getAverageDataArray(for: 5, component: .minute)
        var breathingRateCount = [BreathingRate]()
        for each in data3 {
            breathingRateCount.append(BreathingRate(timeStamp: each.timeStamp, value: each.reading))
        }
        
        self.breathingRateService.add(models: breathingRateCount) { [weak self](response, error) in
            guard let presenterVC = self else {return}
            if let response = response {
                var reading: [CoreDataBreathingRate] = []
                for each in breathingRateCount {
                    CoreDataBreathingRate.add(timeInterval: each.timestamp, value: Double(each.value))
                    reading.append(CoreDataBreathingRate.fetch(timeStamp: each.timestamp)!)
                }
                
                presenterVC.delegate?.postBreathingRateSuccess(with: data3)
            }
            
            presenterVC.addTemperature()
        }
    }
    
    func addTemperature(){
        // Add temperature reading
        let data4 = skinTempReading.getAverageDataArray(for: 5, component: .minute)
        var skinTempReadings = [SkinTemperature]()
        for each in data4 {
            skinTempReadings.append(SkinTemperature(timeStamp: each.timeStamp, value: each.reading))
        }
        
        self.temperatureService.add(models: skinTempReadings) { [weak self](response, error) in
            guard let presenterVC = self else {return}
            
            if let response = response {
                var reading: [CoreDataSkinTemperature] = []
                for each in skinTempReadings {
                    CoreDataSkinTemperature.add(timeInterval: each.timestamp, value: Double(each.value))
                    reading.append(CoreDataSkinTemperature.fetch(timeStamp: each.timestamp)!)
                }
                presenterVC.delegate?.postTempSuccess(with: data4)
            }
            
            presenterVC.addOxygenData()
        }
    }
    
    func addOxygenData(){
        // Add spO2 reading
        let data5 = spO2Reading.getAverageDataArray(for: 5, component: .minute)
        var spO2Readings = [SaturatedOxygen]()
        for each in data5 {
            spO2Readings.append(SaturatedOxygen(timeStamp: each.timeStamp, value: Int(each.reading)))
        }
        
        self.oxygenRateService.add(models: spO2Readings) { [weak self](response, error) in
            guard let presenterVC = self else {return}

            if let response = response {
                var reading: [CoreDataOxygen] = []
                for each in spO2Readings {
                    CoreDataOxygen.add(timeInterval: each.timestamp, value: each.value)
                    reading.append(CoreDataOxygen.fetch(timeStamp: each.timestamp)!)
                }
                presenterVC.delegate?.postSpO2Success(with: data5)
            }
			presenterVC.afterAllDataUpdate(error: error)
        }
    }

    
	func afterAllDataUpdate(error: MovanoError?){
        delegate?.removeActivityIndicator()
        delegate?.navigateToDetailsScreen(timeRange: timeRange,error: error)
    }
 
	private func updateStepCount(stepCount:Int, timeStamp : TimeInterval) {
		Logger.shared.addLog("Logs: last session step count is : \(BluetoothManager.shared.lastSessionStepCount)")
		Logger.shared.addLog("Logs: received step count from device is : \(stepCount)")
		var newCount = 0
		if stepCount != 0 {
			if(BluetoothManager.shared.lastSessionStepCount != 0) {
				if(self.stepCount == 0) {
					if stepCount > BluetoothManager.shared.lastSessionStepCount {
						newCount = stepCount - BluetoothManager.shared.lastSessionStepCount
					} else {
						newCount = stepCount
					}
				} else {
					if stepCount > (BluetoothManager.shared.lastSessionStepCount - self.stepCount) {
						newCount = stepCount - BluetoothManager.shared.lastSessionStepCount - self.stepCount
					} else {
						newCount = stepCount
					}
				}
				if stepCount > BluetoothManager.shared.lastSessionStepCount {
					self.stepCount = stepCount - BluetoothManager.shared.lastSessionStepCount
				} else {
					self.stepCount = stepCount
				}
				
			} else {
				if(self.stepCount == 0) {
					newCount = stepCount
				} else {
					if stepCount > self.stepCount {
						newCount = stepCount - self.stepCount
					} else {
						newCount = stepCount
					}
				}
				self.stepCount = stepCount
			}
			sessionStepCount = stepCount
			if newCount != 2 && newCount != 0 && newCount != 1 {
				self.stepCounts.removeAll()
				self.stepCounts.append(Reading(timeStamp: timeStamp, reading: Double(newCount)))
			} else {
				self.stepCounts.append(Reading(timeStamp: timeStamp, reading: Double(newCount)))
			}
			
			lastNonZeroStepCount = self.stepCount
		} else {
			self.stepCount = 0
		}
		Logger.shared.addLog("Logs: stored step count for upload is : \(self.stepCount)")
//		if stepCount == 0 {
//			if(BluetoothManager.shared.lastSessionStepCount != 0) {
//
//			}
//
//		}
	}
	
    private func updateHeartRate(heartRate:Int, timeStamp : TimeInterval) {
		self.heartRate = heartRate
		self.pulseRateReading.append(Reading(timeStamp: Date().timeIntervalSince1970, reading: Double(heartRate)))
	}
	
    private func updateBreathingRate(breathingRate:Int, timeStamp : TimeInterval) {
		self.breathingRate = breathingRate
		if breathingRate != 0 {
			self.breathingRateReading.append(Reading(timeStamp: Date().timeIntervalSince1970, reading: Double(breathingRate)))
		}
	}
	
    private func updateSpO2(spo2:Int, timeStamp : TimeInterval) {
		self.spO2 = spo2
		if spo2 != 0 {
			self.spO2Reading.append(Reading(timeStamp: Date().timeIntervalSince1970, reading: Double(spo2)))
		}
	}
	
    private func updateSkinTemp(tempValue:Double, timeStamp : TimeInterval) {
        let tempValue1 = (Utility.getTempVariation(temperature: tempValue) * 10).rounded() / 10
        self.skinTemp = Float(tempValue)
		if tempValue != 32.0 {
			self.skinTempReading.append(Reading(timeStamp: Date().timeIntervalSince1970, reading: tempValue))
		}
	}
	
	func postFileInfo(filename: String, username: String?) {
        dispatchGroup.enter()
		uploaderService.postFileInfo(filename: filename, username: username) {[weak self] (response, error) in
            guard let presenterVC = self else {return}
                defer {
                    if(presenterVC.leaveGroup()){
                    presenterVC.dispatchGroup.leave()
                    }else{
                        presenterVC.delegate?.removeActivityIndicator()
                    }
                }
			if let error = error {
				self?.delegate?.postFileInfoErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.postFileInfoSuccess(with: response, timeStamp: self?.timeRange.endTime ?? 0)
			}
		}
	}
	
	func uploadFileToPresigned(url: String, fileUrl: URL) {
		self.dispatchGroup.enter()
		uploaderService.uploadFileToPresigned(url: url, fileUrl: fileUrl) { [weak self] (isProgressCompleted, error) in
            guard let presenterVC = self else {return}
                defer {
                    if(presenterVC.leaveGroup()){
                    presenterVC.dispatchGroup.leave()
                    }else{
                        presenterVC.delegate?.removeActivityIndicator()
                    }
                }
			if let error = error {
				self?.delegate?.uploadFileToPresignedErrorHandler(error: error)
				return
			}
			
			if isProgressCompleted {
				self?.delegate?.uploadFileToPresignedCompleted()
			}
		}
	}
	
	func putMessageToSns(request: MessageToSnsRequestModel) {
		self.dispatchGroup.enter()
		uploaderService.putMessageToSns(request: request) { [weak self] (response, error) in
            guard let presenterVC = self else {return}
                defer {
                    if(presenterVC.leaveGroup()){
                    presenterVC.dispatchGroup.leave()
                    }else{
                        presenterVC.delegate?.removeActivityIndicator()
                    }
                }
			if let error = error {
				self?.delegate?.putMessageToSnsErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.delegate?.putMessageToSnsSuccess(timeRange: self!.timeRange)
			}
		}
	}
    
    func leaveGroup()->Bool{
        let list = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
        var count = 0
        if(list.count > 0){
            count = Int(list[0]) ?? 0
        }
        
       let leaveGrp =  count > 0 ? true : false
        return leaveGrp
    }
	
	func fetchHeartRateData(range: DateRange) {
		
	}
}

// MARK: - Bluetooth related operation
extension ExercisePresenter {

	@objc private func bluetoothManagerValueUpdate(notification: NSNotification) {
		if let object = notification.object as? String, notification.name.rawValue == BLE_HT_CharacteristicTemperature.uuidString {
			let value = object.toDouble!
			self.temperatureReading.append(Reading(timeStamp: Date().timeIntervalSince1970, reading: value))
		}
	}
    
}
