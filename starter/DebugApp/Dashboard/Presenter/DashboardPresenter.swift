//
//  DashboardPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 03/07/21.
//

import Foundation
import CoreBluetooth
import UIKit
import SwiftUI

// swiftlint:disable all
class DashboardPresenter: DashboardPresenterProtocol {
	
	private weak var delegate: DashboardViewDelegate?
    private var dashboardDataService: DashboradDataWebServiceProtocol
    private var breathingRateService: BreathingRateWebServiceProtocol
    private var bloodPressureService: BloodPressureWebServiceProtocol
    private var pulseRateService: PulseRateWebServiceProtocol
    private var temperatureRateService: SkinTemperatureWebServiceProtocol
    private var sleepService: SleepWebServiceProtocol
    private var stepCountService: StepCountWebServiceProtocol
    private var oxygenService: OxygenDataWebServiceProtocol

    required init(breathingRateService: BreathingRateWebServiceProtocol, bloodPressureService: BloodPressureWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, temperatureRateService: SkinTemperatureWebServiceProtocol, sleepService: SleepWebServiceProtocol, stepCountService: StepCountWebServiceProtocol,oxygenService: OxygenDataWebServiceProtocol,dashboardDataService: DashboradDataWebServiceProtocol, delegate: DashboardViewDelegate) {
        self.breathingRateService = breathingRateService
        self.bloodPressureService = bloodPressureService
        self.pulseRateService = pulseRateService
        self.temperatureRateService = temperatureRateService
        self.delegate = delegate
        self.sleepService = sleepService
        self.oxygenService = oxygenService
        self.stepCountService = stepCountService
        self.dashboardDataService = dashboardDataService
    }
    
    func fetchDailyAvgData(forDate:Date) {
        
        self.dashboardDataService.fetchDashboradData(forDate:forDate.dateToString(format: DateFormat.serverDateFormat)){ [weak self] (dailyAvgResp, error) in
            
            if let error = error {
                self?.delegate?.afterDailyAvgDataResponseFetched(result: .failure(error))
                return
            }
            
            if let _dailyAvgResp = dailyAvgResp {
                self?.delegate?.afterDailyAvgDataResponseFetched(result: .success(_dailyAvgResp))
            }
        }
	}
 
    func fetchAllDataPoints(forRange:Range<Double>,forStepCountRange:Range<Double>){
        self.dashboardDataService.fetchAllDataPoints(timestampRange: forRange,forStepCountRange: forStepCountRange){ [weak self] (allDataPoints,error) in

            if let error = error {
                self?.delegate?.afertAllPointsDataFetched(result: .failure(error))
                return
            }
            
            var bpDashboardDataModel:DashboardDataModel = DashboardDataModel(parameter: Parameter(name: "Nocturnal Blood Pressure"), image: #imageLiteral(resourceName: "BP"))
            var sleepDashboardDataModel:DashboardDataModel = DashboardDataModel(parameter: Parameter(name: "Sleep"), image: #imageLiteral(resourceName: "Sleep"))
            var stepsDashboardDataModel:DashboardDataModel =  DashboardDataModel(parameter: Parameter(name: "Steps"), image: #imageLiteral(resourceName: "Steps"))
            var scoreAffectingMetrics:[MetricIndicatorModel] = []

            if let allDataPoints = allDataPoints{
                var dataPointsAvailable = 0
                var sleepScore:Double = 0.0
                var pulseRateScore:Double = 0.0
               // var hrvScore:Double = 0.0
                var spo2Score:Double = 0.0
                var breathingRateScore:Double = 0.0
                var temperatureScore:Double = 0.0
                var systolicBPScore:Double = 0.0
                var diastolicBPScore:Double = 0.0

                //Calculate sleep score
                if let goalSleepHr = UserDefault.shared.goals.sleepHour.value as? Range<Int>, let sleepDataPoints = allDataPoints.coreDataSleepStageList{
                    if(sleepDataPoints.count > 0){
                        let goalSleepMinutes = Double(goalSleepHr.upperBound * 60)
                       // let valuesInSleepRange = sleepDataPoints.filter { $0?.sleepStage ?? 0 >= goalSleepMinutes }.count
                        let achievedSleepMinutes = Double(sleepDataPoints.count * 5)
                        if(achievedSleepMinutes >= goalSleepMinutes){
                            sleepScore = 10.0
                        }else{
                            sleepScore = ((achievedSleepMinutes/goalSleepMinutes) * 100) / 10
                        }

                        if(sleepScore < 8){
                            let sleepStr = "You slept for \(Utility.convertminutesToHMFormat(sleepDataPoints.count * 5)), your target sleep was \(Utility.convertminutesToHMFormat(goalSleepHr.upperBound * 60))"

							scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: (sleepScore < 6 ? .alertRed : .alertYellow), metricType: .sleep, metricNegativeScore: sleepStr))
                        }
                        dataPointsAvailable += 1
                        
                        
                        //Create Sleep Dashboard data model
                        var sleepSldierParamObj = SliderViewParam()
                        var isSleepGoalAchieved = false
                        
                            if(achievedSleepMinutes > goalSleepMinutes){
                                //Actual sleep hrs are more than goal set
                                isSleepGoalAchieved = true
                                sleepSldierParamObj.showIndicator = true
                                sleepSldierParamObj.leftBarColor = Color.blue
                                sleepSldierParamObj.rightBarColor = Color.blue
                                sleepSldierParamObj.goalValue = "\(Int(goalSleepMinutes/60))h"
                                sleepSldierParamObj.value = Double((100 * goalSleepMinutes)/achievedSleepMinutes)
                            }else if(achievedSleepMinutes == goalSleepMinutes){
                                //Sleep time is same as goal set
                                isSleepGoalAchieved = true
                                sleepSldierParamObj.showIndicator = false
                                sleepSldierParamObj.leftBarColor = Color.blue
                                sleepSldierParamObj.rightBarColor = Color.blue
                                sleepSldierParamObj.goalValue = "\(Int(goalSleepMinutes/60))h"
                                sleepSldierParamObj.value = 100.0
                            } else {
                               //Sleep time is less than goal set
                                if(achievedSleepMinutes == 0){
                                    sleepSldierParamObj.leftBarColor = Color(UIColor(named: "DarkGreyBlue") ?? UIColor.darkGray)
                                }else{
                                    sleepSldierParamObj.leftBarColor = Color.blue
                                }
                                sleepSldierParamObj.showIndicator = false
                                sleepSldierParamObj.rightBarColor = Color(UIColor(named: "DarkGreyBlue") ?? UIColor.darkGray)
                                sleepSldierParamObj.goalValue = "\(Int(goalSleepMinutes/60))h"
                                sleepSldierParamObj.value = Double((100 * achievedSleepMinutes)/goalSleepMinutes)
                            }
                        
                        let unit = Utility.convertminutesToHMFormat(Int(goalSleepMinutes))
						sleepDashboardDataModel = DashboardDataModel(parameter: Parameter(name: "Sleep", reading: Utility.convertminutesToHMFormat(Int(achievedSleepMinutes)), unit: "/\(unit)", iconName: ""), image: #imageLiteral(resourceName: "Sleep"), change: nil,sliderViewParam: sleepSldierParamObj, isGoalAchieved: isSleepGoalAchieved,metricType: .sleep)
                        
                    }
                }

                //Calculate Pulse Rate Score
                if let goalPulseRate = UserDefault.shared.goals.pulseRate.value as? Range<Int>, let pulseRateDataPoints = allDataPoints.coreDataPulseRateList{
                    if(pulseRateDataPoints.count > 0){
                        let valuesInGoalRange = Double(pulseRateDataPoints.filter{ Int(truncating: $0.value) >= goalPulseRate.lowerBound && Int(truncating: $0.value) <= goalPulseRate.upperBound}.count)

                        pulseRateScore = ((valuesInGoalRange/Double(pulseRateDataPoints.count)) * 100) / 10

                        if(pulseRateScore < 8){
                            let metrciImpactStr = "Your Heart Rate was outside your target range for \(String(format: "%.01f", (10-pulseRateScore)*10)) % of time."

                            scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: (pulseRateScore < 6 ? .alertRed : .alertYellow), metricType: .pulseRate, metricNegativeScore: metrciImpactStr))
                        }
                        dataPointsAvailable += 1

                    }
                }

                //Calculate spo2 score
                if let gloalSpo2 = UserDefault.shared.goals.oxygen.value as? Range<Int>, let spo2DataPoints = allDataPoints.coreDataOxygenList{
                    if(spo2DataPoints.count > 0){
                        let valuesInSpo2Range = spo2DataPoints.filter{ Int(truncating: $0.value) >= gloalSpo2.lowerBound && Int(truncating: $0.value) <= gloalSpo2.upperBound}.count

                        spo2Score = ((Double(valuesInSpo2Range)/Double(spo2DataPoints.count)) * 100) / 10
                        if(spo2Score < 8){
                            let metrciImpactStr = "Your Oxygen Rate was outside your target range for \(String(format: "%.01f", (10-spo2Score)*10)) % of time."

                            scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: (spo2Score < 6 ? .alertRed : .alertYellow), metricType: .oxygen, metricNegativeScore: metrciImpactStr))
                        }
                        dataPointsAvailable += 1

                    }

                }

                //Calculate breathing rate score
                if let goalBreathingRate = UserDefault.shared.goals.breathingRate.value as? Range<Int>, let breathingRateDataPoints = allDataPoints.coreDataBreathingRateList{
                    if(breathingRateDataPoints.count > 0){
                        let valuesInGoalRange = breathingRateDataPoints.filter{ Int(truncating: $0.value) >= goalBreathingRate.lowerBound && Int(truncating: $0.value) <= goalBreathingRate.upperBound}.count

                        breathingRateScore = ((Double(valuesInGoalRange)/Double(breathingRateDataPoints.count)) * 100) / 10

                        if(breathingRateScore < 8){
                            let metrciImpactStr = "Your Breathing Rate was outside your target range for \(String(format: "%.01f", (10-breathingRateScore)*10)) % of time."

                            scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: (breathingRateScore < 6 ? .alertRed : .alertYellow), metricType: .breathingRate,metricNegativeScore: metrciImpactStr))
                        }
                        dataPointsAvailable += 1

                    }

                }

                //Calculate temperature score
                if let goalTemp = UserDefault.shared.goals.temperature.value as? Range<Int>, let tempDataPoints = allDataPoints.coreDataSkinTemperatureList{
                    if(tempDataPoints.count > 0){
                        let valuesInGoalRange = tempDataPoints.filter{
                            Double(truncating: $0.value) >= Double(goalTemp.lowerBound) && Double(truncating: $0.value) <= Double(goalTemp.upperBound)
                        }.count

                        temperatureScore = ((Double(valuesInGoalRange)/Double(tempDataPoints.count)) * 100) / 10


                        if(temperatureScore < 8){
                            let metrciImpactStr = "Your Skin Temperature was outside your target range for \(String(format: "%.01f", (10-temperatureScore)*10)) % of time."

                            scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: (temperatureScore < 6 ? .alertRed : .alertYellow), metricType: .temperature, metricNegativeScore: metrciImpactStr))
                        }
                        dataPointsAvailable += 1

                    }

                }

                //Calculate Systolic and Diastolic blood pressure score
                if let systolicGoalBPVlaue = UserDefault.shared.goals.systolic.value as? Range<Int>,let diastolicGoalValue = UserDefault.shared.goals.diastolic.value as? Range<Int>, let bpDataPoints = allDataPoints.coreDataBPReadingList{
                    if(bpDataPoints.count > 0){
                        var isGoalAchieved = false
                        let systolicValuesInGoalRange = bpDataPoints.filter{ Int(truncating: $0.systolic) >= systolicGoalBPVlaue.lowerBound && Int(truncating: $0.systolic) <= systolicGoalBPVlaue.upperBound}.count

                        let diastolicValuesInGoalRange = bpDataPoints.filter{ Int(truncating: $0.diastolic) >= diastolicGoalValue.lowerBound && Int(truncating: $0.diastolic) <= diastolicGoalValue.upperBound}.count

                        systolicBPScore = ((Double(systolicValuesInGoalRange)/Double(bpDataPoints.count)) * 100) / 10
//                        if(systolicBPScore < 8){
//                            let metrciImpactStr = "Your Systolic BP was outside your target range for \(String(format: "%.01f", (10-systolicBPScore)*10)) % of time."
//
//                            scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: (systolicBPScore < 6 ? UIImage(named: "ErrorOutlineRed") : UIImage(named: "ErrorOutlineYellow")) ?? UIImage.init(), metricType: .bloodPressure, metricNegativeScore: metrciImpactStr))
//                        }
                        dataPointsAvailable += 1

                        diastolicBPScore = ((Double(diastolicValuesInGoalRange)/Double(bpDataPoints.count)) * 100) / 10

//                        if(diastolicBPScore < 8){
//                            let metrciImpactStr = "Your Diastolic BP was outside your target range for \(String(format: "%.01f", (10-diastolicBPScore)*10)) % of time."
//
//                            scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: (diastolicBPScore < 6 ? UIImage(named: "ErrorOutlineRed") : UIImage(named: "ErrorOutlineYellow")) ?? UIImage.init(), metricType: .bloodPressure, metricNegativeScore: metrciImpactStr))
//                        }
						var indicatorImage = ImageType.noData
                        var metrciImpactStr = ""

                        if(diastolicBPScore < 6 || systolicBPScore < 6){
							indicatorImage = .alertRed
                        }else if(diastolicBPScore < 8 || systolicBPScore < 8){
							indicatorImage = .alertYellow
                        }

                        if(diastolicBPScore < 8 && systolicBPScore < 8){
                            metrciImpactStr = "Your Diastolic BP was outside your target range for \(String(format: "%.01f", (10-diastolicBPScore)*10)) % of time and your Systolic BP was outside your target range for \(String(format: "%.01f", (10-systolicBPScore)*10)) % of time."
                        }else if(diastolicBPScore < 8){
                            metrciImpactStr = "Your Diastolic BP was outside your target range for \(String(format: "%.01f", (10-diastolicBPScore)*10)) % of time."
                        }else if(systolicBPScore < 8){
                            metrciImpactStr = "Your Systolic BP was outside your target range for \(String(format: "%.01f", (10-systolicBPScore)*10)) % of time."
                        }
                        
                        if(diastolicBPScore < 8 || systolicBPScore < 8){
                            scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: indicatorImage, metricType: .bloodPressure, metricNegativeScore: metrciImpactStr))
                        }
                        dataPointsAvailable += 1
                        let diastolicSum = bpDataPoints.reduce(0) { $0 + Int(truncating: $1.diastolic) }
                        
                        //Create Dashboard dataModel for BP
						let diastolicaTotal = Int((Double(diastolicSum)/Double(bpDataPoints.count)).rounded())
						//bpDataPoints.reduce(0) { (result: Int, nextItem: CoreDataBPReading) -> Int in
//                            return Int(truncating: nextItem.diastolic)
//                        }
                        let systolicSum = bpDataPoints.reduce(0) { $0 + Int(truncating: $1.systolic) }

						let systolicaTotal = Int((Double(systolicSum)/Double(bpDataPoints.count)).rounded())
						//bpDataPoints.reduce(0) { (result: Int, nextItem: CoreDataBPReading) -> Int in
//                            return Int(truncating: nextItem.systolic)
//                        }
						if (systolicBPScore == 10 && diastolicBPScore == 10) {
							isGoalAchieved = true
						}
						
                        let bpSliderParamOjb = BPSliderViewParam(systolicValue: Double(systolicaTotal), diastolicValue: Double(diastolicaTotal), systolicLeftBarColor: Color(UIColor(named: "DarkGreyBlue") ?? UIColor.darkGray), diastolicLeftBarColor: Color.blue, systolicRightBarColor: Color(UIColor(named: "BPCustomCompLeftBar") ?? UIColor.blue), diastolicRightBarColor: Color(UIColor(named: "DarkGreyBlue") ?? UIColor.darkGray), systolicGoalValue: String(describing: systolicGoalBPVlaue.upperBound), diastolicgoalValue: String(describing: diastolicGoalValue.upperBound))
                        
						bpDashboardDataModel = DashboardDataModel(parameter: Parameter(name: "Nocturnal Blood Pressure", reading: "\(String(describing: systolicaTotal))/\(String(describing: diastolicaTotal))", unit: "mm hg", iconName: "sleep-BPIcon"), image: #imageLiteral(resourceName: "BP"), change: nil,bpSliderViewParam: bpSliderParamOjb, isGoalAchieved: isGoalAchieved,metricType: .nocturnalBP)
                    }
                }

                //Create Dashborad data model for stepcount
                if let stepCountDataList = allDataPoints.coreDataStepCountList{
                    var stepSldierParamObj = SliderViewParam()
                    var isStepGoalAchieved = false
                    var goalSteps = 0
                    let acheivedStepCount = stepCountDataList.reduce(0) { (result: Int, nextItem: CoreDataStepCount) -> Int in
                        return result + nextItem.value.intValue
                    }
                   if let goalStepCount = UserDefault.shared.goals.stepCountGoal.value as? Int {
                     goalSteps = goalStepCount
                    if(acheivedStepCount > goalStepCount){
                            //Actual sleep hrs are more than goal set
                            isStepGoalAchieved = true
                            stepSldierParamObj.showIndicator = true
                            stepSldierParamObj.leftBarColor = Color.blue
                            stepSldierParamObj.rightBarColor = Color.blue
                            stepSldierParamObj.goalValue = "\(goalStepCount/1000)k"
                            stepSldierParamObj.value = Double((100 * goalStepCount)/acheivedStepCount)
                        }else if(acheivedStepCount == goalStepCount){
                            //Sleep time is same as goal set
                            isStepGoalAchieved = true
                            stepSldierParamObj.showIndicator = false
                            stepSldierParamObj.leftBarColor = Color.blue
                            stepSldierParamObj.rightBarColor = Color.blue
                            stepSldierParamObj.goalValue = "\(goalStepCount/1000)k"
                            stepSldierParamObj.value = 100.0
                        } else {
                           //Sleep time is less than goal set
                            if(acheivedStepCount == 0){
                                stepSldierParamObj.leftBarColor = Color(UIColor(named: "DarkGreyBlue") ?? UIColor.darkGray)
                            }else{
                                stepSldierParamObj.leftBarColor = Color.blue

                            }
                            stepSldierParamObj.showIndicator = false
                            stepSldierParamObj.rightBarColor = Color(UIColor(named: "DarkGreyBlue") ?? UIColor.darkGray)
                            stepSldierParamObj.goalValue = "\(goalStepCount/1000)k"
                            stepSldierParamObj.value = Double((100 * acheivedStepCount)/goalStepCount)
                        }
                    }
					stepsDashboardDataModel = DashboardDataModel(parameter: Parameter(name: "Steps", reading: "\(acheivedStepCount)", unit: "/\(goalSteps)", iconName: "sleep_StepsIcon"), image: #imageLiteral(resourceName: "Steps"), change: nil,sliderViewParam: stepSldierParamObj, isGoalAchieved: isStepGoalAchieved,metricType: .steps)
                }
                
                
                
                let totalScore = ((sleepScore + pulseRateScore + spo2Score + breathingRateScore + temperatureScore + systolicBPScore + diastolicBPScore)/7)
                let formatter = NumberFormatter()
                formatter.maximumFractionDigits = 1
                formatter.roundingMode = .down
                var scoreInString = formatter.string(from: NSNumber.init(value: totalScore))
              //  let roundedScoreValue = round(totalScore * 10) / 10.0
                if(dataPointsAvailable == 0){
                    scoreAffectingMetrics.removeAll()
					scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: .noData, metricType: .noData, metricNegativeScore: ""))
                    scoreInString = "--"
                }else if(dataPointsAvailable > 0 && dataPointsAvailable != 7){
                    scoreAffectingMetrics.removeAll()
					scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: .alertYellow, metricType: .noSufficientData, metricNegativeScore: ""))
                    scoreInString = "--"
                }else if(scoreAffectingMetrics.count == 0){
                    scoreAffectingMetrics.removeAll()
					scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: .alertGreen, metricType: .allGood, metricNegativeScore: ""))
                }else if(scoreAffectingMetrics.count > 2){
                    scoreAffectingMetrics.removeAll()
					scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: .alertRed, metricType: .generic, metricNegativeScore: ""))
                }

                self?.delegate?.afertAllPointsDataFetched(result: .success(HealthScoreModel(healthScore: scoreInString ?? "--", metricIndicatorsList: scoreAffectingMetrics,dashBoardDataModelList: [bpDashboardDataModel,sleepDashboardDataModel,stepsDashboardDataModel])))
            }else{
                scoreAffectingMetrics.removeAll()
				scoreAffectingMetrics.append(MetricIndicatorModel(indicatorImage: .noData, metricType: .noData, metricNegativeScore: ""))
                self?.delegate?.afertAllPointsDataFetched(result: .success(HealthScoreModel(healthScore: "--", metricIndicatorsList: scoreAffectingMetrics,dashBoardDataModelList: [bpDashboardDataModel,sleepDashboardDataModel,stepsDashboardDataModel])))


            }

        }

    }

    
}
