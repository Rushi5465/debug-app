//
//  BPReadingPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 02/07/21.
//

import Foundation

class BPReadingPresenter: BPReadingPresenterProtocol {
	
	private var bpService: BloodPressureWebServiceProtocol
	private var pauseDataService: PauseDataWebServiceProtocol
	private weak var delegate: BloodPressureViewDelegate?
	
	var timer = Timer()
	var timerCounter = 0
	
	required init(bpService: BloodPressureWebServiceProtocol, pauseDataService: PauseDataWebServiceProtocol, delegate: BloodPressureViewDelegate) {
		self.bpService = bpService
		self.pauseDataService = pauseDataService
		self.delegate = delegate
	}
	
	func fetchParameters() {
		let step = Parameter(name: "BLOOD PRESSURE", reading: nil, unit: "mmHg",iconName: "sleep-BPIcon")
		let distance = Parameter(name: "HEART RATE", reading: nil, unit: "bpm",iconName: "sleep-HeartIcon")
		let floorClimbed = Parameter(name: "SKIN TEMP VAR", reading: nil, unit: "°f",iconName: "sleep_SkintempIcon")
		let breathingRate = Parameter(name: "BREATHING RATE", reading: nil, unit: "brpm",iconName: "sleep-BreathingIcon")
		
		self.delegate?.parameter(list: [step, distance, floorClimbed, breathingRate])
	}
	
	private func getBloodPressureDataByTimeStamp(for patient: Patient, timestamp: Double) {
		bpService.getBloodPressureDataByTimeStamp(for: patient, timestamp: timestamp) { [weak self] (response, error) in
			if let error = error {
				self?.delegate?.getBpByTimeStampErrorHandler(error: error)
				return
			}
			
			if let response = response {
				self?.timer.invalidate()
				self?.delegate?.getBpByTimeStampSuccess(with: response)
			}
		}
	}
	
	func fetchReading(request: MessageToSnsRequestModel) {
		self.timerCounter = 0
		
		self.timer = Timer.scheduledTimer(withTimeInterval: 4, repeats: true) { (timer) in
			if self.timerCounter == 15 {
				timer.invalidate()
				let error = MovanoError.requestTimeOut
				self.delegate?.getBpByTimeStampErrorHandler(error: error)
				return
			}
			self.timerCounter += 1
			let patient = Patient(id: KeyChain.shared.idToken, name: KeyChain.shared.firstName + "_" + KeyChain.shared.lastName)
			self.getBloodPressureDataByTimeStamp(for: patient, timestamp: Double(request.file_timestamp))
		}
	}
	
	func addPauseData(timeRange: TimeRange) {
		
		let model = PauseReading(startTime: timeRange.startTime, endTime: timeRange.endTime)
		self.pauseDataService.add(model: model) { [weak self] (item, error) in
			if let error = error {
				self?.delegate?.postPauseDataErrorHandler(error: error)
				return
			}
			
			if let item = item {
				self?.delegate?.postPauseDataSuccess(with: item)
			}
		}
	}
}
