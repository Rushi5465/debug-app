//
//  Reading.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 19/07/21.
//

import Foundation

struct Reading {
	
	let timeStamp: TimeInterval
	var reading: Double
}
