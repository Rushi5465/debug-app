//
//  FetchPresignResponseModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/06/21.
//

import Foundation

struct FetchPresignResponseModel: Decodable {
	var message: String
	var status_code: Int
	var data: FetchPresignDataModel
}
