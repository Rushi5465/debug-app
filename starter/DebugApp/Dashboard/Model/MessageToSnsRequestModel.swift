//
//  MessageToSnsRequestModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/06/21.
//

import Foundation

struct MessageToSnsRequestModel: Encodable {
	
	var file_name: String
	var folder_name: String
	var file_timestamp: Int
}
