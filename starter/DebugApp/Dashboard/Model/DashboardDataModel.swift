//
//  DashboardDataModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 23/07/21.
//

import UIKit

struct DashboardDataModel {
	
	var parameter: Parameter
	var image: UIImage
	var change: Int?
    var sliderViewParam:SliderViewParam?
    var bpSliderViewParam:BPSliderViewParam?
    var isGoalAchieved:Bool
    var metricType:MetricType

    init(parameter:Parameter,image:UIImage) {
        self.parameter = parameter
        self.image = image
        self.change = 0
        self.sliderViewParam = nil
        self.isGoalAchieved = false
        self.metricType = .generic
    }
    
    init(parameter:Parameter,image:UIImage,change:Int?,sliderViewParam:SliderViewParam,bpSliderViewParam:BPSliderViewParam,isGoalAchieved:Bool,metricType:MetricType) {
        self.parameter = parameter
        self.image = image
        self.change = change
        self.sliderViewParam = sliderViewParam
        self.isGoalAchieved = isGoalAchieved
        self.metricType = metricType
        self.bpSliderViewParam = bpSliderViewParam
        
    }
    init(parameter:Parameter,image:UIImage,change:Int?,sliderViewParam:SliderViewParam,isGoalAchieved:Bool,metricType:MetricType) {
        self.parameter = parameter
        self.image = image
        self.change = change
        self.sliderViewParam = sliderViewParam
        self.isGoalAchieved = isGoalAchieved
        self.metricType = metricType
        
    }
    
    init(parameter:Parameter,image:UIImage,change:Int?,bpSliderViewParam:BPSliderViewParam,isGoalAchieved:Bool,metricType:MetricType) {
        self.parameter = parameter
        self.image = image
        self.change = change
        self.bpSliderViewParam = bpSliderViewParam
        self.isGoalAchieved = isGoalAchieved
        self.metricType = metricType
    }
}

struct HealthScoreModel{
    let healthScore: String
    let metricIndicatorsList:[MetricIndicatorModel]
    let dashBoardDataModelList:[DashboardDataModel]
}
struct MetricIndicatorModel {
    let indicatorImage:ImageType
    let metricType:MetricType
    let metricNegativeScore:String
}

enum ImageType: String {
	case alertRed = "ErrorOutlineRed"
	case alertYellow = "ErrorOutlineYellow"
	case alertGreen = "IndicatorGreen"
	case noData = ""
}

enum MetricType: String {
    case bloodPressure = "Blood Pressure"
    case temperature = "Skin Temperature"
    case breathingRate = "Breathing Rate"
    case pulseRate = "Heart Rate"
    case oxygen = "SpO2"
    case sleep = "Sleep"
    case hrv = "HRV"
    case generic = "Need attention"
    case allGood = "All good !"
    case steps = "Steps"
    case nocturnalBP = "Nocturnal"
    case noSufficientData = "No sufficient data available"
    case noData = ""
}

struct AllDataPointsModel {
    var coreDataBPReadingList : [CoreDataBPReading]?
    var coreDataBreathingRateList:[CoreDataBreathingRate]?
    var coreDataPulseRateList:[CoreDataPulseRate]?
    var coreDataSkinTemperatureList:[CoreDataSkinTemperature]?
    var coreDataOxygenList:[CoreDataOxygen]?
    var coreDataSleepStageList:[CoreDataSleepStage]?
    var coreDataStepCountList:[CoreDataStepCount]?
}

struct DailyAverageModel: Codable {
    let average:Double?
    let total:Double?
    let count:Double?
    
    init() {
        average = 0.0
        total = 0.0
        count = 0.0
    }
	
	init(average: Double?) {
		self.average = average
		self.total = 0
		self.count = 0
	}
	
    enum CodingKeys: String, CodingKey {
        case average = "average"
        case total = "total"
        case count = "count"
    }
}
struct DailyAveragesDataModel{
    let averageSpo2: DailyAverageModel?
    let averageHrv: DailyAverageModel?
    let userId: String?
    let averageSystolic:DailyAverageModel?
    let averagePulseRate:DailyAverageModel?
    let totalCalBrunt:DailyAverageModel?
    let totalDistanceCovered:DailyAverageModel?
    let averageDiastolic:DailyAverageModel?
    let totalStepCount:DailyAverageModel?
    let averageBreathingRate:DailyAverageModel?
    let averageTemperature:DailyAverageModel?
    let sleepData:[Int?]?
    let sleepStartTime:String
    let sleepEndTime:String
    
	init(sleepStartTime:String, sleepEndTime:String,averagePulseRate: DailyAverageModel?) {
        self.sleepStartTime = sleepStartTime
        self.sleepEndTime = sleepEndTime
        self.averageSpo2 = nil
        self.averageHrv = nil
        self.userId = ""
        self.averageSystolic = nil
        self.averageDiastolic = nil
        self.averagePulseRate = averagePulseRate
        self.totalCalBrunt = nil
        self.totalDistanceCovered = nil
        self.totalStepCount = nil
        self.averageTemperature = nil
        self.sleepData = []
        self.averageBreathingRate = nil
    }
}

struct BPAllDataPointsModel {
    let userId:String?
    let dataTimestamp:Float?
    let diastolic:Double?
    let systolic:Double?
    let pulseRate:Double?
}
struct DashboardModel: Codable {
    let date: String
    let nocturnalSystolic: Int
    let nocturnalDystolic: Int
    let sleepTime: Int
    let stepCount: Int
    let oxygen: Int
    let heartRate: Int

    enum CodingKeys: String, CodingKey {
        case date
        case nocturnalSystolic = "noc_systolic"
        case nocturnalDystolic = "noc_dystolic"
        case sleepTime = "sleep_time"
        case stepCount = "step_count"
        case oxygen = "spo2"
        case heartRate = "heart_rate"
    }
}
