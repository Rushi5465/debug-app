//
//  MessageToSnsResponseModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/06/21.
//

import Foundation

struct MessageToSnsResponseModel: Decodable {
	var message: String
	var status_code: Int
	var data: MessageToSnsDataModel?
}

struct MessageToSnsDataModel: Decodable {
	var id: String?
	var username: String?
}
