//
//  FetchPresignDataModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/06/21.
//

import Foundation

struct FetchPresignDataModel: Decodable {
	var url: String
	var folder_name: String
	var file_name: String
}
