//
//  TimeRange.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/07/21.
//

import Foundation

struct TimeRange {
	
	var startTime: TimeInterval! = nil
	var endTime: TimeInterval! = nil
}
