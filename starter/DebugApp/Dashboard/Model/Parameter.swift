//
//  Parameter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/07/21.
//

import Foundation

struct Parameter {
	
	let name: String
	var reading: String?
	let unit: String?
	let iconName: String?
    
    init(name:String) {
        self.name = name
        self.reading = "-"
        self.unit = "-"
		self.iconName = ""
    }
    
	init(name:String,reading:String?,unit:String?,iconName:String?) {
        self.name = name
        self.reading = reading
        self.unit = unit
		self.iconName = iconName
    }
}
