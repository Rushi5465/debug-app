//
//  DataUploaderWebService.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 01/07/21.
//

import Foundation

class DataUploaderWebService: DataUploaderWebServiceProtocol {
	
	private var service: RestService
	
	init(service: RestService = RestService.shared) {
		self.service = service
	}
	
	func postFileInfo(filename: String, username: String?, completionHandler: @escaping (FetchPresignResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.uploadFiles)!
		
		guard let url = urlComponent.url else {
			completionHandler(nil, MovanoError.invalidRequestURL)
			return
		}
		
		let parameters: [String: Any] = ["filename": filename,
										 "username": username as Any]
		
		var header = Header()
		header.addHeader(key: "Authorization", value: KeyChain.shared.idToken)
		header.addHeader(key: "AccessToken", value: KeyChain.shared.accessToken!)
		
		let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters)
		
		service.request(apiType: APIType.FETCH_PRESIGNED_API, request: request) { (result) in
			switch result {
				case .success(let response):
					if let responseModel = try? JSONDecoder().decode(FetchPresignResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
					}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
	
	func uploadFileToPresigned(url: String, fileUrl: URL, completionHandler: @escaping (Bool, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: url)!
		
		guard let url = urlComponent.url else {
			completionHandler(false, MovanoError.invalidRequestURL)
			return
		}
		
		let request = NetworkRequest(url: url, method: .put, header: nil, param: nil)
		
		service.uploadFile(apiType: "", request: request, fileUrl: fileUrl) { (result) in
			switch result {
				case .success:
					completionHandler(true, nil)
				case .failure(let error):
					completionHandler(false, error)
			}
		}
	}
	
	func putMessageToSns(request: MessageToSnsRequestModel, completionHandler: @escaping (MessageToSnsResponseModel?, MovanoError?) -> Void) {
		
		do {
			let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.putMessageToSns)!
			
			guard let url = urlComponent.url else {
				completionHandler(nil, MovanoError.invalidRequestURL)
				return
			}
			
			let jsonData = try JSONEncoder().encode(request)
			let parameters = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [String: Any]
			
			var header = Header()
			header.addHeader(key: "Authorization", value: KeyChain.shared.idToken)
			header.addHeader(key: "AccessToken", value: KeyChain.shared.accessToken!)
			
			let request = NetworkRequest(url: url, method: .post, header: header.allHeaders, param: parameters)
			
			service.request(apiType: APIType.FETCH_PRESIGNED_API, request: request) { (result) in
				switch result {
					case .success(let response):
						if let responseModel = try? JSONDecoder().decode(MessageToSnsResponseModel.self, from: response.rawData()) {
							completionHandler(responseModel, nil)
						} else {
							completionHandler(nil, MovanoError.invalidResponseModel)
						}
					case .failure(let error):
						completionHandler(nil, error)
				}
			}
		} catch let error{
			print("\(error)")
		}
	}
	
}
