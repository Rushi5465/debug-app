//
//  DashboradDataWebService.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 24/09/21.
//

import Foundation

// swiftlint:disable all
class DashboardDataWebService : DashboradDataWebServiceProtocol{
    
    private var service: RestService
    private var graphQL: ApolloClientService

    init(graphQL: ApolloClientService = ApolloClientService(),service: RestService = RestService.shared) {
        self.graphQL = graphQL
        self.service = service
    }
  
    func fetchDashboradData(forDate:String,completionHandler: @escaping (DailyAveragesDataModel?, MovanoError?) -> Void){
        let date = forDate.toDate(format: DateFormat.serverDateFormat) ?? Date()
        if(Utility.makeAPICall(date: date) || !CoreDataSleepTime.isSleepTimeAvailable(for: date)){
            
            let query = GetDailyAveragesQuery(daily_averages_date: forDate)
            
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { [weak self] (result) in
                switch result {
                case .success(let graphQLResult):
                    if let item = graphQLResult.data, let avgData = item.getDailyAverages{
                        let avgPulseRate = convertStringToDictionary(text: avgData.averagePulseRate ?? "")
                        let dailyAvgPulseRateData = DailyAverageModel(average: (avgPulseRate?["average"] as? Double))
                        
                        let avgHRV = convertStringToDictionary(text: avgData.averageHrv ?? "")
                        let dailyAvgHRVData = DailyAverageModel(average: (avgHRV?["average"] as? Double))
                        
                        let avgOxygenRate = convertStringToDictionary(text: avgData.averageSpO2 ?? "")
                        let dailyAvgOxygenData = DailyAverageModel(average: (avgOxygenRate?["average"] as? Double))
                        
                        let avgBreathingRate = convertStringToDictionary(text: avgData.averageBreathingRate ?? "")
                        let dailyAvgBreathingRateData = DailyAverageModel(average: (avgBreathingRate?["average"] as? Double))
                        
                        let avgSystolicRate = convertStringToDictionary(text: avgData.averageSystolic ?? "")
                        let dailyAvgSystolicData = DailyAverageModel(average: (avgSystolicRate?["average"] as? Double))
                        
                        let avgDiastolicRate = convertStringToDictionary(text: avgData.averageDiastolic ?? "")
                        let dailyAvgDiastolicData = DailyAverageModel(average: (avgDiastolicRate?["average"] as? Double))
                        
                        let avgSkinTempRate = convertStringToDictionary(text: avgData.averageTemperature ?? "")
                        let dailyAvgTempData = DailyAverageModel(average: (avgSkinTempRate?["average"] as? Double))
                        
						let totalStepData = convertStringToDictionary(text: avgData.totalStepCount ?? "")
						let dailyTotalStepData = DailyAverageModel(average: (totalStepData?["total"] as? Double))
						
						let totalCaloriesData = convertStringToDictionary(text: avgData.totalCaloriesBurnt ?? "")
						let dailyTotalCaloriesData = DailyAverageModel(average: (totalCaloriesData?["total"] as? Double))
						
                        if let date = forDate.toDate(format: DateFormat.serverDateFormat), avgData.sleepStartTime != nil && avgData.sleepEndTime != nil {
                            CoreDataSleepTime.addSleepTime(date: date, sleepStartTime: avgData.sleepStartTime ?? "0", sleepEndTime: avgData.sleepEndTime ?? "0", deepValue: avgData.sleepData?[0] ?? 0, lightValue: avgData.sleepData?[1] ?? 0, remValue: avgData.sleepData?[2] ?? 0, awakeValue: avgData.sleepData?[3] ?? 0, avgHR: dailyAvgPulseRateData.average ?? 0.0,avgHRV: dailyAvgHRVData.average ?? 0.0, avgOxygen: dailyAvgOxygenData.average ?? 0.0, avgBreathingRate: dailyAvgBreathingRateData.average ?? 0.0, avgSystolicBP: dailyAvgSystolicData.average ?? 0.0, avgDiastolicBP: dailyAvgDiastolicData.average ?? 0.0, avgSkinTempVar: dailyAvgTempData.average ?? 0.0, totalStepCount: Int(dailyTotalStepData.total ?? 0), totalCaloriesBurnt: dailyTotalCaloriesData.total ?? 0.0, healthScore: avgData.healthScore ?? 0.0)
                        }
                        
                        let dailyAvgData = DailyAveragesDataModel(sleepStartTime: avgData.sleepStartTime ?? "0", sleepEndTime: avgData.sleepEndTime ?? "0", averagePulseRate: dailyAvgPulseRateData)

                            completionHandler(dailyAvgData,nil)
                            
                        }else{
                            //Call must proceed, inorder to fetch step count data
                            let dailyAvgData = DailyAveragesDataModel(sleepStartTime: "0", sleepEndTime: "0", averagePulseRate: nil)
                                completionHandler(dailyAvgData,nil)
                        }
                        
                    case .failure(let error):
                        if error.localizedDescription == "Received error response: Unauthorized" ||
                            error.localizedDescription == "Received error response: Token has expired." {
                            let coreDataSleeTime = CoreDataSleepTime.fetch(forDate: date)
                           let dailyAvgData = DailyAveragesDataModel(sleepStartTime: coreDataSleeTime?.sleepStartTime ?? "0", sleepEndTime: coreDataSleeTime?.sleepEndTime ?? "0", averagePulseRate: nil)
                               completionHandler(dailyAvgData,nil)

                        } else {
                            completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                        }
                        Logger.shared.addLog(error.localizedDescription)
                    }
                }
        }else{
            let coreDataSleeTime = CoreDataSleepTime.fetch(forDate: date)
			let dailyAvgData = DailyAveragesDataModel(sleepStartTime: coreDataSleeTime?.sleepStartTime ?? "0", sleepEndTime: coreDataSleeTime?.sleepEndTime ?? "0", averagePulseRate: DailyAverageModel(average: coreDataSleeTime?.avgHR))
               completionHandler(dailyAvgData,nil)
       
        }
             
        
    }

    func fetchAllDataPoints(timestampRange: Range<Double>,forStepCountRange:Range<Double>,completionHandler: @escaping (AllDataPointsModel?, MovanoError?)->Void){
        if(Utility.makeAPICall(timeIntervalRange: timestampRange) || !CoreDataBreathingRate.isReadingAvailable(range: timestampRange) || !CoreDataOxygen.isReadingAvailable(range: timestampRange) || !CoreDataPulseRate.isReadingAvailable(range: timestampRange) || !CoreDataSkinTemperature.isReadingAvailable(range: timestampRange) || !CoreDataSleepStage.isSleepStageAvailable(range: timestampRange) || !CoreDataStepCount.isReadingAvailable(range: timestampRange) ||
            !CoreDataBPReading.isBPReadingAvailable(range: timestampRange)){
            
            let query = QueryAllDataPointsByTimestampRangeQuery(from_ts: timestampRange.lowerBound,to_ts: timestampRange.upperBound, from_ts_sc: forStepCountRange.lowerBound, to_ts_sc: forStepCountRange.upperBound)
            
            _ = self.graphQL.client!.watch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { (result) in
                switch result {
                case .success(let allDataPoints):
                    if let graphQLResult = allDataPoints.data{
                        var allDataPointsModel:AllDataPointsModel = AllDataPointsModel()
                        
                        //Breathing rate readings
                        if let items = graphQLResult.queryBreathingRateByTimestampRange?.items{
                            for each in items where each != nil {
                                CoreDataBreathingRate.add(timeInterval: each!.breathingRateTimestamp, value: each!.breathingRateValue)
                            }
                            let breathingRateData = CoreDataBreathingRate.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                            allDataPointsModel.coreDataBreathingRateList = breathingRateData
                        } else {
                            allDataPointsModel.coreDataBreathingRateList = nil
                        }
                        
                        //BP readings
                        if let item = graphQLResult.queryBloodPressureByTimestampRange?.items {
                            for each in item where each != nil {
                                CoreDataBPReading.addBPReading(timeInterval: each!.dataTimestamp, systolic: each!.systolic!, diastolic: each!.diastolic!, pulseRate: each!.pulseRate!)
                            }
                            let bpReadingData = CoreDataBPReading.fetchBloodPressure(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                            allDataPointsModel.coreDataBPReadingList = bpReadingData
                        } else {
                            allDataPointsModel.coreDataBPReadingList = nil

                        }
                        
                        //Oxygen readings
                        if let item = graphQLResult.querySpO2ByTimestampRange?.items {
                            for each in item where each != nil {
                                CoreDataOxygen.add(timeInterval: each!.spO2Timestamp, value: each!.spO2Value)
                            }
                            
                            let oxygenData = CoreDataOxygen.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                            allDataPointsModel.coreDataOxygenList = oxygenData
                        } else {
                            allDataPointsModel.coreDataOxygenList = nil

                        }
                        
                        //Pulse rate readings
                        if let items = graphQLResult.queryPulseRateByTimestampRange?.items {
                            for each in items where each != nil {
                                CoreDataPulseRate.add(timeInterval: each!.pulseRateTimestamp, value: each!.pulseRateValue)
                            }
                            let pulseRateData = CoreDataPulseRate.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                            allDataPointsModel.coreDataPulseRateList = pulseRateData
                        } else {
                            allDataPointsModel.coreDataPulseRateList = nil
                        }
                        
                        //Skin temp readings
                        if let items = graphQLResult.querySkinTemperatureByTimestampRange?.items {
                            for each in items where each != nil {
                                CoreDataSkinTemperature.add(timeInterval: each!.temperatureTimestamp, value: each!.temperatureValue)
                            }
                            let skinTempData = CoreDataSkinTemperature.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                            allDataPointsModel.coreDataSkinTemperatureList = skinTempData
                        } else {
                            allDataPointsModel.coreDataSkinTemperatureList = nil
                        }
                        
                        //sleepStage Readings
                        if let items = graphQLResult.querySleepDataByTimestampRange?.items {
								for each in items where each != nil {
									CoreDataSleepStage.addSleepStage(timeInterval: each!.sleepTimestamp, value: each!.sleepStage)
								}
                            let sleepStageData = CoreDataSleepStage.fetchSleepStage(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
                            allDataPointsModel.coreDataSleepStageList = sleepStageData
                        } else {
                            allDataPointsModel.coreDataSleepStageList = nil
                        }
                        
                        //Step Count Readings
                        if let items = graphQLResult.queryStepCountByTimestampRange?.items {
                            
                            for each in items where each != nil {
                                CoreDataStepCount.add(timeInterval: each!.stepCountTimestamp, value: each!.stepCountValue, calories: each!.caloriesBurnt, distance: each!.distanceCovered)
                            }
                            let stepCountData = CoreDataStepCount.fetch(for: Date(timeIntervalSince1970: forStepCountRange.lowerBound) ..< Date(timeIntervalSince1970: forStepCountRange.upperBound))
                            allDataPointsModel.coreDataStepCountList = stepCountData
                        } else {
                            allDataPointsModel.coreDataStepCountList = nil
                        }
                        
                        completionHandler(allDataPointsModel,nil)
                    }else{
                        completionHandler(nil, .invalidResponseModel)
                    }
                    
                case .failure(let error):
                    if error.localizedDescription == "Received error response: Unauthorized" ||
                        error.localizedDescription == "Received error response: Token has expired." {
                        
                    } else {
//                        completionHandler(nil, MovanoError.failedRequest(description: error.localizedDescription))
                    }
					var allDataPointsModel:AllDataPointsModel = AllDataPointsModel()
					allDataPointsModel.coreDataBreathingRateList = CoreDataBreathingRate.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
					allDataPointsModel.coreDataBPReadingList = CoreDataBPReading.fetchBloodPressure(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
					allDataPointsModel.coreDataOxygenList = CoreDataOxygen.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
					allDataPointsModel.coreDataPulseRateList = CoreDataPulseRate.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
					allDataPointsModel.coreDataSkinTemperatureList = CoreDataSkinTemperature.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
					allDataPointsModel.coreDataSleepStageList = CoreDataSleepStage.fetchSleepStage(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
					allDataPointsModel.coreDataStepCountList = CoreDataStepCount.fetch(for: Date(timeIntervalSince1970: forStepCountRange.lowerBound) ..< Date(timeIntervalSince1970: forStepCountRange.upperBound))
					
					completionHandler(allDataPointsModel,nil)

                    Logger.shared.addLog(error.localizedDescription)
                }
            }
        }else{
            var allDataPointsModel:AllDataPointsModel = AllDataPointsModel()
            allDataPointsModel.coreDataBreathingRateList = CoreDataBreathingRate.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            allDataPointsModel.coreDataBPReadingList = CoreDataBPReading.fetchBloodPressure(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            allDataPointsModel.coreDataOxygenList = CoreDataOxygen.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            allDataPointsModel.coreDataPulseRateList = CoreDataPulseRate.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            allDataPointsModel.coreDataSkinTemperatureList = CoreDataSkinTemperature.fetch(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            allDataPointsModel.coreDataSleepStageList = CoreDataSleepStage.fetchSleepStage(for: Date(timeIntervalSince1970: timestampRange.lowerBound) ..< Date(timeIntervalSince1970: timestampRange.upperBound))
            allDataPointsModel.coreDataStepCountList = CoreDataStepCount.fetch(for: Date(timeIntervalSince1970: forStepCountRange.lowerBound) ..< Date(timeIntervalSince1970: forStepCountRange.upperBound))
            
            completionHandler(allDataPointsModel,nil)
        }

    }
    
	func requestAccessToken(completionHandler: @escaping (AccessTokenResponseModel?, MovanoError?) -> Void) {
		
		let urlComponent = URLComponents(string: Config.shared.currentServer.serverUrl + APPURL.Routes.getnewToken)!
		
		guard let url = urlComponent.url else {
			return
		}
		
		var header = Header()
		header.addHeader(key: "UsernameSRP", value: KeyChain.shared.usernameSrp)
		header.addHeader(key: "Authorization", value: APPURL.authToken)
		header.addHeader(key: "RefreshToken", value: KeyChain.shared.refreshToken)
		
		let tokerRequest = NetworkRequest(url: url, method: .get, header: header.allHeaders, param: nil)
		service.request(apiType: APIType.NEW_TOKEN_API, request: tokerRequest) { (result) in
			switch result {
				case .success(let response):
					
					let userSession = UserSession(json: response["data"])
					KeyChain.shared.saveAccessToken(response: userSession)
					
					if let responseModel = try? JSONDecoder().decode(AccessTokenResponseModel.self, from: response.rawData()) {
						completionHandler(responseModel, nil)
					} else {
						completionHandler(nil, MovanoError.invalidResponseModel)
				}
				case .failure(let error):
					completionHandler(nil, error)
			}
		}
	}
}
