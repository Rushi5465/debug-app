//
//  DashboardTableViewCell.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 20/09/21.
//

import UIKit
import SwiftUI

class DashboardTableViewCell: UITableViewCell {

    // MARK: - IB Outlets
    @IBOutlet weak var titleLabel: MovanoLabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var badgeImage: UIImageView!
    @IBOutlet weak var measureLabel: MovanoLabel!
    @IBOutlet weak var sldierContainerView: UIView!
    @IBOutlet weak var unitLabel: MovanoLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 6, left: 15, bottom: 6, right: 15))
    }
    
}
