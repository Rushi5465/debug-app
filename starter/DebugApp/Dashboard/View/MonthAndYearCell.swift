//
//  MonthAndYearCell.swift
//  BloodPressureApp
//
//  Created by Rushikant on 09/08/21.
//

import UIKit

class MonthAndYearCell: UICollectionViewCell {
	
	@IBOutlet weak var titleLabel: MovanoLabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		self.contentView.roundCorners([.layerMaxXMinYCorner, .layerMaxXMaxYCorner], radius: 1)
	}
}
