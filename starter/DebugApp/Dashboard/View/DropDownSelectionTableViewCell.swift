//
//  DropDownSelectionTableViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 09/08/21.
//

import UIKit

class DropDownSelectionTableViewCell: UITableViewCell {

	@IBOutlet weak var imageIcon: UIImageView!
	@IBOutlet weak var titleLabel: MovanoLabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
