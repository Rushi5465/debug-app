//
//  CustomSlider.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 28/09/21.
//

import Foundation
import SwiftUI

struct SliderViewParam {
    var value:Double
    var leftBarColor : Color
    var rightBarColor : Color
    var goalValue: String
    var showIndicator:Bool
    
    init() {
        value = 0.0
        leftBarColor = Color.clear
        rightBarColor = Color.clear
        goalValue = ""
        showIndicator = false
    }
	
    init(value:Double, leftBarColor: Color, rightBarColor : Color, goalValue: String, showIndicator:Bool) {
        self.value = value
        self.leftBarColor = leftBarColor
        self.rightBarColor = rightBarColor
        self.goalValue = goalValue
        self.showIndicator = showIndicator
    }
}
struct CustomSliderView: View {
    
    @State var value: Double
    var sliderViewParam: SliderViewParam?
    
    init(sliderViewParam: SliderViewParam) {
        self.sliderViewParam = sliderViewParam
        self.value = sliderViewParam.value
    }
    var body: some View {
        let background = Color.clear

        return ZStack {
            background.edgesIgnoringSafeArea(.all)
            VStack(spacing: 30) {
                Group {
                    
                    // custom sliders logic go here
                    CustomSlider(value: $value, range: (0, 100)) { modifiers in
                      ZStack {
                        sliderViewParam?.rightBarColor.cornerRadius(5).frame(height: 10).modifier(modifiers.barRight)
                        sliderViewParam?.leftBarColor.cornerRadius(5).frame(height: 10).modifier(modifiers.barLeft)
                      

                        if(sliderViewParam?.showIndicator ?? false){
                            ZStack {
                                Rectangle().fill(Color.white)
                            }.modifier(modifiers.knob)
                            
                            Text(sliderViewParam?.goalValue ?? "")
                                .offset(y: 7)
                                .foregroundColor(Color.white)
                                .lineLimit(nil)
                                .font(.system(size: 10))
                                .animation(nil)
                                .modifier(modifiers.rangeLable)
                        }
                       
                      }.frame(height: 14)
                    }.frame(height: 50)
                    
                }.frame(width:200)
            }
        }
    }
}

struct CornerRadiusStyle: ViewModifier {
    var radius: CGFloat
    var corners: UIRectCorner

    struct CornerRadiusShape: Shape {

        var radius = CGFloat.infinity
        var corners = UIRectCorner.allCorners

        func path(in rect: CGRect) -> Path {
            let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            return Path(path.cgPath)
        }
    }

    func body(content: Content) -> some View {
        content
            .clipShape(CornerRadiusShape(radius: radius, corners: corners))
    }
}

extension View {
    func cornerRadius(radius: CGFloat, corners: UIRectCorner) -> some View {
        ModifiedContent(content: self, modifier: CornerRadiusStyle(radius: radius, corners: corners))
    }
}

struct CustomSliderComponents {
    let barLeft: CustomSliderModifier
    let barRight: CustomSliderModifier
    let knob: CustomSliderModifier
    let rangeLable: CustomSliderModifier
}

struct CustomSliderModifier: ViewModifier {
    enum Name {
        case barLeft
        case barRight
        case knob
        case rangeLable
    }
    let name: Name
    let size: CGSize
    let offsetX: CGFloat
    let offsetY: CGFloat
    
    func body(content: Content) -> some View {
        content
        .frame(width: size.width)
        .position(x: size.width*0.5, y: size.height*0.5)
        .offset(x: offsetX, y: offsetY)
    }
}

struct CustomSlider<Component: View>: View {

    @Binding var value: Double
    var range: (Double, Double)
    var knobWidth: CGFloat?
    let viewBuilder: (CustomSliderComponents) -> Component

    init(value: Binding<Double>, range: (Double, Double), knobWidth: CGFloat? = nil,
         _ viewBuilder: @escaping (CustomSliderComponents) -> Component
    ) {
        _value = value
        self.range = range
        self.viewBuilder = viewBuilder
        self.knobWidth = CGFloat(1.0)
    }

    var body: some View {
      return GeometryReader { geometry in
        self.view(geometry: geometry) // function below
      }
    }
    
    private func view(geometry: GeometryProxy) -> some View {
      let frame = geometry.frame(in: .global)
      let drag = DragGesture(minimumDistance: 0).onChanged({ drag in
        //Uncommet to make slider functional
       // self.onDragChange(drag, frame)
      }
      )
      let offsetX = self.getOffsetX(frame: frame)

        let knobSize = CGSize(width: knobWidth ?? frame.height, height: frame.height)
      let barLeftSize = CGSize(width: CGFloat(offsetX + knobSize.width * 0.5), height:  frame.height)
      let barRightSize = CGSize(width: frame.width - barLeftSize.width, height: frame.height)
        let rangeLableSize = CGSize(width: 20.0, height: 10)
        var knobOffSet = offsetX
        if(value < 10.0){
           knobOffSet = offsetX + 7
        }else if(value > 90.0){
            knobOffSet = offsetX - 7
        }
      let modifiers = CustomSliderComponents(
        barLeft: CustomSliderModifier(name: .barLeft, size: barLeftSize, offsetX: 0, offsetY: 0),
        barRight: CustomSliderModifier(name: .barRight, size: barRightSize, offsetX: barLeftSize.width - 7, offsetY: 0),
       
         knob: CustomSliderModifier(name: .knob, size: knobSize, offsetX: knobOffSet, offsetY: 0),
         rangeLable: CustomSliderModifier(name: .rangeLable, size: rangeLableSize, offsetX: offsetX - 10, offsetY: 0))

      return ZStack { viewBuilder(modifiers).gesture(drag) }
    }
    
    private func onDragChange(_ drag: DragGesture.Value, _ frame: CGRect) {
        let width = (knob: Double(knobWidth ?? frame.size.height), view: Double(frame.size.width))
        let xrange = (min: Double(0), max: Double(width.view - width.knob))
        var value = Double(drag.startLocation.x + drag.translation.width) // knob center x
        value -= 0.5*width.knob // offset from center to leading edge of knob
        value = value > xrange.max ? xrange.max : value // limit to leading edge
        value = value < xrange.min ? xrange.min : value // limit to trailing edge
        value = value.convert(fromRange: (xrange.min, xrange.max), toRange: range)
        self.value = value
    }
    
    private func getOffsetX(frame: CGRect) -> CGFloat {
        let width = (knob: knobWidth ?? frame.size.height, view: frame.size.width)
        let xrange: (Double, Double) = (0, Double(width.view - width.knob))
        let result = self.value.convert(fromRange: range, toRange: xrange)
        return CGFloat(result)
    }
}

extension Double {
    func convert(fromRange: (Double, Double), toRange: (Double, Double)) -> Double {
        // Example: if self = 1, fromRange = (0,2), toRange = (10,12) -> solution = 11
        var value = self
        value -= fromRange.0
        value /= Double(fromRange.1 - fromRange.0)
        value *= toRange.1 - toRange.0
        value += toRange.0
        return value
    }
}
