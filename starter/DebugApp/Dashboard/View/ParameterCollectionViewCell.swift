//
//  ParameterCollectionViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/07/21.
//

import UIKit

class ParameterCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var readingLabel: MovanoLabel!
	@IBOutlet weak var parameterLabel: MovanoLabel!
	@IBOutlet weak var parameterImage: UIImageView!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		
		parameterLabel.sizeToFit()
		parameterLabel.adjustsFontSizeToFitWidth = true
    }

	func readingData(value: String, unit: String?) -> NSAttributedString {
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.semiBold(withSize: 22)]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.regular(withSize: 12)]
		
		let attributedString = NSMutableAttributedString(string: value, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1))
		if let unit = unit {
			attributedString.append(NSMutableAttributedString(string: " " + unit, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		}
		return attributedString
	}
}
