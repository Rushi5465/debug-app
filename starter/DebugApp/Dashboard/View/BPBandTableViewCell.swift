//
//  BPBandTableViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 23/06/21.
//

import UIKit

class BPBandTableViewCell: UITableViewCell {
	
	@IBOutlet weak var bandValue: MovanoLabel!
	@IBOutlet weak var lineView: UIView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		self.lineView.roundCorners([.layerMaxXMinYCorner, .layerMaxXMaxYCorner], radius: 1)
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}
	
}
