//
//  CustomBloodPressureSlider.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 10/10/21.
//

import Foundation
import SwiftUI

// swiftlint:disable all
//Model that will hold all required parameters to design view
struct BPSliderViewParam {
    var systolicValue:Double
    var diastolicValue:Double
    var systolicLeftBarColor : Color
    var diastolicLeftBarColor : Color
    var systolicRightBarColor : Color
    var diastolicRightBarColor : Color
    var systolicGoalValue: String
    var diastolicgoalValue: String
    
    init() {
        systolicValue = 0.0
        diastolicValue = 0.0
        systolicLeftBarColor = Color.clear
        diastolicLeftBarColor = Color.clear
        systolicRightBarColor = Color.clear
        diastolicRightBarColor = Color.clear
        systolicGoalValue = ""
        diastolicgoalValue = ""
    }
    
    init(systolicValue:Double,diastolicValue:Double, systolicLeftBarColor: Color,diastolicLeftBarColor:Color, systolicRightBarColor : Color,diastolicRightBarColor:Color, systolicGoalValue: String, diastolicgoalValue:String) {
        self.systolicValue = systolicValue
        self.diastolicValue = diastolicValue
        self.systolicLeftBarColor = systolicLeftBarColor
        self.diastolicLeftBarColor = diastolicLeftBarColor
        self.systolicRightBarColor = systolicRightBarColor
        self.diastolicRightBarColor = diastolicRightBarColor
        self.systolicGoalValue = systolicGoalValue
        self.diastolicgoalValue = diastolicgoalValue
    }
}

struct CustomBPSlider<Component: View>: View {

    @Binding var leftBarValue: Double
    @Binding var rightBarValue: Double
     var leftBarGoalValue: Double
     var rightBarGoalValue: Double

    var range: (Double, Double)
    var knobWidth: CGFloat?
    let viewBuilder: (CustomBPSliderComponents) -> Component

    init(leftBarValue: Binding<Double>, rightBarValue:Binding<Double>,leftBarGoalValue:Double,rightBarGoalValue:Double,range: (Double, Double), knobWidth: CGFloat? = nil,
         _ viewBuilder: @escaping (CustomBPSliderComponents) -> Component
    ) {
        _leftBarValue = leftBarValue
        _rightBarValue = rightBarValue
        self.leftBarGoalValue = leftBarGoalValue
        self.rightBarGoalValue = rightBarGoalValue
        self.range = range
        self.viewBuilder = viewBuilder
        self.knobWidth = CGFloat(1.0)
    }

    var body: some View {
      return GeometryReader { geometry in
        self.view(geometry: geometry) // function below
      }
    }
    
//    private func view(geometry: GeometryProxy) -> some View {
//        let frame = geometry.frame(in: .global)
//
//        //let offsetX = self.getOffsetX(frame: frame)
//        let systolicBarOffsetX = self.getOffsetXForLeftBar(frame: CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width/2, height: frame.height))
//        let diastolicBarOffsetX = self.getOffsetYForRightBar(frame: CGRect(x: (frame.origin.x + frame.width/2), y: frame.origin.y, width: frame.width/2, height: frame.height)) + frame.width/2
//
//        let knobSize = CGSize(width: knobWidth ?? frame.height, height: frame.height)
//
//        let barLeftSizeDiastolic = CGSize(width: CGFloat(diastolicBarOffsetX - frame.width/2 + knobSize.width * 0.5), height:  frame.height)
//      let barRightSizeDiastolic = CGSize(width: frame.width/2 - barLeftSizeDiastolic.width, height: frame.height)
//
//        let barRightSizeSystolic = CGSize(width: CGFloat((systolicBarOffsetX + knobSize.width * 0.5) + 7), height:  frame.height)
//        let barLeftSizeSystolic = CGSize(width: frame.width/2 - (barRightSizeSystolic.width - 7), height: frame.height)
//
//      let rangeLableSize = CGSize(width: 20.0, height: 10)
//        let modifiers = CustomBPSliderComponents(
//            barLeftSystolic: CustomBPSliderModifier(name: .barLeftSystolic, size: barLeftSizeSystolic, offsetX: 0, offsetY: 0),
//            barLeftDiastolic: CustomBPSliderModifier(name: .barLeftDiastolic, size: barLeftSizeDiastolic, offsetX: frame.width/2, offsetY: 0),
//            barRightSystolic: CustomBPSliderModifier(name: .barRightSystolic, size: barRightSizeSystolic, offsetX: barLeftSizeSystolic.width, offsetY: 0),
//            barRightDiastolic: CustomBPSliderModifier(name: .barRightDiastolic, size: barRightSizeDiastolic, offsetX: (barLeftSizeDiastolic.width + frame.width/2) - 5, offsetY: 0),
//            knobSystolic: CustomBPSliderModifier(name: .knobSystolic, size: knobSize, offsetX: systolicBarOffsetX, offsetY: 0),
//            knobDiastolic: CustomBPSliderModifier(name: .knobDiastolic, size: knobSize, offsetX: diastolicBarOffsetX, offsetY: 0),
//            rangeLableSystolic: CustomBPSliderModifier(name: .rangeLableSystolic, size: rangeLableSize, offsetX: systolicBarOffsetX - 6, offsetY: 0),
//            rangeLableDiastolic: CustomBPSliderModifier(name: .rangeLableDiastolic, size: rangeLableSize, offsetX: diastolicBarOffsetX - 6, offsetY: 0),
//            knobMidRange: CustomBPSliderModifier(name: .knobDiastolic, size: knobSize, offsetX: frame.width/2, offsetY: 0), midRangeLable: CustomBPSliderModifier(name: .midRangeLabel, size: rangeLableSize, offsetX: frame.width/2 - 6, offsetY: 0))
//
//      return ZStack { viewBuilder(modifiers)}
//    }
    
    private func view(geometry: GeometryProxy) -> some View {
        let frame = geometry.frame(in: .global)
      
        let convertedSystolicGoal = (Double(frame.width/2) * leftBarGoalValue)/range.1
        let convertedSystolicValue = (Double(frame.width/2) * leftBarValue)/range.1
        let convertedDiastolicGoal = (Double(frame.width/2) * rightBarGoalValue)/range.1
        let convertedDiastolicValue = (Double(frame.width/2) * rightBarValue)/range.1
        let knobSize = CGSize(width: knobWidth ?? frame.height, height: frame.height)
      
        let barRightSizeSystolic = CGSize(width: CGFloat(convertedSystolicValue), height:  frame.height)

        let barLeftSizeSystolic = CGSize(width: (frame.width/2 - barRightSizeSystolic.width) + 5, height: frame.height)

        let barLeftSizeDiastolic = CGSize(width: CGFloat(convertedDiastolicValue), height:  frame.height)
        let barRightSizeDiastolic = CGSize(width: frame.width/2 - barLeftSizeDiastolic.width, height: frame.height)

        
      let rangeLableSize = CGSize(width: 25.0, height: 10)
        let modifiers = CustomBPSliderComponents(
            barLeftSystolic: CustomBPSliderModifier(name: .barLeftSystolic, size: barLeftSizeSystolic, offsetX: 0, offsetY: 0),
            barLeftDiastolic: CustomBPSliderModifier(name: .barLeftDiastolic, size: barLeftSizeDiastolic, offsetX: frame.width/2, offsetY: 0),
            barRightSystolic: CustomBPSliderModifier(name: .barRightSystolic, size: barRightSizeSystolic, offsetX: barLeftSizeSystolic.width - 5, offsetY: 0),
            barRightDiastolic: CustomBPSliderModifier(name: .barRightDiastolic, size: barRightSizeDiastolic, offsetX: (barLeftSizeDiastolic.width + frame.width/2) - 7, offsetY: 0),
            knobSystolic: CustomBPSliderModifier(name: .knobSystolic, size: knobSize, offsetX: (frame.width/2) - CGFloat(convertedSystolicGoal), offsetY: 0),
            knobDiastolic: CustomBPSliderModifier(name: .knobDiastolic, size: knobSize, offsetX: (frame.width/2) + CGFloat(convertedDiastolicGoal), offsetY: 0),
            rangeLableSystolic: CustomBPSliderModifier(name: .rangeLableSystolic, size: rangeLableSize, offsetX: ((frame.width/2) - CGFloat(convertedSystolicGoal)) - 6, offsetY: 0),
            rangeLableDiastolic: CustomBPSliderModifier(name: .rangeLableDiastolic, size: rangeLableSize, offsetX: ((frame.width/2) + CGFloat(convertedDiastolicGoal)) - 6, offsetY: 0),
            knobMidRange: CustomBPSliderModifier(name: .knobDiastolic, size: knobSize, offsetX: frame.width/2, offsetY: 0), midRangeLable: CustomBPSliderModifier(name: .midRangeLabel, size: rangeLableSize, offsetX: frame.width/2 - 6, offsetY: 0))

      return ZStack { viewBuilder(modifiers)}
    }
    
    
  
//    private func getOffsetX(frame: CGRect) -> CGFloat {
//        let width = (knob: knobWidth ?? frame.size.height, view: frame.size.width)
//        let xrange: (Double, Double) = (0, Double(width.view - width.knob))
//        let result = self.value.convert(fromRange: range, toRange: xrange)
//        return CGFloat(result)
//    }
//
    func getOffsetXForLeftBar(frame:CGRect) -> CGFloat{
        let width = (knob: knobWidth ?? frame.size.height, view: frame.size.width)
        let xrange: (Double, Double) = (0, Double(width.view - width.knob))
        let result = self.leftBarValue.convert(fromRange: range, toRange: xrange)
        return CGFloat(result)
    }
    
    func getOffsetYForRightBar(frame:CGRect) -> CGFloat{
        let width = (knob: knobWidth ?? frame.size.height, view: frame.size.width)
        let xrange: (Double, Double) = (0, Double(width.view - width.knob))
        let result = self.rightBarValue.convert(fromRange: range, toRange: xrange)
        return CGFloat(result)
    }
}

struct CustomBPSliderModifier: ViewModifier {
    enum Name {
        case barLeftSystolic
        case barRightSystolic
        case barLeftDiastolic
        case barRightDiastolic
        case knobSystolic
        case knobDiastolic
        case rangeLableSystolic
        case rangeLableDiastolic
        case knobMidRange
        case midRangeLabel
    }
    let name: Name
    let size: CGSize
    let offsetX: CGFloat
    let offsetY: CGFloat
    
    func body(content: Content) -> some View {
        content
        .frame(width: size.width)
        .position(x: size.width*0.5, y: size.height*0.5)
        .offset(x: offsetX, y: offsetY)
    }
}

struct CustomBPSliderComponents {
    let barLeftSystolic: CustomBPSliderModifier
    let barLeftDiastolic: CustomBPSliderModifier
    let barRightSystolic: CustomBPSliderModifier
    let barRightDiastolic: CustomBPSliderModifier
    let knobSystolic: CustomBPSliderModifier
    let knobDiastolic: CustomBPSliderModifier
    let rangeLableSystolic: CustomBPSliderModifier
    let rangeLableDiastolic: CustomBPSliderModifier
    let knobMidRange:CustomBPSliderModifier
    let midRangeLable:CustomBPSliderModifier

}

struct CustomBPSliderView: View {
    
    @State var leftBarValue: Double
    @State var rightBarValue: Double
    var sliderViewParam: BPSliderViewParam?
    
    init(sliderViewParam: BPSliderViewParam) {
        self.sliderViewParam = sliderViewParam
        self.leftBarValue = sliderViewParam.systolicValue
        self.rightBarValue = sliderViewParam.diastolicValue
    }
    var body: some View {
        //let background = Color(red: 0.07, green: 0.07, blue: 0.12)
        let background = Color.clear

        return ZStack {
            background.edgesIgnoringSafeArea(.all)
            VStack(spacing: 30) {
                Group {
                    
                    // custom sliders logic go here
                    let leftGoal = Double(sliderViewParam?.systolicGoalValue ?? "0") ?? 0.0
                    let rightGoal = Double(sliderViewParam?.diastolicgoalValue ?? "") ?? 0.0
                    let range = leftGoal > rightGoal ? leftGoal + 10 : rightGoal + 10
                    CustomBPSlider(leftBarValue: $leftBarValue, rightBarValue: $rightBarValue ,leftBarGoalValue: leftGoal, rightBarGoalValue: rightGoal, range: (0, range)) { modifiers in
                      ZStack {
                        sliderViewParam?.systolicLeftBarColor.cornerRadius(radius: 5, corners: [.topLeft,.bottomLeft]).frame(height:10).modifier(modifiers.barLeftSystolic)
                        sliderViewParam?.systolicRightBarColor.cornerRadius(radius: 5, corners: [.topLeft,.bottomLeft]).frame(height:10).modifier(modifiers.barRightSystolic)

                            ZStack {
                                Rectangle().fill(Color.white)
                            }.modifier(modifiers.knobSystolic)

                            Text(sliderViewParam?.systolicGoalValue ?? "")
                                .offset(y: 7)
                                .foregroundColor(Color.white)
                                .lineLimit(nil)
								.font(Font(uiFont: UIFont.gilroy.regular(withSize: 12)))
                                .animation(nil)
                                .modifier(modifiers.rangeLableSystolic)
                        sliderViewParam?.diastolicRightBarColor.cornerRadius(5).frame(height:10).modifier(modifiers.barRightDiastolic)
                        sliderViewParam?.diastolicLeftBarColor.cornerRadius(radius: 5, corners: [.topRight,.bottomRight]).frame(height:10).modifier(modifiers.barLeftDiastolic)
                        ZStack {
                            Rectangle().fill(Color.white)
                        }.modifier(modifiers.knobDiastolic)
                        
                        Text(sliderViewParam?.diastolicgoalValue ?? "")
                            .offset(y: 7)
                            .foregroundColor(Color.white)
                            .lineLimit(nil)
							.font(Font(uiFont: UIFont.gilroy.regular(withSize: 12)))
                            .animation(nil)
                            .modifier(modifiers.rangeLableDiastolic)
                        
                        ZStack {
                            Rectangle().fill(Color.white)
                        }.modifier(modifiers.knobMidRange)

                        Text("0")
                            .offset(y: 7)
                            .foregroundColor(Color.white)
                            .lineLimit(nil)
                            .font(Font(uiFont: UIFont.gilroy.regular(withSize: 12)))
                            .animation(nil)
                            .modifier(modifiers.midRangeLable)

                      }.frame(height: 14)
                    }.frame(height: 50)
                    
                }.frame(width:200)
            }
        }
    }
}
