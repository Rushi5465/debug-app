//
//  ExerciseReadingViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/07/21.
//

import Foundation

protocol ExerciseReadingViewDelegate: AnyObject {
	
	func getBpByTimeStampSuccess(with responseModel: [QueryClinicalTrialUsersDataByTimestampRangeQuery.Data.QueryClinicalTrialUsersDataByTimestampRange.Item?])
	func getBpByTimeStampErrorHandler(error: MovanoError)
	
	func postExerciseDataSuccess(with responseModel: CoreDataExercise)
	func postExerciseDataErrorHandler(error: MovanoError)
	
	func fetchHeartRateSuccess(model: ActivityChartModel)
	
	func parameter(list: [Parameter])
}
