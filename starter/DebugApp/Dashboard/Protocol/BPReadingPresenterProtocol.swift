//
//  BPReadingPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 02/07/21.
//

import Foundation

protocol BPReadingPresenterProtocol {
	
	init(bpService: BloodPressureWebServiceProtocol, pauseDataService: PauseDataWebServiceProtocol, delegate: BloodPressureViewDelegate)
	func fetchReading(request: MessageToSnsRequestModel)
	func addPauseData(timeRange: TimeRange)
	func fetchParameters()
}
