//
//  ExerciseViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/07/21.
//

import Foundation

protocol ExerciseViewDelegate: AnyObject {
	
	func zipFile(error: MovanoError, completionHandler: @escaping () -> Void)
	func zipFile(data: Data, completionHandler: @escaping () -> Void)
	func postFileInfoSuccess(with responseModel: FetchPresignResponseModel, timeStamp: TimeInterval)
	func postFileInfoErrorHandler(error: MovanoError)
	func uploadFileToPresignedCompleted()
	func uploadFileToPresignedErrorHandler(error: MovanoError)
	func putMessageToSnsSuccess(timeRange: TimeRange)
	func putMessageToSnsErrorHandler(error: MovanoError)
	
	func timer(currentStopWatchInstance: Int)
	func fetchExerciseParameter(stepCount: Int, lastNonZeroStepCount: Int)
	func fetchExerciseParameter(heartRate: Int)
	func fetchExerciseParameter(distance: Double?)
	func fetchExerciseParameter(breathingRate: Int)
	func fetchExerciseParameter(spo2: Int)
	func fetchExerciseParameter(skinTemp: Float)
	func fetchExerciseCalorieParameter(stepCount: Int)
    func postStepsSuccess(with stepCount: Int)
	func postStepsSuccess(with responseModel: [CoreDataStepCount])
	func postStepsErrorHandler(error: MovanoError)
	func postCaloriesErrorHandler(error: MovanoError)
	func postSpO2Success(with responseModel: [Reading])
	func postSpO2ErrorHandler(error: MovanoError)
	func postDistanceSuccess(with responseModel: [CoreDataActivity])
	func postDistanceErrorHandler(error: MovanoError)
	func postBreathingRateSuccess(with responseModel: [Reading])
	func postBreathingRateErrorHandler(error: MovanoError)
	func postTempSuccess(with responseModel: [Reading])
	func postTempErrorHandler(error: MovanoError)
	func fetchChargingCurrent(chargingCurrent: Int)
	
	func createChartElement(reading: [Reading])
	
	func removeActivityIndicator()
    func showWrongDeviceError()
    func showDeviceDisconnectedError()
    func navigateToDetailsScreen(timeRange: TimeRange, error: MovanoError?)
}
