//
//  DashboardViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 03/07/21.
//

import Foundation
import UIKit

protocol DashboardViewDelegate: AnyObject {
    func afterDailyAvgDataResponseFetched(result: Result<DailyAveragesDataModel, MovanoError>)
    func afertAllPointsDataFetched(result: Result<HealthScoreModel, MovanoError>)
}
