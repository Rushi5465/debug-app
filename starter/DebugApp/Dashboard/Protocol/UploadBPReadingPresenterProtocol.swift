//
//  UploadBPReadingPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/06/21.
//

import Foundation

// swiftlint:disable all
protocol UploadBPReadingPresenterProtocol {
	
	init(uploaderService: DataUploaderWebServiceProtocol, temperatureService: SkinTemperatureWebServiceProtocol, pauseDataService: PauseDataWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, breathingRateService: BreathingRateWebService, delegate: UploadBPReadingViewDelegate)
	func postFileInfo(filename: String, username: String?)
	func uploadFileToPresigned(url: String, fileUrl: URL)
	func putMessageToSns(request: MessageToSnsRequestModel)
	func startTimer()
	func stopTimer()
	func zipFile()
	func addTemperatureReading(for timeStamp: TimeInterval)
	func addPulseRateReading(for timeStamp: TimeInterval)
}
