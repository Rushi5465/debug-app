//
//  UploadBPReadingViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 01/07/21.
//

import Foundation

protocol UploadBPReadingViewDelegate: AnyObject {
	
	func zipFile(error: MovanoError)
	func zipFile(data: Data)
	func timer(current StopWatch: Int)
	func postFileInfoSuccess(with responseModel: FetchPresignResponseModel, timeStamp: TimeInterval)
	func postFileInfoErrorHandler(error: MovanoError)
	func uploadFileToPresignedCompleted()
	func uploadFileToPresignedErrorHandler(error: MovanoError)
	func putMessageToSnsSuccess(with responseModel: MessageToSnsResponseModel, timeRange: TimeRange)
	func putMessageToSnsErrorHandler(error: MovanoError)
	
    func postTemperatureReadingSuccess(with responseModel: [CoreDataSkinTemperature])
    func postTemperatureReadingErrorHandler(error: MovanoError)
	
	func postPulseRateSuccess(with responseModel: [CoreDataPulseRate])
	func postPulseRateErrorHandler(error: MovanoError)
	
	func postBreathingRateSuccess(with responseModel: [CoreDataBreathingRate])
	func postBreathingRateErrorHandler(error: MovanoError)
	
	func removeActivityIndicator()
}
