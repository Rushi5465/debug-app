//
//  BloodPressureViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 02/07/21.
//

import Foundation

protocol BloodPressureViewDelegate: AnyObject {
	
	func getBpByTimeStampSuccess(with responseModel: [QueryClinicalTrialUsersDataByTimestampRangeQuery.Data.QueryClinicalTrialUsersDataByTimestampRange.Item?])
	func getBpByTimeStampErrorHandler(error: MovanoError)
	func postPauseDataSuccess(with reading: CoreDataPauseReading)
	func postPauseDataErrorHandler(error: MovanoError)
	
	func parameter(list: [Parameter])
}
