//
//  ExerciseReadingPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/07/21.
//

import Foundation

protocol ExerciseReadingPresenterProtocol {
	
	init(bpService: BloodPressureWebServiceProtocol, exerciseService: ExerciseDataWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, delegate: ExerciseReadingViewDelegate)
	func fetchReading(request: MessageToSnsRequestModel)
	func fetchParameters()
}
