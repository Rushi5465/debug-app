//
//  DashboradDataWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 24/09/21.
//

import Foundation

protocol DashboradDataWebServiceProtocol {
    func fetchDashboradData(forDate:String,completionHandler: @escaping (DailyAveragesDataModel?, MovanoError?) -> Void)
    func fetchAllDataPoints(timestampRange: Range<Double>,forStepCountRange:Range<Double>,completionHandler: @escaping (AllDataPointsModel?, MovanoError?)->Void)
    
}
