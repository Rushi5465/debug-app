//
//  ExercisePresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/07/21.
//

import Foundation

// swiftlint:disable all
protocol ExercisePresenterProtocol {

	init(uploaderService: DataUploaderWebServiceProtocol, temperatureService: SkinTemperatureWebServiceProtocol, stepCountService: StepCountWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, breathingRateService: BreathingRateWebServiceProtocol, oxygenRateService: OxygenDataWebServiceProtocol, delegate: ExerciseViewDelegate,exerciseService: ExerciseDataWebServiceProtocol)
	func startTimer()
	func stopTimer()
	func fetchHeartRateData(range: DateRange)
}
