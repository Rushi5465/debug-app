//
//  DashboardPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 03/07/21.
//

import Foundation

// swiftlint:disable all
protocol DashboardPresenterProtocol {
    init(breathingRateService: BreathingRateWebServiceProtocol, bloodPressureService: BloodPressureWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, temperatureRateService: SkinTemperatureWebServiceProtocol, sleepService: SleepWebServiceProtocol, stepCountService: StepCountWebServiceProtocol,oxygenService: OxygenDataWebServiceProtocol,dashboardDataService: DashboradDataWebServiceProtocol, delegate: DashboardViewDelegate)
    func fetchDailyAvgData(forDate:Date)
}
