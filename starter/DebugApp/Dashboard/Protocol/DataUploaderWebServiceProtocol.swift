//
//  DataUploaderWebServiceProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/06/21.
//

import Foundation

protocol DataUploaderWebServiceProtocol {
	
	func postFileInfo(filename: String, username: String?, completionHandler: @escaping (FetchPresignResponseModel?, MovanoError?) -> Void)
	func uploadFileToPresigned(url: String, fileUrl: URL, completionHandler: @escaping (Bool, MovanoError?) -> Void)
	func putMessageToSns(request: MessageToSnsRequestModel, completionHandler: @escaping (MessageToSnsResponseModel?, MovanoError?) -> Void)
	
}
