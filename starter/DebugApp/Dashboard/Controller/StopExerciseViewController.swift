//
//  StopExerciseViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 23/06/21.
//

import UIKit
import HGCircularSlider

// swiftlint:disable all
class StopExerciseViewController: UIViewController {
	
	@IBOutlet weak var circularSlider: UIView!
	@IBOutlet weak var timerLabel: MovanoLabel!
	@IBOutlet weak var stopButton: MovanoButton!
	@IBOutlet weak var dataCollectionView: UICollectionView!
	@IBOutlet weak var collectionHeight: NSLayoutConstraint!
	@IBOutlet weak var heartRateLabel: MovanoLabel!
	@IBOutlet weak var caloriesLabel: MovanoLabel!
	
	let manager = DocumentManager(manager: .default)
	var fileUrl: URL!
	var request: MessageToSnsRequestModel!
	
	var presenter: ExercisePresenter?
	var parameterList = [Parameter]() {
		didSet {
			self.dataCollectionView.reloadData()
		}
	}
	var temperatureReading = Double(0)
	var breathingRate = Int(0)
	var distance : Double?
	var heartRate = Int(0)
	var stepCount = Int(0)
	var spo2 = Int(0)
	var skinTemp = Float(0)
	var caloriesBurnt = Float(0)
	var elementList = [ChartElement]()
	var totalTime = 0
	var afterAllDataFetched = true
	override func viewDidLoad() {
        super.viewDidLoad()
		
		self.registerCollectionCell()
		self.assignDelegates()
		self.loadData()
		self.registerPresenters()
		self.presenter?.startTimer()
		
		self.dataCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		hud.dismiss()
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.collectionHeight.constant = self.dataCollectionView.contentSize.height
	}
	
	func loadData() {
		let step = Parameter(name: "STEPS", reading: nil, unit: nil, iconName: "sleep_StepsIcon")
		let distance = Parameter(name: "DISTANCE", reading: nil, unit: "miles", iconName: "sleep-DistanceIcon")
		let heartRate = Parameter(name: "HEART RATE", reading: nil, unit: "bpm", iconName: "sleep-HeartIcon")
		let oxygen = Parameter(name: "OXYGEN", reading: nil, unit: "%", iconName: "sleep-OxygenIcon")
		let temp = Parameter(name: "SKIN TEMP VAR", reading: nil, unit: "°f", iconName: "sleep_SkintempIcon")
		let chargingCurrent = Parameter(name: "CHARGING POWER", reading: nil, unit: "mW", iconName: "exercise_chargeIcon")
		
		self.parameterList.append(contentsOf: [heartRate, oxygen, step, distance, temp, chargingCurrent])
	}
    
	@IBAction func backButtonClicked(_ sender: MovanoButton) {
        showSessionEndAlert()
	}
    func showSessionEndAlert(){
        var actions = [UIAlertAction]()
        actions.append(UIAlertAction(title: "Continue", style: .default, handler: nil))
        actions.append(UIAlertAction(title: "Leave", style: .cancel, handler: { (_) in
            self.presenter?.invalidateTimer()
            self.navigationController?.popToRootViewController(animated: true)
        }))
        self.showAlert(title: "Attention", message: "Your recorded data will be discarded, leave the session?", actions: actions)
    }
	@IBAction func stopButtonClicked(_ sender: Any) {
		hud.show(in: self.view)
		self.presenter?.stopTimer()
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let stepCountService = StepCountWebService()
			let breathingRateService = BreathingRateWebService()
			let pulseRateService = PulseRateWebService()
			let temperatureService = SkinTemperatureWebService()
			let uploaderService = DataUploaderWebService()
			let oxygenRateService = OxygenDataWebService()
            let exerciseService = ExerciseDataWebService()
            self.presenter = ExercisePresenter(uploaderService: uploaderService, temperatureService: temperatureService, stepCountService: stepCountService, pulseRateService: pulseRateService, breathingRateService: breathingRateService, oxygenRateService: oxygenRateService, delegate: self, exerciseService: exerciseService)
            BluetoothManager.shared.setBluetoothService(serivce: presenter)
		}
	}
	
	func registerCollectionCell() {
		let parameterCell = UINib.init(nibName: "ParameterCollectionViewCell", bundle: nil)
		self.dataCollectionView.register(parameterCell, forCellWithReuseIdentifier: ParameterCollectionViewCell.IDENTIFIER)
	}
	
	func assignDelegates() {
		self.dataCollectionView.delegate = self
		self.dataCollectionView.dataSource = self
	}
	
	func upload(data: Data) {
		
		if let url = self.manager.addFile(data: data, fileName: (KeyChain.shared.firstName + "_" + KeyChain.shared.lastName)) {
			self.fileUrl = url
			
			self.presenter?.postFileInfo(filename: KeyChain.shared.firstName + "_" + KeyChain.shared.lastName + ".zip", username: (KeyChain.shared.firstName + "_" + KeyChain.shared.lastName))
		}
	}
}

extension StopExerciseViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: (collectionView.bounds.width / 2.0) - 9, height: 75)
	}
}

extension StopExerciseViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.parameterList.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = ParameterCollectionViewCell.dequeueReusableCell(collectionView: collectionView, indexPath: indexPath)
		
		let parameter = self.parameterList[indexPath.row]
		if parameter.name == "CHARGING POWER" && (parameter.reading?.toDouble == 0 || parameter.reading?.toDouble == nil) {
			cell.parameterImage.image = UIImage(named: "")
			cell.parameterLabel.text = ""
			cell.readingLabel.text = ""
		} else {
			if let reading = parameter.reading {
				if parameter.name == "SKIN TEMP VAR" {
					let value = abs(Double(reading) ?? 0)
					if value == 32.0 {
						cell.readingLabel.text = "-"
					} else {
						cell.readingLabel.attributedText = cell.readingData(value: "\(value)", unit: parameter.unit)
					}
				} else {
					if (reading.toDouble != 0 && reading.toDouble != nil) {
						cell.readingLabel.attributedText = cell.readingData(value: reading, unit: parameter.unit)
					} else {
						cell.readingLabel.text = "-"
					}
				}
			} else {
				cell.readingLabel.text = "-"
			}
			cell.parameterImage.image = UIImage(named: parameter.iconName ?? "")
			cell.parameterLabel.text = (parameter.name == "SKIN TEMP VAR") ? "SKIN TEMP" : parameter.name
		}
		
		return cell
	}
}

extension StopExerciseViewController: ExerciseViewDelegate {
    
    func showWrongDeviceError() {
        let actions = [UIAlertAction(title: "Try Again", style: .default, handler: {[weak self] (_) in
            self?.navigationController?.popToRootViewController(animated: true)
        })]
        self.showAlert(title: "Error", message: "Please connect to correct Movano device", actions: actions)
    }
    
    
    func showDeviceDisconnectedError(){
        var actions = [UIAlertAction]()
        
        actions.append(UIAlertAction(title: "Upload", style: .default, handler: { [weak self](_) in
            hud.show(in: self?.view ?? UIView())
            self?.presenter?.callAllApisToUploadData()
        }))
        
        actions.append(UIAlertAction(title: "Discard", style: .cancel, handler: { (_) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        
        self.showAlert(title: "Error", message: "Movano device disconnected. Do you want to upload recorded data?", actions: actions)
    }
	func postBreathingRateSuccess(with responseModel: [Reading]) {
        if(responseModel.count > 0){
            var breathingRateTotal = 0.0
            for eachItem in responseModel{
                breathingRateTotal = eachItem.reading + breathingRateTotal
            }
            let breathingRateAvg = breathingRateTotal / Double(responseModel.count)
            self.breathingRate = Int(breathingRateAvg)
        }
       
    }
	
	func postBreathingRateErrorHandler(error: MovanoError) {
		
	}
	
	func postTempSuccess(with responseModel: [Reading]) {
        if(responseModel.count > 0){
            var totalTemp = 0.0
            for eachItem in responseModel{
                let tempVar = Utility.getTempVariation(temperature: eachItem.reading)
                totalTemp = totalTemp + eachItem.reading
            }
            
            let avgTemp = totalTemp / Double(responseModel.count)
            self.skinTemp = Float(avgTemp)
        }
        
        
	}
	
	func postTempErrorHandler(error: MovanoError) {
		
	}
    
    func postStepsSuccess(with stepCount: Int){
        // Convert steps to calories and
        let heightInInches = KeyChain.shared.height?.toDouble ?? 0.0
        let weight = ((KeyChain.shared.weight?.toDouble ?? 0.0) * 0.453592) // lbs to kg
        let strideLength = UtilityConstant.STRIDE_LENGTH_CONSTANT * heightInInches
        let tempDistanceCoveredKm = (strideLength * Double(stepCount)).inchToKm() // inch to KM
        let distanceInMile = (strideLength * Double(stepCount)).inchToMile()
        let finalCaloriesBurnt = ((tempDistanceCoveredKm * weight * 1.036) * 100.0) / 100
        self.stepCount = stepCount
        self.caloriesBurnt = Float(finalCaloriesBurnt)
        self.distance = distanceInMile
    }
	
	func postStepsSuccess(with responseModel: [CoreDataStepCount]) {
		if let reading = responseModel.last {
            self.stepCount = Int(truncating: reading.value)
            self.caloriesBurnt = Float(truncating: reading.calories ?? 0)
            self.distance = Double(truncating: reading.distance ?? 0)
		} else {
			self.stepCount = 0
			self.caloriesBurnt = 0
			self.distance = 0
		}
	}
	
	func postStepsErrorHandler(error: MovanoError) {
		
	}
	
	func postCaloriesErrorHandler(error: MovanoError) {
		
	}
	
	func postSpO2Success(with responseModel: [Reading]) {
        if(responseModel.count > 0){
            var totalSpo2 = 0.0
            for eachItem in responseModel{
                totalSpo2 = totalSpo2 + eachItem.reading
            }
            
            let avgSpo2 = totalSpo2 / Double(responseModel.count)
            self.spo2 = Int(avgSpo2)
        }
       
	}
	
	func postSpO2ErrorHandler(error: MovanoError) {
		
	}
	
	func postDistanceSuccess(with responseModel: [CoreDataActivity]) {
        if responseModel.last != nil {
//			self.distance = (reading.value)
		} else {
//			self.distance = 0
		}
	}
	
	func postDistanceErrorHandler(error: MovanoError) {
		
	}
	
	func zipFile(error: MovanoError, completionHandler: @escaping () -> Void) {
		let actions = [UIAlertAction(title: "Try Again", style: .default, handler: { (_) in
			self.navigationController?.popToRootViewController(animated: true)
		})]
		self.showAlert(title: "Error", message: error.errorMessage, actions: actions)
		completionHandler()
	}
	
	func zipFile(data: Data, completionHandler: @escaping () -> Void) {
		self.upload(data: data)
		completionHandler()
	}
	
	func postFileInfoSuccess(with responseModel: FetchPresignResponseModel, timeStamp: TimeInterval) {
		
		request = MessageToSnsRequestModel(file_name: responseModel.data.file_name, folder_name: responseModel.data.folder_name, file_timestamp: Int(timeStamp))
		let url = responseModel.data.url
		self.presenter?.uploadFileToPresigned(url: url, fileUrl: self.fileUrl)
	}
	
	func postFileInfoErrorHandler(error: MovanoError) {
		self.showErrorAlert(error: error)
	}
	
	func uploadFileToPresignedCompleted() {
		self.presenter?.putMessageToSns(request: self.request)
		self.manager.deleteFile(self.fileUrl)
	}
	
	func uploadFileToPresignedErrorHandler(error: MovanoError) {
		self.showErrorAlert(error: error)
	}
	
    func navigateToDetailsScreen(timeRange: TimeRange, error: MovanoError?){
		let controller = ExerciseDataViewController.instantiate(fromAppStoryboard: .dashboard)
		controller.request = request
		controller.stepCount = stepCount
		controller.breathingRate = breathingRate
		controller.heartRate = heartRate
		controller.distance = distance
		controller.temperatureReading = ((Double(skinTemp) * 10).rounded() / 10)
		controller.spo2 = spo2
		controller.timeRange = timeRange
		controller.caloriesBurnt = caloriesBurnt
		controller.elementList = elementList
		controller.totalTime = self.totalTime
		
        CoreDataExercise.add(startTime: timeRange.startTime, endTime: timeRange.endTime, systolic: 0.0, diastolic: 0.0, breathingRate: Double(breathingRate), skinTemperature: ((Double(skinTemp) * 100).rounded() / 100), pulseRate: Double(heartRate), caloriesBurnt: Double(caloriesBurnt), stepCount: Double(stepCount), distanceCovered: distance, oxygen: Double(spo2))

        if(afterAllDataFetched){
            afterAllDataFetched = false
			if let error = error {
				let alert = AlertController(title: "", message: "\(error.errorMessage)", preferredStyle: .alert)
				let okAction = UIAlertAction(title: "OK", style: .default) {_ in
					self.navigationController?.pushViewController(controller, animated: true)
				}
				okAction.setValue(UIColor.primaryColor, forKey: "titleTextColor")
				alert.addAction(okAction)
				self.present(alert, animated: true, completion: nil)
			} else {
				self.navigationController?.pushViewController(controller, animated: true)
			}
		} else {
			if let error = error {
				let alert = AlertController(title: "", message: "\(error.errorMessage)", preferredStyle: .alert)
				let okAction = UIAlertAction(title: "OK", style: .default){_ in
					self.navigationController?.pushViewController(controller, animated: true)
				}
				okAction.setValue(UIColor.primaryColor, forKey: "titleTextColor")
				alert.addAction(okAction)
				self.present(alert, animated: true, completion: nil)
			}
		}
        
    }
	func putMessageToSnsSuccess(timeRange: TimeRange) {
//		let controller = ExerciseDataViewController.instantiate(fromAppStoryboard: .dashboard)
//		controller.request = request
//		controller.stepCount = stepCount
//		controller.breathingRate = breathingRate
//		controller.heartRate = heartRate
//		controller.distance = distance
//		controller.temperatureReading = ((Double(skinTemp) * 10).rounded() / 10)
//		controller.spo2 = spo2
//		controller.timeRange = timeRange
//		controller.caloriesBurnt = caloriesBurnt
//		controller.elementList = elementList
//		controller.totalTime = self.totalTime
//		self.navigationController?.pushViewController(controller, animated: true)
	}
	
	func putMessageToSnsErrorHandler(error: MovanoError) {
		self.showErrorAlert(error: error)
	}
	
	func timer(currentStopWatchInstance: Int) {
		self.timerLabel.text = currentStopWatchInstance.toCounterFormat
		self.totalTime = currentStopWatchInstance
	}
	
	func fetchExerciseParameter(stepCount: Int, lastNonZeroStepCount: Int = 0) {
		self.stepCount = lastNonZeroStepCount
		self.parameterList[2].reading = "\(stepCount)"
	}
	
	func fetchExerciseParameter(heartRate: Int) {
		self.heartRate = heartRate
		self.heartRateLabel.text = "\(heartRate)"
		self.parameterList[0].reading = "\(heartRate)"
	}
	
	func fetchExerciseParameter(distance: Double?) {
		self.distance = distance
		if let distance = distance {
			self.parameterList[3].reading = "\(distance)"
		} else {
			self.parameterList[3].reading = nil
		}
	}
	
	func fetchExerciseParameter(breathingRate: Int) {
		self.breathingRate = breathingRate
		//self.parameterList[3].reading = "\(breathingRate)"
	}
	
	func fetchExerciseParameter(spo2: Int) {
		self.spo2 = spo2
		self.parameterList[1].reading = "\(spo2)"
	}
	
	func fetchExerciseParameter(skinTemp: Float) {
		let tempSkin = ((skinTemp * 10).rounded() / 10)
		self.skinTemp = tempSkin
		self.parameterList[4].reading = "\(tempSkin)"
	}
	
	func fetchExerciseCalorieParameter(stepCount: Int) {
		let heightInInches = KeyChain.shared.height?.toDouble ?? 0.0
		let weight = ((KeyChain.shared.weight?.toDouble ?? 0.0) * 0.453592) // lbs to kg
		let strideLength = UtilityConstant.STRIDE_LENGTH_CONSTANT * heightInInches
		let tempDistanceCoveredKm = (strideLength * Double(stepCount)).inchToKm() // inch to KM
		let finalCaloriesBurnt = ((tempDistanceCoveredKm * weight * 1.036) * 100.0).rounded() / 100
		self.caloriesBurnt = Float(finalCaloriesBurnt)
	}
	
	func fetchChargingCurrent(chargingCurrent: Int) {
		self.parameterList[5].reading = "\(chargingCurrent)"
	}
	
	func createChartElement(reading: [Reading]) {
		var chartElements = [ChartElement]()
		for x in 0..<reading.count {
			chartElements.append(ChartElement(date: Date(timeIntervalSince1970: reading[x].timeStamp), value: reading[x].reading))
		}
		self.elementList = chartElements
	}
	
	func removeActivityIndicator() {
		hud.dismiss()
	}
}
