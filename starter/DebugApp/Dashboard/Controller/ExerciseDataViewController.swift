//
//  ExerciseDataViewController.swift
//  BloodPressureApp
//
//  Created by Rushikant on 21/07/21.
//

import UIKit
import Highcharts

// swiftlint:disable all
class ExerciseDataViewController: UIViewController {
	
	@IBOutlet weak var mainScrollView: UIScrollView!
	@IBOutlet weak var dateLabel: MovanoLabel!
	@IBOutlet weak var dataCollectionView: UICollectionView!
	@IBOutlet weak var collectionHeight: NSLayoutConstraint!
	@IBOutlet weak var dateValueLabel: MovanoLabel!
	
	//ChartView
	@IBOutlet weak var chartSuperView: UIView!
	@IBOutlet weak var totalTimeLabel: MovanoLabel!
	@IBOutlet weak var bpValueLabel: MovanoLabel!
	@IBOutlet weak var chartView: HIChartView!
	
	@IBOutlet weak var topConstraintForChartView: NSLayoutConstraint!
	
	var presenter: ExerciseReadingPresenter?
	var temperatureReading: Double!
	var request: MessageToSnsRequestModel!
	var breathingRate: Int!
	var distance: Double?
	var heartRate: Int!
	var stepCount: Int!
	var spo2 = Int(0)
	var timeRange: TimeRange!
	var caloriesBurnt = Float(0)
	var elementList = [ChartElement]()
	var totalTime = 0
	var isCameFromActivitySession = false
	var startTimeForGraph = Date()
	var endTimeForGraph = Date()
	
	var parameterList = [Parameter]() {
		didSet {
			self.dataCollectionView.reloadData()
		}
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let exerciseService = ExerciseDataWebService()
			let bpService = BloodPressureWebService()
			let pulseRateService = PulseRateWebService()
			self.presenter = ExerciseReadingPresenter(bpService: bpService, exerciseService: exerciseService, pulseRateService: pulseRateService, delegate: self)
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.registerPresenters()
		self.registerCollectionCell()
		self.assignDelegates()
		
		self.dataCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
		let tableHeight = self.collectionHeight
//		self.topConstraintForChartView.constant = (UIScreen.main.bounds.height - 440)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.loadChartData(chartElement: self.elementList)
		self.presenter?.fetchParameters()
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.collectionHeight.constant = self.dataCollectionView.contentSize.height
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if !isCameFromActivitySession {
			hud.show(in: self.view)
            self.dateValueLabel.text = Date(timeIntervalSince1970: timeRange.startTime) .dateToString(format: "dd MMM, hh:mm a")
			self.chartSuperView.isHidden = false
           // self.presenter?.addPauseData(timeRange: self.timeRange)
		} else {
			hud.show(in: self.view)
			let rangeStartTime = self.startTimeForGraph
			let rangeEndTime = self.endTimeForGraph
			
			// Set sesssion date
			self.dateValueLabel.text = rangeStartTime.dateToString(format: "dd MMM, hh:mm a")
			
			// Fetch heart rate data for selected session
//			self.presenter?.fetchHeartRateData(range: DateRange(start: rangeStartTime, end: rangeEndTime, type: .daily))
			self.chartSuperView.isHidden = true
			self.chartView.isHidden = true
			hud.dismiss(animated: true)
		}
	}
	
	func registerCollectionCell() {
		let parameterCell = UINib.init(nibName: "ParameterCollectionViewCell", bundle: nil)
		self.dataCollectionView.register(parameterCell, forCellWithReuseIdentifier: ParameterCollectionViewCell.IDENTIFIER)
	}
	
	func assignDelegates() {
		self.dataCollectionView.delegate = self
		self.dataCollectionView.dataSource = self
	}
	
	@IBAction func backButtonClicked(_ sender: MovanoButton) {
        if(!isCameFromActivitySession){
            NotificationCenter.default.post(name: NSNotification.Name("updateStepCount"), object: nil)
        }
        isCameFromActivitySession = false

		self.navigationController?.popToRootViewController(animated: true)
	}
	
	@IBAction func doneButtonClicked(_ sender: Any) {
		NotificationCenter.default.post(name: NSNotification.Name("updateStepCount"), object: nil)
		self.navigationController?.popToRootViewController(animated: false)
	}
	
	func loadChartData(chartElement: [ChartElement]) {
		let chartData = ActivityChartData(barColor: .barPurple, elements: chartElement)
		let totalPulseSum = chartElement.reduce(0) { (result: Double, nextItem: ChartElement) -> Double in
			return result + (nextItem.value ?? 0)
		}
		
		var nonoZeroValuesCount = 0
		for x in 0..<chartElement.count {
			if chartElement[x].value != 0.0 {
				nonoZeroValuesCount = nonoZeroValuesCount + 1
			}
		}
		
		let pulseCountAvg = ((chartElement.count != 0) && (nonoZeroValuesCount != 0)) ? NSNumber(value: (totalPulseSum)/Double(nonoZeroValuesCount)) : 0
		let heartRateModel = ActivityChartModel(type: "Heart Rate", value: pulseCountAvg, goal: nil, unit: "bpm", chartData: chartData)
		let pulseRateRange = UserDefault.shared.goals.pulseRate.value as! Range<Int>
		self.loadChartView(model: heartRateModel, limitLineValues: [pulseRateRange.lowerBound, pulseRateRange.upperBound])
	}
	
	func loadChartView(model: ActivityChartModel, limitLineValues: [Int] = [], isSleep: Bool = false) {
		self.bpValueLabel.text = "\(Int(model.value ?? 0))"
		if let unit = model.unit {
			self.bpValueLabel.attributedText = self.unitAttributeLabel(value: self.bpValueLabel.text ?? "0", unit: unit)
		}
		
		// second to h & m
		let hour = self.totalTime / 3600
		let min = (self.totalTime % 3600) / 60
		//		let second = (self.totalTime % 3600) % 60
		
		// Attributed text for total sleep
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.semiBold(withSize: 18)]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.gilroy.regular(withSize: 12)]
		
		let customeTotaTimeText = NSMutableAttributedString(string: "\(hour)", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1))
		customeTotaTimeText.append(NSAttributedString(string: "h ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		customeTotaTimeText.append(NSAttributedString(string: "\(min)", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
		customeTotaTimeText.append(NSMutableAttributedString(string: "m", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		self.totalTimeLabel.attributedText = customeTotaTimeText
		
		if model.chartData.elements.count > 0 {
			self.fetchActivityChart(chartData: model.chartData, limitLineValues: limitLineValues)
		}
	}
	
	private func fetchActivityChart(chartData: ActivityChartData, limitLineValues: [Int] = []) {
		
		var xAxis = [TimeInterval]()
		xAxis = chartData.elements.map({ (element) -> TimeInterval in
			return element.date.timeIntervalSince1970
		})
		
		var data = [[Double?]]()
		
		for (pointIndex, each) in chartData.elements.enumerated() {
			if each.value == 0.0 {
				data.append([xAxis[pointIndex] * 1000, nil])
			} else {
				data.append([xAxis[pointIndex] * 1000, each.value])
			}
		}
		
		var nonZeroData: [ChartElement] = []
		for (pointIndex, each) in chartData.elements.enumerated() {
			if each.value != 0.0 && each.value != nil {
				nonZeroData.append(ChartElement(date: Date(timeIntervalSince1970: (xAxis[pointIndex] * 1000)), value: each.value))
			}
		}
		var minValueForHR = 0.0
		if nonZeroData.count != 0 {
			minValueForHR = nonZeroData.reduce(nonZeroData.last!) { aggregate, element in
				(aggregate.value ?? 0) > (element.value ?? 0) ? element : aggregate
			}.value ?? 0
		}
		
		let maxValueForHR = chartData.elements.reduce(chartData.elements.last!) { aggregate, element in
			(aggregate.value ?? 0) < (element.value ?? 0) ? element : aggregate
		}.value ?? 0
		
		let values = [data]
		let chart = HIChart()
		chart.type = "line"
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
//		hixAxis.dateTimeLabelFormats.day = HIDay()
//		hixAxis.dateTimeLabelFormats.day.main = "%l %p"
//		hixAxis.dateTimeLabelFormats.hour = HIHour()
//		hixAxis.dateTimeLabelFormats.hour.main = "%l %p"
//		hixAxis.dateTimeLabelFormats.minute = HIMinute()
//		hixAxis.dateTimeLabelFormats.minute.main = "%l %p"
	
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		hixAxis.tickWidth = 2
		hixAxis.lineWidth = 0
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		hixAxis.tickInterval = NSNumber(value: 60 * 1000)
		
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.overflow = "justify"
		yAxis.labels.style = HICSSObject()
		yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		yAxis.labels.style.fontSize = "12"
		yAxis.labels.style.color = "#a5a5b2"
		yAxis.gridLineWidth = 0
		
//		if limitLineValues[1] > (Int(maxValueForHR) + 5) {
//			yAxis.max = NSNumber(value: limitLineValues[1] + 5)
//		} else {
//			yAxis.max = NSNumber(value: Int(maxValueForHR) + 5)
//		}
//
//		if limitLineValues[0] < (Int(minValueForHR) - 5) {
//			yAxis.min = NSNumber(value: limitLineValues[0] - 5)
//		} else {
//			yAxis.min = NSNumber(value: Int(minValueForHR) - 5)
//		}
		
		yAxis.max = NSNumber(value: 200)
		yAxis.min = NSNumber(value: 0)
		yAxis.tickInterval = NSNumber(value: 20)
		yAxis.minTickInterval = NSNumber(value: 20)
		
		let plotLines1 = HIPlotLines()
		plotLines1.value = yAxis.min
		plotLines1.width = 1
		plotLines1.color = HIColor(uiColor: .white)
		yAxis.plotLines = [plotLines1]
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
//
		hixAxis.labels.enabled = true
		hixAxis.gridLineWidth = 0
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		
		let plotoptions = HIPlotOptions()
		plotoptions.line = HILine()
		plotoptions.line.states = HIStates()
		plotoptions.line.states.inactive = HIInactive()
		plotoptions.line.states.inactive.opacity = NSNumber(value: true)
		plotoptions.line.animation = HIAnimationOptionsObject()
		plotoptions.line.animation.duration = NSNumber(value: 0)
		plotoptions.line.marker = HIMarker()
		plotoptions.line.marker.enabled = false
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		let marker = HIMarker()
		marker.enabled = true
		
		var series = [HISeries]()
		let line = HILine()
		line.name = "Data"
		line.data = values[0] as [Any]
		if line.data.count == 1 {
			line.marker = marker
		}
		series.append(line)
		
		let uiColors = [chartData.barColor.hexString]
		
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = series
		options.colors = uiColors
		options.plotOptions = plotoptions
		options.time = time
		options.legend = legend
		
		self.chartView.options = options
		
		hud.dismiss(animated: true)
	}
	
	func unitAttributeLabel(value: String, unit: String) -> NSAttributedString {
		let str = NSMutableAttributedString()
		
		let attrs1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.semiBold(withSize: 16), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textColor]
		let attrs2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.gilroy.regular(withSize: 12), convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.textColor]
		
		str.append(NSMutableAttributedString(string: value + " ", attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs1)))
		str.append(NSMutableAttributedString(string: unit, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs2)))
		
		return str
		
	}
}

// MARK: - UICollectionViewDelegate
extension ExerciseDataViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: (collectionView.bounds.width / 2.0) - 9, height: 75)
	}
	
}

// MARK: - UICollectionViewDataSource
extension ExerciseDataViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.parameterList.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = ParameterCollectionViewCell.dequeueReusableCell(collectionView: collectionView, indexPath: indexPath)
		
		let parameter = self.parameterList[indexPath.row]
		if let reading = parameter.reading {
			if parameter.name == "SKIN TEMP VAR" {
				let value = abs(Double(reading) ?? 0)
				if value == 32 {
					cell.readingLabel.attributedText = cell.readingData(value: "0.0", unit: parameter.unit)
				} else {
					let tempValue = (value * 10).rounded() / 10
					cell.readingLabel.attributedText = cell.readingData(value: "\(tempValue)", unit: parameter.unit)
				}
				
			} else if parameter.name == "CALORIES  BURNED" {
				let readingInInt = Int(Double(reading)?.rounded() ?? 0)
				cell.readingLabel.attributedText = cell.readingData(value: "\(readingInInt)", unit: parameter.unit)
			} else {
				cell.readingLabel.attributedText = cell.readingData(value: reading, unit: parameter.unit)
			}
		} else {
			cell.readingLabel.text = "-"
		}
		
		cell.parameterImage.image = UIImage(named: parameter.iconName ?? "")
		cell.parameterLabel.text = (parameter.name == "SKIN TEMP VAR") ? "SKIN TEMP" : parameter.name
		
		return cell
	}
}

// MARK: - ExerciseReadingViewDelegate
extension ExerciseDataViewController: ExerciseReadingViewDelegate {
	
	func getBpByTimeStampSuccess(with responseModel: [QueryClinicalTrialUsersDataByTimestampRangeQuery.Data.QueryClinicalTrialUsersDataByTimestampRange.Item?]) {
		if let bp = responseModel.first {
//			self.parameterList[6].reading = "\(bp!.systolic!) / \(bp!.diastolic!)"
//			self.dateLabel.text = Date(timeIntervalSince1970: bp!.dataTimestamp).dateToString(format: "dd MMM yyyy | hh:mm a")
			self.dateValueLabel.text = Date(timeIntervalSince1970: bp!.dataTimestamp).dateToString(format: "dd MMM, hh:mm a")
			self.presenter?.addPauseData(timeRange: self.timeRange)
		}
	}
	
	func getBpByTimeStampErrorHandler(error: MovanoError) {
		self.showErrorAlert(error: error)
		hud.dismiss()
	}
	
	func parameter(list: [Parameter]) {
		self.parameterList = list
		self.parameterList[4].reading = "\(self.temperatureReading!)"
		self.parameterList[5].reading = "\(self.breathingRate!)"
		self.parameterList[3].reading = self.distance?.stringValue
//		self.parameterList[7].reading = "\(self.heartRate!)"
		self.parameterList[0].reading = "\(Int(self.caloriesBurnt.rounded()))"
		self.parameterList[1].reading = "\(self.stepCount!)"
		self.parameterList[2].reading = "\(self.spo2)"
	}
	
	func postExerciseDataSuccess(with responseModel: CoreDataExercise) {
		hud.dismiss()
	}
	
	func postExerciseDataErrorHandler(error: MovanoError) {
		hud.dismiss()
	}
	
	func fetchHeartRateSuccess(model: ActivityChartModel) {
		self.loadChartView(model: model)
	}
}
