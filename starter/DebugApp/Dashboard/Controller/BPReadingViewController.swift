//
//  BPReadingViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 02/07/21.
//

import UIKit

class BPReadingViewController: UIViewController {

	@IBOutlet weak var dateLabel: MovanoLabel!
	@IBOutlet weak var dataCollectionView: UICollectionView!
	@IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var btnDone: MovanoButton!
    
	var presenter: BPReadingPresenter?
	var request: MessageToSnsRequestModel!
	var temperatureReading: Double!
	var breathingRateReading: Int!
	var timeRange: TimeRange!
	var bpReading = "0/0"
	var heartRate = "0"
	var isCameFromActivitySession = false
	var pulseRateReading = 0
	
	var parameterList = [Parameter]() {
		didSet {
			self.dataCollectionView.reloadData()
		}
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let pauseReadingService = PauseDataWebService()
			let bpService = BloodPressureWebService()
			self.presenter = BPReadingPresenter(bpService: bpService, pauseDataService: pauseReadingService, delegate: self)
		}
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()

		self.registerPresenters()
		self.registerCollectionCell()
		self.assignDelegates()
		
		self.dataCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
		
		if isCameFromActivitySession {
            btnDone.isHidden = true
			let bpReading = Parameter(name: "BLOOD PRESSURE", reading: self.bpReading, unit: "mmHg",iconName: "sleep-BPIcon")
			let heartRateReading = Parameter(name: "HEART RATE", reading: self.heartRate, unit: "bpm",iconName: "sleep-HeartIcon")
			let tempReading = Parameter(name: "SKIN TEMP VAR", reading: "\(self.temperatureReading)", unit: "°F",iconName: "sleep_SkintempIcon")
			let breathingRate = Parameter(name: "BREATHING RATE", reading: self.bpReading, unit: "brpm",iconName: "sleep-BreathingIcon")
			self.parameter(list: [bpReading, heartRateReading, tempReading, breathingRate])
        }else{
            btnDone.isHidden = false
        }
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		if !isCameFromActivitySession {
			hud.show(in: self.view)
			self.presenter?.fetchParameters()
			self.presenter?.fetchReading(request: self.request)
		}
		isCameFromActivitySession = false
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
		self.collectionHeight.constant = self.dataCollectionView.contentSize.height
	}
	
	func registerCollectionCell() {
		let parameterCell = UINib.init(nibName: "ParameterCollectionViewCell", bundle: nil)
		self.dataCollectionView.register(parameterCell, forCellWithReuseIdentifier: ParameterCollectionViewCell.IDENTIFIER)
	}
	
	func assignDelegates() {
		self.dataCollectionView.delegate = self
		self.dataCollectionView.dataSource = self
	}
	
	@IBAction func backButtonClicked(_ sender: MovanoButton) {
        NotificationCenter.default.post(name: NSNotification.Name("updateStepCount"), object: nil)
		self.navigationController?.popToRootViewController(animated: true)
	}
	
	@IBAction func doneButtonClicked(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("updateStepCount"), object: nil)
		self.navigationController?.popToRootViewController(animated: false)
	}
}

// MARK: - UICollectionViewDelegate
extension BPReadingViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: (collectionView.bounds.width / 2.0) - 5, height: 75)
	}
	
}

// MARK: - UICollectionViewDataSource
extension BPReadingViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.parameterList.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = ParameterCollectionViewCell.dequeueReusableCell(collectionView: collectionView, indexPath: indexPath)
		
		let parameter = self.parameterList[indexPath.row]
		if let reading = parameter.reading {
			cell.readingLabel.attributedText = cell.readingData(value: reading, unit: parameter.unit)
		} else {
			cell.readingLabel.text = "-"
		}
		
		cell.parameterImage.image = UIImage(named: parameter.iconName ?? "")
		cell.parameterLabel.text = parameter.name
		
		return cell
	}
}

// MARK: - BloodPressureViewDelegate
extension BPReadingViewController: BloodPressureViewDelegate {
	
	func getBpByTimeStampSuccess(with responseModel: [QueryClinicalTrialUsersDataByTimestampRangeQuery.Data.QueryClinicalTrialUsersDataByTimestampRange.Item?]) {
		if let bp = responseModel.first {
			self.parameterList[0].reading = "\(bp!.systolic!) / \(bp!.diastolic!)"
			self.bpReading = "\(bp!.systolic!) / \(bp!.diastolic!)"
			self.parameterList[1].reading = "\(pulseRateReading)"
			self.heartRate = "\(pulseRateReading)"
			self.dateLabel.text = "Take a Pause"
//			Date(timeIntervalSince1970: bp!.dataTimestamp).dateToString(format: "dd MMM yyyy | hh:mm a")
			self.presenter?.addPauseData(timeRange: self.timeRange)
		}
	}
	
	func getBpByTimeStampErrorHandler(error: MovanoError) {
		self.showErrorAlert(error: error)
		hud.dismiss()
	}
	
	func postPauseDataErrorHandler(error: MovanoError) {
		hud.dismiss()
	}
	
	func postPauseDataSuccess(with reading: CoreDataPauseReading) {
		hud.dismiss()
	}
	
	func parameter(list: [Parameter]) {
		let tempValue = (Utility.getTempVariation(temperature: (self.temperatureReading ?? 0.0)) * 10).rounded() / 10
		
		self.parameterList = list
		self.parameterList[0].reading = self.bpReading
		self.parameterList[1].reading = "\(self.heartRate)"
		self.parameterList[2].reading = "\(tempValue)"
		self.parameterList[3].reading = "\(self.breathingRateReading ?? 0)"
	}
}
