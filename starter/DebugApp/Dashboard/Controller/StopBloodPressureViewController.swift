//
//  StopBloodPressureViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/06/21.
//

import UIKit
import HGCircularSlider

// swiftlint:disable all
class StopBloodPressureViewController: UIViewController {
	
	@IBOutlet weak var circularSlider: CircularSlider!
	@IBOutlet weak var timerLabel: MovanoLabel!
	@IBOutlet weak var stopButton: MovanoButton!
	
	let manager = DocumentManager(manager: .default)
	var fileUrl: URL!
	var totalCountDown = 30
	var presenter: UploadBPReadingPresenter?
	var request: MessageToSnsRequestModel!
	var temperatureReading = Double(0)
	var breathingRate = Int(0)
	var pulseRateReading = Int(0)
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.registerPresenters()
		circularSlider.minimumValue = 0.0
		circularSlider.maximumValue = CGFloat(totalCountDown)
		circularSlider.endPointValue = circularSlider.maximumValue
		
		self.presenter?.startTimer()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		hud.dismiss()
		self.presenter?.stopTimer()
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let pauseReadingService = PauseDataWebService()
			let pulseRateService = PulseRateWebService()
			let temperatureService = SkinTemperatureWebService()
			let uploaderService = DataUploaderWebService()
			let breathingRateService = BreathingRateWebService()
			self.presenter = UploadBPReadingPresenter(uploaderService: uploaderService, temperatureService: temperatureService, pauseDataService: pauseReadingService, pulseRateService: pulseRateService, breathingRateService: breathingRateService, delegate: self)
		}
	}
	
	@IBAction func backButtonClicked(_ sender: MovanoButton) {
		self.navigationController?.popToRootViewController(animated: true)
	}

	@IBAction func stopButtonClicked(_ sender: Any) {
		self.presenter?.stopTimer()
		self.presenter?.zipFile()
	}
	
	func upload(data: Data) {
		
		if let url = self.manager.addFile(data: data, fileName: (KeyChain.shared.firstName + "_" + KeyChain.shared.lastName)) {
			self.fileUrl = url
			
			hud.show(in: self.view)
			self.presenter?.postFileInfo(filename: KeyChain.shared.firstName + "_" + KeyChain.shared.lastName + ".zip", username: (KeyChain.shared.firstName + "_" + KeyChain.shared.lastName))
		}
	}
}

// MARK: - UploadBPReadingViewDelegate
extension StopBloodPressureViewController: UploadBPReadingViewDelegate {
	
	func zipFile(error: MovanoError) {
		let actions = [UIAlertAction(title: "Try Again", style: .default, handler: { (_) in
			self.navigationController?.popToRootViewController(animated: true)
		})]
		self.showAlert(title: "Error", message: error.errorMessage, actions: actions)
	}
	
	func zipFile(data: Data) {
		self.showUploadPopup(data: data) {
			self.upload(data: data)
		}
	}
	
	func timer(current StopWatch: Int) {
		self.timerLabel.text = StopWatch.toCounterFormat
		self.circularSlider.endPointValue = CGFloat(StopWatch)
	}
	
	func postFileInfoSuccess(with responseModel: FetchPresignResponseModel, timeStamp: TimeInterval) {
		
		request = MessageToSnsRequestModel(file_name: responseModel.data.file_name, folder_name: responseModel.data.folder_name, file_timestamp: Int(timeStamp))
		let url = responseModel.data.url
		self.presenter?.uploadFileToPresigned(url: url, fileUrl: self.fileUrl)
        self.presenter?.addTemperatureReading(for: timeStamp)
		self.presenter?.addPulseRateReading(for: timeStamp)
		self.presenter?.addBreathingRateReading(for: timeStamp)
	}
	
	func postFileInfoErrorHandler(error: MovanoError) {
		self.showErrorAlert(error: error)
	}
	
	func uploadFileToPresignedCompleted() {
		self.presenter?.putMessageToSns(request: self.request)
		self.manager.deleteFile(self.fileUrl)
	}
	
	func uploadFileToPresignedErrorHandler(error: MovanoError) {
		self.showErrorAlert(error: error)
	}
	
	func putMessageToSnsSuccess(with responseModel: MessageToSnsResponseModel, timeRange: TimeRange) {
		let controller = BPReadingViewController.instantiate(fromAppStoryboard: .dashboard)
		controller.request = request
		controller.temperatureReading = temperatureReading
		controller.breathingRateReading = breathingRate
		controller.timeRange = timeRange
		controller.pulseRateReading = pulseRateReading
		self.navigationController?.pushViewController(controller, animated: true)
	}
	
	func putMessageToSnsErrorHandler(error: MovanoError) {
		self.showErrorAlert(error: error)
	}
	
	func postTemperatureReadingSuccess(with responseModel: [CoreDataSkinTemperature]) {
		if let reading = responseModel.last {
			self.temperatureReading = reading.value.doubleValue
		} else {
			self.temperatureReading = 0
		}
	}
	
	func postTemperatureReadingErrorHandler(error: MovanoError) {
	}
	
	func postPulseRateSuccess(with responseModel: [CoreDataPulseRate]) {
		if let reading = responseModel.last {
			self.pulseRateReading = Int(reading.value)
		} else {
			self.pulseRateReading = 0
		}
	}
	
	func postPulseRateErrorHandler(error: MovanoError) {
	}
	
	func postBreathingRateSuccess(with responseModel: [CoreDataBreathingRate]) {
		if let reading = responseModel.last {
			self.breathingRate = Int(reading.value.doubleValue)
		} else {
			self.breathingRate = 0
		}
	}
	
	func postBreathingRateErrorHandler(error: MovanoError) {
		
	}
	
	func removeActivityIndicator() {
		hud.dismiss()
	}
}
