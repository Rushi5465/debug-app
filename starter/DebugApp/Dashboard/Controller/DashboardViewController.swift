//
//  DashboardViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 23/07/21.
//

import UIKit
import HGCircularSlider
import SwiftUI
// swiftlint:disable all
class DashboardViewController: UIViewController {
	
    // MARK: - IB Outlets
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var rightIndicatorView: UIView!
    @IBOutlet weak var leftIndicatorView: UIView!
    @IBOutlet weak var lblHealthScore: MovanoLabel!
    
    @IBOutlet weak var leftIndicaotrImageView: UIImageView!
    
    @IBOutlet weak var leftIndicatorLable: MovanoLabel!
    
    @IBOutlet weak var rightIndicatorImageView: UIImageView!
    @IBOutlet weak var rightIndicatorLable: MovanoLabel!
    
    @IBOutlet weak var rightIndicatorButton: MovanoButton!
    @IBOutlet weak var leftIndicatorButton: MovanoButton!
    var bluetoothManager: BluetoothManager!
	
	var presenter: DashboardPresenter?
    var metricIndicatorModelLeft: MetricIndicatorModel?
    var metricIndicatorModelRight: MetricIndicatorModel?
    var sleepMetricIndicatorModel:MetricIndicatorModel?
	var list = [DashboardDataModel]() {
		didSet {
			self.tableView.reloadData()
            hud.dismiss()
		}
	}
	
	var selectedDate: DateRange! {
		didSet {
                hud.show(in: self.parent?.parent?.view ?? self.view)
            
            presenter?.fetchDailyAvgData(forDate: selectedDate.start)
		}
	}
    
    var currentSleepStartDate: Date?
    var currentSleepEndDate: Date?
	var avgHR = 0
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.registerPresenters()
		setupNewDashborad()
		
		self.selectedDate = DateManager.current.selectedDate
		
		

	}
	
	override func viewWillAppear(_ animated: Bool) {
        if(!self.selectedDate.start.isEqual(to: DateManager.current.selectedDate.start, toGranularity: .day) || (Date().isEqual(to: self.selectedDate.start, toGranularity: .day))){
            self.selectedDate = DateManager.current.selectedDate
        }
		super.viewWillAppear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("updateStepCount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateStepCount), name: NSNotification.Name("updateStepCount"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.calendarDateSelected), name: NSNotification.Name("calendarDateSelected"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateDataAsPerGoalChange), name: NSNotification.Name("updateDataAsPerGoalChange"), object: nil)

	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("updateDataAsPerGoalChange"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("calendarDateSelected"), object: nil)

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("updateStepCount"), object: nil)
		
	}
    
    deinit {
        self.presenter = nil
    }
    //MARK: IBActions
    
    @IBAction func btnLeftIndicationClicked(_ sender: Any) {
        
        switch metricIndicatorModelLeft?.metricType {
        case .breathingRate:
            print("breathingRate")
            let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.currentSleepStartDate = self.currentSleepStartDate
            controller.dataType = .breathingRateReading
            controller.metricModel = metricIndicatorModelLeft
            parent?.navigationController?.pushViewController(controller, animated: true)
        case .sleep:
            print("sleep")
            let controller = SleepStagesDetailsViewController.instantiate(fromAppStoryboard: .sleep)
			controller.avgHeartRate = "\(self.avgHR)"
            controller.metricIndicatorModel = self.sleepMetricIndicatorModel
            parent?.navigationController?.pushViewController(controller, animated: true)
        case .bloodPressure:
            print("systolic")
            let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.currentSleepStartDate = self.currentSleepStartDate
            controller.dataType = .bpReading
            controller.metricModel = metricIndicatorModelLeft

            parent?.navigationController?.pushViewController(controller, animated: true)

        case .oxygen:
            print("oxygen")
            let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.currentSleepStartDate = self.currentSleepStartDate
            controller.dataType = .spO2Reading
            controller.metricModel = metricIndicatorModelLeft

            parent?.navigationController?.pushViewController(controller, animated: true)
        case .temperature:
            let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.currentSleepStartDate = self.currentSleepStartDate
            controller.dataType = .tempratureReading
            controller.metricModel = metricIndicatorModelLeft

            parent?.navigationController?.pushViewController(controller, animated: true)
            print("temperature")

        case .pulseRate:
            let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.currentSleepStartDate = self.currentSleepStartDate
            print("pulseRate")
            controller.dataType = .heartRateReading
            controller.metricModel = metricIndicatorModelLeft

            parent?.navigationController?.pushViewController(controller, animated: true)
        case .hrv:
            print("hrv")

        case .generic:
            print("generic")
            //let controller = SleepViewController.instantiate(fromAppStoryboard: .sleep)
           // self.navigationController?.pushViewController(controller, animated: true)
            let controller = SleepStagesDetailsViewController.instantiate(fromAppStoryboard: .sleep)
            controller.metricIndicatorModel = self.sleepMetricIndicatorModel
			controller.avgHeartRate = "\(self.avgHR)"
            parent?.navigationController?.pushViewController(controller, animated: true)
        default:
            print("default")
        }
    }
    
    @IBAction func btnRightIndicationClicked(_ sender: Any) {
        
        switch metricIndicatorModelRight?.metricType {
        case .breathingRate:
            print("breathingRate")
            let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.currentSleepStartDate = self.currentSleepStartDate
            controller.dataType = .breathingRateReading
            controller.metricModel = metricIndicatorModelRight
            parent?.navigationController?.pushViewController(controller, animated: true)
        case .sleep:
            print("sleep")
            let controller = SleepStagesDetailsViewController.instantiate(fromAppStoryboard: .sleep)
			controller.avgHeartRate = "\(self.avgHR)"
            controller.metricIndicatorModel = self.sleepMetricIndicatorModel
            parent?.navigationController?.pushViewController(controller, animated: true)
        case .bloodPressure:
            print("systolic")
            let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.currentSleepStartDate = self.currentSleepStartDate
            controller.dataType = .bpReading
            controller.metricModel = metricIndicatorModelRight

            parent?.navigationController?.pushViewController(controller, animated: true)
       
        case .oxygen:
            print("oxygen")
            let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.currentSleepStartDate = self.currentSleepStartDate
            controller.dataType = .spO2Reading
            controller.metricModel = metricIndicatorModelRight

            parent?.navigationController?.pushViewController(controller, animated: true)
        case .temperature:
            print("temperature")
            let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.currentSleepStartDate = self.currentSleepStartDate
            controller.dataType = .tempratureReading
            controller.metricModel = metricIndicatorModelRight

            parent?.navigationController?.pushViewController(controller, animated: true)
        case .pulseRate:
            print("pulseRate")
            let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.currentSleepStartDate = self.currentSleepStartDate
            controller.dataType = .heartRateReading
            controller.metricModel = metricIndicatorModelRight

            parent?.navigationController?.pushViewController(controller, animated: true)
        case .hrv:
            print("hrv")

        case .generic:
            print("generic")
            let controller = SleepStagesDetailsViewController.instantiate(fromAppStoryboard: .sleep)
			controller.avgHeartRate = "\(self.avgHR)"
            controller.metricIndicatorModel = self.sleepMetricIndicatorModel
            parent?.navigationController?.pushViewController(controller, animated: true)
        default:
            print("default")
        }
    }
	
	@objc private func updateStepCount() {
		self.selectedDate = DateManager.current.selectedDate
	}
	
	@objc private func updateDataAsPerGoalChange() {
		hud.show(in: self.parent?.parent?.view ?? self.view)
		presenter?.fetchDailyAvgData(forDate: selectedDate.start)
	}
    
}

// MARK: - DashboardViewDelegate
extension DashboardViewController: DashboardViewDelegate {
    func afterDailyAvgDataResponseFetched(result: Result<DailyAveragesDataModel, MovanoError>) {
        switch result {
        case .failure(let error):
            print(error)
			hud.dismiss()
            //error Alert
			let coreDataSleep = CoreDataSleepTime.fetch(forDate: selectedDate.start)
			let sleepStartTime = Double(coreDataSleep?.sleepStartTime ?? "0") ?? 0.0
			let sleepEndTime = Double(coreDataSleep?.sleepEndTime ?? "0") ?? 0.0
			var stepCountRange = selectedDate.intervalRange
			if selectedDate.end > Date() {
				stepCountRange = (selectedDate.start.timeIntervalSince1970 ..< Date().timeIntervalSince1970)
			}
			if sleepStartTime != 0 && sleepEndTime != 0 {
				presenter?.fetchAllDataPoints(forRange: sleepStartTime ..< sleepEndTime,forStepCountRange: stepCountRange)
			} else {
				let tempSleepStartTime = 0.0
				let tempSleepEndTime = 0.0
				presenter?.fetchAllDataPoints(forRange: tempSleepStartTime ..< tempSleepEndTime,forStepCountRange: stepCountRange)
			}
          
        case .success(let dailyAvgDataModel):
            let sleepStartTime = Double(dailyAvgDataModel.sleepStartTime) ?? 0.0
            let sleepEndTime = Double(dailyAvgDataModel.sleepEndTime) ?? 0.0
            self.currentSleepEndDate = Date(timeIntervalSince1970: sleepEndTime)
            self.currentSleepStartDate = Date(timeIntervalSince1970: sleepStartTime)
			self.avgHR = Int((dailyAvgDataModel.averagePulseRate?.average ?? 0.0).rounded())
			var stepCountRange = selectedDate.intervalRange
			if selectedDate.end > Date() {
				stepCountRange = (selectedDate.start.timeIntervalSince1970 ..< Date().timeIntervalSince1970)
			}
			
			if sleepStartTime != 0 && sleepEndTime != 0 {
				presenter?.fetchAllDataPoints(forRange: sleepStartTime ..< sleepEndTime,forStepCountRange: stepCountRange)
			} else {
				let tempSleepStartTime = 0.0
				let tempSleepEndTime = 0.0
				presenter?.fetchAllDataPoints(forRange: tempSleepStartTime ..< tempSleepEndTime,forStepCountRange: stepCountRange)
			}
        }

    }
    
    func afertAllPointsDataFetched(result: Result<HealthScoreModel, MovanoError>){
       
        switch result {
        case .failure(let error):
            print(error)
            hud.dismiss()
            //error Alert
            
        case .success(let healthScoreModel):
            leftIndicatorView.isHidden = false
            rightIndicatorView.isHidden = true
                lblHealthScore.text = healthScoreModel.healthScore
                metricIndicatorModelLeft = healthScoreModel.metricIndicatorsList[0]
            if(metricIndicatorModelLeft?.metricType == .sleep){
                sleepMetricIndicatorModel = metricIndicatorModelLeft
            }
            if(!(metricIndicatorModelLeft?.metricType == .allGood || metricIndicatorModelLeft?.metricType == .noData || metricIndicatorModelLeft?.metricType == .noSufficientData)){
                leftIndicatorButton.setImage(UIImage(named: "rightarrow"), for: .normal)
            }else{
                leftIndicatorButton.setImage(UIImage(), for: .normal)
            }
                leftIndicatorLable.text = healthScoreModel.metricIndicatorsList[0].metricType.rawValue
			leftIndicaotrImageView.image = UIImage(named: healthScoreModel.metricIndicatorsList[0].indicatorImage.rawValue)
                if(healthScoreModel.metricIndicatorsList.count > 1){
                    rightIndicatorView.isHidden = false
                    metricIndicatorModelRight = healthScoreModel.metricIndicatorsList[1]
                    if(metricIndicatorModelRight?.metricType == .sleep){
                        sleepMetricIndicatorModel = metricIndicatorModelRight
                    }
                    rightIndicatorButton.setImage(UIImage(named: "rightarrow"), for: .normal)
                    rightIndicatorLable.text = healthScoreModel.metricIndicatorsList[1].metricType.rawValue
					rightIndicatorImageView.image = UIImage(named: healthScoreModel.metricIndicatorsList[1].indicatorImage.rawValue)
                }
            list.removeAll()
            list = healthScoreModel.dashBoardDataModelList
        }


    }
}

// MARK: - UITableView Delegate Callbacks
extension DashboardViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return list.count
	}
		
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 120
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = DashboardTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
        cell.contentView.layer.cornerRadius = 5
        cell.contentView.layer.masksToBounds = true
		let item = self.list[indexPath.item]
		
		cell.titleLabel.text = item.parameter.name
		cell.iconImageView.image = item.image
		
		var measureString = ""
		if let reading = item.parameter.reading {
			measureString += reading
		} else {
			measureString = "-"
		}
		
		if let unit = item.parameter.unit {
            cell.unitLabel.text = unit
        }else{
            cell.unitLabel.text = "-"
        }
        if(item.isGoalAchieved){
            cell.badgeImage.isHidden = false
        }else{
            cell.badgeImage.isHidden = true
        }
		cell.measureLabel.text = measureString
        for view in cell.sldierContainerView.subviews{
            view.removeFromSuperview()
        }
        if(item.metricType == .nocturnalBP){
            if let sliderVWParam = item.bpSliderViewParam{
                let childView = UIHostingController(rootView: CustomBPSliderView(sliderViewParam: sliderVWParam))
                childView.view.frame = cell.sldierContainerView.bounds
                childView.view.backgroundColor = UIColor.clear
                cell.sldierContainerView.addSubview(childView.view)
            }
        }else{
            if let sliderVWParam = item.sliderViewParam{
                let childView = UIHostingController(rootView: CustomSliderView(sliderViewParam: sliderVWParam))
                childView.view.frame = cell.sldierContainerView.bounds
                childView.view.backgroundColor = UIColor.clear
                cell.sldierContainerView.addSubview(childView.view)
            }
        }
        
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch list[indexPath.row].metricType {
        case .nocturnalBP:
            let controller = SleepHistoryDataViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.currentSleepStartDate = self.currentSleepStartDate
			if metricIndicatorModelLeft?.metricType == .bloodPressure {
				controller.metricModel = metricIndicatorModelLeft
			}
						if metricIndicatorModelRight?.metricType == .bloodPressure {
				controller.metricModel = metricIndicatorModelRight
			}
            controller.dataType = .bpReading
            parent?.navigationController?.pushViewController(controller, animated: true)
        case .sleep:
            let controller = SleepStagesDetailsViewController.instantiate(fromAppStoryboard: .sleep)
            controller.currentSleepEndDate = self.currentSleepEndDate
            controller.currentSleepStartDate = self.currentSleepStartDate
			controller.avgHeartRate = "\(self.avgHR)"
            parent?.navigationController?.pushViewController(controller, animated: true)
        case .steps:
            let controller = ActivityDetailViewController.instantiate(fromAppStoryboard: .activity)
            controller.dataType = .stepCount
            controller.elementColor = .barPurple
			controller.isCameFromDashboard = true
            parent?.navigationController?.pushViewController(controller, animated: true)
        default:
            print("Default selected")
        }
	}
    
}

// MARK: - Calendar Callbacks
extension DashboardViewController {
	@objc private func calendarDateSelected(notification: NSNotification) {
		guard let object = notification.object as? DateRange else {
			return
		}
		if(object.type == .daily){
			self.selectedDate = object
		}
	}
}

// MARK: - New dashboard design logic
extension DashboardViewController {
	
    func setupNewDashborad() {
        setupEmptyMetrics()
        setupTableViewDelegate()
        registerNibs()
    }

    func setupEmptyMetrics(){
        let bpDashboardDataModel:DashboardDataModel = DashboardDataModel(parameter: Parameter(name: "Nocturnal Blood Pressure"), image: #imageLiteral(resourceName: "BP"))
        let sleepDashboardDataModel:DashboardDataModel = DashboardDataModel(parameter: Parameter(name: "Sleep"), image: #imageLiteral(resourceName: "Sleep"))
        let stepsDashboardDataModel:DashboardDataModel =  DashboardDataModel(parameter: Parameter(name: "Steps"), image: #imageLiteral(resourceName: "Steps"))
        list = [bpDashboardDataModel,sleepDashboardDataModel,stepsDashboardDataModel]
    }
    
    func setupTableViewDelegate() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func registerNibs() {
        let dashboradCellNib = UINib(nibName: "DashboardTableViewCell", bundle: nil)
		tableView.register(dashboradCellNib, forCellReuseIdentifier: DashboardTableViewCell.IDENTIFIER)
    }
	
	func registerPresenters() {
		if presenter == nil {
			let dashboradDataService = DashboardDataWebService()
            let breathingRateService = BreathingRateWebService()
            let pulseRateService = PulseRateWebService()
            let temperatureService = SkinTemperatureWebService()
            let bpService = BloodPressureWebService()
            let sleepService = SleepWebService()
            let stepCountService = StepCountWebService()
            let oxygenService = OxygenDataWebService()
            presenter = DashboardPresenter(breathingRateService: breathingRateService, bloodPressureService: bpService, pulseRateService: pulseRateService, temperatureRateService: temperatureService, sleepService: sleepService, stepCountService: stepCountService, oxygenService: oxygenService, dashboardDataService: dashboradDataService, delegate: self)
		}
	}
}
