//
//  ActionSheetOverlayView.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 06/08/21.
//

import UIKit

struct ActionSheetOption {
		
	var title: ActionSheetType
	var image: UIImage?
}

enum ActionSheetType: String {
	case takeAPause = "Take A Pause"
	case startExtercise = "Start Exercise"
	case accelerometer = "Accelerometer Reading"
    case debug = "Debug Movano Ring"
	
}

protocol ActionSheetDelegate {
	func actionSheetItemSelected(_ item: ActionSheetOption, for viewController: ActionSheetOverlayView)
}

class ActionSheetOverlayView: UIViewController {
	
	var hasSetPointOrigin = false
	var pointOrigin: CGPoint?
	
	@IBOutlet weak var slideIdicator: UIView!
	@IBOutlet weak var tableView: UITableView!
	
	var delegate: ActionSheetDelegate?
	
	var optionList = [ActionSheetOption]() {
		didSet {
			self.tableView.reloadData()
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.registerCellToListView()
		self.assignDelegate()
		
		let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
		view.addGestureRecognizer(panGesture)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.optionList = //[ActionSheetOption(title: .takeAPause, image: #imageLiteral(resourceName: "take-a-pause")),
        [ActionSheetOption(title: .startExtercise, image: #imageLiteral(resourceName: "start-exercise")),ActionSheetOption(title: .debug, image: nil)]
						   //ActionSheetOption(title: .accelerometer, image: nil)]
	}
	
	override func viewDidLayoutSubviews() {
		if !hasSetPointOrigin {
			hasSetPointOrigin = true
			pointOrigin = self.view.frame.origin
		}
	}
	
	func registerCellToListView() {
		let profileNib = UINib.init(nibName: "DropDownSelectionTableViewCell", bundle: nil)
		self.tableView.register(profileNib, forCellReuseIdentifier: DropDownSelectionTableViewCell.IDENTIFIER)
	}
	
	func assignDelegate() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
	}
	
	@objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
		let translation = sender.translation(in: view)
		
		// Not allowing the user to drag the view upward
		guard translation.y >= 0 else { return }
		
		// setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
		view.frame.origin = CGPoint(x: 0, y: self.pointOrigin!.y + translation.y)
		
		if sender.state == .ended {
			let dragVelocity = sender.velocity(in: view)
			if dragVelocity.y >= 1300 {
				self.dismiss(animated: true, completion: nil)
			} else {
				// Set back to original position of the view controller
				UIView.animate(withDuration: 0.3) {
					self.view.frame.origin = self.pointOrigin ?? CGPoint(x: 0, y: 400)
				}
			}
		}
	}
}

extension ActionSheetOverlayView: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let item = self.optionList[indexPath.row]
		self.delegate?.actionSheetItemSelected(item, for: self)
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 84
	}
}

extension ActionSheetOverlayView: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return optionList.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = DropDownSelectionTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
		let item = optionList[indexPath.row]
		
		cell.imageIcon.image = item.image
		cell.titleLabel.text = item.title.rawValue
		
		return cell
	}
}
