//
//  StartExerciseViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 23/06/21.
//

import UIKit
import NotificationBannerSwift

// swiftlint:disable all
class StartExerciseViewController: UIViewController {
    @IBOutlet weak var titleLable: MovanoLabel!
    
    var action : ActionSheetType = .startExtercise
	var timer = Timer()
	let dataDirectoryPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)  // The top level directory.
	
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLable.text = action.rawValue
        // Do any additional setup after loading the view.
		
		scanForBLEDevice()
    }
    
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		NotificationCenter.default.addObserver(self, selector: #selector(self.deviceDidConnect), name: NSNotification.Name("deviceDidConnect"), object: nil)
		
//		if KeyChain.shared.firstName != "" {
//			let controller = UserViewController.instantiate(fromAppStoryboard: .bluetooth)
//			self.navigationController?.pushViewController(controller, animated: false)
//		}
		
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		NotificationCenter.default.removeObserver(self, name: Notification.Name("deviceDidConnect"), object: nil)
	}
	
	@IBAction func backButtonClicked(_ sender: MovanoButton) {
		self.navigationController?.popViewController(animated: true)
	}

	@IBAction func startButtonClicked(_ sender: Any) {
		if bluetoothManager.connectedPeripheral != nil {
			// Cleared all logs
			Logger.shared.messages = []
			UserDefault.shared.logs = Logger()
			
			let controller = DebugMovanoRingViewController.instantiate(fromAppStoryboard: .debug)
			self.navigationController?.pushViewController(controller, animated: false)
		} else {
			self.pairDeviceError()
		}
		
	}
	
	func pairDeviceError() {
		if UserDefault.shared.connectedDevice != nil{
			self.showMyDeviceScreenPopup()
		}else{
			self.showDevicePopup()
		}
	}
	
	/**
	 Create a date and time string of the form "yyyy-MM-dd'T'HHmmss".
	 */
	func timeStamp() -> String {
		let date = Date()
		let format = DateFormatter()
		format.dateFormat = "yyyy-MM-dd'T'HHmmss"
		let timestamp = format.string(from: date)
		return "\(timestamp)"
	}
	
	@objc private func deviceDidConnect(notification: NSNotification) {
		guard let object = notification.object as? PeripheralDevice else {
			return
		}
		
		let banner = FloatingNotificationBanner(title: "Device Connected!", subtitle: "Connected to " + object.detail.name + ".", titleFont: UIFont.gilroy.bold(withSize: 15), titleColor: .backgroundColor, titleTextAlign: .left, subtitleFont: UIFont.gilroy.regular(withSize: 14), subtitleColor: .backgroundColor, subtitleTextAlign: .left, leftView: nil, rightView: nil, style: .success, colors: nil, iconPosition: .top)
		banner.backgroundColor = UIColor.textColor
		banner.show()
	}
	
	@objc func scanForBLEDevice(){
		if UserDefault.shared.connectedDevice != nil {
			//bluetoothManager.autoConnect = true
			Logger.shared.addLog("Found BLE device info in user defaults : \(String(describing: UserDefault.shared.connectedDevice))")
			self.timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: {[weak self] _ in
				self?.checkAndConnect()
			   })
		}
	}
	
	func checkAndConnect(){
		if(bluetoothManager.explicitDisconnection == false){
			if(bluetoothManager.isBLEConnected == false){
				bluetoothManager.startScanning()
			}
		}
	}
	
	func stopTimer() {
		timer.invalidate()
	}
	
	deinit {
		stopTimer()
	}
}
