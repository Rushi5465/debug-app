//
//  StartBloodPressureViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/06/21.
//

import UIKit

class StartBloodPressureViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
	
	@IBAction func backButtonClicked(_ sender: MovanoButton) {
		self.navigationController?.popViewController(animated: true)
	}

	@IBAction func startButtonClicked(_ sender: Any) {
		let controller = StopBloodPressureViewController.instantiate(fromAppStoryboard: .dashboard)
		self.navigationController?.pushViewController(controller, animated: false)
	}

}
