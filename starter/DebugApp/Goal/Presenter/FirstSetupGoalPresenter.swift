//
//  FirstSetupGoalPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 20/09/21.
//

import Foundation

class FirstSetupGoalPresenter: FirstSetupGoalPresenterProtocol {
	
	private weak var delegate: FirstSetupGoalViewDelegate?
	private var goalService: UserGoalsWebServiceProtocol
	
	required init(goalService: UserGoalsWebServiceProtocol, delegate: FirstSetupGoalViewDelegate) {
		self.goalService = goalService
		self.delegate = delegate
	}
	
	func fetchGoal() {
		let goal = UserDefault.shared.goals
		self.delegate?.fetchGoals(goals: [goal.systolic, goal.diastolic, goal.pulseRate, goal.breathingRate])
		
		self.goalService.fetch { goal, error in
			if let goal = goal {
				self.delegate?.fetchGoals(goals: [goal.systolic, goal.diastolic, goal.pulseRate, goal.breathingRate])
			}
		}
	}
}
