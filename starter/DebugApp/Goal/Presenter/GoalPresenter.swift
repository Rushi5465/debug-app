//
//  GoalPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/09/21.
//

import Foundation

class GoalPresenter: GoalPresenterProtocol {
	
	private weak var delegate: GoalViewDelegate?
	private var goalService: UserGoalsWebServiceProtocol
	
	required init(goalService: UserGoalsWebServiceProtocol, delegate: GoalViewDelegate) {
		self.goalService = goalService
		self.delegate = delegate
	}
	
	func fetchGoal() {
		self.delegate?.fetchGoals(goals: UserDefault.shared.goals.all)
		
		self.goalService.fetch { goal, error in
			if let goal = goal {
				self.delegate?.fetchGoals(goals: goal.all)
			}
		}
	}
	
	func updateGoal(model: Goal) {
		self.goalService.update(model: model) { goal, error in
			if let error = error {
				self.delegate?.updateGoal(result: .failure(error))
				return
			}
			if let goal = goal {
				UserDefault.shared.goals = goal
				self.delegate?.updateGoal(result: .success(goal))
			}
		}
	}
	
	func addGoal(model: Goal) {
		self.goalService.add(model: model) { goal, error in
			if let error = error {
				self.delegate?.updateGoal(result: .failure(error))
				return
			}
			if let goal = goal {
				UserDefault.shared.goals = goal
				self.delegate?.updateGoal(result: .success(goal))
			}
		}
	}
}
