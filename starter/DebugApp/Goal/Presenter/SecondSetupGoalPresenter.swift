//
//  SecondSetupGoalPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/09/21.
//

import Foundation

class SecondSetupGoalPresenter: SecondSetupGoalPresenterProtocol {
	
	private weak var delegate: SecondSetupGoalViewDelegate?
	private var goalService: UserGoalsWebServiceProtocol
	
	required init(goalService: UserGoalsWebServiceProtocol, delegate: SecondSetupGoalViewDelegate) {
		self.goalService = goalService
		self.delegate = delegate
	}
	
	func fetchGoal() {
		let goal = UserDefault.shared.goals
		self.delegate?.fetchGoals(goals: [goal.oxygen, goal.stepCountGoal, goal.caloriesBurntGoal, goal.sleepHour])
		
		self.goalService.fetch { goal, error in
			if let goal = goal {
				self.delegate?.fetchGoals(goals: [goal.oxygen, goal.stepCountGoal, goal.caloriesBurntGoal, goal.sleepHour])
			}
		}
	}
	
	func addGoal(model: Goal) {
		self.goalService.add(model: model) { goal, error in
			if let error = error {
				self.delegate?.addGoal(result: .failure(error))
				return
			}
			if let goal = goal {
				UserDefault.shared.goals = goal
				self.delegate?.addGoal(result: .success(goal))
			}
		}
	}
}
