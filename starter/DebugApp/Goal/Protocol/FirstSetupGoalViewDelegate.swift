//
//  FirstSetupGoalViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 20/09/21.
//

import Foundation

protocol FirstSetupGoalViewDelegate: AnyObject {
	
	func fetchGoals(goals: [UserGoal])
}
