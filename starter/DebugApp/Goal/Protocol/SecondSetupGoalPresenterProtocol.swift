//
//  SecondSetupGoalPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/09/21.
//

import Foundation

protocol SecondSetupGoalPresenterProtocol {
	
	init(goalService: UserGoalsWebServiceProtocol, delegate: SecondSetupGoalViewDelegate)

	func fetchGoal()
	func addGoal(model: Goal)
}
