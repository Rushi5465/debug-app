//
//  GoalPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/09/21.
//

import Foundation

protocol GoalPresenterProtocol {
	
	init(goalService: UserGoalsWebServiceProtocol, delegate: GoalViewDelegate)

	func fetchGoal()
	func updateGoal(model: Goal)
}
