//
//  GoalViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/09/21.
//

import Foundation

protocol GoalViewDelegate: AnyObject {
	
	func fetchGoals(goals: [UserGoal])
	func updateGoal(result: Result<Goal, Error>)
}
