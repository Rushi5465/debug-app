//
//  SecondSetupGoalViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 21/09/21.
//

import Foundation

protocol SecondSetupGoalViewDelegate: AnyObject {
	
	func fetchGoals(goals: [UserGoal])
	func addGoal(result: Result<Goal, Error>)
}
