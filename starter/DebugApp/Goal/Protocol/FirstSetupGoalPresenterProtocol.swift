//
//  FirstSetupGoalPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 20/09/21.
//

import Foundation

protocol FirstSetupGoalPresenterProtocol {
	
	init(goalService: UserGoalsWebServiceProtocol, delegate: FirstSetupGoalViewDelegate)

	func fetchGoal()
}
