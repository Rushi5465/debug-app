//
//  GoalTableViewCell.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 20/09/21.
//

import UIKit

class GoalTableViewCell: UITableViewCell {
	
	@IBOutlet weak var titleLabel: MovanoLabel!
	@IBOutlet weak var slider: UISlider!
	
	@IBOutlet weak var label1: MovanoLabel!
	@IBOutlet weak var label2: MovanoLabel!
	@IBOutlet weak var label3: MovanoLabel!
	@IBOutlet weak var label4: MovanoLabel!
	@IBOutlet weak var label5: MovanoLabel!
	
	var delegate: GoalDelegate?
	var goal: UserGoal!
	
	func loadView(goal: UserGoal) {
		self.goal = goal
		self.titleLabel.text = goal.type.rawValue
		
		self.slider.minimumValue = Float(goal.minimum)
		self.slider.maximumValue = Float(goal.maximum)
		if let value = goal.value as? Int {
			self.slider.value = Float(value)
		}
		
		self.label1.text = "\(goal.minimum)"
		self.label2.text = "\(goal.minimum + ((goal.maximum - goal.minimum) * 1 / 4))"
		self.label3.text = "\(goal.minimum + ((goal.maximum - goal.minimum) * 2 / 4))"
		self.label4.text = "\(goal.minimum + ((goal.maximum - goal.minimum) * 3 / 4))"
		self.label5.text = "\(goal.maximum)"
	}
	
	@IBAction func rangeSlideValueChanged(_ sender: UISlider) {
		self.goal.value = Int(sender.value)
		self.delegate?.goalView(newValue: self.goal)
	}
	
}
