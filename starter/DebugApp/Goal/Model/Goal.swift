//
//  Goal.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 14/09/21.
//

import Foundation

// swiftlint:disable all
struct UserGoal {
	var value: Any
	let minimum: Int
	let maximum: Int
	let type: GoalType
	
	var intValue: Int {
		guard let thisValue = value as? Int else {
			return 0
		}
		return thisValue
	}
}

enum GoalType: String {
	case systolic = "Systolic Blood Pressure"
	case diastolic = "Diastolic Blood Pressure"
	case temperature = "Skin Temperature"
	case breathingRate = "Breathing Rate"
	case pulseRate = "Heart Rate"
	case oxygen = "SpO2"
	case sleep = "Sleep Hours"
	case stepCount = "Step Count"
	case caloriesBurned = "Calories Burned"
}

struct Goal: Codable {
	var userID: String
	private var stepCount: Int
	private var caloriesBurned: Int
	private var systolicLow: Int
	private var systolicHigh: Int
	private var diastolicLow: Int
	private var diastolicHigh: Int
	private var temperatureLow: Int
	private var temperatureHigh: Int
	private var breathingRateLow: Int
	private var breathingRateHigh: Int
	private var pulseRateLow: Int
	private var pulseRateHigh: Int
	private var oxygenLow: Int
	private var oxygenHigh: Int
	private var sleepHrsLow: Int
	private var sleepHrsHigh: Int
	
	init(userID: String = "", stepCount: Int = 5000, caloriesBurned: Int = 500, systolicLow: Int = 100, systolicHigh: Int = 140, diastolicLow: Int = 60, diastolicHigh: Int = 100, temperatureLow: Int = 94, temperatureHigh: Int = 98, breathingRateLow: Int = 12, breathingRateHigh: Int = 20, pulseRateLow: Int = 60, pulseRateHigh: Int = 100, oxygenLow: Int = 94, oxygenHigh: Int = 100, sleepHrsLow: Int = 5, sleepHrsHigh: Int = 9) {
		self.userID = userID
		self.stepCount = stepCount
		self.caloriesBurned = caloriesBurned
		self.systolicLow = systolicLow
		self.systolicHigh = systolicHigh
		self.diastolicLow = diastolicLow
		self.diastolicHigh = diastolicHigh
		self.temperatureLow = temperatureLow
		self.temperatureHigh = temperatureHigh
		self.breathingRateLow = breathingRateLow
		self.breathingRateHigh = breathingRateHigh
		self.pulseRateLow = pulseRateLow
		self.pulseRateHigh = pulseRateHigh
		self.oxygenLow = oxygenLow
		self.oxygenHigh = oxygenHigh
		self.sleepHrsLow = sleepHrsLow
		self.sleepHrsHigh = sleepHrsHigh
	}
	
	var systolic: UserGoal {
		return UserGoal(value: systolicLow ..< systolicHigh, minimum: 80, maximum: 160, type: .systolic)
	}
	
	var diastolic: UserGoal {
		return UserGoal(value: diastolicLow ..< diastolicHigh, minimum: 40, maximum: 120, type: .diastolic)
	}
	
	var temperature: UserGoal {
		return UserGoal(value: temperatureLow ..< temperatureHigh, minimum: 94, maximum: 98, type: .temperature)
	}
	
	var breathingRate: UserGoal {
		return UserGoal(value: breathingRateLow ..< breathingRateHigh, minimum: 8, maximum: 24, type: .breathingRate)
	}
	
	var pulseRate: UserGoal {
		return UserGoal(value: pulseRateLow ..< pulseRateHigh, minimum: 40, maximum: 120, type: .pulseRate)
	}
	
	var oxygen: UserGoal {
		return UserGoal(value: oxygenLow ..< oxygenHigh, minimum: 80, maximum: 100, type: .oxygen)
	}
	
	var sleepHour: UserGoal {
		return UserGoal(value: sleepHrsLow ..< sleepHrsHigh, minimum: 4, maximum: 12, type: .sleep)
	}
	
	var stepCountGoal: UserGoal {
		return UserGoal(value: stepCount, minimum: 0, maximum: 10000, type: .stepCount)
	}
	
	var caloriesBurntGoal: UserGoal {
		return UserGoal(value: caloriesBurned, minimum: 0, maximum: 1000, type: .caloriesBurned)
	}
	
	var all: [UserGoal] {
		return [systolic, diastolic, pulseRate, temperature, breathingRate, oxygen, sleepHour, stepCountGoal, caloriesBurntGoal]
	}
	
	var createGoalsInput: CreateUserGoalsInput {
		return CreateUserGoalsInput(stepCount: stepCount, caloriesBurned: caloriesBurned, systolicLow: systolicLow, systolicHigh: systolicHigh, diastolicLow: diastolicLow, diastolicHigh: diastolicHigh, temperatureLow: temperatureLow, temperatureHigh: temperatureHigh, breathingRateLow: breathingRateLow, breathingRateHigh: breathingRateHigh, pulseRateLow: pulseRateLow, pulseRateHigh: pulseRateHigh, spO2Low: oxygenLow, spO2High: oxygenHigh, sleepHrsLow: sleepHrsLow, sleepHrsHigh: sleepHrsHigh)
	}
	
	var updateGoalsInput: UpdateUserGoalsInput {
		return UpdateUserGoalsInput(stepCount: stepCount, caloriesBurned: caloriesBurned, systolicLow: systolicLow, systolicHigh: systolicHigh, diastolicLow: diastolicLow, diastolicHigh: diastolicHigh, temperatureLow: temperatureLow, temperatureHigh: temperatureHigh, breathingRateLow: breathingRateLow, breathingRateHigh: breathingRateHigh, pulseRateLow: pulseRateLow, pulseRateHigh: pulseRateHigh, spO2Low: oxygenLow, spO2High: oxygenHigh, sleepHrsLow: sleepHrsLow, sleepHrsHigh: sleepHrsHigh)
	}
	
	mutating func updateValue(userGoal: UserGoal) {
		var lowerValue = Int()
		var upperValue = Int()
		if let value = userGoal.value as? Range<Int> {
			lowerValue = value.lowerBound
			upperValue = value.upperBound
		} else if let value = userGoal.value as? Int {
			lowerValue = value
			upperValue = 0
		}
		
		if userGoal.type == .systolic {
			self.systolicLow = lowerValue
			self.systolicHigh = upperValue
		} else if userGoal.type == .diastolic {
			self.diastolicLow = lowerValue
			self.diastolicHigh = upperValue
		} else if userGoal.type == .pulseRate {
			self.pulseRateLow = lowerValue
			self.pulseRateHigh = upperValue
		} else if userGoal.type == .temperature {
			self.temperatureLow = lowerValue
			self.temperatureHigh = upperValue
		} else if userGoal.type == .oxygen {
			self.oxygenLow = lowerValue
			self.oxygenHigh = upperValue
		} else if userGoal.type == .breathingRate {
			self.breathingRateLow = lowerValue
			self.breathingRateHigh = upperValue
		} else if userGoal.type == .sleep {
			self.sleepHrsLow = lowerValue
			self.sleepHrsHigh = upperValue
		} else if userGoal.type == .stepCount {
			self.stepCount = lowerValue
		} else if userGoal.type == .caloriesBurned {
			self.caloriesBurned = lowerValue
		}
	}
}
