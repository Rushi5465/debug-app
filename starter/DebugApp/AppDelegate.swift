//
//  AppDelegate.swift
//  starter
//
//  Created by Sagar Varsani on 4/24/19.
//  Copyright © 2019 Indexnine Technologies. All rights reserved.
//

import UIKit
import JGProgressHUD
import Apollo
import CoreData

let hud = JGProgressHUD(style: .dark)
let bluetoothManager = BluetoothManager.shared

// swiftlint:disable all
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
	var navController: UINavigationController?
	var blurEffectView: UIView?
	var alert: UIAlertController?
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		
		if KeyChain.shared.accessToken != nil {
			KeyChain.shared.firstOpen = true
		}
		clearDataForReInstalls()
		UINavigationBar.appearance().setUp()
		hud.position = .center
		hud.textLabel.text = "Loading"
		KeyChain.shared.isFaceID = BiometricAuthentication.shared.isFaceIDSupported
		KeyChain.shared.isTouchID = BiometricAuthentication.shared.isTouchIDSupported
		NavigationUtility.setInitialController()
		checkForAuthenticationSupport()
		
		
		// Check for existing user
		if KeyChain.shared.firstName != "" {
			let yesAction = UIAlertAction(title: "Yes", style: .default, handler: nil)
			let noAction = UIAlertAction(title: "No", style: .default) { UIAlertAction in
//				KeyChain.shared.clearTokens()
//				UserDefault.shared.deleteAllUserDefault()
				NotificationCenter.default.post(name: NSNotification.Name("showPageWithoutUser"), object: nil)
			}
			let alertController = UIAlertController(title: "Alert", message: "Do you want to continue with existing user?", preferredStyle: .actionSheet)
			alertController.addAction(yesAction)
			alertController.addAction(noAction)
			self.window?.makeKeyAndVisible()
			self.window?.rootViewController?.present(alertController, animated: true, completion: nil)

		}
		
		
		// Tab bar font change
		UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.gilroy.regular(withSize: 12)], for: UIControl.State.normal)
		UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.gilroy.regular(withSize: 12)], for: UIControl.State.selected)
		return true
	}
	
	var orientationLock: UIInterfaceOrientationMask = [.portrait]
	
	func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
		return self.orientationLock
	}
	
	func applicationWillEnterForeground(_ application: UIApplication) {
		checkForAuthenticationSupport()
	}
	
	func applicationDidEnterBackground(_ application: UIApplication) {
		if let navController = navController, let blurEffectView = blurEffectView {
			navController.view.removeView(specificView: blurEffectView)
		}
		
		if let alert = alert {
			alert.dismiss(animated: false, completion: nil)
		}
	}
	
	func applicationWillTerminate(_ application: UIApplication) {
		bluetoothManager.updateNotificationStateToFalse(uuid: TransferService.dateServiceUUID)
		if ServerManager.shared.automaticallyManagedServer.type.rawValue != ServerManager.shared.currentServer.type.rawValue {
			KeyChain.shared.clearTokens()
			KeyChain.shared.userState =  NavigationUtility.UserState.logout.rawValue
			KeyChain.shared.firstName = ""
			KeyChain.shared.lastName = ""
			KeyChain.shared.email = ""
		}
		Logger.shared.addLog("END OF SESSION")
	}
	
	func clearDataForReInstalls() {
		let currentVersion: String = Bundle.main.releaseVersionNumber
		let currentBuildVersion: String = Bundle.main.buildVersionNumber
		
		let versionOfLastRun: String? = UserDefaults.standard.object(forKey: "VersionOfLastRun") as? String
		let buildVersion: String? = UserDefaults.standard.object(forKey: "BuildVersion") as? String
		if versionOfLastRun == nil || versionOfLastRun != currentVersion {
			KeyChain.shared.clear()
		} else {
			if buildVersion == nil || buildVersion != currentBuildVersion {
				KeyChain.shared.clear()
			}
		}
		UserDefaults.standard.set(currentVersion, forKey: "VersionOfLastRun")
		UserDefaults.standard.set(currentBuildVersion, forKey: "BuildVersion")
		UserDefaults.standard.synchronize()
	}
	
	func checkForAuthenticationSupport() {
		if KeyChain.shared.accessToken != nil {
			authenticateUser()
		}
	}
	
	func performBiometricLogin() {
		let faceIdAccounts = KeyChain.shared.userAccounts.filter { (account) -> Bool in
			return account.isSecurityEnabled
		}
		
		let faceIdAccount = faceIdAccounts.first
		if (faceIdAccount != nil) && KeyChain.shared.accessToken == nil {
			if let navController = self.window!.rootViewController as? UINavigationController {
				BiometricAuthentication.shared.loginUserWithBiometric(user: faceIdAccount!, currentView: navController.view) { (_, error) in
					if let movanoError = error as? MovanoError {
						navController.showAlert(title: "Movano", message: movanoError.errorMessage)
					}
				}
			}
		}
	}
	
	func authenticateUser() {
		if (KeyChain.shared.currentUser?.isSecurityEnabled ?? false) && KeyChain.shared.accessToken != nil {
			if let navController = self.window!.rootViewController as? UINavigationController {
				
				let blurEffect = UIBlurEffect(style: .light)
				let blurEffectView = UIVisualEffectView(effect: blurEffect)
				blurEffectView.frame = navController.view.bounds
				navController.view.addSubview(blurEffectView)
				navController.view.bringSubviewToFront(blurEffectView)
				checkAuth(navController: navController, blurEffectView: blurEffectView)
				self.navController = navController
				self.blurEffectView = blurEffectView
			}
		}
	}
	
	func checkAuth(navController: UINavigationController, blurEffectView: UIView) {
		BiometricAuthentication.shared.authenticateUser { (status, error) in
			if let movanoError = error {
				var actions: [UIAlertAction]?
				if movanoError == .userCancel {
					actions = [UIAlertAction]()
					var message = "Unlock using "
					if BiometricAuthentication.shared.isFaceIDSupported {
						message.append("Face ID")
					} else if BiometricAuthentication.shared.isTouchIDSupported {
						message.append("Touch ID")
					}
					actions?.append(UIAlertAction(title: message, style: .default, handler: { (_) in
						self.checkAuth(navController: navController, blurEffectView: blurEffectView)
					}))
					self.alert = navController.showAuthAlert(title: "", message: movanoError.message, actions: actions)
				}
			}
			
			if status {
				navController.view.removeView(specificView: blurEffectView)
			}
		}
	}
	
	// MARK: - Core Data stack
	
	lazy var persistentContainer: NSPersistentContainer = {
		/*
		 The persistent container for the application. This implementation
		 creates and returns a container, having loaded the store for the
		 application to it. This property is optional since there are legitimate
		 error conditions that could cause the creation of the store to fail.
		 */
		let container = NSPersistentContainer(name: "BloodPressureApp")
		container.loadPersistentStores(completionHandler: { (storeDescription, error) in
			if let error = error as NSError? {
				// Replace this implementation with code to handle the error appropriately.
				// fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
				
				/*
				 Typical reasons for an error here include:
				 * The parent directory does not exist, cannot be created, or disallows writing.
				 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
				 * The device is out of space.
				 * The store could not be migrated to the current model version.
				 Check the error message to determine what the actual problem was.
				 */
				fatalError("Unresolved error \(error), \(error.userInfo)")
			}
		})
		return container
	}()
	
	// MARK: - Core Data Saving support
	
	lazy var managedObjectContext: NSManagedObjectContext? = {
		let coordinator = self.persistentStoreCoordinator
		if coordinator == nil {
			return nil
		}
		var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
		managedObjectContext.persistentStoreCoordinator = coordinator
		return managedObjectContext
	}()
	
	lazy var managedObjectModel: NSManagedObjectModel = {
		let modelURL = Bundle.main.url(forResource: "BloodPressureApp", withExtension: "momd")!
		return NSManagedObjectModel(contentsOf: modelURL)!
	}()
	
	lazy var applicationDocumentsDirectory: URL = {
		let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
		return urls[urls.count-1]
	}()
	
	lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
		var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
		let url = self.applicationDocumentsDirectory.appendingPathComponent("BloodPressureApp.sqlite")
		
		var error: NSError? = nil
		var failureReason = "There was an error creating or loading the application's saved data."
		do {
			try coordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
		} catch var error1 as NSError {
			error = error1
			coordinator = nil
			// Report any error we got.
			var dict = [String: AnyObject]()
			dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
			dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
			dict[NSUnderlyingErrorKey] = error
			error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
			
			NSLog("Unresolved error \(String(describing: error)), \(error!.userInfo)")
			
			let actions = [UIAlertAction(title: "Ok", style: .default, handler: { (_) in
				abort()
			})]
			self.window?.rootViewController?.showAlert(title: "Alert", message: "Application will be forcefully terminated. Please uninstall the current version and install the updated version.", actions: actions)
		} catch {
			fatalError()
		}
		
		return coordinator
	}()
	
	func saveContext () {
		let context = persistentContainer.viewContext
		if context.hasChanges {
			do {
				try context.save()
			} catch {
				let nserror = error as NSError
				fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
			}
		}
	}
}
