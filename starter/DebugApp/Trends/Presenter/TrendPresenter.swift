//
//  TrendPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 23/09/21.
//

import Foundation
import Highcharts
import UIKit

// swiftlint:disable all
class TrendPresenter: TrendPresenterProtocol {
	
	private weak var delegate: TrendViewDelegate?
	private var breathingRateService: BreathingRateWebServiceProtocol
	private var bloodPressureService: BloodPressureWebServiceProtocol
	private var pulseRateService: PulseRateWebServiceProtocol
	private var temperatureRateService: SkinTemperatureWebServiceProtocol
	private var sleepService: SleepWebServiceProtocol
	private var stepCountService: StepCountWebServiceProtocol
	private var hrvService: HRVWebServiceProtocol
	
	var trendList = [TrendModel]() {
		didSet {
			self.delegate?.fetchTrendList(list: trendList)
		}
	}
	
	private var isSleepData = Bool()
	
	private var sleepTrendList = [TrendModel]() {
		didSet {
			if isSleepData {
				self.trendList = sleepTrendList
			}
		}
	}
	
	private var activityTrendList = [TrendModel]() {
		didSet {
			if !isSleepData {
				self.trendList = activityTrendList
			}
		}
	}
	
	private weak var chartDelegate: ChartDelegate?
	
	required init(breathingRateService: BreathingRateWebServiceProtocol, bloodPressureService: BloodPressureWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, temperatureRateService: SkinTemperatureWebServiceProtocol, sleepService: SleepWebServiceProtocol, stepCountService: StepCountWebServiceProtocol, hrvService: HRVWebServiceProtocol, delegate: TrendViewDelegate, chartDelegate: ChartDelegate) {
		self.breathingRateService = breathingRateService
		self.bloodPressureService = bloodPressureService
		self.pulseRateService = pulseRateService
		self.temperatureRateService = temperatureRateService
		self.delegate = delegate
		self.sleepService = sleepService
		self.stepCountService = stepCountService
		self.hrvService = hrvService
		self.chartDelegate = chartDelegate
	}
	
	func initialiseSleepModel() {
		
		let systolicModel = TrendModel(parameter: "SYSTOLIC BLOOD PRESSURE", avgValue: 0, unit: "mm Hg", chartData: TrendChartData(min: NSNumber(value: 80), max: NSNumber(value: 160), limitLines: [], elements: []))
		let diastolicModel = TrendModel(parameter: "DIASTOLIC BLOOD PRESSURE", avgValue: 0, unit: "mm Hg", chartData: TrendChartData(min: NSNumber(value: 40), max: NSNumber(value: 120), limitLines: [], elements: []))
		let sleepStageModel = TrendModel(parameter: "SLEEP STAGES", avgValue: 0, unit: nil, chartData: TrendChartData(limitLines: [], elements: []))
		let heartRateModel = TrendModel(parameter: "HEART RATE", avgValue: 0, unit: "bpm", chartData: TrendChartData(min: NSNumber(value: 40), max: NSNumber(value: 200), limitLines: [], elements: []))
		let breathingRateModel = TrendModel(parameter: "BREATHING RATE", avgValue: 0, unit: "brpm", chartData: TrendChartData(min: NSNumber(value: 12), max: NSNumber(value: 20), limitLines: [], elements: []))
		let tempModel = TrendModel(parameter: "SKIN TEMP VARIATION", avgValue: 0, unit: "°f", chartData: TrendChartData(min: NSNumber(value: 96), max: NSNumber(value: 102), limitLines: [], elements: []))
		let hrvModel = TrendModel(parameter: "HEART RATE VARIATION", avgValue: 0, unit: "ms", chartData: TrendChartData(min: NSNumber(value: 25), max: NSNumber(value: 55), limitLines: [], elements: []))
		sleepTrendList = [systolicModel, diastolicModel, sleepStageModel, heartRateModel, hrvModel, breathingRateModel, tempModel]
	}
	
	func initialiseActivityModel() {
		
		let stepCountModel = TrendModel(parameter: "STEP COUNT", avgValue: 0, unit: "Steps", chartData: TrendChartData(min: NSNumber(value: 0), limitLines: [], elements: []))
		let calorieModel = TrendModel(parameter: "CALORIE COUNT", avgValue: 0, unit: "Cal", chartData: TrendChartData(min: NSNumber(value: 0), limitLines: [], elements: []))
		activityTrendList = [stepCountModel, calorieModel]
	}
	
	func fetchSleepData(range: DateRange) {
		self.isSleepData = true
		self.initialiseSleepModel()
//		self.fetchBreathingRate(range: range)
//		self.fetchBloodPressure(range: range)
//		self.fetchPulseRate(range: range)
//		self.fetchTemperature(range: range)
//		self.fetchSleepStage(range: range)
//		self.fetchHRVData(range: range)
	}
	
	func fetchActivityData(range: DateRange) {
		self.isSleepData = false
		self.initialiseActivityModel()
//		self.fetchStepCount(range: range)
	}
	
	private func fetchStepCount(range: DateRange) {
		self.stepCountService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				var averageStepCount = item.reduce(0) { (result: Int, nextItem: CoreDataStepCount) -> Int in
					return result + nextItem.value.intValue
				}
				
				let limitLines = [UserDefault.shared.goals.stepCountGoal.value as! Int]
				if !item.isEmpty {
					let elements = CoreDataStepCount.fetchStepCountChartData(dateRange: range)
					let count = elements.filter { each in
						return (each.value != nil)
					}.count
					let chartData = TrendChartData(min: NSNumber(value: 0), limitLines: limitLines, elements: elements)
					averageStepCount = (count != 0) ? averageStepCount / count : 0
					let model = TrendModel(parameter: "Step Count", avgValue: averageStepCount, unit: "Steps", chartData: chartData)
					self?.activityTrendList[0] = model
				} else {
					let chartData = TrendChartData(min: NSNumber(value: 0), limitLines: limitLines, elements: [])
					let model = TrendModel(parameter: "Step Count", avgValue: 0, unit: "Steps", chartData: chartData)
					self?.activityTrendList[0] = model
				}
				
				var averageCalorie = item.reduce(0) { (result: Int, nextItem: CoreDataStepCount) -> Int in
					return result + (nextItem.calories?.intValue ?? 0)
				}
				
				let calorieLimitLines = [UserDefault.shared.goals.caloriesBurntGoal.value as! Int]
				if !item.isEmpty {
					let elements = CoreDataStepCount.fetchCalorieChartData(dateRange: range)
					let count = elements.filter { each in
						return (each.value != nil)
					}.count
					let chartData = TrendChartData(min: NSNumber(value: 0), limitLines: calorieLimitLines, elements: elements)
					averageCalorie = (count != 0) ? averageCalorie / count : 0
					let model = TrendModel(parameter: "Calorie Count", avgValue: averageCalorie, unit: "Cal", chartData: chartData)
					self?.activityTrendList[1] = model
				} else {
					let chartData = TrendChartData(min: NSNumber(value: 0), limitLines: calorieLimitLines, elements: [])
					let model = TrendModel(parameter: "Calorie Count", avgValue: 0, unit: "Cal", chartData: chartData)
					self?.activityTrendList[1] = model
				}
			}
			
			self?.delegate?.dismisHud()
		}
	}
	
	private func fetchBreathingRate(range: DateRange) {
		
		self.breathingRateService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			
			if let item = item {
				var averageBreathingRate = item.reduce(0) { (result: Int, nextItem: CoreDataBreathingRate) -> Int in
					return result + nextItem.value.intValue
				}
				let breathingRateRange = UserDefault.shared.goals.breathingRate.value as! Range<Int>
				let limitLines = [breathingRateRange.lowerBound, breathingRateRange.upperBound]
				if !item.isEmpty {
					let elements = CoreDataBreathingRate.fetchBreathingRateChartData(dateRange: range)
					let chartData = TrendChartData(min: NSNumber(value: 12), max: NSNumber(value: 20), limitLines: limitLines, elements: elements)
					averageBreathingRate = (item.count != 0) ? averageBreathingRate / (item.count) : 0
					let model = TrendModel(parameter: "BREATHING RATE", avgValue: averageBreathingRate, unit: "brpm", chartData: chartData)
					self?.sleepTrendList[5] = model
				} else {
					let chartData = TrendChartData(min: NSNumber(value: 12), max: NSNumber(value: 20), limitLines: limitLines, elements: [])
					let model = TrendModel(parameter: "BREATHING RATE", avgValue: 0, unit: "brpm", chartData: chartData)
					self?.sleepTrendList[5] = model
				}
			}
			self?.fetchBloodPressure(range: range)
		}
	}
	
	private func fetchBloodPressure(range: DateRange) {
		self.bloodPressureService.listBloodPressures(range: range.intervalRange) { [weak self] (item, error) in
			
			if let item = item {
				var averageSystolic = item.reduce(0) { (result: Int, nextItem: CoreDataBPReading) -> Int in
					return result + nextItem.systolic.intValue
				}
				let systolicRange = UserDefault.shared.goals.systolic.value as! Range<Int>
				let systolicLimitLines = [systolicRange.lowerBound, systolicRange.upperBound]
				
				var averageDiastolic = item.reduce(0) { (result: Int, nextItem: CoreDataBPReading) -> Int in
					return result + nextItem.diastolic.intValue
				}
				let diastolicRange = UserDefault.shared.goals.diastolic.value as! Range<Int>
				let diastolicLimitLines = [diastolicRange.lowerBound, diastolicRange.upperBound]
				
				if !item.isEmpty {
					let elements = CoreDataBPReading.fetchBloodPressureChartData(dateRange: range).1
					let chartData = TrendChartData(min: NSNumber(value: 80), max: NSNumber(value: 160), limitLines: systolicLimitLines, elements: elements)
					averageSystolic = (item.count != 0) ? averageSystolic / (item.count) : 0
					let model = TrendModel(parameter: "SYSTOLIC BLOOD PRESSURE", avgValue: averageSystolic, unit: "mmHg", chartData: chartData)
					self?.sleepTrendList[0] = model
					
					let diastolicElements = CoreDataBPReading.fetchBloodPressureChartData(dateRange: range).0
					let diastolicChartData = TrendChartData(min: NSNumber(value: 40), max: NSNumber(value: 120), limitLines: diastolicLimitLines, elements: diastolicElements)
					averageDiastolic = (item.count != 0) ? averageDiastolic / (item.count) : 0
					let diastolicModel = TrendModel(parameter: "DIASTOLIC BLOOD PRESSURE", avgValue: averageDiastolic, unit: "mmHg", chartData: diastolicChartData)
					self?.sleepTrendList[1] = diastolicModel
				} else {
					let chartData = TrendChartData(min: NSNumber(value: 80), max: NSNumber(value: 160), limitLines: systolicLimitLines, elements: [])
					let model = TrendModel(parameter: "SYSTOLIC BLOOD PRESSURE", avgValue: 0, unit: "mmHg", chartData: chartData)
					self?.sleepTrendList[0] = model
					
					let diastolicChartData = TrendChartData(min: NSNumber(value: 40), max: NSNumber(value: 120), limitLines: diastolicLimitLines, elements: [])
					let diastolicModel = TrendModel(parameter: "DIASTOLIC BLOOD PRESSURE", avgValue: 0, unit: "mmHg", chartData: diastolicChartData)
					self?.sleepTrendList[1] = diastolicModel
				}
			}
			
			self?.fetchPulseRate(range: range)
		}
	}
	
	private func fetchPulseRate(range: DateRange) {
		self.pulseRateService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				var averagePulseRate = item.reduce(0) { (result: Int, nextItem: CoreDataPulseRate) -> Int in
					return result + nextItem.value.intValue
				}
				
				let pulseRateRange = UserDefault.shared.goals.pulseRate.value as! Range<Int>
				let limitLines = [pulseRateRange.lowerBound, pulseRateRange.upperBound]
				if !item.isEmpty {
					let elements = CoreDataPulseRate.fetchPulseRateChartData(dateRange: range)
					let chartData = TrendChartData(min: NSNumber(value: 40), max: NSNumber(value: 200), limitLines: limitLines, elements: elements)
					averagePulseRate = (item.count != 0) ? averagePulseRate / (item.count) : 0
					let model = TrendModel(parameter: "HEART RATE", avgValue: averagePulseRate, unit: "bpm", chartData: chartData)
					self?.sleepTrendList[3] = model
				} else {
					let chartData = TrendChartData(min: NSNumber(value: 40), max: NSNumber(value: 200), limitLines: limitLines, elements: [])
					let model = TrendModel(parameter: "HEART RATE", avgValue: 0, unit: "bpm", chartData: chartData)
					self?.sleepTrendList[3] = model
				}
			}
			
			self?.fetchTemperature(range: range)
		}
	}
	
	private func fetchTemperature(range: DateRange) {
		self.temperatureRateService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				var averageTemperature = item.reduce(0) { (result: Int, nextItem: CoreDataSkinTemperature) -> Int in
					return result + nextItem.value.intValue
				}
				
				let pulseRateRange = UserDefault.shared.goals.temperature.value as! Range<Int>
				let limitLines = [pulseRateRange.lowerBound, pulseRateRange.upperBound]
				if !item.isEmpty {
					let elements = CoreDataSkinTemperature.fetchSkinTemperatureChartData(dateRange: range)
					let chartData = TrendChartData(min: NSNumber(value: 96), max: NSNumber(value: 102), limitLines: limitLines, elements: elements)
					averageTemperature = (item.count != 0) ? averageTemperature / (item.count) : 0
					let model = TrendModel(parameter: "SKIN TEMP VARIATION", avgValue: averageTemperature, unit: "°f", chartData: chartData)
					self?.sleepTrendList[6] = model
				} else {
					let chartData = TrendChartData(min: NSNumber(value: 96), max: NSNumber(value: 102), limitLines: limitLines, elements: [])
					let model = TrendModel(parameter: "SKIN TEMP VARIATION", avgValue: 0, unit: "°f", chartData: chartData)
					self?.sleepTrendList[6] = model
				}
			}
			
			self?.fetchSleepStage(range: range)
		}
	}
	
	private func fetchSleepStage(range: DateRange) {
		self.sleepService.fetch(by: range) { [weak self] (item, error) in
			if let item = item {
				var avgSleep = item.reduce(0) { (result: Int, nextItem: CoreDataSleepStage) -> Int in
					return result + nextItem.value.intValue
				}
				
				let pulseRateRange = UserDefault.shared.goals.sleepHour.value as! Range<Int>
				let limitLines = [pulseRateRange.lowerBound, pulseRateRange.upperBound]
				let elements = CoreDataSleepStage.fetchSleepStageChartData(dateRange: range)
				let chartData = TrendChartData(limitLines: limitLines, elements: elements)
				if !item.isEmpty {
					avgSleep = (item.count != 0) ? avgSleep / (item.count) : 0
					let model = TrendModel(parameter: "SLEEP STAGES", avgValue: avgSleep, unit: nil, chartData: chartData)
					self?.sleepTrendList[2] = model
				} else {
					let model = TrendModel(parameter: "SLEEP STAGES", avgValue: 0, unit: nil, chartData: chartData)
					self?.sleepTrendList[2] = model
				}
			}
			
			self?.fetchHRVData(range: range)
		}
	}
	
	func fetchHRVData(range: DateRange) {
		self.hrvService.fetch(by: range.intervalRange) { [weak self] (item, error) in
			if let item = item {
				var averageHRVRate = item.reduce(0) { (result: Int, nextItem: CoreDataHRV) -> Int in
					return result + nextItem.value.intValue
				}
				let limitLines = [25, 55]
				if !item.isEmpty {
					let elements = CoreDataHRV.fetchHRVChartData(dateRange: range)
					let chartData = TrendChartData(min: NSNumber(value: 25), max: NSNumber(value: 55), limitLines: limitLines, elements: elements)
					averageHRVRate = averageHRVRate / (item.count)
					let model = TrendModel(parameter: "HEART RATE VARIATION", avgValue: averageHRVRate, unit: "ms", chartData: chartData)
					self?.sleepTrendList[4] = model
				} else {
					let chartData = TrendChartData(min: NSNumber(value: 25), max: NSNumber(value: 55), limitLines: limitLines, elements: [])
					let model = TrendModel(parameter: "HEART RATE VARIATION", avgValue: 0, unit: "ms", chartData: chartData)
					self?.sleepTrendList[4] = model
				}
			}
			
			self?.delegate?.dismisHud()
		}
	}
	
	func pieChartForGoal(goal: Double, achievedValue: Double) {
		let chart = HIChart()
		chart.type = "solidgauge"
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		chart.height = 100
		chart.width = 100
		
		let pane = HIPane()
		pane.startAngle = NSNumber(value: (180 - (achievedValue * 18)))
		
		let backGround = HIBackground()
		backGround.backgroundColor = HIColor(uiColor: .black)
		backGround.borderWidth = NSNumber(value: 0)
		backGround.borderColor = HIColor(uiColor: .black)
		backGround.shape = "arc"
		backGround.innerRadius = "75%"
		backGround.outerRadius = "100%"
		pane.background = [backGround]
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let yAxis = HIYAxis()
		yAxis.min = 0
		yAxis.max = 10
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.enabled = NSNumber(value: 0)
		yAxis.tickColor = HIColor(uiColor: .clear)
		yAxis.lineColor = HIColor(uiColor: .clear)
		yAxis.gridLineColor = HIColor(uiColor: .clear)
		yAxis.minorTickColor = HIColor(uiColor: .clear)
		yAxis.labels.formatter = HIFunction(jsFunction: "function(){if(this.value) { return '' } }")
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotoptions = HIPlotOptions()
		plotoptions.gauge = HIGauge()
		plotoptions.gauge.states = HIStates()
		plotoptions.gauge.states.inactive = HIInactive()
		plotoptions.gauge.states.inactive.opacity = NSNumber(value: true)
		plotoptions.gauge.animation = HIAnimationOptionsObject()
		plotoptions.gauge.animation.duration = NSNumber(value: 0)
		plotoptions.gauge.marker = HIMarker()
		plotoptions.gauge.marker.enabled = false
		
		plotoptions.solidgauge = HISolidgauge()
		plotoptions.solidgauge.rounded = false
		plotoptions.solidgauge.dataLabels = HIDataLabelsOptionsObject()
		plotoptions.solidgauge.dataLabels.y = NSNumber(value: achievedValue)
		plotoptions.solidgauge.dataLabels.borderWidth = NSNumber(value: 0)
		plotoptions.solidgauge.dataLabels.enabled = NSNumber(value: 0)
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		let series = HISeries()
		series.data = [["y": achievedValue, "color": UIColor.barPurple.hexString, "innerRadius": "75%"]]
		let marker = HIMarker()
		marker.enabled = false
		series.marker = HIMarker()
		series.marker.states = HIStates()
		series.marker.states.hover = HIHover()
		series.marker.states.hover.enabled = false
		series.step = "center"
//		series.shadow = 5
		series.color = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 1], stops: [
			[0, "#02D7B0"],
			[1, "#228FFE"]
		])
		series.label = HILabel()
		series.label.enabled = NSNumber(value: 0)
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = [series]
		options.plotOptions = plotoptions
		options.legend = legend
		options.responsive = HIResponsive()
		options.yAxis = [yAxis]
		options.pane = pane
		
		self.chartDelegate?.chart(with: options, peakValue: "")
	}
	
	func fetchMonthlyDataForDailyAverage(range: DateRange) {
		self.sleepService.fetchData(by: range.intervalRange){ [weak self] (item, error) in
			if item?.count == 0 {
				if let error = error {
					self?.delegate?.monthlyDailyAvgDataFetched(result: .failure(error))
					return
				}
			}
            if let _dailyAvgResp = item {
                self?.delegate?.monthlyDailyAvgDataFetched(result: .success(_dailyAvgResp))
            }
		}
	}
}
