//
//  TrendDetailPresenter.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/09/21.
//

import Foundation
import Highcharts

// swiftlint:disable all
class TrendDetailPresenter: TrendDetailPresenterProtocol {
	
	private weak var chartDelegate: ChartDelegate?
	
	required init(chartDelegate: ChartDelegate) {
		self.chartDelegate = chartDelegate
	}
	
	func fetchChart(chartData: TrendChartData, isSleepData: Bool) {
		if isSleepData {
			fetchSleepChart(chartData: chartData)
		} else {
			fetchActivityChart(chartData: chartData)
		}
	}
	
	private func fetchSleepChart(chartData: TrendChartData) {
		
		var xAxis = [TimeInterval]()
		var limitLineData = [[[Double?]]]()
		xAxis = chartData.elements.map({ (element) -> TimeInterval in
			return element.date.timeIntervalSince1970
		})
		
		var data = [[Double?]]()
		
		for (pointIndex, each) in chartData.elements.enumerated() {
			data.append([xAxis[pointIndex] * 1000, each.value])
		}
		
		if chartData.limitLines.count > 0 {
			for x in 0 ..< chartData.limitLines.count {
				limitLineData.append([[Double?]]())
				for pointIndex in chartData.elements.indices {
					limitLineData[x].append(([xAxis[pointIndex] * 1000, Double(chartData.limitLines[x])]))
				}
			}
		}
		
		let values = [data]
		let chart = HIChart()
		chart.type = "line"
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
		hixAxis.dateTimeLabelFormats.day = HIDay()
		hixAxis.dateTimeLabelFormats.day.main = "%e"
		
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		
		hixAxis.tickWidth = 0
		hixAxis.lineWidth = 0
		
		hixAxis.tickInterval = NSNumber(value: 3600 * 1000 * 24 * 2)
		
		hixAxis.lineWidth = 0
		
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.overflow = "justify"
		yAxis.labels.style = HICSSObject()
		yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		yAxis.labels.style.fontSize = "12"
		yAxis.labels.style.color = "#a5a5b2"
		yAxis.min = chartData.min
		yAxis.max = chartData.max
		yAxis.gridLineWidth = NSNumber(value: 0)
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotoptions = HIPlotOptions()
		plotoptions.line = HILine()
		plotoptions.line.states = HIStates()
		plotoptions.line.states.inactive = HIInactive()
		plotoptions.line.states.inactive.opacity = NSNumber(value: true)
		plotoptions.line.animation = HIAnimationOptionsObject()
		plotoptions.line.animation.duration = NSNumber(value: 0)
		
		hixAxis.labels.enabled = true
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		var finalSeries:[HISeries] = []
		let series = HISeries()
		let uiColors = [UIColor.textColor.hexString]
		if limitLineData.count > 0 {
			for y in 0..<limitLineData.count {
				series.data = values[0] as [Any]
				series.lineWidth = 4.0
				series.threshold = NSNumber(value: chartData.limitLines[y])
				series.negativeColor = ((y == 1) ? HIColor(uiColor: .barPink) : HIColor(uiColor: .barPurple))
				finalSeries.append(series)
			}
		} else {
				series.data = values[0] as [Any]
				series.lineWidth = 4.0
				finalSeries.append(series)
		}
		
		let marker = HIMarker()
		marker.enabled = false
		series.marker = HIMarker()
		
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		for y in 0 ..< limitLineData.count {
			let limitLineSeries = HISeries()
			limitLineSeries.data = limitLineData[y]
			limitLineSeries.dashStyle = "shortdot"
			limitLineSeries.color = HIColor(uiColor: UIColor.textColor)
			limitLineSeries.marker = marker
			
			finalSeries.append(limitLineSeries)
		}
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = finalSeries
		options.colors = uiColors
		options.plotOptions = plotoptions
		options.time = time
		options.legend = legend
		
		self.chartDelegate?.chart(with: options, peakValue: "")
	}
	
	private func fetchActivityChart(chartData: TrendChartData) {
		
		var xAxis = [TimeInterval]()
		xAxis = chartData.elements.map({ (element) -> TimeInterval in
			return element.date.timeIntervalSince1970
		})
		
		var data = [[Double?]]()
		
		for (pointIndex, each) in chartData.elements.enumerated() {
			data.append([xAxis[pointIndex] * 1000, each.value])
		}
		let values = [data]
		let chart = HIChart()
		chart.type = "column"
		chart.backgroundColor = HIColor(uiColor: UIColor.clear)
		
		let hititle = HITitle()
		hititle.text = ""
		
		let hisubtitle = HISubtitle()
		hisubtitle.text = ""
		
		let hixAxis = HIXAxis()
		hixAxis.type = "datetime"
		hixAxis.dateTimeLabelFormats = HIDateTimeLabelFormats()
		hixAxis.dateTimeLabelFormats.day = HIDay()
		hixAxis.dateTimeLabelFormats.day.main = "%e"
		
		let hixAxisTitle = HITitle()
		hixAxisTitle.text = ""
		hixAxis.title = hixAxisTitle
		
		hixAxis.tickWidth = 0
		hixAxis.lineWidth = 0
		
		hixAxis.minRange = NSNumber(value: 3600 * 1000 * 24)
		hixAxis.tickInterval = NSNumber(value: 3600 * 1000 * 24 * 3)
		
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		hixAxis.labels = HILabels()
		hixAxis.labels.style = HICSSObject()
		hixAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		hixAxis.labels.style.fontSize = "12"
		hixAxis.labels.style.color = "#a5a5b2"
		
		let yAxis = HIYAxis()
		yAxis.title = HITitle()
		yAxis.title.text = ""
		yAxis.labels = HILabels()
		yAxis.labels.overflow = "justify"
		yAxis.labels.style = HICSSObject()
		yAxis.labels.style.fontFamily = Utility.getFontFamilyForCharts()
		yAxis.labels.style.fontSize = "12"
		yAxis.labels.style.color = "#a5a5b2"
		yAxis.gridLineWidth = NSNumber(value: 0)
		
		let tooltip = HITooltip()
		tooltip.headerFormat = "<span style=\"font-size:10px\">{point.key}</span><table>"
		tooltip.pointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y}</b></td></tr>"
		tooltip.footerFormat = "</table>"
		tooltip.shared = true
		tooltip.useHTML = true
		tooltip.positioner = HIFunction(jsFunction: "function(labelWidth, labelHeight, point) { var tooltipX = point.plotX; var tooltipY = point.plotY + 30; return { x: tooltipX, y: tooltipY };}")
		
		let legend = HILegend()
		legend.enabled = NSNumber(value: false)
		
		let plotoptions = HIPlotOptions()
		plotoptions.column = HIColumn()
		plotoptions.column.states = HIStates()
		plotoptions.column.states.inactive = HIInactive()
		plotoptions.column.states.inactive.opacity = NSNumber(value: true)
		hixAxis.labels.enabled = true
		hixAxis.tickColor = HIColor(uiColor: UIColor.textColor)
		plotoptions.column.animation = HIAnimationOptionsObject()
		plotoptions.column.animation.duration = NSNumber(value: 0)
		//		plotoptions.column.pointWidth = 300
		
		let credits = HICredits()
		credits.enabled = NSNumber(value: false)
		
		let exporting = HIExporting()
		exporting.enabled = false
		
		var series = [HISeries]()
		let bar = HIColumn()
		bar.name = "Data"
		bar.data = values[0] as [Any]
		series.append(bar)
		
		let uiColors = [UIColor.barPurple.hexString]
		
		let time =  HITime()
		time.timezoneOffset = NSNumber(value: (-1 * (TimeZone.current.secondsFromGMT() / 60)))
		
		let options = HIOptions()
		options.chart = chart
		options.title = hititle
		options.subtitle = hisubtitle
		options.xAxis = [hixAxis]
		options.yAxis = [yAxis]
		options.tooltip = tooltip
		options.credits = credits
		options.exporting = exporting
		options.series = series
		options.colors = uiColors
		options.plotOptions = plotoptions
		options.time = time
		options.legend = legend
		
		self.chartDelegate?.chart(with: options, peakValue: "")
	}
}
