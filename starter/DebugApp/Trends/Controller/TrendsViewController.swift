//
//  TrendsViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 23/09/21.
//

import UIKit
import Highcharts
import SwiftUI

// swiftlint:disable all
class TrendsViewController: UIViewController {

	@IBOutlet weak var segmentView: CustomSegmentView!
	@IBOutlet weak var tableView: UITableView!
	
	//Dashboard Cards View
	@IBOutlet weak var mainScrollView: UIScrollView!
	@IBOutlet weak var healthScoreView: UIView!
	@IBOutlet weak var healthScoreChartView: HIChartView!
	@IBOutlet weak var healthScoreValueLabel: MovanoLabel!
	@IBOutlet weak var healthScoreLabel: MovanoLabel!
	
	@IBOutlet weak var stepCountCardView: UIView!
	@IBOutlet weak var stepGoalLabel: MovanoLabel!
	
    @IBOutlet weak var stepCountProgressCustomView: UIView!
    @IBOutlet weak var sleepCardView: UIView!
	@IBOutlet weak var sleepGoalLabel: MovanoLabel!
	
    @IBOutlet weak var sleepProgressCustomView: UIView!
    @IBOutlet weak var compareCardView: UIView!
	@IBOutlet weak var compareValueLabel: MovanoLabel!
	
    @IBOutlet weak var sleepVsStepView: UIView!
    var presenter: TrendPresenter?
	
	var list = Trend() {
		didSet {
			tableView.reloadData()
		}
	}
	
	var selectedDate: DateRange! {
		didSet {
			self.fetchData()
		}
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		self.registerPresenters()
		self.registerCellToListView()
		
		self.segmentView.delegate = self
		self.segmentView.segmentList = ["Sleep", "Activity"]
		
		let todaysDate = Date()
		let modDate = Calendar.current.date(byAdding: .day, value: -29, to: todaysDate)
        hud.show(in: self.view)
		self.presenter?.fetchMonthlyDataForDailyAverage(range: DateRange(start: modDate ?? Date(), end: todaysDate, type: .monthly))
        
        //setupSwiftUI()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.calendarDateSelected), name: NSNotification.Name("calendarDateSelected"), object: nil)
		
		self.selectedDate = DateManager.current.selectedMonth
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name("calendarDateSelected"), object: nil)
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			let breathingRateService = BreathingRateWebService()
			let pulseRateService = PulseRateWebService()
			let temperatureService = SkinTemperatureWebService()
			let bpService = BloodPressureWebService()
			let sleepService = SleepWebService()
			let stepCountService = StepCountWebService()
			let hrvService = HRVWebService()
			self.presenter = TrendPresenter(breathingRateService: breathingRateService, bloodPressureService: bpService, pulseRateService: pulseRateService, temperatureRateService: temperatureService, sleepService: sleepService, stepCountService: stepCountService, hrvService: hrvService, delegate: self, chartDelegate: self)
		}
	}
	
	func fetchData() {
//		self.presenter?.pieChartForGoal(goal: 10, achievedValue: 0)
		
		/*
		hud.show(in: self.parent?.parent?.view ?? self.view)
		if self.segmentView.selectedSegment == 0 {
			self.presenter?.fetchSleepData(range: selectedDate)
		} else if self.segmentView.selectedSegment == 1 {
			self.presenter?.fetchActivityData(range: selectedDate)
		}
		 */
	}
	
	func registerCellToListView() {
		let glucoseCell = UINib.init(nibName: "TrendTableViewCell", bundle: nil)
		self.tableView.register(glucoseCell, forCellReuseIdentifier: TrendTableViewCell.IDENTIFIER)
	}
    
    func monthlyDailyAvgDataFetched(result: Result<[CoreDataSleepTime], MovanoError>) {
        switch result {
        case .failure(let error):
            print(error)
            hud.dismiss()
        case .success(let dailyAvgDataModel):
            self.parceMonthlyDailyAvgData(monthlyDailyAvgData: dailyAvgDataModel)
        }
    }
    
    func parceMonthlyDailyAvgData(monthlyDailyAvgData:[CoreDataSleepTime]){
        let pink = Color(UIColor(named: "TrendsPink") ?? UIColor.green)
        let green = Color(UIColor(named: "TrendsGreen") ?? UIColor.green)
        let lightGreen = Color(UIColor(named: "TrendsLightGreen") ?? UIColor.green)
        let black = Color(UIColor(named: "TrendsBlack") ?? UIColor.green)
        var dataDict:[String:Any?] = [:]
        var stepProgressData:[DayElementModel] = []
        var sleepProgressData:[DayElementModel] = []
        let todaysDate = Date()
        let nilValue: Any? = nil
        for index in stride(from: 0, through: -29, by: -1) {
            if let modDate = Calendar.current.date(byAdding: .day, value: index, to: todaysDate){
                let key = modDate.dateToString(format: DateFormat.serverDateFormat)
                dataDict[key] = nilValue
                stepProgressData.append(DayElementModel(color: black, lableColor: loadLableColor(forDate: modDate)))
            }
        }
        
        for eachItem in monthlyDailyAvgData{
            let key =  eachItem.date.dateToString(format: DateFormat.serverDateFormat)
            dataDict[key] = eachItem
        }
        
        let dateArraySorted = dataDict.sorted { $0.key > $1.key }
        
        var totalScore = 0.0
        var totalSleepHr = 0
        var healthScoreNonZeroNonNilVlaues = 0.0
        var sleepNonZeroNonNilVlaues = 0
        var stepGoalAchievedDays = 0
        var inactiveDays = 0
        var goodSleepDyas = 0
        var badSleepDyas = 0
        var goalSleepMinutes = 0
        var increasedSleepHrTotal = 0
        var nonIncreasedSleepTotal = 0
        var daysOnSleepIncreased = 0
        var increasedStepsTotal = 0
        if let goalSleepHr = UserDefault.shared.goals.sleepHour.value as? Range<Int>{
             goalSleepMinutes = goalSleepHr.upperBound * 60
        }
        var goalStepCount = 0
        if let goalStepCount_ = UserDefault.shared.goals.stepCountGoal.value as? Int {
            goalStepCount = goalStepCount_
        }

        
        for index in 0..<dateArraySorted.count{
            if let coreDataSleepObj = dateArraySorted[index].value as? CoreDataSleepTime{
                //Health Score
                if(coreDataSleepObj.healthScore != 0.0){
                    totalScore = totalScore + coreDataSleepObj.healthScore
                    healthScoreNonZeroNonNilVlaues = healthScoreNonZeroNonNilVlaues + 1
                }
                //Sleep Progress Model
                 var sleepScore = 0.0
                let achievedSleepMinutes = (coreDataSleepObj.awakeValue + coreDataSleepObj.remValue + coreDataSleepObj.deepValue + coreDataSleepObj.lightValue)
                 if(achievedSleepMinutes >= goalSleepMinutes){
                     sleepScore = 10.0
                 }else{
                     sleepScore = ((Double(achievedSleepMinutes)/Double(goalSleepMinutes)) * 100) / 10
                 }
                if(sleepScore < 6.0){
                    badSleepDyas = badSleepDyas + 1
                }else if(sleepScore > 8.0){
                    goodSleepDyas = goodSleepDyas + 1
                }
                
                if(achievedSleepMinutes > 0){
                    totalSleepHr = achievedSleepMinutes + totalSleepHr
                    sleepNonZeroNonNilVlaues = sleepNonZeroNonNilVlaues + 1
                }
                 //Step progress model
                let achievedStepCount = coreDataSleepObj.totalStepCount
                if(achievedStepCount >= goalStepCount){
                    stepGoalAchievedDays = stepGoalAchievedDays + 1
                    stepProgressData[index].color = green
                }else if(achievedStepCount == 0){
                    inactiveDays = inactiveDays + 1
                    stepProgressData[index].color = black
                }else{
                    stepProgressData[index].color = lightGreen
                }
                
                //Sleep and step comparison
                if(achievedStepCount > goalStepCount && achievedSleepMinutes > goalSleepMinutes){
                    increasedStepsTotal = increasedStepsTotal + achievedStepCount
                    increasedSleepHrTotal = increasedSleepHrTotal + achievedSleepMinutes
                    daysOnSleepIncreased = daysOnSleepIncreased + 1
                    
                    sleepProgressData.append(DayElementModel(color: pink, lableColor: Color.clear))
                }else{
                    nonIncreasedSleepTotal = nonIncreasedSleepTotal + achievedSleepMinutes
                    sleepProgressData.append(DayElementModel(color: black, lableColor: Color.clear))
                }
            }else{
                //Data is not present for the day consider as inactive
                stepProgressData[index].color = black
                inactiveDays = inactiveDays + 1
                sleepProgressData.append(DayElementModel(color: black, lableColor: Color.clear))
            }
        }
        
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 1
        formatter.roundingMode = .down
        
        stepGoalLabel.text = "You reached your step goals \(stepGoalAchievedDays) times and were inactive for \(inactiveDays) days in the last 30 days."
        var avgSleep = 0
        if(sleepNonZeroNonNilVlaues > 0){
            avgSleep = totalSleepHr / sleepNonZeroNonNilVlaues
            let avgSleepInStr = Utility.convertminutesToHMFormat(avgSleep)
            sleepGoalLabel.text = "You slept \(avgSleepInStr) (avg) in the past 30 days."
        }
        
        var avgStepIncreasedStr = "\(goalStepCount)"
        if(goalStepCount > 1000){
//            let avgStepInc = Double(goalStepCount) / 1000
            let avgStepIncStr = formatter.string(from: NSNumber.init(value: goalStepCount))
            avgStepIncreasedStr = "\(avgStepIncStr ?? avgStepIncreasedStr)"
        }
        if(daysOnSleepIncreased > 0){
            let avgSleepIncreased  = increasedSleepHrTotal / daysOnSleepIncreased
            let avgNonIncSleep = nonIncreasedSleepTotal / (30 - daysOnSleepIncreased)
            let sleepAvgDiff = avgSleepIncreased - avgNonIncSleep
            let avgSleepIncreasedStr = Utility.convertminutesToHMFormat(sleepAvgDiff)
            compareValueLabel.text = "Your sleep increased by \(avgSleepIncreasedStr) on the days when you walked \(avgStepIncreasedStr) Steps in past 30 days."
        }else{
            compareValueLabel.text = "Your sleep increased by 0h 0m on the days when you walked \(avgStepIncreasedStr) Steps in past 30 days."
        }
        
        if(healthScoreNonZeroNonNilVlaues > 0){
            let avgScore = totalScore / healthScoreNonZeroNonNilVlaues
            let scoreInString = formatter.string(from: NSNumber.init(value: avgScore))
            healthScoreValueLabel.text = scoreInString
			self.presenter?.pieChartForGoal(goal: 10, achievedValue: avgScore)
        }
        
        
        //SwiftUI Custom views
        //Step count progress custom view
        for view in self.stepCountProgressCustomView.subviews{
            view.removeFromSuperview()
        }
        let childView = UIHostingController(rootView:  StepGoalProgressView(dataList:stepProgressData ))
        childView.view.frame = self.stepCountProgressCustomView.bounds
        childView.view.backgroundColor = UIColor.clear
        self.stepCountProgressCustomView.addSubview(childView.view)
        
        //Sleep progress view
        for view in self.sleepProgressCustomView.subviews{
            view.removeFromSuperview()
        }
        let childView2 = UIHostingController(rootView: SleepProgressView(goodSleepDays: goodSleepDyas, badSleepDays: badSleepDyas))
        childView2.view.frame = self.sleepProgressCustomView.bounds
        childView2.view.backgroundColor = UIColor.clear
        self.sleepProgressCustomView.addSubview(childView2.view)
        
        for view in self.sleepVsStepView.subviews{
            view.removeFromSuperview()
        }
        
        let childView3 = UIHostingController(rootView:  StepGoalProgressView(dataList:sleepProgressData ))
        childView3.view.frame = self.sleepVsStepView.bounds
        childView3.view.backgroundColor = UIColor.clear
        self.sleepVsStepView.addSubview(childView3.view)
        hud.dismiss()
    }
    
    func loadLableColor(forDate date: Date) -> Color{
        let trendsText = Color(UIColor(named: "TrendsTextColor") ?? UIColor.white)
        let weekday = Calendar.current.component(.weekday, from: date)
        switch weekday {
        case 1:
            return trendsText
        case 2:
            return Color.clear
        case 3:
            return Color.clear
        case 4:
            return Color.clear
        case 5:
            return Color.clear
        case 6:
            return Color.clear
        case 7:
            return trendsText
        default:
            return Color.clear
        }
    }

}

extension TrendsViewController: TrendViewDelegate {
	
	func fetchTrendList(list: [TrendModel]) {
		self.list = list
	}
	
	func dismisHud() {
		hud.dismiss()
	}
}

extension TrendsViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
}

extension TrendsViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.list.count
	}
	
	func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
		return false
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = TrendTableViewCell.dequeueReusableCell(tableView: tableView, indexPath: indexPath)
		let item = self.list[indexPath.row]
		cell.titleLabel.text = item.parameter
		
		if item.parameter == "Sleep Stages" {
			cell.valueLabel.text = DateComponentsFormatter().difference(from: TimeInterval(item.avgValue * 60))
		} else {
			var value = "\(item.avgValue)"
			
			if let unit = item.unit {
				value += " " + unit
			}
			cell.valueLabel.text = value
		}
		cell.delegate = self
		cell.indexPath = indexPath
		cell.isOpen = item.isOpen
		if self.segmentView.selectedSegment == 0 {
			cell.fetchChart(chartData: item.chartData)
		} else {
			cell.fetchActivityChart(chartData: item.chartData)
		}
		
		return cell
	}
}

extension TrendsViewController: TrendTableViewCellDelegate {
	
	func refreshLayout(for indexpath: IndexPath, isOpen: Bool) {
		self.list[indexpath.row].isOpen = isOpen
	}
	
	func fullScreenCharts(for indexpath: IndexPath, isActivated: Bool) {
		
		let controller = TrendDetailViewController.instantiate(fromAppStoryboard: .trends)
		controller.model = list[indexpath.row]
		controller.isSleepData = (self.segmentView.selectedSegment == 0) ? true: false
        parent?.navigationController?.pushViewController(controller, animated: false)
	}
}

extension TrendsViewController: CustomSegmentViewDelegate {
	
	func segmentChanged(to indexpath: IndexPath) {
		self.fetchData()
	}
}

// MARK: - Calendar Callbacks
extension TrendsViewController {
	
	@objc private func calendarDateSelected(notification: NSNotification) {
		guard let object = notification.object as? DateRange else {
			return
		}
        if(object.type == .monthly){
            self.selectedDate = object
        }
	}
}

extension TrendsViewController: ChartDelegate {
	func chart(with options: HIOptions, peakValue: String) {
		self.healthScoreChartView.isUserInteractionEnabled = false
		self.healthScoreChartView.options = options
	}
}
