//
//  TrendDetailViewController.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/09/21.
//

import UIKit
import Highcharts

class TrendDetailViewController: UIViewController {

	@IBOutlet weak var chartView: HIChartView!
	
	var presenter: TrendDetailPresenter?
	var model: TrendModel!
	var isSleepData: Bool!
	
	override func viewDidLoad() {
        super.viewDidLoad()

		self.registerPresenters()
    }
    
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		AppUtility.lockOrientation([.landscapeLeft])
		
		let value = UIInterfaceOrientation.landscapeLeft.rawValue
		UIDevice.current.setValue(value, forKey: "orientation")
		UINavigationController.attemptRotationToDeviceOrientation()
		
		self.presenter?.fetchChart(chartData: self.model.chartData, isSleepData: isSleepData)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		AppUtility.lockOrientation(.portrait)
		
		let value = UIInterfaceOrientation.portrait.rawValue
		UIDevice.current.setValue(value, forKey: "orientation")
		UINavigationController.attemptRotationToDeviceOrientation()
	}
	
	@IBAction func exitFullscreenClicked(_ sender: Any) {
		self.navigationController?.popViewController(animated: false)
	}
	
	func registerPresenters() {
		if self.presenter == nil {
			self.presenter = TrendDetailPresenter(chartDelegate: self)
		}
	}
}

extension TrendDetailViewController: ChartDelegate {
	
	func chart(with options: HIOptions, peakValue: String) {
		self.chartView.options = options
	}
}
