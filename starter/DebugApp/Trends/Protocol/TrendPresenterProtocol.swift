//
//  TrendPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 23/09/21.
//

import Foundation

// swiftlint:disable all
protocol TrendPresenterProtocol {
	
	init(breathingRateService: BreathingRateWebServiceProtocol, bloodPressureService: BloodPressureWebServiceProtocol, pulseRateService: PulseRateWebServiceProtocol, temperatureRateService: SkinTemperatureWebServiceProtocol, sleepService: SleepWebServiceProtocol, stepCountService: StepCountWebServiceProtocol, hrvService: HRVWebServiceProtocol, delegate: TrendViewDelegate, chartDelegate: ChartDelegate)

	func initialiseSleepModel()
	func initialiseActivityModel()
	func fetchSleepData(range: DateRange)
	func fetchActivityData(range: DateRange)
	func fetchMonthlyDataForDailyAverage(range: DateRange)
}
