//
//  TrendViewDelegate.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 23/09/21.
//

import Foundation

protocol TrendViewDelegate: AnyObject {
	func fetchTrendList(list: [TrendModel])
	func dismisHud()
    func monthlyDailyAvgDataFetched(result: Result<[CoreDataSleepTime], MovanoError>)
}
