//
//  TrendDetailPresenterProtocol.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 30/09/21.
//

import Foundation

protocol TrendDetailPresenterProtocol {
	
	init(chartDelegate: ChartDelegate)

	func fetchChart(chartData: TrendChartData, isSleepData: Bool)
}
