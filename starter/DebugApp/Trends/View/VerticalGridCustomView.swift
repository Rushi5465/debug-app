//
//  VerticalGridCustomView.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 14/12/21.
//

import Foundation
import SwiftUI
import Combine

struct SleepProgressView: View {
    @State var goodSleepDays:Int
    @State var badSleepDays:Int
    var body: some View {
        let green:Color = Color(UIColor(named: "TrendsGreen") ?? UIColor.green)
        let red:Color = Color(UIColor(named: "TrendsRed") ?? UIColor.red)
        HStack {
            VStack{
                HStack{
                    if(badSleepDays > 10){
                        let iterationCount = badSleepDays / 10
                        ForEach(0..<iterationCount){index in
                            VStack(spacing: 2) {
                                    ForEach(0..<10) { index in
                                        SleepRectView(color: red)
                                    }
                                }.frame(maxHeight:.infinity,alignment: .bottom)
                        }
                        let remainingItem = badSleepDays % 10
                        VStack(spacing: 2) {
                                ForEach(0..<remainingItem) { index in
                                    SleepRectView(color: red)
                                }
                        }.frame(maxHeight:.infinity,alignment: .bottom)
                    }else{
                        VStack(spacing: 2) {
                                ForEach(0..<badSleepDays) { index in
                                    SleepRectView(color: red)
                                }
                        }.frame(maxHeight:.infinity,alignment: .bottom)
                    }

                    if(badSleepDays == 0){
                        Text("\(badSleepDays)")
                            .foregroundColor(red)
                            .offset(x: 0, y: 0)
                            .lineLimit(nil)
                           // .font(.system(size: 30))
                            .font(.custom(Utility.getCustomFontName(fontWeight: .Regular), size: 30))
                            .animation(nil)
                            .frame(maxHeight:.infinity,alignment: .bottom)
                    }else{
                        Text("\(badSleepDays)")
                            .foregroundColor(red)
                            .offset(x: 0, y: 8.0)
                            .lineLimit(nil)
                           // .font(.system(size: 30))
                            .font(.custom(Utility.getCustomFontName(fontWeight: .Regular), size: 30))
                            .animation(nil)
                            .frame(maxHeight:.infinity,alignment: .bottom)
                    }
                        
                }.frame(minWidth: 0, maxWidth: .infinity,alignment: .leading)
                
                Text("Poor sleep days")
                    .foregroundColor(Color.white)
                    .lineLimit(nil)
                    //.font(.system(size: 13))
                    .font(.custom(Utility.getCustomFontName(fontWeight: .Regular), size: 13))
                    .animation(nil)
                    .frame(minWidth: 0, maxWidth: .infinity,alignment: .leading)
                    
            }.frame(minWidth: 0, maxWidth: .infinity,alignment: .topLeading)
            
            
            VStack{
                HStack{
                    if(goodSleepDays > 10){
                        let iterationCount = goodSleepDays / 10
                        ForEach(0..<iterationCount){index in
                            VStack(spacing: 2) {
                                    ForEach(0..<10) { index in
                                        SleepRectView(color: green)
                                    }
                                }.frame(maxHeight:.infinity,alignment: .bottom)
                        }
                        let remainingItem = goodSleepDays % 10
                        if(remainingItem != 0){
                            VStack(spacing: 2) {
                                    ForEach(0..<remainingItem) { index in
                                        SleepRectView(color: green)
                                    }
                                }.frame(maxHeight:.infinity,alignment: .bottom)
                        }
                      
                    }else{
                        VStack(spacing: 2) {
                                ForEach(0..<goodSleepDays) { index in
                                    SleepRectView(color: green)
                                }
                        }.frame(maxHeight:.infinity,alignment: .bottom)
                    }
                    if(goodSleepDays == 0){
                        Text("\(goodSleepDays)")
                            .foregroundColor(green)
                            .offset(x: 0, y: 0)
                            .lineLimit(nil)
                           // .font(.system(size: 30))
                            .font(.custom(Utility.getCustomFontName(fontWeight: .Regular), size: 30))
                            .animation(nil)
                            .frame(maxHeight:.infinity,alignment: .bottom)
                    }else{
                        Text("\(goodSleepDays)")
                            .foregroundColor(green)
                            .offset(x: 0, y: 8.0)
                            .lineLimit(nil)
                            //.font(.system(size: 30))
                            .font(.custom(Utility.getCustomFontName(fontWeight: .Regular), size: 30))
                            .animation(nil)
                            .frame(maxHeight:.infinity,alignment: .bottom)
                    }
                        
                }.frame(minWidth: 0, maxWidth: .infinity,alignment: .leading)
                
                Text("Good sleep days")
                    .foregroundColor(Color.white)
                    .lineLimit(nil)
                    //.font(.system(size: 13))
                    .font(.custom(Utility.getCustomFontName(fontWeight: .Regular), size: 13))
                    .animation(nil)
                    .frame(minWidth: 0, maxWidth: .infinity,alignment: .leading)
            }.frame(minWidth: 0, maxWidth: .infinity,alignment: .topLeading)
           
            
            
        }.frame(minWidth: 0, maxWidth: .infinity,alignment: .top)//.frame(width: .infinity, height: 100, alignment: .bottom)
    }
}

struct SleepRectView: View {
    @State var color:Color
    
    var body: some View {
        VStack{
            ZStack {
                Rectangle()
                    .fill(color)
                 .cornerRadius(7)
            }.frame(minWidth: 0, maxWidth: .infinity)//.frame(width:7, height: 30)
        }.frame(width:30, height: 5, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    
       
    }
}
