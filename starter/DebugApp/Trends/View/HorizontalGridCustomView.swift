//
//  HorizontalGridCustomView.swift
//  BloodPressureApp
//
//  Created by Shital Sawant on 14/12/21.
//


import Foundation
import SwiftUI
import Combine

struct DayElementModel{
    var color: Color
    var lableColor: Color
}

struct StepGoalProgressView: View {
    @State var dataList:[DayElementModel]
    var body: some View {
        VStack {
                HStack(spacing: 2) {
                    ForEach(0..<dataList.count) { index in
                        RectView(labelColor: dataList[index].lableColor, color: dataList[index].color)
                    }
                }.frame(minWidth: 0, maxWidth: .infinity, alignment: .topLeading)
                .fixedSize(horizontal: true, vertical: true)

        }//.frame(minWidth: 0, maxWidth: .infinity,alignment: .center)//.frame(width: .infinity, height: 100, alignment: .bottom)
        .frame(
              minWidth: 0,
              maxWidth: .infinity,
              minHeight: 0,
              maxHeight: .infinity,
              alignment: .topLeading
            )
    }
}

struct RectView: View {
    @State var labelColor: Color
    @State var color:Color
    
    var body: some View {

        VStack{
            Text("S").frame(width: 7, height: 20)
                .offset(x: 0, y: 10)
                .foregroundColor(labelColor)
                .lineLimit(nil)
               // .font(.system(size: 10))
                .font(.custom(Utility.getCustomFontName(fontWeight: .Regular), size: 10))
                .animation(nil)

			let superviewWidth = UIScreen.main.bounds.width - 44
			let eachBlocKWidth = (superviewWidth - 58) / 30
            ZStack {
                Rectangle()
                    .fill(color)
                 .cornerRadius(7)
            }.frame(height: 30)
				.frame(width: eachBlocKWidth, height: 30)//.frame(minWidth: 0, maxWidth: .infinity)//
        }//.frame(minWidth: 0, maxWidth: .infinity, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    
       
    }
}
