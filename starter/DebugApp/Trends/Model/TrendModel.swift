//
//  TrendModel.swift
//  BloodPressureApp
//
//  Created by Sankalp Gupta on 23/09/21.
//

import Foundation
import UIKit

struct TrendModel {
	
	let parameter: String
	let avgValue: Int
	let unit: String?
	var isOpen: Bool = false
	let chartData: TrendChartData
}

struct TrendChartData {
	var min: NSNumber?
	var max: NSNumber?
	var limitLines: [Int]
	var elements: [ChartElement]
}

typealias Trend = [TrendModel]
